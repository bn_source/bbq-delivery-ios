//
//  Created by Bhamidipati Kishore on 28/08/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQLoginVerifyUserTests.swift
//

import XCTest
import Hippolyte

class MockLoginVerifyViewModel:LoginVerifyViewModel {
}

class BBQLoginVerifyUserTests: XCTestCase {
    
    var mockloginVerifyViewModel:MockLoginVerifyViewModel!
    var mockVerifyUserDataModel:VerifyUserDataModel?
    var mockGenerateOTPDataModel:GenerateOTPDataModel?
    var mockVerifyOtpDataModel: VerifyOTPDataModel?
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        mockloginVerifyViewModel = MockLoginVerifyViewModel()
    }
    
    ////////////////////////////////////////////////////////////////////////////////
    // MARK: - VerifyUser
    
    func testLoginVerifyUserEmptyCountryCode() {
        let e = expectation(description: "Check the empty Country Code")
        mockloginVerifyViewModel.verifyUserPhoneNUmber(countryCode: "", mobileNumber: 9656016125, completion: { (isLoginSuccess) in
            if isLoginSuccess {
                XCTFail()
            } else {
                XCTAssertEqual(isLoginSuccess, false)
                XCTAssertNotNil(BBQUserManager.networkResponse)
                XCTAssertEqual(BBQUserManager.networkResponse?.statusCode, 400)
            }
            e.fulfill()
        })
        waitForExpectations(timeout: 10.0, handler: nil)
    }
    
    func testLoginVerifyUserEmptyMobileNumber() {
        let e = expectation(description: "Check the empty mobile number")
        mockloginVerifyViewModel.verifyUserPhoneNUmber(countryCode: "+91", mobileNumber: 0, completion: { (isLoginSuccess) in
            if isLoginSuccess {
                XCTFail()
            } else {
                XCTAssertEqual(isLoginSuccess, false)
                XCTAssertNotNil(BBQUserManager.networkResponse)
                XCTAssertEqual(BBQUserManager.networkResponse?.statusCode, 400)
            }
            e.fulfill()
        })
        waitForExpectations(timeout: 10.0, handler: nil)
    }
    
    func testLoginVerifyUserInvalidCountryCode() {
        let e = expectation(description: "Check the invalid Country Code")
        mockloginVerifyViewModel.verifyUserPhoneNUmber(countryCode: "+9", mobileNumber: 9656016125, completion: { (isLoginSuccess) in
            if isLoginSuccess {
                XCTFail()
            } else {
                XCTAssertEqual(isLoginSuccess, false)
                XCTAssertNotNil(BBQUserManager.networkResponse)
                XCTAssertEqual(BBQUserManager.networkResponse?.statusCode, 400)
            }
            e.fulfill()
        })
        waitForExpectations(timeout: 10.0, handler: nil)
    }
    
    func testLoginVerifyUserInvalidMobileNumber() {
        let e = expectation(description: "Check the invalid mobile number")
        mockloginVerifyViewModel.verifyUserPhoneNUmber(countryCode: "+91", mobileNumber: 9656, completion: { (isLoginSuccess) in
            if isLoginSuccess {
                XCTFail()
            } else {
                XCTAssertEqual(isLoginSuccess, false)
                XCTAssertNotNil(BBQUserManager.networkResponse)
                XCTAssertEqual(BBQUserManager.networkResponse?.statusCode, 400)
            }
            e.fulfill()
        })
        waitForExpectations(timeout: 10.0, handler: nil)
    }
    
    func testLoginVerifyExistingUser() {
        let e = expectation(description: "Check the Verify User with existing user")
        mockloginVerifyViewModel.verifyUserPhoneNUmber(countryCode: "+91", mobileNumber: 9742892334, completion: { (isLoginSuccess) in
            if isLoginSuccess {            XCTAssertEqual(self.mockloginVerifyViewModel.userVerificationData?.isExistingUser,false)
            } else {
                XCTFail()
            }
            e.fulfill()
        })
        waitForExpectations(timeout: 10.0, handler: nil)
    }
    
    func testLoginVerifyUnRegisteredUser() {
        let e = expectation(description: "Check the Verify User with unregistered user")
        mockloginVerifyViewModel.verifyUserPhoneNUmber(countryCode: "+91", mobileNumber: 9535088103, completion: { (isLoginSuccess) in
            if isLoginSuccess {                XCTAssertEqual(self.mockloginVerifyViewModel.userVerificationData?.isExistingUser, false)
            } else {
                XCTFail()
            }
            e.fulfill()
        })
        waitForExpectations(timeout: 10.0, handler: nil)
    }
    
    func testLoginVerifyUserDataModelNotNil() {
        let e = expectation(description: "Check the Verify User Data Model Not Nil")
        mockloginVerifyViewModel.verifyUserPhoneNUmber(countryCode: "+91", mobileNumber: 9535088103, completion: { (isLoginSuccess) in
            if isLoginSuccess {
                XCTAssertNotNil(self.mockloginVerifyViewModel.userVerificationData)
            } else {
                XCTFail()
            }
            e.fulfill()
        })
        waitForExpectations(timeout: 10.0, handler: nil)
    }
    
    func testMockLoginVerifyUserAPI() {
        
        guard let gitUrl = URL(string: "https://sample.bbq.com") else { return }
        
        var stub = StubRequest(method: .GET, url: gitUrl)
        var response = StubResponse()
        let path = Bundle(for: type(of: self)).path(forResource: "BBQVerifyUser", ofType: "json")!
        
        let data = NSData(contentsOfFile: path)!
        let body = data
        response.body = body as Data
        stub.response = response
        Hippolyte.shared.add(stubbedRequest: stub)
        Hippolyte.shared.start()
        
        let promise = expectation(description: "Verify User")
        URLSession.shared.dataTask(with: gitUrl) { (data, response
            , error) in
            do {
                let decoder = JSONDecoder()
                guard let data = data else { return }
                self.mockVerifyUserDataModel = try decoder.decode(VerifyUserDataModel.self, from: data)
                XCTAssertEqual(self.mockVerifyUserDataModel?.name, "User1")
                promise.fulfill()
            }catch let err {
                print("Error", err)
            }
            }.resume()
        waitForExpectations(timeout: 10, handler: nil)
    }
    
    ////////////////////////////////////////////////////////////////////////////////
    // MARK: - GenerateOTP
    func testMockLoginGenerateOTPEmptyCountryCode() {
        let e = expectation(description: "Check the empty country code")
        mockloginVerifyViewModel.generateOTP(countryCode: "", mobileNumber: 9656016125, otpId: "17", completion:{ (isLoginSuccess) in
            if isLoginSuccess {
                XCTFail()
            } else {
                XCTAssertEqual(isLoginSuccess, false)
            }
            e.fulfill()
        })
        waitForExpectations(timeout: 10.0, handler: nil)
    }
    
    func testLoginGenerateOTPEmptyMobileNumber() {
        let e = expectation(description: "Check the empty mobile number")
        mockloginVerifyViewModel.generateOTP(countryCode: "+91", mobileNumber: 0, otpId: "17", completion: { (isLoginSuccess) in
            if isLoginSuccess {
                XCTFail()
            } else {
                XCTAssertEqual(isLoginSuccess, false)
            }
            e.fulfill()
        })
        waitForExpectations(timeout: 10.0, handler: nil)
    }
    
    func testLoginGenerateOTPEmptyOtpId() {
        let e = expectation(description: "Check the empty mobile number")
        mockloginVerifyViewModel.generateOTP(countryCode: "+91", mobileNumber: 9656016125, otpId: "", completion: { (isLoginSuccess) in
            if isLoginSuccess {
                XCTFail()
            } else {
                XCTAssertEqual(isLoginSuccess, false)
            }
            e.fulfill()
        })
        waitForExpectations(timeout: 10.0, handler: nil)
    }
    
    func testLoginGenerateOTPInvalidCountryCode() {
        let e = expectation(description: "Check the invalid Country Code")
        mockloginVerifyViewModel.generateOTP(countryCode: "+9", mobileNumber: 9656016125, otpId: "17", completion: { (isLoginSuccess) in
            if isLoginSuccess {
                XCTFail()
            } else {
                XCTAssertEqual(isLoginSuccess, false)
            }
            e.fulfill()
        })
        waitForExpectations(timeout: 10.0, handler: nil)
    }
    
    func testLoginGenerateOTPInvalidMobileNumber() {
        let e = expectation(description: "Check the invalid mobile number")
        mockloginVerifyViewModel.generateOTP(countryCode: "+91", mobileNumber: 9656,otpId: "17", completion: { (isLoginSuccess) in
            if isLoginSuccess {
                XCTFail()
            } else {
                XCTAssertEqual(isLoginSuccess, false)
            }
            e.fulfill()
        })
        waitForExpectations(timeout: 10.0, handler: nil)
    }
    
    func testLoginGenerateOTPDataModelNotNil() {
        let e = expectation(description: "Check the Generate OTP Data Model Not Nil")
        mockloginVerifyViewModel.generateOTP(countryCode: "+91", mobileNumber: 9535088103, otpId: "17", completion: { (isLoginSuccess) in
            if isLoginSuccess {
                XCTAssertNotNil(self.mockloginVerifyViewModel.expiryTime)
            } else {
                XCTFail()
            }
            e.fulfill()
        })
        waitForExpectations(timeout: 10.0, handler: nil)
    }
    
    func testMockLoginGenerateOTPAPI() {
        
        guard let gitUrl = URL(string: "https://sample.bbq.com") else { return }
        
        var stub = StubRequest(method: .GET, url: gitUrl)
        var response = StubResponse()
        let path = Bundle(for: type(of: self)).path(forResource: "BBQGenerateOTP", ofType: "json")!
        
        let data = NSData(contentsOfFile: path)!
        let body = data
        response.body = body as Data
        stub.response = response
        Hippolyte.shared.add(stubbedRequest: stub)
        Hippolyte.shared.start()
        
        let promise = expectation(description: "Generate OTP")
        URLSession.shared.dataTask(with: gitUrl) { (data, response
            , error) in
            do {
                let decoder = JSONDecoder()
                guard let data = data else { return }
                self.mockGenerateOTPDataModel = try decoder.decode(GenerateOTPDataModel.self, from: data)
                XCTAssertEqual(self.mockGenerateOTPDataModel?.expiryTime, 5)
                promise.fulfill()
            }catch let err {
                print("Error", err)
            }
            }.resume()
        waitForExpectations(timeout: 10, handler: nil)
    }
    
    ////////////////////////////////////////////////////////////////////////////////
    // MARK: - VerifyOTP
    func testMockLoginVerifyOTPWithEmptyOTP() {
        let e = expectation(description: "Check the empty otp")
        mockloginVerifyViewModel.verifyOTP(otp: 0, otpId: "17", completion:{ (isLoginSuccess) in
            if isLoginSuccess {
                XCTFail()
            } else {
                XCTAssertEqual(isLoginSuccess, false)
            }
            e.fulfill()
        })
        waitForExpectations(timeout: 10.0, handler: nil)
    }
    
    func testLoginVerifyOTPEmptyWithEmptyOTPId() {
        let e = expectation(description: "Check the empty otpId")
        mockloginVerifyViewModel.verifyOTP(otp: 742824, otpId: "", completion: { (isLoginSuccess) in
            if isLoginSuccess {
                XCTFail()
            } else {
                XCTAssertEqual(isLoginSuccess, false)
                XCTAssertNotNil(BBQUserManager.networkResponse)
                XCTAssertEqual(BBQUserManager.networkResponse?.statusCode, 400)
            }
            e.fulfill()
        })
        waitForExpectations(timeout: 10.0, handler: nil)
    }
    
    func testLoginVerifyOTPWithInvalidOtp() {
        let e = expectation(description: "Check the invalid otp")
        mockloginVerifyViewModel.verifyOTP(otp: 7424, otpId: "17", completion: { (isLoginSuccess) in
            if isLoginSuccess {
                XCTFail()
            } else {
                XCTAssertEqual(isLoginSuccess, false)
                XCTAssertNotNil(BBQUserManager.networkResponse)
                XCTAssertEqual(BBQUserManager.networkResponse?.statusCode, 400)
            }
            e.fulfill()
        })
        waitForExpectations(timeout: 10.0, handler: nil)
    }
    
    func testLoginVerifyOTPWithInvalidOtpId() {
        let e = expectation(description: "Check the invalid otpId")
        mockloginVerifyViewModel.verifyOTP(otp: 742824, otpId: "17565", completion: { (isLoginSuccess) in
            if isLoginSuccess {
                XCTFail()
            } else {
                XCTAssertEqual(isLoginSuccess, false)
                XCTAssertNotNil(BBQUserManager.networkResponse)
                XCTAssertEqual(BBQUserManager.networkResponse?.statusCode, 400)
            }
            e.fulfill()
        })
        waitForExpectations(timeout: 10.0, handler: nil)
    }
    
    func testLoginVerifyOTPDataModelNotNil() {
        let e = expectation(description: "Check the Verify OTP Data Model Not Nil")
        mockloginVerifyViewModel.verifyOTP(otp: 742824, otpId: "17", completion: { (isLoginSuccess) in
            if isLoginSuccess {
                XCTAssertNotNil(self.mockloginVerifyViewModel.optVerificationMessage)
            } else {
                XCTFail()
            }
            e.fulfill()
        })
        waitForExpectations(timeout: 10.0, handler: nil)
    }
    
    func testMockLoginVerifyOTPAPI() {
        
        guard let gitUrl = URL(string: "https://sample.bbq.com") else { return }
        
        var stub = StubRequest(method: .GET, url: gitUrl)
        var response = StubResponse()
        let path = Bundle(for: type(of: self)).path(forResource: "BBQVerifyOTP", ofType: "json")!
        
        let data = NSData(contentsOfFile: path)!
        let body = data
        response.body = body as Data
        stub.response = response
        Hippolyte.shared.add(stubbedRequest: stub)
        Hippolyte.shared.start()
        
        let promise = expectation(description: "Generate OTP")
        URLSession.shared.dataTask(with: gitUrl) { (data, response
            , error) in
            do {
                let decoder = JSONDecoder()
                guard let data = data else { return }
                self.mockVerifyOtpDataModel = try decoder.decode(VerifyOTPDataModel.self, from: data)
                XCTAssertEqual(self.mockVerifyOtpDataModel?.responseMessage, "success")
                promise.fulfill()
            }catch let err {
                print("Error", err)
            }
            }.resume()
        waitForExpectations(timeout: 10, handler: nil)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        mockloginVerifyViewModel = nil
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
