//
//  Created by Abhijit on 18/09/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQLocationUserTests.swift
//

import XCTest
import Hippolyte

class BBQLocationUserTests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        MockBBQLocationViewModel = BBQLocationViewModel()
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testLatLongParameter()
    {
        
        let e = expectation(description: "Check the Parameter")
        MockBBQLocationViewModel.getCities(lat: Int, long: Float,completion:)
        completion: { (isgetCitiesCalled) in
            if isgetCitiesCalled {
                XCTFail()
            } else {
                XCTAssertEqual(isgetCitiesCalled, false)
                XCTAssertNotNil(BBQUserManager.networkResponse)
                XCTAssertEqual(BBQUserManager.networkResponse?.statusCode, 400)
            }
            e.fulfill()
        })
        waitForExpectations(timeout: 10.0, handler: nil)
        
    }
    
    func testLatLongEmpty() {
        let e = expectation(description: "Check the empty Lat Long")
        mockloginVerifyViewModel.verifyUserPhoneNUmber(lat: "", long: "", completion: { (isgetCitiesCalled) in
            if isgetCitiesCalled {
                XCTFail()
            } else {
                XCTAssertEqual(isgetCitiesCalled, false)
                XCTAssertNotNil(BBQUserManager.networkResponse)
                XCTAssertEqual(BBQUserManager.networkResponse?.statusCode, 400)
            }
            e.fulfill()
        })
        waitForExpectations(timeout: 10.0, handler: nil)
    }
}
