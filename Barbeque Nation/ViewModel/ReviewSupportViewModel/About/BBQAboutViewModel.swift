//
 //  Created by Rabindra L on 12/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQAboutViewModel.swift
 //

import Foundation

class BBQAboutViewModel {
    var aboutDataModel: [BBQAboutModel]?
    
    func getAboutApp(copyRight: String?) -> String {
        
        let versionNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        let buildNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
        let endText = copyRight?.htmlToString
        let versioData = endText?.components(separatedBy: "\n")

        let  copyRightText = versioData?[2]
        let about = """
        \(kVersionLabel) \(versionNumber)
        \(kBuildLabel) \(buildNumber)
        \(copyRightText ?? "")
        """

       return about
    }
    
    func getItemList() -> [String] {
        let list = [kTermsCondition, kPrivacyPolicy, kAppVersion, kAbtBbqNation]
        return list
    }
    
    func getAboutList(completion: @escaping (Bool) -> ()) {
        BBQHelpSupportManager.getAboutList{ (data, error) in
            if error == nil {
                do {
                    let decoder = JSONDecoder()
                    guard let responseData = data else {
                        DispatchQueue.main.async {
                            completion(false)
                        }
                        return
                    }
                    self.aboutDataModel = try decoder.decode([BBQAboutModel].self, from: responseData)

                    DispatchQueue.main.async {
                        completion(true)
                    }
                } catch {
                    DispatchQueue.main.async {
                        completion(false)
                    }
                }
            } else {
                if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse!.statusCode >= 400 {
                    BBQUserManager.configureErrorModel(data: data)
                }
                DispatchQueue.main.async {
                    completion(false)
                }
            }
        }
    }
}
