//
//  Created by Rabindra L on 03/10/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQFeedbackViewModel.swift
//

import Foundation

class BBQFeedbackViewModel {
    var bookingFeedBackModel: [BBQBookingFeedbackQuestionsModel]?
    
    func getBookingFeedbackQuestions(completion: @escaping (Bool) -> ()) {
        BBQFeedbackManager.getBookingFeedbackQuestions { (data, error) in
            if error == nil {
                do {
                    let decoder = JSONDecoder()
                    guard let responseData = data else {
                        DispatchQueue.main.async {
                            completion(false)
                        }
                        return
                    }
                    self.bookingFeedBackModel =
                        try decoder.decode([BBQBookingFeedbackQuestionsModel].self,
                                                                   from: responseData)
                    
                    DispatchQueue.main.async {
                        completion(true)
                    }
                } catch {
                    DispatchQueue.main.async {
                        completion(false)
                    }
                }
            } else {
                if BBQUserManager.networkResponse !=
                    nil, BBQUserManager.networkResponse?.statusCode == 400 {
                    BBQUserManager.configureErrorModel(data: data)
                }
                DispatchQueue.main.async {
                    completion(false)
                }
            }
        }
    }
    
    func submitBookingFeedback(rating: Int,
                               bookingID:String, qid:Int,
                               completion: @escaping (Bool) -> ()) {
        BBQFeedbackManager.submitBookingFeedback(rating: rating,
                                                 bookingID: bookingID,
                                                 qid: qid){ (data, error) in
            if error == nil {
                do {
                    DispatchQueue.main.async {
                        completion(true)
                    }
                }
            } else {
                if BBQUserManager.networkResponse != nil,
                    BBQUserManager.networkResponse?.statusCode == 400 {
                    BBQUserManager.configureErrorModel(data: data)
                }
                DispatchQueue.main.async {
                    completion(false)
                }
            }
        }
    }
}
