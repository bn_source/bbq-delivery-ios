//
 //  Created by Rabindra L on 13/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQHelpSupportViewModel.swift
 //

import Foundation

class BBQHelpSupportViewModel {
    var faqDataModel: BBQHelpSupportModel?
    var faqByIdDataModel: BBQHelpSupportByIdModel?
    
    func getQuestionList(completion: @escaping (Bool) -> ()) {
        BBQHelpSupportManager.getFAQList{ (data, error) in
            if error == nil {
                do {
                    let decoder = JSONDecoder()
                    guard let responseData = data else {
                        DispatchQueue.main.async {
                            completion(false)
                        }
                        return
                    }
                    self.faqDataModel = try decoder.decode(BBQHelpSupportModel.self, from: responseData)
                    
                    DispatchQueue.main.async {
                        completion(true)
                    }
                } catch {
                    DispatchQueue.main.async {
                        completion(false)
                    }
                }
            } else {
                if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse?.statusCode == 400 {
                    BBQUserManager.configureErrorModel(data: data)
                }
                DispatchQueue.main.async {
                    completion(false)
                }
            }
        }
    }
    
    
    func getQuestionListById(id:[Int], completion: @escaping (Bool) -> ()) {
        BBQHelpSupportManager.getFAQListById(id: id){ (data, error) in
            if error == nil {
                do {
                    let decoder = JSONDecoder()
                    guard let responseData = data else {
                        DispatchQueue.main.async {
                            completion(false)
                        }
                        return
                    }
                    self.faqByIdDataModel = try decoder.decode(BBQHelpSupportByIdModel.self, from: responseData)
                    
                    DispatchQueue.main.async {
                        completion(true)
                    }
                } catch {
                    DispatchQueue.main.async {
                        completion(false)
                    }
                }
            } else {
                if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse?.statusCode == 400 {
                    BBQUserManager.configureErrorModel(data: data)
                }
                DispatchQueue.main.async {
                    completion(false)
                }
            }
        }
    }
    
}
