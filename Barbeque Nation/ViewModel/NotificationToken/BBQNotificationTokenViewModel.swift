//
//  Created by Bhamidipati Kishore on 24/09/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQNotificationTokenViewModel.swift
//

import UIKit

class BBQNotificationTokenViewModel: BaseViewModel {
    
    private var tokenDataModel: BBQNotificationTokenModel?
    
    func deleteNotificationToken(token: String, deviceType: String, completion: @escaping (Bool) -> ()) {
        BBQNotificationTokenManager.deleteNotificationToken(token: token, deviceType: deviceType){ (model, result) in
            if result {
                self.tokenDataModel = model
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    func deleteNotificationTokenForGuestUser(token: String, deviceType: String, completion: @escaping (Bool) -> ()) {
        BBQNotificationTokenManager.deleteNotificationTokenForGuestUser(token: token, deviceType: deviceType){ (model, result) in
            if result {
                self.tokenDataModel = model
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    func postNotificationToken(token: String, deviceType: String, completion: @escaping (Bool) -> ()) {
        BBQNotificationTokenManager.postNotificationToken(token: token, deviceType: deviceType){ (model, result) in
            if result {
                self.tokenDataModel = model
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    func postNotificationTokenForGuestUser(token: String, deviceType: String, completion: @escaping (Bool) -> ()) {
        BBQNotificationTokenManager.postNotificationTokenForGuestUser(token: token, deviceType: deviceType){ (model, result) in
            if result {
                self.tokenDataModel = model
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    func checkAnUpdateFCMToken() {
        if BBQUserDefaults.sharedInstance.firebaseLocalToken !=  BBQUserDefaults.sharedInstance.firebaseToken  {
            
            // received different fcmToken , Store it and in user defaults and procced
            //If received the same token, do not hit api
                        
            if BBQUserDefaults.sharedInstance.accessToken == "" && BBQUserDefaults.sharedInstance.isGuestUserAPI == false {
                
                if BBQUserDefaults.sharedInstance.firebaseLocalToken != BBQUserDefaults.sharedInstance.firebaseToken {
                    
                    //if fcmToken this blank , do not call the api
                    if BBQUserDefaults.sharedInstance.firebaseToken != ""{
                        self.deleteNotificationTokenForGuestUser(token: BBQUserDefaults.sharedInstance.firebaseToken, deviceType: "IOS") { isSuccess in
                            
                        }
                    }
                    self.postNotificationTokenForGuestUser(token: BBQUserDefaults.sharedInstance.firebaseLocalToken, deviceType: "IOS"){ (isSuccess) in
                        if isSuccess {
                            
                            BBQUserDefaults.sharedInstance.firebaseToken = BBQUserDefaults.sharedInstance.firebaseLocalToken
                            print("Create token for guest user in SPlash ",isSuccess)
                        }
                        
                    }
                    BBQUserDefaults.sharedInstance.isGuestUserAPI = true
                }
            }else if BBQUserDefaults.sharedInstance.accessToken != "" && BBQUserDefaults.sharedInstance.firebaseLocalToken != ""{
                if BBQUserDefaults.sharedInstance.firebaseToken != ""{
                    self.deleteNotificationToken(token: BBQUserDefaults.sharedInstance.firebaseToken, deviceType: "IOS") { isSuccess in
                        
                    }
                }
                self.postNotificationToken(token: BBQUserDefaults.sharedInstance.firebaseLocalToken, deviceType: "IOS"){ (isSuccess) in
                    BBQUserDefaults.sharedInstance.firebaseToken = BBQUserDefaults.sharedInstance.firebaseLocalToken
                }
            }
        }
    }
}
