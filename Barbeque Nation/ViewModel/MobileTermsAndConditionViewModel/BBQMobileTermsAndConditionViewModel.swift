//
 //  Created by Bhamidipati Kishore on 14/11/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQMobileTermsAndConditionViewModel.swift
 //

import UIKit

class BBQMobileTermsAndConditionViewModel: BaseViewModel {
    private var mobileTermsAndConditionsModel: [BBQMobileTermsAndConditionsDataModel]? = nil
    
    var getMobileTermsAndConditionsModel: [BBQMobileTermsAndConditionsDataModel]? {
        return mobileTermsAndConditionsModel
    }
}

//MARK: Service Call Handler
extension BBQMobileTermsAndConditionViewModel {
    
    func getMobileTermsAndConditions(completion: @escaping (Bool) -> ()) {
        
        BBQMobileTermsAndConditionManager.getMobileTermsAndConditions(completion: { (data,result) in
            if result
            {
                self.mobileTermsAndConditionsModel = data
                completion(true)
            }else{
                completion(false)
            }
        })    
    }
}
