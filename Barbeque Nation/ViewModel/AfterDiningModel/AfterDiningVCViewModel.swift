//
 //  Created by Sakir Sherasiya on 21/12/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQAfterDiningControllerViewModel.swift
 //

import UIKit

class AfterDiningVCViewModel: BaseViewModel {
    
    var paymentRecieptURLString : String  {
        return self.dataModel?.billLink ?? ""
    }

    
    
    var dataModel: BBQDiningDataModel?
    var arrServerAppliedCoupons = [BookingVoucher]()
    var arrServerAppliedVouchers = [BookingVoucher]()
    var arrServerAppliedDiscount = [DiscountDetails]()
    var arrAppliedCoupons = [VoucherCouponsData]()
    var arrAppliedVoucher = [VoucherCouponsData]()
    
    var appliedSmiles: Int = 0
    var appliedServerSmiles: Int = 0
    var bookingSubTotal: Double = 0{
        didSet{
            
        }
    }
    //var taxAbleAmoint: Double = 0
    var bookingTaxList = [Taxes]()
    var bookingTaxAmount: Double = 0
    var bookingDTaxAmount: Double = 0
    var bookingDVatTaxAmount: Double = 0
    var bookingDCESSTaxAmount: Double = 0
    var bookingVatTaxAmount: Double = 0
    var bookingCESSTaxAmount: Double = 0
    var netPayableAmount: Double = 0
    var responseGSTBaseAmount: Double = 0
    var responseVATBaseAmount: Double = 0
    var newGSTBaseAmount: Double = 0
    var newVATBaseAmount: Double = 0
    var countGSTBaseAmount: Double = 0
    var countVATBaseAmount: Double = 0
    
    var gstRatio: Double = 1.0
    var vatRation: Double = 0.0
    
    // MARK: API Methods
    
    func fetchAfterDiningPaymentData(for bookingID: String, isNeedToRefresh: Bool,
                                     completion: @escaping (_ isFetched: Bool)->()) {
        BBQBookingManager.afterDiningPaymentData(for: bookingID) { (diningData, error) in
            if error == nil {
                var objData = diningData
                objData?.netPayable = diningData?.netPayable == 0 ? diningData?.billTotal : diningData?.netPayable
                self.dataModel = objData
                if isNeedToRefresh{
                    self.setUpPaymentData(model: self.dataModel)
                }
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    func setUpPaymentData(model: BBQDiningDataModel?) {
        guard let model = model else { return }
        countRation()
        self.arrServerAppliedVouchers = model.getVouchers()
        self.arrServerAppliedCoupons = model.getCoupons()
        self.arrServerAppliedDiscount = model.getOfferDiscounts()
        self.appliedServerSmiles = model.loyaltyPoints ?? 0
        self.bookingTaxList = model.taxes ?? [Taxes]()
        self.netPayableAmount = model.netPayable ?? 0.0
        
        self.bookingTaxAmount = 0.0
        self.bookingDTaxAmount = 0.0
        for tax in bookingTaxList{
            self.bookingTaxAmount += tax.taxAmount ?? 0.0
            self.bookingDTaxAmount += tax.taxAmount ?? 0.0
        }
        self.bookingSubTotal = (model.netPayable ?? 0.0) - self.bookingTaxAmount
        if bookingSubTotal <= 0{
            bookingSubTotal = 0
        }
        reverseCountingForTax()
        appliedSmiles = 0
        arrAppliedCoupons = [VoucherCouponsData]()
        arrAppliedVoucher = [VoucherCouponsData]()
    }
    
    func getArrayBreakUpData() -> (invoiceBreakup: [AfterDiningData], calculationCells: [AfterDiningPaymentCellType]) {
        var arrDiningData = [AfterDiningData]()
        var arrCalcuationsCells = [AfterDiningPaymentCellType]()
        arrDiningData.append(AfterDiningData(title: "Bill Amount", value: String(format: "%@%.02f", getCurrency(), dataModel?.billWithoutTax ?? 0.0), breakup: []))
        arrCalcuationsCells.append(.text)
        //payableSubtotalFromCalculation = dataModel?.billWithoutTax ?? 0.0
        //arrCalcuationsCells.append(.subTotal)
        if appliedServerSmiles > 0{
            arrDiningData.append(AfterDiningData(title: "Smiles", value: String(format: "-%@%li", getCurrency(), appliedServerSmiles), breakup: []))
            arrCalcuationsCells.append(.text)
            //payableSubtotalFromCalculation -= Double(appliedServerSmiles)
        }else{
            arrCalcuationsCells.append(.smiles)
        }
        
        if let discount = dataModel?.getOfferDiscount(), discount.total > 0{
            arrDiningData.append(AfterDiningData(title: "Offers", value: String(format: "-%@%.02f", getCurrency(), discount.total), breakup: discount.breakUP))
            arrCalcuationsCells.append(.text)
            //payableSubtotalFromCalculation -= discount.total
        }
        if let coupons = dataModel?.getCouponDiscount(), coupons.total > 0{
            arrDiningData.append(AfterDiningData(title: "Coupons", value: String(format: "-%@%.02f", getCurrency(), coupons.total), breakup: coupons.breakUP))
            arrCalcuationsCells.append(.text)
            //payableSubtotalFromCalculation -= coupons.total
        }else{
            arrCalcuationsCells.append(.coupons)
        }
    
        newGSTBaseAmount = dataModel?.gstBaseAmount ?? 0.0
        newVATBaseAmount = dataModel?.vatBaseAmount ?? 0.0
        countVATBaseAmount = newVATBaseAmount
        countGSTBaseAmount = newGSTBaseAmount
        
        _ = recountTheTax(isCountApplied: true)
        //arrDiningData.append(AfterDiningData(title: "Taxes", value: String(format: "%@%.02f", getCurrency(), taxes.taxAmount), breakup: taxes.taxes))
        //payableSubtotalFromCalculation += taxes.taxAmount
        arrCalcuationsCells.append(.taxes)
//        if vatRation != 0{
//            arrCalcuationsCells.append(.vat)
//            if dataModel?.getCessTax() != 0{
//                arrCalcuationsCells.append(.cess)
//            }
//        }
        
        
        var paid_amount = 0.0
        var paidArray = [AfterDiningData.BreakUp]()
        for payment in dataModel?.payment_details ?? [PaymentDetail](){
            if payment.type != "INVOICE", payment.type != "COUPON", payment.type != "GIFT_COUPON"{
                paid_amount += payment.amount ?? 0.0
                paidArray.append(AfterDiningData.BreakUp(title: payment.name ?? "", value: String(format: "%.02f", payment.amount ?? 0.0)))
            }
        }
        if paid_amount > 0{
            arrDiningData.append(AfterDiningData(title: "Payments", value: String(format: "-%@%.02f", getCurrency(), paid_amount), breakup: paidArray))
            arrCalcuationsCells.append(.text)
            netPayableAmount -= paid_amount
        }
        
//        if let vouchers = dataModel?.payment_details.count > 0{
//            arrDiningData.append(AfterDiningData(title: "Vouchers", value: String(format: "-%@%.02f", getCurrency(), vouchers.total), breakup: vouchers.breakUP))
//            //payableSubtotalFromCalculation -= vouchers.total
//            netPayableAmount -= vouchers.total
//            arrCalcuationsCells.append(.text)
//        }
//        if let payment = dataModel?.getPaidAmounts(), payment.total > 0{
//            arrDiningData.append(AfterDiningData(title: "Paid Amount", value: String(format: "-%@%.02f", getCurrency(), payment.total), breakup: payment.breakUP))
//            payableSubtotalFromCalculation -= payment.total
//            arrCalcuationsCells.append(.text)
//        }
        reverseCountingForTax()
        netPayableAmount = netPayableAmount < 0 ? 0 : netPayableAmount
        arrDiningData.append(AfterDiningData(title: "Payable Amount", value: String(format: "%@%.02f", getCurrency(), netPayableAmount), breakup: []))
        //arrCalcuationsCells.append(.taxes)
        arrCalcuationsCells.append(.vouchers)
        arrCalcuationsCells.append(.netPayable)
        //getSubtotalAmountBeforeTax()
        return (arrDiningData, arrCalcuationsCells)
    }
    
    func countRation() {
        if dataModel?.gstBaseAmount == 0 && dataModel?.vatBaseAmount != 0 {
            gstRatio = 0
            vatRation = 1
        }else if dataModel?.gstBaseAmount != 0 && dataModel?.vatBaseAmount == 0 {
            gstRatio = 1
            vatRation = 0
        }else if dataModel?.gstBaseAmount == 0 && dataModel?.vatBaseAmount == 0 {
            gstRatio = 1
            vatRation = 0
        }else{
            gstRatio = (dataModel?.gstBaseAmount ?? 0.0) / ((dataModel?.gstBaseAmount ?? 0.0) + (dataModel?.vatBaseAmount ?? 0.0))
            vatRation = (dataModel?.vatBaseAmount ?? 0.0) / ((dataModel?.gstBaseAmount ?? 0.0) + (dataModel?.vatBaseAmount ?? 0.0))
        }
    }
    
    func reverseCountingForTax() {
        var tax_per = 1.0
        
        var tax_gst = 0.0
        for list in bookingTaxList {
            tax_gst += list.taxPercentage ?? 0.0
        }
        
        tax_per += (tax_gst * gstRatio)/100
        
        if dataModel?.vatBaseAmount != 0{
            let vat_tax = ((dataModel?.vat_tax?.taxPercentage ?? 0) * vatRation)/100
            tax_per += vat_tax
            tax_per += (vat_tax * (dataModel?.getCessTax() ?? 0))/100
        }
        
        let baseAmount = netPayableAmount / tax_per
        responseGSTBaseAmount = baseAmount * gstRatio
        responseVATBaseAmount = baseAmount * vatRation
    }
    
    
    func updateBooking(finalPaymentData: FinalPaymentUpdateBody, completion: @escaping (Bool)->()) {
        if let jsonData = self.createJsonForFinalPayment(updatePaymentData: finalPaymentData) {
            
            BBQBookingManager.updateFinalPayment(updatePayJsonData: jsonData) { (model, result) in
                if result {
                    completion(true)
                } else {
                    completion(false)
                }
            }
        }
        
        
    }
    
    // MARK: Public Methods
    
    func countTheSubtotalAmount() {
        
        newGSTBaseAmount = dataModel?.gstBaseAmount ?? 0.0
        newVATBaseAmount = dataModel?.vatBaseAmount ?? 0.0
        countGSTBaseAmount = responseGSTBaseAmount
        countVATBaseAmount = responseVATBaseAmount
        
        //taxAbleAmoint = self.bookingSubTotal
        if countGSTBaseAmount <= 0{
            removeCards()
            _ = recountTheTax(isCountApplied: true)
            return
        }
        
        for coupon in arrAppliedCoupons {
            newGSTBaseAmount -= (coupon.denomination ?? 0) * gstRatio
            countGSTBaseAmount -= (coupon.denomination ?? 0) * gstRatio
            if newVATBaseAmount != 0{
                newVATBaseAmount -= (coupon.denomination ?? 0) * vatRation
                countVATBaseAmount -= (coupon.denomination ?? 0) * vatRation
            }
        }
        
        if countGSTBaseAmount <= 0{
            appliedSmiles = 0
            _ = recountTheTax(isCountApplied: true)
            arrAppliedVoucher = [VoucherCouponsData]()
            return
        }
        
        if Int((countGSTBaseAmount+countVATBaseAmount).rounded(.up)) < appliedSmiles{
            appliedSmiles = Int((countGSTBaseAmount+countVATBaseAmount).rounded(.up))
        }
        newGSTBaseAmount -= Double(appliedSmiles) * gstRatio
        countGSTBaseAmount -= Double(appliedSmiles) * gstRatio
        if newVATBaseAmount != 0{
            newVATBaseAmount -= Double(appliedSmiles) * vatRation
            countVATBaseAmount -= Double(appliedSmiles) * vatRation
        }
        
        if newGSTBaseAmount <= 0{
            _ = recountTheTax(isCountApplied: true)
            arrAppliedVoucher = [VoucherCouponsData]()
            return
        }
        
        _ = recountTheTax(isCountApplied: true)
        
        for voucher in arrAppliedVoucher {
            netPayableAmount -= voucher.denomination ?? 0.0
        }
        
        if netPayableAmount <= 0.0{
            netPayableAmount = 0
        }
        
        
    }
    
    func getSmilesDataCount() -> Double {
//        var taxes = 100.0
//        for tax in bookingTaxList{
//            taxes += tax.taxPercentage ?? 0
//        }
//        return ((100.0*netPayableAmount)/taxes).rounded(.up)
        
        
        var tax_per = 1.0
        
        var tax_gst = 0.0
        for list in bookingTaxList {
            tax_gst += list.taxPercentage ?? 0.0
        }
        
        tax_per += (tax_gst * gstRatio)/100
        
        if dataModel?.vatBaseAmount != 0{
            let vat_tax = ((dataModel?.vat_tax?.taxPercentage ?? 0) * vatRation)/100
            tax_per += vat_tax
            tax_per += (vat_tax * (dataModel?.getCessTax() ?? 0))/100
        }
        
        return (netPayableAmount / tax_per).rounded(.up)
        
    }
    
    func recountTheTax(isCountApplied: Bool) -> [AfterDiningData.BreakUp] {
        bookingTaxAmount = 0
        bookingDTaxAmount = 0
        var breakup = [AfterDiningData.BreakUp]()
        for i in 0..<bookingTaxList.count{
            bookingTaxList[i].taxAmount = ((bookingTaxList[i].taxPercentage ?? 0.0) * newGSTBaseAmount) / 100.0
            bookingTaxAmount += ((bookingTaxList[i].taxPercentage ?? 0.0) * countGSTBaseAmount) / 100.0
            bookingDTaxAmount += bookingTaxList[i].taxAmount ?? 0.0
            breakup.append(AfterDiningData.BreakUp(title: bookingTaxList[i].tax ?? "", value: String(format: "%.02f", bookingTaxList[i].taxAmount ?? 0)))
        }
        if newVATBaseAmount != 0{
            let tempVAT = ((dataModel?.vat_tax?.taxPercentage ?? 0) * newVATBaseAmount) / 100.0
            bookingVatTaxAmount = ((dataModel?.vat_tax?.taxPercentage ?? 0) * countVATBaseAmount) / 100.0
            bookingCESSTaxAmount = ((dataModel?.getCessTax() ?? 0) * bookingVatTaxAmount) / 100.0
            bookingDTaxAmount += tempVAT
            breakup.append(AfterDiningData.BreakUp(title: dataModel?.vat_tax?.tax ?? "", value: String(format: "%.02f", tempVAT)))
            if dataModel?.getCessTax() != 0{
                bookingDTaxAmount += ((dataModel?.getCessTax() ?? 0) * tempVAT) / 100.0
                breakup.append(AfterDiningData.BreakUp(title: dataModel?.cess_tax?.tax ?? "", value: String(format: "%.02f", ((dataModel?.getCessTax() ?? 0) * tempVAT) / 100.0)))
            }
        }else{
            bookingVatTaxAmount = 0
            bookingCESSTaxAmount = 0
        }
        
        if isCountApplied{
            netPayableAmount = countGSTBaseAmount + bookingTaxAmount + countVATBaseAmount + bookingVatTaxAmount + bookingCESSTaxAmount
        }

        return breakup
    }
    
//    func getSubtotalAmountBeforeTax() {
//        var taxPer: Double = 0.0
//        for tax in bookingTaxList{
//            taxPer += tax.taxPercentage ?? 0.0
//        }
//        payableSubtotalFromCalculation = ((payableSubtotalFromCalculation * 100) / (100.0 + taxPer))
//    }
    
    func recountTheTax(withTotal total: Double) {
        bookingTaxAmount = 0
        bookingDTaxAmount = 0
        var breakup = [AfterDiningData.BreakUp]()
        for i in 0..<bookingTaxList.count{
            bookingTaxList[i].taxAmount = ((bookingTaxList[i].taxPercentage ?? 0.0) * newGSTBaseAmount) / 100.0
            bookingTaxAmount += bookingTaxList[i].taxAmount ?? 0
            breakup.append(AfterDiningData.BreakUp(title: bookingTaxList[i].tax ?? "", value: String(format: "%.02f", bookingTaxList[i].taxAmount ?? 0)))
        }
        if newVATBaseAmount != 0{
            bookingVatTaxAmount = ((dataModel?.vat_tax?.taxPercentage ?? 0) * newVATBaseAmount) / 100.0
            bookingCESSTaxAmount = ((dataModel?.getCessTax() ?? 0) * bookingVatTaxAmount) / 100.0
        }else{
            bookingVatTaxAmount = 0
            bookingCESSTaxAmount = 0
        }
    }

    
    
    func formattedDiningDate(bookedDate: String) -> String {
        let bookingDate = self.stringToDate(dateString: bookedDate)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, dd MMM"
        let formattedString = dateFormatter.string(from: bookingDate)
        
        return formattedString
    }
    
    // MARK: Private Methods
   
    
    private func removeCards() {
        appliedSmiles = 0
        arrAppliedCoupons = [VoucherCouponsData]()
        arrAppliedVoucher = [VoucherCouponsData]()
    }
    
    
    func isCouponsEnabled(isCorpOffersAdded: Bool) -> Bool {
        if arrServerAppliedCoupons.count > 0{
            return false
        }
        return true
    }
    
    func isHappinessCardsEnabled() -> Bool {
        return true
    }
    
    func isCorpOffersEnabled(isCouponsAdded: Bool) -> Bool {
        return false
    }
    
    func isPointsEnabled() -> Bool {
        if appliedServerSmiles > 0{
            return false
        }
        return true
    }
    
    func isPaymentEnabled() -> Bool {
        return dataModel?.showPayment ?? false
    }

    
    private func twelveHourFormat(time: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        var date = dateFormatter.date(from: time)
        dateFormatter.dateFormat = "h:mm a"
        if let date_ref = date {
            let date12 = dateFormatter.string(from: date_ref)
            return date12
        }
        dateFormatter.dateFormat = "HH:mm:ss"
        date = dateFormatter.date(from: time)
        dateFormatter.dateFormat = "h:mm a"
        if let date_ref = date {
            let date12 = dateFormatter.string(from: date_ref)
            return date12
        }
        return ""
    }
    
    private func stringToDate(dateString: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        let date = dateFormatter.date(from: dateString)!
        return date
    }
    
    private func createJsonForFinalPayment(updatePaymentData: FinalPaymentUpdateBody) -> Json? {
        do {
            let encoded = try JSONEncoder().encode(updatePaymentData)
            let jsonString = String(data: encoded, encoding: .utf8)
            let data = jsonString?.data(using: .utf8)
            
            if let jsonArray = try JSONSerialization.jsonObject(with: data!, options : .allowFragments) as? Json {
                print(jsonArray) // use the json here
                return jsonArray
            }
        } catch let error as NSError {
            print(error)
        }
        return nil
    }
    
    
    
}
