//
 //  Created by Ajith CP on 12/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQAfterDiningControllerViewModel.swift
 //

import UIKit

class BBQAfterDiningControllerViewModel: BaseViewModel {
    
    internal var offerCards = [BBQOfferType : (Double, Bool)]()
    internal var offerType : BBQOfferType?
    
    var diningDataModel  : BBQDiningDataModel?
    var pointsValueModel : BBQPointsValueModel?
    var taxDataList : [Taxes]?

    
    
    var bookingSlotInformation : String = ""
    
    private var couponAmount : Double = 0.00
    private var corpOffersAmount : Double = 0.00
    private var voucherAmount : Double = 0.00
    private var pointsAmount : Double = 0.00
    
    var isAmountNil : Bool = false
    var isOrderModified : Bool = false
    
    var lastNetPayable  : Double = 0.00
    var bookingPayTotal : Double = 0.00 {
        didSet {
            if bookingPayTotal.rounded() <= 0.00 {
                isAmountNil = true
            } else {
                isAmountNil = false
            }
        }
    }
    
    var paymentRecieptURLString : String  {
        return self.diningDataModel?.billLink ?? ""
    }
    
    var bookingSubTotal : Double = 0.00
    
    var appliedCardAmount : Double = 0.00 {
        didSet {
            if self.offerCards.count < 1 {
                self.bookingPayTotal = self.bookingSubTotal + self.appliedCardAmount
            }
        }
    }
    
    var totalNumberOfPacks : Int?
    var appliedCouponList  : [VoucherCouponsData]?
    var appliedVoucherList : [VoucherCouponsData]?
    var appliedCorpOffersList : [VoucherCouponsData]?
    
    // MARK: API Methods
    
    func fetchAfterDiningPaymentData(for bookingID: String,
                                     completion: @escaping (_ isFetched: Bool)->()) {
        BBQBookingManager.afterDiningPaymentData(for: bookingID) { (diningData, error) in
            if error == nil {
                var objData = diningData
                objData?.netPayable = diningData?.netPayable == 0 ? diningData?.billTotal : diningData?.netPayable
                self.diningDataModel = objData
                self.setupDataSupplyModels(with: self.diningDataModel)
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    func afterDiningPaymentDataDetails(for bookingID: String,
                                     completion: @escaping (_ isFetched: Bool)->()) {
        BBQBookingManager.afterDiningPaymentDataDetails(for: bookingID) { (diningData, error) in
            if error == nil {
                var objData = diningData
                objData?.netPayable = diningData?.netPayable == 0 ? diningData?.billTotal : diningData?.netPayable
                self.diningDataModel = objData
                self.setupDataSupplyModels(with: self.diningDataModel)
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    func updateBooking(finalPaymentData: FinalPaymentUpdateBody, completion: @escaping (Bool)->()) {
        if let jsonData = self.createJsonForFinalPayment(updatePaymentData: finalPaymentData) {
            
            BBQBookingManager.updateFinalPayment(updatePayJsonData: jsonData) { (model, result) in
                if result {
                    completion(true)
                } else {
                    completion(false)
                }
            }
        }
        
        
    }
    
    // MARK: Public Methods
    func getAddedAmounts() -> [Double]{
        var arrAddedAmounts = [Double]()
        if let bookingDetails = diningDataModel?.bookingDetails{
            for pack in bookingDetails{
                for _ in 0..<(pack.packs ?? 0){
                    arrAddedAmounts.append(pack.price ?? 0.0)
                }
            }
        }
        return arrAddedAmounts
    }
    
    func getEstimatedTotal() -> String {
        let totalAmount = diningDataModel?.billWithoutTax ?? 0
//        for tax in diningDataModel?.taxes ?? [Taxes](){
//            totalAmount += tax.taxAmount ?? 0
//        }
        if totalAmount <= 0{
            return String(format: "%@%.0f", getCurrency(), diningDataModel?.billWithTaxTotal ?? getTotalWithTax())
        }
        return String(format: "%@%.0f", getCurrency(), totalAmount)
    }
    
    func getTotalWithTax() -> Double {
        var totalAmount = diningDataModel?.billWithoutTax ?? 0
        for tax in diningDataModel?.taxes ?? [Taxes](){
            totalAmount += tax.taxAmount ?? 0
        }
        return totalAmount > (diningDataModel?.netPayable ?? 0) ? totalAmount : (diningDataModel?.netPayable ?? 0)
    }
    
    func getPaymentAfterDiningAmountRowData() -> [AmountRowData] {
        
        var amountSplitList = [AmountRowData]()
        var amountItem =  AmountRowData()
        amountItem.amountTitle = kDiningSubTotal
        amountItem.amountValue =  String(format: "%.2f", self.diningDataModel?.billTotal ?? 0.00)
        amountSplitList.append(amountItem)
        return amountSplitList
    }
    
    func adjustTotalAmountForPayment(with adjustmentAmount: Double,
                                     isAddition: Bool,
                                     offerType: BBQOfferType) {
        self.offerType = offerType
        
        let formattedAdjustAmount = Double(String(format: "%.2f", adjustmentAmount)) ?? 0.00
        let amountForCalculation  = self.appliedOnTotalSubTotal(adjustedAmount: formattedAdjustAmount,
                                                                type: offerType)
        
        self.offerCards.updateValue((amountForCalculation, isAddition), forKey: offerType)
        
        if let appliedCouponAmount = self.offerCards[.Coupons]?.0,
            let isAdd = self.offerCards[.Coupons]?.1 {
            self.couponAmount = isAdd ? -appliedCouponAmount : appliedCouponAmount
        }
        
        if let appliedPointsAmount = self.offerCards[.Smiles]?.0,
            let isAdd = self.offerCards[.Smiles]?.1 {
            self.pointsAmount = isAdd ? -appliedPointsAmount : appliedPointsAmount
        }
        
        if let appliedCorpOffersAmount = self.offerCards[.CorporateOffers]?.0,
            let isAdd = self.offerCards[.CorporateOffers]?.1 {
            self.corpOffersAmount = isAdd ? -appliedCorpOffersAmount : appliedCorpOffersAmount
        }
        
        if let appliedVoucherAmount = self.offerCards[.Vouchers]?.0,
            let isAdd = self.offerCards[.Vouchers]?.1 {
            self.voucherAmount = isAdd ? -appliedVoucherAmount : appliedVoucherAmount
        }
        
        switch offerType {
        case .CorporateOffers:
            calculateBookingAmountSubTotal(with: corpOffersAmount)
        case .Coupons:
            calculateBookingAmountSubTotal(with: couponAmount)
        case .Vouchers:
            calculateFinalAmountForPay(with: voucherAmount)
        case .Smiles:
            calculateFinalAmountForPay(with: pointsAmount)
        }
        
        if !isAddition {
            self.removeCards(for: offerType)
        }
        
    }
    
    func consumedCouponAmount(denomination: Double, payAmount: Double) -> Double {
        if denomination > payAmount {
            return payAmount
        }
        return denomination
    }
    
    func resetBookingCardsApplication() {
        self.offerCards.removeAll()
        self.appliedCardAmount = 0.00
    }
    
    
    func getAppliedVouchers() -> (coupons: [VoucherCouponsData], vouchers: [VoucherCouponsData],
        corpOffers: [VoucherCouponsData]) {
            var appliedVouchers = [VoucherCouponsData]()
            var appliedCoupons  = [VoucherCouponsData]()
            var appliedCorpOffers = [VoucherCouponsData]()
            
            if let voucherDetails  = self.diningDataModel?.vouchers {
                for item in voucherDetails {
                    let appliedItem = VoucherCouponsData(appliedVouchers: item)
                    if item.voucherType == "GC" && item.isCorpVoucher == 0 { // Correct it from server, string to bool
                        appliedCoupons.append(appliedItem)
                    } else if item.voucherType == "GC" && item.isCorpVoucher == 1 {
                        appliedCorpOffers.append(appliedItem)
                    } else if item.voucherType == "GV" {
                        appliedVouchers.append(appliedItem)
                    }
                }
            }
            return (appliedCoupons, appliedVouchers, appliedCorpOffers)
    }
    
    func formattedDiningDate(bookedDate: String) -> String {
        let bookingDate = self.stringToDate(dateString: bookedDate)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, dd MMM"
        let formattedString = dateFormatter.string(from: bookingDate)
        
        return formattedString
    }
    
    // MARK: Private Methods
    
    private func appliedOnTotalSubTotal(adjustedAmount: Double, type: BBQOfferType) -> Double {
        if type == .Vouchers || type == .Smiles {
            return adjustedAmount
        }
        let adjustAmount = type == .Coupons ? couponAmount : corpOffersAmount
        if adjustedAmount >= self.bookingSubTotal - adjustAmount {
            return self.bookingSubTotal
        }
        return adjustedAmount
    }
    
    private func calculateBookingAmountSubTotal(with amount: Double) {
        bookingSubTotal += amount
        bookingPayTotal = bookingSubTotal
        self.balanceAdjustmentAmount()
    }
    
    private func calculateFinalAmountForPay(with amount: Double) {
        bookingPayTotal += amount
    }
    
    private func balanceAdjustmentAmount() {
        if offerType == .Coupons {
            bookingPayTotal +=  self.corpOffersAmount + self.pointsAmount + self.voucherAmount
        }
        if offerType == .CorporateOffers {
            bookingPayTotal +=  self.voucherAmount + self.pointsAmount + self.couponAmount
        }
    }
    
    private func removeCards(for offerType: BBQOfferType) {
        self.offerCards.remove(at: self.offerCards.index(forKey: offerType)!)
        self.couponAmount = offerType == .Coupons ? 0.00 : self.couponAmount
        self.voucherAmount = offerType == .Vouchers ? 0.00 : self.voucherAmount
        self.pointsAmount = offerType == .Smiles ? 0.00 : self.pointsAmount
        self.corpOffersAmount = offerType == .CorporateOffers ? 0.00 : self.corpOffersAmount
    }
    
    private func setupDataSupplyModels(with diningModel: BBQDiningDataModel?)  {
        self.taxDataList = diningModel?.taxes
        
        if let bookingDetails = diningModel?.bookingDetails {
            self.bookingSubTotal = diningModel?.billTotal ?? 0.00
            self.totalNumberOfPacks = self.getTotalPacks(from: bookingDetails)
            
            self.appliedCouponList = self.getAppliedVouchers().coupons
            self.appliedVoucherList = self.getAppliedVouchers().vouchers
            self.appliedCorpOffersList = self.getAppliedVouchers().corpOffers
        }
       
        if let payTotal = diningModel?.netPayable {
    
            if self.lastNetPayable != 0.00 && self.lastNetPayable != payTotal {
                self.isOrderModified = true
            } else {
                self.isOrderModified = false
            }
            self.lastNetPayable = payTotal
            self.bookingPayTotal = payTotal
        }
        
        if let bookingDate = diningModel?.reservationDate, let bookingTime = diningModel?.reservationTime {
            let dateString = self.formattedDiningDate(bookedDate: bookingDate)
            let timeString = self.twelveHourFormat(time: bookingTime)
            self.bookingSlotInformation = "\(dateString) , \(timeString)"
        }
        
        if let appliedPoints = diningModel?.loyaltyPointsAmount, appliedPoints != 0 {
            self.pointsValueModel = BBQPointsValueModel(redeemedAmount: appliedPoints,
                                                        redeemedPoints: diningModel?.loyaltyPoints)
        }
        
    }
    
    private func getBookingSubTotalAmount(from bookingDetails: [RescheduleDetails]) -> Double {
        
        var subTotal = 0.00
        for buffetItem in bookingDetails {
            subTotal += buffetItem.totalPrice ?? 0.00
        }
        return subTotal
    }
    
    private func getTotalPacks(from bookingDetails: [RescheduleDetails]) -> Int {
        var totalPacks = 0
        for buffetItem in bookingDetails {
            totalPacks += buffetItem.packs ?? 0
        }
        return totalPacks
    }
    
    private func getTaxAmountValue(with taxPercentage: Double) -> Double {
        return self.bookingSubTotal * (taxPercentage/100)
    }
    
    func isCouponsEnabled(isCorpOffersAdded: Bool) -> Bool {
        if (offerCards.keys.contains(.Coupons) || !isAmountNil) && !isCorpOffersAdded {
            return true
        }
        return false
    }
    
    func isHappinessCardsEnabled() -> Bool {
        if offerCards.keys.contains(.Vouchers) || !isAmountNil {
            return true
        }
        return false
    }
    
    func isCorpOffersEnabled(isCouponsAdded: Bool) -> Bool {
        if (offerCards.keys.contains(.CorporateOffers) || !isAmountNil) && !isCouponsAdded {
            return true
        }
        return false
    }
    
    func isPointsEnabled() -> Bool {
        if offerCards.keys.contains(.Smiles) || !isAmountNil {
            return true
        }
        return false
    }
    
    func isPaymentEnabled() -> Bool {
        return diningDataModel?.showPayment ?? false
    }

    
    private func twelveHourFormat(time: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        var date = dateFormatter.date(from: time)
        dateFormatter.dateFormat = "h:mm a"
        if let date_ref = date {
            let date12 = dateFormatter.string(from: date_ref)
            return date12
        }
        dateFormatter.dateFormat = "HH:mm:ss"
        date = dateFormatter.date(from: time)
        dateFormatter.dateFormat = "h:mm a"
        if let date_ref = date {
            let date12 = dateFormatter.string(from: date_ref)
            return date12
        }
        return ""
    }
    
    private func stringToDate(dateString: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        let date = dateFormatter.date(from: dateString)!
        return date
    }
    
    private func createJsonForFinalPayment(updatePaymentData: FinalPaymentUpdateBody) -> Json? {
        do {
            let encoded = try JSONEncoder().encode(updatePaymentData)
            let jsonString = String(data: encoded, encoding: .utf8)
            let data = jsonString?.data(using: .utf8)
            
            if let jsonArray = try JSONSerialization.jsonObject(with: data!, options : .allowFragments) as? Json {
                print(jsonArray) // use the json here
                return jsonArray
            }
        } catch let error as NSError {
            print(error)
        }
        return nil
    }
    
    
    
}
