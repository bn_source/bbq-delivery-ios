//
//  HamMenuModel.swift
//  Barbeque Nation
//
//  Created by Abhijit on 07/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import Foundation

enum  HamburgerMenu: Int, CaseIterable {
    
    case home_Menu_Model = 0
   // case myTransactions_Menu_Model
    case myReservations_Menu_Model
    case myHappinessCard_Menu_Model
    case delivery_History_Model
    case promotionsandoffers_menu_model
    case profile_Menu_Model
    case manage_address_Model
    case inviteFriends

    case notifications_Menu_Model
    case outletInfo_Menu_Model
    case HelpAndSupport_Menu_Model
    case about_Menu_Model
    case feedback_Menu_Model
    case logout_Menu_Model
   
    
    static var caseCount: Int { return self.allCases.count }
    
    static var mainCaseCount: Int { return 8 }
    static var otherCaseCount: Int { return 6 }
    func getIndexPath() -> IndexPath {
        return IndexPath(row: self.rawValue > (HamburgerMenu.mainCaseCount - 1) ? self.rawValue - HamburgerMenu.mainCaseCount : self.rawValue, section: self.rawValue > (HamburgerMenu.mainCaseCount - 1) ? 1 : 0)
    }
}


class HamMenuViewModel {
    var count: Int {
        return HamburgerMenu.caseCount
    }
    
    func countForSection(_ section :Int) -> Int {
        
        if section == 0 {
            if isAppReferAndEarnActivated == false{
                return HamburgerMenu.mainCaseCount - 1
            }
            return HamburgerMenu.mainCaseCount

        } else if section == 1 {
            
            return HamburgerMenu.otherCaseCount

        }
        return 0
        
    }
    
    //TODO: Abhijit Move String to common string file
    func getMenuItemFor(indexPath: IndexPath) -> String {
        
        let rowIndex = indexPath.section > 0 ? indexPath.row + HamburgerMenu.mainCaseCount  : indexPath.row
            switch rowIndex {
            case HamburgerMenu.home_Menu_Model.rawValue:
                return kHamMenuHomeTitle
                
            case HamburgerMenu.myReservations_Menu_Model.rawValue:
                return kHamMenuReservationTitle
                
            case HamburgerMenu.myHappinessCard_Menu_Model.rawValue:
                return kHamMenuHappinessCardTitle
                
                
                
            case HamburgerMenu.promotionsandoffers_menu_model.rawValue:
                
                //            var userTitle = ""
                //            if let firstName = BBQUserDefaults.sharedInstance.UserName.components(separatedBy: " ").first {
                //                let name = String(firstName.filter { !" \n".contains($0) })
                //                userTitle = "\(kHamMenuPromotionsAndOffersTitleUser) \(name)"
                //            }
                //            return BBQUserDefaults.sharedInstance.isGuestUser ? kHamMenuPromotionsAndOffersTitle : userTitle
                return kHamMenuPromotionsAndOffersTitle
                
                
            case HamburgerMenu.profile_Menu_Model.rawValue:
                return kHamMenuProfileTitle
                
                
            case HamburgerMenu.delivery_History_Model.rawValue:
                return kDeliveryHistory
                
            case HamburgerMenu.manage_address_Model.rawValue:
                return kManageAddresses
                
                
            case HamburgerMenu.inviteFriends.rawValue:
                return kHamMenuInviteFriendsTitle
                
                
            case HamburgerMenu.notifications_Menu_Model.rawValue:
                return kHamMenuNotificationsTitle
                
            case HamburgerMenu.outletInfo_Menu_Model.rawValue:
                return kHamMenuOutletInfo
                
                
            case HamburgerMenu.HelpAndSupport_Menu_Model.rawValue:
                return kHamMenuHelpAndSupportTitle
                
            case HamburgerMenu.about_Menu_Model.rawValue:
                return kHamMenuAboutTitle
              
            case HamburgerMenu.feedback_Menu_Model.rawValue:
                return kHamMenuFeedbackTitle
                
            case HamburgerMenu.logout_Menu_Model.rawValue:
                if BBQUserDefaults.sharedInstance.isGuestUser {
                    return kHamMenuLoginTitle
                } else {
                    return kHamMenuLogoutTitle
                }
                
                
            default:
                return ""
            }
    }
    
   // kHamMenuPromotionsAndOffer
 
    
   
    
    
    
    
    func getMenuImageFor(indexPath: IndexPath) -> String {
        
        let rowIndex = indexPath.section > 0 ? indexPath.row + HamburgerMenu.mainCaseCount  : indexPath.row
        switch rowIndex {
        case HamburgerMenu.home_Menu_Model.rawValue:
            return kHamMenuHomeImage

           
        case HamburgerMenu.myReservations_Menu_Model.rawValue:
           return kHamMenuReservationImage
            
        case HamburgerMenu.myHappinessCard_Menu_Model.rawValue:
           return kHamMenuHappinessCardImage
   
        case HamburgerMenu.feedback_Menu_Model.rawValue:
             return kHamMenuOutletInfoImage
            
        case HamburgerMenu.promotionsandoffers_menu_model.rawValue:
            
//            var userTitle = ""
//            if let firstName = BBQUserDefaults.sharedInstance.UserName.components(separatedBy: " ").first {
//                let name = String(firstName.filter { !" \n".contains($0) })
//                userTitle = "\(kHamMenuPromotionsAndOffersTitleUser) \(name)"
//            }
//            return BBQUserDefaults.sharedInstance.isGuestUser ? kHamMenuPromotionsAndOffersTitle : userTitle
            return kHamMenuSpeciallyforUImage
            
    
            
        case HamburgerMenu.profile_Menu_Model.rawValue:
            return kHamMenuProfileImage
     
        case HamburgerMenu.delivery_History_Model.rawValue:
            return kHamMenuMyOrderImage
            
        case HamburgerMenu.manage_address_Model.rawValue:
            return kManageAddressesImage
            
            
            case HamburgerMenu.outletInfo_Menu_Model.rawValue:
                return kHamMenuOutletInfoImage
                
            case HamburgerMenu.notifications_Menu_Model.rawValue:
                return kHamMenuNotificationsImage
                
            case HamburgerMenu.HelpAndSupport_Menu_Model.rawValue:
                return kHamMenuHelpAndSupportImage
                
            case HamburgerMenu.about_Menu_Model.rawValue:
                return kHamMenuAboutImage
                
            case HamburgerMenu.logout_Menu_Model.rawValue:
                if BBQUserDefaults.sharedInstance.isGuestUser {
                    return kHamMenuLogoutImage
                } else {
                    return kHamMenuLogoutImage
                }
                
             case HamburgerMenu.inviteFriends.rawValue :
                 return kHamMenuInviteFriendsImage
            default:
                return ""
            }
        }
    

    func logoutUser(completion: @escaping (String?, Bool) -> ()) {
        BBQLoginManager.logoutUser() { (model, result) in
            if result! {
                completion(model?.logoutMessage, true)
                //AnalyticsHelper.shared.resetMoEngageuser()
            } else {
                completion(nil, false)
            }
        }
    }
}

