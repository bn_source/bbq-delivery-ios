//
//  Created by Bhamidipati Kishore on 23/09/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQLoyalityPointsViewModel.swift
//

import UIKit

class BBQLoyalityPointsViewModel: BaseViewModel {
    private var loyalityModel: BBQLoyalityPointsDataModel? = nil
    
    var getLoyalityModel: BBQLoyalityPointsDataModel? {
        return loyalityModel
    }
}

//MARK: Service Call Handler
extension BBQLoyalityPointsViewModel {
    
    func getLoyalityPoints(pageNumber:Int,pageSize:Int, sort:String,completion: @escaping (Bool)->()) {
        BBQLoyalityPointsManager.getLoyalityPoints(pageNumber: pageNumber, pageSize: pageSize, sort: sort) { (model, result) in
            if let resultValue = result {
                //Assign to model
                if resultValue {
                    self.loyalityModel = model
                    completion(true)
                }else{
                    completion(false)
                }
            }
        }
    }
    
    public func transactionIDDescription(for index: Int) -> String {
        var transactionIdString = ""
        if let transaction = self.loyalityModel?.loyaltyTransactions?[index] {
            if transaction.eventType?.contains("VOUCHER") ?? false {
                transactionIdString = "Transaction ID: \(transaction.transactionID ?? "")"
            } else if transaction.eventType?.contains("BOOKING") ?? false {
                transactionIdString = "Booking ID: \(transaction.transactionID ?? "")"
            }
        }
        return transactionIdString
    }
    
    // MARK: Format Methods
    
    func expiryDateString(for expiryStamp: Int64, transactionType: String) -> String {
        if transactionType == "CREDIT" {
            let date = Date(timeIntervalSince1970: Double(expiryStamp))
            let dateFormatter = DateFormatter()
            dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
            dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
            dateFormatter.timeZone = .current
            let formattedString = formattedDateString(date: date)
            return formattedString
        }
        return ""
    }
    
    private func formattedDateString(date: Date) -> String {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "dd/MMM/yyyy"
        var formattedDate = dateformatter.string(from: date)
        formattedDate = "Expiry: \(formattedDate)"
        return formattedDate
    }
    
    public func getExpiredPointsString() -> String {
        let pointsEarned = self.loyalityModel?.pointsEarned ?? 0
        let pointsSpent = self.loyalityModel?.pointsSpent ?? 0
        let pointsAvail = self.loyalityModel?.availablePoints ?? 0
        let expiredSmiles  = pointsEarned - pointsSpent - pointsAvail
        return "\(expiredSmiles)"
    }
    public func getExpiredPointsCount() -> String {
        let pointsEarned = self.loyalityModel?.pointsEarned ?? 0
        let pointsSpent = self.loyalityModel?.pointsSpent ?? 0
        let pointsAvail = self.loyalityModel?.availablePoints ?? 0
        let expiredSmiles  = pointsEarned - pointsSpent - pointsAvail
        return "\(expiredSmiles)"
    }
}
