/*
 *  Created by Nischitha on 23/09/19.
 *  Copyright © BARBEQUE NATION HOSPITALITY LIMITED 2019. All rights reserved.
 
 * CHANGE LOG
 * —------------------------------------------------------------------------------
 * ID          Bug ID      Author Name          Date                  Description
 * —------------------------------------------------------------------------------
 * 1            NA         Nischitha         23/09/19              Initial Version
 */

import Foundation
class WelcomeViewModel: BaseViewModel {
    
    //MARK:- Properties
    var welcomeModel : WelcomeModel?
    
    //MARK:- Functions
    func getWelcomeInfo(completion: @escaping (Bool) -> ())  {
        BBQHelpSupportManager.getWelcomeInfo(completion: { (data,error) in
            if error == false
            {
                self.welcomeModel = data
                completion(true)
            }else{
                completion(false)
            }
        })
    }
}
