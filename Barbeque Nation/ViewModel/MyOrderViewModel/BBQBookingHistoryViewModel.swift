//
//  Created by Nischitha on 06/09/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQBookingHistoryViewModel.swift
//

import Foundation
class BookingHistoryViewModel : BaseViewModel {
    //MARK:- Properties
    private var bookinghistoryDataModel: BookingHistoryModel?
    private var bookingHistoryData : [BookingHistory]?
    private var branchId: String = ""
    private var totalBookingItems: Int = 0
    
    var getBookingHistoryDetailsData: [BookingHistory]? {
        return bookingHistoryData
    }
    
    var getTotalBookingItems: Int? {
        return totalBookingItems
    }
    
}

extension BookingHistoryViewModel
{
    func getBookingHistory(status: Int, pageNo: String, completion: @escaping (Bool) -> ()) {
        BBQBookingManager.getBookingHistory(status: status, pageNo: pageNo){ (data, error) in
            if error == nil {
                do {
                    let decoder = JSONDecoder()
                    guard let responseData = data else {
                        DispatchQueue.main.async {
                            completion(false)
                        }
                        return
                    }
                    self.bookinghistoryDataModel = try decoder.decode(BookingHistoryModel.self, from: responseData)
                    self.bookingHistoryData = self.bookinghistoryDataModel?.bookingsHistory
                    self.totalBookingItems = self.bookinghistoryDataModel?.totalElements ?? 0
                    
                    
                    print(self.bookinghistoryDataModel!)
                    
                    DispatchQueue.main.async {
                        completion(true)
                    }
                } catch {
                    DispatchQueue.main.async {
                        completion(false)
                    }
                }
            } else {
                if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse?.statusCode == 400 {
                    BBQUserManager.configureErrorModel(data: data)
                }
                DispatchQueue.main.async {
                    completion(false)
                }
            }
        }
        
        
    }
    func setTotalBookingItems(count:Int){
        totalBookingItems = totalBookingItems - count
    }
}
