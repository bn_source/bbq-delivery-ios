//
//  Created by Nischitha on 16/09/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQVoucherHistoryViewModel.swift
//

import Foundation
class BBQVoucherHistoryViewModel {
    
    //MARK:- Properties
    private var vouchorHistoryModel : VoucherHistoryModel?
    private var vouchorHistoryData : [Vouchers]?
    private var totalVoucherItems: Int?
    private var termsAndConditions: [String]?
    var getTermsAndConditions: [String]? {
        return termsAndConditions
    }
    var getVoucherHistoryDetailsData: [Vouchers]? {
        return vouchorHistoryData
    }
    var getTotalVoucherItems: Int? {
        return totalVoucherItems
    }
    //MARK:- Functions
    func getVoucherHistoryDetails(pageNo: String, completion: @escaping (Bool) -> ()) {
        BBQBookingManager.getVoucherHistory(pageNo: pageNo, completion: { (data, error) in
            if error == false{
                self.vouchorHistoryModel = data
                self.vouchorHistoryData = self.vouchorHistoryModel?.vouchers
                self.totalVoucherItems = Int(self.vouchorHistoryModel?.totalElements ?? "0")
                self.termsAndConditions = self.vouchorHistoryModel?.termsAndConditions
                completion(true)
            }else{
                completion(false)
            }
        })
    }
}
