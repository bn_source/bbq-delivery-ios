//
//  Created by Rabindra L on 30/08/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQCancellationViewModel.swift
//

import Foundation

class CancellationViewModel:BaseViewModel {
    
    enum BBQRefundType : String {
        case cash = "CASH"
        case points = "POINTS"
    }
    
    private var cancellationDataModel: BookingCancellationModel?
    
    var cancelReasonModel: [BBQCancelReasonsModel]?
    var refundOptionModel : BBQRefundDataModel?
    var refundTypeList = [(String, String, String)]()
   
    // MARK: API Methods
    
    func cancelBooking(bookingID: String, reasonID:Int,
                       refundType:String, completion: @escaping (Bool) -> ()) {
        guard  bookingID.count > 0 ,
            reasonID >= 0,
            refundType.count >= 0
            else {
            print("Incorrect values passed while cancellation")
            return
        }
        BBQBookingManager.cancelBooking(bookingId: bookingID,
                                        reasonID: reasonID,
                                        refundType: refundType){
                                            (data, error) in
            if error == nil {
                do {
                    AnalyticsHelper.shared.triggerEvent(type: .booking_refund_success_value)
                    let decoder = JSONDecoder()
                    guard let responseData = data else {
                        DispatchQueue.main.async {
                            completion(false)
                        }
                        return
                    }
                    
                    self.cancellationDataModel =
                        try decoder.decode(BookingCancellationModel.self,
                                           from: responseData)
                    DispatchQueue.main.async {
                        completion(true)
                    }
                } catch {
                    DispatchQueue.main.async {
                        completion(false)
                    }
                }
            } else {
                AnalyticsHelper.shared.triggerEvent(type: .booking_refund_failure_value)
                if BBQUserManager.networkResponse != nil,
                    BBQUserManager.networkResponse?.statusCode == 400 {
                    BBQUserManager.configureErrorModel(data: data)
                }
                DispatchQueue.main.async {
                    completion(false)
                }
            }
        }
    }
    
    func getCancelReason(completion: @escaping (Bool) -> ()) {
        BBQBookingManager.getCancelReaosns{ (data, error) in
            if error == nil {
                do {
                    let decoder = JSONDecoder()
                    guard let responseData = data else {
                        DispatchQueue.main.async {
                            completion(false)
                        }
                        return
                    }
                    self.cancelReasonModel = try decoder.decode([BBQCancelReasonsModel].self, from: responseData)
                    
                    DispatchQueue.main.async {
                        completion(true)
                    }
                } catch {
                    DispatchQueue.main.async {
                        completion(false)
                    }
                }
            } else {
                if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse?.statusCode == 400 {
                    BBQUserManager.configureErrorModel(data: data)
                }
                DispatchQueue.main.async {
                    completion(false)
                }
            }
        }
    }
    
    func fetchRefundOptions(bookingID: String, completion: @escaping (Bool) -> ()) {
        BBQBookingManager.refundOptions(bookingID: bookingID) { (dataModel, error) in
            if error == nil {
                self.refundOptionModel = dataModel
                
                if let refundPoints = self.refundOptionModel?.loyaltyPointsRefund?.loyaltyPoints,
                    refundPoints > 0 {
                    
                    self.refundTypeList.append((kMsgBBQPoints,
                                                kMsgReedemNextVisit,
                                                String(refundPoints)))
                }
                
                if let refundCash = self.refundOptionModel?.cashRefund?.refundAmount,  refundCash > 0.00 {
                    let refundPercentageText = self.cashDeductionPercentage()
                    self.refundTypeList.append((kMsgOriginModePayment,
                                                refundPercentageText,
                                                String(format: "%.2f", refundCash)))
                }
                
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    // MARK: Bussiness Logic Methods
    
    private func cashDeductionPercentage() -> String {
        if let refundPercentage = self.refundOptionModel?.cashRefund?.cashRefundPercentage {
            let deduction = 100 - refundPercentage
            let completeText = "*\(deduction)% \(kMsgDeductionCharge)"
            return completeText
        }
        return ""
    }
    
    func showRefundMode(isReasonSelected: Bool) -> Bool {
        if self.refundTypeList.count > 0 && isReasonSelected {
            return true
        }
        return false
    }
    
    func refundMode(for index: Int) -> BBQRefundType {
        if refundTypeList.count > index, refundTypeList[index].0 == kMsgBBQPoints {
            return .points
        }
        return .cash
    }
    
}


