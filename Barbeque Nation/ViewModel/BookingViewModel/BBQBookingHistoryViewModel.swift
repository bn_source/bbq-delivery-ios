//
 //  Created by Nischitha on 06/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQBookingHistoryViewModel.swift
 //

import Foundation
class BookingHistoryViewModel : BaseViewModel {
    //MARK:- Properties
    private var bookinghistoryDataModel: BookingHistoryModel?
    private var upcomingReservationDetailsData : [UpcomingReservationDetails]?
    private var branchId: String = ""
    
    var getupcomingReservationDetailsData: [UpcomingReservationDetails]? {
        return upcomingReservationDetailsData
    }
}

extension BookingHistoryViewModel
{
    func getBookingHistory(completion: @escaping (Bool) -> ()) {
        BBQBookingManager.getBookingHistory{ (data, error) in
            if error == nil {
                do {
                    let decoder = JSONDecoder()
                    guard let responseData = data else {
                        DispatchQueue.main.async {
                            completion(false)
                        }
                        return
                    }
                    self.bookinghistoryDataModel = try decoder.decode(BookingHistoryModel.self, from: responseData)
                    self.upcomingReservationDetailsData = self.bookinghistoryDataModel?.upcomingReservation
                    
                    print(self.bookinghistoryDataModel!)
                    
                    //self.reservationData = self.upcomingReservationDataModel?.upcoming_reservations
                    DispatchQueue.main.async {
                        completion(true)
                    }
                } catch {
                    DispatchQueue.main.async {
                        completion(false)
                    }
                }
            } else {
                if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse?.statusCode == 400 {
                    BBQUserManager.configureErrorModel(data: data)
                }
                DispatchQueue.main.async {
                    completion(false)
                }
            }
        }
        
        
    }
}
