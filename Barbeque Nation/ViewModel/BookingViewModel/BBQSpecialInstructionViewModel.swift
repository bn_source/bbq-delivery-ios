//
//  Created by Maya R on 14/09/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQSpecialInstructionViewModel.swift
//

import Foundation
import Alamofire

class BBQSpecialInstructionViewModel :BaseViewModel{
    private var instructionDataModel: SpecialInsructionsModel?
    private var celebrations : [CelebrationModel]?
    private var joinedBy : [JoinedByModel]?
    
    func getCelebrations()->[CelebrationModel]{
        var celebarray = [CelebrationModel]()
        if(celebrations != nil){
            celebarray = celebrations!
        }
        return celebarray
    }
    
    func getJoinedBy()->[JoinedByModel]{
        var joinArray = [JoinedByModel]()
        if(joinedBy != nil){
            joinArray = joinedBy!
        }
        return joinArray
    }
}
extension BBQSpecialInstructionViewModel{
    func getSpecialInstructionValues( completion: @escaping (Bool)-> ()){
        BBQBookingManager.getSpecialInstruction { (data, error) in
            if error == nil {
                do {
                    let decoder = JSONDecoder()
                    guard let responseData = data else {
                        DispatchQueue.main.async {
                            completion(false)
                        }
                        return
                    }
                    self.instructionDataModel = try decoder.decode(SpecialInsructionsModel.self, from: responseData)
                    
                    self.celebrations = self.instructionDataModel?.celebratingDetails
                    self.joinedBy = self.instructionDataModel?.joinedByDetails
                    
                    DispatchQueue.main.async {
                        completion(true)
                    }
                } catch {
                    DispatchQueue.main.async {
                        completion(false)
                    }
                }
            } else {
                if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse?.statusCode == 400 {
                    BBQUserManager.configureErrorModel(data: data)
                }
                DispatchQueue.main.async {
                    completion(false)
                }
            }
        }
    }
    
    func updateSpecialInstructions(bokingId:String,celebrationIds:[String],joinedIds:[String],instructiontext:String, completion: @escaping (Bool)-> ()){
        BBQBookingManager.updateSpecialInstruction(bookingId: bokingId, occassionIds: celebrationIds, joinedBy: joinedIds, specialNote: instructiontext){ (data, error) in
            if error == nil {
                DispatchQueue.main.async {
                    completion(true)
                }
                
            } else {
                if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse!.statusCode >= 300 {
                    BBQUserManager.configureErrorModel(data: data)
                }
                DispatchQueue.main.async {
                    completion(false)
                }
            }
        }
    }
    
    
}
