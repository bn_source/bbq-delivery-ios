//
 //  Created by Sridhar on 25/08/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQBookingViewModel.swift
 //

import UIKit


//enum PaymentType: String, CaseIterable {
//    case minimum, full // other; removing other payment option for now.
//
//    static var caseCount: Int { return self.allCases.count }
//
//    var description: String {
//        get {
//            switch(self) {
//            case .minimum:
//                return kMinimumPaySectionTitle
//            case .full:
//                return kFullPaySectionTitle
//            }
//        }
//    }
//}

class BookingViewModel: BaseViewModel {
    
    private var bookingslotswithBranchDataModel: BookingSlotsWithBranchDataModel?
    private var menubranchbuffetDataModel: MenuBranchBuffetDataModel?
    private var createBookingDataModel: CreateBookingDataModel?
    private var bookingupdateDataModel: UpdateBookingDataModel?
    
     var vouchersAndCouponsModel: VoucherCouponsModel?
    
    //MARK: - Get Slots for booking starts
    private var branchId: String = ""
    
    var branch_id: String {
        set {
            self.branchId = newValue
        } get {
            return self.branchId
        }
    }
    
    private var branchIdForAPI: Int = 0
    
    var branch_idForAPI: Int {
        set {
            self.branchIdForAPI = newValue
        } get {
            return self.branchIdForAPI
        }
    }
    
    private var branchIdStringForAPI: String = ""
    
    var branch_idStringForAPI: String {
        set {
            self.branchIdStringForAPI = newValue
        } get {
            return self.branchIdStringForAPI
        }
    }
    
    private var branchName: String = ""
    
    var branch_name: String {
        set {
            self.branchName = newValue
        } get {
            return self.branchName
        }
    }
    
    private var pickaDate: String = ""
    
    var pickaDate_Value: String {
        set {
            self.pickaDate = newValue
        } get {
            return self.pickaDate
        }
    }
    
    private var pickaDateForAPI: String = ""
    
    var pickaDate_ValueForAPI: String {
        set {
            self.pickaDateForAPI = newValue
        } get {
            return self.pickaDateForAPI
        }
    }
    
    /*private var pickaDateAPI: Date = Date
    
    var pickaDate_API: Date {
        set {
            self.pickaDate_API = newValue
        } get {
            return self.pickaDate_API
        }
    }*/
    
    private var bbqPointsData: Int64 = 0
    
    var bbqPointsValue: Int64 {
        set {
            self.bbqPointsData = newValue
        } get {
            return self.bbqPointsData
        }
    }
    
    private var slotavailableData : [SlotsAvailable]?
    
    func getSlotsAvailableData()->[SlotsAvailable]{
        let slotsArray :[SlotsAvailable] = slotavailableData!
        for slotDict in slotsArray {
            var slotModel = SlotsAvailable()
            
            slotModel.slotId = slotDict.slotId
            slotModel.slotDesc = slotDict.slotDesc
            slotModel.diningType = slotDict.diningType
            slotModel.slotStartTime = slotDict.slotStartTime
            slotModel.slotStopTime = slotDict.slotStopTime
            slotModel.totalCapacity = slotDict.totalCapacity
            slotModel.availableCapacity = slotDict.availableCapacity
        }
        return slotsArray
    }
    
    private var amenitiesData : [Amenities]?
    
    func getAmenitiesData()->[Amenities]{
        let amenitiesArray : [Amenities] = amenitiesData!
        for amenitiesDict in amenitiesArray {
            var amenitiesModel = Amenities()
            
            amenitiesModel.name = amenitiesDict.name
            amenitiesModel.description = amenitiesDict.description
            amenitiesModel.image = amenitiesDict.image
        }
        return amenitiesArray
    }
    
    func getNearByBranchesData()->[NearByBranches] {
        let nearbybranchesArray : [NearByBranches] = []
        for nearbybranchesDict in nearbybranchesArray {
            var nearbybranchesModel = NearByBranches()
            
            nearbybranchesModel.branchId = nearbybranchesDict.branchId
            nearbybranchesModel.branchName = nearbybranchesDict.branchName
            nearbybranchesModel.distance = nearbybranchesDict.distance
        }
        return nearbybranchesArray
    }
    
    /*private var taxDetailsData : TaxDetails?
    
    var cgst: Double? {
        return taxDetailsData?.cgst ?? 0.0
    }
    var sgst: Double? {
        return taxDetailsData?.sgst ?? 0.0
    }
    var servicecharge: Double? {
        return taxDetailsData?.servicecharge ?? 0.0
    }*/
    private var taxesData : [Taxes]?
    
    func getTaxesData()->[Taxes]{
        let taxesArray : [Taxes] = taxesData!
        for taxesDict in taxesArray {
            var taxesModel = Taxes()
            taxesModel.tax = taxesDict.tax
            taxesModel.taxPercentage = taxesDict.taxPercentage
        }
        return taxesArray
    }
    
    private var servicechargeData: Double = 0.0
    
    var serviceChargeValue: Double {
        set {
            self.servicechargeData = newValue
        } get {
            return self.servicechargeData
        }
    }
    
    //MARK: - Get Slots for booking ends
    
    
    //MARK: - Date Formatter to display Starts
    
    
    func getSelectedDateForAPI() -> String
    {
        let selectDate = self.pickaDateForAPI
        
        let separatedString = selectDate.components(separatedBy: "-")
        let dateStringValue = separatedString[0]+"-"+separatedString[1]+"-"+separatedString[2]
        
        /*let dateformatter = DateFormatter()
        let selectedDate = dateformatter.date(from: selectDate)!
        dateformatter.dateFormat = "yyyy-MM-dd"
        let currentselectedDate = dateformatter.string(from: selectedDate.self)
        //let currentselectedDate = dateformatter.string(from: selectDate.self)
        return currentselectedDate*/
        
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let date = dateFormatter.date(from: dateStringValue)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let changedDate = dateFormatter.string(from: date!)
        
        return changedDate
    }
    
    func getConfirmationDate() -> String
    {
        var confirmDate:Date?
        if todayPressedFlag == true
        {
            confirmDate = Date()
        }
        else if tomorrowPressedFlag == true
        {
            confirmDate = Date()
            let tomorrow = confirmDate?.tomorrow
            
            confirmDate = tomorrow
            
        }
        else if selectadatePressedFlag == true
        {
            /*let selectDate = self.pickaDate
            let dateformatter = DateFormatter()
            confirmDate = dateformatter.date(from: selectDate)!*/
            
            let selectDate = self.pickaDateForAPI
            
            let separatedString = selectDate.components(separatedBy: "-")
            let dateStringValue = separatedString[0]+"-"+separatedString[1]+"-"+separatedString[2]
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            
            confirmDate = dateFormatter.date(from: dateStringValue)
            //dateFormatter.dateFormat = "yyyy-MM-dd"
        }
        //let confirmDate = Date()
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "EEEE, dd MMM,"
        let currentselectedDate = dateformatter.string(from: confirmDate!.self)
        return currentselectedDate
    }
    //MARK: - Date Formatter to display Ends
    
    //MARK: - Bool Starts
    private var todayPressed: Bool = false
    
    var todayPressedFlag: Bool {
        set {
            self.todayPressed = newValue
        } get {
            return self.todayPressed
        }
    }
    
    private var tomorrowPressed: Bool = false
    
    var tomorrowPressedFlag: Bool {
        set {
            self.tomorrowPressed = newValue
        } get {
            return self.tomorrowPressed
        }
    }
    
    private var selectadatePressed: Bool = false
    
    var selectadatePressedFlag: Bool {
        set {
            self.selectadatePressed = newValue
        } get {
            return self.selectadatePressed
        }
    }
    
    private var selectadateFromHomeorBooking: Bool = false
    
    var selectadateFromHomeorBookingFlag: Bool {
        set {
            self.selectadateFromHomeorBooking = newValue
        } get {
            return self.selectadateFromHomeorBooking
        }
    }
    //MARK: - Bool Ends
    
    
    // MARK: - Menu Branch Buffet starts
    private var menuId: String = ""
    
    var menu_id: String {
        set {
            self.menuId = newValue
        } get {
            return self.menuId
        }
    }
    
    private var menuName: String = ""
    
    var menu_Name: String {
        set {
            self.menuName = newValue
        } get {
            return self.menuName
        }
    }
    
    private var currencyType: String = ""
    
    var currency_Type: String {
        set {
            self.currencyType = newValue
        } get {
            return self.currencyType
        }
    }
    
    private var buffetsInfo : Buffets?
    func getbuffetsInfo()-> Buffets{
        let buffetsArray : Buffets = buffetsInfo!
        var buffetsModel = Buffets()
        buffetsModel.buffetTitle = buffetsArray.buffetTitle
        buffetsModel.buffetDescription = buffetsArray.buffetDescription
        buffetsModel.buffetData = buffetsArray.buffetData
        return buffetsArray
    }
    
    private var buffetdataInfo : [BuffetData]?
    func getbuffetdataInfo()->[BuffetData]{
        let buffetsdataArray = [BuffetData]()
        guard buffetdataInfo != nil else {
            return buffetsdataArray
        }
        let buffetdataArray :[BuffetData] = buffetdataInfo!
        for buffetdataDict in buffetdataArray {
            var buffetdataModel = BuffetData()
            buffetdataModel.buffetId = buffetdataDict.buffetId
            buffetdataModel.buffetName = buffetdataDict.buffetName
            buffetdataModel.buffetDescription = buffetdataDict.buffetDescription
            buffetdataModel.buffetPrice = buffetdataDict.buffetPrice
            buffetdataModel.buffetCurrency = buffetdataDict.buffetCurrency
            buffetdataModel.buffetType = buffetdataDict.buffetType
            buffetdataModel.buffetImage = buffetdataDict.buffetImage
            buffetdataModel.buffetItems = buffetdataDict.buffetItems
        }
        return buffetdataArray
    }
    
    private var buffetitemsData : [BuffetItems]?
    
    func getbuffetitemsData()->[BuffetItems]{
        let buffetsItemsArray = [BuffetItems]()
        guard buffetitemsData != nil else {
            return buffetsItemsArray
        }
        let buffetitemsArray :[BuffetItems] = buffetitemsData!
        for buffetitemsDict in buffetitemsArray {
            var buffetitemsModel = BuffetItems()
            
            buffetitemsModel.buffetItems_name = buffetitemsDict.buffetItems_name
            buffetitemsModel.buffetItems_menuItems = buffetitemsDict.buffetItems_menuItems
        }
        return buffetitemsArray
    }
    
    private var menuitemsData : [MenuItems]?
    
    func getmenuitemsData()->[MenuItems]{
        let menuitemsArray :[MenuItems] = menuitemsData!
        for menuitemsDict in menuitemsArray {
            var menuitemsModel = MenuItems()
            
            menuitemsModel.menuitems_Id = menuitemsDict.menuitems_Id
            menuitemsModel.menuitems_Name = menuitemsDict.menuitems_Name
            menuitemsModel.menuitems_Tags = menuitemsDict.menuitems_Tags
            menuitemsModel.menuitems_Type = menuitemsDict.menuitems_Type
            menuitemsModel.menuitems_Image = menuitemsDict.menuitems_Image
            menuitemsModel.menuitems_Category = menuitemsDict.menuitems_Category
            menuitemsModel.menuitems_Currency = menuitemsDict.menuitems_Currency
            menuitemsModel.menuitems_POSCode = menuitemsDict.menuitems_POSCode
            menuitemsModel.menuitems_Description = menuitemsDict.menuitems_Description
            menuitemsModel.menuitems_DefaultPrice = menuitemsDict.menuitems_DefaultPrice
            menuitemsModel.menuitems_SaleTypeAlaCarte = menuitemsDict.menuitems_SaleTypeAlaCarte
        }
        return menuitemsArray
    }
    
    private var tagsData : [Tags]?
    
    
    func gettagsData()->[Tags]{
        let tagsArray :[Tags] = tagsData!
        for tagsDict in tagsArray {
            var tagsModel = Tags()
            
            tagsModel.tagId = tagsDict.tagId
            tagsModel.tagImage = tagsDict.tagImage
            tagsModel.tagName = tagsDict.tagName
        }
        return tagsArray
    }
    // MARK: - Menu Branch Buffet Ends
    
    
    // MARK: - GST & Service charge conversion Starts
    func getGSTData() -> Double
    {
        /*let valueSGST = (taxDetailsData?.sgst)
        let valueCGST = (taxDetailsData?.cgst)!
        guard valueSGST != nil, valueCGST != nil else {
            return valueCGST
        }
        let value1GST = Float ((taxDetailsData?.sgst)!)
        let value2GST = Float ((taxDetailsData?.cgst)!)
        
        let totalGST = Double (value1GST + value2GST)
        return totalGST*/
        return 3.0
    }
    
    func getServiceChargeData() -> Double
    {
        let valueServiceCharge = self.serviceChargeValue //(taxDetailsData?.servicecharge)!
        return valueServiceCharge
    }
    // MARK: - GST & Service charge conversion Ends
    
    
    //MARK: - Create Booking starts
    private var bookingId: String = ""
    
    var booking_id: String {
        set {
            self.bookingId = newValue
        } get {
            return self.bookingId
        }
    }
    
    private var bookingStatus: String = ""
    
    var booking_status: String {
        set {
            self.bookingStatus = newValue
        } get {
            return self.bookingStatus
        }
    }
    
    private var bookingBranchName: String = ""
    
    var booking_branchname: String {
        set {
            self.bookingBranchName = newValue
        } get {
            return self.bookingBranchName
        }
    }
    
    private var branchAddress: String = ""
    
    var branch_address: String {
        set {
            self.branchAddress = newValue
        } get {
            return self.branchAddress
        }
    }
    
    private var branchContactNo: String = ""
    
    var branch_contact_no: String {
        set {
            self.branchContactNo = newValue
        } get {
            return self.branchContactNo
        }
    }
    
    private var billTotalValue: Double = 0.0
    
    var billTotal: Double {
        set {
            self.billTotalValue = newValue
        } get {
            return self.billTotalValue
        }
    }
    
    private var latitudeValue: Double = 0.0
    
    var latitude: Double {
        set {
            self.latitudeValue = newValue
        } get {
            return self.latitudeValue
        }
    }
    
    private var longitudeValue: Double = 0.0
    
    var longitude: Double {
        set {
            self.longitudeValue = newValue
        } get {
            return self.longitudeValue
        }
    }
    
    private var branchImageData: String = ""
    
    var branchImage: String {
        set {
            self.branchImageData = newValue
        } get {
            return self.branchImageData
        }
    }
    
    private var advanceAmountValue: Double = 0.0
    
    var advanceAmount: Double {
        set {
            self.advanceAmountValue = newValue
        } get {
            return self.advanceAmountValue
        }
    }
    
    private var billwithoutTaxValue: Double = 0.0
    
    var billwithoutTax: Double {
        set {
            self.billwithoutTaxValue = newValue
        } get {
            return self.billwithoutTaxValue
        }
    }
    
    private var bookingdetailsData : [BookingDetails]?
    
    func getbookingdetailsData()->[BookingDetails]{
        let bookingdetailsDataArray = [BookingDetails]()
        guard bookingdetailsData != nil else {
            return bookingdetailsDataArray
        }
        let bookingdetailsArray : [BookingDetails] = bookingdetailsData!
        for bookingdetailsDict in bookingdetailsArray {
            var bookingdetailsModel = BookingDetails()
            bookingdetailsModel.menuId = bookingdetailsDict.menuId
            bookingdetailsModel.buffetId = bookingdetailsDict.buffetId
            bookingdetailsModel.packs = bookingdetailsDict.packs
            //bookingdetailsModel.menuName = bookingdetailsDict.menuName
            //bookingdetailsModel.menuType = bookingdetailsDict.menuType
        }
        return bookingdetailsArray
    }
    
    
    // MARK: - Create Booking Ends
    
    // MARK: - BookingDetails Data Starts
    var bookedDetailsData = [[String:String]]()
    var detailsData = [String: String]()
    var userVoucherData = [String: String]()

    func setupBookedPacksData(forRow indexValue:Int, forPerson peopleValue:Int)
    {
        if bookedDetailsData.count > indexValue
        {
            if bookedDetailsData[indexValue].keys.contains("buffet_id")
            {
                //let existId = details["buffet_id"]
                let existId = bookedDetailsData[indexValue]["buffet_id"]
                //let existStringId = String(existId!)
                if self.buffetdataInfo![indexValue].buffetId == existId
                {
                    bookedDetailsData[indexValue]["packs"] = String(peopleValue)
                    return
                }
            }
        }
        /*let details: [String: Int]*/
        let packsValue = peopleValue
        
        detailsData = ["menu_id":self.menu_id , "buffet_id": self.buffetdataInfo![indexValue].buffetId! , "packs": String(packsValue) , "buffet_name":self.buffetdataInfo![indexValue].buffetName! ,"buffet_type":self.buffetdataInfo![indexValue].buffetType,"buffet_currency":self.buffetdataInfo![indexValue].buffetCurrency] as! [String : String]
        bookedDetailsData.append(detailsData)
    }
    
    func addVoucherDetailsToBookingData(vouchers:[VoucherCouponsData]){
        for vouchObj in vouchers{
            let denominationString:String = String(describing: vouchObj.denomination!)
            userVoucherData = ["voucher_code":vouchObj.barcode! , "voucher_type": vouchObj.voucherType! , "amount": denominationString]
            bookedDetailsData.append(userVoucherData)
        }

    }
    
    func setupUpdateBookedPacksData(forRow indexValue:Int, forPerson peopleValue:Int)
    {
        for details in bookedDetailsData
        {
            let buffetIdValue = details["buffet_id"]
            //let buffetIdValueString = String(buffetIdValue)
            let actualbuffetId = self.buffetdataInfo![indexValue].buffetId!
            if buffetIdValue == actualbuffetId
            {
                bookedDetailsData[indexValue]["packs"] = String(peopleValue)
                return
            }
        }
    }
    
    private var slotIdForConfirmation: String = ""
    
    var slotConfirmation_Id: String {
        set {
            self.slotIdForConfirmation = newValue
        } get {
            return self.slotIdForConfirmation
        }
    }
    
    private var slotTimeForConfirmation: String = ""
    
    var slotConfirmation_Time: String {
        set {
            self.slotTimeForConfirmation = newValue
        } get {
            return self.slotTimeForConfirmation
        }
    }
    
    private var slotReservationTimeForConfirmation: String = ""
    
    var slotReservationConfirmation_Time: String {
        set {
            self.slotReservationTimeForConfirmation = newValue
        } get {
            return self.slotReservationTimeForConfirmation
        }
    }
    // MARK: - BookingDetails Data Ends
}
// MARK: - Date Extension
//Date setting for Today and Tomorrow
//extension Date {
//    var tomorrow: Date? {
//        return Calendar.current.date(byAdding: .day, value: 1, to: self)
//    }
//}

// MARK: - BookingViewModel Extension

extension BookingViewModel {
    // MARK: - getbookingslotswithBranch
    func getbookingslotswithBranch(reservationDate: String, branchId: String, dinnerType: String, completion: @escaping (Bool)-> ())
    {
        BBQBookingManager.getBookingSlotsWithBranch(reservationDate: reservationDate, branchId: branchId, dinnerType: dinnerType){ (model, result) in
            if result {
                
                self.bookingslotswithBranchDataModel = model
                print(self.bookingslotswithBranchDataModel!)
                
                self.branchId = self.bookingslotswithBranchDataModel?.preferredBranch?.branchId ?? ""
                
                self.branchName = self.bookingslotswithBranchDataModel?.preferredBranch?.branchName ?? ""
                
                self.taxesData = self.bookingslotswithBranchDataModel?.preferredBranch?.taxes
                
                self.slotavailableData = self.bookingslotswithBranchDataModel?.preferredBranch?.slotsavailable
                
                self.amenitiesData = self.bookingslotswithBranchDataModel?.preferredBranch?.amenities
                
                self.servicechargeData = self.bookingslotswithBranchDataModel?.preferredBranch?.serviceCharge ?? 0.0
                completion(true)
                
            } else {
                completion(false)
            }
        }
    }
    
    // MARK: - getmenubranchBuffet
    func getmenubranchBuffet(branchId: String, reservationDate: String, slotId: Int, completion: @escaping (Bool)-> ())
    {
        BBQBookingManager.getmenubranchBuffet(branchId: branchId, reservationDate: reservationDate, slotId: slotId){ (model, result) in
            if result {
                self.menubranchbuffetDataModel = model
                self.menuId = self.menubranchbuffetDataModel?.menuId ?? ""
                self.menuName = self.menubranchbuffetDataModel?.menuName ?? ""
                self.buffetsInfo = self.menubranchbuffetDataModel?.buffets
                self.buffetdataInfo = self.buffetsInfo?.buffetData
                guard self.buffetdataInfo != nil else {
                    completion(true)
                    return 
                }
                for buffetItem in self.buffetdataInfo! {
                    self.buffetitemsData = buffetItem.buffetItems
                }
                completion(true)
                
                
            } else {
                completion(false)
            }
        }
        
    }
    
    // MARK: - createBooking
    func createBooking(branchId: String, slotId: String, reservationDate: String, reservationTime: String, bookingDetails: [[String: Any]], completion: @escaping (Bool)-> ())
    {
        /*BBQBookingManager.createBooking(branchId: branchId, slotId: slotId, reservationDate: reservationDate, reservationTime: reservationTime, bookingDetails: bookingDetails as [[String: Any]] as! [[String : Int]]) {
            (model, result) in
            if result {
                
                self.createBookingDataModel = model
                print(self.createBookingDataModel!)
                
                self.bookingId = self.createBookingDataModel?.bookingId ?? ""
                
                self.bookingStatus = self.createBookingDataModel?.bookingStatus ?? ""
                
                let status = self.bookingStatus
                let statusValue = Constants.BBQBookingStatus.confirmedBooking//"BOOKING_CONFIRMED"
                let statusNotValid = Constants.BBQBookingStatus.pendingBooking//"PENDING"
                if status == statusNotValid
                {
                    self.advanceAmountValue = self.createBookingDataModel?.advanceAmount ?? 0.0
                }
                else if status == statusValue
                {
                    self.bookingBranchName = self.createBookingDataModel?.bookingStatus ?? ""
                    
                    self.branchAddress = self.createBookingDataModel?.branchAddress ?? ""
                    
                    self.branchContactNo = self.createBookingDataModel?.branchContactNumber ?? ""
                    
                    self.billTotalValue = self.createBookingDataModel?.billTotal ?? 0.0
                    
                    self.amenitiesData = self.createBookingDataModel?.amenities
                    
                    self.bookingdetailsData = self.createBookingDataModel?.bookingDetails
                    
                    self.latitudeValue = self.createBookingDataModel?.lat ?? 0.0
                    
                    self.longitudeValue = self.createBookingDataModel?.long ?? 0.0
                    
                    self.branchImageData = self.createBookingDataModel?.branchImage ?? ""
                    
                    self.billwithoutTaxValue = self.createBookingDataModel?.billWithoutTax ?? 0.0
                    
                }
                completion(true)
                
            }else {
                completion(false)
            }
        }*/
    }
    
    // MARK: - updateBooking
    func updateBooking(bookingId: String, paymentOrderId:String, paymentId: String, paymentGatewayName: String, paymentSignature:String, completion: @escaping (Bool)->())
    {
        BBQBookingManager.updateBooking(bookingId:bookingId , paymentOrderId: paymentOrderId, paymentId: paymentId, paymentGatewayName: paymentGatewayName, paymentSignature: paymentSignature) {
            (model, result) in
            if result {
                self.bookingupdateDataModel = model
                self.bookingId = self.bookingupdateDataModel?.bookingId ?? ""
                self.bookingStatus = self.bookingupdateDataModel?.bookingStatus ?? ""
                self.bookingBranchName = self.bookingupdateDataModel?.bookingStatus ?? ""
                self.branchAddress = self.bookingupdateDataModel?.branchAddress ?? ""
                self.branchContactNo = self.bookingupdateDataModel?.branchContactNumber ?? ""
                self.branchImageData = self.bookingupdateDataModel?.branchImage ?? ""
                self.latitudeValue = self.bookingupdateDataModel?.lat ?? 0.0
                self.longitudeValue = self.bookingupdateDataModel?.long ?? 0.0
                self.amenitiesData = self.bookingupdateDataModel?.amenities
                //self.bookingdetailsData = self.bookingupdateDataModel?.bookingDetails
                self.billwithoutTaxValue = self.bookingupdateDataModel?.billWithoutTax ?? 0.0
                self.billTotalValue = self.bookingupdateDataModel?.billTotal ?? 0.0
               completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    func getVoucherAndCoupons(completion: @escaping (Bool)->()) {
//        BBQBookingManager.getVoucherAndCoupons { (model, result) in
//            if result {
//                //Assign to model
//                self.vouchersAndCouponsModel = model
//                completion(true)
//            } else {
//                completion(false)
//            }
//        }
        BBQVouchersCouponsManager.getTheCouponsForBooking{(model, result)in
            if result {
                //Assign to model
                self.vouchersAndCouponsModel = model
                completion(true)
            } else {
                completion(false)
            }
        }

    }
    
    func verifySelectedVoucher(barcodeValue: String,selectedPax: Int,voucherAmount: Double, completion: @escaping (Bool)->()) {
        BBQBookingManager.verifyBookingVoucher(barCode: barcodeValue, covers: selectedPax, voucherAmount: voucherAmount)  { (model, result) in
            if result {
                completion(true)
            } else {
                completion(false)
            }
        }
    }
}
