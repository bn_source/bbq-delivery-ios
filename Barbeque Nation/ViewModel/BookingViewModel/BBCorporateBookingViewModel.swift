//
 //  Created by Maya R on 12/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified CorporateBookingViewModel.swift
 //

import Foundation
import Alamofire

class CorporateBookingViewModel :BaseViewModel{
    
    private var coprBooking: CorporateBookingModel?
    private var corporateOffModel:CorporateOffersModel?
    private var coprorateCoupons : [CorporateOffer]?

    var getCorporateOtp: CorporateBookingModel? {
        return self.coprBooking
    }
    
    func getCoporateOffersList()->[CorporateOffer]{
        var offerArray = [CorporateOffer] ()
        if(coprorateCoupons != nil){
            offerArray = coprorateCoupons!
        }
        return offerArray
        
    }

}
extension CorporateBookingViewModel{
    
    func verifyCoporateEmail(corporateEmailId: String,
                             otpID: String,
                             completion: @escaping (Bool)-> ()){
        BBQCorporateBookingManager.verifyCorporateBooking(corporateEmail: corporateEmailId, otpID: otpID){ (data, error) in
            if error == nil {
                do {
                    let decoder = JSONDecoder()
                    guard let responseData = data else {
                        DispatchQueue.main.async {
                            completion(false)
                        }
                        return
                    }
                    self.coprBooking = try decoder.decode(CorporateBookingModel.self, from: responseData)
                    DispatchQueue.main.async {
                        completion(true)
                    }
                } catch {
                    DispatchQueue.main.async {
                        completion(false)
                    }
                }
            } else {
                if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse?.statusCode == 400 {
                    BBQUserManager.configureErrorModel(data: data)
                }
                DispatchQueue.main.async {
                    completion(false)
                }
            }
        }
    }
    
    func getCorporateOfferCoupons(corporateEmail: String,
                                  packs: Int,
                                  reservationDate: String,
                                  branchID:String,
                                  totalAmount:Double,
                                  slotId:Int,
                                  completion: @escaping (Bool)-> ()){
        BBQCorporateBookingManager.getCoporateOffers(corpEmail: corporateEmail, noOfPacks: packs, reservationDate: reservationDate, branchId: branchID, calculatedAmount: totalAmount, slotId: slotId){(data, error)in
            
            if error == nil {
                do {
                    let decoder = JSONDecoder()
                    guard let responseData = data else {
                        DispatchQueue.main.async {
                            completion(false)
                        }
                        return
                    }
                    self.corporateOffModel = try decoder.decode(CorporateOffersModel.self, from: responseData)
                    self.coprorateCoupons = self.corporateOffModel?.corporateOffersList
                    DispatchQueue.main.async {
                        completion(true)
                    }
                } catch {
                    DispatchQueue.main.async {
                        completion(false)
                    }
                }
            } else {
                if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse?.statusCode == 400 {
                    BBQUserManager.configureErrorModel(data: data)
                }
                DispatchQueue.main.async {
                    completion(false)
                }
            }
            
        }
    
    }
    
    func verifyVouchersAndCoupons(barcodeValue: String,packs:Int,amount:Double,branch_ID:String,reservationDate:String, slotID: Int, completion: @escaping (Bool)->()) {
        AnalyticsHelper.shared.triggerEvent(type: .corporate_offers_status)
        BBQCorporateBookingManager.verifyBookingCouponsandVouchers(barcodeValue: barcodeValue,packs:packs,amount:amount,branch_ID:branch_ID,reservationDate:reservationDate, slotID: slotID)  { (model, result) in
            if (model != nil) {
                AnalyticsHelper.shared.triggerEvent(type: .corporate_offers_success_value)
                completion(true)
            } else {
                AnalyticsHelper.shared.triggerEvent(type: .corporate_offers_failure_value)
                completion(false)
            }
        }
    }
    
    func verifyManualCouponCode(barcodeValue: String,
                                packs:Int,
                                branch_ID:String,
                                reservationDate:String,
                                slotID: Int, completion: @escaping (VoucherCouponsData?, Error?)->()) {
        AnalyticsHelper.shared.triggerEvent(type: .corporate_offers_status)
        BBQCorporateBookingManager.verifyManualCoupons(barcodeValue: barcodeValue,
                                                       packs:packs,
                                                       branch_ID:branch_ID,
                                                       reservationDate:reservationDate) { (model, error) in
            if (model != nil) {
                AnalyticsHelper.shared.triggerEvent(type: .corporate_offers_success_value)
                completion(model, error)
            } else {
                AnalyticsHelper.shared.triggerEvent(type: .corporate_offers_failure_value)
                completion(nil, error)
            }
        }
        
    }
    
}
