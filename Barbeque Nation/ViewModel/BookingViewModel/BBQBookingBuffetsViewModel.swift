//
 //  Created by Ajith CP on 16/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQBookingBuffetsViewModel.swift
 //

import UIKit
import Alamofire

typealias BookingDataForCorporateOffers = (reservationDate: String, packs: Int, branchId: String, totalAmount: Double, slotId: Int,currency:String)

class BBQBookingBuffetsViewModel: NSObject {
    private var menuBranchBuffetDataModel: MenuBranchBuffetDataModel?
    private var createBookingDataModel: CreatebookingDataModel?
    //private var bookingUpdateDataModel: UpdateBookingDataModel?
    private var BranchBuffetMenu: BranchBuffetDataModel?
    private var corporateOffersData: BookingDataForCorporateOffers?
    
    private var bookingDetailsBodyDataArray = [BookingDetailsBody]()
    private var bookingVoucherDetailsBodyDataArray = [VoucherDetailsBody]()
    
    var menuId: String? {
        if let buffetModel = self.BranchBuffetMenu {
            return buffetModel.menuId
        }
        return nil
    }
    
    var buffetData: [Buffetdata]? {
        if let buffetModel = self.BranchBuffetMenu, let buffets = buffetModel.buffets, let buffetData = buffets.buffetData {
            return buffetData
        }
        return nil
    }
    
    var bookingStatus: String? {
        if let createBookingModel = self.createBookingDataModel, let status = createBookingModel.bookingStatus {
            return status
        }
        return nil
    }
    
    var bookingId: String? {
        if let createBookingModel = self.createBookingDataModel, let bookingId = createBookingModel.bookingId {
            return bookingId
        }
        return nil
    }
    
    var createBookingModel: CreatebookingDataModel? {
        if let model = self.createBookingDataModel {
            return model
        }
        return nil
    }
    
//    var updateBookingModel: UpdateBookingDataModel? {
//        if let model = self.bookingUpdateDataModel {
//            return model
//        }
//        return nil
//    }
    
    var bookingDataForCorporateOffers: BookingDataForCorporateOffers? {
        return self.corporateOffersData
    }
    
    func getAmountTobePaid() -> (advanceAmount: Double, totalBill: Double, bookingID: String) {
        if let createBookingModel = self.createBookingDataModel, let amount = createBookingModel.advanceAmount, let totalBill = createBookingModel.billTotal, let bookingID = createBookingModel.bookingId {
            return (advanceAmount: amount, totalBill: totalBill, bookingID: bookingID)
        }
        return (advanceAmount: 0.0, totalBill: 0.0, bookingID: "")
    }
    
    func getbuffetDataFor(index: Int) -> Buffetdata? {
        if let buffetModel = self.BranchBuffetMenu, let buffets = buffetModel.buffets, let buffetData = buffets.buffetData {
            return buffetData[index]
        }
        return nil
    }
    
    func getbuffetItemsFor(buffetIndex: Int) -> [ItemsBuffet]? {
        if let buffetItems = getbuffetDataFor(index: buffetIndex)?.buffetItems {
            return buffetItems
        }
        return nil
    }
    
    func getMenuItemsFor(buffetIndex: Int, buffetItemIndex: Int) -> [MenuItems]? {
        if let buffetItems = getbuffetItemsFor(buffetIndex: buffetIndex), let buffetMenus = buffetItems[buffetItemIndex].buffetItems_menuItems {
            return buffetMenus
        }
        return nil
    }
    
    func saveBookingDataForCorporateOffers(reservationDate: String, packs: Int, branchId: String, totalAmount: Double, slotId: Int,currency:String) {
        self.corporateOffersData = (reservationDate: reservationDate, packs: packs, branchId: branchId, totalAmount: totalAmount, slotId: slotId, currency:currency)
    }
    
    func createJsonForCreateBooking(createBookingData: CreateBookingBody) -> Json? {
        do {
            let encoded = try JSONEncoder().encode(createBookingData)
            let jsonString = String(data: encoded, encoding: .utf8)
            let data = jsonString?.data(using: .utf8)
            
            if let jsonArray = try JSONSerialization.jsonObject(with: data!, options : .allowFragments) as? Json {
                print(jsonArray) // use the json here
                return jsonArray
            }
        } catch let error as NSError {
            print(error)
        }
        return nil
    }
}

//MARK:- Service Calls
extension BBQBookingBuffetsViewModel {
    // MARK: - getmenubranchBuffet
    func getMenuBranchBuffet(branchId: String, reservationDate: String, reservationTime: String,
                             slotId: Int, isEarlyBird: Bool, isLateBird: Bool,
                             completion: @escaping (Bool)-> ()) {
        
        BBQBookingManager.getmenubranchBuffet(branchId: branchId, reservationDate: reservationDate, time: reservationTime, slotId: slotId, isEarlyBird: isEarlyBird, isLateBird: isLateBird) { (model, result) in
            if result {
                self.BranchBuffetMenu = model
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    // MARK: - createBooking
    func createBooking(createBookingData: CreateBookingBody, completion: @escaping (Bool, String?)-> ()) {
        if let jsonData = self.createJsonForCreateBooking(createBookingData: createBookingData) {
            BBQBookingManager.createBooking(createBookingJsonData: jsonData) {
                (model, result, error) in
                if result {
                    self.createBookingDataModel = model
                    if let errors = model?.fieldErrors, errors.count > 0{
                        completion(false,errors.first?.message)
                    }else{
                        completion(true,nil)
                    }
                } else {
                    if let errors = model?.fieldErrors, errors.count > 0{
                        completion(false,errors.first?.message)
                    }else{
                        completion(false,nil)
                    }
                }
            }
        }
    }
    
    // MARK: - check for Reservation Rule
    func checkReservationRule(createBookingData: CreateBookingBody, no_of_pax: Int, payable_amt: Double, completion: @escaping (Bool, PendingBookingViewModel.PendingBookingData?)-> ()) {
        if let params = self.createJsonForCreateBooking(createBookingData: createBookingData){
            BBQBookingManager.checkForReservationRules(createBookingData: params, no_of_pax: no_of_pax, payable_amt: payable_amt) { model, result in
                completion(result, model)
            }
        }
    }
    
    
    
    // MARK: - updateBooking
    func updateBooking(bookingId: String, paymentOrderId:String, paymentId: String, paymentGatewayName: String, paymentSignature:String, completion: @escaping (Bool)->()) {
        
        BBQBookingManager.updateBooking(bookingId:bookingId , paymentOrderId: paymentOrderId, paymentId: paymentId, paymentGatewayName: paymentGatewayName, paymentSignature: paymentSignature) {
            (model, result) in
            if result {
                self.createBookingDataModel = model
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    // MARK: - rescheduleBooking
    func rescheduleBooking(rescheduleBookingData: CreateBookingBody, completion: @escaping (Bool)-> ()) {
        if let jsonData = self.createJsonForCreateBooking(createBookingData: rescheduleBookingData) {
            BBQBookingManager.rescheduleBooking(rescheduleBookingJsonData: jsonData) {
                (model, result) in
                if result {
                    self.createBookingDataModel = model
                    completion(true)
                } else {
                    completion(false)
                }
            }
        }
    }
    
    func rescheduleResetAll(bookingId: String, completion: @escaping (Bool)-> ()) {
        BBQBookingManager.rescheduleResetAll(bookingId: bookingId) { (result) in
            if result {
                completion(true)
            } else {
                completion(false)
            }
        }
    }
}

//MARK:- Create/Reschedule Body construction
extension BBQBookingBuffetsViewModel {
    
    //Create/Reschedule API Body construction - Handle with care.
    //Note:- It could break Create/Reschedule service call.
    
    var getBookingDetailsBodyData: [BookingDetailsBody]? {
        return self.bookingDetailsBodyDataArray
    }
    
    func constructBookingDetailsPayload(bookingDetailsBodyData: BookingDetailsBody?) {
        if let data = bookingDetailsBodyData {
            if self.bookingDetailsBodyDataArray.count > 0 {
                var buffetFoundIndex = -1
                for (index, bookingDetailsData) in self.bookingDetailsBodyDataArray.enumerated() {
                    if bookingDetailsData.buffetId == data.buffetId {
                        buffetFoundIndex = index
                        break
                    }
                }
                
                if buffetFoundIndex != -1 {
                    if data.packs == "0" {
                        self.bookingDetailsBodyDataArray.remove(at: buffetFoundIndex)
                    } else {
                        self.bookingDetailsBodyDataArray.remove(at: buffetFoundIndex)
                        self.bookingDetailsBodyDataArray.insert(data, at: buffetFoundIndex)
                    }
                } else {
                    self.bookingDetailsBodyDataArray.append(data)
                }
            } else {
                self.bookingDetailsBodyDataArray.append(data)
            }
        }
    }
    
    func createConsolidatedVoucherDetails(appliedCorpOffers: CorporateOffer?, appliedCoupons: [VoucherCouponsData]?, appliedVouchers: [VoucherCouponsData]?, arrAddedAmounts: [Double], preferredBranch: PreferredBranch?) -> (voucherDetails:[VoucherDetailsBody], voucher_meal_discount: Double) {
        self.bookingVoucherDetailsBodyDataArray.removeAll()
        let voucher_meal_discount = self.contructVoucherDetails(appliedVochers: appliedVouchers, arrAddedAmounts: arrAddedAmounts, preferredBranch: preferredBranch)
        self.contructCouponsDetails(appliedCoupons: appliedCoupons)
        self.contructCorpOffersDetails(appliedCorpOffers: appliedCorpOffers)
        return (self.bookingVoucherDetailsBodyDataArray, voucher_meal_discount)
    }
    
    private func contructVoucherDetails(appliedVochers: [VoucherCouponsData]?, arrAddedAmounts: [Double], preferredBranch: PreferredBranch?) -> Double {
        var arrAddedAmounts = arrAddedAmounts
        var voucher_meal_discount: Double = 0
        if let vouchers = appliedVochers {
            for voucherData in vouchers {
                var dataFound = false
                for globalData in self.bookingVoucherDetailsBodyDataArray {
                    if globalData.voucherCode == voucherData.barcode ?? "" {
                        dataFound = true
                        break
                    }
                }
                if dataFound == false {
                    if voucherData.pax_applicable > 0{
                        let data = countTotalAmount(voucherData: voucherData, arrAddedAmounts: arrAddedAmounts, preferredBranch: preferredBranch)
                        arrAddedAmounts = data.arrAddedAmounts
                        voucher_meal_discount = voucher_meal_discount + data.totalAmount - (voucherData.denomination ?? 0.0)
                        let voucherDetailsBodyData = VoucherDetailsBody(title: voucherData.title ?? "", voucherCode: voucherData.barcode ?? "", voucherType: "GV", amount: data.totalAmount, isCorporateVoucher: "0", pax_applicable: voucherData.pax_applicable)
                        self.bookingVoucherDetailsBodyDataArray.append(voucherDetailsBodyData)
                    }else{
                        let voucherDetailsBodyData = VoucherDetailsBody(title: voucherData.title ?? "", voucherCode: voucherData.barcode ?? "", voucherType: "GV", amount: voucherData.denomination ?? 0.0, isCorporateVoucher: "0", pax_applicable: voucherData.pax_applicable)
                        self.bookingVoucherDetailsBodyDataArray.append(voucherDetailsBodyData)
                    }
                }
            }
        }
        return voucher_meal_discount
    }
    
    private func countTotalAmount(voucherData: VoucherCouponsData, arrAddedAmounts: [Double], preferredBranch: PreferredBranch?) -> (totalAmount:Double, arrAddedAmounts:[Double]) {
        var voucherAmount: Double = 0.0
        var arrAddedAmounts = arrAddedAmounts
        
        for _ in 0..<voucherData.pax_applicable{
            if arrAddedAmounts.count <= 0 {
                break
            }
            var index = 0
            for i in 0..<arrAddedAmounts.count{
                if arrAddedAmounts[index] < arrAddedAmounts[i]{
                    index = i
                }
            }
            voucherAmount += arrAddedAmounts[index]
            arrAddedAmounts.remove(at: index)
        }
        
        var taxAmount: Double = 0.0
        let taxList = preferredBranch?.taxes ?? [Taxes]()
        for item in taxList {
            taxAmount += self.getTaxAmountValue(with: voucherAmount, and: item.taxPercentage ?? 0.0)
        }
        
        taxAmount += preferredBranch?.serviceCharge ?? 0.0
        
        return (voucherAmount+taxAmount, arrAddedAmounts)
    }
    
    private func getTaxAmountValue(with voucherAmount: Double, and taxPercentage: Double) -> Double {
        return voucherAmount * (taxPercentage/100)
    }
    
    private func contructCouponsDetails(appliedCoupons: [VoucherCouponsData]?) {
        if let coupons = appliedCoupons {
            for couponData in coupons {
                var dataFound = false
                for globalData in self.bookingVoucherDetailsBodyDataArray {
                    if globalData.voucherCode == couponData.barcode ?? "" {
                        dataFound = true
                        break
                    }
                }
                if dataFound == false {
                    let couponsDetailsBodyData = VoucherDetailsBody(title: couponData.title ?? "", voucherCode: couponData.barcode ?? "", voucherType: "GC", amount: couponData.denomination ?? 0.0, isCorporateVoucher: "0", pax_applicable: 0)
                    self.bookingVoucherDetailsBodyDataArray.append(couponsDetailsBodyData)
                }
            }
        }
    }
    
    private func contructCorpOffersDetails(appliedCorpOffers: CorporateOffer?) {
        if let coupons = appliedCorpOffers {
            var dataFound = false
            for globalData in self.bookingVoucherDetailsBodyDataArray {
                if globalData.voucherCode == coupons.corporateBarCode {
                    dataFound = true
                    break
                }
            }
            if dataFound == false {
                let corpCouponsDetailsBodyData = VoucherDetailsBody(title: coupons.corporateTitle, voucherCode: coupons.corporateBarCode, voucherType: "GC", amount: coupons.corporateDenomination, isCorporateVoucher: "1", pax_applicable: 0)
                self.bookingVoucherDetailsBodyDataArray.append(corpCouponsDetailsBodyData)
            }
        }
    }
}
