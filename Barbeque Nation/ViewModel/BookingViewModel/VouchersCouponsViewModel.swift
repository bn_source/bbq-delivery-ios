//
 //  Created by Maya R on 18/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified VouchersCouponsViewModel.swift
 //

import Foundation
import Alamofire

class VouchersCouponsViewModel:BaseViewModel{
    var vouchersAndCouponsModel: VoucherCouponsModel?
    var vouchersAndCouponsData: VoucherCouponsData?

}

extension VouchersCouponsViewModel{
    func getAllTheCouponsForBooking(noOfPack:Int,
                                    amount:Double,
                                    branchID:String,
                                    reservationDate:String,
                                    slotID:Int,
                                    currency:String,
                                    completion: @escaping (Bool)->()){
        BBQVouchersCouponsManager.getTheCouponsForBooking(noOfPack:noOfPack,
                                                          amount:amount,
                                                          branchID:branchID,
                                                          reservationDate:reservationDate,
                                                          slotID:slotID,
                                                          currency:currency)
        {(model, result)in
            if result {
                //Assign to model
                self.vouchersAndCouponsModel = model
                if var modelObj = model?.couponsAndVouchers{
                    modelObj.sort(by: { i1, i2 in
                        return i1.voucherApplicability ?? false
                    })
                    self.vouchersAndCouponsModel?.couponsAndVouchers = modelObj
                }
                completion(true)
            } else {
                completion(false)
            }
        }
        
    }
    
    func getAllTheVouchersForBooking(completion: @escaping (Bool)->()){
        BBQVouchersCouponsManager.getTheVouchersForBooking{(model, result)in
            if result {
                //Assign to model
                self.vouchersAndCouponsModel = model
                if var modelObj = model?.couponsAndVouchers{
                    modelObj.sort(by: { i1, i2 in
                        return i1.voucherApplicability ?? false
                    })
                    self.vouchersAndCouponsModel?.couponsAndVouchers = modelObj
                }
                completion(true)
            } else {
                completion(false)
            }
        }
        
    }
    
     func verifyPurchasedCouponAndVoucher(barcodeValue: String,
                                                 mobileNumber:String,
                                                 totalAmount:Double,
                                                 countryCode:String,
                                               completion:@escaping (Bool)->()){
        BBQVouchersCouponsManager.verifyPurchasedCouponsandVouchers(barcodeValue: barcodeValue, mobileNumber: mobileNumber, totalAmount: totalAmount, countryCode: countryCode) { (model, result) in
            if result! {
                self.vouchersAndCouponsData = model
                completion(true)
            } else {
                completion(false)
            }

        }
        
    }
    


}
