//
 //  Created by Chandan Singh on 16/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQSlotsViewModel.swift
 //

import Foundation

enum BBQOfferType : String {
    case Smiles = "SMILES"
    case Vouchers = "VOUCHERS"
    case Coupons = "COUPONS"
    case CorporateOffers = "CORPOFFERS"
}

class SlotsViewModel: BaseViewModel {
    
    private var bookingSlotsWithBranchDataModel: BookingSlotsWithBranchDataModel?
    
    internal var offerCards = [BBQOfferType : (Double, Bool)]()
    internal var offerType : BBQOfferType?
    internal var dateType : BBQBookingDateType?
    
    private var couponAmount : Double = 0.00
    private var corpOffersAmount : Double = 0.00
    private var voucherAmount : Double = 0.00
    private var pointsAmount : Double = 0.00
    
    var isAmountNil : Bool = false
    
    var finalAmountForPayment : Double = 0.00 {
        didSet {
            if finalAmountForPayment.rounded() <= 0.00 {
                isAmountNil = true
            } else {
                isAmountNil = false
            }
        }
    }
    
    var appliedCardAmount : Double = 0.00 {
        didSet {
            if self.offerCards.count < 1 {
                self.finalAmountForPayment = self.bookingAmountSubTotal +
                    self.totalTaxAmount +
                    self.appliedCardAmount
            }

        }
    }
   
    var totalTaxAmount : Double  {
        set {
            self.finalAmountForPayment = self.bookingAmountSubTotal +
                newValue +
                self.appliedCardAmount
        } get {
            
            let taxList = self.getTaxesForPrefferedBranch()?.taxes ?? [Taxes]()
            var taxNserviceAmount = 0.00
            for item in taxList {
                taxNserviceAmount += self.getTaxAmountValue(with: item.taxPercentage ?? 0.0)
            }
            taxNserviceAmount += self.getTaxesForPrefferedBranch()?.serviceCharge ?? 0.0
            return taxNserviceAmount
        }
    }
  
    var bookingAmountSubTotal : Double = 0.0
    
    
    
    
    
    var prefferedBranchData: PreferredBranch? {
        if let slotsData = self.bookingSlotsWithBranchDataModel, let prefBranch = slotsData.preferredBranch {
            return prefBranch
        }
        return nil
    }
    
    var availableSlotsData: [SlotsAvailable] = [SlotsAvailable]()
    
    private func arrangeSlotForTheDate() {
        availableSlotsData.removeAll()
        if let slotsData = self.bookingSlotsWithBranchDataModel, let prefBranch = slotsData.preferredBranch, var availableSlotData = prefBranch.slotsavailable {
            if self.dateType == .Today {
                for slotItem in availableSlotData {
                    if let invalidOffset = availableSlotData.firstIndex(where: {_ in slotItem.slotStartTime?.isPastTimeString() ?? false}) {
                        availableSlotData.remove(at: invalidOffset)
                    }
                }
            }
            self.availableSlotsData = availableSlotData
        }
    }
    
    func getSmilesDataCount() -> Double {
        var taxes = 100.0
        for tax in getTaxesForPrefferedBranch()?.taxes ?? [Taxes](){
            taxes += tax.taxPercentage ?? 0
        }
        return ((100.0*finalAmountForPayment)/taxes).rounded(.up)
    }
    
    func getSlotDataFor(index: Int) -> (slotId: Int, slotStartTime: String)? {
        if availableSlotsData.count > index {
            
            //To reduce the chances of crash
            //case index > avilableSlotData.count
            let avilableSlotData = availableSlotsData[index]
            return (slotId: avilableSlotData.slotId ?? 0, slotStartTime: avilableSlotData.slotStartTime ?? "")
        }
        return nil
    }
    
    func getSlotInfoFor(slotID: Int) -> (isEarlyBird: Bool, isLateBird: Bool)? {
        if availableSlotsData.count > 0 {
            for item in availableSlotsData {
                if item.slotId == slotID {
                    return (isEarlyBird: item.isEarlyBird ?? false,
                            isLateBird: item.isLateBird ?? false)
                }
            }
        }
        return nil
    }
    
    func getTaxesForPrefferedBranch() -> (taxes: [Taxes]?, serviceCharge: Double?)? {
        if let prefferedBranch = self.prefferedBranchData {
            return (taxes: prefferedBranch.taxes, serviceCharge: prefferedBranch.serviceCharge)
        }
        return nil
    }
    
    func getSlotTimeFor(slotId: Int) -> String {
        var slotTime = ""
        if availableSlotsData.count > 0 {
            for slotData in availableSlotsData {
                if slotData.slotId == slotId {
                    slotTime = slotData.slotStartTime ?? ""
                    break
                }
            }
        }
        return slotTime
    }
    
    func getSlotTime(from slotIndex: Int) -> String {
        var slotTime = ""
        
        if availableSlotsData.count > slotIndex {
            for (index, slotData) in availableSlotsData.enumerated() {
                if index == slotIndex {
                    slotTime = slotData.slotStartTime ?? ""
                    break
                }
            }
        }
        return slotTime
    }
    
    func getAvailableCapacity(from slotIndex: Int) -> Int {
        var availableCapacity = 0
        
        if availableSlotsData.count > slotIndex {
            for (index, slotData) in availableSlotsData.enumerated() {
                if index == slotIndex {
                    availableCapacity = slotData.availableCapacity ?? 0
                    break
                }
            }
        }
        return availableCapacity
    }
    

    
    func getFirstAvailableSlot() -> (index: Int?, slotID: Int?) {
        for item in self.availableSlotsData {
            if let capacity = item.availableCapacity, capacity > 0 {
                let index = self.availableSlotsData.firstIndex(where: {$0 == item})
                return (index, item.slotId)
            }
        }
        return (nil, nil)
    }
}

extension SlotsViewModel {
    func getBookingSlotsWithBranch(selectedDateType: BBQBookingDateType,
                                   reservationDate: String,
                                   branchId: String, dinnerType: String,
                                   completion: @escaping (Bool)-> ()) {
        self.dateType = selectedDateType
        BBQBookingManager.getBookingSlotsWithBranch(reservationDate: reservationDate, branchId: branchId, dinnerType: dinnerType){ (model, result) in
            if result {
                self.bookingSlotsWithBranchDataModel = model
                self.arrangeSlotForTheDate()
                completion(true)
            } else {
                completion(false)
            }
        }
    }
}

extension SlotsViewModel {
    
    // Calculations for dynamic booking amounts, very sensitive part
    // TODO: AjithCP Document methods
    
    func getTaxAmountValue(with taxPercentage: Double) -> Double {
        return self.bookingAmountSubTotal * (taxPercentage/100)
    }
    
    func adjustTotalAmountForPayment(with adjustmentAmount: Double,
                                     isAddition: Bool,
                                     offerType: BBQOfferType) {
        self.offerType = offerType
    
        let formattedAdjustAmount = Double(String(format: "%.2f", adjustmentAmount)) ?? 0.00
        let amountForCalculation  = self.appliedOnTotalSubTotal(adjustedAmount: formattedAdjustAmount,
                                                                type: offerType)

        self.offerCards.updateValue((amountForCalculation, isAddition), forKey: offerType)
        
        if let appliedCouponAmount = self.offerCards[.Coupons]?.0,
            let isAdd = self.offerCards[.Coupons]?.1 {
            self.couponAmount = isAdd ? -appliedCouponAmount : appliedCouponAmount
        }
        
        if let appliedPointsAmount = self.offerCards[.Smiles]?.0,
            let isAdd = self.offerCards[.Smiles]?.1 {
            self.pointsAmount = isAdd ? -appliedPointsAmount : appliedPointsAmount
        }
        
        if let appliedCorpOffersAmount = self.offerCards[.CorporateOffers]?.0,
            let isAdd = self.offerCards[.CorporateOffers]?.1 {
            self.corpOffersAmount = isAdd ? -appliedCorpOffersAmount : appliedCorpOffersAmount
        }
        
        if let appliedVoucherAmount = self.offerCards[.Vouchers]?.0,
            let isAdd = self.offerCards[.Vouchers]?.1 {
            self.voucherAmount = isAdd ? -appliedVoucherAmount : appliedVoucherAmount
        }

        switch offerType {
        case .CorporateOffers:
            calculateBookingAmountSubTotal(with: corpOffersAmount)
        case .Coupons:
            calculateBookingAmountSubTotal(with: couponAmount)
        case .Vouchers:
            calculateFinalAmountForPay(with: voucherAmount)
        case .Smiles:
            calculateBookingAmountSubTotal(with: pointsAmount)
        }
        
        if !isAddition {
            self.removeCards(for: offerType)
        }

    }
    
    func consumedCouponAmount(denomination: Double, payAmount: Double) -> Double {
        if denomination > payAmount {
            return payAmount
        }
        return denomination
    }
    
    func resetBookingCardsApplication() {
        self.offerCards.removeAll()
        self.appliedCardAmount = 0.00
    }
    
    func totalVoucherAmount(for appliedVochers:[VoucherCouponsData]) -> (amount: Double, packs: Int) {
        var totalVoucherAmount = 0.00
        var packs = 0
        for voucherData in appliedVochers {
            if let deductionAmountForItem = voucherData.denomination, voucherData.pax_applicable == 0 {
                totalVoucherAmount += deductionAmountForItem
            }else{
                packs += voucherData.pax_applicable
            }
        }
        return (totalVoucherAmount, packs)
    }
    
    
    func getBookingAmountRowData() -> [AmountRowData] {
        var amountSplitList = [AmountRowData]()
        var amountItem =  AmountRowData()
        amountItem.amountTitle = kSubTotalTitle
        amountItem.amountValue =  String(format: "%.2f", self.bookingAmountSubTotal)
        amountSplitList.append(amountItem)
        
        var taxAmount: Double = 0
        let taxList = self.getTaxesForPrefferedBranch()?.taxes ?? [Taxes]()
        for item in taxList {
            //amountItem.amountTitle = item.tax
            //amountItem.amountValue = String(format: "%.2f", self.getTaxAmountValue(with: item.taxPercentage ?? 0.0))
            //amountSplitList.append(amountItem)
            taxAmount += self.getTaxAmountValue(with: item.taxPercentage ?? 0.0)
        }
        
//        amountItem.amountTitle = kDiningServiceCharge
//        amountItem.amountValue =
//            String(format: "%.2f", self.getTaxesForPrefferedBranch()?.serviceCharge ?? 0.0)
//        amountSplitList.append(amountItem)
        taxAmount += self.getTaxesForPrefferedBranch()?.serviceCharge ?? 0.0
        amountSplitList.append(AmountRowData(amountTitle: kDiningTaxServiceCharge, amountValue: String(format: "%.2f", taxAmount)))
        return amountSplitList
    }
    
    func getTaxBreakUpModel() -> [TaxBreakUp]{
        var taxBreakups = [TaxBreakUp]()
        let taxList = self.getTaxesForPrefferedBranch()?.taxes ?? [Taxes]()
        for item in taxList {
            var tax = TaxBreakUp(label: item.tax ?? "", value: String(format: "%.2f", self.getTaxAmountValue(with: item.taxPercentage ?? 0.0)))
            taxBreakups.append(tax)
        }
        var tax = TaxBreakUp(label: kDiningServiceCharge, value: String(format: "%.2f", self.getTaxesForPrefferedBranch()?.serviceCharge ?? 0.0))
        taxBreakups.append(tax)
        return taxBreakups
    }
    
    func isSlotAvailable(for index: Int) -> Bool {
        if availableSlotsData.count > index{
            return (availableSlotsData[index].availableCapacity != 0)
        }
        return false
    }
    
    func isCouponsEnabled(isCorpOffersAdded: Bool) -> Bool {
        if (offerCards.keys.contains(.Coupons) || !isAmountNil) && !isCorpOffersAdded {
            return true
        }
        return false
    }
    
    func isHappinessCardsEnabled() -> Bool {
        if offerCards.keys.contains(.Vouchers) || !isAmountNil {
            return true
        }
        return false
    }
    
    func isCorpOffersEnabled(isCouponsAdded: Bool) -> Bool {
        if (offerCards.keys.contains(.CorporateOffers) || !isAmountNil) && !isCouponsAdded {
            return true
        }
        return false
    }
    
    func isPointsEnabled() -> Bool {
        if offerCards.keys.contains(.Smiles) || !isAmountNil {
            return true
        }
        return false
    }
    
    // Array of index to refresh after add/remove cartds
    func cardArrayToRefresh() -> [IndexPath] {
        let amountSplitIndex = IndexPath(row: 1, section: 0)
        let smilesIndex = IndexPath(row: 2, section: 0)
        let happCardIndex = IndexPath(row: 4, section: 0)
        let couponIndex = IndexPath(row: 3, section: 0)
        let corpOffersIndex = IndexPath(row: 5, section: 0)
        let paymentIndex = IndexPath(row: 6, section: 0)
        return [amountSplitIndex, smilesIndex, couponIndex, happCardIndex, corpOffersIndex, paymentIndex]
    }
    
    // MARK: Private Methods
    
    private func calculateBookingAmountSubTotal(with amount: Double) {
        bookingAmountSubTotal += amount
        finalAmountForPayment = (bookingAmountSubTotal + totalTaxAmount)
        self.balanceAdjustmentAmount()
    }
    
    private func calculateFinalAmountForPay(with amount: Double) {
        if amount == 0{
            calculateBookingAmountSubTotal(with: amount)
        }else{
            finalAmountForPayment += amount
        }
    }
    
    private func removeCards(for offerType: BBQOfferType) {
        self.offerCards.remove(at: self.offerCards.index(forKey: offerType)!)
        self.couponAmount = offerType == .Coupons ? 0.00 : self.couponAmount
        self.voucherAmount = offerType == .Vouchers ? 0.00 : self.voucherAmount
        self.pointsAmount = offerType == .Smiles ? 0.00 : self.pointsAmount
        self.corpOffersAmount = offerType == .CorporateOffers ? 0.00 : self.corpOffersAmount
    }
    
    private func balanceAdjustmentAmount() {
        if offerType == .Coupons {
            finalAmountForPayment +=  self.corpOffersAmount + self.voucherAmount
        }
        if offerType == .CorporateOffers {
            finalAmountForPayment +=  self.voucherAmount + self.couponAmount
        }
        if offerType == .Smiles {
            finalAmountForPayment +=  self.voucherAmount
        }
    }
    
    private func appliedOnTotalSubTotal(adjustedAmount: Double, type: BBQOfferType) -> Double {
        if type == .Vouchers || type == .Smiles {
            return adjustedAmount
        }
        let adjustAmount = type == .Coupons ? couponAmount : corpOffersAmount
        if adjustedAmount >= self.bookingAmountSubTotal - adjustAmount {
            return self.bookingAmountSubTotal
        }
        return adjustedAmount
    }
}

