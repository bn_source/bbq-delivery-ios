//
//  BBQBookingConfirmationViewModel.swift
//  Barbeque Nation
//
//  Created by Nischitha on 13/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import Foundation
import UIKit

class BookingConfirmationViewModel {
    
    var bookingID: String = ""
    var model: BBQDiningDataModel?
    var coupons = [BookingVoucher]()
    var vouchers = [BookingVoucher]()
    
    var voucher_amount: Double = 0
    var coupon_amount: Double = 0
    var discount_details: Double = 0
    var sub_total: Double = 0
    var taxes = [Taxes]()
    var tax_amount: Double = 0
    
    init(bookingID: String) {
        self.bookingID = bookingID
    }
    
    func getBookingData(completion: @escaping (_ model: BBQDiningDataModel?, _ error: Error?)->()) {
        UIUtils.showLoader()
        let endPoint = BBQBookingRouter.bookingPaymentData(bookingID: bookingID)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQBookingManager.parseAfterDiningPayment(response: responseData, error: responseError)
            if let value = modelValue {
                self.model = value
                self.vouchers.removeAll()
                self.coupons.removeAll()
                for coupon in modelValue?.vouchers ?? [BookingVoucher]() {
                    if coupon.voucherType == "GV"{
                        self.vouchers.append(coupon)
                    }else{
                        self.coupons.append(coupon)
                    }
                }
                self.saveCouponAmount()
                self.saveVoucherAmount()
                self.saveDiscountDetails()
                self.saveSubTotal()
                self.saveTaxes()
                DispatchQueue.main.async {
                    completion(value, responseError)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, responseError)
                }
            }
            UIUtils.hideLoader()
        }
    }

    // MARK: - updateBooking
    
    func updateBooking(bookingId: String, paymentOrderId:String, paymentId: String, paymentGatewayName: String, paymentSignature:String, completion: @escaping (Bool)->()) {

        BBQBookingManager.updateBooking(bookingId:bookingId , paymentOrderId: paymentOrderId, paymentId: paymentId, paymentGatewayName: paymentGatewayName, paymentSignature: paymentSignature) {
            (model, result) in
            if result {
                completion(true)
            } else {
                completion(false)
            }
        }
    }

//    // MARK: - rescheduleBooking
//    func rescheduleBooking(rescheduleBookingData: CreateBookingBody, completion: @escaping (Bool)-> ()) {
//        if let jsonData = self.createJsonForCreateBooking(createBookingData: rescheduleBookingData) {
//            BBQBookingManager.rescheduleBooking(rescheduleBookingJsonData: jsonData) {
//                (model, result) in
//                if result {
//                    self.createBookingDataModel = model
//                    completion(true)
//                } else {
//                    completion(false)
//                }
//            }
//        }
//    }
//
    func rescheduleResetAll(bookingId: String, completion: @escaping (Bool)-> ()) {
        BBQBookingManager.rescheduleResetAll(bookingId: bookingId) { (result) in
            if result {
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    private func saveVoucherAmount(){
        voucher_amount = 0
        for voucher in vouchers {
            voucher_amount += voucher.amount ?? 0.0
        }
    }
    
    private func saveDiscountDetails(){
        discount_details = 0
        for coupon in model?.discountDetails ?? [DiscountDetails]() {
            discount_details += coupon.amount ?? 0.0
        }
    }
    
    private func saveCouponAmount() {
        coupon_amount = 0
        for coupon in coupons {
            coupon_amount += coupon.amount ?? 0.0
        }
    }
    
    private func saveSubTotal() {
        sub_total = model?.billWithoutTax ?? 0
        sub_total -= Double(model?.loyaltyPoints ?? 0)
        sub_total -= coupon_amount
        sub_total = sub_total > 0 ? sub_total : 0
    }
    
    private func saveTaxes() {
        tax_amount = 0
        taxes.removeAll()
        for tax in model?.taxes ?? [Taxes](){
            if let tax_per = tax.taxPercentage, tax_per == 0{
                tax_amount += (sub_total * tax_per * 0.01)
            }else{
                taxes.append(tax)
                tax_amount += tax.taxAmount ?? 0.0
            }
        }
        tax_amount += model?.vat_tax?.taxAmount ?? 0.0
        tax_amount += model?.cess_tax?.taxAmount ?? 0.0
    }
    
    func getTotalPayableAmount() -> String {
        if let status = self.model?.bookingStatus, (status == "SETTLED" || status == "PRE_RECEIPT"){
            return String(format: "%@%.02f", getCurrency(), self.model?.netPayable ?? 0.0)
        }
        if let netPayable = self.model?.netPayable, netPayable > 0 {
            return String(format: "%@%.02f", getCurrency(), netPayable)
        }else{
            let billTotal = self.model?.billTotal ?? 0
            return String(format: "%@%.02f", getCurrency(), billTotal)
        }
    
    }
    
    
    func isBookingSettled() -> Bool{
        return model?.isBookingSettled() ?? false
    }
    
    func callNumber()
    {
        guard let phone = model?.branchContactNumber else {
            return
        }
        
        if let phoneUrl = URL(string: "telprompt://\(phone.replacingOccurrences(of: " ", with: ""))"){
            if(UIApplication.shared.canOpenURL(phoneUrl)) {
                UIApplication.shared.open(phoneUrl) { (success) in
                    if(!success) {
                        // Show an error message: Failed opening the url
                    }
                }
            }
        }else if let phoneFallbackUrl = URL(string: "tel://\(phone.replacingOccurrences(of: " ", with: ""))"){
            if(UIApplication.shared.canOpenURL(phoneFallbackUrl)) {
               UIApplication.shared.open(phoneFallbackUrl) { (success) in
                   if(!success) {
                       // Show an error message: Failed opening the url
                   }
               }
           }
        }else {
            // Show an error message: Your device can not do phone calls.
        }
    }
    
//    // MARK: API Methods
//    
//    func fetchAfterDiningPaymentData(for bookingID: String,
//                                     completion: @escaping (_ isFetched: Bool)->()) {
//        BBQBookingManager.afterDiningPaymentData(for: bookingID) { (diningData, error) in
//            if error == nil {
//                var objData = diningData
//                objData?.netPayable = diningData?.netPayable == 0 ? diningData?.billTotal : diningData?.netPayable
//                self.diningDataModel = objData
//                self.setupDataSupplyModels(with: self.diningDataModel)
//                completion(true)
//            } else {
//                completion(false)
//            }
//        }
//    }
//    
//    func afterDiningPaymentDataDetails(for bookingID: String,
//                                     completion: @escaping (_ isFetched: Bool)->()) {
//        BBQBookingManager.afterDiningPaymentDataDetails(for: bookingID) { (diningData, error) in
//            if error == nil {
//                var objData = diningData
//                objData?.netPayable = diningData?.netPayable == 0 ? diningData?.billTotal : diningData?.netPayable
//                self.diningDataModel = objData
//                self.setupDataSupplyModels(with: self.diningDataModel)
//                completion(true)
//            } else {
//                completion(false)
//            }
//        }
//    }
//    
//    func updateBooking(finalPaymentData: FinalPaymentUpdateBody, completion: @escaping (Bool)->()) {
//        if let jsonData = self.createJsonForFinalPayment(updatePaymentData: finalPaymentData) {
//            
//            BBQBookingManager.updateFinalPayment(updatePayJsonData: jsonData) { (model, result) in
//                if result {
//                    completion(true)
//                } else {
//                    completion(false)
//                }
//            }
//        }
//        
//        
//    }
}
