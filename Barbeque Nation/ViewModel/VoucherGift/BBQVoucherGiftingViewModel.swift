//
 //  Created by Ajith CP on 03/01/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified BBQVoucherGiftingViewModel.swift
 //

import UIKit

class BBQVoucherGiftingViewModel: NSObject {

    func giftVoucherTo(barCode: String,
                       countryCode: String,
                       phoneNumber: String,
                       email: String, 
                       name: String,
                       completion: @escaping (_ result: Bool)->()) {
        
        BBQBookingManager.giftVoucher(barCode: barCode,
                                      countryCode: countryCode,
                                      phoneNumber: phoneNumber,
                                      email: email,
                                      name: name) { (isGifted) in
                                        completion(isGifted)
        }
    }
    
    
    func isValidMobileNumber(countyryCode: String, mobileNumber: String) -> Bool {
        return BBQPhoneNumberValidation.isValidNumber(number: mobileNumber,
                                                      for: countyryCode)
    }
    
    func isValidEmailAddress(emailText: String) -> Bool {
        return emailText.isValidEmailID()
    }
    
    func isValidNameEntered(nameText: String) -> Bool {
        return nameText.count > 2 ? true : false
    }
    
    func isGiftingPageValidated(mobileValid: Bool, emailValid: Bool, nameValid: Bool) -> Bool {
        return mobileValid && emailValid && nameValid
    }

}
