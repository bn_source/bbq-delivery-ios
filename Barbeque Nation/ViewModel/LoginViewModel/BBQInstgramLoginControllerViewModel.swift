//
//  BBQInstgramLoginControllerViewModel.swift
//  Barbeque Nation
//
//  Created by Ajith CP on 12/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

class BBQInstgramLoginControllerViewModel: BaseViewModel {
    
    private var instUserDataModel: InstagramUserDataModel?
    
    
    var instagramAuthenticationURL: String {
        return String(format: Constants.InstagramSignup.instagramAuthFormat,
                      arguments:[Constants.InstagramSignup.instagramAuthURL,
                                 Constants.InstagramSignup.instagramClientID,
                                 Constants.InstagramSignup.instagramClientSecret,
                                 Constants.InstagramSignup.instagramRedirectURI,
                                 Constants.InstagramSignup.instagramScope])
    }
    
    func isRecievedRequestForCallbackURLWithToken(request: URLRequest) -> (isSuccess: Bool, authToken: String) {
        
        let requestURLString = (request.url?.absoluteString)! as String
        if requestURLString.hasPrefix(Constants.InstagramSignup.instagramRedirectURI) {
            let range: Range<String.Index> = requestURLString.range(of: "#access_token=")!
            let authToken = String(requestURLString[range.upperBound...])
            return (true, authToken)
        }
        return (false, "")
    }
    
    func getInstagramUserInfo(with accessToken: String, completion: @escaping (String) -> ())  {
        BBQUserManager.getInstagramUser(with: accessToken) { (data, error) in
            
            if error == nil {
                do {
                    let decoder = JSONDecoder()
                    guard let responseData = data else {
                        DispatchQueue.main.async {
                            completion("")
                        }
                        return
                    }
                    
                    self.instUserDataModel = try decoder.decode(InstagramUserDataModel.self,
                                                                from: responseData)
                    DispatchQueue.main.async {
                        completion(self.instUserDataModel?.instaUser?.userFullName ?? "")
                    }
                } catch {
                    DispatchQueue.main.async {
                        completion("")
                    }
                }
            } else {
                DispatchQueue.main.async {
                    completion("")
                }
            }
        }
    }
}
