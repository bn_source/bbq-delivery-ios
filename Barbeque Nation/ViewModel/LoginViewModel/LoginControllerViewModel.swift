//
//  LoginControllerViewModel.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 04/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import Foundation


class LoginControllerViewModel: BaseViewModel {
   
    private var tokenDataModel: TokenDataModel?
    
    private var userModel: SocialLoginUserModel?
    //Access Token
    var accessToken: String {
        return BBQUserDefaults.sharedInstance.accessToken
    }
    
    //Refresh Token
    var refeshToken: String {
        return BBQUserDefaults.sharedInstance.refreshToken
    }
    
}

extension LoginControllerViewModel {
   
    func refreshToken(completion: @escaping (Bool) -> ()) {
        BBQLoginManager.refreshToken { [weak self] (model, result) in
            if let self = self {
                self.tokenDataModel = model
                result == true ? completion(true): completion(false)
            }
        }
    }
    
    func login(countryCode: String, mobileNumber: Int64, otpId: String, otp: Int, completion: @escaping (Bool) -> ()) {
        BBQLoginManager.login(countryCode: countryCode, mobileNumber: mobileNumber, otpId: otpId, otp: otp) { [weak self] (model, result) in
            if let self = self {
                self.tokenDataModel = model
                result == true ? completion(true): completion(false)
            }
        }
    }
    
    
    /* For both social login and signup we have single API. Once it called only with token and provider,
        it will return whether it's existing user or not. For existing user API will return access tokens.
        If not an existing user same API will be called again after filling signup form with remaining parameters.
        In that case it will return access tokens for signup.
    **/
    func socialLogin(token: String, provider: String, countryCode: String, mobileNumber: Int64, otpId: String, otp: Int, title: String, name: String , completion: @escaping (_ isNewUser:Bool, _ isLoginFailed: Bool) -> ()) {
        BBQLoginManager.socialLogin(token: token, provider: provider, countryCode: countryCode, mobileNumber: mobileNumber, otpId: otpId, otp: otp, title: title, name: name) { [weak self] (model, isNewUser, isLoginFailed) in
            if let self = self {
                self.tokenDataModel = model
                completion(isNewUser, isLoginFailed)                
            }
        }
    }
    
    func registerUser(countryCode: String, mobileNumber: Int64, email: String = "", title: String, name: String, dob: String = "", maritalStatus: Int = 0, newsletter: String = "", anniversary: String = "", otpId: String, otp: Int,  referralCode: String,completion: @escaping (Bool) -> ()) {
        BBQLoginManager.registerUser(countryCode: countryCode, mobileNumber: mobileNumber, email: email, title: title, name: name, dob: dob, maritalStatus: maritalStatus, newsletter: newsletter, anniversary: anniversary, otpId: otpId, otp: otp, referralCode: referralCode) { [weak self] (model, result) in
            
            if let self = self {
                self.tokenDataModel = model
                result == true ? completion(true): completion(false)
            }
        }
    }
}
