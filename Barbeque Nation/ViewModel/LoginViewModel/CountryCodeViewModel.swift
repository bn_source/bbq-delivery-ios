//  CountryCodeViewModel.swift
/*
 *  Created by Rabindra L on 08/08/19.
 *  Copyright © BARBEQUE NATION HOSPITALITY LIMITED 2019. All rights reserved.
 *  Last modified 03/09/19
 
 * CHANGE LOG
 * —------------------------------------------------------------------------------
 * ID          Bug ID      Author Name          Date                  Description
 * —------------------------------------------------------------------------------
 * 1            NA         Rabindra L           08/08/19        Initial Version
 * 2            1          Rabindra L           03/09/19        Review comments
 */

import Foundation

class CountryCodeViewModel {
    
    var countries: [(String, String)] = {
        var countryList:[(countryCode: String, countryName: String)] = []
        
        for code in NSLocale.isoCountryCodes as [String] {
            let id = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
            let name = NSLocale(localeIdentifier: "en_UK").displayName(forKey: NSLocale.Key.identifier, value: id) ?? ""
            countryList += [(countryCode: code, countryName: name)]
        }
        
        return countryList
    }()
    
    func flag(country:String) -> String {
        let base : UInt32 = UInt32(Constants.values.countryFlagStartValue)
        var s = ""
        for v in country.unicodeScalars {
            s.unicodeScalars.append(UnicodeScalar(base + v.value)!)
        }
        return String(s)
    }
    
    func getCountryList() -> [CountryCodeModel] {
        
        var countryList: [CountryCodeModel] = []
        for (countryCode, countryName) in countries {
            var model = CountryCodeModel()
            model.countryCode = countryCode
            model.flag = flag(country: countryCode)
            model.phoneNumberCode = "+\(PhoneNumberCode.forCountryCode(code: countryCode))"
            model.countryName = countryName
            
            countryList.append(model)
        }
        
        return countryList.sorted {
            $0.countryCode < $1.countryCode
        }
    }
    
    func getTitleList() -> [String] {
        return [Constants.App.title.mr, Constants.App.title.mrs,  Constants.App.title.miss]
    }
}
