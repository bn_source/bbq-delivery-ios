//
//  LoginViewModel.swift
//  Barbeque Nation
//
//  Created by Rabindra L on 26/07/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

class LoginVerifyViewModel: BaseViewModel {
    
    private var verifyUserDataModel: VerifyUserDataModel?
    private var generateOtpDataModel: GenerateOTPDataModel?
    private var verifyOtpDataModel: VerifyOTPDataModel?
    private var verifyReferralDataModel: veriyReferralCodeModel?
    
    private var logoutUserDataModel:LogoutUserDataModel?
    
    private var otpIdForUser: String?
    
    //Existing User
    var userVerificationData: VerifyUserDataModel? {
        guard let userData = self.verifyUserDataModel else {
            return nil
        }
        return userData
    }
    
    //OTP ID
    var otpId: String {
        set {
            self.otpIdForUser = newValue
        } get {
            return self.otpIdForUser ?? "0"
        }
    }
    
    //Expiry Time
    var expiryTime: Int64 {
        guard let time = generateOtpDataModel?.expiryTime else {
            return 0
        }
        return time
    }
    
    //Message
    var optVerificationMessage: String {
        return verifyOtpDataModel?.responseMessage ?? ""
    }
    
    func formattedUserTitle(with titleText: String) -> String {
        let formattedString =  titleText.replacingOccurrences(of: ".",
                                                              with: "",
                                                              options: NSString.CompareOptions.literal,
                                                              range: nil)
        return formattedString
    }
}

extension LoginVerifyViewModel {
    
    func verifyUserPhoneNUmber(countryCode: String, mobileNumber: Int64, otpGenerate: Bool,
                               completion: @escaping (Bool) -> ()) {
        
        BBQLoginManager.verifyUser(countryCode: countryCode, mobileNumber: mobileNumber,
                                   otpGenerate: otpGenerate) { (model, result) in
            if result {
                self.verifyUserDataModel = model
                self.otpId = self.verifyUserDataModel?.otpId ?? "0"
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    func generateOTP(countryCode: String, mobileNumber: Int64, otpId: String, completion: @escaping (Bool) -> ()) {
        
        BBQLoginManager.generateOTP(countryCode: countryCode, mobileNumber: mobileNumber, otpId: otpId) { (model, result) in
            if result {
                self.generateOtpDataModel = model
                self.otpId = self.generateOtpDataModel?.otpId ?? "0"
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    func verifyOTP(otp: Int, otpId: String, completion: @escaping (Bool) -> ()) {
        BBQLoginManager.verifyOTP(otp: otp, otpId: otpId) { (model, result) in
            if result {
                self.verifyOtpDataModel = model
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    func verifyReferralCodeEntered(referralCode:String,completion: @escaping (Bool) -> ()){
        BBQLoginManager.verifyReferralCode(referralcode: referralCode) { (model, result) in
            if result {
                self.verifyReferralDataModel = model
                completion(true)
            } else {
                completion(false)
            }
        }
    }
}
