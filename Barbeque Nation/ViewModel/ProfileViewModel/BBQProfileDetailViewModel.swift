//
//  BBQProfileDetailViewModel.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 08/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import Foundation

class BBQProfileDetailViewModel {
    
    private let profileDetails: ProfileDataModel
    
    init(data: ProfileDataModel) {
        self.profileDetails = data
    }
    
    var mobileNumber: String {
        return profileDetails.mobileNumber ?? "0"
    }
    
    var emailId: String {
        return profileDetails.email ?? ""
    }
    
    var birthday: String {
        return profileDetails.dob ?? ""
    }
    
    var anniversary: String {
        return profileDetails.anniversaryDate ?? ""
    }
    
    var noticifationChoice: Bool {
        if profileDetails.newsletter == "0" {
            return false
        }
        return true
    }
    
    var corporateEmailId: String {
        return profileDetails.email ?? ""
    }
}
