//
//  BBQProfileViewModel.swift
//  Barbeque Nation
//
//  Created by Ajith CP on 06/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit
import FirebaseAnalytics

enum EntryType : Int, CaseIterable {
    case mobileNumber = 1
    case emailID
    case birthDay
    case anniversary
    case notificationsChoice
    case deleteAccount
    
    static var caseCount: Int { return self.allCases.count }
}

class BBQProfileViewModel: BaseViewModel {
    
    //MARK:- Properties
    private var tokenDataModel: TokenDataModel?
     var profileDataModel: ProfileDataModel?
    private var editedMobileNumber: String?
    private var editedEmailID: String?
    
    private var deleteMessage: String = ""
    private var selectedImage: UIImage?
    private var otpModel: GenerateOTPDataModel?
    
    var validPhoneNumber:(length:Int, code:String) = (10,"+91")
    
    var getOTPModel: GenerateOTPDataModel? {
        return self.otpModel
    }

    var responseMessage: String {
        set {
            self.deleteMessage = newValue
        } get {
            return self.deleteMessage
        }
    }
    
    var selectedProfileImage : UIImage? {
        set {
            self.selectedImage = newValue
        } get {
            return self.selectedImage
        }
    }
    
    var profileData : ProfileDataModel? {
        set {
            self.profileDataModel = newValue
        } get {
            return self.profileDataModel
        }
    }
    
    var updatedNumber : String? {
        set {
            self.editedMobileNumber = newValue
        } get {
            return self.editedMobileNumber ??
                ("\(self.profileData?.countryCode ?? "") \(self.profileDataModel?.mobileNumber ?? "0")")
        }
    }
    
    var updatedEmailID : String? {
        set {
            self.editedEmailID = newValue
        } get {
            return self.editedEmailID ?? "\(self.profileDataModel?.email ?? "")"
        }
    }
    
    
    func randomImageNameString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
    
    // MARK: - Datasource
    
    var isGuestUser : Bool {
        return BBQUserDefaults.sharedInstance.isGuestUser
    }
   
    class func formattedDateString(date: Date) -> String {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "dd/MMM/yyyy"
        let formattedDate = dateformatter.string(from: date)
        return formattedDate
    }
    
    func displayFormattedDateString(dateString: String, entryType: EntryType) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateFromString = dateFormatter.date(from: dateString)
       // dateFormatter.dateFormat = "dd/MMM/yyyy"
        if let date = dateFromString {
            return dateFormatter.string(from: date)
        }
        return entryType == .birthDay ? kAddBirthdayPlaceholder : kAddAnniversaryPlaceholder
    }
    
    func minimumDatePickerValue() -> Date {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "dd/MMM/yyyy"
        let minimumDate = dateformatter.date(from: "01/Jan/1950") // This is as per requirement.
        return minimumDate!
    }
    
    func requestFormmatedDateString(with dateString: String) -> String {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "dd-MM-yyyy"
        let formattedDate = dateformatter.date(from: dateString)
        if let date = formattedDate {
            return dateformatter.string(from: date)
        }
        return ""
    }
    
    func profileTableEntriesCount() -> Int {
        return EntryType.caseCount + 1
    }
    
    
    func profileEntriesDescription(for index: Int) -> (description: String, imageName: String , titleText: String) {
        switch index {
          
        case EntryType.emailID.rawValue:
            return (self.profileDataModel?.email ?? kAddEmailPlaceholder, Constants.AssetName.blackMailIcon, "ProfileMail")
            
        case EntryType.birthDay.rawValue:
            let formattedBDay = self.displayFormattedDateString(dateString: profileDataModel?.dob ?? "",
                                                                entryType: .birthDay)
            return (formattedBDay, Constants.AssetName.blackBirthdayIcon , "BirthDay")
            
        case EntryType.anniversary.rawValue:
            let formattedADay = self.displayFormattedDateString(dateString: profileDataModel?.anniversaryDate ?? "",
                                                                entryType: .anniversary)
            return (formattedADay, Constants.AssetName.blackAnniversaryIcon , "Anniversary")
            
        case EntryType.notificationsChoice.rawValue:
            return (kNotifications, Constants.AssetName.notificationIcon , "")
            
        case EntryType.deleteAccount.rawValue:
            return ("Delete Account", "BBQBin" , "")
            
        default:
            
            if let mobileNumber = self.profileDataModel?.mobileNumber, let countryCode = self.profileDataModel?.countryCode {
                return ("\(countryCode) \(mobileNumber)", Constants.AssetName.blackMobileIcon, "Mobile")
            }
            return (kAddAddMobilePlaceholder, Constants.AssetName.blackMobileIcon , "Mobile")
        }
    }
    
    func profileEditEntryData(for index: Int) -> (description: String, imageName: String)  {
        switch index {
            
        case EntryType.mobileNumber.rawValue:
            return (kAddAddMobilePlaceholder, Constants.AssetName.blackMobileIcon)
            
        case EntryType.emailID.rawValue:
            return (kAddEmailPlaceholder, Constants.AssetName.blackMailIcon)
            
        case EntryType.birthDay.rawValue:
            return (kAddBirthdayPlaceholder, Constants.AssetName.blackBirthdayIcon)
            
        default:
            return (kAddAnniversaryPlaceholder, Constants.AssetName.blackAnniversaryIcon)
        }
    }
    
    func isCalendarIconVisible(for index: Int) -> Bool {
        switch index {
        case 3, 4:
            return isGuestUser ? false : true
        default:
            return false
        }
    }
    
    func editTextFieldType(for fieldIndex: Int) -> EditFieldType {
        switch fieldIndex {
        case EntryType.mobileNumber.rawValue:
            return .mobileField
            
        case EntryType.emailID.rawValue:
            return .emailField
            
        case EntryType.birthDay.rawValue:
            return .birthdayField
            
        case EntryType.anniversary.rawValue:
            return .anniversaryField
            
        default:
            return .none
        }
    }
    
    func isCharacterEditableTextFields(for fieldType: EditFieldType) -> Bool {
        switch fieldType {
        case .mobileField, .emailField:
            return true
        default:
            return true
        }
    }
    
    func isValidPhoneNumber(number: NSString) -> Bool {
        if BBQPhoneNumberValidation.isValidNumber(number: number as String,
                                                  for: BBQUserDefaults.sharedInstance.phoneCode) {
            validPhoneNumber.code = BBQUserDefaults.sharedInstance.phoneCode
            validPhoneNumber.length = number.length
        } else {
            return String(number).isValidMobileNumber(countryCode: validPhoneNumber.code,
                                                 length: validPhoneNumber.length)
        }
        return true
    }
    
    func isValidEmailAddress(emailText: String) -> Bool {
        return emailText.isValidEmailID()
    }
    
    func isFormValidated(mobile: String, email: String) -> Bool {
        let isMobileValid = self.isValidPhoneNumber(number: NSString(string: mobile))
        let isEmailValid  = self.isValidEmailAddress(emailText: email)
        if isMobileValid && isEmailValid {
            return true
        }
        return false
    }
    
    func keyBoardType(for fieldIndex: Int) -> UIKeyboardType {
        switch fieldIndex {
        case EntryType.mobileNumber.rawValue:
            return .phonePad
        case EntryType.emailID.rawValue:
            return .emailAddress
        default:
            return .numberPad
        }
    }

    func profilePointsTitleString(isAnimated: Bool) -> String {
        return isAnimated ? kBBQPoints : kPoints
    }
    
    
    func profileNameLabelString() -> String  {
        return isGuestUser ? kStringGuest : self.profileDataModel?.name ?? ""
    }
    
    func profileTitleLabelString() -> String  {
        if isGuestUser {
            return kStringGuestProfile
        } else if let userName = self.profileDataModel?.name {
                let firstName = userName.components(separatedBy: " ").first ?? ""
                return "\(firstName)\(kUserProfileFormatted)"
        }
        return ""
    }
    
    func addProfileImage() -> UIImage {
        return (isGuestUser ? UIImage(named: Constants.AssetName.emptyAvtarTwo) :
            UIImage(named: Constants.AssetName.emptyAvtarOne)) ?? UIImage()
    }
    
    func addProfileImageButton() -> (isEnabled: Bool, textColor: UIColor) {
        if isGuestUser {
            return (false, UIColor.gray)
        } else {
            return (true, UIColor.menuOrangeColor)
        }
    }
    
    func editButton(isEditMode: Bool) -> (image: UIImage, editTitle: String, isEnabled: Bool, color: UIColor) {
        if isEditMode {
            return (UIImage(named: Constants.AssetName.orangeTickIcon) ?? UIImage(),
                    kStringUpdate,
                    true, UIColor.menuOrangeColor)
        } else {
            let editButtonImage = (isGuestUser ? UIImage(named: Constants.AssetName.editGreyIcon) :
                UIImage(named: Constants.AssetName.profileEditIcon)) ?? UIImage()
            let editButtonColor = isGuestUser ? UIColor.gray : UIColor.menuOrangeColor
            let isEnabled = isGuestUser ? false : true
            return (editButtonImage, kStringEdit, isEnabled, editButtonColor)
        }
    }
    
    func profileDetailViewModel() -> BBQProfileDetailViewModel? {
        guard let viewModel = self.profileDataModel else {
            return nil
        }
        let detailViewModel = BBQProfileDetailViewModel(data: viewModel)
        return detailViewModel
    }

}

//MARK:- Service call handler
extension BBQProfileViewModel {
    
    func getUserProfile(completion: @escaping (Bool) -> ()) {
        BBQUserManager.getUser { [weak self] (model, result) in
            if let self = self {
                if result {
                    self.profileDataModel = model
                    BBQUserDefaults.sharedInstance.UserLoyaltyPoints = Int(model?.loyaltyPoints ?? 0)
                    if let model = model {
                        SharedProfileInfo.shared.loadSharedProfileInfo(profileInfo: model)
                    }
                    completion(true)
                } else {
                    completion(false)
                }
            }
        }
    }
    
    func updateUser(title: String, newsLetter: String, maritalStatus: String, name: String,
                    dob: String, anniversary: String,  isNotificationEnabled: Bool,
                    completion: @escaping (Bool) -> ()) {
        
        BBQUserManager.updateUser(title: title, newsLetter: newsLetter, maritalStatus: maritalStatus,
                                  name: name, dob: dob, anniversary: anniversary,
                                  isNotificationEnabled: isNotificationEnabled) { [weak self] (responseMessage, result) in
            if let self = self {
                if result {
                    self.responseMessage = responseMessage ?? ""
                    completion(true)
                } else {
                    completion(false)
                }
            }
        }
    }
    
    //Delete is not used as of now. But lets keep it for any future use.
    func deleteUser(otp: Int, otpId: Int, completion: @escaping (Bool) -> ()) {
        BBQUserManager.deleteUser(otp: otp, otpId: otpId) { [weak self] (responseMessage, result) in
            if let self = self {
                if result {
                    self.responseMessage = responseMessage ?? ""
                    completion(true)
                } else {
                    completion(false)
                }
            }
        }
    }
    
    func updateUserMobile(countryCode: String,
                          mobileNumber: Int64,
                          otpId: Int,
                          otp: Int, completion: @escaping (Bool) -> ()) {
        BBQUserManager.updateUserMobile(countryCode: countryCode,
                                        mobileNumber: mobileNumber,
                                        otpId: otpId,
                                        otp: otp) { (result) in
            result == true ? completion(true): completion(false)
        }
    }
    
    func  generateEmailOTP(emailID: String, completion: @escaping (Bool) -> ()) {
        BBQUserManager.generateEmailOTP(emailID: emailID) { (model, result) in
            self.otpModel = model
            result == true ? completion(true): completion(false)
        }
    }
    
    func updateUserEmail(email: String, otpID: Int, otp: Int,
                         completion: @escaping (Bool) -> ()) {
        BBQUserManager.updateUserEmail(email: email, otpID: otpID, otp: otp) { (result) in
            result == true ? completion(true): completion(false)
        }
    }
    
    func updateProfileImage(selectedImage: UIImage, imgExtension: String?, completion: @escaping (Bool) -> ()) {
        // Generating random file name along with extension to save it in back end.
        let randomFileName = self.randomImageNameString(length: 10) + (imgExtension ?? "")
        BBQUserManager.updateUserProfileImage(selectedImage: selectedImage, fileName: randomFileName) { (result) in
            result == true ? completion(true): completion(false)
        }
    }
    
    func getUserLocation(from lattitude: String, longtitude: String,
                         completion: @escaping (String) -> ()) {
        BBQUserManager.getUserLocation(from: lattitude, longtitude: longtitude) { (locationText) in
            completion(locationText)
        }
    }
    
}
