//
 //  Created by Ajith CP on 13/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQMobileVerifierViewModel.swift
 //

import UIKit

class BBQMobileVerifierViewModel: NSObject {
    
    private var otpID: String = "0"
    private var countryCode : String?
    private var mobileNumber : String?
    
    var otpDataModel : GenerateOTPDataModel?
    
    var otpIdentifier: String {
        set {
            self.otpID = newValue
        } get {
            return self.otpID
        }
    }
    
    func verifiedMobileNumberString() -> String {
        return "\(self.countryCode!) \(self.mobileNumber!)"
    }
    
    func generateMobileOTP(countryCode: String,
                           mobileNumber: Int64,
                           otpId: String,
                           completion: @escaping (Bool) -> ()) {
       
        self.countryCode = countryCode
        self.mobileNumber = String(mobileNumber)
        
        BBQLoginManager.generateOTP(countryCode: countryCode,
                                    mobileNumber: mobileNumber,
                                    otpId: otpId) { (model, result) in
            if result {
                self.otpDataModel = model
                self.otpIdentifier = model?.otpId ?? self.otpDataModel?.otpId ?? "0"
                completion(true)
            } else {
                completion(false)
            }
        }
    }

    func verifyUser(countryCode: String, mobileNumber: Int64, otpGenerate: Bool,
                    completion: @escaping (Bool) -> ()) {
        
        BBQLoginManager.verifyUser(countryCode: countryCode, mobileNumber: mobileNumber, otpGenerate: otpGenerate) { (model, result) in
            if result {
                AnalyticsHelper.shared.triggerEvent(type: .verify_user_success_value)
                completion(model?.isExistingUser ?? false)
            } else {
                AnalyticsHelper.shared.triggerEvent(type: .verify_user_failure_value)
                completion(false)
            }
        }
    }
}
