//  BBQLocationViewModel.swift
/*
 *  Created by Abhijit Singh on 20/08/19.
 *  Copyright © BARBEQUE NATION HOSPITALITY LIMITED 2019. All rights reserved.
 *  Last modified 03/09/19
 
 * CHANGE LOG
 * —------------------------------------------------------------------------------
 * ID          Bug ID      Author Name          Date                  Description
 * —------------------------------------------------------------------------------
 *
 */

import Foundation

class BBQLocationViewModel: BaseViewModel {
    
    var modelData: LocationResponse?
    var homeViewController = BBQHomeViewController()
    
    var cityList:[Cities] {
        get{
            var citylist = [Cities]()
            if let model = modelData?.bcitydata {
                for city in model {
                    citylist.append(city)
                }
            }
            return citylist
        }
    }
    
    var citiesArray:[Cities] = []
    var branchesarray:[Branches] = []
    var cityArray:[CitiesData] = [
        
        CitiesData(cityId:1, cityName: kKolkata ),
        CitiesData(cityId:2, cityName: kBangalore ),
        CitiesData(cityId:3, cityName: kMumbai ),
        CitiesData(cityId:4, cityName: kNewDelhi ),
        CitiesData(cityId:5, cityName: kChennai ),
        CitiesData(cityId:6, cityName: kGoa )]
    
    var outletsArray : [OutletData] = [OutletData(outletId: 1, outletName: kJPNagar),
                                       OutletData(outletId: 2, outletName: kWhiteField),
                                       OutletData(outletId: 3, outletName: kBallygunge),
                                       OutletData(outletId: 4, outletName: kEsplanade),
                                       OutletData(outletId: 5, outletName: kWhiteField),
                                       OutletData(outletId: 6, outletName: kJPNagar),
                                       OutletData(outletId: 7, outletName: kJPNagar),
                                       OutletData(outletId: 8, outletName: kJPNagar)]
}

extension BBQLocationViewModel {
    func getOutlet(for city:String) -> [Branches] {
        var outletList = [Branches]()
        
        if let model = modelData?.bcitydata {
            for outletDetails in model {
                if outletDetails.city_name?.capitalized == city.capitalized {
                    if let branches = outletDetails.branches {
                        for branch in branches {
                            outletList.append(branch)
                        }
                    }
                }
            }
        }
        
        return outletList
    }
    
    func getCities(lat: Float, long: Float, completion: @escaping (Bool)-> ()) {
        
        BBQHomeManager.latLong(latitude: lat, longitude: long) { (model, result) in
            if result {
//                print(model?.bcitydata as Any)
                
                self.modelData = model
                
                if let cities =  model?.bcitydata{
                    self.citiesArray = cities
                }
                if let branches = model?.near_by{
                    self.branchesarray = branches
                }
                completion(true)
            } else {
                completion(false)
            }
        }
    }
}

