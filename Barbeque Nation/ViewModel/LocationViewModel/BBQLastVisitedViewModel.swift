//
//  Created by Abhijit on 23/09/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQLastVisitedViewModel.swift
//

import Foundation


class BBQLastVisitedViewModel:BaseViewModel{
   
    var bbqLastVisitedModel = BBQLastVisitedBranchModel()
    
    func getLastVisitedBranch(id:String, completion: @escaping (Bool) -> ()) {
        BBQLastVisitedBranchManager.getLastVistedBranch(branchId: id){ (data, error) in
            if error == nil {
                do {
                    let decoder = JSONDecoder()
                    guard let responseData = data else {
                        DispatchQueue.main.async {
                            completion(false)
                        }
                        return
                    }
                    self.bbqLastVisitedModel = try decoder.decode(BBQLastVisitedBranchModel.self, from: responseData)
                    
                    DispatchQueue.main.async {
                        completion(true)
                    }
                } catch {
                    DispatchQueue.main.async {
                        completion(false)
                    }
                }
            } else {
                if  BBQLastVisitedBranchManager.networkResponse != nil, BBQLastVisitedBranchManager.networkResponse?.statusCode == 400 {
                    BBQLastVisitedBranchManager.configureErrorModel(data: data)
                }
                DispatchQueue.main.async {
                    completion(false)
                }
            }
        }
    }
    
    
}
