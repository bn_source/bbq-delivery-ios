//
 //  Created by Abhijit on 12/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQOutletInfoViewModel.swift
 //

import Foundation


class BBQOutletInfoViewModel:BaseViewModel{
    
    private var outletInfoResponse: OutletInfoModel?
    var outletInfoData:OutletInfoModel?{
        return outletInfoResponse ?? nil
    }
    private var menuBranchBuffetResponse: MenuBranchBuffetDataModel?
    var menuBranchBuffetData:MenuBranchBuffetDataModel?{
        return menuBranchBuffetResponse ?? nil
    }
    private var BranchBuffetMenu:BranchBuffetDataModel?
    public func totalBuffetData() -> [BuffetData] {
        // Adding beverages data along with buffet data. Since it is having different ressponse structure
        // creating models separetly and appendinf with original datasource.
        if var totalBuffets = self.menuBranchBuffetResponse?.buffets?.buffetData {
            if let beveragesData = self.menuBranchBuffetResponse?.beverageData, beveragesData.count > 0 {
                var buffetItems = [BuffetItems]()
                var buffetData = BuffetData()
                buffetData.buffetName = "Beverages"
                for item in beveragesData {
                    if let menuItems = item.menuItems {
                        var buffetItem = BuffetItems()
                        buffetItem.buffetItems_name = item.menuTitle
                        buffetItem.buffetItems_menuItems = menuItems
                        buffetItems.append(buffetItem)
                        buffetData.buffetItems = buffetItems

                    }
                }
                totalBuffets.append(buffetData)
            }
            return totalBuffets
        }
        return [BuffetData]()
    }
    
}

extension BBQOutletInfoViewModel{
    
    func getOutletInfo(branchId: String, completion:@escaping(Bool)->()){
        BBQOutletInfoManager.outletInfo(branchId: branchId) { (model, result) in
            if result! {
                self.outletInfoResponse = model
                completion(true)
            }
            else {
                completion(false)
            }
        }
    }
    // MARK: - getmenubranchBuffet
    func getmenubranchBuffet(branchId: String, reservationDate: String, slotId: Int, completion: @escaping (Bool)-> ())
    {
        BBQBookingManager.getmenubranchBuffet(branchId: branchId, reservationDate: reservationDate, time: "",
                                              slotId: slotId, isEarlyBird: false,
                                              isLateBird: false){ (model, result) in
            if result {
               self.BranchBuffetMenu = model
                completion(true)
            
            } else {
                completion(false)
            }
        }
        
    }
    
}
