//
//  HomeViewModel.swift
//  Barbeque Nation
//
//  Created by Maya R on 10/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import Foundation

import UIKit
import Alamofire



class HomeViewModel: BaseViewModel {
    
    private var homeDataModel: BBQHomeDataModel?
    private var homeLocationDataModel: HomeLocationModel?
    
    private var upcomingReservationDataModel: UpcomingReservationModel?
    
    private var promotionsData : [PromotionModel]?
    private var vouchersData : [VouchersModel]?
    private var reservationData : [UpcomingBookingModel]?
    private var nearbyLocationData : [NearbyOutelt]?
    private var locationDataModel: LocationResponse?
    
    var isForceUpdate : Bool = false
    var isRecommendUpdate : Bool = false
    var isUpdateRequired : Bool = false
    
    func getPromotionsDataData()->[PromotionModel]{
        var promosArray = [PromotionModel]()
        if(promotionsData != nil){
            promosArray = promotionsData!
        }
        return promosArray
    }
    
    
    func getVouchersData()->[VouchersModel]{
        var vouchArray = [VouchersModel] ()
        if(vouchersData != nil){
            vouchArray = vouchersData!
        }
        return vouchArray
        
    }
    
    
    func getReservationsData()->[UpcomingBookingModel]{
        var reserveArray = [UpcomingBookingModel] ()
        if(reservationData != nil){
            reserveArray = reservationData!
        }
        return reserveArray
    }
    
    func getNearbyLocationData()->[NearbyOutelt]{
        var nearArray = [NearbyOutelt] ()
        if(nearbyLocationData != nil){
            nearArray = nearbyLocationData!
            for nearDict in nearArray {
                var nearObj = NearbyOutelt()
                nearObj.nearbyBranchId = nearDict.nearbyBranchId
                nearObj.nearbyBranchName = nearDict.nearbyBranchName
                nearObj.nearbyBranchLatitude = nearDict.nearbyBranchLatitude
                nearObj.nearbyBranchLatitude = nearDict.nearbyBranchLatitude
                nearObj.nearbyBranchLongitude = nearDict.nearbyBranchLongitude
                nearObj.nearbyCityCode = nearDict.nearbyCityCode
                nearObj.nearbyCityName = nearDict.nearbyCityName
                
            }
            
        }
        return nearArray
        
    }
}

extension HomeViewModel{
    
    func getAppupdateInformation(completion: @escaping (_ result: Bool)->()) {
        if let appVersion = UIApplication.appVersion {
            let deviceType = "IOS"
            BBQUserManager.getAppUpdateInformation(appVersion: appVersion,
                                                   deviceType: deviceType) { (model, isResult) in
                if isResult {
                    self.isForceUpdate = model?.isForceUpdate ?? false
                    self.isRecommendUpdate = model?.isRecommendedUpdate ?? false
                    self.isUpdateRequired = self.isForceUpdate  || self.isRecommendUpdate
                    completion(self.isUpdateRequired)
                } else {
                    self.isUpdateRequired = false
                    completion(self.isUpdateRequired)
                }
            }
        } else {
            completion(false)
        }
    }
    
    //MARK : getPromosVouchers
    func getPromosVouchers(latitide: Double,
                           longitude: Double,
                           brach_ID: String,
                           completion: @escaping (Bool)-> ()){
        BBQHomeManager.getVouchersAndPromotions(latitude: latitide, longitude: longitude, brachID: brach_ID) { (data, error) in
            if error == nil {
                do {
                    let decoder = JSONDecoder()
                    guard let responseData = data else {
                        DispatchQueue.main.async {
                            completion(false)
                        }
                        return
                    }
                    self.homeDataModel = try decoder.decode(BBQHomeDataModel.self, from: responseData)
                    
                    self.promotionsData = self.homeDataModel?.promotions
                    self.vouchersData = self.homeDataModel?.vouchers
                    
                    DispatchQueue.main.async {
                        completion(true)
                    }
                } catch {
                    DispatchQueue.main.async {
                        completion(false)
                    }
                }
            } else {
                if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse?.statusCode == 400 {
                    BBQUserManager.configureErrorModel(data: data)
                }
                DispatchQueue.main.async {
                    completion(false)
                }
            }
        }
    }
    
    //MARK:getUpcoming reservation
    func getUpcomingReservations(completion:@escaping (Bool)->()){
        BBQHomeManager.getupcomingReservations { (data, error) in
            if error == nil {
                do {
                    let decoder = JSONDecoder()
                    guard let responseData = data else {
                        DispatchQueue.main.async {
                            completion(false)
                        }
                        return
                    }
                    
                    self.upcomingReservationDataModel = try decoder.decode(UpcomingReservationModel.self, from: responseData)
                    
                    self.reservationData = self.upcomingReservationDataModel?.upcoming_reservations
                    DispatchQueue.main.async {
                        completion(true)
                    }
                }
                catch {
                    DispatchQueue.main.async {
                        completion(false)
                    }
                }
            } else {
                if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse?.statusCode == 400 {
                    BBQUserManager.configureErrorModel(data: data)
                }
                DispatchQueue.main.async {
                    completion(false)
                }
            }
        }
    }
    
    //MARK:getNearbyOutelts
    
    func getNearbyOutlets(latitide: Double,
                          longitude: Double,
                          completion: @escaping (Bool)-> ()){
        BBQHomeManager.getNearbyOutletDetails(latitude: latitide, longitude: longitude) { (data, error) in
            if error == nil {
                do {
                    let decoder = JSONDecoder()
                    guard let responseData = data else {
                        DispatchQueue.main.async {
                            completion(false)
                        }
                        return
                    }
                    
                    /*
                     if let data = data, let json = try JSONSerialization.jsonObject(with: data) as? [String: Any]{
                         let brands = BrandsData(JSON: json)
                     */
                    self.homeLocationDataModel = try decoder.decode(HomeLocationModel.self, from: responseData)
                    print(self.homeLocationDataModel!)
                    if (self.homeLocationDataModel?.near_by_Outlets!.count ?? 0) > 0 {
                        self.nearbyLocationData = self.homeLocationDataModel?.near_by_Outlets
                        currentUserCity = self.nearbyLocationData?.first?.nearbyCityName ?? ""
                    }
                    DispatchQueue.main.async {
                        completion(true)
                    }
                } catch {
                    DispatchQueue.main.async {
                        completion(false)
                    }
                }
            } else {
                if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse?.statusCode == 400 {
                    BBQUserManager.configureErrorModel(data: data)
                }
                DispatchQueue.main.async {
                    completion(false)
                }
            }
        }
    }
    
    func getNearbyOutletsV1(latitide: Double,
                            longitude: Double,
                            isInitialLoad: Bool = false,
                            completion: @escaping (Bool)-> ()){
        BBQHomeManager.getNearbyOutletDetailsV1(latitude: latitide, longitude: longitude) { (data, error) in
            if error == nil {
                do {
                    if let data = data, let json = try JSONSerialization.jsonObject(with: data) as? [String: Any]{
                        let brands = BrandsData(JSON: json)
                        if isInitialLoad{
                            savedResponseOfBranches = brands
                        }
                        brandlistForHomePage.removeAll()
                        brandlistForHomePage.append(BrandInfo(id: "", name: "Barbeque\nNation", logo: "https://gvgc.bnhl.in/delivery/bbqnewlogo.jpg", colorCode: "#FF6301|#FFFFFF"))
                        for brand in brands?.brands ?? [BBQBrands](){
                            brandlistForHomePage.append(BrandInfo(brand: brand))
                        }
                        DispatchQueue.main.async {
                            completion(true)
                        }
                    }
                } catch {
                    DispatchQueue.main.async {
                        completion(false)
                    }
                }
            } else {
                if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse?.statusCode == 400 {
                    BBQUserManager.configureErrorModel(data: data)
                }
                DispatchQueue.main.async {
                    completion(false)
                }
            }
        }
    }
    
}

class VersionCheck {
  
  public static let shared = VersionCheck()
  
  func isUpdateAvailable(callback: @escaping (Bool ,String, String , String)->Void) {
    let bundleId = Bundle.main.infoDictionary!["CFBundleIdentifier"] as! String
    Alamofire.request("https://itunes.apple.com/lookup?bundleId=\(bundleId)").responseJSON { response in
      if let json = response.result.value as? NSDictionary, let results = json["results"] as? NSArray, let entry = results.firstObject as? NSDictionary, let versionStore = entry["version"] as? String, let versionLocal = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ,  let releaseNotes = entry["releaseNotes"] as? String {
        let arrayStore = versionStore.split(separator: ".").compactMap { Int($0) }
        let arrayLocal = versionLocal.split(separator: ".").compactMap { Int($0) }

        if arrayLocal.count != arrayStore.count {
          callback(true, versionLocal, versionStore , releaseNotes) // different versioning system
          return
        }

        // check each segment of the version
        for (localSegment, storeSegment) in zip(arrayLocal, arrayStore) {
          if localSegment < storeSegment {
            callback(true ,versionLocal, versionStore, releaseNotes)
            return
          }
        }
      }
      callback(false, "" , "" , "") // no new version or failed to fetch app store version
    }
  }
  
}

struct AppVersionInformation {
    
    var localVersion: String?
    var appStoreVersion: String?
    var releaseNotes: String?
    var hasUpdate : Bool = false

    init(localV: String , appStoreV: String , notes: String , needToUpdate: Bool) {
        
        localVersion = localV
        appStoreVersion = appStoreV
        releaseNotes = notes
        hasUpdate = needToUpdate
        
    }
}
class GlobalAppVersion {

    // Now sharedAppInformaion is your singleton,
    static let sharedAppInformaion = GlobalAppVersion()

    var informationAboutVersion : AppVersionInformation?

}







