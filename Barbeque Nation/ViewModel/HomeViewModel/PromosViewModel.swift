//
//  PromosViewModel.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 13/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

class PromosViewModel {
    
    //Populate the real data in this array
    var promosArray: [PromosData] = []
    
    
    init(promosDataArray: [PromotionModel]) {
        for promosData in promosDataArray {
            let id = String(format: "%d", promosData.promotionId ?? 0)
            let promoData = PromosData(promoId: id, promoTitle: promosData.promotion_title ?? "", promoDescription: promosData.promotion_description ?? "", promoImage: promosData.promotion_image ?? "", promotion_url: promosData.promotion_url ?? "")
            promosArray.append(promoData)
        }
    }
    

}
