//
 //  Created by Chandan Singh on 05/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified VoucherDetailsViewModel.swift
 //

import Foundation

class VoucherDetailsViewModel: BaseViewModel {
    
    private var voucherDetails: VoucherDetails?
    private var promotionDetails: PromotionDetailsModel?
    
    var voucherDetailsData: VoucherDetails? {
        return voucherDetails ?? nil
    }
    
    var promotionDetailsData: PromotionDetailsModel? {
        return promotionDetails ?? nil
    }
}

extension VoucherDetailsViewModel {
    
    func getVoucherDetails(voucherId: Int, completion: @escaping (Bool)-> ()){
        BBQHomeManager.getVoucherDetails(voucherId: String(voucherId)) { (model, result) in
            if result {
                self.voucherDetails = model
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    func getPromotionDetails(promotionId: Int, completion: @escaping (Bool)-> ()){
        BBQHomeManager.getPromotionDetails(promotionId: String(promotionId)) { (model, result) in
            if result {
                self.promotionDetails = model
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    func promoDetails() -> (title:String, description: String, image: String) {
        return (self.promotionDetails?.promotionTitle ?? "",
                self.promotionDetails?.promotionDescription ?? "",
                self.promotionDetails?.promotionImage ?? "")
    }
}


extension VoucherDetailsViewModel {
    
    // Copied logic from existing class
    // TODO: Refactor with constannts
    public func getPromoDetailsSections() -> [[String:String]] {
        
        var sectionArray = [[String:String]]()

        if let policy = self.promotionDetails?.privacyAndPolicy, policy != "" {
            sectionArray.append(["Policy":policy])
        }
        var timingString = ""
        if let timings = self.promotionDetails?.promotionTimings, timings.count > 0 {
            for obj in timings {
                if let day = obj.day, let time = obj.timings, day != "", time != "" {
                    timingString =  timingString + day + "\t" + time + "\n"
                }
            }
            if timingString != "" {
                 let trimmed = timingString.trimmingCharacters(in: .whitespacesAndNewlines)
                 sectionArray.append(["Timings":trimmed])
            }
        }

        if let validOn = self.promotionDetails?.promotionNotValidOn, validOn != "" {
            sectionArray.append(["Not valid on":validOn])
        }

        if let validity = self.promotionDetails?.promotionValidity, validity != "" {
            sectionArray.append(["Valid till":validity])
        }
        var regionString = ""
        if let promotionRegion = self.promotionDetails?.promotionRegion, promotionRegion.count > 0 {
            for region in promotionRegion {
                regionString =  regionString + region + "\n"
            }
            
            if regionString != "" {
                let trimmed = regionString.trimmingCharacters(in: .whitespacesAndNewlines)
                sectionArray.append(["Applicable at":trimmed])
            }
        }
        if let promoTimigs = self.promotionDetails?.promotionRegionTiming, promoTimigs != "" {
            sectionArray.append(["Promotion region timing":promoTimigs])
        }
        if let thingsToRemember = self.promotionDetails?.thingsToRemember, thingsToRemember != "" {
            sectionArray.append(["Things to remember":thingsToRemember])
        }

        return sectionArray
    }
    
    public func getPrivacyAndPolicyData() -> [[String:String]] {
        var sectionArray = [[String:String]]()
        if let policy = self.promotionDetails?.privacyAndPolicy, policy != "" {
            sectionArray.append(["Policy":policy])
        }
        return sectionArray
    }
}



