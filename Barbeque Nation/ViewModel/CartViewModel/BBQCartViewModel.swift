//
//  Created by Chandan Singh on 08/09/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQCartViewModel.swift
//

import Foundation

//Cart calculation initial values
struct CartCalculationsData {
    var isCouponAdded = false
    var couponName = ""
    var couponBarcode = ""
    var couponPrice = 0.0
    
    var pointApplied = false
    var applicablePoint = 0
    var applicableAmount = 0.00
    var totalPoint = BBQUserDefaults.sharedInstance.UserLoyaltyPoints
    var isPointEnabled = false
    
    var extimatedTotalPrice: String = "0"
}

class CartViewModel: BaseViewModel {
    
    //private var addCartModel: ModifyCartModel? = nil
    private var modifyCartModel: ModifyCartModel? = nil
    var vouchersAndCouponsModel: VoucherCouponsModel? = nil
    private var pointsValueModel: BBQPointsValueModel? = nil
    private var pointsCountModel: BBQPointsCountModel? = nil
    
//    var getAddCartModel: ModifyCartModel? {
//        return addCartModel
//    }
    
    var getModifyCartModel: ModifyCartModel? {
        return modifyCartModel
    }
    
    var getPointValueModel: BBQPointsValueModel? {
        return pointsValueModel
    }
    
    var getPointCountModel: BBQPointsCountModel? {
        return pointsCountModel
    }
    
    var getAllCoupons: [VoucherCouponsData] {
        var couponsArray = [VoucherCouponsData]()
        
        guard let model = self.vouchersAndCouponsModel, let data = model.couponsAndVouchers else {
            return couponsArray
        }
        
        for voucherCouponsData in data {
            if voucherCouponsData.voucherType?.trimmingCharacters(in: .whitespaces).lowercased().contains("GC".lowercased()) ?? false {
                couponsArray.append(voucherCouponsData)
            }
        }
        return couponsArray
    }
    
    var getActiveCoupons: [VoucherCouponsData] {
        var couponsArray = [VoucherCouponsData]()
        
        guard let model = self.vouchersAndCouponsModel, let data = model.couponsAndVouchers else {
            return couponsArray
        }
        
        for voucherCouponsData in data {
            if voucherCouponsData.voucherType?.trimmingCharacters(in: .whitespaces).lowercased().contains("GC".lowercased()) ?? false,
                voucherCouponsData.status?.lowercased() == "Active".lowercased() {
                couponsArray.append(voucherCouponsData)
            }
        }
        return couponsArray
    }
}

//MARK: Service Call Handler
extension CartViewModel {
    func addToCart(productId: Int, quantity: Int, completion: @escaping (Bool)->()) {
        BBQCartManager.addToCart(productId: productId, quantity: quantity) { (model, result) in
            if result {
                //Assign to model
                self.modifyCartModel = model
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    func getCart(completion: @escaping (Bool)->()) {
        BBQCartManager.getCart() { (model, result) in
            if result {
                //Assign to model
                self.modifyCartModel = model
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    func updateOrderItem(orderId: String, orderItem: String, quantity: Int, completion: @escaping (Bool)->()) {
        BBQCartManager.updateOrderItem(orderId: orderId, orderItem: orderItem, quantity: quantity) { (model, result) in
            if result {
                //Assign to model
                self.modifyCartModel = model
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    func deleteOrderItem(orderId: String, orderItemId: String, completion: @escaping (Bool)->()) {
        BBQCartManager.deleteOrderItem(orderId: orderId, orderItemId: orderItemId) { (model, result) in
            if result {
                //Assign to model
                self.modifyCartModel = model
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    func emptyCartFor(cartID: String, completion: @escaping (Bool)->()) {
        BBQCartManager.emptyCartFor(cartID: cartID) { (isEmptied) in
            completion(isEmptied)
        }
    }
    
    func getVoucherAndCoupons(completion: @escaping (Bool)->()) {
        //updated with new api to get the coupons
        BBQVouchersCouponsManager.getPurchasedCouponsInCart{(model, result)in
            if result {
                //Assign to model
                if var modelObj = model?.couponsAndVouchers{
                    modelObj.sort(by: { i1, i2 in
                        return i1.voucherApplicability ?? false
                    })
                    self.vouchersAndCouponsModel?.couponsAndVouchers = modelObj
                }
                self.vouchersAndCouponsModel = model
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    func getLoyaltyPointsValue(amount: String, currency: String, completion: @escaping (Bool)->()) {
        BBQCartManager.getLoyaltyPointsValue(amount: amount, currency: currency) { (model, result) in
            if result {
                //Assign to model
                self.pointsValueModel = model
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    func getLoyaltyPointsCount(completion: @escaping (Bool)->()) {
        BBQCartManager.getLoyaltyPointsCount { (model, result) in
            if result {
                //Assign to model
                self.pointsCountModel = model
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    func getItemQuantity(id: Int) -> Int {
        if let modifyCartModel = modifyCartModel, let orderItems = modifyCartModel.orderItems{
            for item in orderItems{
                if let productId = item.productId, productId == id{
                    return item.quantity ?? 0
                }
            }
        }
        return 0
    }
}
