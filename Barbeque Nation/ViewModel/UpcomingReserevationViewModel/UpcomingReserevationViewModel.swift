//
 //  Created by Mahmadsakir on 14/12/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified UpcomingReserevationViewModel.swift
 //

import Foundation
import UIKit
import SafariServices
import PDFKit

class UpcomingReserevationViewModel: NSObject, UpcomingReserevationDelegate{
    
    var homeVC: BBQHomeViewController?
    var currency: String = ""
    var upcomingBooking: UpcomingBookingModel?
    
    func onClickBtnCab(cell: BBQUpcomingReservationCell, upcomingBooking: UpcomingBookingModel) {
        AnalyticsHelper.shared.triggerEvent(type: .H14C)
        if let latitude = upcomingBooking.latitude, let longitude = upcomingBooking.longitude{
            let directionBottomSheet = Bundle.main.loadNibNamed("BookingRedirectionBottomSheet", owner: nil)!.first as! BookingRedirectionBottomSheet
            directionBottomSheet.latitude =  latitude
            directionBottomSheet.longitude =  longitude
            directionBottomSheet.bottomSheetType = .Cab
            directionBottomSheet.view.maskByRoundingCorners(cornerRadius: 10, maskedCorners: CornerMask.topCornersMask)
            if UIApplication.shared.canOpenURL(URL(fileURLWithPath: "uber://")) {
                directionBottomSheet.isUberAvailable = true
            }
            if UIApplication.shared.canOpenURL(URL(fileURLWithPath: "olacabs://")) {
                directionBottomSheet.isOlaAvailable = true
            }
            if let homeVC = homeVC {
                homeVC.bottomSheetController!.present(directionBottomSheet, on: homeVC)
            }
        }
    }
    
    func onClickBtnNavigation(cell: BBQUpcomingReservationCell, upcomingBooking: UpcomingBookingModel) {
        AnalyticsHelper.shared.triggerEvent(type: .H14B)
        if let latitude = upcomingBooking.latitude, let longitude = upcomingBooking.longitude{
            let directionBottomSheet = Bundle.main.loadNibNamed("BookingRedirectionBottomSheet", owner: nil)!.first as! BookingRedirectionBottomSheet
            directionBottomSheet.latitude =  latitude
            directionBottomSheet.longitude =  longitude
            directionBottomSheet.bottomSheetType = .Direction
            directionBottomSheet.view.maskByRoundingCorners(cornerRadius: 10, maskedCorners: CornerMask.topCornersMask)
            if(UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
                directionBottomSheet.isGoogleMapsAvailable = true
            }
            if let homeVC = homeVC {
                homeVC.bottomSheetController!.present(directionBottomSheet, on: homeVC)
            }
        }
    }
    
    func onClickBtnCall(cell: BBQUpcomingReservationCell, upcomingBooking: UpcomingBookingModel) {
        AnalyticsHelper.shared.triggerEvent(type: .H14D)
        if let phoneNumber = upcomingBooking.branchContactNumber{
            homeVC?.callNumber(phone: phoneNumber)
        }
    }
    
    func onClickBtnRestaurantInfo(cell: BBQUpcomingReservationCell, upcomingBooking: UpcomingBookingModel) {
        let controller = UIStoryboard.loadOutletInfoViewController()
        controller.selectedBranchId = upcomingBooking.branchId
        controller.view.layoutIfNeeded()
        homeVC?.navigationController?.pushViewController(controller, animated: true)
    }
    
    func onClickBtnPayNow(cell: BBQUpcomingReservationCell, upcomingBooking: UpcomingBookingModel) {
        // pusing the booking history storyboard
        AnalyticsHelper.shared.triggerEvent(type: .H14E)
//        let  bookingHistoryController = UIStoryboard.loadReservationViewController()
//        //setting the booking id for booking history screen
//        bookingHistoryController.bookingID = upcomingBooking.bookingId
//        bookingHistoryController.isComingFromNotification = false
//        bookingHistoryController.isPaymentInitiated = true
//        homeVC?.navigationController?.pushViewController(bookingHistoryController, animated: true)
        proccedToPayment(upcomingBooking: upcomingBooking)
    }
    func onClickBtnViewBill(cell: BBQUpcomingReservationCell, upcomingBooking: UpcomingBookingModel) {
        if let invoiceLink = upcomingBooking.bill_link{
            let webView = UIStoryboard.loadCommonWebView()
            webView.URLSring = invoiceLink
            webView.strTitle = "Invoice"
            webView.orderID = upcomingBooking.bookingId ?? "Invoice"
            homeVC?.present(webView, animated: true, completion: nil)
        }
    }
    
    func onClickBtnViewDetails(cell: BBQUpcomingReservationCell, upcomingBooking: UpcomingBookingModel) {
        // pusing the booking history storyboard
//        let  bookingHistoryController = UIStoryboard.loadReservationViewController()
//        //setting the booking id for booking history screen
//        bookingHistoryController.bookingID = upcomingBooking.bookingId!
//        bookingHistoryController.isComingFromNotification = false
//        homeVC?.navigationController?.pushViewController(bookingHistoryController, animated: true)
        AnalyticsHelper.shared.triggerEvent(type: .H14A)
        let reservationDetailsVC = ReservationDetailsVC()
        reservationDetailsVC.bookingID = upcomingBooking.bookingId ?? ""
        
        if homeVC == nil {
            cell.parentViewController?.navigationController?.pushViewController(reservationDetailsVC, animated: true)
        }else{
            homeVC?.navigationController?.pushViewController(reservationDetailsVC, animated: true)
        }
    }
    
    private func proccedToPayment(upcomingBooking: UpcomingBookingModel) {
        if upcomingBooking.bookingStatus == "PRE_RECEIPT"{
            AnalyticsHelper.shared.triggerEvent(type: .RH02E)
            showAfterDiningScreen(bookingId: upcomingBooking.bookingId ?? "")
        }else{
            AnalyticsHelper.shared.triggerEvent(type: .RH02D)
            AnalyticsHelper.shared.triggerEvent(type: .RH05)
            let rpController = BBQRazorpayContainerController()
            rpController.modalPresentationStyle = .overCurrentContext
            homeVC?.present(rpController, animated: true, completion: nil)
            rpController.payDelegate = self
            if let amount = upcomingBooking.netPayable, amount > 0{
                self.upcomingBooking = upcomingBooking
                rpController.createOrderForPayment(with:amount,
                                                   currency:upcomingBooking.currency ?? "",
                                                   title: "",
                                                   and: "Advance payment",
                                                   bookingID: upcomingBooking.bookingId ?? "", loyalty_points: nil)
            }else if let amount = upcomingBooking.billTotal, amount > 0{
                self.upcomingBooking = upcomingBooking
                rpController.createOrderForPayment(with:amount,
                                                   currency:upcomingBooking.currency ?? "",
                                                   title: "",
                                                   and: "Advance payment",
                                                   bookingID: upcomingBooking.bookingId ?? "", loyalty_points: nil)
            }
        }
    }
    
    private func showAfterDiningScreen(bookingId: String) {
        let diningVC = UIStoryboard.loadAfterDining()
        diningVC.bookingIDString = bookingId
        diningVC.delegate = self
        diningVC.view.layoutIfNeeded()
        let proposedHeight =  UIScreen.main.bounds.height -  (500)
        let configuration = BottomSheetConfiguration(animationDuration: 0.4,
                                                     minimumTopSpacing: CGFloat(proposedHeight))
        let bottomSheetController = BottomSheetController(configuration: configuration)
        diningVC.diningBottomSheet = bottomSheetController
        if let homeVC = homeVC {
            bottomSheetController.present(diningVC, on: homeVC)
        }
    }
}

extension UpcomingReserevationViewModel: BBQAfterDiningDelegate{
    func onPaymentSuceess() {
        homeVC?.dismiss(animated: true)
        homeVC?.getTheUpcomingReservation()
    }
}

extension UpcomingReserevationViewModel: BBQRazorpayControllerProtocol{
    func onRazorpayPaymentError(_ code: Int32, description str: String) {
        homeVC?.dismiss(animated: true)
        AnalyticsHelper.shared.triggerEvent(type: .RH05B)
        switch code {
        case 0,1,3:
            if let homeVC = homeVC{
                PopupHandler.showSingleButtonsPopup(title: kFailedTitle,
                                                    message: str,
                                                    on: homeVC, firstButtonTitle: kOkButtonTitle) { }

            }
            break
        case 2:
            self.showTransactionCancelled()
            
        default:
            print("");
        }
        self.upcomingBooking = nil
    }
    
    func onRazorpayPaymentSuccess(_ paymentData: BBQPaymentDataModel) {
        homeVC?.dismiss(animated: true)
        AnalyticsHelper.shared.triggerEvent(type: .RH05A)
        AnalyticsHelper.shared.triggerEvent(type: .RH06)
        let paymentGateway = Constants.BBQBookingStatus.razorPayment
        
        if let bookingId = upcomingBooking?.bookingId{
            if let view = homeVC?.view{
                BBQActivityIndicator.showIndicator(view: view)
            }
            self.updateBooking(bookingId: bookingId, paymentOrderId: paymentData.razorpayOrderID ?? "", paymentId: paymentData.paymentID ?? "", paymentGatewayName: paymentGateway, paymentSignature: paymentData.razorpaySignature ?? "") { (isSuccess) in
                if let view = self.homeVC?.view{
                    BBQActivityIndicator.hideIndicator(from: view)
                }
                
                
                if isSuccess {
                    AnalyticsHelper.shared.triggerEvent(type: .RH06A)
                    self.homeVC?.getTheUpcomingReservation()
                    self.showPaymentSuccessScreen()
                }else{
                    AnalyticsHelper.shared.triggerEvent(type: .RH06B)
                }
                self.upcomingBooking = nil
            }
        }
    }
    
    private func updateBooking(bookingId: String, paymentOrderId:String, paymentId: String, paymentGatewayName: String, paymentSignature:String, completion: @escaping (Bool)->()) {
        
        BBQBookingManager.updateBooking(bookingId:bookingId , paymentOrderId: paymentOrderId, paymentId: paymentId, paymentGatewayName: paymentGatewayName, paymentSignature: paymentSignature) {
            (model, result) in
            if result {
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    private func showTransactionCancelled() {
        if let homeVC = homeVC{
            PopupHandler.showSingleButtonsPopup(title: kMyCartTransactionCancelTitle,
                                                message: kMyCartTransactionCancelMessage,
                                                on: homeVC,
                                                firstButtonTitle: kButtonOkay) { }
        }
    }
    
    private func showPaymentSuccessScreen() {
        let registrationConfirmationVC = UIStoryboard.loadRegistrationConfirmation()
        registrationConfirmationVC.modalPresentationStyle = .overFullScreen
        registrationConfirmationVC.titleText = kPaymentSuccessText
        registrationConfirmationVC.navigationText = kMyTransactionPaymentRedirectionMessage
        
        //let keyWindow = UIApplication.shared.keyWindow?.rootViewController?.presentedViewController
        homeVC?.present(registrationConfirmationVC, animated: false) {
            sleep(UInt32(2.0))
            
            self.homeVC?.dismiss(animated: true) {
                
            }
        }
    }
}
