//
 //  Created by Arpana on 27/09/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified VoucherAndCouponListWithCountViewModel.swift
 //

import Foundation

class VoucherAndCouponListWithCountViewModel {
    
    
    private var vouchersAndCouponsModel: VoucherAndCouponListWithCountModel? = nil
    
    var getVouchersAndCouponsModel: VoucherAndCouponListWithCountModel? {
        return vouchersAndCouponsModel
    }
}

//MARK: Service Call Handler
extension VoucherAndCouponListWithCountViewModel {
    
    
    func getCouponAndVoucherListWithCount(completion:   @escaping (Bool)->()) {
        
        // Blocking API call for guest user
        if BBQUserDefaults.sharedInstance.isGuestUser {
            completion(false)
        }
        
        
        BBQVouchersCouponsManager.getCouponAndVoucherListWithCount() { (model, result) in
            
            if result {
                //Assign to model
                self.vouchersAndCouponsModel = model
                print (model)
                completion(true)
            } else {
                completion(false)
            }
        }
    }
}
        
    
    
   
