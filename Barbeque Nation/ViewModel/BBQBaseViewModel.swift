//
//  BBQBaseViewModel.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 10/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import Foundation

class BaseViewModel {
    
    //Server Error
    var serverErrorMessage: String {
        return BaseManager.errorDataModel?.fieldErrors?[0].message ?? ""
    }
}
