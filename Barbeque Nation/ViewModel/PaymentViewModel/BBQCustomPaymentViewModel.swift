//
//  Created by vinoth kumar on 09/01/21
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
//  All rights reserved.
//  Last modified BBQCustomPaymentViewModel.swift
//

import Foundation
import WebKit
import Razorpay

enum CustomPaymentSection:Int,CaseIterable {
    
    case upi
    case cards
    case wallets
    case netBanking
    
    var options:SectionOptions {
        var sectionOptions = SectionOptions.init(title: "")
        
        switch self {
        case .wallets:
            sectionOptions = SectionOptions.init(title: "Wallets")
            return sectionOptions
        case .cards:
            sectionOptions = SectionOptions.init(title: "Credit/Debit Cards")
            return sectionOptions
        case .upi:
            sectionOptions = SectionOptions.init(title: "UPI")
            return sectionOptions
        case .netBanking:
            sectionOptions = SectionOptions.init(title: "Net Banking")
            return sectionOptions
        }
    }
    
    var type:AnyClass {
        switch self {
        case .wallets:
            return BBQPaymentWalletTableViewCell.self
            
        case .cards,.upi:
            return AddNewCardTableViewCell.self
            
        case .netBanking:
            return BBQNetBankCollectionTableViewCell.self
        }
    }
    
}
class CustomPaymentViewModel {
    var sections:[CellSection]
    var razorPay:Razorpay
    var razorPayoptions:[String:Any]
    var razorPayDelegate:Any?
    var delegate: CustomPaymentViewModelDelegate?
    var paymentMethod:PaymentMethods?
    var webView:WKWebView
    var selectedOptions: CustomPaymentSection?
    var upiApps = [UPIApp]()
    
    var billAmount:String {
        if let amount = self.razorPayoptions["amount"] as? Int {
            if amount % 100 == 0{
                return (amount/100).description
            }else{
                return String(format: "%.02f", Double(amount)/100)
            }
        }
        return ""
    }
    
    private var famousBanks:[String] = ["HDFC","ICIC","UTIB","KKBK","YESB"]
    init(razorPay:Razorpay,razorpayOptions:[String:Any],delegate:Any?,webView:WKWebView, response: [AnyHashable: Any]?) {
        self.webView = webView
        self.razorPay = razorPay
        self.razorPayoptions = razorpayOptions
        self.razorPayDelegate = delegate
        
        //Initilize Sections for tableView
        var tempSections = [CellSection]()
        for i in CustomPaymentSection.allCases {
            let section = CellSection.init(options: i.options, data: [], type: i.type)
            if(i == .cards) {
                section.data = [StringData(text: "Add New Card",image: "CardLogos")]
            }
            else if i == .upi {
                section.data = [StringData(text: "Add New UPI ID",image: "upiLogos")]
            }
            tempSections.append(section)
        }
        
        
        
        sections = tempSections
        savePaymentMethods(response: response) { isCompleted in
            
        }
    }
    func createCustomer(completed:@escaping (Bool) -> Void) {
        BBQPaymentManager.createCustomer { (data, error) in
            if error == nil {
                if let response = try? JSONSerialization.jsonObject(with: data ?? Data(), options: JSONSerialization.ReadingOptions.allowFragments) as? [String:Any]{
                    
                    if let id = response["razorpay_customer_id"] as? String {
                        SharedProfileInfo.shared.profileData?.razorpayId = id
                        completed(true)
                        
                        //                        BBQUserManager.updateRazorPayID(razorpayID: id) { (isSuccess) in
                        //                            BBQUserManager.getUser { (profileData, isSucess) in
                        //                                SharedProfileInfo.shared.profileData?.razorpayId = profileData?.razorpayId;
                        //                            }
                        //                        }
                    }
                }
            }
            else {
                completed(false)
            }
        }
    }
    
    
    
    
    ///will fetch all payments from the razor will initilize the paymentMethod Object and setup Section data for uitableviewcell
    private func checkForPaymentMethods(completed:@escaping (Bool)->Void){
        if paymentMethod == nil{
            fetchPaymentMethods { isCompleted in
                completed(isCompleted)
            }
        }else{
            completed(true)
        }
    }
    
    func fetchUPIApps(completed:@escaping (Bool)->Void){
        RazorpayCheckout.getAppsWhichSupportUpi { (upiApps) in
            self.upiApps.removeAll()
            for upi in upiApps{
                let objUpiAPP = UPIApp(dict: upi, amount: self.billAmount)
                self.upiApps.append(objUpiAPP)
                var index = 0
                if let count = self.sections[CustomPaymentSection.upi.rawValue].data?.count, count > 1{
                    index = count - 1
                }
                self.sections[CustomPaymentSection.upi.rawValue].data?.insert(objUpiAPP, at: index)
            }
            completed(true)
        }
    }
    
    private func savePaymentMethods(response: [AnyHashable: Any]?, completed:@escaping (Bool)->Void){
        if let response = response{
            DispatchQueue.main.async {
                self.paymentMethod = PaymentMethods.init(razorPay: self.razorPay,response: response)
                self.sections.getSection(section: .wallets)?.data = self.sortTheWallets(wallets: self.paymentMethod?.wallet?.sorted(by: {$0.name < $1.name}) ?? [])
                self.sections.getSection(section: .netBanking)?.data = self.getNetBankCollection(data: self.paymentMethod?.netbank ?? [])
            }
            completed(true)
        }else{
            completed(false)
        }
    }
    
    func fetchUserTokens(completed:@escaping (Bool)->Void) {
        checkForPaymentMethods { isCompleted in
            BBQPaymentManager.fetchSavedTokens { (data, error) in
                if(error == nil) {
                    if let response = try? JSONSerialization.jsonObject(with: data ?? Data(), options: JSONSerialization.ReadingOptions.allowFragments) as? [String:Any]{
                        if let items = response["items"] as? [[String:Any]] {
                            for i in items {
                                if let type = i["method"] as? String{
                                    if type.lowercased() == "upi"{
                                        var token = SavedUPI.init(response: i)
                                        token.amountToPaid = self.billAmount
                                        self.sections[CustomPaymentSection.upi.rawValue].data?.insert(token, at: 0)
                                    }else if type.lowercased() == "card"{
                                        var token = SavedCard.init(response: i)
                                        token.amountToPaid = self.billAmount;
                                        self.sections[CustomPaymentSection.cards.rawValue].data?.insert(token, at: 0)
                                    }
                                    
                                }
                                
                            }
                        }
                    }
                }
                self.fetchUPIApps { isCompleted in
                    completed(true)
                }
            }
        }
        
        
        
    }
    
    func fetchPaymentMethods(completed:@escaping (Bool)->Void) {
        self.razorPay.getPaymentMethods(withOptions: nil) { (response) in
            self.paymentMethod = PaymentMethods.init(razorPay: self.razorPay,response: response)
            
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                // here "jsonData" is the dictionary encoded in JSON data

                let decoded = try JSONSerialization.jsonObject(with: jsonData, options: [])
                // here "decoded" is of type `Any`, decoded from JSON data

                // you can now cast it with the right type
                if let dictFromJSON = decoded as? [String:String] {
                    // use dictFromJSON
                }
            } catch {
                print(error.localizedDescription)
            }
            self.sections.getSection(section: .wallets)?.data = self.sortTheWallets(wallets: self.paymentMethod?.wallet?.sorted(by: {$0.name < $1.name}) ?? [])
            DispatchQueue.main.async {
                self.sections.getSection(section: .netBanking)?.data = self.getNetBankCollection(data: self.paymentMethod?.netbank ?? [])
            }
            completed(true)
        } andFailureCallback: { (error) in
            completed(false)
            print("Error fetching payment methods:\(error)")
        }
    }
    
    func sortTheWallets(wallets: [Wallet]?) -> [Wallet]?{
        let checkWallets = ["paytm", "amazonpay", "payzapp", "phonepe"]
        if var tempWallets = wallets{
            var index = 0
            for wallet in checkWallets {
                if let tempIndex = tempWallets.firstIndex(where: { check in check.name == wallet }) {
                    tempWallets.swapAt(index, tempIndex)
                    index += 1
                }
            }
            return tempWallets
        }
        return wallets
    }
    
    //    func getSavedCards(complete)
    /// will trimm the netbanking methods from fetched payment to famous banks to display it in collection view
    func getNetBankCollection(data:[NetBank]) -> [CellSection] {
        var trimmed = data.filter { (bank) -> Bool in
            self.famousBanks.contains(bank.bankCode)
        }
        trimmed.sort { (one, two) -> Bool in
            one.bankName < two.bankName
        }
        
        
        let netBankCollection = CellSection.init(data: Array(trimmed), type: BBQNetbankingCollectionViewCell.self)
        return [netBankCollection]
        
    }
    
    /// Initilize pay using saved card
    func payusingSavedCard(card:SavedCard) {
        self.razorPayoptions["method"] = "card"
        self.razorPayoptions["card[cvv]"] = card.cvv ?? ""
        self.razorPayoptions["customer_id"] = SharedProfileInfo.shared.profileData?.razorpayId ?? ""
        self.razorPayoptions["token"] = card.id
        self.razorPay.authorize(self.razorPayoptions);
    }
    /// Initilize pay using saved card
    func payusingSavedUPI(upi:SavedUPI) {
        self.razorPayoptions["method"] = "upi"
        self.razorPayoptions["customer_id"] = SharedProfileInfo.shared.profileData?.razorpayId ?? ""
        self.razorPayoptions["token"] = upi.id
        self.razorPay.authorize(self.razorPayoptions);
    }
    /// Initilize pay using app UPI
    func payusingAppUPI(upi: UPIApp) {
        self.razorPayoptions["method"] = "upi"
        self.razorPayoptions["_[flow]"] = "intent"
        self.razorPayoptions["customer_id"] = SharedProfileInfo.shared.profileData?.razorpayId ?? ""
        self.razorPayoptions["upi_app_package_name"] = upi.appPackageNam
        self.razorPay.authorize(self.razorPayoptions);
    }
    /// Initilize pay using wallet
    func payusingWallet(wallet:Wallet) {
        self.razorPayoptions["method"] = "wallet"
        self.razorPayoptions["wallet"] = wallet.name
        self.razorPay.authorize(self.razorPayoptions);
    }
    /// Initilize pay using net bank
    func payusingNetbank(bank:NetBank) {
        self.razorPayoptions["method"] = "netbanking"
        self.razorPayoptions["bank"] = bank.bankCode
        self.razorPay.authorize(self.razorPayoptions);
    }
    /// Initilize pay using net bank
    func payusingCard(card:CardDetails) {
        self.razorPayoptions["method"] = "card"
        self.razorPayoptions["card[name]"] = card.cardName
        self.razorPayoptions["card[number]"] = card.cardNumber
        self.razorPayoptions["card[expiry_month]"] = card.expiryMonth
        self.razorPayoptions["card[expiry_year]"] = card.expiryYear
        self.razorPayoptions["card[cvv]"] = card.cvv
        if(card.save) {
            self.razorPayoptions["save"] = "1"
            self.razorPayoptions["customer_id"] = SharedProfileInfo.shared.profileData?.razorpayId ?? ""
            
        }
        
        self.razorPay.authorize(self.razorPayoptions);
    }
    func payusingUPI(upi:String, save: Bool) {
        self.razorPayoptions["method"] = "upi"
        self.razorPayoptions["vpa"] = upi
        self.razorPayoptions["customer_id"] = SharedProfileInfo.shared.profileData?.razorpayId ?? ""
        if save{
            self.razorPayoptions["save"] = "1"
        }
        self.razorPay.authorize(self.razorPayoptions);
        
    }
    func cancelledPayment() {
        //                (self.razorPayDelegate as? BBQRazorpayContainerController)?.onPaymentError(2, description: "Canceled", andData: ["error" : 1]);
        //                (self.razorPayDelegate as? BBQDeliveryCartVC)?.onPaymentError(2, description: "Canceled", andData: ["error" : 1]);
        delegate?.paymentCancelled()
        webView.load(URLRequest(url: URL(string:"about:blank")!))
    }
    
    func getTokens() -> [Any] {
        var arrToken = [Any]()
        
        for i in 0 ..< ((sections[CustomPaymentSection.upi.rawValue].data?.count ?? 0) - upiApps.count - 1){
            arrToken.append(sections[CustomPaymentSection.upi.rawValue].data?[i] as Any)
        }
        for i in 0 ..< ((self.sections[CustomPaymentSection.cards.rawValue].data?.count ?? 0) - 1){
            arrToken.append(self.sections[CustomPaymentSection.cards.rawValue].data?[i] as Any)
        }
        for upiApp in upiApps {
            arrToken.append(upiApp)
        }
        //arrToken.reverse()
        return arrToken
    }
    
    func triggerPaymentSuccessEvent(){
        if let selectedOptions = selectedOptions{
            AnalyticsHelper.shared.triggerEvent(type: .CP06A)
            switch selectedOptions{
            case .cards:
                AnalyticsHelper.shared.triggerEvent(type: .CP06AB)
                break
            case .netBanking:
                AnalyticsHelper.shared.triggerEvent(type: .CP06AC)
                break
            case .upi:
                AnalyticsHelper.shared.triggerEvent(type: .CP06AA)
                break
            case .wallets:
                AnalyticsHelper.shared.triggerEvent(type: .CP06AD)
                break
            }
        }
    }
    func triggerPaymentCancelEvent(){
        if let selectedOptions = selectedOptions{
            AnalyticsHelper.shared.triggerEvent(type: .CP06C)
            switch selectedOptions{
            case .cards:
                AnalyticsHelper.shared.triggerEvent(type: .CP06CB)
                break
            case .netBanking:
                AnalyticsHelper.shared.triggerEvent(type: .CP06CC)
                break
            case .upi:
                AnalyticsHelper.shared.triggerEvent(type: .CP06CA)
                break
            case .wallets:
                AnalyticsHelper.shared.triggerEvent(type: .CP06CD)
                break
            }
        }
    }
    func triggerPaymentFailEvent(){
        if let selectedOptions = selectedOptions{
            AnalyticsHelper.shared.triggerEvent(type: .CP06B)
            switch selectedOptions{
            case .cards:
                AnalyticsHelper.shared.triggerEvent(type: .CP06BB)
                break
            case .netBanking:
                AnalyticsHelper.shared.triggerEvent(type: .CP06BC)
                break
            case .upi:
                AnalyticsHelper.shared.triggerEvent(type: .CP06BA)
                break
            case .wallets:
                AnalyticsHelper.shared.triggerEvent(type: .CP06BD)
                break
            }
        }
    }
    func triggerPaymentStartEvent(){
        if let selectedOptions = selectedOptions{
            AnalyticsHelper.shared.triggerEvent(type: .CP06)
            switch selectedOptions{
            case .cards:
                AnalyticsHelper.shared.triggerEvent(type: .CP03)
                break
            case .netBanking:
                AnalyticsHelper.shared.triggerEvent(type: .CP04)
                break
            case .upi:
                AnalyticsHelper.shared.triggerEvent(type: .CP02)
                break
            case .wallets:
                AnalyticsHelper.shared.triggerEvent(type: .CP05)
                break
            }
        }
    }
}

/*
 appLogo
 appIconBase64
 appPackageNam
 appName
 */
struct UPIApp: CellDataProvider{
    var appName: String = ""
    var appIconBase64: String = ""
    var appPackageNam: String = ""
    var appLogo: String = ""
    var isSelected: Bool = false
    var amountToPaid:String?
    var isFirstCell: Bool = false
    var isLastCell: Bool = false
    
    init(dict: [AnyHashable: Any], amount: String) {
        self.appName = String(format: "%@", dict["appName"] as? CVarArg ?? "")
        self.appIconBase64 = String(format: "%@", dict["appIconBase64"] as? CVarArg ?? "")
        self.appPackageNam = String(format: "%@", dict["appPackageName"] as? CVarArg ?? "")
        self.appLogo = String(format: "%@", dict["appLogo"] as? CVarArg ?? "")
        self.amountToPaid = amount
    }
}


protocol CustomPaymentViewModelDelegate{
    func paymentCancelled()
    func paymentFailed()
}
