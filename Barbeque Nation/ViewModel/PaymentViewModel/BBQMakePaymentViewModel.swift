//
//  Created by Ajith CP on 11/08/19.
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQMakePaymentViewModel.swift
//

import Foundation
//import Razorpay

enum PaymentType: String, CaseIterable {
    case minimum, full // other; removing other payment option for now.
    
    static var caseCount: Int { return self.allCases.count }
    
    var description: String {
        get {
            switch(self) {
            case .minimum:
                return kMinimumPaySectionTitle
            case .full:
                return kFullPaySectionUnselectedTitle
            }
        }
    }
}

class MakePaymentViewModel: BaseViewModel {
    
    private var paymentDataModel: BBQPaymentDataModel?
    private var paymentOrderDataModel: BBQPaymentOrderDataModel?
    
    var checkoutStatus: String = ""
    
    var numberOfPaymentType: Int {
        return PaymentType.caseCount
    }
    
    func formattedCheckoutAmount(amount: Double) -> String {
        // This is in currency subunits. 1000 = 1000 paise= INR 10.
        let formatted  = Int(amount * 100)
        return String(formatted)
    }
    func checkoutpageOptions(with paymentModel: BBQRazorPayRequestModel) -> [String:Any] {
       // Add options for how razor pay checkout page looks like.
       // Find more here https://razorpay.com/docs/payment-gateway/web-integration/standard/#checkout-form
        let amount = Int((Float(paymentModel.amount ?? "0.0") ?? 0) * 100)
        return [
            "amount": amount,
            "currency": paymentModel.currency ?? "",
            "description": paymentModel.description ?? "",
            "order_id": paymentModel.order_id ?? "",
//            "name": paymentModel.name ?? "",
            "email" : SharedProfileInfo.shared.profileData?.email ?? BBQUserDefaults.sharedInstance.customPaymentEmail,
            "contact": SharedProfileInfo.shared.profileData?.mobileNumber as Any,
            "prefill": ["name": BBQUserDefaults.sharedInstance.UserName, "email": SharedProfileInfo.shared.profileData?.email ?? BBQUserDefaults.sharedInstance.customPaymentEmail, "contact": SharedProfileInfo.shared.profileData?.mobileNumber as Any]
            
        ]
    }
    func checkoutpageOptionsOld(with paymentModel: BBQRazorPayRequestModel) -> [String:Any] {
       // Add options for how razor pay checkout page looks like.
       // Find more here https://razorpay.com/docs/payment-gateway/web-integration/standard/#checkout-form
        return [
            "amount": paymentModel.amount ?? "",
            "currency": paymentModel.currency ?? "",
            "description": paymentModel.description ?? "",
            "order_id": paymentModel.order_id ?? "",
            "image": paymentModel.image ?? "",
            "name": paymentModel.name ?? "",
            "theme": paymentModel.theme ?? [:],
            "prefill": ["name": BBQUserDefaults.sharedInstance.UserName, "email": SharedProfileInfo.shared.profileData?.email  ?? BBQUserDefaults.sharedInstance.customPaymentEmail, "contact": SharedProfileInfo.shared.profileData?.mobileNumber as Any],
            "retry": false,
            "timeout": 300
            
        ]
    }
    
    var sectionsData: [Section] = [
        Section(title: PaymentType.minimum.description, rowItems: [
            RowItem(placeholder: "", amount: "")
        ]),
        Section(title: PaymentType.full.description, rowItems: [
            RowItem(placeholder: "", amount: "")
        ])
    ]
    
    
    // MARK: Data Fetch Methods
    
    /// Create order before starting check out and this will return order id which needs to be passed with checkout.
    /// - Parameters:
    ///     - amount: total amount need to be captured, mandatory field
    ///     - currency: urrency code for the currency in which we want to accept the payment, mandatory field
    ///     - receipt: Your receipt id for this order. Maximum length 40 characters, optional
    ///     - payment_capture: Payment capture flag for auto capturing payment, optional
    func createPaymentOrder(with amount: String,
                            currency: String,
                            bookingID: String,
                            loyalty_points: Int?,
                            completion: @escaping (String) -> ()) {
        
        BBQPaymentManager.createRazorpayOrder(amount: amount, currency: currency, bookingID: bookingID, loyalty_points: loyalty_points) { (data, error) in
            if error == nil {
                do {
                    let decoder = JSONDecoder()
                    guard let responseData = data else {
                        DispatchQueue.main.async {
                            completion("")
                        }
                        return
                    }
                    self.paymentOrderDataModel =
                        try decoder.decode(BBQPaymentOrderDataModel.self,
                                           from: responseData)
                    
                    DispatchQueue.main.async {
                        completion(self.paymentOrderDataModel?.orderID ?? "")
                    }
                } catch {
                    DispatchQueue.main.async {
                        completion("")
                    }
                }
            } else {
                DispatchQueue.main.async {
                    completion("")
                }
            }
        }
    }
    
    func createCartPaymentOrder(discount: String = "0", loyalty_points: String = "0", bar_code: String = "", completion: @escaping (String) -> ()) {
        BBQPaymentManager.getCartPaymentDetails(discount: discount, loyalty_points: loyalty_points, bar_code: bar_code) { (model, result) in
            if result {
                //Assign to model
                if let paymentModel = model, let orderId = paymentModel.orderId {
                    completion(orderId)
                } else {
                    completion("")
                }
            } else {
                completion("")
            }
        }
    }
    
    func paymentCheckout(points: Int, barCode: String, razorpayPaymentId: String, razorpayOrderId: String, razorpaySignature: String, completion: @escaping (Bool)->()) {
        
        BBQPaymentManager.paymentCheckout(points: points, barCode: barCode, razorpayPaymentId: razorpayPaymentId, razorpayOrderId: razorpayOrderId, razorpaySignature: razorpaySignature) { (statusMessage, result) in
            
            if result {
                //Assign to model
                if let message = statusMessage {
                    self.checkoutStatus = message
                }
                completion(true)
            } else {
                completion(false)
            }
        }
    }
}
