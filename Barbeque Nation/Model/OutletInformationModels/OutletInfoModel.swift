//
//  Created by Abhijit on 12/09/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified OutletInfoModel.swift
//

import Foundation

struct OutletInfoModel: Codable {
    var status:String?
    var name:String?
    var city_code:String?
    var area: String?
    var country:String?
    var store_code:String?
    var branch_name:String?
    var branch_image:String?
    var address: String?
    var landmark:String?
    var longitude:Double?
    var latitude:Double?
    var branch_phone:String?
    var lunch_capacity:Int?
    var dinner_capacity:Int?
    var slots:[SlotsLeft]?
    var mobile_app:String?
    var cgst:Double?
    var sgst:Double?
    var service_charge:Int?
    var terms_condition:String?
    var outletAmenities: [OutletAmenities]?
    var promotion : [PromotionModel]?
    var closed:String?
    var branchGallery :[String]?
    var branchMenu: [String]?
    var lunchTime : String?
    var dinnerTime :String?
    var advance_payment: String = "0"
   
    
    private enum OutletInfoCodingKeys:String,CodingKey{
        case status = "status"
        case name = "name"
        case cityCode = "city_code"
        case area = "area"
        case country = "country"
        case store_code = "store_code"
        case branch_name = "branch_name"
        case branch_image = "branch_image"
        case address = "address"
        case landmark = "landmark"
        case longitude = "longitude"
        case latitude = "latitude"
        case branch_phone = "branch_phone"
        case lunch_capacity = "lunch_capacity"
        case dinner_capacity = "dinner_capacity"
        case slots = "slots"
        case mobile_app = "mobile_app"
        case cgst = "cgst"
        case sgst = "sgst"
        case service_charge = "service_charge"
        case terms_condition = "terms_condition"
        case outletAmenities = "amenities"
        case promotion = "promotion"
        case closed = "closed"
        case branchGallery = "branch_gallery"
        case lunchTime = "lunch_time"
        case dinnerTime = "dinner_time"
        case advance_payment = "advance_payment"
        case branchMenu = "branch_menu_image"
    }
}


extension OutletInfoModel {
    init(from decoder:Decoder) throws{
        let outletInfo = try decoder.container(keyedBy: OutletInfoCodingKeys.self)
       // decodeIfPresent(String.self, forKey: .bookingId) ?? ""
        status = try outletInfo.decodeIfPresent(String.self, forKey: .status) ?? ""
        name =  try outletInfo.decodeIfPresent(String.self, forKey: .name) ?? ""
        city_code = try outletInfo.decodeIfPresent(String.self, forKey: .cityCode) ?? ""
        area = try outletInfo.decodeIfPresent(String.self, forKey: .area) ?? ""
        country = try outletInfo.decodeIfPresent(String.self, forKey: .country) ?? ""
        store_code = try outletInfo.decodeIfPresent(String.self, forKey: .store_code) ?? ""
        branch_name = try outletInfo.decodeIfPresent(String.self, forKey: .branch_name) ?? ""
        branch_image = try outletInfo.decodeIfPresent(String.self, forKey: .branch_image) ?? ""
        address = try outletInfo.decodeIfPresent(String.self, forKey: .address) ?? ""
        landmark = try outletInfo.decodeIfPresent(String.self, forKey: .landmark) ?? ""
        longitude = try outletInfo.decodeIfPresent(Double.self, forKey: .longitude) ?? 0.0
        latitude = try outletInfo.decodeIfPresent(Double.self, forKey: .latitude) ?? 0.0
        branch_phone = try outletInfo.decodeIfPresent(String.self, forKey: .branch_phone) ?? ""
        lunch_capacity = try outletInfo.decodeIfPresent(Int.self, forKey: .lunch_capacity) ?? 0
        dinner_capacity = try outletInfo.decodeIfPresent(Int.self, forKey: .dinner_capacity) ?? 0
        //slot_based = try outletInfo.decode([String].self, forKey: .slot_based)
        mobile_app = try outletInfo.decodeIfPresent(String.self, forKey: .mobile_app) ?? ""
        area = try outletInfo.decodeIfPresent(String.self, forKey: .area) ?? ""
        cgst = try outletInfo.decodeIfPresent(Double.self, forKey: .cgst) ?? 0.0
        sgst = try outletInfo.decodeIfPresent(Double.self, forKey: .sgst) ?? 0.0
        service_charge = try outletInfo.decodeIfPresent(Int.self, forKey: .service_charge) ?? 0
        terms_condition = try outletInfo.decodeIfPresent(String.self, forKey: .terms_condition) ?? ""
        outletAmenities = try outletInfo.decodeIfPresent([OutletAmenities]?.self, forKey: .outletAmenities) as? [OutletAmenities]
        closed = try outletInfo.decodeIfPresent(String.self, forKey: .closed) ?? ""
        promotion = try outletInfo.decodeIfPresent([PromotionModel]?.self, forKey: .promotion) as? [PromotionModel]
        branchGallery = try outletInfo.decodeIfPresent([String]?.self, forKey: .branchGallery) as? [String]
        branchMenu = try outletInfo.decodeIfPresent([String]?.self, forKey: .branchMenu) as? [String]
        lunchTime = try outletInfo.decodeIfPresent(String?.self, forKey: .lunchTime) ?? ""
        dinnerTime = try outletInfo.decodeIfPresent(String?.self, forKey: .dinnerTime) ?? ""
        advance_payment = try outletInfo.decodeIfPresent(String.self, forKey: .advance_payment) ?? "0"
    }
}

struct OutletAmenities: Codable {
    var description:String?
    var image:String?
    var name:String?
    private enum OutletInfoCodingKeys:String,CodingKey{
        case description = "description"
        case image = "image"
        case name = "name"
    }
}
extension OutletAmenities {
    init(from decoder:Decoder) throws{
        let outletInfo = try decoder.container(keyedBy: OutletInfoCodingKeys.self)
        description = try outletInfo.decode(String.self, forKey: .description)
        image = try outletInfo.decode(String.self, forKey: .image)
        name = try outletInfo.decode(String.self, forKey: .name)
    }
}
struct SlotsLeft: Codable{
    var slot_id:String?
    var slot_name:String?
     private enum OutletInfoCodingKeys:String,CodingKey{
        case slot_id = "slot_id"
        case slot_name = "slot_name"
    }
}
extension SlotsLeft{
    init(from decoder:Decoder) throws{
        let outletInfo = try decoder.container(keyedBy: OutletInfoCodingKeys.self)
        slot_id = try outletInfo.decode(String.self, forKey: .slot_id)
        slot_name = try outletInfo.decode(String.self, forKey: .slot_name)
    }
}

