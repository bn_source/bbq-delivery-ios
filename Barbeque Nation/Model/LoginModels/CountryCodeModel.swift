//
//  CountryCodeModel.swift
//  Barbeque Nation
//
//  Created by Rabindra L on 08/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import Foundation

struct CountryCodeModel {
    var flag:String
    var countryCode:String
    var phoneNumberCode:String
    var countryName:String
    
    init() {
        flag = ""
        countryCode = ""
        phoneNumberCode = ""
        countryName = ""
    }
}
