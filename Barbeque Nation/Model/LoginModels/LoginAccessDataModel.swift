//
//  LoginAccessDataModel.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 05/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import Foundation

struct TokenDataModel: Codable {
    
    var tokenType: String?
    var expiryTime: Int64
    var accessToken: String?
    var refreshToken: String?
    var title: String?
    var name: String?
    var loyaltyPoints: Int?
    
    private enum TokenDataCodingKeys: String, CodingKey {
        case tokenType = "token_type"
        case expiryTime = "expires_in"
        case accessToken = "access_token"
        case refreshToken = "refresh_token"
        case title = "title"
        case name = "name"
        case loyaltyPoints = "loyalty_points"
    }
}

struct SocialLoginUserModel: Codable {
    var isNewUser: Bool?
    
    private enum CodingKeys: String, CodingKey {
        case isNewUser = "new_user"
    }
}

extension TokenDataModel {
    init(from decoder: Decoder) throws {
        let tokenDataInfo = try decoder.container(keyedBy: TokenDataCodingKeys.self)
        tokenType = try tokenDataInfo.decodeIfPresent(String.self, forKey: .tokenType) ?? ""
        expiryTime = try tokenDataInfo.decodeIfPresent(Int64.self, forKey: .expiryTime) ?? 0
        accessToken = try tokenDataInfo.decodeIfPresent(String.self, forKey: .accessToken) ?? ""
        refreshToken = try tokenDataInfo.decodeIfPresent(String.self, forKey: .refreshToken) ?? ""
        title = try tokenDataInfo.decodeIfPresent(String.self, forKey: .title) ?? ""
        name = try tokenDataInfo.decodeIfPresent(String.self, forKey: .name) ?? ""
        loyaltyPoints = try tokenDataInfo.decodeIfPresent(Int.self, forKey: .loyaltyPoints) ?? 0
    }
}


struct LogoutModel:Codable {
    var logoutMessage:String?
    private enum LogoutCodingKeys: String, CodingKey {
        case message = "message"
    }
}

extension LogoutModel {
    init(from decoder: Decoder) throws {
        let logoutInfo = try decoder.container(keyedBy: LogoutCodingKeys.self)
        logoutMessage = try logoutInfo.decodeIfPresent(String.self, forKey: .message) ?? ""
    }
}
