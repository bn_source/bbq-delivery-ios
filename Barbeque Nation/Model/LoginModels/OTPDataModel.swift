//
//  OTPDataModel.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 04/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import Foundation

struct VerifyUserDataModel: Codable {
    var otpId: String?
    var expiryTime: Int64
    var title: String?
    var name: String?
    var isExistingUser: Bool = false
    var otpValidation: Bool = true
    
    private enum UserCodingKeys: String, CodingKey {
        case otpId = "otp_id"
        case expiryTime = "expiry_time"
        case title = "title"
        case name = "name"
        case isExistingUser = "existing_user"
    }
}

extension VerifyUserDataModel {
    init(from decoder: Decoder) throws {
        let userInfo = try decoder.container(keyedBy: UserCodingKeys.self)
        
        otpId = try userInfo.decodeIfPresent(String.self, forKey: .otpId) ?? ""
        expiryTime = try userInfo.decodeIfPresent(Int64.self, forKey: .expiryTime) ?? 0
        title = try userInfo.decodeIfPresent(String.self, forKey: .title) ?? ""
        name = try userInfo.decodeIfPresent(String.self, forKey: .name) ?? ""
        isExistingUser = try userInfo.decodeIfPresent(Bool.self, forKey: .isExistingUser) ?? false
    }
}

struct GenerateOTPDataModel: Codable {
    var otpId: String?
    var expiryTime: Int64
    
    private enum OTPCodingKeys: String, CodingKey {
        case otpId = "otp_id"
        case expiryTime = "expiry_time"
    }
}

extension GenerateOTPDataModel {
    // TODO: expiry_time type is shuffling for send/resend OTP API. Correct it from back end and handle
    // exact type here.
    init(from decoder: Decoder) throws {
        let otpInfo = try decoder.container(keyedBy: OTPCodingKeys.self)
        otpId = try otpInfo.decodeIfPresent(String.self, forKey: .otpId) ?? ""
        expiryTime = try otpInfo.decodeIfPresent(Int64.self, forKey: .expiryTime) ?? 0
    }
}

struct VerifyOTPDataModel: Codable {
    let responseMessage: String?
    
    private enum CodingKeys: String, CodingKey {
        case responseMessage = "message"
    }
}

struct veriyReferralCodeModel: Codable {
    let responseMessage: Int?
    
    private enum referralCodingKeys: String, CodingKey {
        case responseMessage = "uid"
    }
}

struct LogoutUserDataModel: Codable {
    let responseLogoutMessage: String?
    
    private enum CodingKeys: String, CodingKey {
        case responseLogoutMessage = "message"
    }
}

extension veriyReferralCodeModel {
    init(from decoder: Decoder) throws {
        let referInfo = try decoder.container(keyedBy: referralCodingKeys.self)
        responseMessage = try referInfo.decodeIfPresent(Int.self, forKey: .responseMessage) ?? 0
    }
}
