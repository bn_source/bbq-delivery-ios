//
 //  Created by Nischitha on 06/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQBookingHistoryModel.swift
 //

import Foundation

struct BookingHistoryModel: Codable {
    let upcomingReservation: [UpcomingReservationDetails]?
    let bookingsHistory: [BookingHistory]?
   
    
    private enum BookingHistoryDataKeys: String, CodingKey {
        case upcomingReservation = "upcoming_reservation"
        case bookingsHistory = "bookings"
    }
}

struct UpcomingReservationDetails: Codable {
    
    var bookingId: String?
    var bookingStatus: String?
    var branchName: String?
    var branchAddress: String?
    var branchContactNumber: String?
    var branchImage: String?
    var latitude: Double?
    var longitude: Double?
    var amenities: [AmenitiesDetails]?
    var bookingDetails: [BookingsDetails]?
    var billTotal: Double?
    var showPayment: Bool?
    var bbqPoints: Int64?
    var opened : Bool = false
    var cellExpand :Bool?
    
    
    private enum UpcomingReservationCodingKeys: String, CodingKey {
        
        case bookingId = "booking_id"
        case bookingStatus = "booking_status"
        case branchName = "branch_name"
        case branchAddress = "branch_address"
        case branchContactNumber = "branch_contact_no"
        case branchImage = "branch_image"
        case latitude = "lat"
        case longitude = "long"
        case amenities = "amenities"
        case bookingDetails = "booking_details"
        case billTotal = "bill_total"
        case showPayment = "show_payment"
        case bbqPoints = "bbq_points"
    }
}

struct AmenitiesDetails:Codable {
    
    var name: String?
    var description: String?
    var image: String?
    
    private enum AmenitiesCodingKeys: String, CodingKey {
        
        case name = "name"
        case description = "description"
        case image = "image"
    }
}

struct BookingsDetails: Codable {
    
    var id: Int64?
    var menuId: Int64?
    var buffetId: Int?
    var packs: Int?
    var buffetDetails: [BuffetDetails]?
    
    
    private enum BookingDetailsKeys: String, CodingKey {
        
        case id = "id"
        case menuId = "menu_id"
        case buffetId = "buffet_id"
        case packs = "packs"
        case buffetDetails = "buffet_details"
    }
}
struct BuffetDetails: Codable {
    var name: String?
    var type: String?
    
    private enum BuffetDetailsKeys: String, CodingKey {
        case name = "name"
        case type = "type"
    }
}



struct BookingHistory: Codable {
     var opened : Bool = false
}
extension BuffetDetails {
    init(from decoder:Decoder) throws {
        let buffetDetailsInfo = try decoder.container(keyedBy: BuffetDetailsKeys.self)
        name = try buffetDetailsInfo.decodeIfPresent(String.self, forKey: .name) ?? ""
        type = try buffetDetailsInfo.decodeIfPresent(String.self, forKey: .type) ?? ""
        
    }
}
extension BookingsDetails {
    init(from decoder:Decoder) throws {
        let bookingDetailsInfo = try decoder.container(keyedBy: BookingDetailsKeys.self)
        id = try bookingDetailsInfo.decodeIfPresent(Int64.self, forKey: .id) ?? Int64(0.0)
        menuId = try bookingDetailsInfo.decodeIfPresent(Int64.self, forKey: .menuId) ?? Int64(0.0)
        buffetId = try bookingDetailsInfo.decodeIfPresent(Int.self, forKey: .buffetId) ?? 0
        packs = try bookingDetailsInfo.decodeIfPresent(Int.self, forKey: .packs) ?? 0
        buffetDetails = try bookingDetailsInfo.decodeIfPresent([BuffetDetails].self, forKey: .buffetDetails) 
    }
}

extension AmenitiesDetails {
    init(from decoder:Decoder) throws {
        let amenitiesInfo = try decoder.container(keyedBy: AmenitiesCodingKeys.self)
        name = try amenitiesInfo.decodeIfPresent(String.self, forKey: .name) ?? ""
        description = try amenitiesInfo.decodeIfPresent(String.self, forKey: .description) ?? ""
        image = try amenitiesInfo.decodeIfPresent(String.self, forKey: .image) ?? ""
    }
}

extension UpcomingReservationDetails {
    
    init(from decoder:Decoder) throws {
        
        let upcomingReservationInfo = try decoder.container(keyedBy: UpcomingReservationCodingKeys.self)
        bookingId = try upcomingReservationInfo.decodeIfPresent(String.self, forKey: .bookingId) ?? ""
        bookingStatus = try upcomingReservationInfo.decodeIfPresent(String.self, forKey: .bookingStatus) ?? ""
        branchName = try upcomingReservationInfo.decodeIfPresent(String.self, forKey: .branchName) ?? ""
        branchAddress = try upcomingReservationInfo.decodeIfPresent(String.self, forKey: .branchAddress) ?? ""
        branchContactNumber = try upcomingReservationInfo.decodeIfPresent(String.self, forKey: .branchContactNumber) ?? ""
        branchImage = try upcomingReservationInfo.decodeIfPresent(String.self, forKey: .branchImage) ?? ""
        latitude = try upcomingReservationInfo.decodeIfPresent(Double.self, forKey: .latitude) ?? 0.0
        longitude = try upcomingReservationInfo.decodeIfPresent(Double.self, forKey: .longitude) ?? 0.0
        amenities = try upcomingReservationInfo.decodeIfPresent([AmenitiesDetails].self, forKey: .amenities)
        bookingDetails = try upcomingReservationInfo.decodeIfPresent([BookingsDetails].self, forKey: .bookingDetails)
        billTotal = try upcomingReservationInfo.decodeIfPresent(Double.self, forKey: .billTotal) ?? 0.0
        showPayment = try upcomingReservationInfo.decodeIfPresent(Bool.self, forKey: .showPayment)
        bbqPoints = try upcomingReservationInfo.decodeIfPresent(Int64.self, forKey: .bbqPoints)
        

    }
}
extension BookingHistoryModel {
    
    init(from decoder:Decoder) throws {
        let bookingHistoryModelInfo = try decoder.container(keyedBy: BookingHistoryDataKeys.self)
        upcomingReservation = try bookingHistoryModelInfo.decodeIfPresent([UpcomingReservationDetails].self, forKey: .upcomingReservation)
        bookingsHistory = try bookingHistoryModelInfo.decodeIfPresent([BookingHistory].self, forKey: .bookingsHistory)
        
    }
}
