//
//  BookingConfirmationModel.swift
//  Barbeque Nation
//
//  Created by Nischitha on 13/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import Foundation

struct BookingConfirmationModel {
    var bookingId: String? = ""
    var bookingDate: String? = ""
    var bookingPlace: String? = ""
    var bookingOrder: [String: Any]? = nil
    var pointsReedemed: String? = ""
    var estimatedTotal: String? = ""
    var advanceAmount: String? = ""
}
