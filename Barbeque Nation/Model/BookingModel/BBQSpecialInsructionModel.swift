//
 //  Created by Maya R on 14/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQSpecialInsructionModel.swift
 //

import Foundation

struct SpecialInsructionsModel :Codable{
    var celebratingDetails:[CelebrationModel]
    var joinedByDetails:[JoinedByModel]
    private enum SpecialInsructionsKeys: String, CodingKey {
        case celebratingFor = "celebrating_for"
        case joinedBy = "joined_by"
    }
}

struct CelebrationModel:Codable {
    var occasionId:String?
    var occasionName:String?
    private enum CelebrationKeys: String, CodingKey {
        case occasionName = "occasion_name"
        case occasionId = "occasion_id"
    }
}

struct JoinedByModel:Codable {
    var relationshipId:String?
    var relationshipName:String?
    private enum JoinedByKeys: String, CodingKey {
        case relationshipName = "relationship_name"
        case relationshipId = "relationship_id"
    }
}

extension SpecialInsructionsModel{
    init(from decoder: Decoder) throws {
        let instructInfo = try decoder.container(keyedBy: SpecialInsructionsKeys.self)
        celebratingDetails = try instructInfo.decode([CelebrationModel].self, forKey: .celebratingFor)
        joinedByDetails = try instructInfo.decode([JoinedByModel].self, forKey: .joinedBy)
    }
}
extension CelebrationModel{
    init(from decoder: Decoder) throws {
        let celebrateInfo = try decoder.container(keyedBy: CelebrationKeys.self)
        occasionName = try celebrateInfo.decodeIfPresent(String.self, forKey: .occasionName) ?? ""
        occasionId = try celebrateInfo.decodeIfPresent(String.self, forKey: .occasionId) ?? ""

    }
}

extension JoinedByModel{
    init(from decoder: Decoder) throws {
        let joinedInfo = try decoder.container(keyedBy: JoinedByKeys.self)
        relationshipName = try joinedInfo.decodeIfPresent(String.self, forKey: .relationshipName) ?? ""

        relationshipId = try joinedInfo.decodeIfPresent(String.self, forKey: .relationshipId) ?? ""
    }
}
