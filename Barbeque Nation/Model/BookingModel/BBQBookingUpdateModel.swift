//
 //  Created by Sridhar on 19/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQBookingUpdateModel.swift
 //

import Foundation

// MARK: - Update Booking API Starts
// MARK: - PreferredBranch and NearByBranch
struct UpdateBookingDataModel: Codable {
    let preferredBranch: PreferredBranch?
    let nearbyBranches: [NearByBranches]?
    
    private enum UpdateBookingDataKeys: String, CodingKey {
        case preferredBranch = "preferred_branch"
        case nearbyBranches = "near_by_branches"
    }
}
