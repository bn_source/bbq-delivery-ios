//
//  BBQBookingMenuModel.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 14/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

struct MenuModel {
    var menuName: String? = nil
    var menuCategory: [Category]? = nil
    
    init(menuName: String, menuCategory: [Category]) {
        self.menuName = menuName
        self.menuCategory = menuCategory
    }
}

struct Category {
    var categoryName: String? = nil
    var menuData: [MenuData]? = nil
    
    init(categoryName: String, menuData: [MenuData]) {
        self.categoryName = categoryName
        self.menuData = menuData
    }
}

struct MenuData {
    var foodName: String? = nil
    var foodDescription: String? = nil
    var foodImage: UIImage? = nil
    var isVeg: Bool = true
    var tagImage: String? = nil
    
    init(foodName: String, foodDescription: String, foodImage: UIImage, isVeg: Bool, tagImage: String) {
        self.foodName = foodName
        self.foodDescription = foodDescription
        self.foodImage = foodImage
        self.isVeg = isVeg
        self.tagImage = tagImage
    }
}
