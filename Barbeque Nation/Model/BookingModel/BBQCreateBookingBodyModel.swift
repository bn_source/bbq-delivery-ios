//
 //  Created by Chandan Singh on 18/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQCreateBookingBodyModel.swift
 //

import Foundation

struct CreateBookingBody: Encodable {
    var bookingId: String? = nil
    var branchId: String
    var slotId: String
    var reservationDate: String
    var reservationTime: String
    var loyaltyPoints: Int
    var loyaltyAmount: String
    var bookingDetails: [BookingDetailsBody]
    var voucherDetails: [VoucherDetailsBody]? = nil
    var voucher_meal_discount: Double = 0.0
    
    private enum CreateBookingBodyKeys: String, CodingKey {
        case bookingId = "booking_id"
        case branchId = "branch_id"
        case slotId = "slot_id"
        case reservationDate = "reservation_date"
        case reservationTime = "reservation_time"
        case loyaltyPoints = "loyalty_points"
        case loyaltyAmount = "loyalty_points_amount"
        case bookingDetails = "booking_details"
        case voucherDetails = "voucher_details"
        case voucher_meal_discount = "voucher_meal_discount"
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CreateBookingBodyKeys.self)
        if self.bookingId != nil {
            //Used for reschedule booking
            try container.encodeIfPresent(bookingId, forKey: .bookingId)
        }
        try container.encodeIfPresent(branchId, forKey: .branchId)
        try container.encodeIfPresent(slotId, forKey: .slotId)
        try container.encodeIfPresent(reservationDate, forKey: .reservationDate)
        try container.encodeIfPresent(reservationTime, forKey: .reservationTime)
        try container.encodeIfPresent(loyaltyPoints, forKey: .loyaltyPoints)
        try container.encodeIfPresent(loyaltyAmount, forKey: .loyaltyAmount)
        try container.encodeIfPresent(bookingDetails, forKey: .bookingDetails)
        try container.encodeIfPresent(voucherDetails, forKey: .voucherDetails)
       // try container.encodeIfPresent(voucher_meal_discount, forKey: .voucher_meal_discount)
    }
}

struct BookingDetailsBody: Encodable {
    var menuId: String
    var buffetId: String
    var packs: String
    var bufetType: String
    var customerType: String


    init(menuId: String, buffetId: String, packs: String , buffetType: String, customerType: String) {
        self.menuId = menuId
        self.buffetId = buffetId
        self.packs = packs
        self.bufetType = buffetType
        self.customerType = customerType
    }
    
    private enum BookingDetailsKeys: String, CodingKey {
        case menuId = "menu_id"
        case buffetId = "buffet_id"
        case packs = "packs"
        case buffetType = "buffet_type"
        case customerType = "customerType"
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: BookingDetailsKeys.self)
        try container.encodeIfPresent(menuId, forKey: .buffetId)
        try container.encodeIfPresent(buffetId, forKey: .buffetId)
        try container.encodeIfPresent(packs, forKey: .packs)
        try container.encodeIfPresent(bufetType, forKey: .buffetType)
        try container.encodeIfPresent(customerType, forKey: .customerType)

    }
}

struct VoucherDetailsBody: Encodable {
    var title: String
    var voucherCode: String
    var voucherType: String
    var amount: Double
    var isCorporateVoucher: String
    var pax_applicable: Int = 0
    
    init(title: String, voucherCode: String, voucherType: String, amount: Double, isCorporateVoucher: String, pax_applicable: Int) {
        self.title = title
        self.voucherCode = voucherCode
        self.voucherType = voucherType
        self.amount = amount
        self.isCorporateVoucher = isCorporateVoucher
        self.pax_applicable = pax_applicable
    }
    
    private enum VoucherDetailsBodyKeys: String, CodingKey {
        case title = "title"
        case voucherCode = "voucher_code"
        case voucherType = "voucher_type"
        case amount = "amount"
        case isCorporateVoucher = "corporate_voucher"
        case pax_applicable = "pax_applicable"
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: VoucherDetailsBodyKeys.self)
        try container.encodeIfPresent(title, forKey: .title)
        try container.encodeIfPresent(voucherCode, forKey: .voucherCode)
        try container.encodeIfPresent(voucherType, forKey: .voucherType)
        try container.encodeIfPresent(amount, forKey: .amount)
        try container.encodeIfPresent(isCorporateVoucher, forKey: .isCorporateVoucher)
        try container.encodeIfPresent(pax_applicable, forKey: .pax_applicable)
    }
}

struct FinalPaymentUpdateBody: Encodable {
    
    var bookingId: String? = nil
    var branch_id: String?
    var paymentGateway : String? = nil
    var paymentID : String? = nil
    var orderID : String? = nil
    var paymentSignature : String? = nil
    var loyaltyPoints: Int? = nil
    var loyaltyAmount: Double? = nil
    var voucherDetails: [VoucherDetailsBody]? = nil
    var currency: String?
    var reservation_date: String?
    var reservation_time: String?
    var country_code: String?
    var mobile_number: Int64?
    
    private enum CreateBookingBodyKeys: String, CodingKey {
        case bookingId = "booking_id"
        case loyaltyPoints = "loyalty_points"
        case loyaltyAmount = "loyalty_points_amount"
        
        case paymentGateway = "payment_gateway"
        case paymentID = "payment_id"
        case orderID = "payment_order_id"
        case paymentSignature = "payment_signature"
        case voucherDetails = "voucher_details"
        case branch_id = "branch_id"
        case currency = "currency"
        case reservation_date = "reservation_date"
        case reservation_time = "reservation_time"
        case country_code = "country_code"
        case mobile_number = "mobile_number"
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CreateBookingBodyKeys.self)
        
        try container.encodeIfPresent(bookingId, forKey: .bookingId)
        
        try container.encodeIfPresent(loyaltyPoints, forKey: .loyaltyPoints)
        try container.encodeIfPresent(loyaltyAmount, forKey: .loyaltyAmount)
        try container.encodeIfPresent(voucherDetails, forKey: .voucherDetails)
        
        try container.encodeIfPresent(paymentGateway, forKey: .paymentGateway)
        try container.encodeIfPresent(paymentID, forKey: .paymentID)
        try container.encodeIfPresent(orderID, forKey: .orderID)
        try container.encodeIfPresent(paymentSignature, forKey: .paymentSignature)
        try container.encodeIfPresent(branch_id, forKey: .branch_id)
        try container.encodeIfPresent(currency, forKey: .currency)
        try container.encodeIfPresent(reservation_date, forKey: .reservation_date)
        try container.encodeIfPresent(reservation_time, forKey: .reservation_time)
        try container.encodeIfPresent(country_code, forKey: .country_code)
        try container.encodeIfPresent(mobile_number, forKey: .mobile_number)
    }
}
