//
 //  Created by Maya R on 12/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQCoporateBookingModel.swift
 //

import Foundation

struct CorporateBookingModel:Codable{
    var verifyCorporate:Bool?
    var corporateOtpId:String
    var corporateOtpExpiry:Double
    
    private enum CorporateBookingKeys: String, CodingKey {
        case verifyCorporate = "verify_corporate"
        case corporateOtpId = "otp_id"
        case corporateOtpExpiry = "otp_expiry"
    }
}

struct CorporateOffersModel:Codable{
    var corporateOffersList:[CorporateOffer]
    private enum CorporateOffersKeys: String, CodingKey {
        case corporateOffersList = "vouchers"
    }
}

struct CorporateOffer : Codable {
    var corporateBarCode:String
    var corporateId:String
    var corporateName:String
    var corporateCurrency:String
    var corporateDenomination:Double
    var corporateDescrition:String
    var corporateMessage:String
    var corporateMessageType:String
    var corporateTerms:[String]
    var corporateTitle:String
    var corporateValidity:String
    var corporateHeaderId:Int
    var corporateVoucherType:String
    var corporateImage:String
    var pax_applicable: Int = 0


    private enum CorporateOfferKeys: String, CodingKey {
        case corporateBarCode = "bar_code"
        case corporateId = "corporate_id"
        case corporateName = "corporate_name"
        case corporateCurrency = "currency"
        case corporateDenomination = "denomination"
        case corporateDescrition = "description"
        case corporateMessage = "message"
        case corporateMessageType = "message_type"
        case corporateTerms = "terms_condition"
        case corporateTitle = "title"
        case corporateValidity = "validity"
        case corporateHeaderId = "voucher_head_id"
        case corporateVoucherType = "voucher_type"
        case corporateImage = "image"
        case pax_applicable = "pax_applicable"
    }
    
    init(voucherCode: String, voucherType: String, voucherAmount: Double,
         corpID : String? = nil,
         corpName : String? = nil,
         corpCurrency : String? = nil,
         corpDescription : String? = nil,
         corpMessage : String? = nil,
         corpMessageType : String? = nil,
         corpTerms : [String]? = nil,
         corpTitle : String? = nil,
         corpValidity : String? = nil,
         corpHeaderID : Int? = nil,
         corpImage : String? = nil, pax_applicable: Int) {
        
        self.corporateBarCode = voucherCode
        self.corporateVoucherType = voucherType
        self.corporateDenomination = voucherAmount
        
        self.corporateId = corpID ?? ""
        self.corporateName = corpName ?? ""
        self.corporateCurrency = corpCurrency ?? ""
        self.corporateDescrition = corpDescription ?? ""
        self.corporateMessage = corpMessage ?? ""
        self.corporateMessageType = corpMessageType ?? ""
        self.corporateTerms = corpTerms ?? [""]
        self.corporateTitle = corpTitle ?? ""
        self.corporateValidity = corpValidity ?? ""
        self.corporateHeaderId = corpHeaderID ?? 0
        self.corporateImage = corpImage ?? ""
        self.pax_applicable = pax_applicable
    }
}

extension CorporateBookingModel{
    init(from decoder:Decoder)throws{
        let corporateInfo = try decoder.container(keyedBy: CorporateBookingKeys.self)
        verifyCorporate = try corporateInfo.decodeIfPresent(Bool.self, forKey: .verifyCorporate) ?? false
        corporateOtpId = try corporateInfo.decodeIfPresent(String.self, forKey: .corporateOtpId) ?? ""
        corporateOtpExpiry = try corporateInfo.decodeIfPresent(Double.self, forKey: .corporateOtpExpiry) ?? 0.0
    }
}

extension CorporateOffersModel{
    init(from decoder:Decoder)throws{
        let corporateOfferInfo = try decoder.container(keyedBy: CorporateOffersKeys.self)
        corporateOffersList = try corporateOfferInfo.decodeIfPresent([CorporateOffer].self, forKey: .corporateOffersList)!
    }
}

extension CorporateOffer{
    init(from decoder:Decoder)throws{
        let corporateInfo = try decoder.container(keyedBy: CorporateOfferKeys.self)

        corporateBarCode = try corporateInfo.decodeIfPresent(String.self, forKey: .corporateBarCode) ?? ""
        corporateId = try corporateInfo.decodeIfPresent(String.self, forKey: .corporateId) ?? ""
        corporateName = try corporateInfo.decodeIfPresent(String.self, forKey: .corporateName) ?? ""
        corporateCurrency = try corporateInfo.decodeIfPresent(String.self, forKey: .corporateCurrency) ?? ""
        corporateDenomination = try corporateInfo.decodeIfPresent(Double.self, forKey: .corporateDenomination) ?? 0.0
        corporateDescrition = try corporateInfo.decodeIfPresent(String.self, forKey: .corporateDescrition) ?? ""
        corporateMessage = try corporateInfo.decodeIfPresent(String.self, forKey: .corporateMessage) ?? ""
        corporateMessageType = try corporateInfo.decodeIfPresent(String.self, forKey: .corporateMessageType) ?? ""
        corporateTerms = try corporateInfo.decodeIfPresent([String].self, forKey: .corporateTerms) ?? []
        corporateTitle = try corporateInfo.decodeIfPresent(String.self, forKey: .corporateTitle) ?? ""
        corporateValidity = try corporateInfo.decodeIfPresent(String.self, forKey: .corporateValidity) ?? ""
        corporateHeaderId = try corporateInfo.decodeIfPresent(Int.self, forKey: .corporateHeaderId) ?? 0
        corporateVoucherType = try corporateInfo.decodeIfPresent(String.self, forKey: .corporateVoucherType) ?? ""
        corporateImage = try corporateInfo.decodeIfPresent(String.self, forKey: .corporateImage) ?? ""
        
    }
}
