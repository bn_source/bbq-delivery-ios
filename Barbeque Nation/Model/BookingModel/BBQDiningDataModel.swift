//
 //  Created by Ajith CP on 12/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQDiningDataModel.swift
 //

import Foundation
import UIKit

// MARK: - BBQDiningDataModel
struct BBQDiningDataModel : Codable {
    
    let bookingID, reservationDate, reservationTime: String?
    let branchName : String?
    var billTotal: Double?
    let billWithoutTax: Double?
    var billWithTaxTotal: Double?
    var netPayable : Double?
    let loyaltyPoints : Int?
    let serviceCharge, serviceChargeAmount, loyaltyPointsAmount: Double?
    let taxes: [Taxes]?
    let showPayment: Bool?
    let advanceAmount: Double?
    let payments: Payments?
    var amenities: [Amenities]?
    var bookingDetails: [RescheduleDetails]?
    var bookingStatus: String?
    var branchAddress: String?
    var branchContactNumber: String?
    var branchId: String?
    var branchImage: String?
    var currency: String?
    var latitude: Double?
    var longitude: Double?
    var slotId: String?
    var numberOfPacks : Int?
    var billLink : String?
    var billWithTax: Double?
    var outletUrl:String?
    var childPax, adultPax: Int?
    var vouchers: [BookingVoucher]?
    var discountDetails: [DiscountDetails]?
    var gstBaseAmount: Double?
    var vatBaseAmount: Double?
    var service_charges: Double?
    var vat_tax: Taxes?
    var cess_tax: Taxes?
    var payment_details: [PaymentDetail]?
    
    enum CodingKeys: String, CodingKey {
        case bookingID = "booking_id"
        case reservationDate = "reservation_date"
        case reservationTime = "reservation_time"
        case branchName = "branch_name"
        case billTotal = "bill_total"
        case netPayable = "net_payable"
        case loyaltyPoints = "loyalty_points"
        case loyaltyPointsAmount = "loyalty_points_amount"
        case serviceCharge = "service_charge"
        case serviceChargeAmount = "service_charge_amount"
        case taxes = "taxes"
        case showPayment = "show_payment"
        case advanceAmount = "advance_amount"
        case payments = "payments"
        case amenities = "amenities"
        case bookingDetails = "booking_details"
        case bookingStatus = "booking_status"
        case branchAddress = "branch_address"
        case branchContactNumber = "branch_contact_no"
        case branchId = "branch_id"
        case branchImage = "branch_image"
        case currency = "currency"
        case latitude = "lat"
        case longitude = "long"
        case slotId = "slot_id"
        case vouchers = "vouchers"
        case numberOfPacks = "packs"
        case billLink = "bill_link"
        case billWithoutTax = "bill_without_tax"
        case outletUrl = "location_url"
        case adultPax = "adultPax"
        case childPax = "childPax"
        case billWithTax = "bill_with_tax"
        case discountDetails = "discount_details"
        case vatBaseAmount = "vat_baseamount"
        case gstBaseAmount = "gst_baseamount"
        case payment_details = "payment_details"
        case service_charges = "service_charges"
        case vat_tax = "vat_tax"
        case cess_tax = "cess_tax"
    }
    
    init(from decoder: Decoder) throws {
        let bookings = try decoder.container(keyedBy: CodingKeys.self)
        
        bookingID = try bookings.decodeIfPresent(String.self, forKey: .bookingID) ?? nil
        reservationDate = try bookings.decodeIfPresent(String.self, forKey: .reservationDate) ?? nil
        reservationTime = try bookings.decodeIfPresent(String.self, forKey: .reservationTime) ?? nil
        branchName = try bookings.decodeIfPresent(String.self, forKey: .branchName) ?? nil
        billTotal = try bookings.decodeIfPresent(Double.self, forKey: .billTotal) ?? nil
        netPayable = try bookings.decodeIfPresent(Double.self, forKey: .netPayable) ?? nil
        loyaltyPoints = try bookings.decodeIfPresent(Int.self, forKey: .loyaltyPoints) ?? nil
        loyaltyPointsAmount = try bookings.decodeIfPresent(Double.self, forKey: .loyaltyPointsAmount) ?? nil
        serviceCharge = try bookings.decodeIfPresent(Double.self, forKey: .serviceCharge) ?? nil
        serviceChargeAmount = try bookings.decodeIfPresent(Double.self, forKey: .serviceChargeAmount) ?? nil
        taxes = try bookings.decodeIfPresent([Taxes].self, forKey: .taxes) ?? nil
        showPayment = try bookings.decodeIfPresent(Bool.self, forKey: .showPayment) ?? nil
        advanceAmount = try bookings.decodeIfPresent(Double.self, forKey: .advanceAmount) ?? 0.0
        payments = try bookings.decodeIfPresent(Payments.self, forKey: .payments) ?? nil
        amenities = try bookings.decodeIfPresent([Amenities].self, forKey: .amenities) ?? nil
        bookingDetails = try bookings.decodeIfPresent([RescheduleDetails].self, forKey: .bookingDetails) ?? nil
        bookingStatus = try bookings.decodeIfPresent(String.self, forKey: .bookingStatus) ?? nil
        branchAddress = try bookings.decodeIfPresent(String.self, forKey: .branchAddress) ?? nil
        branchContactNumber = try bookings.decodeIfPresent(String.self, forKey: .branchContactNumber) ?? nil
        branchId = try bookings.decodeIfPresent(String.self, forKey: .branchId) ?? nil
        branchImage = try bookings.decodeIfPresent(String.self, forKey: .branchImage) ?? nil
        currency = try bookings.decodeIfPresent(String.self, forKey: .currency) ?? nil
        latitude = try bookings.decodeIfPresent(Double.self, forKey: .latitude) ?? 0.0
        longitude = try bookings.decodeIfPresent(Double.self, forKey: .longitude) ?? 0.0
        slotId = try bookings.decodeIfPresent(String.self, forKey: .slotId) ?? nil
        vouchers = try bookings.decodeIfPresent([BookingVoucher].self, forKey: .vouchers) ?? nil
        discountDetails = try bookings.decodeIfPresent([DiscountDetails].self, forKey: .discountDetails) ?? nil
        numberOfPacks = try bookings.decodeIfPresent(Int.self, forKey: .numberOfPacks) ?? nil
        billLink = try bookings.decodeIfPresent(String.self, forKey: .billLink) ?? nil
        billWithoutTax = try bookings.decodeIfPresent(Double.self, forKey: .billWithoutTax) ?? nil
        outletUrl = try bookings.decodeIfPresent(String.self, forKey: .outletUrl) ?? ""
        childPax = try bookings.decodeIfPresent(Int.self, forKey: .childPax) ?? 0
        adultPax = try bookings.decodeIfPresent(Int.self, forKey: .adultPax) ?? 0
        billWithTaxTotal = try bookings.decodeIfPresent(Double.self, forKey: .billWithTax) ?? 0.0
        vatBaseAmount = try bookings.decodeIfPresent(Double?.self, forKey: .vatBaseAmount) ?? 0.0
        gstBaseAmount = try bookings.decodeIfPresent(Double?.self, forKey: .gstBaseAmount) ?? 0.0
        service_charges = try bookings.decodeIfPresent(Double?.self, forKey: .service_charges) ?? 0.0
        payment_details = try bookings.decodeIfPresent([PaymentDetail]?.self, forKey: .payment_details) ?? nil
        
        do {
            vat_tax = try bookings.decodeIfPresent(Taxes?.self, forKey: .vat_tax) ?? nil
            cess_tax = try bookings.decodeIfPresent(Taxes?.self, forKey: .cess_tax) ?? nil
        } catch {
            print("Error decoding vat_tax: \(error)")
        }
    }
    
    func getCessTax() -> Double{
        if (cess_tax?.taxAmount ?? 0.0) != 0{
            return (cess_tax?.taxPercentage ?? 0.0) > 0 ? cess_tax?.taxPercentage ?? 0 : vat_tax?.taxPercentage ?? 0
        }
        return 0
    }
    
    func getStatus() -> (String, UIColor, Int) {
        if bookingStatus == "CONFIRMED" || bookingStatus == "EXPECTED" || bookingStatus == "Rescheduled" || bookingStatus == "UPDATE RESERVATION"{
            return ("Upcoming", UIColor(red: 255.0/255.0, green: 136.0/255.0, blue: 0.0/255.0, alpha: 1.0), 0)
        }else if bookingStatus == "ARRIVED" || bookingStatus == "SEATED"{
            return ("On Going", UIColor(red: 255.0/255.0, green: 136.0/255.0, blue: 0.0/255.0, alpha: 1.0), 1)
        }else if bookingStatus == "PRE_RECEIPT"{
            return ("Unpaid", UIColor.red, 2)
        }else if bookingStatus == "SETTLED"{
            return ("Paid", UIColor(red: 5.0/255.0, green: 166.0/255.0, blue: 96.0/255.0, alpha: 1.0), 3)
        }else if bookingStatus == "CANCELLED" || bookingStatus == "NO SHOW"{
            return ("Cancelled", UIColor(red: 60.0/255.0, green: 72.0/255.0, blue: 88.0/255.0, alpha: 1.0), 4)
        }else{
            return (bookingStatus ?? "", UIColor(red: 255.0/255.0, green: 136.0/255.0, blue: 0.0/255.0, alpha: 1.0), 4)
        }
    }
    
    func getTaxes() -> [TaxBreakUp] {
        var taxList = [TaxBreakUp]()
        for list in taxes ?? [Taxes]() {
            taxList.append(TaxBreakUp(label: list.tax ?? "", value: String(format: "%.2f", list.taxAmount ?? 0)))
        }
        if (vat_tax?.taxAmount ?? 0) > 0{
            taxList.append(TaxBreakUp(label: vat_tax?.tax ?? "", value: String(format: "%.2f", vat_tax?.taxAmount ?? 0)))
        }
        if (cess_tax?.taxAmount ?? 0) > 0{
            taxList.append(TaxBreakUp(label: cess_tax?.tax ?? "", value: String(format: "%.2f", cess_tax?.taxAmount ?? 0)))
        }
        
        return taxList
    }
    
    func getOfferDiscount() -> (total: Double, discount_details: [DiscountDetails], breakUP: [AfterDiningData.BreakUp]) {
        var total = 0.0
        var discount_details = [DiscountDetails]()
        var breakUp = [AfterDiningData.BreakUp]()
        for list in discountDetails ?? [DiscountDetails]() {
            if let type = list.type, type != "GIFT_COUPON", type != "INVOICE", type != "GIFT_VOUCHER"{
                total += list.amount ?? 0.0
                discount_details.append(list)
                breakUp.append(AfterDiningData.BreakUp(title: list.name ?? "Offer", value: String(format: "%.02f", list.amount ?? 0)))
            }
        }
        return (total, discount_details, breakUp)
    }
    
    func getCouponDiscount() -> (total: Double, discount_details: [BookingVoucher], breakUP: [AfterDiningData.BreakUp]) {
        var total = 0.0
        var discount_details = [BookingVoucher]()
        var breakUp = [AfterDiningData.BreakUp]()
        for list in vouchers ?? [BookingVoucher]() {
            if list.voucherType == "GC"{
                total += list.amount ?? 0
                discount_details.append(list)
                breakUp.append(AfterDiningData.BreakUp(title: list.title ?? "Coupon", value: String(format: "%.02f", list.amount ?? 0)))
            }
        }
        return (total, discount_details, breakUp)
    }
    
    func getPaidAmount() -> ([AfterDiningData.BreakUp], Double){
        var paid_amount = 0.0
        var paidArray = [AfterDiningData.BreakUp]()
        for payment in payment_details ?? [PaymentDetail](){
            if payment.type != "INVOICE", payment.type != "COUPON", payment.type != "GIFT_COUPON", payment.type != "GIFT_VOUCHER"{
                paid_amount += payment.amount ?? 0.0
                paidArray.append(AfterDiningData.BreakUp(title: payment.name ?? "", value: String(format: "%.02f", payment.amount ?? 0.0)))
            }
        }
        return (paidArray, paid_amount)
    }
    
    func getPaymentAmountInDetailScreen() -> Double{
        if bookingStatus == "PRE_RECEIPT" || bookingStatus == "SETTLED"{
            return getPaidAmount().1
        }else{
            return payments?.amountPaid ?? 0.0
        }
    }
    
    func getVoucherDiscount() -> (total: Double, discount_details: [BookingVoucher], breakUP: [AfterDiningData.BreakUp]) {
        var total = 0.0
        var discount_details = [BookingVoucher]()
        var breakUp = [AfterDiningData.BreakUp]()
        for list in vouchers ?? [BookingVoucher]() {
            if list.voucherType == "GV"{
                total += list.amount ?? 0
                discount_details.append(list)
                breakUp.append(AfterDiningData.BreakUp(title: list.title ?? "Voucher", value: String(format: "%.02f", list.amount ?? 0)))
            }
        }
        return (total, discount_details, breakUp)
    }
    
    
    func getPaidAmounts() -> (total: Double, discount_details: [PaymentsTransactions], breakUP: [AfterDiningData.BreakUp]) {
        var discount_details = [PaymentsTransactions]()
        var breakUp = [AfterDiningData.BreakUp]()
        if let payments = payments, let amount = payments.amountPaid, amount > 0{
            for transactions in payments.transactions{
                discount_details.append(transactions)
                breakUp.append(AfterDiningData.BreakUp(title: transactions.payment_id ?? "Payment", value: String(format: "%.02f", transactions.paid_amount ?? 0)))
            }
        }
        return (payments?.amountPaid ?? 0.0, discount_details, breakUp)
    }
    
    func getVoucherAmount() -> Double {
        var total = 0.0
        for list in vouchers ?? [BookingVoucher]() {
            total += list.amount ?? 0.0
        }
        return total
    }
    
    func getPaidDetails() -> Double {
        var total = 0.0
        for list in payment_details ?? [PaymentDetail]() {
            total += list.amount ?? 0.0
        }
        return total
    }
    
    func getVouchers() -> [BookingVoucher] {
        var arrVouchers = [BookingVoucher]()
        for list in vouchers ?? [BookingVoucher]() {
            if list.voucherType == "GV"{
                arrVouchers.append(list)
            }
        }
        return arrVouchers
    }
    
    func getCoupons() -> [BookingVoucher] {
        var arrVouchers = [BookingVoucher]()
        for list in vouchers ?? [BookingVoucher]() {
            if list.voucherType == "GC"{
                arrVouchers.append(list)
            }
        }
        return arrVouchers
    }
    
    
    func getVouchersAmount() -> Double {
        var arrVouchers: Double = 0
        for list in vouchers ?? [BookingVoucher]() {
            if list.voucherType == "GV"{
                arrVouchers += list.amount ?? 0
            }
        }
        return arrVouchers
    }
    
    func getCouponsAmount() -> Double {
        var arrVouchers: Double = 0
        for list in vouchers ?? [BookingVoucher]() {
            if list.voucherType == "GC"{
                arrVouchers += list.amount ?? 0
            }
        }
        return arrVouchers
    }
    
    func getOfferDiscounts() -> [DiscountDetails] {
        var arrDiscounts = [DiscountDetails]()
        for list in discountDetails ?? [DiscountDetails]() {
            if let type = list.type, type != "GIFT_COUPON", type != "INVOICE", type != "GIFT_VOUCHER"{
                arrDiscounts.append(list)
            }
        }
        return arrDiscounts
    }
    
    func getDiscountsAmount() -> Double {
        var amount: Double = 0
        if bookingStatus == "PRE_RECEIPT" || bookingStatus == "SETTLED"{
            for list in discountDetails ?? [DiscountDetails]() {
                if let type = list.type, type != "INVOICE", type != "GIFT_VOUCHER"{
                    amount += list.amount ?? 0.0
                }
            }
        }else{
            return getCouponsAmount()
        }
        if amount == 0{
            return getCouponsAmount()
        }
        return amount
    }
    
    func getOfferDiscountDetails() -> Double {
        var total = 0.0
        for list in discountDetails ?? [DiscountDetails]() {
            if let type = list.type, type != "GIFT_COUPON", type != "INVOICE", type != "GIFT_VOUCHER"{
                total += list.amount ?? 0.0
            }
        }
        return total
    }
    
    func getCouponOfferDiscountDetails() -> Double {
        var total = 0.0
        for list in discountDetails ?? [DiscountDetails]() {
            if let type = list.type, type != "GIFT_VOUCHER"{
                total += list.amount ?? 0.0
            }
        }
        return total
    }
    
    func isBookingSettled() -> Bool{
        if bookingStatus == "PRE_RECEIPT"{
            if let amount = netPayable, amount == 0{
                return true
            }else if let billWithTaxTotal = billWithTaxTotal{
                if let amount = netPayable, amount > 0, (billWithTaxTotal - getCouponOfferDiscountDetails() - getPaidDetails()).rounded(.down) <= 0{
                    return true
                }
            }
        }
        return false
    }
}

// MARK: - Payments
struct Payments: Codable {
    let amountPaid, amountRefund: Double?
    let transactions: [PaymentsTransactions]
    
    enum CodingKeys: String, CodingKey {
        case amountPaid = "amount_paid"
        case amountRefund = "amount_refund"
        case transactions = "transactions"
    }
    
    init(from decoder: Decoder) throws {
        let payment = try decoder.container(keyedBy: CodingKeys.self)
        
        amountPaid = try payment.decodeIfPresent(Double.self, forKey: .amountPaid) ?? 0.0
        amountRefund = try payment.decodeIfPresent(Double.self, forKey: .amountRefund) ?? 0.0
        transactions = try payment.decodeIfPresent([PaymentsTransactions].self, forKey: .transactions) ?? [PaymentsTransactions]()
    }
}

struct PaymentsTransactions: Codable {
    let payment_id, transaction_status: String?
    let paid_amount: Double?
    
    enum CodingKeys: String, CodingKey {
        case payment_id = "payment_id"
        case transaction_status = "transaction_status"
        case paid_amount = "paid_amount"
    }
    
    init(from decoder: Decoder) throws {
        let payment = try decoder.container(keyedBy: CodingKeys.self)
        
        payment_id = try payment.decodeIfPresent(String.self, forKey: .payment_id) ?? ""
        transaction_status = try payment.decodeIfPresent(String.self, forKey: .transaction_status) ?? ""
        paid_amount = try payment.decodeIfPresent(Double.self, forKey: .paid_amount) ?? 0.0
    }
}


// MARK: - Tax
struct Tax: Codable {
    let tax: String?
    let taxPercentage: Double?
    let taxAmount: Double?
    
    enum CodingKeys: String, CodingKey {
        case tax = "tax"
        case taxPercentage = "tax_percentage"
        case taxAmount = "tax_amount"
    }
    
    init(from decoder: Decoder) throws {
        let taxStructure = try decoder.container(keyedBy: CodingKeys.self)
        
        tax = try taxStructure.decodeIfPresent(String.self, forKey: .tax) ?? nil
        taxPercentage = try taxStructure.decodeIfPresent(Double.self, forKey: .taxPercentage) ?? 0.0
        taxAmount = try taxStructure.decodeIfPresent(Double.self, forKey: .taxAmount) ?? nil
    }
}

// MARK: - Discount Details
struct DiscountDetails: Codable {
    let name: String?
    let type: String?
    let coupon_number: String?
    let amount: Double?
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case type = "type"
        case coupon_number = "coupon_number"
        case amount = "amount"
    }
    
    init(from decoder: Decoder) throws {
        let taxStructure = try decoder.container(keyedBy: CodingKeys.self)
        
        name = try taxStructure.decodeIfPresent(String.self, forKey: .name) ?? nil
        type = try taxStructure.decodeIfPresent(String.self, forKey: .type) ?? nil
        coupon_number = try taxStructure.decodeIfPresent(String.self, forKey: .coupon_number) ?? nil
        amount = try taxStructure.decodeIfPresent(Double.self, forKey: .amount) ?? 0.0
    }
}

// MARK: - Payment Details
struct PaymentDetail: Codable {
    let name: String?
    let type: String?
    let voucher_number: String?
    let amount: Double?
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case type = "type"
        case voucher_number = "voucher_number"
        case amount = "amount"
    }
    
    init(from decoder: Decoder) throws {
        let taxStructure = try decoder.container(keyedBy: CodingKeys.self)
        
        name = try taxStructure.decodeIfPresent(String.self, forKey: .name) ?? nil
        type = try taxStructure.decodeIfPresent(String.self, forKey: .type) ?? nil
        voucher_number = try taxStructure.decodeIfPresent(String.self, forKey: .voucher_number) ?? nil
        amount = try taxStructure.decodeIfPresent(Double.self, forKey: .amount) ?? 0.0
    }
}

            



// MARK: - Vouchers
struct BookingVoucher: Codable {
    let title: String?
    let voucherType: String?
    let voucherCode: String?
    let amount: Double?
    let currency: String?
    let isCorpVoucher : Int?
    
    enum BookingVoucherCodingKeys: String, CodingKey {
        case title = "title"
        case voucherType = "voucher_type"
        case voucherCode = "voucher_code"
        case amount = "amount"
        case currency = "currency"
        case isCorpVoucher = "corporate_voucher"
    }
    
    init(from decoder: Decoder) throws {
        let bookingVoucherKey = try decoder.container(keyedBy: BookingVoucherCodingKeys.self)
        title = try bookingVoucherKey.decodeIfPresent(String.self, forKey: .title) ?? nil
        voucherType = try bookingVoucherKey.decodeIfPresent(String.self, forKey: .voucherType) ?? nil
        voucherCode = try bookingVoucherKey.decodeIfPresent(String.self, forKey: .voucherCode) ?? nil
        amount = try bookingVoucherKey.decodeIfPresent(Double.self, forKey: .amount) ?? nil
        currency = try bookingVoucherKey.decodeIfPresent(String.self, forKey: .currency) ?? nil
        isCorpVoucher = try bookingVoucherKey.decodeIfPresent(Int.self, forKey: .isCorpVoucher) ?? nil
    }
}

struct RescheduleDetails: Codable {
    var menuId: Int?
    var buffetId: Int?
    var packs: Int?
    var buffetName: String?
    var buffetType: String?
    var price: Double?
    var totalPrice: Double?
    
    private enum RescheduleDetailsKeys: String, CodingKey {
        case menuId = "menu_id"
        case buffetId = "buffet_id"
        case packs = "packs"
        case buffetName = "buffet_name"
        case buffetType = "buffet_type"
        case price = "price"
        case totalPrice = "total_price"
    }
    
    init(from decoder:Decoder) throws {
        let bookingDetailsInfo = try decoder.container(keyedBy: RescheduleDetailsKeys.self)
        menuId = try bookingDetailsInfo.decodeIfPresent(Int.self, forKey: .menuId) ?? 0
        buffetId = try bookingDetailsInfo.decodeIfPresent(Int.self, forKey: .buffetId) ?? 0
        packs = try bookingDetailsInfo.decodeIfPresent(Int.self, forKey: .packs) ?? 0
        buffetName = try bookingDetailsInfo.decodeIfPresent(String.self, forKey: .buffetName) ?? ""
        buffetType = try bookingDetailsInfo.decodeIfPresent(String.self, forKey: .buffetType) ?? ""
        price = try bookingDetailsInfo.decodeIfPresent(Double.self, forKey: .price) ?? 0.00
        totalPrice = try bookingDetailsInfo.decodeIfPresent(Double.self, forKey: .totalPrice) ?? 0.00
    }
}
