//
 //  Created by Rabindra L on 01/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQCancelReasonsModel.swift
 //

import Foundation

struct BBQCancelReasonsModel: Codable {
    var id:Int?
    var reason:String?
    
    private enum BBQCancelReasonsModelKeys: String, CodingKey {
        case id = "id"
        case reason = "reason"
    }
}

extension BBQCancelReasonsModel {
    init(from decoder: Decoder) throws {
        let cancelReason = try decoder.container(keyedBy: BBQCancelReasonsModelKeys.self)
        id = try cancelReason.decodeIfPresent(Int.self, forKey: .id) ?? nil
        reason = try cancelReason.decodeIfPresent(String.self, forKey: .reason) ?? nil
    }
}
