//
 //  Created by Sridhar on 25/08/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQBookingModel.swift
 //

import Foundation

// MARK: - Get Slots API Starts
// MARK: - PreferredBranch and NearByBranch
struct BookingSlotsWithBranchDataModel: Codable {
    let preferredBranch: PreferredBranch?
    let nearbyBranches: [NearByBranches]?
    
    private enum BookingSlotsWithBranchDataKeys: String, CodingKey {
        case preferredBranch = "preferred_branch"
        case nearbyBranches = "near_by_branches"
    }
}

// MARK: - PreferredBranch
struct PreferredBranch:Codable {
    let branchId: String?
    let branchName: String?
    let amenities: [Amenities]?
    let branchImages: String?
    let slotsavailable: [SlotsAvailable]?
    let taxes: [Taxes]?
    let serviceCharge: Double?
    let terms: String?
    
    private enum preferredBranchCodingKeys: String, CodingKey {
        case branchId = "branch_id"
        case branchName = "branch_name"
        case amenities = "amenities"
        case branchImages = "branch_images"
        case slotsavailable = "slots_available"
        case taxes = "taxes"
        case serviceCharge = "service_charge"
        case terms = "terms"
    }
}

// MARK: - Amenities
struct Amenities:Codable {
    var name: String?
    var description: String?
    var image: String?
    
    private enum amenitiesCodingKeys: String, CodingKey {
        case name = "name"
        case description = "description"
        case image = "image"
    }
}

// MARK: - SlotsAvailable
struct SlotsAvailable:Codable, Equatable {
    var slotId: Int?
    var slotDesc: String?
    var diningType: String?
    var slotStartTime: String?
    var slotStopTime: String?
    var totalCapacity: Int?
    var availableCapacity: Int?
    var isFlexiSlot : Bool?
    var isEarlyBird :  Bool?
    var isLateBird : Bool?
    
    private enum slotsavailableCodingKeys: String, CodingKey {
        case slotId = "slot_id"
        case slotDesc = "slot_desc"
        case diningType = "dining_type"
        case slotStartTime = "slot_start_time"
        case slotStopTime = "slot_stop_time"
        case totalCapacity = "total_capacity"
        case availableCapacity = "available_capacity"
        case isFlexiSlot = "flexy_slot"
        case isEarlyBird = "early_bird"
        case isLateBird = "late_bird"
        
    }
}

// MARK: - TaxDetails
struct Taxes:Codable {
    var tax:String?
    var taxPercentage:Double?
    var taxAmount: Double?
    //let servicecharge: Double?
    
    private enum taxesCodingKeys: String, CodingKey {
        case tax = "tax"
        case taxPercentage = "tax_percentage"
        case taxAmount = "tax_amount"
        //case servicecharge = "Service Charges"
    }
    
    mutating func updateTaxPer(tax_per: Double?){
        self.taxPercentage = tax_per
    }
}

struct AmountRowData {
    var amountTitle : String?
    var amountValue : String?
}

// MARK: - NearByBranches
struct NearByBranches: Codable {
    var branchId: String?
    var branchName: String?
    var distance: Double?
    
    private enum nearbybranchesCodingKeys: String, CodingKey {
        case branchId = "branch_id"
        case branchName = "branch_name"
        case distance = "distance"
    }
}

// MARK: - Decoder of PreferredBranch and NearByBranches
extension BookingSlotsWithBranchDataModel {
    init(from decoder: Decoder) throws {
        let bookingSlotsWithBranchInfo = try decoder.container(keyedBy: BookingSlotsWithBranchDataKeys.self)
        preferredBranch = try bookingSlotsWithBranchInfo.decodeIfPresent(PreferredBranch.self, forKey: .preferredBranch) ?? nil
        nearbyBranches = try bookingSlotsWithBranchInfo.decodeIfPresent([NearByBranches].self, forKey: .nearbyBranches) ?? nil
    }
}

// MARK: - Decoder of NearByBranches
extension NearByBranches {
    init(from decoder: Decoder) throws {
        let nearbyBranchesInfo = try decoder.container(keyedBy: nearbybranchesCodingKeys.self)
        branchId = try nearbyBranchesInfo.decodeIfPresent(String.self, forKey: .branchId) ?? ""
        branchName = try nearbyBranchesInfo.decodeIfPresent(String.self, forKey: .branchName) ?? ""
        distance = try nearbyBranchesInfo.decodeIfPresent(Double.self, forKey: .distance) ?? 0.0
    }
}

// MARK: - Decoder of PreferredBranch
extension PreferredBranch {
    init(from decoder:Decoder) throws {
        let preferredBranchInfo = try decoder.container(keyedBy: preferredBranchCodingKeys.self)
        branchId = try preferredBranchInfo.decodeIfPresent(String.self, forKey: .branchId) ?? ""
        branchName = try preferredBranchInfo.decodeIfPresent(String.self, forKey: .branchName) ?? ""
        amenities = try preferredBranchInfo.decodeIfPresent([Amenities].self, forKey: .amenities) ?? nil
        branchImages = try preferredBranchInfo.decodeIfPresent(String.self, forKey: .branchImages) ?? ""
        slotsavailable = try preferredBranchInfo.decodeIfPresent([SlotsAvailable].self, forKey: .slotsavailable) ?? nil
        taxes = try preferredBranchInfo.decodeIfPresent([Taxes].self, forKey: .taxes) ?? nil
        serviceCharge = try preferredBranchInfo.decodeIfPresent(Double.self, forKey: .serviceCharge) ?? 0.0
        terms = try preferredBranchInfo.decodeIfPresent(String.self, forKey: .terms) ?? ""
    }
}

// MARK: - Decoder of Amenities
extension Amenities {
    init(from decoder: Decoder) throws {
        let amenitiesInfo = try decoder.container(keyedBy: amenitiesCodingKeys.self)
        name = try amenitiesInfo.decodeIfPresent(String.self, forKey: .name) ?? ""
        description = try amenitiesInfo.decodeIfPresent(String.self, forKey: .description) ?? ""
        image = try amenitiesInfo.decodeIfPresent(String.self, forKey: .image) ?? ""
    }
}

// MARK: - Decoder of SlotsAvailable
extension SlotsAvailable {
    init(from decoder: Decoder) throws {
        let slotsAvailableInfo = try decoder.container(keyedBy: slotsavailableCodingKeys.self)
        slotId = try slotsAvailableInfo.decodeIfPresent(Int.self, forKey: .slotId) ?? 0
        slotDesc = try slotsAvailableInfo.decodeIfPresent(String.self, forKey: .slotDesc) ?? ""
        diningType = try slotsAvailableInfo.decodeIfPresent(String.self, forKey: .diningType) ?? ""
        slotStartTime = try slotsAvailableInfo.decodeIfPresent(String.self, forKey: .slotStartTime) ?? ""
        slotStopTime = try slotsAvailableInfo.decodeIfPresent(String.self, forKey: .slotStopTime) ?? ""
        totalCapacity = try slotsAvailableInfo.decodeIfPresent(Int.self, forKey: .totalCapacity) ?? 0
        availableCapacity = try slotsAvailableInfo.decodeIfPresent(Int.self, forKey: .availableCapacity) ?? 0
        isFlexiSlot = try slotsAvailableInfo.decodeIfPresent(Bool.self, forKey: .isFlexiSlot)
        isEarlyBird = try slotsAvailableInfo.decodeIfPresent(Bool.self, forKey: .isEarlyBird)
        isLateBird = try slotsAvailableInfo.decodeIfPresent(Bool.self, forKey: .isLateBird)
    }
}

// MARK: - Decoder of TaxDetails
extension Taxes {
    init(from decoder: Decoder) throws {
        let taxesInfo = try decoder.container(keyedBy: taxesCodingKeys.self)
        tax = try taxesInfo.decodeIfPresent(String.self, forKey: .tax) ?? ""
        taxPercentage = try taxesInfo.decodeIfPresent(Double.self, forKey: .taxPercentage) ?? 0.0
        taxAmount = try taxesInfo.decodeIfPresent(Double.self, forKey: .taxAmount) ?? 0.0
    }
}
// MARK: - Get Slots API Ends

// MARK: - Get MenuBranchBuffet API Starts
// MARK: - MenuBranchBuffet
struct MenuBranchBuffetDataModel:Codable {
    let menuId: String?
    let menuName: String?
    let buffets: Buffets?
    let beverageData : [Beverages]?
    
    private enum menubranchbuffetCodingKeys: String, CodingKey {
        case menuId = "menu_id"
        case menuName = "menu_name"
        case buffets = "buffets"
        case beverageData = "beverage_data"
    }
}

// MARK: - Beverages

struct Beverages : Codable {
    let menuItems : [MenuItems]?
    let menuTitle : String?
    let menuDescription : String?
    
    private enum beveragesCodingKeys: String, CodingKey {
        case menuItems = "menu_items"
        case menuTitle = "menu_title"
        case menuDescription = "menu_description"
    }
    
    init(from decoder:Decoder) throws {
        let beverageInfo = try decoder.container(keyedBy: beveragesCodingKeys.self)
       
        menuItems = try beverageInfo.decodeIfPresent([MenuItems].self, forKey: .menuItems) ?? nil
        menuTitle = try beverageInfo.decodeIfPresent(String.self, forKey: .menuTitle) ?? ""
        menuDescription = try beverageInfo.decodeIfPresent(String.self, forKey: .menuDescription) ?? ""
    }
    
}

// MARK: - Buffets
struct Buffets:Codable {
    var buffetTitle: String?
    var buffetDescription: String?
    var buffetData: [BuffetData]?
    
    private enum buffetsCodingKeys: String, CodingKey {
        case buffetTitle = "buffet_title"
        case buffetDescription = "buffet_description"
        case buffetData = "buffet_data"
    }
}

// MARK: - BuffetData
struct BuffetData:Codable {
    var buffetId: String?
    var buffetName:String?
    var buffetDescription:String?
    var buffetPrice:Double?
    var buffetCurrency:String?
    var buffetType:String?
    var buffetImage:String?
    var buffetItems:[BuffetItems]?
    
    private enum buffetsdataCodingKeys: String, CodingKey {
        case buffetId = "buffet_id"
        case buffetName = "buffet_name"
        case buffetDescription = "buffet_description"
        case buffetPrice = "buffet_price"
        case buffetCurrency = "buffet_currency"
        case buffetType = "buffet_type"
        case buffetImage = "buffet_image"
        case buffetItems = "buffet_items"
    }
}

// MARK: - BuffetItems
struct BuffetItems:Codable {
    var buffetItems_name: String?
    var buffetItems_menuItems: [MenuItems]?

    private enum buffetitemsCodingKeys: String, CodingKey {
        case buffetItems_name = "name"
        case buffetItems_menuItems = "menu_items"
    }
}

// MARK: - MenuItems
struct MenuItems:Codable {
    var menuitems_Id: String?
    var menuitems_Name: String?
    var menuitems_Tags: Tags?
    var menuitems_Type: String?
    var menuitems_Image: String?
    var menuitems_Category: String?
    var menuitems_Currency: String?
    var menuitems_POSCode: String?
    var menuitems_Description: String?
    var menuitems_DefaultPrice: String?
    var menuitems_SaleTypeAlaCarte: String?
    
    private enum menuitemsCodingKeys: String, CodingKey {
        case menuitems_Id = "id"
        case menuitems_Name = "name"
        case menuitems_Tags = "tags"
        case menuitems_Type = "type"
        case menuitems_Image = "image"
        case menuitems_Category = "category"
        case menuitems_Currency = "currency"
        case menuitems_POSCode = "pos_code"
        case menuitems_Description = "description"
        case menuitems_DefaultPrice = "default_price"
        case menuitems_SaleTypeAlaCarte = "sale_type_ala_carte"
    }
}

// MARK: - Tags
struct Tags: Codable {
    var tagId: Int?
    var tagName: String?
    var tagImage: String?
    
    private enum tagsCodingKeys: String, CodingKey {
        case tagId = "id"
        case tagName = "name"
        case tagImage = "image"
    }
}

// MARK: - Decoder of Tags
extension Tags {
    init(from decoder: Decoder) throws {
        let itemTagInfo = try decoder.container(keyedBy: tagsCodingKeys.self)
        tagId = try itemTagInfo.decodeIfPresent(Int.self, forKey: .tagId) ?? 0
        tagName = try itemTagInfo.decodeIfPresent(String.self, forKey: .tagName) ?? ""
        tagImage = try itemTagInfo.decodeIfPresent(String.self, forKey: .tagImage) ?? ""
    }
}

// MARK: - Decoder of MenuItems
extension MenuItems {
    init(from decoder:Decoder) throws {
        let menuitemsInfo = try decoder.container(keyedBy: menuitemsCodingKeys.self)
        menuitems_Id = try menuitemsInfo.decodeIfPresent(String.self, forKey: .menuitems_Id) ?? ""
        menuitems_Name = try menuitemsInfo.decodeIfPresent(String.self, forKey: .menuitems_Name) ?? ""
        menuitems_Tags = try menuitemsInfo.decodeIfPresent(Tags.self, forKey: .menuitems_Tags) ?? nil
        menuitems_Type = try menuitemsInfo.decodeIfPresent(String.self, forKey: .menuitems_Type) ?? ""
        menuitems_Image = try menuitemsInfo.decodeIfPresent(String.self, forKey: .menuitems_Image) ?? ""
        menuitems_Category = try menuitemsInfo.decodeIfPresent(String.self, forKey: .menuitems_Category) ?? ""
        menuitems_Currency = try menuitemsInfo.decodeIfPresent(String.self, forKey: .menuitems_Currency) ?? ""
        menuitems_POSCode = try menuitemsInfo.decodeIfPresent(String.self, forKey: .menuitems_POSCode) ?? ""
        menuitems_Description = try menuitemsInfo.decodeIfPresent(String.self, forKey: .menuitems_Description) ?? ""
        menuitems_DefaultPrice = try menuitemsInfo.decodeIfPresent(String.self, forKey: .menuitems_DefaultPrice) ?? ""
        menuitems_SaleTypeAlaCarte = try menuitemsInfo.decodeIfPresent(String.self, forKey: .menuitems_SaleTypeAlaCarte) ?? ""
    }
}

// MARK: - Decoder of BuffetItems
extension BuffetItems {
    init(from decoder:Decoder) throws {
        let buffetitemsInfo = try decoder.container(keyedBy: buffetitemsCodingKeys.self)
        buffetItems_menuItems = try buffetitemsInfo.decodeIfPresent([MenuItems].self, forKey: .buffetItems_menuItems) ?? nil
        buffetItems_name = try buffetitemsInfo.decodeIfPresent(String.self, forKey: .buffetItems_name) ?? ""
    }
}

// MARK: - Decoder of BuffetData
extension BuffetData {
    init(from decoder:Decoder) throws {
        let buffetdataInfo = try decoder.container(keyedBy: buffetsdataCodingKeys.self)
        buffetId = try buffetdataInfo.decodeIfPresent(String.self, forKey: .buffetId) ?? ""
        buffetName = try buffetdataInfo.decodeIfPresent(String.self, forKey: .buffetName) ?? ""
        buffetDescription = try buffetdataInfo.decodeIfPresent(String.self, forKey: .buffetDescription) ?? ""
        //buffetPrice = try buffetdataInfo.decodeIfPresent(Double.self, forKey: .buffetPrice) ?? 0.0
        
        if let value = try? buffetdataInfo.decodeIfPresent(Int.self, forKey: .buffetPrice) {
            buffetPrice = Double(value)
        } else if let value = try? buffetdataInfo.decodeIfPresent(String.self, forKey: .buffetPrice) {
            buffetPrice = (value as NSString).doubleValue
        } else {
            buffetPrice = try buffetdataInfo.decodeIfPresent(Double.self, forKey: .buffetPrice)
        }
        
        buffetCurrency = try buffetdataInfo.decodeIfPresent(String.self, forKey: .buffetCurrency) ?? ""
        buffetType = try buffetdataInfo.decodeIfPresent(String.self, forKey: .buffetType) ?? ""
        buffetImage = try buffetdataInfo.decodeIfPresent(String.self, forKey: .buffetImage) ?? ""
        buffetItems = try buffetdataInfo.decodeIfPresent([BuffetItems].self, forKey: .buffetItems) ?? nil
    }
}

// MARK: - Decoder of Buffets
extension Buffets {
    init(from decoder:Decoder) throws {
        let buffetsInfo = try decoder.container(keyedBy: buffetsCodingKeys.self)
        buffetTitle = try buffetsInfo.decodeIfPresent(String.self, forKey: .buffetTitle) ?? ""
        buffetDescription = try buffetsInfo.decodeIfPresent(String.self, forKey: .buffetDescription) ?? ""
        buffetData = try buffetsInfo.decodeIfPresent([BuffetData].self, forKey: .buffetData) ?? nil
    }
}

// MARK: - Decoder of MenuBranchBuffet
extension MenuBranchBuffetDataModel {
    init(from decoder:Decoder) throws {
        let menubranchbuffetInfo = try decoder.container(keyedBy: menubranchbuffetCodingKeys.self)
        menuId = try menubranchbuffetInfo.decodeIfPresent(String.self, forKey: .menuId) ?? ""
        menuName = try menubranchbuffetInfo.decodeIfPresent(String.self, forKey: .menuName) ?? ""
        buffets = try menubranchbuffetInfo.decodeIfPresent(Buffets.self, forKey: .buffets) ?? nil
        beverageData = try menubranchbuffetInfo.decodeIfPresent([Beverages].self, forKey: .beverageData) ?? nil
    }
}
// MARK: - Get MenuBranchBuffet API Ends

// MARK: - Get CreateBooking API Starts
// MARK: - CreateBooking
struct CreateBookingDataModel: Codable {
    var bookingId: String?
    var bookingStatus: String?
    var branchName: String?
    var branchAddress: String?
    var branchContactNumber: String?
    var branchImage: String?
    var lat: Double?
    var long: Double?
    var amenities: [Amenities]?
    var bookingDetails: [BookingDetails]?
    var billTotal: Double?
    var advanceAmount: Double?
    var billWithoutTax: Double?
    var serviceCharge: Double?
    var serviceChargeAmount: Double?
    var loyaltyPoints: Int?
    var loyaltyPointsAmount: Int?
    var branchId: String?
    var currency: String?
    var bookingTaxes: [BookingTaxes]?
    var voucherDetails: [BookingVoucherDetails]?
    let fieldErrors : [FieldErrors]?
    
    private enum CreateBookingDataKeys: String, CodingKey {
        case bookingId = "booking_id"
        case bookingStatus = "booking_status"
        case branchName = "branch_name"
        case branchAddress = "branch_address"
        case branchContactNumber = "branch_contact_no"
        case branchImage = "branch_image"
        case lat = "lat"
        case long = "long"
        case amenities = "amenities"
        case bookingDetails = "booking_details"
        case billTotal = "bill_total"
        case advanceAmount = "advance_amount"
        case billWithoutTax = "bill_without_tax"
        case serviceCharge = "service_charge"
        case serviceChargeAmount = "service_charge_amount"
        case loyaltyPoints = "loyalty_points"
        case loyaltyPointsAmount = "loyalty_points_amount"
        case branchId = "branch_id"
        case currency = "currency"
        case bookingTaxes = "taxes"
        case voucherDetails = "voucher_details"
        case fieldErrors = "fieldErrors"
    }
}

// MARK: - Decoder of CreateBooking
extension CreateBookingDataModel {
    init(from decoder:Decoder) throws {
        let createBookingInfo = try decoder.container(keyedBy: CreateBookingDataKeys.self)
        bookingId = try createBookingInfo.decodeIfPresent(String.self, forKey: .bookingId) ?? ""
        bookingStatus = try createBookingInfo.decodeIfPresent(String.self, forKey: .bookingStatus) ?? ""
        branchName = try createBookingInfo.decodeIfPresent(String.self, forKey: .branchName) ?? ""
        branchAddress = try createBookingInfo.decodeIfPresent(String.self, forKey: .branchAddress) ?? ""
        branchContactNumber = try createBookingInfo.decodeIfPresent(String.self, forKey: .branchContactNumber) ?? ""
        branchImage = try createBookingInfo.decodeIfPresent(String.self, forKey: .branchImage) ?? ""
        lat = try createBookingInfo.decodeIfPresent(Double.self, forKey: .lat) ?? 0.0
        long = try createBookingInfo.decodeIfPresent(Double.self, forKey: .long) ?? 0.0
        amenities = try createBookingInfo.decodeIfPresent([Amenities].self, forKey: .amenities) ?? nil
        bookingDetails = try createBookingInfo.decodeIfPresent([BookingDetails].self, forKey: .bookingDetails) ?? nil
        billTotal = try createBookingInfo.decodeIfPresent(Double.self, forKey: .billTotal) ?? 0.0
        advanceAmount = try createBookingInfo.decodeIfPresent(Double.self, forKey: .advanceAmount) ?? 0.0
        billWithoutTax = try createBookingInfo.decodeIfPresent(Double.self, forKey: .billWithoutTax) ?? 0.0
        serviceCharge = try createBookingInfo.decodeIfPresent(Double.self, forKey: .serviceCharge) ?? 0.0
        serviceChargeAmount = try createBookingInfo.decodeIfPresent(Double.self, forKey: .serviceChargeAmount) ?? 0.0
        loyaltyPoints = try createBookingInfo.decodeIfPresent(Int.self, forKey: .loyaltyPoints) ?? 0
        loyaltyPointsAmount = try createBookingInfo.decodeIfPresent(Int.self, forKey: .loyaltyPointsAmount) ?? 0
        branchId = try createBookingInfo.decodeIfPresent(String.self, forKey: .branchId) ?? ""
        currency = try createBookingInfo.decodeIfPresent(String.self, forKey: .currency) ?? ""
        bookingTaxes = try createBookingInfo.decodeIfPresent([BookingTaxes].self, forKey: .bookingTaxes) ?? nil
        voucherDetails = try createBookingInfo.decodeIfPresent([BookingVoucherDetails].self, forKey: .voucherDetails) ?? nil
        fieldErrors = try createBookingInfo.decodeIfPresent([FieldErrors].self, forKey: .fieldErrors)
    }
}

struct FieldErrors : Codable {
    let field : String?
    let message : String?

    enum CodingKeys: String, CodingKey {

        case field = "field"
        case message = "message"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        field = try values.decodeIfPresent(String.self, forKey: .field)
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }

}

struct BookingVoucherDetails: Codable {
    var title: String
    var voucherCode: String
    var voucherType: String
    var amount: Double
    
    private enum BookingVoucherDetailsKeys: String, CodingKey {
        case title = "title"
        case voucherCode = "voucher_code"
        case voucherType = "voucher_type"
        case amount = "amount"
    }
    
    init(from decoder:Decoder) throws {
        let bookingDetailsInfo = try decoder.container(keyedBy: BookingVoucherDetailsKeys.self)
        title = try bookingDetailsInfo.decodeIfPresent(String.self, forKey: .title) ?? ""
        voucherCode = try bookingDetailsInfo.decodeIfPresent(String.self, forKey: .voucherCode) ?? ""
        voucherType = try bookingDetailsInfo.decodeIfPresent(String.self, forKey: .voucherType) ?? ""
        amount = try bookingDetailsInfo.decodeIfPresent(Double.self, forKey: .amount) ?? 0.0
    }
}

// MARK: - BookingDetails
struct BookingDetails: Codable {
    var menuId: String?
    var buffetId: String?
    var packs: String?
    var buffetName: String?
    var buffetType: String?
    var price: Int?
    var totalPrice: Int?
    
    private enum BookingDetailsKeys: String, CodingKey {
        case menuId = "menu_id"
        case buffetId = "buffet_id"
        case packs = "packs"
        case buffetName = "buffet_name"
        case buffetType = "buffet_type"
        case price = "price"
        case totalPrice = "total_price"
    }
}

// MARK: - Decoder of BookingDetails
extension BookingDetails {
    init(from decoder:Decoder) throws {
        //menuId, buffetId, packs are sometimes Int and other times String.
        //It's better keep a check on those.
        let bookingDetailsInfo = try decoder.container(keyedBy: BookingDetailsKeys.self)
        //menuId = try bookingDetailsInfo.decodeIfPresent(String.self, forKey: .menuId) ?? ""
        if let value = try? bookingDetailsInfo.decodeIfPresent(Int.self, forKey: .menuId) {
            menuId = String(value)
        } else {
            menuId = try bookingDetailsInfo.decodeIfPresent(String.self, forKey: .menuId)
        }
        //buffetId = try bookingDetailsInfo.decodeIfPresent(String.self, forKey: .buffetId) ?? ""
        if let value = try? bookingDetailsInfo.decodeIfPresent(Int.self, forKey: .buffetId) {
            buffetId = String(value)
        } else {
            buffetId = try bookingDetailsInfo.decodeIfPresent(String.self, forKey: .buffetId)
        }
        //packs = try bookingDetailsInfo.decodeIfPresent(String.self, forKey: .packs) ?? ""
        if let value = try? bookingDetailsInfo.decodeIfPresent(Int.self, forKey: .packs) {
            packs = String(value)
        } else {
            packs = try bookingDetailsInfo.decodeIfPresent(String.self, forKey: .packs)
        }
        buffetName = try bookingDetailsInfo.decodeIfPresent(String.self, forKey: .buffetName) ?? ""
        buffetType = try bookingDetailsInfo.decodeIfPresent(String.self, forKey: .buffetType) ?? ""
        price = try bookingDetailsInfo.decodeIfPresent(Int.self, forKey: .price) ?? 0
        totalPrice = try bookingDetailsInfo.decodeIfPresent(Int.self, forKey: .totalPrice) ?? 0
    }
}

// MARK: - BookingDetails
struct BookingTaxes: Codable {
    var taxName: String?
    var taxAmount: Double?
    var texPercentage: Double?
    
    private enum BookingTaxesKeys: String, CodingKey {
        case taxName = "tax"
        case taxAmount = "tax_amount"
        case texPercentage = "tax_percentage"
    }
}

extension BookingTaxes {
    init(from decoder:Decoder) throws {
        let bookingTaxesInfo = try decoder.container(keyedBy: BookingTaxesKeys.self)
        taxName = try bookingTaxesInfo.decodeIfPresent(String.self, forKey: .taxName) ?? ""
        taxAmount = try bookingTaxesInfo.decodeIfPresent(Double.self, forKey: .taxAmount) ?? 0.0
        texPercentage = try bookingTaxesInfo.decodeIfPresent(Double.self, forKey: .texPercentage) ?? 0.0
    }
}

// MARK: - Get CreateBooking API Ends

// MARK: - Put Update Booking API Starts
struct UpdateBookingDataModel: Codable {
    var bookingId: String?
    var bookingStatus: String?
    var branchName: String?
    var branchAddress: String?
    var branchContactNumber: String?
    var branchImage: String?
    var lat: Double?
    var long: Double?
    var amenities: [Amenities]?
    var bookingDetails: [UpcomingDetails]?
    var billWithoutTax: Double?
    var cgst: Double?
    var sgst: Double?
    var serviceCharge: Double?
    var billTotal: Double?
    
    private enum UpdateBookingDataKeys: String, CodingKey {
        case bookingId = "booking_id"
        case bookingStatus = "booking_status"
        case branchName = "branch_name"
        case branchAddress = "branch_address"
        case branchContactNumber = "branch_contact_no"
        case branchImage = "branch_image"
        case lat = "lat"
        case long = "long"
        case amenities = "amenities"
        case bookingDetails = "booking_details"
        case billWithoutTax = "bill_without_tax"
        case cgst = "cgst"
        case sgst = "sgst"
        case serviceCharge = "service_charge"
        case billTotal = "bill_total"
    }
}

// MARK: - Decoder of UpdateBookingDataModel
extension UpdateBookingDataModel {
    init(from decoder:Decoder) throws {
        let updateBookingInfo = try decoder.container(keyedBy: UpdateBookingDataKeys.self)
        bookingId = try updateBookingInfo.decodeIfPresent(String.self, forKey: .bookingId) ?? ""
        bookingStatus = try updateBookingInfo.decodeIfPresent(String.self, forKey: .bookingStatus) ?? ""
        branchName = try updateBookingInfo.decodeIfPresent(String.self, forKey: .branchName) ?? ""
        branchAddress = try updateBookingInfo.decodeIfPresent(String.self, forKey: .branchAddress) ?? ""
        branchContactNumber = try updateBookingInfo.decodeIfPresent(String.self, forKey: .branchContactNumber) ?? ""
        branchImage = try updateBookingInfo.decodeIfPresent(String.self, forKey: .branchImage) ?? ""
        lat = try updateBookingInfo.decodeIfPresent(Double.self, forKey: .lat) ?? 0.0
        long = try updateBookingInfo.decodeIfPresent(Double.self, forKey: .long) ?? 0.0
        amenities = try updateBookingInfo.decodeIfPresent([Amenities].self, forKey: .amenities) ?? nil
        bookingDetails = try updateBookingInfo.decodeIfPresent([UpcomingDetails].self, forKey: .bookingDetails) ?? nil
        billWithoutTax = try updateBookingInfo.decodeIfPresent(Double.self, forKey: .billWithoutTax) ?? 0.0
        cgst = try updateBookingInfo.decodeIfPresent(Double.self, forKey: .cgst) ?? 0.0
        sgst = try updateBookingInfo.decodeIfPresent(Double.self, forKey: .sgst) ?? 0.0
        serviceCharge = try updateBookingInfo.decodeIfPresent(Double.self, forKey: .serviceCharge) ?? 0.0
        billTotal = try updateBookingInfo.decodeIfPresent(Double.self, forKey: .billTotal) ?? 0.0
    }
}

struct UpcomingDetails: Codable {
    var menuId: String?
    var buffetId: String?
    var packs: String?
    var buffetName: String?
    var buffetType: String?
    var price: Int?
    var totalPrice: Int?
    
    private enum UpcomingDetailsKeys: String, CodingKey {
        case menuId = "menu_id"
        case buffetId = "buffet_id"
        case packs = "packs"
        case buffetName = "buffet_name"
        case buffetType = "buffet_type"
        case price = "price"
        case totalPrice = "total_price"
    }
    
    init(from decoder:Decoder) throws {
        let bookingDetailsInfo = try decoder.container(keyedBy: UpcomingDetailsKeys.self)
        if let value = try? bookingDetailsInfo.decodeIfPresent(Int.self, forKey: .menuId) {
            menuId = String(value)
        } else {
            menuId = try bookingDetailsInfo.decodeIfPresent(String.self, forKey: .menuId)
        }
        if let value = try? bookingDetailsInfo.decodeIfPresent(Int.self, forKey: .buffetId) {
            buffetId = String(value)
        } else {
            buffetId = try bookingDetailsInfo.decodeIfPresent(String.self, forKey: .buffetId)
        }
        if let value = try? bookingDetailsInfo.decodeIfPresent(Int.self, forKey: .packs) {
            packs = String(value)
        } else {
            packs = try bookingDetailsInfo.decodeIfPresent(String.self, forKey: .packs)
        }
        buffetName = try bookingDetailsInfo.decodeIfPresent(String.self, forKey: .buffetName) ?? ""
        buffetType = try bookingDetailsInfo.decodeIfPresent(String.self, forKey: .buffetType) ?? ""
        price = try bookingDetailsInfo.decodeIfPresent(Int.self, forKey: .price) ?? 0
        totalPrice = try bookingDetailsInfo.decodeIfPresent(Int.self, forKey: .totalPrice) ?? 0
    }
}
