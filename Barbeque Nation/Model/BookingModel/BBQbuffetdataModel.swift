import Foundation

// MARK: - Get MenuBranchBuffet API Starts
// MARK: - MenuBranchBuffet
struct BranchBuffetDataModel:Codable {
    let menuId: String?
    let menuName: String?
    let buffets: BuffetsData?
    
    private enum menubranchbuffetCodingKeys: String, CodingKey {
        case menuId = "menu_id"
        case menuName = "menu_name"
        case buffets = "buffets"
    }
}


// MARK: - Buffets
struct BuffetsData:Codable {
    var buffetTitle: String?
    var buffetDescription: String?
    var buffetData: [Buffetdata]?
    
    private enum buffetsCodingKeys: String, CodingKey {
        case buffetTitle = "buffet_title"
        case buffetDescription = "buffet_description"
        case buffetData = "buffet_data"
    }
}

// MARK: - BuffetData
struct Buffetdata:Codable {
    var buffetId: Int
    var buffetName:String?
    var buffetDescription:String?
    var buffetPrice:Double?
    var buffetCurrency:String?
    var buffetType:String?
    var buffetImage:String?
    var customerType:String?
    var buffetItems:[ItemsBuffet]?
    
    private enum buffetsdataCodingKeys: String, CodingKey {
        case buffetId = "priceId"
//        case buffetId = "buffet_id"
        case buffetName = "buffet_name"
        case buffetDescription = "buffet_description"
        case buffetPrice = "baseAmount"
        case buffetCurrency = "priceType"
        case buffetType = "foodType"
        case buffetImage = "buffet_image"
        case customerType = "customerType"
        case buffetItems = "buffet_items"
    }
}

// MARK: - BuffetItems
struct ItemsBuffet:Codable {
    var buffetItems_name: String?
    var buffetItems_menuItems: [MenuItems]?

    private enum buffetitemsCodingKeys: String, CodingKey {
        case buffetItems_name = "foodType"
        case buffetItems_menuItems = "menu_items"
    }
}

// MARK: - MenuItems
struct Menuitems:Codable {
    var menuitems_Id: String?
    var menuitems_Name: String?
    var menuitems_Tags: tags?
    var menuitems_Type: String?
    var menuitems_Image: String?
    var menuitems_Category: String?
    var menuitems_Currency: String?
    var menuitems_POSCode: String?
    var menuitems_Description: String?
    var menuitems_DefaultPrice: String?
    var menuitems_SaleTypeAlaCarte: String?
    
    private enum menuitemsCodingKeys: String, CodingKey {
        case menuitems_Id = "id"
        case menuitems_Name = "name"
        case menuitems_Tags = "tags"
        case menuitems_Type = "type"
        case menuitems_Image = "image"
        case menuitems_Category = "category"
        case menuitems_Currency = "currency"
        case menuitems_POSCode = "pos_code"
        case menuitems_Description = "description"
        case menuitems_DefaultPrice = "default_price"
        case menuitems_SaleTypeAlaCarte = "sale_type_ala_carte"
    }
}

// MARK: - Tags
struct tags: Codable {
    var tagId: Int?
    var tagName: String?
    var tagImage: String?
    
    private enum tagsCodingKeys: String, CodingKey {
        case tagId = "id"
        case tagName = "name"
        case tagImage = "image"
    }
}

// MARK: - Decoder of Tags
extension tags {
    init(from decoder: Decoder) throws {
        let itemTagInfo = try decoder.container(keyedBy: tagsCodingKeys.self)
        tagId = try itemTagInfo.decodeIfPresent(Int.self, forKey: .tagId) ?? 0
        tagName = try itemTagInfo.decodeIfPresent(String.self, forKey: .tagName) ?? ""
        tagImage = try itemTagInfo.decodeIfPresent(String.self, forKey: .tagImage) ?? ""
    }
}

// MARK: - Decoder of MenuItems
extension Menuitems {
    init(from decoder:Decoder) throws {
        let menuitemsInfo = try decoder.container(keyedBy: menuitemsCodingKeys.self)
        menuitems_Id = try menuitemsInfo.decodeIfPresent(String.self, forKey: .menuitems_Id) ?? ""
        menuitems_Name = try menuitemsInfo.decodeIfPresent(String.self, forKey: .menuitems_Name) ?? ""
        menuitems_Tags = try menuitemsInfo.decodeIfPresent(tags.self, forKey: .menuitems_Tags) ?? nil
        menuitems_Type = try menuitemsInfo.decodeIfPresent(String.self, forKey: .menuitems_Type) ?? ""
        menuitems_Image = try menuitemsInfo.decodeIfPresent(String.self, forKey: .menuitems_Image) ?? ""
        menuitems_Category = try menuitemsInfo.decodeIfPresent(String.self, forKey: .menuitems_Category) ?? ""
        menuitems_Currency = try menuitemsInfo.decodeIfPresent(String.self, forKey: .menuitems_Currency) ?? ""
        menuitems_POSCode = try menuitemsInfo.decodeIfPresent(String.self, forKey: .menuitems_POSCode) ?? ""
        menuitems_Description = try menuitemsInfo.decodeIfPresent(String.self, forKey: .menuitems_Description) ?? ""
        menuitems_DefaultPrice = try menuitemsInfo.decodeIfPresent(String.self, forKey: .menuitems_DefaultPrice) ?? ""
        menuitems_SaleTypeAlaCarte = try menuitemsInfo.decodeIfPresent(String.self, forKey: .menuitems_SaleTypeAlaCarte) ?? ""
    }
}

// MARK: - Decoder of BuffetItems
extension ItemsBuffet {
    init(from decoder:Decoder) throws {
        let buffetitemsInfo = try decoder.container(keyedBy: buffetitemsCodingKeys.self)
        buffetItems_menuItems = try buffetitemsInfo.decodeIfPresent([MenuItems].self, forKey: .buffetItems_menuItems) ?? nil
        buffetItems_name = try buffetitemsInfo.decodeIfPresent(String.self, forKey: .buffetItems_name) ?? ""
    }
}

// MARK: - Decoder of BuffetData
extension Buffetdata {
    init(from decoder:Decoder) throws {
        let buffetdataInfo = try decoder.container(keyedBy: buffetsdataCodingKeys.self)
        buffetId = try buffetdataInfo.decodeIfPresent(Int.self, forKey: .buffetId) ?? 0
        buffetName = try buffetdataInfo.decodeIfPresent(String.self, forKey: .buffetName) ?? ""
        buffetDescription = try buffetdataInfo.decodeIfPresent(String.self, forKey: .buffetDescription) ?? ""
        //buffetPrice = try buffetdataInfo.decodeIfPresent(Double.self, forKey: .buffetPrice) ?? 0.0
        
        if let value = try? buffetdataInfo.decodeIfPresent(Int.self, forKey: .buffetPrice) {
            buffetPrice = Double(value)
        } else if let value = try? buffetdataInfo.decodeIfPresent(String.self, forKey: .buffetPrice) {
            buffetPrice = (value as NSString).doubleValue
        } else {
            buffetPrice = try buffetdataInfo.decodeIfPresent(Double.self, forKey: .buffetPrice)
        }
        customerType = try buffetdataInfo.decodeIfPresent(String.self, forKey: .customerType) ?? ""
        buffetCurrency = try buffetdataInfo.decodeIfPresent(String.self, forKey: .buffetCurrency) ?? ""
        buffetType = try buffetdataInfo.decodeIfPresent(String.self, forKey: .buffetType) ?? ""
        buffetImage = try buffetdataInfo.decodeIfPresent(String.self, forKey: .buffetImage) ?? ""
        buffetItems = try buffetdataInfo.decodeIfPresent([ItemsBuffet].self, forKey: .buffetItems) ?? nil
    }
}

// MARK: - Decoder of Buffets
extension BuffetsData {
    init(from decoder:Decoder) throws {
        let buffetsInfo = try decoder.container(keyedBy: buffetsCodingKeys.self)
        buffetTitle = try buffetsInfo.decodeIfPresent(String.self, forKey: .buffetTitle) ?? ""
        buffetDescription = try buffetsInfo.decodeIfPresent(String.self, forKey: .buffetDescription) ?? ""
        buffetData = try buffetsInfo.decodeIfPresent([Buffetdata].self, forKey: .buffetData) ?? nil
    }
}

// MARK: - Decoder of MenuBranchBuffet
extension BranchBuffetDataModel {
    init(from decoder:Decoder) throws {
        let menubranchbuffetInfo = try decoder.container(keyedBy: menubranchbuffetCodingKeys.self)
        menuId = try menubranchbuffetInfo.decodeIfPresent(String.self, forKey: .menuId) ?? ""
        menuName = try menubranchbuffetInfo.decodeIfPresent(String.self, forKey: .menuName) ?? ""
        buffets = try menubranchbuffetInfo.decodeIfPresent(BuffetsData.self, forKey: .buffets) ?? nil
}
}
// MARK: - Get MenuBranchBuffet API Ends

// MARK: - Get CreateBooking API Starts
// MARK: - CreateBooking
struct CreatebookingDataModel: Codable {
    var bookingId: String?
    var bookingStatus: String?
    var branchName: String?
    var branchAddress: String?
    var branchContactNumber: String?
    var branchImage: String?
    var lat: Double?
    var long: Double?
    var amenities: [Amenities]?
    var bookingDetails: [Bookingdetails]?
    var billTotal: Double?
    var advanceAmount: Double?
    var billWithoutTax: Double?
    var serviceCharge: Double?
    var serviceChargeAmount: Double?
    var loyaltyPoints: Int?
    var loyaltyPointsAmount: Int?
    var branchId: String?
    var currency: String?
    var bookingTaxes: [Bookingtaxes]?
    var voucherDetails: [BookingVoucherDetails]?
    let fieldErrors : [FieldErrors]?
    
    private enum CreateBookingDataKeys: String, CodingKey {
        case bookingId = "booking_id"
        case bookingStatus = "booking_status"
        case branchName = "branch_name"
        case branchAddress = "branch_address"
        case branchContactNumber = "branch_contact_no"
        case branchImage = "branch_image"
        case lat = "lat"
        case long = "long"
        case amenities = "amenities"
        case bookingDetails = "booking_details"
        case billTotal = "bill_total"
        case advanceAmount = "advance_amount"
        case billWithoutTax = "bill_without_tax"
        case serviceCharge = "service_charge"
        case serviceChargeAmount = "service_charge_amount"
        case loyaltyPoints = "loyalty_points"
        case loyaltyPointsAmount = "loyalty_points_amount"
        case branchId = "branch_id"
        case currency = "currency"
        case bookingTaxes = "taxes"
        case voucherDetails = "voucher_details"
        case fieldErrors = "fieldErrors"
    }
}

// MARK: - Decoder of CreateBooking
extension CreatebookingDataModel {
    init(from decoder:Decoder) throws {
        let createBookingInfo = try decoder.container(keyedBy: CreateBookingDataKeys.self)
        bookingId = try createBookingInfo.decodeIfPresent(String.self, forKey: .bookingId) ?? ""
        bookingStatus = try createBookingInfo.decodeIfPresent(String.self, forKey: .bookingStatus) ?? ""
        branchName = try createBookingInfo.decodeIfPresent(String.self, forKey: .branchName) ?? ""
        branchAddress = try createBookingInfo.decodeIfPresent(String.self, forKey: .branchAddress) ?? ""
        branchContactNumber = try createBookingInfo.decodeIfPresent(String.self, forKey: .branchContactNumber) ?? ""
        branchImage = try createBookingInfo.decodeIfPresent(String.self, forKey: .branchImage) ?? ""
        lat = try createBookingInfo.decodeIfPresent(Double.self, forKey: .lat) ?? 0.0
        long = try createBookingInfo.decodeIfPresent(Double.self, forKey: .long) ?? 0.0
        amenities = try createBookingInfo.decodeIfPresent([Amenities].self, forKey: .amenities) ?? nil
        bookingDetails = try createBookingInfo.decodeIfPresent([Bookingdetails].self, forKey: .bookingDetails) ?? nil
        billTotal = try createBookingInfo.decodeIfPresent(Double.self, forKey: .billTotal) ?? 0.0
        advanceAmount = try createBookingInfo.decodeIfPresent(Double.self, forKey: .advanceAmount) ?? 0.0
        billWithoutTax = try createBookingInfo.decodeIfPresent(Double.self, forKey: .billWithoutTax) ?? 0.0
        serviceCharge = try createBookingInfo.decodeIfPresent(Double.self, forKey: .serviceCharge) ?? 0.0
        serviceChargeAmount = try createBookingInfo.decodeIfPresent(Double.self, forKey: .serviceChargeAmount) ?? 0.0
        loyaltyPoints = try createBookingInfo.decodeIfPresent(Int.self, forKey: .loyaltyPoints) ?? 0
        loyaltyPointsAmount = try createBookingInfo.decodeIfPresent(Int.self, forKey: .loyaltyPointsAmount) ?? 0
        branchId = try createBookingInfo.decodeIfPresent(String.self, forKey: .branchId) ?? ""
        currency = try createBookingInfo.decodeIfPresent(String.self, forKey: .currency) ?? ""
        bookingTaxes = try createBookingInfo.decodeIfPresent([Bookingtaxes].self, forKey: .bookingTaxes) ?? nil
        voucherDetails = try createBookingInfo.decodeIfPresent([BookingVoucherDetails].self, forKey: .voucherDetails) ?? nil
        fieldErrors = try createBookingInfo.decodeIfPresent([FieldErrors].self, forKey: .fieldErrors)
    }
}

struct Fielderrors : Codable {
    let field : String?
    let message : String?

    enum CodingKeys: String, CodingKey {

        case field = "field"
        case message = "message"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        field = try values.decodeIfPresent(String.self, forKey: .field)
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }

}

struct BookingvoucherDetails: Codable {
    var title: String
    var voucherCode: String
    var voucherType: String
    var amount: Double
    
    private enum BookingVoucherDetailsKeys: String, CodingKey {
        case title = "title"
        case voucherCode = "voucher_code"
        case voucherType = "voucher_type"
        case amount = "amount"
    }
    
    init(from decoder:Decoder) throws {
        let bookingDetailsInfo = try decoder.container(keyedBy: BookingVoucherDetailsKeys.self)
        title = try bookingDetailsInfo.decodeIfPresent(String.self, forKey: .title) ?? ""
        voucherCode = try bookingDetailsInfo.decodeIfPresent(String.self, forKey: .voucherCode) ?? ""
        voucherType = try bookingDetailsInfo.decodeIfPresent(String.self, forKey: .voucherType) ?? ""
        amount = try bookingDetailsInfo.decodeIfPresent(Double.self, forKey: .amount) ?? 0.0
    }
}

// MARK: - BookingDetails
struct Bookingdetails: Codable {
    var menuId: String?
    var buffetId: String?
    var packs: String?
    var buffetName: String?
    var buffetType: String?
    var price: Int?
    var totalPrice: Int?
    
    private enum BookingDetailsKeys: String, CodingKey {
        case menuId = "menu_id"
        case buffetId = "priceId"
        case packs = "packs"
        case buffetName = "buffet_name"
        case buffetType = "foodType"
        case price = "price"
        case totalPrice = "total_price"
    }
}

// MARK: - Decoder of BookingDetails
extension Bookingdetails {
    init(from decoder:Decoder) throws {
        //menuId, buffetId, packs are sometimes Int and other times String.
        //It's better keep a check on those.
        let bookingDetailsInfo = try decoder.container(keyedBy: BookingDetailsKeys.self)
        //menuId = try bookingDetailsInfo.decodeIfPresent(String.self, forKey: .menuId) ?? ""
        if let value = try? bookingDetailsInfo.decodeIfPresent(Int.self, forKey: .menuId) {
            menuId = String(value)
        } else {
            menuId = try bookingDetailsInfo.decodeIfPresent(String.self, forKey: .menuId)
        }
        //buffetId = try bookingDetailsInfo.decodeIfPresent(String.self, forKey: .buffetId) ?? ""
        if let value = try? bookingDetailsInfo.decodeIfPresent(Int.self, forKey: .buffetId) {
            print(value)
            buffetId = "\(value)"
        } else {
            buffetId = try bookingDetailsInfo.decodeIfPresent(String.self, forKey: .buffetId)
        }
        //packs = try bookingDetailsInfo.decodeIfPresent(String.self, forKey: .packs) ?? ""
        if let value = try? bookingDetailsInfo.decodeIfPresent(Int.self, forKey: .packs) {
            packs = String(value)
        } else {
            packs = try bookingDetailsInfo.decodeIfPresent(String.self, forKey: .packs)
        }
        buffetName = try bookingDetailsInfo.decodeIfPresent(String.self, forKey: .buffetName) ?? ""
        buffetType = try bookingDetailsInfo.decodeIfPresent(String.self, forKey: .buffetType) ?? ""
        price = try bookingDetailsInfo.decodeIfPresent(Int.self, forKey: .price) ?? 0
        totalPrice = try bookingDetailsInfo.decodeIfPresent(Int.self, forKey: .totalPrice) ?? 0
    }
}

// MARK: - BookingDetails
struct Bookingtaxes: Codable {
    var taxName: String?
    var taxAmount: Double?
    var texPercentage: Double?
    
    private enum BookingTaxesKeys: String, CodingKey {
        case taxName = "tax"
        case taxAmount = "tax_amount"
        case texPercentage = "tax_percentage"
    }
}

extension Bookingtaxes {
    init(from decoder:Decoder) throws {
        let bookingTaxesInfo = try decoder.container(keyedBy: BookingTaxesKeys.self)
        taxName = try bookingTaxesInfo.decodeIfPresent(String.self, forKey: .taxName) ?? ""
        taxAmount = try bookingTaxesInfo.decodeIfPresent(Double.self, forKey: .taxAmount) ?? 0.0
        texPercentage = try bookingTaxesInfo.decodeIfPresent(Double.self, forKey: .texPercentage) ?? 0.0
    }
}

// MARK: - Get CreateBooking API Ends
