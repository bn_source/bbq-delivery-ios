//
 //  Created by Chandan Singh on 20/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQVoucherCouponsModel.swift
 //

import Foundation

struct VoucherCouponsModel: Decodable {
    var message: String?
    var messageType: String?
    var pageNumber:String?
    var pageSize:String?
    var voucherTermsAndConditions:[String]?
    var totalElements:String
    var couponsAndVouchers: [VoucherCouponsData]?

    private enum VoucherCouponsModelKeys: String, CodingKey {
        case message = "message"
        case messageType = "message_type"
        case pageNumber = "page_number"
        case pageSize = "page_size"
        case voucherTermsAndConditions = "terms_conditions"
        case totalElements = "total_elements"
        case couponsAndVouchers = "vouchers"
    }
}

extension VoucherCouponsModel {
    init(from decoder: Decoder) throws {
        let voucherCouponsInfo = try decoder.container(keyedBy: VoucherCouponsModelKeys.self)
        message = try voucherCouponsInfo.decodeIfPresent(String.self, forKey: .message) ?? ""
        messageType = try voucherCouponsInfo.decodeIfPresent(String.self, forKey: .messageType) ?? ""
        pageNumber = try voucherCouponsInfo.decodeIfPresent(String.self, forKey: .pageNumber) ?? ""
        pageSize = try voucherCouponsInfo.decodeIfPresent(String.self, forKey: .pageSize) ?? ""
        voucherTermsAndConditions = try voucherCouponsInfo.decodeIfPresent([String].self, forKey: .voucherTermsAndConditions) ?? []
        totalElements = try voucherCouponsInfo.decodeIfPresent(String.self, forKey: .totalElements) ?? ""
        couponsAndVouchers = try voucherCouponsInfo.decodeIfPresent([VoucherCouponsData].self, forKey: .couponsAndVouchers) ?? nil

    }
}

struct VoucherCouponsData: Decodable {
    var voucherApplicability:Bool?
    var barcode: String?
    var currency:String?
    var denomination: Double?
    var voucherDescription:String?
    var issueDate:String?
    var voucherOrderID :String?
    var source: String?
    var status: String?
    var title: String?
    var validity: String?
    var voucherHeadId: Int?
    var voucherType: String?
    var voucherImage:String
    var pax_applicable: Int = 0
    
    private enum VoucherCouponsDataKeys: String, CodingKey {
        
        case voucherApplicability = "applicability"
        case barcode = "bar_code"
        case currency = "currency"
        case denomination = "denomination"
        case voucherDescription = "description"
        case issueDate = "issue_date"
        case voucherOrderID = "order_id"
        case source = "source"
        case status = "status"
        case title = "title"
        case validity = "validity"
        case voucherHeadId = "voucher_head_id"
        case voucherType = "voucher_type"
        case voucherImage = "image"
        case pax_applicable = "pax_applicable"
    }
    // Custom initializer
    
    init(appliedVouchers: BookingVoucher? = nil) {
        self.barcode = appliedVouchers?.voucherCode
        self.title   = appliedVouchers?.title
        self.voucherType  = appliedVouchers?.voucherType
        self.denomination = appliedVouchers?.amount
        self.currency     = appliedVouchers?.currency
        
        // Defaults values
        self.voucherHeadId = 0
        self.voucherImage = ""
        self.validity = ""
        self.status = ""
        self.source = ""
    }
}

extension VoucherCouponsData {
    init(from decoder: Decoder) throws {
        let voucherCouponsDataInfo = try decoder.container(keyedBy: VoucherCouponsDataKeys.self)
        voucherApplicability = try voucherCouponsDataInfo.decodeIfPresent(Bool.self, forKey: .voucherApplicability) ?? false
        barcode = try voucherCouponsDataInfo.decodeIfPresent(String.self, forKey: .barcode) ?? ""
        currency = try voucherCouponsDataInfo.decodeIfPresent(String.self, forKey: .currency) ?? ""
        denomination = try voucherCouponsDataInfo.decodeIfPresent(Double.self, forKey: .denomination) ?? 0.0
        voucherDescription = try voucherCouponsDataInfo.decodeIfPresent(String.self, forKey: .voucherDescription) ?? ""
        issueDate = try voucherCouponsDataInfo.decodeIfPresent(String.self, forKey: .issueDate) ?? ""
        voucherOrderID = try voucherCouponsDataInfo.decodeIfPresent(String.self, forKey: .voucherOrderID) ?? ""
        source = try voucherCouponsDataInfo.decodeIfPresent(String.self, forKey: .source) ?? ""
        status = try voucherCouponsDataInfo.decodeIfPresent(String.self, forKey: .status) ?? ""
        title = try voucherCouponsDataInfo.decodeIfPresent(String.self, forKey: .title) ?? ""
        validity = try voucherCouponsDataInfo.decodeIfPresent(String.self, forKey: .validity) ?? ""
        voucherHeadId = try voucherCouponsDataInfo.decodeIfPresent(Int.self, forKey: .voucherHeadId) ?? 0
        voucherType = try voucherCouponsDataInfo.decodeIfPresent(String.self, forKey: .voucherType) ?? ""
        voucherImage = try voucherCouponsDataInfo.decodeIfPresent(String.self, forKey: .voucherImage) ?? ""
        pax_applicable = try voucherCouponsDataInfo.decodeIfPresent(Int.self, forKey: .pax_applicable) ?? 0
    }
}
