//
 //  Created by Chandan Singh on 02/01/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified BBQGiftVoucherModel.swift
 //

import Foundation

struct BBQGiftVoucherModel: Decodable {
    let message : String?
    let messageType: String?
    
    enum BBQGiftVoucherModelCodingKeys: String, CodingKey {
        case message = "message"
        case messageType = "message_type"
    }
    
    init(from decoder: Decoder) throws {
        let giftVoucher = try decoder.container(keyedBy: BBQGiftVoucherModelCodingKeys.self)
        
        message = try giftVoucher.decodeIfPresent(String.self, forKey: .message) ?? ""
        messageType = try giftVoucher.decodeIfPresent(String.self, forKey: .messageType) ?? ""
    }
}
