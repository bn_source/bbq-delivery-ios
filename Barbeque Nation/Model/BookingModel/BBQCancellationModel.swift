//
 //  Created by Rabindra L on 31/08/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQCancellationModel.swift
 //

import Foundation

struct BookingCancellationModel:Codable {
    var booking_status:Bool
    var message:String
    var refund_details:[RefundDetails]?
    
    private enum BookingCancellationDataKeys: String, CodingKey {
        case booking_status = "booking_status"
        case message = "message"
        case refund_details = "refund_details"
    }
}

struct RefundDetails:Codable {
    var refund_status:String
    var refund_id:String
    var refund_amount:Double
    var refund_currency:String
    var refund_created:Double
    var payment_id:String

    private enum RefundDetailsDataKeys: String, CodingKey {
        case refund_status = "refund_status"
        case refund_id = "refund_id"
        case refund_amount = "refund_amount"
        case refund_currency = "refund_currency"
        case refund_created = "refund_created"
        case payment_id = "payment_id"
    }
}

extension RefundDetails {
    init(from decoder: Decoder) throws {
        let refundDetailsInfo = try decoder.container(keyedBy: RefundDetailsDataKeys.self)
        refund_status = try refundDetailsInfo.decodeIfPresent(String.self, forKey: .refund_status) ?? ""
        refund_id = try refundDetailsInfo.decodeIfPresent(String.self, forKey: .refund_id) ?? ""
        refund_amount = try refundDetailsInfo.decodeIfPresent(Double.self, forKey: .refund_amount) ?? 0.0
        refund_currency = try refundDetailsInfo.decodeIfPresent(String.self, forKey: .refund_currency) ?? ""
        refund_created = try refundDetailsInfo.decodeIfPresent(Double.self, forKey: .refund_created) ?? 0.0
        payment_id = try refundDetailsInfo.decodeIfPresent(String.self, forKey: .payment_id) ?? ""
    }
}

extension BookingCancellationModel {
    init(from decoder: Decoder) throws {
        let bookingCancellationInfo = try decoder.container(keyedBy: BookingCancellationDataKeys.self)
        booking_status = try bookingCancellationInfo.decodeIfPresent(Bool.self, forKey: .booking_status) ?? false
        message = try bookingCancellationInfo.decodeIfPresent(String.self, forKey: .message) ?? ""
        refund_details = try bookingCancellationInfo.decodeIfPresent([RefundDetails].self, forKey: .refund_details) ?? nil
    }
}
