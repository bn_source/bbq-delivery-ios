//
 //  Created by Maya R on 03/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQCorporateBookingModel.swift
 //

import Foundation


struct CorporateOffers {
    var offerImageName:String
    var offerText:String
    
    init() {
        offerImageName = ""
        offerText = ""
    }
}
