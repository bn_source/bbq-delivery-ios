//
 //  Created by Ajith CP on 11/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQRefundDataModel.swift
 //

import Foundation

// MARK: - BBQRefundDataModel
struct BBQRefundDataModel: Codable {
    let cashRefund: CashRefund?
    let loyaltyPointsRefund: LoyaltyPointsRefund?
    let bookingLoyaltyPoints: BookingLoyaltyPoints?
    
    enum CodingKeys: String, CodingKey {
        case cashRefund = "cash_refund"
        case loyaltyPointsRefund = "loyalty_points_refund"
        case bookingLoyaltyPoints = "booking_loyalty_points"
    }
    
    init(from decoder: Decoder) throws {
        let refundData = try decoder.container(keyedBy: CodingKeys.self)
        
        cashRefund = try refundData.decodeIfPresent(CashRefund.self, forKey: .cashRefund) ?? nil
        loyaltyPointsRefund = try refundData.decodeIfPresent(LoyaltyPointsRefund.self, forKey: .loyaltyPointsRefund) ?? nil
        bookingLoyaltyPoints = try refundData.decodeIfPresent(BookingLoyaltyPoints.self, forKey: .bookingLoyaltyPoints) ?? nil
    }
}

// MARK: - BookingLoyaltyPoints
struct BookingLoyaltyPoints: Codable {
    let loyaltyPoints, loyaltyPointsAmount: Int?
    
    enum CodingKeys: String, CodingKey {
        case loyaltyPoints = "loyalty_points"
        case loyaltyPointsAmount = "loyalty_points_amount"
    }
    
    init(from decoder: Decoder) throws {
        let bookingPoints = try decoder.container(keyedBy: CodingKeys.self)
        
        loyaltyPoints = try bookingPoints.decodeIfPresent(Int.self, forKey: .loyaltyPoints) ?? nil
        loyaltyPointsAmount = try bookingPoints.decodeIfPresent(Int.self, forKey: .loyaltyPointsAmount) ?? nil

    }
}

// MARK: - CashRefund
struct CashRefund: Codable {
    let refundAmount: Double?
    let currency: String?
    let cashRefundPercentage: Int?
    
    enum CodingKeys: String, CodingKey {
        case refundAmount = "refund_amount"
        case currency
        case cashRefundPercentage = "cash_refund_percentage"
    }
    
    init(from decoder: Decoder) throws {
        let cashRefund = try decoder.container(keyedBy: CodingKeys.self)
        
        refundAmount = try cashRefund.decodeIfPresent(Double.self, forKey: .refundAmount) ?? nil
        currency = try cashRefund.decodeIfPresent(String.self, forKey: .currency) ?? nil
        cashRefundPercentage = try cashRefund.decodeIfPresent(Int.self, forKey: .cashRefundPercentage) ?? nil
    }
}



// MARK: - LoyaltyPointsRefund
struct LoyaltyPointsRefund: Codable {
    let loyaltyPoints, loyaltyPointsPercentage: Int?
    
    enum CodingKeys: String, CodingKey {
        case loyaltyPoints = "loyalty_points"
        case loyaltyPointsPercentage = "loyalty_points_percentage"
    }
    
    init(from decoder: Decoder) throws {
        let pointRefund = try decoder.container(keyedBy: CodingKeys.self)
        
        loyaltyPoints = try pointRefund.decodeIfPresent(Int.self, forKey: .loyaltyPoints) ?? nil
        loyaltyPointsPercentage = try pointRefund.decodeIfPresent(Int.self, forKey: .loyaltyPointsPercentage) ?? nil
    }
}
