//
 //  Created by Abhijit on 20/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQHelpSupportByIdModel.swift
 //

import Foundation

struct BBQHelpSupportByIdModel: Codable {
    var tag_ids: [Int]?
    var qa: [QA]?
    
    private enum BBQHelpSupportByIdModelKeys: String, CodingKey {
        case tag_ids = "tag_ids"
        case qa = "qa"
    }
}

extension BBQHelpSupportByIdModel {
    init(from decoder: Decoder) throws {
        let reviewSupportInfo = try decoder.container(keyedBy: BBQHelpSupportByIdModelKeys.self)
        tag_ids = try reviewSupportInfo.decodeIfPresent([Int].self, forKey: .tag_ids) ?? nil
        qa = try reviewSupportInfo.decodeIfPresent([QA].self, forKey: .qa) ?? nil
    }
}

