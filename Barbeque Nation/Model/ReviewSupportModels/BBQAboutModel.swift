//
 //  Created by Rabindra L on 19/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQAboutModel.swift
 //

import Foundation

struct BBQAboutModel: Codable {
    var field_title:String?
    var icon_path:String?
    var field_description:String?
    
    private enum BBQAboutModelKeys: String, CodingKey {
        case field_title = "field_title"
        case icon_path = "icon_path"
        case field_description = "field_description"
    }
}

extension BBQAboutModel {
    init(from decoder: Decoder) throws {
        let reviewSupportInfo = try decoder.container(keyedBy: BBQAboutModelKeys.self)
        field_title = try reviewSupportInfo.decodeIfPresent(String.self, forKey: .field_title) ?? nil
        icon_path = try reviewSupportInfo.decodeIfPresent(String.self, forKey: .icon_path) ?? nil
        field_description = try reviewSupportInfo.decodeIfPresent(String.self, forKey: .field_description) ?? nil
    }
}
