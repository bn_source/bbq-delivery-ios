//
 //  Created by Rabindra L on 19/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQHelpSupportModel.swift
 //

import Foundation

struct BBQHelpSupportModel: Codable {
    var tags: [Tag]?
    var qa: [QA]?
    
    private enum BBQHelpSupportModelKeys: String, CodingKey {
        case tags = "tags"
        case qa = "qa"
    }
}

struct Tag: Codable {
    var tagId:String?
    var tagName:String?
    var isSelected: Bool? = false
    
    private enum TagKeys: String, CodingKey {
        case tagId = "id"
        case tagName = "tag_name"
    }
}

struct QA:Codable {
    var Q:String?
    var A:String?
    var isExpanded = false
    
    private enum QAKeys: String, CodingKey {
        case Q = "Q"
        case A = "A"
    }
}

extension BBQHelpSupportModel {
    init(from decoder: Decoder) throws {
        let reviewSupportInfo = try decoder.container(keyedBy: BBQHelpSupportModelKeys.self)
        tags = try reviewSupportInfo.decodeIfPresent([Tag].self, forKey: .tags) ?? nil
        qa = try reviewSupportInfo.decodeIfPresent([QA].self, forKey: .qa) ?? nil
    }
}

extension Tag {
    init(from decoder: Decoder) throws {
        let reviewSupportInfo = try decoder.container(keyedBy: TagKeys.self)
        tagId = try reviewSupportInfo.decodeIfPresent(String.self, forKey: .tagId) ?? nil
        tagName = try reviewSupportInfo.decodeIfPresent(String.self, forKey: .tagName) ?? nil
    }
}

extension QA {
    init(from decoder: Decoder) throws {
        let reviewSupportInfo = try decoder.container(keyedBy: QAKeys.self)
        Q = try reviewSupportInfo.decodeIfPresent(String.self, forKey: .Q) ?? nil
        A = try reviewSupportInfo.decodeIfPresent(String.self, forKey: .A) ?? nil
    }
}
