//
//  BBQErrorDataModel.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 09/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import Foundation

struct ErrorDataModel {
    let fieldErrors: [FieldError]?
}

extension ErrorDataModel: Decodable {
    private enum ErrorDataModelCodingKeys: String, CodingKey {
        case fieldErrors = "fieldErrors"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ErrorDataModelCodingKeys.self)
        fieldErrors = try container.decodeIfPresent([FieldError].self, forKey: .fieldErrors) ?? nil
    }
}

struct FieldError {
    let field: String?
    let message: String?
}

extension FieldError: Decodable {
    private enum FieldErrorCodingKeys: String, CodingKey {
        case field = "field"
        case message = "message"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: FieldErrorCodingKeys.self)
        field = try container.decodeIfPresent(String.self, forKey: .field) ?? ""
        message = try container.decodeIfPresent(String.self, forKey: .message) ?? ""
    }
}
