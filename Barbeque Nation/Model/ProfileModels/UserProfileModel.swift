//
//  UserProfileModel.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 07/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import Foundation

struct ProfileDataModel: Codable {
    var email: String?
    var countryCode: String?
    var mobileNumber: String?
    var title: String?
    var name: String?
    var dob: String?
    var maritalStatus: String?
    var anniversaryDate: String? = "" 
    var newsletter: String?
    var emailVerified: Bool
    var profileImageURL: String?
    var isNotificationEnabled : Int?
    var loyaltyPoints : Int64?
    var lastVisitedBranch: String?
    var lastVisitedBranchName:String?
    var referralCode:String
    var razorpayId:String?
    
    
    private enum ProfileCodingKeys: String, CodingKey {
        case email = "email"
        case countryCode = "country_code"
        case mobileNumber = "mobile_number"
        case title = "title"
        case name = "name"
        case dob = "date_of_birth"
        case maritalStatus = "marital_status"
        case anniversaryDate = "anniversary_date"
        case newsletter = "newsletter"
        case emailVerified = "email_verified"
        case profileImageURL = "profile_picture"
        case isNotificationEnabled = "notification_enabled"
        case loayltyPoints = "bbq_points"
        case lastVisitedBranch = "last_visited_branch"
        case lastVisitedBranchName = "last_visited_branch_name"
        case referralCode = "referral_code"
        case razorpayId = "razorpay_id"
    }
}

extension ProfileDataModel {
    init(from decoder: Decoder) throws {
        let userInfo = try decoder.container(keyedBy: ProfileCodingKeys.self)
        
        email = try userInfo.decodeIfPresent(String.self, forKey: .email) ?? nil
        countryCode = try userInfo.decodeIfPresent(String.self, forKey: .countryCode) ?? ""
        mobileNumber = String(format: "%li", try userInfo.decodeIfPresent(Int64.self, forKey: .mobileNumber) ?? "0")
        title = try userInfo.decodeIfPresent(String.self, forKey: .title) ?? ""
        name = try userInfo.decodeIfPresent(String.self, forKey: .name) ?? ""
        dob = try userInfo.decodeIfPresent(String.self, forKey: .dob) ?? nil
        maritalStatus = try userInfo.decodeIfPresent(String.self, forKey: .maritalStatus) ?? ""
        anniversaryDate = try userInfo.decodeIfPresent(String.self, forKey: .anniversaryDate) ?? nil
        newsletter = try userInfo.decodeIfPresent(String.self, forKey: .newsletter) ?? "0"
        emailVerified = try userInfo.decodeIfPresent(Bool.self, forKey: .emailVerified) ?? false
        profileImageURL = try userInfo.decodeIfPresent(String.self, forKey: .profileImageURL) ?? nil
        isNotificationEnabled = try userInfo.decodeIfPresent(Int.self, forKey: .isNotificationEnabled) ?? 1
        loyaltyPoints = try userInfo.decodeIfPresent(Int64.self, forKey: .loayltyPoints) ?? 0
        lastVisitedBranch = try userInfo.decodeIfPresent(String.self, forKey: .lastVisitedBranch) ?? ""
        lastVisitedBranchName = try userInfo.decodeIfPresent(String.self, forKey: .lastVisitedBranchName) ?? ""
        referralCode = try userInfo.decodeIfPresent(String.self, forKey: .referralCode) ?? ""
        razorpayId = try userInfo.decodeIfPresent(String.self, forKey: .razorpayId) ?? ""

    }
}


// MARK:- Instagram User
// Only mapping usefull data for login manipulation,
// Visit https://www.instagram.com/developer/endpoints/users/ for more usage.
struct InstagramUserDataModel : Codable {

    var instaUser : InstagramUser?
    
    private enum CodingKeys: String, CodingKey {
        case instaUser = "data"
    }
}

struct InstagramUser : Codable {
    
    var userFullName : String?
    
    private enum CodingKeys: String, CodingKey {
        case userFullName = "full_name"
    }
}

// This class desigend as singleton for fetching latest user data. All values are read only here
class SharedProfileInfo  : NSObject {
    
    public static let shared: SharedProfileInfo = {
        let instance = SharedProfileInfo()
        return instance
    }()

    private override init(){}
    
    var profileData : ProfileDataModel?
   
    func loadSharedProfileInfo(profileInfo: ProfileDataModel) -> () {
        self.profileData = profileInfo
        //AnalyticsHelper.shared.setMoEngageUserProfile()
    }
    
}


struct UpdateInfoModel : Codable {

    var isForceUpdate : Bool?
    var isRecommendedUpdate : Bool?
    
    private enum CodingKeys: String, CodingKey {
        case isForceUpdate = "force_upgrade"
        case isRecommendedUpdate = "recommend_upgrade"
    }
    
    init(from decoder: Decoder) throws {
        let updateInfo = try decoder.container(keyedBy: CodingKeys.self)
        
        // TODO: Remove hardcoded when server completes the changes.
        isForceUpdate = try updateInfo.decodeIfPresent(Bool.self, forKey: .isForceUpdate) ?? false
        isRecommendedUpdate = try updateInfo.decodeIfPresent(Bool.self, forKey: .isRecommendedUpdate) ?? false
        
    }
}
