//
//  PromosDataModel.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 13/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

struct PromosData {
    var promoId: String? = ""
    var promoTitle: String? = ""
    var promoDescription: String? = ""
    var promoDuration: String? = ""
    var promoImage: String? = nil
    var isExpanded: Bool = false
    var promotion_url = ""
    
    init(promoId: String, promoTitle: String, promoDescription: String, promoDuration: String = "", promoImage: String?, isExpanded: Bool = false, promotion_url: String) {
        self.promoId = promoId
        self.promoTitle = promoTitle
        self.promoDescription = promoDescription
        self.promoDuration = promoDuration
        self.promoImage = promoImage
        self.isExpanded = isExpanded
        self.promotion_url = promotion_url
    }
}
