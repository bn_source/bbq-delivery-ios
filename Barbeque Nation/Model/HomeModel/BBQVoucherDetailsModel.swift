//
 //  Created by Chandan Singh on 07/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQVoucherDetailsModel.swift
 //

import Foundation

struct VoucherDetails: Codable {
    var voucherBuffetId: String?
    var voucherCancellationPolicy: String?
    var voucherFoodClassification: String?
    var voucherTnC: String?
    var voucherTimings: [Timings]?
    var voucherTreatType: String?
    var voucherApplicableBranches: [VoucherBranch]?
    var voucherBannerPath: String?
    var voucherCurrency: String?
    var voucherDescription: String?
    var voucherGallery: [String]?
    var voucherId: Int?
    var voucherName: String?
    var voucherProductType: String?
    var voucherType: String?
    var voucherPrice: Double?
    var voucherValidOn: [String]?
    var voucherNotValidOn: [String]?
    var voucherSlots: [VoucherSlots]?
    var voucherHowToUse: String?
    var voucherValidity:String?
    
    private enum VoucherDetailsCodingKeys: String, CodingKey {
        case voucherBuffetId = "buffet_id"
        case voucherCancellationPolicy = "cancellation_policy"
        case voucherTimings = "timings"
        case voucherId = "voucher_id"
        case voucherProductType = "voucher_product_type"
        case voucherName = "voucher_name"
        case voucherBannerPath = "voucher_banner"
        case voucherGallery = "voucher_gallery"
        case voucherDescription = "voucher_description"
        case voucherType = "voucher_type"
        case voucherFoodClassification = "food_classification"
        case voucherTreatType = "treat_type"
        case voucherTnC = "terms_and_condition"
        case voucherApplicableBranches = "voucher_applicable_branches"
        case voucherPrice = "voucher_price"
        case voucherCurrency = "voucher_currency"
        case voucherValidOn = "voucher_valid_on"
        case voucherValidity = "voucher_validity"
        case voucherNotValidOn = "voucher_not_valid_on"
        case voucherSlots = "voucher_slots"
        case voucherHowToUse = "voucher_how_to_use"
    }
}

extension VoucherDetails {
    init(from decoder: Decoder)throws {
        let voucherDetailsInfo = try decoder.container(keyedBy: VoucherDetailsCodingKeys.self)
        voucherCancellationPolicy = try voucherDetailsInfo.decodeIfPresent(String.self, forKey: .voucherCancellationPolicy) ?? nil
        voucherId = try voucherDetailsInfo.decodeIfPresent(Int.self, forKey: .voucherId) ?? 0
        voucherTimings = try voucherDetailsInfo.decodeIfPresent([Timings].self, forKey: .voucherTimings) ?? nil
        voucherProductType = try voucherDetailsInfo.decodeIfPresent(String.self, forKey: .voucherProductType) ?? ""
        voucherName = try voucherDetailsInfo.decodeIfPresent(String.self, forKey: .voucherName) ?? ""
        voucherBannerPath = try voucherDetailsInfo.decodeIfPresent(String.self, forKey: .voucherBannerPath) ?? ""
        
        voucherDescription = try voucherDetailsInfo.decodeIfPresent(String.self, forKey: .voucherDescription) ?? ""
        voucherType = try voucherDetailsInfo.decodeIfPresent(String.self, forKey: .voucherType) ?? ""
        voucherFoodClassification = try voucherDetailsInfo.decodeIfPresent(String.self, forKey: .voucherFoodClassification) ?? ""
        
        voucherTreatType = try voucherDetailsInfo.decodeIfPresent(String.self, forKey: .voucherTreatType) ?? ""
        voucherTnC = try voucherDetailsInfo.decodeIfPresent(String.self, forKey: .voucherTnC) ?? nil
        voucherBuffetId = try voucherDetailsInfo.decodeIfPresent(String.self, forKey: .voucherBuffetId) ?? ""
        voucherApplicableBranches = try voucherDetailsInfo.decodeIfPresent([VoucherBranch].self, forKey: .voucherApplicableBranches) ?? nil
        
        voucherPrice = try voucherDetailsInfo.decodeIfPresent(Double.self, forKey: .voucherPrice) ?? 0.0
        voucherCurrency = try voucherDetailsInfo.decodeIfPresent(String.self, forKey: .voucherCurrency) ?? ""
        voucherValidOn = try voucherDetailsInfo.decodeIfPresent([String].self, forKey: .voucherValidOn) ?? nil
        voucherNotValidOn = try voucherDetailsInfo.decodeIfPresent([String].self, forKey: .voucherNotValidOn) ?? nil
        voucherSlots = try voucherDetailsInfo.decodeIfPresent([VoucherSlots].self, forKey: .voucherSlots) ?? nil
        voucherHowToUse = try voucherDetailsInfo.decodeIfPresent(String.self, forKey: .voucherHowToUse) ?? nil
        voucherValidity = try voucherDetailsInfo.decodeIfPresent(String.self, forKey: .voucherValidity) ?? nil
        //voucherGallery = try voucherDetailsInfo.decodeIfPresent([String].self, forKey: .voucherId) ?? nil
    }
}

struct Timings: Codable {
    var day: String?
    var time: String?
    
    private enum TimingsKeys: String, CodingKey {
        case day = "day"
        case time = "time"
    }
}

extension Timings {
    init(from decoder: Decoder)throws {
        let timingsInfo = try decoder.container(keyedBy: TimingsKeys.self)
        day = try timingsInfo.decodeIfPresent(String.self, forKey: .day) ?? ""
        time = try timingsInfo.decodeIfPresent(String.self, forKey: .time) ?? ""
    }
}

struct VoucherBranch: Codable {
    var voucherBranchId: String?
    var voucherBranchName: String?
    var voucherBranchCity: String?
    var voucherBranchCountry: String?
    
    private enum VoucherBranchCodingKeys: String, CodingKey {
        case voucherBranchId = "id"
        case voucherBranchName = "branch_name"
        case voucherBranchCity = "city"
        case voucherBranchCountry = "country"
    }
}

extension VoucherBranch {
    init(from decoder: Decoder)throws {
        let voucherBranchInfo = try decoder.container(keyedBy: VoucherBranchCodingKeys.self)
        voucherBranchId = try voucherBranchInfo.decodeIfPresent(String.self, forKey: .voucherBranchId) ?? ""
        voucherBranchName = try voucherBranchInfo.decodeIfPresent(String.self, forKey: .voucherBranchName) ?? ""
        voucherBranchCity = try voucherBranchInfo.decodeIfPresent(String.self, forKey: .voucherBranchCity) ?? ""
        voucherBranchCountry = try voucherBranchInfo.decodeIfPresent(String.self, forKey: .voucherBranchCountry) ?? ""
    }
}

struct VoucherSlots: Codable {
    var voucherSlotId: String?
    var voucherSlotStartTime: String?
    var voucherSlotEndTime: String?
    
    private enum VoucherSlotsCodingKeys: String, CodingKey {
        case voucherSlotId = "slot_id"
        case voucherSlotStartTime = "slot_start_time"
        case voucherSlotEndTime = "slot_end_time"
    }
}

extension VoucherSlots {
    init(from decoder: Decoder)throws {
        let voucherSlotsInfo = try decoder.container(keyedBy: VoucherSlotsCodingKeys.self)
        voucherSlotId = try voucherSlotsInfo.decodeIfPresent(String.self, forKey: .voucherSlotId) ?? ""
        voucherSlotStartTime = try voucherSlotsInfo.decodeIfPresent(String.self, forKey: .voucherSlotStartTime) ?? ""
        voucherSlotEndTime = try voucherSlotsInfo.decodeIfPresent(String.self, forKey: .voucherSlotEndTime) ?? ""
    }
}
