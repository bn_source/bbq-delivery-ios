//
//  BBQHomeDataModel.swift
//  Barbeque Nation
//
//  Created by Maya R on 12/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import Foundation

struct BBQHomeDataModel:Codable{
    var vouchers : [VouchersModel]?
    var promotions: [PromotionModel]?

    private enum HomeCodingKeys: String, CodingKey {
        case voucher = "vouchers"
        case promotion = "promotion"
    }
}

struct PromotionModel:Codable {
    var promotionId: Int?
    var promotion_title: String?
    var promotion_image: String?
    var promotion_description: String?
    var promotion_url: String?
    
    private enum PromotionCodingKeys: String, CodingKey {
        case promotionId = "promotion_id"
        case promotion_title = "promotion_title"
        case promotion_image = "promotion_image"
        case promotion_description = "promotion_description"
        case promotion_url = "promotion_url"
    }
}

struct VouchersModel:Codable {
    var voucherId: Int?
    var voucherName: String?
    var voucherImage: String?
    var voucherShareUrl: String?
    var voucherDesc: String?
    var voucherPrice: VouchersPrice?
    
    private enum VoucherCodingKeys: String, CodingKey {
        case voucherId = "voucher_id"
        case voucherShareUrl = "voucher_url"
        case voucherName = "voucher_name"
        case voucherImage = "image"
        case voucherPrice = "price"
        case voucherDesc = "description"
    }
}

struct VouchersPrice:Codable {
    var number: Double?
    var currencyCode: String?
    
    private enum VouchersPriceCodingKeys: String, CodingKey {
        case number = "number"
        case currencyCode = "currency_code"
    }
}

struct UpcomingReservationModel :Codable {
    var upcoming_reservations: [UpcomingBookingModel]?
    
    private enum UpcomingReservationCodingKeys: String, CodingKey {
        case upcoming_reservation = "bookings"
    }
}

struct UpcomingBookingModel :Codable {
    var bookingId: String?
    var branchName: String?
    var branchId: String?
    var bookingStatus: String?
    var bookedBranchAddress:String?
    var branchImage:String?
    var reservationDate:String?
    var occasionDescription:String?
    var branchContactNumber: String?
    var latitude: Double?
    var longitude: Double?
    var billTotal: Double?
    var netPayable: Double?
    var currency :String?
    var showPayment: Bool
    var bill_link: String?
    
    
    private enum UpcomingBookingCodingKeys: String, CodingKey {
        case bookingId = "booking_id"
        case branchName = "branch_name"
        case bookingStatus = "booking_status"
        case bookedBranchAddress = "branch_address"
        case branchImage   = "branch_image"
        case reservationDate = "reservation_date"
        case occasionDescription = "occasion_description"
        case branchContactNumber = "branch_contact_no"
        case latitude = "lat"
        case longitude = "long"
        case billTotal = "bill_total"
        case netPayable = "net_payable"
        case currency = "currency"
        case branchId = "branch_id"
        case showPayment = "show_payment"
        case bill_link = "bill_link"
    }
}

struct HomeLocationModel :Codable {
    var near_by_Outlets: [NearbyOutelt]?
    
    private enum HomeLocationModelCodingKeys: String, CodingKey {
        case near_by_Outlet = "near_by"
    }
}

struct NearbyOutelt :Codable {
    var nearbyBranchId: String?
    var nearbyBranchName: String?
    var nearbyBranchLatitude: String?
    var nearbyBranchLongitude:String?
    var nearbyBranchDistance:String?
    var nearbyCityCode:String?
    var nearbyCityName:String?
    var nearbyBranchImage: String?
    var nearbyBrandLogo: String?
    var nearbyOnlyDelivery: Bool?
    var nearbyOnlyDining: Bool?

    private enum NearbyOuteltCodingKeys: String, CodingKey {
        case nearbyBranchId = "branch_id"
        case nearbyBranchName = "branch_name"
        case nearbyBranchLatitude = "latitude"
        case nearbyBranchLongitude = "longitude"
        case nearbyBranchDistance = "distance"
        case nearbyCityCode = "city_code"
        case nearbyCityName = "city_name"
        case nearbyBranchImage = "branch_image"
        case nearbyBrandLogo = "brand_logo"
        case nearbyOnlyDelivery = "only_delivery"
        case nearbyOnlyDining = "only_dining"
    }
    
    func getDistance() -> String?{
        if let nearbyBranchDistance = nearbyBranchDistance, let distance = Double(nearbyBranchDistance){
            if distance > 1{
                return String(format: "  %.02fkm   ", distance)
            }else{
                return String(format: "  %.0fm   ", distance * 1000)
            }
        }
        return nil
    }
}

extension BBQHomeDataModel {
    init(from decoder: Decoder) throws {
        let homeDataInfo = try decoder.container(keyedBy: HomeCodingKeys.self)
        promotions = try homeDataInfo.decode([PromotionModel].self, forKey: .promotion)
        vouchers = try homeDataInfo.decode([VouchersModel].self, forKey: .voucher)

    }
}

extension PromotionModel {
    init(from decoder: Decoder) throws {
        let promotionInfo = try decoder.container(keyedBy: PromotionCodingKeys.self)
        promotionId = try promotionInfo.decodeIfPresent(Int.self, forKey: .promotionId) ?? 0
        promotion_title = try promotionInfo.decodeIfPresent(String.self, forKey: .promotion_title) ?? ""
        promotion_image = try promotionInfo.decodeIfPresent(String.self, forKey: .promotion_image) ?? ""
        promotion_description = try promotionInfo.decodeIfPresent(String.self, forKey: .promotion_description) ?? ""
        promotion_url = try promotionInfo.decodeIfPresent(String.self, forKey: .promotion_url) ?? ""
    }
}

extension VouchersModel {
    init(from decoder: Decoder) throws {
        let vouchInfo = try decoder.container(keyedBy: VoucherCodingKeys.self)
        voucherId = try vouchInfo.decodeIfPresent(Int.self, forKey: .voucherId) ?? 0
        voucherShareUrl = try vouchInfo.decodeIfPresent(String.self, forKey: .voucherShareUrl) ?? ""

        voucherName = try vouchInfo.decodeIfPresent(String.self, forKey: .voucherName) ?? ""
        voucherImage = try vouchInfo.decodeIfPresent(String.self, forKey: .voucherImage) ?? ""
        voucherShareUrl = try vouchInfo.decodeIfPresent(String.self, forKey: .voucherShareUrl) ?? ""
        voucherPrice =  try vouchInfo.decodeIfPresent(VouchersPrice.self, forKey: .voucherPrice) ?? nil
        voucherDesc = try vouchInfo.decodeIfPresent(String.self, forKey: .voucherDesc) ?? ""
    }
}

extension VouchersPrice {
    init(from decoder: Decoder) throws {
        let priceInfo = try decoder.container(keyedBy: VouchersPriceCodingKeys.self)
        number = try priceInfo.decodeIfPresent(Double.self, forKey: .number) ?? 0.0
        currencyCode = try priceInfo.decodeIfPresent(String.self, forKey: .currencyCode) ?? ""
    }
}
extension UpcomingReservationModel{
    init(from decoder:Decoder)throws{
        let upcomingInfo = try decoder.container(keyedBy: UpcomingReservationCodingKeys.self)
        upcoming_reservations = try upcomingInfo.decode([UpcomingBookingModel].self, forKey: .upcoming_reservation)
    }
}
extension UpcomingBookingModel{
    init(from decoder:Decoder)throws{
        let upcomingBookingInfo = try decoder.container(keyedBy: UpcomingBookingCodingKeys.self)
        bookingId = try upcomingBookingInfo.decodeIfPresent(String.self, forKey: .bookingId) ?? ""
        branchName = try upcomingBookingInfo.decodeIfPresent(String.self, forKey: .branchName) ?? ""
        bookingStatus = try upcomingBookingInfo.decodeIfPresent(String.self, forKey: .bookingStatus) ?? ""
        bookedBranchAddress = try upcomingBookingInfo.decodeIfPresent(String.self, forKey: .bookedBranchAddress) ?? ""
        branchImage = try upcomingBookingInfo.decodeIfPresent(String.self, forKey: .branchImage) ?? ""
        reservationDate = try upcomingBookingInfo.decodeIfPresent(String.self, forKey: .reservationDate) ?? ""
        occasionDescription = try upcomingBookingInfo.decodeIfPresent(String.self, forKey: .occasionDescription) ?? ""
        branchContactNumber = try upcomingBookingInfo.decodeIfPresent(String.self, forKey: .branchContactNumber) ?? ""
        latitude = try upcomingBookingInfo.decodeIfPresent(Double.self, forKey: .latitude) ?? 0.0
        longitude = try upcomingBookingInfo.decodeIfPresent(Double.self, forKey: .longitude) ?? 0.0
        billTotal = try upcomingBookingInfo.decodeIfPresent(Double.self, forKey: .billTotal) ?? 0.0
        netPayable = try upcomingBookingInfo.decodeIfPresent(Double.self, forKey: .netPayable) ?? nil
        currency = try upcomingBookingInfo.decodeIfPresent(String.self, forKey: .currency) ?? ""
        branchId = try upcomingBookingInfo.decodeIfPresent(String.self, forKey: .branchId)
        showPayment = try upcomingBookingInfo.decodeIfPresent(Bool.self, forKey: .showPayment) ?? false
        bill_link = try upcomingBookingInfo.decodeIfPresent(String.self, forKey: .bill_link)
    }
    
    
}
extension HomeLocationModel{
    init(from decoder:Decoder)throws{
        let loactionModelInfo = try decoder.container(keyedBy: HomeLocationModelCodingKeys.self)
        near_by_Outlets = try loactionModelInfo.decode([NearbyOutelt].self, forKey: .near_by_Outlet)
    }
}

extension NearbyOutelt{
    init(from decoder:Decoder)throws{
        let nearbyOutletInfo = try decoder.container(keyedBy: NearbyOuteltCodingKeys.self)
        
        nearbyBranchId = try nearbyOutletInfo.decodeIfPresent(String.self, forKey: .nearbyBranchId) ?? ""
        nearbyBranchName = try nearbyOutletInfo.decodeIfPresent(String.self, forKey: .nearbyBranchName) ?? ""
        nearbyBranchLatitude = try nearbyOutletInfo.decodeIfPresent(String.self, forKey: .nearbyBranchLatitude) ?? ""
        nearbyBranchLongitude = try nearbyOutletInfo.decodeIfPresent(String.self, forKey: .nearbyBranchLongitude) ?? ""
        nearbyBranchDistance = try nearbyOutletInfo.decodeIfPresent(String.self, forKey: .nearbyBranchDistance) ?? ""
        nearbyCityCode = try nearbyOutletInfo.decodeIfPresent(String.self, forKey: .nearbyCityCode) ?? ""
        nearbyCityName = try nearbyOutletInfo.decodeIfPresent(String.self, forKey: .nearbyCityName) ?? ""
        nearbyBranchImage = try nearbyOutletInfo.decodeIfPresent(String.self, forKey: .nearbyBranchImage) ?? ""
        nearbyBrandLogo = try nearbyOutletInfo.decodeIfPresent(String.self, forKey: .nearbyBrandLogo) ?? ""
        nearbyOnlyDelivery = (try nearbyOutletInfo.decodeIfPresent(String.self, forKey: .nearbyOnlyDelivery) ?? "0") == "0" ? false : true
        nearbyOnlyDining = (try nearbyOutletInfo.decodeIfPresent(String.self, forKey: .nearbyOnlyDining) ?? "0") == "0" ? false : true
    }
    
}
