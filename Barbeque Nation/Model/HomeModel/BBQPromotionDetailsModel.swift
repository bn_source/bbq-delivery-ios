//
 //  Created by Chandan Singh on 15/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQPromotionDetailsModel.swift
 //

import Foundation

struct PromotionDetailsModel: Codable {
    var promotionTitle: String?
    var promotionImage: String?
    var promotionDescription: String?
    var promotionCoupon: String?
    var privacyAndPolicy: String?
    var promotionTimings: [PromotionTimings]?//array
    var promotionApplicableOn: String?
    var promotionNotValidOn: String?
    var promotionValidity: String?
    var promotionRegionTiming: String?
    var thingsToRemember: String?
    var promotionRegion: [String]?
    
    private enum PromotionDetailsModelCodingKeys: String, CodingKey {
        case promotionTitle = "promotion_title"
        case promotionImage = "promotion_image"
        case promotionDescription = "promotion_description"
        case promotionCoupon = "promotion_coupon"
        case privacyAndPolicy = "privacy_and_policy"
        case promotionTimings = "promotion_timings"
        case promotionApplicableOn = "promotion_applicable_on"
        case promotionNotValidOn = "promotion_not_valid_on"
        case promotionValidity = "promotion_validity"
        case promotionRegionTiming = "promotion_region_timing"
        case thingsToRemember = "things_to_remember"
        case promotionRegion = "promotion_region"
    }
}

extension PromotionDetailsModel {
    init(from decoder: Decoder)throws {
        let promotionDetailInfo = try decoder.container(keyedBy: PromotionDetailsModelCodingKeys.self)
        privacyAndPolicy = try promotionDetailInfo.decodeIfPresent(String.self, forKey: .privacyAndPolicy) ?? nil
        promotionApplicableOn = try promotionDetailInfo.decodeIfPresent(String.self, forKey: .promotionApplicableOn) ?? nil
        promotionCoupon = try promotionDetailInfo.decodeIfPresent(String.self, forKey: .promotionCoupon) ?? ""
        promotionDescription = try promotionDetailInfo.decodeIfPresent(String.self, forKey: .promotionDescription) ?? ""
        promotionImage = try promotionDetailInfo.decodeIfPresent(String.self, forKey: .promotionImage) ?? ""
        promotionNotValidOn = try promotionDetailInfo.decodeIfPresent(String.self, forKey: .promotionNotValidOn) ?? nil
        promotionRegion = try promotionDetailInfo.decodeIfPresent([String].self, forKey: .promotionRegion) ?? nil
        promotionTimings = try promotionDetailInfo.decodeIfPresent([PromotionTimings].self, forKey: .promotionTimings) ?? nil
        promotionTitle = try promotionDetailInfo.decodeIfPresent(String.self, forKey: .promotionTitle) ?? ""
        promotionValidity = try promotionDetailInfo.decodeIfPresent(String.self, forKey: .promotionValidity) ?? nil
        thingsToRemember = try promotionDetailInfo.decodeIfPresent(String.self, forKey: .thingsToRemember) ?? nil
        promotionRegionTiming = try promotionDetailInfo.decodeIfPresent(String.self, forKey: .promotionRegionTiming) ?? nil
    }
}

struct PromotionTimings: Codable {
    var day: String?
    var timings: String?
    
    private enum PromotionTimingsCodingKeys: String, CodingKey {
        case day = "day"
        case timings = "timings"
    }
}

extension PromotionTimings {
    init(from decoder: Decoder)throws {
        let timingsInfo = try decoder.container(keyedBy: PromotionTimingsCodingKeys.self)
        day = try timingsInfo.decodeIfPresent(String.self, forKey: .day) ?? ""
        timings = try timingsInfo.decodeIfPresent(String.self, forKey: .timings) ?? ""
    }
}

/*
struct PromotionLink: Codable {
    var entityId: String?
    var title: String?
    var type: String?
    
    private enum PromotionLinkCodingKeys: String, CodingKey {
        case entityId = "entity_id"
        case title = "title"
        case type = "type"
    }
}

extension PromotionLink {
    init(from decoder: Decoder)throws {
        let promotionLinkInfo = try decoder.container(keyedBy: PromotionLinkCodingKeys.self)
        entityId = try promotionLinkInfo.decodeIfPresent(String.self, forKey: .entityId) ?? ""
        title = try promotionLinkInfo.decodeIfPresent(String.self, forKey: .title) ?? ""
        type = try promotionLinkInfo.decodeIfPresent(String.self, forKey: .type) ?? ""
    }
}


struct PromotionApplicableOn: Codable {
    var notValidFor: String?
    var validFor: String?
    
    private enum PromotionApplicableOnCodingKeys: String, CodingKey {
        case notValidFor = "not_valid_for"
        case validFor = "valid_for"
    }
}

extension PromotionApplicableOn {
    init(from decoder: Decoder)throws {
        let applicableOnInfo = try decoder.container(keyedBy: PromotionApplicableOnCodingKeys.self)
        notValidFor = try applicableOnInfo.decodeIfPresent(String.self, forKey: .notValidFor) ?? ""
        validFor = try applicableOnInfo.decodeIfPresent(String.self, forKey: .validFor) ?? ""
    }
}
*/
