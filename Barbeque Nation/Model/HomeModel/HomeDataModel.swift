//
//  HomeModel.swift
//  Barbeque Nation
//
//  Created by Maya R on 12/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import Foundation

struct PromotionDataModel {
    var promoName: String?
    var promoImageName: String?
    init() {
        promoName = ""
        promoImageName = ""
    }
}

struct VoucherDataModel: Codable {
    var voucherName: String?
    var voucherImageName: String?
    var voucherPrice: String?
    init() {
        voucherName = ""
        voucherImageName = ""
        voucherPrice = ""
    }
        
}
