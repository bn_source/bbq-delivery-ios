//
 //  Created by Arpana on 27/09/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified VoucherAndCouponListWithCountModel.swift
 //

//Copyright (c) 2022 Swift Models Generated from JSON powered by http://www.json4swift.com
// To parse the JSON, add this file to your project and do:
//
//   let voucherAndCouponListWithCountModel = try? newJSONDecoder().decode(VoucherAndCouponListWithCountModel.self, from: jsonData)

import Foundation

struct VoucherAndCouponListWithCountModel : Codable {
    let message : String?
    let message_type : String?
    let VCData : voucherCouponData?
    let terms_conditions : [String]?
    var referral_available: Bool = true
    enum CodingKeys: String, CodingKey {

        case message = "message"
        case message_type = "message_type"
        case VCData = "data"
        case terms_conditions = "terms_conditions"
        case referral_available = "referral_available"

    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        message_type = try values.decodeIfPresent(String.self, forKey: .message_type)
        VCData = try values.decodeIfPresent(voucherCouponData.self, forKey: .VCData)
        terms_conditions = try values.decodeIfPresent([String].self, forKey: .terms_conditions)
        referral_available = try values.decodeIfPresent(Bool.self, forKey: .referral_available) ?? false


    }

}

struct CouponCounts : Codable {
    let available : Int?
    let total : Int?
    let redeemed : Int?
    let expired : Int?
    let expiring_count : Int?
    let expiring_timestamp : String?

    enum CodingKeys: String, CodingKey {

        case available = "available"
        case total = "total"
        case redeemed = "redeemed"
        case expired = "expired"
        case expiring_count = "expiring_count"
        case expiring_timestamp = "expiring_timestamp"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        available = try values.decodeIfPresent(Int.self, forKey: .available)
        total = try values.decodeIfPresent(Int.self, forKey: .total)
        redeemed = try values.decodeIfPresent(Int.self, forKey: .redeemed)
        expired = try values.decodeIfPresent(Int.self, forKey: .expired)
        expiring_count = try values.decodeIfPresent(Int.self, forKey: .expiring_count)
        expiring_timestamp = try values.decodeIfPresent(String.self, forKey: .expiring_timestamp)
    }

}
struct CouponsLists : Codable {
    let name : String?
    let barcode : String?
    let title : String?
    let expiring_date : String?
    let description : String?
    let terms_condition : String?

    enum CodingKeys: String, CodingKey {

        case name = "name"
        case barcode = "barcode"
        case title = "title"
        case expiring_date = "expiring_date"
        case description = "description"
        case terms_condition = "terms_condition"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        barcode = try values.decodeIfPresent(String.self, forKey: .barcode)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        expiring_date = try values.decodeIfPresent(String.self, forKey: .expiring_date)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        terms_condition = try values.decodeIfPresent(String.self, forKey: .terms_condition)
    }

}
struct voucherCouponData : Codable {
    let couponCounts : CouponCounts?
    let voucherCounts : VoucherCounts?
    let vouchersLists : [VouchersLists]?
    let couponsLists : [CouponsLists]?

    enum CodingKeys: String, CodingKey {

        case couponCounts = "couponCounts"
        case voucherCounts = "voucherCounts"
        case vouchersLists = "vouchersLists"
        case couponsLists = "couponsLists"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        couponCounts = try values.decodeIfPresent(CouponCounts.self, forKey: .couponCounts)
        voucherCounts = try values.decodeIfPresent(VoucherCounts.self, forKey: .voucherCounts)
        vouchersLists = try values.decodeIfPresent([VouchersLists].self, forKey: .vouchersLists)
        couponsLists = try values.decodeIfPresent([CouponsLists].self, forKey: .couponsLists)
    }

}
struct VoucherCounts : Codable {
    let available : Int?
    let total : Int?
    let redeemed : Int?
    let expired : Int?
    let expiring_count : Int?
    let expiring_timestamp : String?

    enum CodingKeys: String, CodingKey {

        case available = "available"
        case total = "total"
        case redeemed = "redeemed"
        case expired = "expired"
        case expiring_count = "expiring_count"
        case expiring_timestamp = "expiring_timestamp"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        available = try values.decodeIfPresent(Int.self, forKey: .available)
        total = try values.decodeIfPresent(Int.self, forKey: .total)
        redeemed = try values.decodeIfPresent(Int.self, forKey: .redeemed)
        expired = try values.decodeIfPresent(Int.self, forKey: .expired)
        expiring_count = try values.decodeIfPresent(Int.self, forKey: .expiring_count)
        expiring_timestamp = try values.decodeIfPresent(String.self, forKey: .expiring_timestamp)
    }

}
struct VouchersLists : Codable {


    let issue_date : String?
    let min_amount : String?
    let min_packs : String?
    let order_id : String?
    let voucher_head_id : Int?
    var barCode : String?
    var denomination : Int?
    var giftReceived : String?
    var giftReceivedOn : String?
    var giftTo : String?
    var giftToOn : String?
    var giftType : String?
    var source : String?
    var status : String?
    var title : String?
    var validity : String?
    var voucherType : String?
    var currency : String?
    var purchaseDate :String?
    var voucherImage: String?
    var description: String?
    let condition : String?

    enum CodingKeys: String, CodingKey {

        case barCode = "barcode"
        case condition = "condition"
        case currency = "currency"
        case denomination = "denomination"
        case description = "description"
        case giftReceived = "gift_received"
        case giftReceivedOn = "gift_received_on"
        case giftTo = "gift_to"
        case giftToOn = "gift_to_on"
        case giftType = "gift_type"
        case issue_date = "issue_date"
        case min_amount = "min_amount"
        case min_packs = "min_packs"
        case order_id = "order_id"
        case source = "source"
        case status = "status"
        case title = "title"
        case validity = "validity"
        case voucher_head_id = "voucher_head_id"
        case voucherType = "voucher_type"
        case voucherImage = "image"

    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        barCode = try values.decodeIfPresent(String.self, forKey: .barCode)
        condition = try values.decodeIfPresent(String.self, forKey: .condition)
        currency = try values.decodeIfPresent(String.self, forKey: .currency)
        denomination = try values.decodeIfPresent(Int.self, forKey: .denomination)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        giftReceived = try values.decodeIfPresent(String.self, forKey: .giftReceived)
        giftReceivedOn = try values.decodeIfPresent(String.self, forKey: .giftReceivedOn)
        giftTo = try values.decodeIfPresent(String.self, forKey: .giftTo)
        giftToOn = try values.decodeIfPresent(String.self, forKey: .giftToOn)
        giftType = try values.decodeIfPresent(String.self, forKey: .giftType)
        issue_date = try values.decodeIfPresent(String.self, forKey: .issue_date)
        min_amount = try values.decodeIfPresent(String.self, forKey: .min_amount)
        min_packs = try values.decodeIfPresent(String.self, forKey: .min_packs)
        order_id = try values.decodeIfPresent(String.self, forKey: .order_id)
        source = try values.decodeIfPresent(String.self, forKey: .source)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        validity = try values.decodeIfPresent(String.self, forKey: .validity)
        voucher_head_id = try values.decodeIfPresent(Int.self, forKey: .voucher_head_id)
        voucherType = try values.decodeIfPresent(String.self, forKey: .voucherType)
        voucherImage = try values.decodeIfPresent(String.self, forKey: .voucherImage)
    }

}

//struct VouchersLists : Codable {
//    let name : String?
//    let barcode : String?
//    let title : String?
//    let expiring_date : String?
//    let description : String?
//    let terms_condition : String?
//
//    enum CodingKeys: String, CodingKey {
//
//        case name = "name"
//        case barcode = "barcode"
//        case title = "title"
//        case expiring_date = "expiring_date"
//        case description = "description"
//        case terms_condition = "terms_condition"
//    }
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        name = try values.decodeIfPresent(String.self, forKey: .name)
//        barcode = try values.decodeIfPresent(String.self, forKey: .barcode)
//        title = try values.decodeIfPresent(String.self, forKey: .title)
//        expiring_date = try values.decodeIfPresent(String.self, forKey: .expiring_date)
//        description = try values.decodeIfPresent(String.self, forKey: .description)
//        terms_condition = try values.decodeIfPresent(String.self, forKey: .terms_condition)
//    }
//
//}
