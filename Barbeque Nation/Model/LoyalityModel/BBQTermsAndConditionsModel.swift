//
 //  Created by Bhamidipati Kishore on 14/11/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQTermsAndConditionsModel.swift
 //

import Foundation

struct BBQMobileTermsAndConditionsDataModel: Codable {
    let title: String?
    let terms_and_condition:String?
    
    private enum BBQMobileTermsAndConditionsDataModelKeys: String, CodingKey {
        case title = "title"
        case terms_and_condition = "terms_and_condition"
    }
}

extension BBQMobileTermsAndConditionsDataModel {
    init(from decoder:Decoder) throws {
        let TermsModelInfo = try decoder.container(keyedBy: BBQMobileTermsAndConditionsDataModelKeys.self)
        title = try TermsModelInfo.decodeIfPresent(String.self, forKey: .title) ?? ""
        terms_and_condition = try TermsModelInfo.decodeIfPresent(String.self, forKey: .terms_and_condition) ?? ""
    }
}
