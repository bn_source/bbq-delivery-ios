//
//  Created by Bhamidipati Kishore on 23/09/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQLoyalityPointsModel.swift
//

import Foundation

struct BBQLoyalityPointsDataModel: Codable {
    let pointsEarned: Int?
    let pointsSpent:Int?
    let availablePoints:Int?
    let membership:Int?
    let loyaltyTransactions:[LoyaltyTransactions]?
    let totalElements: Int?
    let pageNumber: Int?
    let pageSize: Int?
    
    private enum BBQLoyalityPointsDataModelKeys: String, CodingKey {
        case pointsEarned = "points_earned"
        case pointsSpent = "points_spent"
        case availablePoints = "available_points"
        case membership = "membership"
        case loyaltyTransactions = "loyalty_transactions"
        case totalElements = "total_elements"
        case pageNumber = "page_number"
        case pageSize = "page_size"
    }
}

// MARK: - LoyaltyTransactions
struct LoyaltyTransactions: Codable {
    var points: Int?
    var transaction_type: String?
    var description: String?
    var expiryDate : Int64?
    var eventType : String?
    var transactionID : String?
    var created_on: TimeInterval?
    
    private enum loyaltyTransactionsKeys: String, CodingKey {
        case points = "points"
        case transaction_type = "transaction_type"
        case description = "description"
        case expiryDate = "expiration_date"
        case eventType = "loyalty_event_type"
        case transactionID = "transaction_id"
        case created_on = "created_on"
    }
    
}

extension BBQLoyalityPointsDataModel {
    init(from decoder:Decoder) throws {
        let LoyaltyModelInfo = try decoder.container(keyedBy: BBQLoyalityPointsDataModelKeys.self)
        pointsEarned = try LoyaltyModelInfo.decodeIfPresent(Int.self, forKey: .pointsEarned) ?? 0
        pointsSpent = try LoyaltyModelInfo.decodeIfPresent(Int.self, forKey: .pointsSpent) ?? 0
        availablePoints = try LoyaltyModelInfo.decodeIfPresent(Int.self, forKey: .availablePoints) ?? 0
        membership = try LoyaltyModelInfo.decodeIfPresent(Int.self, forKey: .membership) ?? 0
        loyaltyTransactions = try LoyaltyModelInfo.decodeIfPresent([LoyaltyTransactions].self, forKey: .loyaltyTransactions)
        totalElements = try LoyaltyModelInfo.decodeIfPresent(Int.self, forKey: .totalElements)
        pageNumber = try LoyaltyModelInfo.decodeIfPresent(Int.self, forKey: .pageNumber)
        pageSize = try LoyaltyModelInfo.decodeIfPresent(Int.self, forKey: .pageSize )
    }
}

extension LoyaltyTransactions {
    init(from decoder:Decoder) throws {
        let LoyaltyTransactionsInfo = try decoder.container(keyedBy: loyaltyTransactionsKeys.self)
        points = try LoyaltyTransactionsInfo.decodeIfPresent(Int.self, forKey: .points) ?? 0
        transaction_type = try LoyaltyTransactionsInfo.decodeIfPresent(String.self, forKey: .transaction_type) ?? ""
        description = try LoyaltyTransactionsInfo.decodeIfPresent(String.self, forKey: .description) ?? ""
        expiryDate = try LoyaltyTransactionsInfo.decodeIfPresent(Int64.self, forKey: .expiryDate )
        eventType = try LoyaltyTransactionsInfo.decodeIfPresent(String.self, forKey: .eventType) ?? ""
        transactionID = try LoyaltyTransactionsInfo.decodeIfPresent(String.self, forKey: .transactionID) ?? ""
        created_on = try LoyaltyTransactionsInfo.decodeIfPresent(TimeInterval.self, forKey: .created_on)
    }
}
