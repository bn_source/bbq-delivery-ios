//
//  Created by Bhamidipati Kishore on 19/09/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQNotificationModel.swift
//

import Foundation

struct NotificationModel {
    var titleString:String
    var messageString:String
    var imageUrlString:String
    
    init(titleString:String, messageString:String, imageUrlString:String? = nil) {
        self.titleString = titleString
        self.messageString = messageString
        self.imageUrlString = imageUrlString ?? ""
    }
}
