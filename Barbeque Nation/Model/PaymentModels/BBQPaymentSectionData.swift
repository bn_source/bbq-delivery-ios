//
//  BBQPaymentSectionData.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 11/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import Foundation

struct RowItem {
    var placeholder: String
    var amount: String
    
    public init(placeholder: String, amount: String) {
        self.placeholder = placeholder
        self.amount = amount
    }
}

struct Section {
    var title: String
    var rowItems: [RowItem]
    var collapsed: Bool
    
    public init(title: String, rowItems: [RowItem], collapsed: Bool = true) {
        self.title = title
        self.rowItems = rowItems
        self.collapsed = collapsed
    }
}
