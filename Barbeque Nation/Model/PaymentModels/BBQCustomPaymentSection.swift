//
 //  Created by vinoth kumar on 09/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQCustomPaymentSection.swift
 //

import Foundation
import UIKit

struct CustomPaymentHeader:CellDataProvider {
    var title:String?
    var title1:String?
    var subtitle:String?
    var subtitle1:String?
    var isFirstCell: Bool = false
    var isLastCell: Bool = false

    func getAttribtedTitle() -> NSMutableAttributedString?{
        if let title = title, let title1 = title1 {
            let strAtr = NSMutableAttributedString()
            strAtr.append(NSMutableAttributedString(string: title, attributes: [NSAttributedString.Key.font: UIFont.systemFontOfSize(size: 14.0, weight: .regular), NSAttributedString.Key.foregroundColor: UIColor.darkText]))
            strAtr.append(NSMutableAttributedString(string: title1, attributes: [NSAttributedString.Key.font: UIFont.systemFontOfSize(size: 14.0, weight: .regular), NSAttributedString.Key.foregroundColor: UIColor.darkText]))
            return strAtr
        }
        return nil
    }
    
    func getAttribtedSubTitle() -> NSMutableAttributedString?{
        if let subtitle = subtitle, let subtitle1 = subtitle1 {
            let strAtr = NSMutableAttributedString()
            strAtr.append(NSMutableAttributedString(string: subtitle, attributes: [NSAttributedString.Key.font: UIFont.systemFontOfSize(size: 13.0, weight: .regular), NSAttributedString.Key.foregroundColor: UIColor.text as Any]))
            strAtr.append(NSMutableAttributedString(string: subtitle1, attributes: [NSAttributedString.Key.font: UIFont.systemFontOfSize(size: 14.0, weight: .medium), NSAttributedString.Key.foregroundColor: UIColor.theme as Any]))
            return strAtr
        }
        return nil
    }
}
struct StringData:CellDataProvider {
    var text:String?
    var font:UIFont?
    var fontColor:UIColor?
    var image:String?
    var isFirstCell: Bool = false
    var isLastCell: Bool = false
    
    init(text:String) {
        self.text = text
    }
    init(text:String,image:String) {
        self.text = text
        self.image = image
    }
}
struct SectionHeader {
    var data:String?
    var view:PaymentSectionHeader?
    init(data:String?) {
        
    }
}
struct SectionOptions {
    var header:PaymentSectionHeader?
    var separator:Bool = true
    var footer:Any?
    init(title:String) {
        let header:PaymentSectionHeader = .fromNib()
        header.titleLabel.text = title
        self.header = header
    }
}
enum CollectionCellSizeType {
    case full
    case cell
    case half
    case single
    case halfsqaure(heightFactor:CGFloat)
    case square(size:CGFloat)
    case fullPro(CGFloat)
    case fillCell(CGFloat)
    case showSome(Int)
    case custom(_ size:CGSize)
    case text(String,UIFont)
    case autoWidth
    case auto
}
class CellSection:CellDataProvider {
    var data:[CellDataProvider]?
    var options:SectionOptions?
    var type:AnyClass
    var cellSize:CollectionCellSizeType = .cell
    var isFirstCell: Bool = false
    var isLastCell: Bool = false
    
    init(data:[CellDataProvider],type:AnyClass,_ cellSize:CollectionCellSizeType = .cell){
        self.data = data
        self.type = type
    }
    init(options: SectionOptions, data: [CellDataProvider], type: AnyClass,_ cellSize:CollectionCellSizeType = .cell) {
        
        self.options = options
        self.data = data
        self.type = type
        self.cellSize = cellSize

    }
}
extension Array where Element == CellSection {
    func getSection(section:CustomPaymentSection) -> CellSection? {
        return self.filter({$0.type == section.type}).first
    }
}
