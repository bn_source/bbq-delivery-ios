//
//  BBQPaymentDataModel.swift
//  Barbeque Nation
//
//  Created by Ajith CP on 09/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

struct BBQPaymentDataModel : Codable {
    
    var paymentID : String?
    var razorpayOrderID : String?
    var razorpaySignature : String?
    
    private enum CodingKeys: String, CodingKey {
        
        case paymentID = "razorpay_payment_id"
        case razorpayOrderID   = "razorpay_order_id"
        case razorpaySignature = "razorpay_signature"
    }
}

/**
 Response model returned for create order API with razor pay
 */
struct BBQPaymentOrderDataModel : Codable {
   
    /// razorpay_order_id, this need to be passed with checkout flow
    var orderID : String?
    
    var orderEntity : String?
    var currency : String?
    var orderStatus : String?
    var orderReceipt : String?
    
    var orderAmount : Int64?
    var orderAmountPaid : Int64?
    var orderAmountDue : Int64?
    var orderAttempts : Int64?
    var orderCreatedAt : Int64?
    
    private enum CodingKeys: String, CodingKey {
       
        case orderID = "order_id"
        case orderEntity = "entity"
        case currency = "currency"
        case orderStatus = "status"
        case orderReceipt = "receipt"
        case orderAmount = "amount"
        case orderAmountPaid = "amount_paid"
        case orderAmountDue = "amount_due"
        case orderAttempts = "attempts"
        case orderCreatedAt = "created_at"
    }
}

/**
 Request model passed for checkout flow. Construct internally by BBQRazorpayContainerController
 */
struct BBQRazorPayRequestModel {
    
    var amount : String? // Mandatory
    var currency : String? // Mandatory
    var description : String?
    var order_id : String?
    var image : UIImage?
    var name : String?
    var prefill : [String:String]?
    var theme : [String:String]?
}
