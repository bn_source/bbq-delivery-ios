//
 //  Created by vinoth kumar on 09/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQCellDataProtocol.swift
 //

import Foundation
protocol CellDataProvider {
    //isSelected is set true is particular cell is selected in tableview
    var isSelected:Bool { get set }
    var isFirstCell:Bool { get set }
    var isLastCell:Bool { get set }
}
extension CellDataProvider {
    var isSelected: Bool {
        get {
            return false
        }
        set {
        }
    }
}
protocol CellDataRecevier {
    func didSetData(data:CellDataProvider)
    var delegate:Any? { get set}
}
extension CellDataRecevier {
    var delegate: Any? {
        get {
            return nil
        }
        set {
        }
    }
}
