//
 //  Created by vinoth kumar on 11/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQPaymentMethods.swift
 //

import Foundation
import UIKit
struct Wallet:CellDataProvider {
    var isFirstCell: Bool = false
    var isLastCell: Bool = false
    var name:String
    var isEnabled:Bool
    var logoURL:URL?
    init(razorPay:Razorpay,response:(key:String,value:Int)) {
        self.name = response.key
        self.isEnabled = (response.value != 0) ? true : false
        self.logoURL = razorPay.getWalletLogo(havingWalletName: response.key)
    }
    
}
struct NetBank:CellDataProvider {
    var bankName:String
    var bankCode:String
    var bankLogo:URL?
    var isFirstCell: Bool = false
    var isLastCell: Bool = false
    init(razorPay:Razorpay,response:(key:String,value:String)) {
        self.bankName = response.value
        self.bankCode = response.key
        self.bankLogo = razorPay.getBankLogo(havingBankCode: response.key)
    }
}
struct SavedCard:CellDataProvider {
    var last4:String?
    var network:String?
    var token:String?
    var id:String?
    var cvv:String?
    var isSelected: Bool = false
    var amountToPaid:String?
    var isFirstCell: Bool = false
    var isLastCell: Bool = false
    
    init(response:[String:Any]) {
        if let card = response["card"] as? [String:Any] {
            last4 = card["last4"] as? String
            network = card["network"] as? String
            cvv = ""
            }
        token = response["token"] as? String
        id = response["id"] as? String
    }
}

struct SavedUPI:CellDataProvider {
    var upi:String?
    var token:String?
    var id:String?
    var handler:String?
    var isSelected: Bool = false
    var amountToPaid:String?
    var upiID: String?
    var isFirstCell: Bool = false
    var isLastCell: Bool = false
    
    init(response:[String:Any]) {
        if let card = response["vpa"] as? [String:Any] {
            upi = card["username"] as? String
            handler = card["handle"] as? String
            upiID = (upi ?? "") + "@" + (handler ?? "")
        }
        token = response["token"] as? String
        id = response["id"] as? String
    }
    
    func getUpiImage() -> UIImage? {
        if let handler = handler {
            if handler == "ybl" || handler == "ibl" || handler == "axl"{
                return UIImage(named: "phone_pe")
            }else if handler.starts(with: "ok"){
                return UIImage(named: "google_pay")
            }else if handler == "paytm"{
                return UIImage(named: "paytm_pay")
            }
        }
        return UIImage(named: "upi_logo")
    }
}

struct PaymentMethods {
    var wallet:[Wallet]?
    var netbank:[NetBank]?
    

    init(razorPay:Razorpay,response:[AnyHashable:Any]) {
        if let wallets = response["wallet"] as? [String:Int] {
            var tempArray = [Wallet]()
            for i in wallets {
                let temp = Wallet.init(razorPay: razorPay,response: i);
                tempArray.append(temp)
            }
            self.wallet = tempArray;
        }
        if let wallets = response["netbanking"] as? [String:String] {
            var tempArray = [NetBank]()
            for i in wallets {
                let temp = NetBank.init(razorPay: razorPay,response: i);
                tempArray.append(temp)
            }
            self.netbank = tempArray;
        }
    }
}
