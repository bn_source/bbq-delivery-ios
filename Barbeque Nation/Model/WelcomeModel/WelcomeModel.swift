
/*
 *  Created by Nischitha on 23/09/19.
 *  Copyright © BARBEQUE NATION HOSPITALITY LIMITED 2019. All rights reserved.
 
 * CHANGE LOG
 * —------------------------------------------------------------------------------
 * ID          Bug ID      Author Name          Date                  Description
 * —------------------------------------------------------------------------------
 * 1            NA         Nischitha         23/09/19      Initial Version
 */

import Foundation

struct WelcomeModel:Codable {
    var timestamp:String?
    var welcomeData: [WelcomeData]?
    
    private enum WelcomeModelCodingKeys:String,CodingKey {
        case timestamp = "timestamp"
        case welcomeData = "welcomeData"
    }
}

struct WelcomeData:Codable {
    var welcomeText:String?
    var welcomeImage:String?
    var welcomeVideo:String?
    
    private enum WelcomeDataCodingKeys:String,CodingKey {
        case welcomeText = "welcome_text"
        case welcomeImage = "welcome_image"
        case welcomeVideo = "welcome_video"
    }
}

extension WelcomeData {
    init(from decoder:Decoder) throws {
        let WelcomeDataInfo = try decoder.container(keyedBy: WelcomeDataCodingKeys.self)
        welcomeText = try WelcomeDataInfo.decodeIfPresent(String.self, forKey: .welcomeText) ?? ""
        welcomeImage = try WelcomeDataInfo.decodeIfPresent(String.self, forKey: .welcomeImage) ?? ""
        welcomeVideo = try WelcomeDataInfo.decodeIfPresent(String.self, forKey: .welcomeVideo) ?? ""
    }
}

extension WelcomeModel {
    init(from decoder: Decoder) throws {
        let WelcomeModelInfo = try decoder.container(keyedBy: WelcomeModelCodingKeys.self)
        timestamp = try WelcomeModelInfo.decodeIfPresent(String.self, forKey: .timestamp)
        welcomeData = try WelcomeModelInfo.decodeIfPresent([WelcomeData].self, forKey: .welcomeData)
    }
}

