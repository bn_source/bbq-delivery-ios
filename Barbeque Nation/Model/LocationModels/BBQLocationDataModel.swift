//  BBQLocationDataModel.swift
/*
 *  Created by Abhijit Singh on 20/08/19.
 *  Copyright © BARBEQUE NATION HOSPITALITY LIMITED 2019. All rights reserved.
 *  Last modified 04/09/19
 
 * CHANGE LOG
 * —------------------------------------------------------------------------------
 * ID          Bug ID      Author Name          Date                  Description
 * —------------------------------------------------------------------------------
 *
 */
import Foundation


class  OutletData:NSObject {
    var outletId:Int?
    var outletName:String?
    
    
    init(outletId:Int,outletName:String)
    {
        self.outletId = outletId
        self.outletName = outletName
    }
}
class  CitiesData:NSObject {
    
    var cityId:Int?
    var cityName:String?
    
    init(cityId:Int,cityName:String)
    {
        self.cityId = cityId
        self.cityName = cityName
    }
}


struct LocationResponse:Codable {
    var near_by : [Branches]?
    var bcitydata: [Cities]?
    
    private enum LocationCodingKeys: String, CodingKey {
        case bcitydata = "data"
        case near_by = "near_by"

    }
}

struct  Cities:Codable{
    var city_name: String?
    var city_code: String?
    var cityWeightage:Int?
    var branches:[Branches]?
    
    private enum CitiuesCodingKeys: String, CodingKey {
        case city_name = "city_name"
        case city_code = "city_code"
        case cityWeightage = "weight"
        case  branches = "branches"
    }

  
    
  
}

extension Cities {
      init(from decoder: Decoder) throws {
        
        let citiesInfo = try decoder.container(keyedBy: CitiuesCodingKeys.self)
        city_name = try citiesInfo.decodeIfPresent(String.self, forKey: .city_name) ?? ""
        city_code = try citiesInfo.decodeIfPresent(String.self, forKey: .city_code) ?? ""
        cityWeightage = try citiesInfo.decodeIfPresent(Int.self, forKey: .cityWeightage) ?? 0
        
        branches = try citiesInfo.decodeIfPresent([Branches].self, forKey: .branches) ?? []
        
    }
}
struct Branches:Codable{
    var branch_id: String?
    var branch_name: String?
    var latitude: String?
    var longitude: String?
//    var city_code: String?
//    var weight: String?
//    var city_name:String?
    
    private enum BranchesCodingKeys: String, CodingKey {
        case branch_id = "branch_id"
        case branch_name = "branch_name"
        case latitude = "latitude"
        case longitude = "longitude"
//        case city_code = "city_code"
//        case weight = "weight"
//        case city_name = "city_name"
    }
    
    
}



extension Branches{
    
    init(from decoder: Decoder) throws {
        let branchesInfo = try decoder.container(keyedBy: BranchesCodingKeys.self)
        branch_id = try branchesInfo.decodeIfPresent(String.self, forKey: .branch_id) ?? ""
        branch_name = try branchesInfo.decodeIfPresent(String.self, forKey: .branch_name) ?? ""
        latitude = try branchesInfo.decodeIfPresent(String.self, forKey: .latitude) ?? ""
        longitude = try branchesInfo.decodeIfPresent(String.self, forKey: .longitude) ?? ""
//        city_code = try branchesInfo.decodeIfPresent(String.self, forKey: .city_code) ?? ""
//        weight = try branchesInfo.decodeIfPresent(String.self, forKey: .weight) ?? ""
//        city_name = try branchesInfo.decodeIfPresent(String.self, forKey: .city_name) ?? ""
    }
}
extension Cities{
//    @objc override func value(forKey key: String) -> Any? {
//        switch key {
//        case "city_name":
//            return city_name
//        case "city_code":
//            return city_code
//        default:
//            return nil
//        }
//    }
}
extension LocationResponse {
    init(from decoder: Decoder) throws {
        let locationInfo = try decoder.container(keyedBy: LocationCodingKeys.self)
        
        near_by = try locationInfo.decodeIfPresent([Branches].self, forKey: .near_by) ?? []
        bcitydata = try locationInfo.decodeIfPresent([Cities].self, forKey: .bcitydata) ?? []
    }
}
extension CitiesData {
    @objc override func value(forKey key: String) -> Any? {
        switch key {
        case "id":
            return cityId
        case "cityName":
            return cityName
        default:
            return nil
        }
    }
}




