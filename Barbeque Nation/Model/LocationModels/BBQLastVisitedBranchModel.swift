//
//  Created by Abhijit on 23/09/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQLastVisitedBranchModel.swift
//

import Foundation

struct BBQLastVisitedBranchModel:Codable{
    
    var status:String?
    var message:String?
    
    
    private enum LastVisitedCodingKeys:String,CodingKey{
        
        case status = "status"
        case message = "message"
        
    }
}

extension BBQLastVisitedBranchModel {
    init(from decoder:Decoder) throws{
        let lastVisited = try decoder.container(keyedBy: LastVisitedCodingKeys.self)
        status = try lastVisited.decode(String.self, forKey: .status)
        message =  try lastVisited.decode(String.self, forKey: .message)
    }
}

