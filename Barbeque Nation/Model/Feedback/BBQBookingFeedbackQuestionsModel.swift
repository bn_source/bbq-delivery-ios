//
 //  Created by Rabindra L on 03/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQBookingFeedbackQuestionsModel.swift
 //

import Foundation

struct BBQBookingFeedbackQuestionsModel: Codable {
    var qid:Int?
    var question:String?
    
    private enum BBQBookingFeedbackQuestionsModelKeys: String, CodingKey {
        case qid = "qid"
        case question = "question"
    }
}

extension BBQBookingFeedbackQuestionsModel {
    init(from decoder: Decoder) throws {
        let feedBackQuestions = try decoder.container(keyedBy: BBQBookingFeedbackQuestionsModelKeys.self)
        qid = try feedBackQuestions.decodeIfPresent(Int.self, forKey: .qid) ?? nil
        question = try feedBackQuestions.decodeIfPresent(String.self, forKey: .question) ?? nil
    }
}
