//
//  Created by Bhamidipati Kishore on 24/09/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQNotificationTokenModel.swift
//

import Foundation

struct BBQNotificationTokenModel: Codable {
    let message: String?
    private enum BBQNotificationTokenModelKey: String, CodingKey {
        case message = "message"
    }
}

extension BBQNotificationTokenModel {
    init(from decoder:Decoder) throws {
        let tokenModelInfo = try decoder.container(keyedBy: BBQNotificationTokenModelKey.self)
        message = try tokenModelInfo.decodeIfPresent(String.self, forKey: .message) ?? ""
    }
}

struct NotificationModel {
    var titleString:String
    var messageString:String
    var imageUrlString:String
    
    init(titleString:String, messageString:String, imageUrlString:String? = nil) {
        self.titleString = titleString
        self.messageString = messageString
        self.imageUrlString = imageUrlString ?? ""
    }
}
