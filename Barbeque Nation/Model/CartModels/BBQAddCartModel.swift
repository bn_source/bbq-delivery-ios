//
 //  Created by Chandan Singh on 08/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQAddCartModel.swift
 //

import Foundation

struct AddCartData: Decodable {
    var orderId: Int?
    var orderItems: [OrderItems]?
    
    private enum AddCartDataCodingKeys: String, CodingKey {
        case orderId = "order_id"
        case orderItems = "order_items"
    }
}

extension AddCartData {
    init(from decoder: Decoder) throws {
        let cardDataInfo = try decoder.container(keyedBy: AddCartDataCodingKeys.self)
        orderId = try cardDataInfo.decodeIfPresent(Int.self, forKey: .orderId) ?? 0
        orderItems = try cardDataInfo.decodeIfPresent([OrderItems].self, forKey: .orderItems) ?? nil
    }
}

struct OrderItems: Codable {
    var orderItemId: Int?
    var productId: Int?
    var quantity: Int?
    
    private enum OrderItemsCodingKeys: String, CodingKey {
        case orderItemId = "order_item_id"
        case productId = "product_id"
        case quantity = "quantity"
    }
}

extension OrderItems {
    init(from decoder: Decoder) throws {
        let orderItemsInfo = try decoder.container(keyedBy: OrderItemsCodingKeys.self)
        orderItemId = try orderItemsInfo.decodeIfPresent(Int.self, forKey: .orderItemId) ?? 0
        productId = try orderItemsInfo.decodeIfPresent(Int.self, forKey: .productId) ?? 0
        quantity = try orderItemsInfo.decodeIfPresent(Int.self, forKey: .quantity) ?? 0
    }
}
