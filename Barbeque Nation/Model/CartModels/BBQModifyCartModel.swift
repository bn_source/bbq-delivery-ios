//
 //  Created by Chandan Singh on 09/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQGetAndPatchCartModel.swift
 //

import Foundation

struct ModifyCartModel: Decodable {
    var orderId: Int?
    var totalPrice: Double?
    var countryCode: String?
    var orderItems: [ModifyOrderItems]?
    var taxes: [CardTax]?
    
    private enum ModifyCartModelCodingKeys: String, CodingKey {
        case orderId = "order_id"
        case totalPrice = "total_price"
        case countryCode = "currency_code"
        case orderItems = "order_items"
        case taxes = "taxes"
    }
    
    func getTotalQuantity() -> Int {
        var Qty = 0
        for item in orderItems ?? [ModifyOrderItems](){
            Qty += item.quantity ?? 0
        }
        return Qty
    }
}

extension ModifyCartModel {
    init(from decoder: Decoder) throws {
        let getCartModelInfo = try decoder.container(keyedBy: ModifyCartModelCodingKeys.self)
        orderId = try getCartModelInfo.decodeIfPresent(Int.self, forKey: .orderId) ?? 0
        totalPrice = try getCartModelInfo.decodeIfPresent(Double.self, forKey: .totalPrice) ?? 0.0
        countryCode = try getCartModelInfo.decodeIfPresent(String.self, forKey: .countryCode) ?? ""
        orderItems = try getCartModelInfo.decodeIfPresent([ModifyOrderItems].self, forKey: .orderItems) ?? nil
        taxes = try getCartModelInfo.decodeIfPresent([CardTax].self, forKey: .taxes) ?? nil
    }
}

struct ModifyOrderItems: Decodable {
    var orderItemId: Int?
    var productId: Int?
    var itemImagePath: String?
    var itemTitle: String?
    var quantity: Int?
    var unitPrice: Double?
    var totalPrice: Double?
    
    private enum ModifyOrderItemsCodingKeys: String, CodingKey {
        case orderItemId = "order_item_id"
        case productId = "product_id"
        case itemImagePath = "image"
        case itemTitle = "title"
        case quantity = "quantity"
        case unitPrice = "unit_price"
        case totalPrice = "total_price"
    }
}

struct CardTax:Codable {
    var tax:String?
    var taxPercentage:String?
    
    private enum CardTaxCodingKeys: String, CodingKey {
        case tax = "tax"
        case taxPercentage = "tax_percentage"
    }
}

extension ModifyOrderItems {
    init(from decoder: Decoder) throws {
        let cardDataInfo = try decoder.container(keyedBy: ModifyOrderItemsCodingKeys.self)
        orderItemId = try cardDataInfo.decodeIfPresent(Int.self, forKey: .orderItemId) ?? 0
        productId = try cardDataInfo.decodeIfPresent(Int.self, forKey: .productId) ?? 0
        itemImagePath = try cardDataInfo.decodeIfPresent(String.self, forKey: .itemImagePath) ?? ""
        itemTitle = try cardDataInfo.decodeIfPresent(String.self, forKey: .itemTitle) ?? ""
        quantity = try cardDataInfo.decodeIfPresent(Int.self, forKey: .quantity) ?? 0
        unitPrice = try cardDataInfo.decodeIfPresent(Double.self, forKey: .unitPrice) ?? 0.0
        totalPrice = try cardDataInfo.decodeIfPresent(Double.self, forKey: .totalPrice) ?? 0.0
    }
}

extension CardTax {
    init(from decoder: Decoder) throws {
        let cardDataInfo = try decoder.container(keyedBy: CardTaxCodingKeys.self)
        tax = try cardDataInfo.decodeIfPresent(String.self, forKey: .tax) ?? ""
        taxPercentage = try cardDataInfo.decodeIfPresent(String.self, forKey: .taxPercentage) ?? ""
    }
}
