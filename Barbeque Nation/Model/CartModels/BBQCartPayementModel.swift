//
 //  Created by Chandan Singh on 16/12/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQCartPayementModel.swift
 //

import Foundation

struct BBQCartPayementModel: Decodable {
    var orderId: String?
    var amount: Int?
    var currency: String?
    var key: String?
    
    private enum BBQCartPayementModelKeys: String, CodingKey {
        case orderId = "order_id"
        case amount = "amount"
        case currency = "currency"
        case key = "key"
    }
    
    init(from decoder: Decoder) throws {
        let paymentInfo = try decoder.container(keyedBy: BBQCartPayementModelKeys.self)
        orderId = try paymentInfo.decodeIfPresent(String.self, forKey: .orderId) ?? ""
        amount = try paymentInfo.decodeIfPresent(Int.self, forKey: .amount) ?? 0
        currency = try paymentInfo.decodeIfPresent(String.self, forKey: .currency) ?? ""
        key = try paymentInfo.decodeIfPresent(String.self, forKey: .key) ?? ""
    }
}
