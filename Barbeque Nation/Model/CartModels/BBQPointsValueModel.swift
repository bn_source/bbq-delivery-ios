//
 //  Created by Chandan Singh on 12/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQPointsValueModel.swift
 //

import Foundation

struct BBQPointsCountModel: Codable {
    var remainingPoints: Int?
    
    private enum BBQPointsCountModelKeys: String, CodingKey {
        case remainingPoints = "loyalty_points"
    }
}
extension BBQPointsCountModel {
    init(from decoder: Decoder) throws {
        let pointsCountInfo = try decoder.container(keyedBy: BBQPointsCountModelKeys.self)
        remainingPoints = try pointsCountInfo.decodeIfPresent(Int.self, forKey: .remainingPoints) ?? 0
    }
}

struct BBQPointsValueModel: Codable {
    var availablePoints: Int?
    var redeemablePoints: Int?
    var redeemAmount: Double?
    var currency: String?
    
    private enum BBQPointsValueModelKeys: String, CodingKey {
        case availablePoints = "available_points"
        case redeemablePoints = "redeemable_points"
        case redeemAmount = "redeem_amount"
        case currency = "currency_code"
    }
    
    // Custom initializer
    
    init(redeemedAmount: Double? = nil, redeemedPoints: Int? = nil) {
        self.redeemAmount = redeemedAmount
        self.redeemablePoints = redeemedPoints
    }
    
}

extension BBQPointsValueModel {
    init(from decoder: Decoder) throws {
        let pointsValueInfo = try decoder.container(keyedBy: BBQPointsValueModelKeys.self)
        availablePoints = try pointsValueInfo.decodeIfPresent(Int.self, forKey: .availablePoints) ?? 0
        redeemablePoints = try pointsValueInfo.decodeIfPresent(Int.self, forKey: .redeemablePoints) ?? 0
        redeemAmount = try pointsValueInfo.decodeIfPresent(Double.self, forKey: .redeemAmount) ?? 0.00
        currency = try pointsValueInfo.decodeIfPresent(String.self, forKey: .currency) ?? ""
    }
}
