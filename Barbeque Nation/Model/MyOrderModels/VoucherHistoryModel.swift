/*
 *  Created by Nischitha on 16/09/19.
 *  Copyright © BARBEQUE NATION HOSPITALITY LIMITED 2019. All rights reserved.
 
 * CHANGE LOG
 * —------------------------------------------------------------------------------
 * ID          Bug ID      Author Name          Date                  Description
 * —------------------------------------------------------------------------------
 * 1            NA         Nischitha         16/09/19       Initial Version
 */

import Foundation

struct VoucherHistoryModel: Codable {
    var meassage : String?
    var messageType : String?
    var vouchers : [Vouchers]?
    var pageNumber: String?
    var pageSize :String?
    var totalElements:String?
    var termsAndConditions :[String]?
  
    private enum VoucherHistoryDataKeys: String, CodingKey {
        case meassage = "message"
        case messageType = "message_type"
        case vouchers = "vouchers"
        case pageNumber = "page_number"
        case pageSize = "page_size"
        case totalElements = "total_elements"
        case termsAndConditions = "terms_conditions"
        
    }
    
    init(from decoder:Decoder) throws {
        let VoucherHistoryModelInfo = try decoder.container(keyedBy: VoucherHistoryDataKeys.self)
        meassage = try VoucherHistoryModelInfo.decodeIfPresent(String.self, forKey: .meassage)
        messageType = try VoucherHistoryModelInfo.decodeIfPresent(String.self, forKey: .messageType)
        vouchers = try VoucherHistoryModelInfo.decodeIfPresent([Vouchers].self, forKey: .vouchers)
        pageNumber = try VoucherHistoryModelInfo.decodeIfPresent(String.self, forKey: .pageNumber)
        pageSize = try VoucherHistoryModelInfo.decodeIfPresent(String.self, forKey: .pageSize)
        totalElements = try VoucherHistoryModelInfo.decodeIfPresent(String.self, forKey: .totalElements)
        termsAndConditions = try VoucherHistoryModelInfo.decodeIfPresent([String].self, forKey: .termsAndConditions)

    }
}

struct Vouchers: Codable {
    
    var barCode : String?
    var denomination : Int?
    var giftReceived : String?
    var giftReceivedOn : String?
    var giftTo : String?
    var giftToOn : String?
    var giftType : String?
    var source : String?
    var status : String?
    var title : String?
    var validity : String?
    var voucherType : String?
    var currency : String?
    var purchaseDate :String?
    var voucherImage: String?
    var transactionId :String?
    var description: String?
    
    private enum VouchersDataKeys: String, CodingKey {
        case barCode = "bar_code"
        case denomination = "denomination"
        case giftReceived = "gift_received"
        case giftReceivedOn = "gift_received_on"
        case giftTo = "gift_to"
        case giftToOn = "gift_to_on"
        case giftType = "gift_type"
        case source = "source"
        case status = "status"
        case title = "title"
        case validity = "validity"
        case voucherType = "voucher_type"
        case currency = "currency"
        case purchaseDate = "issue_date"
        case voucherImage = "image"
        case transactionId = "order_id"
        case description = "description"
    }
    
    init(from decoder:Decoder) throws {
        let VouchersDataInfo = try decoder.container(keyedBy: VouchersDataKeys.self)
        barCode = try VouchersDataInfo.decodeIfPresent(String.self, forKey: .barCode)
        denomination = try VouchersDataInfo.decodeIfPresent(Int.self, forKey: .denomination)
        giftReceived = try VouchersDataInfo.decodeIfPresent(String.self, forKey: .giftReceived)
        giftReceivedOn = try VouchersDataInfo.decodeIfPresent(String.self, forKey: .giftReceivedOn)
        giftTo = try VouchersDataInfo.decodeIfPresent(String.self, forKey: .giftTo)
        giftToOn = try VouchersDataInfo.decodeIfPresent(String.self, forKey: .giftToOn)
        giftType = try VouchersDataInfo.decodeIfPresent(String.self, forKey: .giftType)
        source = try VouchersDataInfo.decodeIfPresent(String.self, forKey: .source)
        status = try VouchersDataInfo.decodeIfPresent(String.self, forKey: .status)
        title = try VouchersDataInfo.decodeIfPresent(String.self, forKey: .title)
        validity = try VouchersDataInfo.decodeIfPresent(String.self, forKey: .validity)
        voucherType = try VouchersDataInfo.decodeIfPresent(String.self, forKey: .voucherType)
        currency = try VouchersDataInfo.decodeIfPresent(String.self, forKey: .currency)
        purchaseDate = try VouchersDataInfo.decodeIfPresent(String.self, forKey: .purchaseDate)
        voucherImage = try VouchersDataInfo.decodeIfPresent(String.self, forKey: .voucherImage)
        transactionId = try VouchersDataInfo.decodeIfPresent(String.self, forKey: .transactionId)
        description = try VouchersDataInfo.decodeIfPresent(String.self, forKey: .description)
    }
    
    init(voucher: VouchersLists) {
        barCode = voucher.barCode
        denomination = voucher.denomination
        giftReceived = voucher.giftReceived
        giftReceivedOn = voucher.giftReceivedOn
        giftTo = voucher.giftTo
        giftToOn = voucher.giftToOn
        giftType = voucher.giftType
        source = voucher.source
        status = voucher.status
        title = voucher.title
        validity = voucher.validity
        voucherType = voucher.voucherType
        currency = voucher.currency
        purchaseDate = voucher.purchaseDate
        voucherImage = voucher.voucherImage
        transactionId = voucher.order_id
        description = voucher.description
    }
}


