/*
 *  Created by Nischitha on 06/09/19.
 *  Copyright © BARBEQUE NATION HOSPITALITY LIMITED 2019. All rights reserved.
 
 * CHANGE LOG
 * —------------------------------------------------------------------------------
 * ID          Bug ID      Author Name          Date                  Description
 * —------------------------------------------------------------------------------
 * 1            NA         Nischitha         06/09/19       Initial Version
 */

import Foundation

struct BookingHistoryModel: Codable {
    
    let bookingsHistory: [BookingHistory]?
    var pageNumber: Int?
    var pageSize: Int?
    var totalElements: Int?
   
    private enum BookingHistoryDataKeys: String, CodingKey {
        case bookingsHistory = "bookings"
        case pageNumber = "page_number"
        case pageSize = "page_size"
        case totalElements = "total_elements"
    }
    
    init(from decoder:Decoder) throws {
        let bookingHistoryModelInfo = try decoder.container(keyedBy: BookingHistoryDataKeys.self)
        bookingsHistory = try bookingHistoryModelInfo.decodeIfPresent([BookingHistory].self, forKey: .bookingsHistory)
        pageNumber = try bookingHistoryModelInfo.decodeIfPresent(Int.self, forKey: .pageNumber)
        pageSize = try bookingHistoryModelInfo.decodeIfPresent(Int.self, forKey: .pageSize)
        totalElements = try bookingHistoryModelInfo.decodeIfPresent(Int.self, forKey: .totalElements)
    }
}

struct AmenitiesDetails:Codable {
    
    var name: String?
    var description: String?
    var image: String?
    
    private enum AmenitiesCodingKeys: String, CodingKey {
        
        case name = "name"
        case description = "description"
        case image = "image"
    }
    
    init(from decoder:Decoder) throws {
        let amenitiesInfo = try decoder.container(keyedBy: AmenitiesCodingKeys.self)
        name = try amenitiesInfo.decodeIfPresent(String.self, forKey: .name) ?? ""
        description = try amenitiesInfo.decodeIfPresent(String.self, forKey: .description) ?? ""
        image = try amenitiesInfo.decodeIfPresent(String.self, forKey: .image) ?? ""
    }
}

struct BookingsDetails: Codable {
   
    var menuId: Int64?
    var buffetId: Int?
    var packs: Int?
    var name: String?
    var type: String?
    private enum BookingDetailsKeys: String, CodingKey {
    
        case menuId = "menu_id"
        case buffetId = "buffet_id"
        case packs = "packs"
        case name = "buffet_name"
        case type = "buffet_type"
       
    }
    init(from decoder:Decoder) throws {
        let bookingDetailsInfo = try decoder.container(keyedBy: BookingDetailsKeys.self)
        menuId = try bookingDetailsInfo.decodeIfPresent(Int64.self, forKey: .menuId) ?? Int64(0.0)
        buffetId = try bookingDetailsInfo.decodeIfPresent(Int.self, forKey: .buffetId) ?? 0
        packs = try bookingDetailsInfo.decodeIfPresent(Int.self, forKey: .packs) ?? 0
        name = try bookingDetailsInfo.decodeIfPresent(String.self, forKey: .name) ?? ""
        type = try bookingDetailsInfo.decodeIfPresent(String.self, forKey: .type) ?? ""
    }
}

struct CouponsAndVouchers: Codable {
    var title: String?
    var voucherCode: String?
    var voucherType: String?
    var amount: Double?
    var isCorporateVoucher: Int?
    var pax_applicable: String = "0"
    
    private enum VoucherDetailsKeys: String, CodingKey {
        case title = "title"
        case voucherCode = "voucher_code"
        case voucherType = "voucher_type"
        case amount = "amount"
        case isCorporateVoucher = "corporate_voucher"
        case pax_applicable = "pax_applicable"
    }
    
    
    init(from decoder:Decoder) throws {
        let cardDetails = try decoder.container(keyedBy: VoucherDetailsKeys.self)
        title = try cardDetails.decodeIfPresent(String.self, forKey: .title) ?? ""
        voucherCode = try cardDetails.decodeIfPresent(String.self, forKey: .voucherCode) ?? ""
        voucherType = try cardDetails.decodeIfPresent(String.self, forKey: .voucherType) ?? ""
        amount = try cardDetails.decodeIfPresent(Double.self, forKey: .amount) ?? 0.00
        isCorporateVoucher = try cardDetails.decodeIfPresent(Int.self, forKey: .isCorporateVoucher) ?? 0
        if let pax = try? cardDetails.decodeIfPresent(Int.self, forKey: .pax_applicable){
            pax_applicable = "\(pax)"
        }else{
            pax_applicable = try cardDetails.decodeIfPresent(String.self, forKey: .pax_applicable) ?? "0"
        }
        
    }
}

struct BookingHistory: Codable,Hashable,Comparable {
    //MARK: - DEL-1624 Fix for duplicating and unsorted reservations.
    
    static func < (lhs: BookingHistory, rhs: BookingHistory) -> Bool {
        guard let lhsReservationDate = lhs.reservationDate,
              let rhsReservationDate = rhs.reservationDate
        else { return false }
        
        return lhsReservationDate > rhsReservationDate
    }
    
    static func == (lhs: BookingHistory, rhs: BookingHistory) -> Bool {
        lhs.bookingId == rhs.bookingId
    }
    
    func hash(into hasher: inout Hasher)
    {
        hasher.combine(bookingId)
    }
    
    //END
    
    var bookingId: String?
    var bookingStatus: String?
    var branchName: String?
    var branchAddress: String?
    var branchContactNumber: String?
    var branchImage: String?
    var latitude: Double?
    var longitude: Double?
    var amenities: [AmenitiesDetails]?
    var bookingDetails: [BookingsDetails]?
    var couponsAndVouchers: [CouponsAndVouchers]?
    var billTotal: Double?
    var netPayable: Double?
    var showPayment: Bool?
    var bbqPoints: Int64?
    var reservationDate :String?
    var reservationTime :String?
    var occationDescription: String?
    var bbqnPointsRedeemed :Int?
    var currency :String?
    var outletUrl:String?
    var loyaltyPointsRefund:String?
    var advanceAmount:Double?
    var billLink :String?
    var payment:Payments?
    var opened : Bool = false
    var cellExpand :Bool?
    var isPaymentIntiated: Bool = false
    var billWithoutTax: Double = 0.0
    let taxes: [Taxes]?
    
    
    
    private enum BookingHistoryCodingKeys: String, CodingKey {
        case bookingId = "booking_id"
        case bookingStatus = "booking_status"
        case branchName = "branch_name"
        case branchAddress = "branch_address"
        case branchContactNumber = "branch_contact_no"
        case branchImage = "branch_image"
        case latitude = "lat"
        case longitude = "long"
        case amenities = "amenities"
        case bookingDetails = "booking_details"
        case couponsAndVouchers = "vouchers"
        case billTotal = "bill_total"
        case netPayable = "net_payable"
        case reservationDate = "reservation_date"
        case reservationTime = "reservation_time"
        case showPayment = "show_payment"
        case bbqPoints = "bbq_points"
        case occationDescription = "occasion_description"
        case bbqnPointsRedeemed = "loyalty_points"
        case currency = "currency"
        case outletUrl = "location_url"
        case loyaltyPointsRefund = "loyalty_points_refund"
        case advanceAmount = "advance_amount"
        case billLink = "bill_link"
        case payment = "payments"
        case bill_without_tax = "bill_without_tax"
        case taxes = "taxes"
    }
    init(from decoder:Decoder) throws {
        let bookingHistoryInfo = try decoder.container(keyedBy: BookingHistoryCodingKeys.self)
        bookingId = try bookingHistoryInfo.decodeIfPresent(String.self, forKey: .bookingId) ?? ""
        bookingStatus = try bookingHistoryInfo.decodeIfPresent(String.self, forKey: .bookingStatus) ?? ""
        branchName = try bookingHistoryInfo.decodeIfPresent(String.self, forKey: .branchName) ?? ""
        branchAddress = try bookingHistoryInfo.decodeIfPresent(String.self, forKey: .branchAddress) ?? ""
        branchContactNumber = try bookingHistoryInfo.decodeIfPresent(String.self, forKey: .branchContactNumber) ?? ""
        branchImage = try bookingHistoryInfo.decodeIfPresent(String.self, forKey: .branchImage) ?? ""
        latitude = try bookingHistoryInfo.decodeIfPresent(Double.self, forKey: .latitude) ?? 0.0
        longitude = try bookingHistoryInfo.decodeIfPresent(Double.self, forKey: .longitude) ?? 0.0
        amenities = try bookingHistoryInfo.decodeIfPresent([AmenitiesDetails].self, forKey: .amenities)
        bookingDetails = try bookingHistoryInfo.decodeIfPresent([BookingsDetails].self, forKey: .bookingDetails)
        couponsAndVouchers = try bookingHistoryInfo.decodeIfPresent([CouponsAndVouchers].self, forKey: .couponsAndVouchers)
        billTotal = try bookingHistoryInfo.decodeIfPresent(Double.self, forKey: .billTotal) ?? 0.0
        netPayable = try bookingHistoryInfo.decodeIfPresent(Double.self, forKey: .netPayable) ?? nil
        billWithoutTax = try bookingHistoryInfo.decodeIfPresent(Double.self, forKey: .bill_without_tax) ?? 0.0
        taxes = try bookingHistoryInfo.decodeIfPresent([Taxes].self, forKey: .taxes) ?? nil
        showPayment = try bookingHistoryInfo.decodeIfPresent(Bool.self, forKey: .showPayment)
        bbqPoints = try bookingHistoryInfo.decodeIfPresent(Int64.self, forKey: .bbqPoints) ?? 0
        reservationDate = try bookingHistoryInfo.decodeIfPresent(String.self, forKey: .reservationDate) ?? ""
        reservationTime = try bookingHistoryInfo.decodeIfPresent(String.self, forKey: .reservationTime) ?? ""
        occationDescription = try bookingHistoryInfo.decodeIfPresent(String.self, forKey: .occationDescription) ?? ""
        bbqnPointsRedeemed = try bookingHistoryInfo.decodeIfPresent(Int.self, forKey: .bbqnPointsRedeemed) ?? 0
        currency = try bookingHistoryInfo.decodeIfPresent(String.self, forKey: .currency) ?? ""
        outletUrl = try bookingHistoryInfo.decodeIfPresent(String.self, forKey: .outletUrl) ?? ""
        loyaltyPointsRefund = try bookingHistoryInfo.decodeIfPresent(String.self, forKey: .loyaltyPointsRefund) ?? ""
        advanceAmount = try bookingHistoryInfo.decodeIfPresent(Double.self, forKey: .advanceAmount) ?? 0.0
        billLink = try bookingHistoryInfo.decodeIfPresent(String.self, forKey: .billLink) ?? ""
        payment = try bookingHistoryInfo.decodeIfPresent(Payments.self, forKey: .payment)
    }
    
    func getTotalAmount() -> Double {
        var amount = billWithoutTax
        if let taxes = taxes {
            for tax in taxes {
                amount = amount + (tax.taxAmount ?? 0.0)
            }
        }
        return amount
    }
    
    func isBookingSettled() -> Bool{
        if bookingStatus == "PRE_RECEIPT"{
            if let netPayable = netPayable, let amountPaid = payment?.amountPaid, Int(netPayable) <= Int(amountPaid){
                return true
            }
        }
        return false
    }
    
    func getStatus() -> (Int, String){
//        if booking.bookingStatus == "CONFIRMED" || booking.bookingStatus == "EXPECTED" || booking.bookingStatus == "Rescheduled" || booking.bookingStatus == "UPDATE RESERVATION"{
//            return ("UPCOMING", UIColor(red: 255.0/255.0, green: 136.0/255.0, blue: 0.0/255.0, alpha: 1.0))
//        }else if booking.bookingStatus == "ARRIVED" || booking.bookingStatus == "SEATED"{
//            return ("ON GOING", UIColor(red: 255.0/255.0, green: 136.0/255.0, blue: 0.0/255.0, alpha: 1.0))
//        }else if booking.bookingStatus == "PRE_RECEIPT"{
//            return ("PENDING", UIColor.red)
//        }else
        if bookingStatus == "SETTLED"{
            return (2, "Completed")
        }else if bookingStatus == "CANCELLED" || bookingStatus == "NO SHOW"{
            return (3, "Cancelled")
        }else{
            return (1, "On Going")
        }
    }
}

