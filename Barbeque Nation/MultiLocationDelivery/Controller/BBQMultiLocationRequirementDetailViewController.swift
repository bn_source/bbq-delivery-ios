//
 //  Created by Arpana on 25/04/23
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2023.
 //  All rights reserved.
 //  Last modified BBQMultiLocationRequirementDetailViewController.swift
 //

import UIKit

class BBQMultiLocationRequirementDetailViewController: BBQBaseViewController {
    
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblEmail : UILabel!
    @IBOutlet weak var lblPhone : UILabel!
    @IBOutlet weak var txtFieldOutletSelected: UITextField!
    @IBOutlet weak var txtFieldReservationNo : UITextField!
    var photoUrl = [URL]()
    @IBOutlet weak var txtFieldName: UITextField!
    @IBOutlet weak var txtFieldPhone: UITextField!
    @IBOutlet weak var txtFieldEmail: UITextField!
    lazy  var toolBar : CustomizedToolBar? =  CustomizedToolBar()
    @IBOutlet weak var btnChecTerms: UIButton!
    
    var selectedCity = ""
    @IBOutlet weak var txtFieldReasonDropdown: UITextField! {
        didSet {
            txtFieldReasonDropdown.tintColor = UIColor.lightGray
            if #available(iOS 13.0, *) {
                txtFieldReasonDropdown.setRightButton((UIImage(systemName: "arrowtriangle.down")! ))
            } else {
                    // Fallback on earlier versions
                }
        }
     }
    @IBOutlet weak var picker: UIPickerView!
    //  //////////////////All required outlets for data picker input///////////////////////////////////////////////
    @IBOutlet weak var pickerUIView: UIView!
    @IBOutlet weak var pickerBackgroundView : UIView!
    
    var pickerData: [FeedBackQuestionMaster]?
    var feedbackModelResponse : SideMenuFeedBackRequestModel?
    var feedbackReasonListResponse : FeedbackReasonListModel?

    @IBOutlet weak var txtViewMessage: UITextView!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var viewSubmitBtn: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var vewEditDetails: UIView!
    @IBOutlet weak var viewPreFilledDetails: UIView!
  //  @IBOutlet weak var tbleViewFeedbackType: UITableView!
   // @IBOutlet weak var tblViewReasonHeight : NSLayoutConstraint!
    @IBOutlet weak var deliveryAndDineInSegmentControl: UISegmentedControl!
    var activeTextField: UITextField = UITextField()
    var deliveryDineInValue = 1
    
    @IBOutlet weak var viewTableList : UIView!
    var selectedReason : String?
    var isTermAccepted = false
    //for toggle button selection/unselection
    var unSelectedBorderColor =  UIColor(red: 206/255, green: 207/255, blue: 219/255, alpha:1 )
    var unSelectedTextColor =  UIColor(red: 90/255, green: 102/255, blue: 121/255, alpha:1 )
    var selectedBackgroundColor = UIColor.hexStringToUIColor(hex: "FB4A1E")
    
    var isFeedBackBySelf = true
    var itemProviders : [NSItemProvider] = []
    var iterator  : IndexingIterator<[NSItemProvider]>?
    
        lazy var homeViewModel : HomeViewModel = {
            let homeModel = HomeViewModel()
            return homeModel
        }()
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getreasonList()
        // Connect data:
        self.picker.delegate = self
        self.picker.dataSource = self
        pickerBackgroundView.isHidden = true
        setToolBarForPicker()
        btnSubmit.isEnabled = false
        btnSubmit.alpha = 0.5

        
    }
    
    
    override func viewWillAppear(_ animated : Bool){
        super.viewWillAppear(animated)
         hideNavigationBar(true, animated: true)

        NotificationCenter.default.addObserver(self, selector: #selector(locationDataUpdatedForFeedbackPurpose(_:)), name: Notification.Name(rawValue:Constants.NotificationName.locationDataUpdatedForFeedbackPurpose), object:nil)
        self.setUI()
        setData()
        
    }
    
    
    func getreasonList() {
              
        UIUtils.showLoader()
        btnSubmit.isEnabled = false
        btnSubmit.isUserInteractionEnabled = false
        BBQServiceManager.getInstance().feedbackReasonListFromSideMenuForBulkOrder() { [self]
            (error, response) in
            UIUtils.hideLoader()
             // stopShimmering()
            
            btnSubmit.isEnabled = true
            btnSubmit.isUserInteractionEnabled = true
            if let response = response {
                if response.message_type == "success" {
                    
                    AnalyticsHelper.shared.triggerEvent(type: .ML03A)
                    
                    feedbackReasonListResponse = response
                    if (feedbackReasonListResponse?.feedBackQuestionMaster != nil) && (feedbackReasonListResponse?.feedBackQuestionMaster?.count ?? 0 > 0 ){
                        pickerData = feedbackReasonListResponse?.feedBackQuestionMaster ?? []
                        picker.reloadAllComponents()
                        
                        if feedbackReasonListResponse?.feedBackQuestionMaster?.count ?? 0 > 0 {
                           // txtFieldReasonDropdown.text = feedbackReasonListResponse?.feedBackQuestionMaster?.first?.feed_back_question ?? ""
                            txtFieldReasonDropdown.accessibilityLabel = String(feedbackReasonListResponse?.feedBackQuestionMaster?.first?.feed_back_question_id ?? 0)
                            txtFieldReasonDropdown.text  = String(feedbackReasonListResponse?.feedBackQuestionMaster?.first?.feed_back_type_id ?? 0)
                        }

                    }
                } else {
                    //AnalyticsHelper.shared.triggerEvent(type: .FB02B)
                }
            }
            view.layoutIfNeeded()
        }
  
    }


    
    func setToolBarForPicker()   {
        self.navigationController!.isNavigationBarHidden = false;
        // ToolBar
        // add toolbar for picker view
        toolBar?.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44.0)
        //below two textfield will have pickerview as input
        txtFieldReasonDropdown.inputAccessoryView = toolBar
        txtFieldReasonDropdown.inputView = self.pickerUIView
         toolBar?.toolBarDelegate = self
        toolBar?.setTitle(title: "")
        
        if let pickerRetrieved =   self.picker  {
            pickerRetrieved.backgroundColor = UIColor.white
        }

        
    }
    
    @IBAction func checkTermsAndConditions(_ sender : UIButton){
        
        sender.isSelected = sender.isSelected == true ? false : true
        isTermAccepted = sender.isSelected
        if sender.isSelected == true {
            if #available(iOS 13.0, *) {
                btnChecTerms.setImage(UIImage(systemName: "checkmark.square.fill"), for: .normal)
            } else {
                // Fallback on earlier versions
            }
            
        }else {
            if #available(iOS 13.0, *) {
                btnChecTerms.setImage(UIImage(systemName: "square"), for: .normal)
            } else {
                // Fallback on earlier versions
            }
            
        }
        if (enableSubmitBtn()){
            btnSubmit.isEnabled = true
            btnSubmit.alpha = 1.0
        }else
            {
            btnSubmit.isEnabled = false
            btnSubmit.alpha = 0.5

        }
        
    }
       
    
    func setUI(){
        
        self.navigationView.layer.masksToBounds = false
        self.navigationView.layer.shadowOpacity = 1
        self.navigationView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.02).cgColor
        self.navigationView.layer.shadowOffset = CGSize(width: 0 , height: 2)
        self.navigationView.layer.shadowPath = UIBezierPath(roundedRect:  self.navigationView.bounds, cornerRadius: 0).cgPath
        self.navigationView.layer.shadowRadius = 4
        
        
        self.viewSubmitBtn.layer.masksToBounds = false
        self.viewSubmitBtn.layer.shadowOpacity = 1
        self.viewSubmitBtn.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        self.viewSubmitBtn.layer.shadowOffset = CGSize(width: 0 , height: 2)
        self.viewSubmitBtn.layer.shadowPath = UIBezierPath(roundedRect:  self.navigationView.bounds, cornerRadius: 0).cgPath
        self.viewSubmitBtn.layer.shadowRadius = 4
        self.btnSubmit.backgroundColor = .deliveryThemeColor
        self.btnEdit.tintColor = .deliveryThemeColor
        self.btnEdit.setTitleColor(.deliveryThemeColor, for: .normal)
        self.btnChecTerms.tintColor = .deliveryThemeColor
        //tbleViewFeedbackType.addObserver(self, forKeyPath: "contentSize" , options: .new, context: nil)
        deliveryAndDineInSegmentControl.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.appThemeMediumWith(size: 12)], for: .normal)
        deliveryAndDineInSegmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.lightGray], for: .disabled)
        // selected option color
        deliveryAndDineInSegmentControl.setTitleTextAttributes([.foregroundColor: UIColor.white], for: .selected)
        deliveryAndDineInSegmentControl.selectedSegmentIndex = 0
        
        deliveryAndDineInSegmentControl.backgroundColor = UIColor.hexStringToUIColor(hex: "FFFBF5")
        
        deliveryAndDineInSegmentControl.tintColor =  UIColor.hexStringToUIColor(hex: "FFFBF5")
        
        deliveryAndDineInSegmentControl.setTitleTextAttributes([.foregroundColor: UIColor.hexStringToUIColor(hex: "303030")], for: .normal)

        if #available(iOS 13.0, *) {
            deliveryAndDineInSegmentControl.selectedSegmentTintColor = selectedBackgroundColor
        } else {
            deliveryAndDineInSegmentControl.tintColor = selectedBackgroundColor
        }
        
        
        let color = UIColor(red: 186/255, green: 186/255, blue: 186/255, alpha: 0.8).cgColor
        txtViewMessage.layer.borderColor = color
        txtViewMessage.layer.borderWidth = 0.5
        txtViewMessage.layer.cornerRadius = 5
    }
    
    func setData(){
        
        lblName.text = BBQUserDefaults.sharedInstance.UserName
        lblEmail.text = SharedProfileInfo.shared.profileData?.email
        lblPhone.text = SharedProfileInfo.shared.profileData?.mobileNumber
        
    }
    
    
    override func viewWillDisappear(_ animated : Bool){
        
      //  tbleViewFeedbackType.removeObserver(self, forKeyPath: "contentSize")

    }
    //MARK : - locationDataUpdated called when location changes
    @objc func locationDataUpdatedForFeedbackPurpose(_ notification: Notification) {
        if let dictionary = notification.userInfo as NSDictionary? {
            let branchName = dictionary["branchName"] as! String
            let cityName = dictionary["cityName"] as! String

            selectedCity = cityName
            txtFieldOutletSelected.text = branchName
            txtFieldOutletSelected.accessibilityLabel = dictionary["branchId"] as? String
            
        }
        if (enableSubmitBtn()){
            btnSubmit.isEnabled = true
            btnSubmit.alpha = 1.0
        }else
            {
            btnSubmit.isEnabled = false
            btnSubmit.alpha = 0.5

        }
    }
    

    
    
    @IBAction func valueChangedOnDeliveryAndDineInegmentControl(_ sender: Any) {
        
      //  viewTableList.isHidden = false

        //call reason list depending on delivery/dinein value
    }
   
    @IBAction func backButtonTapped(_ sender: Any) {
        
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func editDetailsClicked(_ sender: Any) {
        
        
        isFeedBackBySelf = false
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
            //Show entry text field for details
            self.vewEditDetails.isHidden = false
            self.viewPreFilledDetails.isHidden = true
        } completion: { (isCompleted) in
            //Show entry text field for details
            self.vewEditDetails.isHidden = false
            self.viewPreFilledDetails.isHidden = true
        }
        
        if lblName.text != "" {
            txtFieldName.text = lblName.text
        }
        if lblEmail.text != "" {
            txtFieldEmail.text = lblEmail.text
        }
        if lblPhone.text != "" {
            txtFieldPhone.text = lblPhone.text
        }
        
        if (enableSubmitBtn()){
            btnSubmit.isEnabled = true
            btnSubmit.alpha = 1.0
        }else
            {
            btnSubmit.isEnabled = false
            btnSubmit.alpha = 0.5

        }
    }
    
    func enableSubmitBtn() -> Bool {
        
        if isFeedBackBySelf == false {
           
            if txtFieldName.text == ""  || txtFieldName.text  == nil {
                
                 return false
            }
            if txtFieldPhone.text  == "" || txtFieldPhone.text  == nil {
                
                 return false
            }
            
            if txtFieldEmail.text  == ""  || txtFieldEmail.text  == nil {
                
                 return false
            }
            
        }
        if txtViewMessage.text == "" || txtViewMessage.text == nil  || txtViewMessage.text == "Write your message here" {
            
            return false
        }

        if (txtFieldReasonDropdown.accessibilityLabel == "")  || (txtFieldReasonDropdown.accessibilityLabel == nil) {
            
             return false
        }
      
        if isTermAccepted == false {
            
             return false
        }
       return true
    }
    
    func validateData() -> Bool {
        
        if isFeedBackBySelf == false {
           
            if txtFieldName.text == "" {
                
                UIUtils.showToast(message: "Please enter name")
                 return false
            }
            if txtFieldPhone.text  == "" {
                
                UIUtils.showToast(message: "Please enter phone no.")
                 return false
            }
            
            var validNumber = BBQPhoneNumberValidation.isValidNumber(number: txtFieldPhone.text ?? "", for: BBQUserDefaults.sharedInstance.phoneCode)
            
            if validNumber == true {
                
                validNumber =   txtFieldPhone.text?.count ?? 0 < 10 ?false :true
                
            }
            if validNumber == false {
               
                UIUtils.showToast(message: "Please enter valid mobile number")
                 return false
            }
            
            if txtFieldEmail.text  == ""  || txtFieldEmail.text  == nil {
                
                UIUtils.showToast(message: "Please enter email")
                 return false
            }
            
            if !(txtFieldEmail.text!.isValidEmailID()) {
                
                UIUtils.showToast(message: "Please enter valid email")
                 return false
            }
            
        }
        if txtViewMessage.text == "" || txtViewMessage.text == nil  || txtViewMessage.text == "Write your message here" {
            
            UIUtils.showToast(message: "Please enter your message")
            return false
        }
   
        if (txtFieldReasonDropdown.accessibilityLabel == "")  || (txtFieldReasonDropdown.accessibilityLabel == nil) {
            
            UIUtils.showToast(message: "Please select feedback subject")
             return false
        }
    

        if isTermAccepted == false {
            
            UIUtils.showToast(message: "Please authorize")
             return false
        }
        btnSubmit.isEnabled = true
        btnSubmit.alpha = 1.0
       return true
    }
    
    @IBAction func submitBtnClicked( _ sender :Any){
        
        if validateData() {
            
            self.feedbackModelResponse = SideMenuFeedBackRequestModel()
            
            self.feedbackModelResponse?.outlet =  BBQUserDefaults.sharedInstance.DeliveryCurrentSelectedLocation
            self.feedbackModelResponse?.branch_id = BBQUserDefaults.sharedInstance.DeliveryBranchIdValue 
            self.feedbackModelResponse?.feedback_message = txtViewMessage.text
            self.feedbackModelResponse?.feedback_type_id =  Int(txtFieldReasonDropdown.text ?? "0")
            self.feedbackModelResponse?.feedback_question_id =  Int(txtFieldReasonDropdown.accessibilityLabel ?? "0" )
            self.feedbackModelResponse?.delivery_date =  txtFieldReservationNo.text ?? ""
          //  self.feedbackModelResponse?.city = selectedCity
            if isFeedBackBySelf {
                
                
                if  SharedProfileInfo.shared.profileData?.email == "" || SharedProfileInfo.shared.profileData?.email == nil {
                    
                    UIUtils.showToast(message: "Please enter valid email by Click on Edit")
                    
                    txtFieldName.text = BBQUserDefaults.sharedInstance.UserName
                    txtFieldPhone.text = SharedProfileInfo.shared.profileData?.mobileNumber
                    txtFieldEmail.becomeFirstResponder()
                    editDetailsClicked(btnEdit)
                    return
                }
                
                self.feedbackModelResponse?.user_email = SharedProfileInfo.shared.profileData?.email //lblEmail.text
                self.feedbackModelResponse?.user_name = BBQUserDefaults.sharedInstance.UserName //lblName.text
                self.feedbackModelResponse?.user_phone = SharedProfileInfo.shared.profileData?.mobileNumber //lblPhone.text
            }else{
                self.feedbackModelResponse?.user_email = txtFieldEmail.text
                self.feedbackModelResponse?.user_name = txtFieldName.text
                self.feedbackModelResponse?.user_phone = txtFieldPhone.text
            }
            
            self.feedbackModelResponse?.user_auth_key =  self.isTermAccepted == true ?1 :0
            self.feedbackModelResponse?.images_url = []
            
            //   AnalyticsHelper.shared.triggerEvent(type: .FB03)
            
            let JSONString = feedbackModelResponse?.toJSONString(prettyPrint: true)
            
            guard let jsonToSend = jsonToDictionary(from: JSONString!) else { return  }
            
            UIUtils.showLoader()
            btnSubmit.isEnabled = false
            AnalyticsHelper.shared.triggerEvent(type: .ML01)
            
            BBQServiceManager.getInstance().submitFeedbackResponseFromSideMenu(params: jsonToSend) { (error, result) in
                DispatchQueue.main.async {
                    
                    UIUtils.hideLoader()
                    self.btnSubmit.isEnabled = true
                    var message = "Thank you for your query. We will contact you soon."
                    var title = "Sucess!"
                    if let response = result {
                        if response.message_type == "failed" {
                            AnalyticsHelper.shared.triggerEvent(type: .ML02B)
                            message = error?.message ?? message
                            title = "Failed!"
                        } else {
                            AnalyticsHelper.shared.triggerEvent(type: .ML02A)
                            
                        }
                        
                        PopupHandler.showSingleButtonsPopup(title: title, message: message, on: self, firstButtonTitle: "OK") {
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                    
                    else {
                        UIUtils.showToast(message: error?.message ?? UNEXPECTED_ERROR)
                    }
                }
            }
        }
    }
                            
    
    @IBAction func editDetailsCloseClicked(_ sender: Any) {
        
        if txtFieldName.text! != "" {
            lblName.text = txtFieldName.text
        }
        if txtFieldEmail.text! != "" {
            lblEmail.text = txtFieldEmail.text
        }
        if txtFieldPhone.text! != "" {
            lblPhone.text = txtFieldPhone.text
        }
        
        isFeedBackBySelf = true

        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
            //hide entry text field for details
            self.vewEditDetails.isHidden = true
            self.viewPreFilledDetails.isHidden = false
        } completion: { (isCompleted) in
            //Show entry text field for details
            self.vewEditDetails.isHidden = true
            self.viewPreFilledDetails.isHidden = false
        }
        if (enableSubmitBtn()){
            btnSubmit.isEnabled = true
            btnSubmit.alpha = 1.0
        }else
            {
            btnSubmit.isEnabled = false
            btnSubmit.alpha = 0.5

        }
  
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            view.endEditing(true)
        activeTextField.resignFirstResponder()

        if self.pickerBackgroundView.isHidden == false {
            self.pickerBackgroundView.isHidden = true
        }
        
        if (enableSubmitBtn()){
            btnSubmit.isEnabled = true
            btnSubmit.alpha = 1.0
        }else
            {
            btnSubmit.isEnabled = false
            btnSubmit.alpha = 0.5

        }
    }
    
}


extension BBQMultiLocationRequirementDetailViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        activeTextField = textField
        if textField == txtFieldOutletSelected {
            txtFieldOutletSelected.accessibilityLabel = ""
            txtFieldOutletSelected.resignFirstResponder()
            activeTextField.resignFirstResponder()
            self.view.endEditing(true)
            selectOutlet()
        }
        
        if textField == txtFieldReasonDropdown {
//            txtFieldReasonDropdown.resignFirstResponder()
//            view.endEditing(true)
            if let pickerview =   self.picker  {
                pickerview.removeFromSuperview()
            }
            txtFieldReasonDropdown.inputView = self.picker;
           
            txtFieldReasonDropdown.becomeFirstResponder()
            pickerTextTapped()
            
            if picker != nil {
            picker.reloadAllComponents()
            }

        }
        
        if textField == txtFieldReservationNo {
            textField.resignFirstResponder()
            showCalendarBottomSheet()
            
        }

    }
    
    // MARK: - showCalendarBottomSheet action
    private func showCalendarBottomSheet() {
        //Create instance for the controller that will be shown
        let viewController = BottomSheetCalendarViewController()
        viewController.delegate = self
        //Present your controller over current controller
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: self.view.frame.size.height - viewController.view.frame.size.height)
        let bottomSheet = BottomSheetController(configuration: configuration)
        bottomSheet.present(viewController, on: self)
    }
    
     func selectOutlet() {
        
         activeTextField.resignFirstResponder()
         txtFieldOutletSelected.resignFirstResponder()
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: self.view.frame.size.height - 250.0)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        let viewController = BBQLocationViewController()
         viewController.isFromFeedbackScreen = true
        viewController.location = BBQLocationManager.shared.currentLocationManager.location
        viewController.setupPresentingController(controller: bottomSheetController)
        bottomSheetController.present(viewController, on: self, viewHeight: UIScreen.main.bounds.height - 165)
        self.view.bringSubviewToFront(viewController.view)

    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
       
        if textField == txtFieldOutletSelected {
            
            activeTextField.resignFirstResponder()
            txtFieldOutletSelected.resignFirstResponder()
            view.endEditing(true)
        }
        if (enableSubmitBtn()){
            btnSubmit.isEnabled = true
            btnSubmit.alpha = 1.0
        }else
            {
            btnSubmit.isEnabled = false
            btnSubmit.alpha = 0.5

        }

        if isFeedBackBySelf == true && textField == txtFieldEmail {
            SharedProfileInfo.shared.profileData?.email =  txtFieldEmail.text
        }

    }
    
    //the below method will allow text field to jump to next textfield on press of enter
    //Since we have assigned tag for each textfield , so once return is call the textfield withh +1 tag will become forst responder
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        
        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            //tag 3 is for country, so need to dismiss keyboard
            nextResponder.becomeFirstResponder()
        }
        textField.resignFirstResponder()
        
        return true
    }
    

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        activeTextField.resignFirstResponder()
        txtFieldOutletSelected.resignFirstResponder()
        self.view.endEditing(true)
    }
    
    //this method is called aver time a character is typed
    
    func  textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if textField == txtFieldOutletSelected  || textField == txtFieldReasonDropdown {
            return false
        }
        
        if textField == txtFieldPhone {
            
            let textVal = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
                            
                if textVal.count >  10 {
                    
                    //unhide nextButton
                  return false
                    
                }
        }
        return true
    }
    
    
}


extension BBQMultiLocationRequirementDetailViewController : UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if (txtViewMessage.text == "Write your message here")
        {
            txtViewMessage.text = nil
            txtViewMessage.font = UIFont(name: Constants.App.BBQAppFont.ThemeRegular, size: 14.0)
        }
        
    }

    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if txtViewMessage.text.isEmpty
        {
            txtViewMessage.text = "Write your message here"
            txtViewMessage.font = UIFont(name: Constants.App.BBQAppFont.ThemeLight, size: 12.0)
        }
        txtViewMessage.resignFirstResponder()
        if (enableSubmitBtn()){
            btnSubmit.isEnabled = true
            btnSubmit.alpha = 1.0
        }else
            {
            btnSubmit.isEnabled = false
            btnSubmit.alpha = 0.5

        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let currentText = textView.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: text)
        return updatedText.count <= 100
    }
  
    
    
}

extension BBQMultiLocationRequirementDetailViewController: BottomSheetCalendarProtocol {
    // MARK: - updateDateFromCalendar action
    func updateDateFromCalendar(originalDate: String, formattedDate: String) {
        self.txtFieldReservationNo.text = formattedDate
    }
}

extension BBQMultiLocationRequirementDetailViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return  5//reasonList?.count ?? 0

    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        guard let btn = (tableView.cellForRow(at: indexPath) as! BBQFeedbackReasonTableViewCell).imageView else {
                  return
              }
        btn.image = UIImage(named: "deSelectedBrand")
    }
        
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        guard let reasonValue = reasonList?[safe: indexPath.row] else {
//
//            return}
        
        
        guard let imgView = (tableView.cellForRow(at: indexPath) as! BBQFeedbackReasonTableViewCell).imageView else {
                  return
              }
        imgView.image = UIImage(named: "SelectedBrand")
        
//        selectedReason = incompleteReasonModelDatum()
//        selectedReason = reasonValue
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BBQFeedbackReasonTableViewCell", for: indexPath) as? BBQFeedbackReasonTableViewCell
//        guard let reasonValue = reasonList?[safe: indexPath.row] else {
//           return UITableViewCell()
//        }
        
        cell?.selectionStyle = .none
        if indexPath.row == 2 {
            
            cell?.titleLabel.text = " Packaging was not good enough Packaging was not good enough"
        }
//            if reasonValue.isSelected  == true{
//            cell?.tickImageView.image = UIImage(named: "CircleSelected")
//        }else{
//            cell?.tickImageView.image = UIImage(named: "Circle")
//        }
//        cell?.titleLabel.text = reasonValue.name
        return cell!
    }
    

    func fixImageOrientation(_ image: UIImage)->UIImage {
        UIGraphicsBeginImageContext(image.size)
        image.draw(at: .zero)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage ?? image
    }

    
    func jsonToDictionary(from text: String) -> [String: Any]? {
        guard let data = text.data(using: .utf8) else { return nil }
        let anyResult = try? JSONSerialization.jsonObject(with: data, options: [])
        //print(anyResult)
        return anyResult as? [String: Any]
    }
}


extension BBQMultiLocationRequirementDetailViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    override func didReceiveMemoryWarning()  {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData?.count ?? 0
    }
    
    // The data to return fopr the row and component (column) that's being passed in
    //    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    //        return pickerData[row]
    //    }
    //
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        
        txtFieldReasonDropdown.text = pickerData?[row].feed_back_question
        txtFieldReasonDropdown.accessibilityLabel = String(pickerData?[row].feed_back_question_id ?? 0)
        deliveryDineInValue = pickerData?[row].feed_back_type_id ?? 1
       // txtFieldReasonDropdown.resignFirstResponder()
        
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        let label: UILabel
        
        
        if let view = view {
            label = view as! UILabel
        }
        else {
            label = UILabel(frame: CGRect(x: 0, y: 0, width: pickerView.frame.width, height: 400))
        }
        
        let title = pickerData?[row].feed_back_question ?? ""
        
        //label.font = UIFont (name: Constants.App.BBQAppFont.ThemeRegular,  size: 14)
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.hexStringToUIColor(hex: "303030"),
                          NSAttributedString.Key.font: UIFont(name: Constants.App.BBQAppFont.ThemeRegular, size: 14.0)!]
        label.attributedText = NSAttributedString(string: title, attributes: attributes)
        
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .center
        label.sizeToFit()
        return label
    }
    
}
    //MARK: The below two methods are called on click of picker done and cancel button
    extension BBQMultiLocationRequirementDetailViewController : ToolBarDoneAndCancelDelegate {
        
        // called when textfield has to take data from picker
        func pickerTextTapped() {
            
            self.txtFieldReasonDropdown.text = ""
            self.txtFieldReasonDropdown.accessibilityLabel = ""
            
            if picker  != nil{
                self.picker.isHidden = false
            }
            self.pickerBackgroundView.isHidden = false // just show the background screen to diable complete UI for pressing other textfield
            
            self.view.layoutIfNeeded()
        }
        
        func doneButtonPressed() {
            
            self.view.layoutIfNeeded()
            if txtFieldReasonDropdown.text == "" {
                
                txtFieldReasonDropdown.text = pickerData?[0].feed_back_question
                txtFieldReasonDropdown.accessibilityLabel = String(pickerData?[0].feed_back_question_id ?? 0)
                deliveryDineInValue = pickerData?[0].feed_back_type_id ?? 1
            }
            txtFieldReasonDropdown.resignFirstResponder()
            self.pickerBackgroundView.isHidden = true
        }
        
        func cancelButtonPressed() {
            
            activeTextField.resignFirstResponder()
            self.txtFieldReasonDropdown.text = ""
            self.txtFieldReasonDropdown.accessibilityLabel = ""
            self.pickerBackgroundView.isHidden = true
        }
    }

