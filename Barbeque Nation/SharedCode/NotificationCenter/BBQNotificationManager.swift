//
 //  Created by Ajith CP on 24/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQNotificationManager.swift
 //

import UIKit

import UserNotifications

/// Manager class for checking device configurations with User NI=otifications
/// Allows to open device settings for the app.
class BBQNotificationManager {
    
    // Blocking init other than singletone instance.
    private init(){}
    
    static let shared: BBQNotificationManager = {
        let instance = BBQNotificationManager()
        return instance
    }()
    
    // Public sigleton variable that gives active notification settings for the app.
    var isPushEnabledFromSettings = false {
        didSet {
            
        }
    }
    
    // This method should be called for opening device settings for the app.
    func openDeviceSettings(isDisabled: Bool?) {
        
        self.navigateUesrToSettings(withMessage: kNotificationSettingsDescription, isDisabled: isDisabled)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
        })
        
    }
    
    // Call this method to when ever changes in device settings are need to be fetched.
    // At the moment it is called while app coming to foreground.
    func listenUserNotificationSettings()  {
        
        self.isPushPermissionGiven { (permission) in
            self.isPushEnabledFromSettings = permission
        }
       
        NotificationCenter.default.addObserver(forName:UIApplication.didBecomeActiveNotification,
                                               object: nil,
                                               queue: .main)
        { [weak self] (notificaion) in
            guard let strongSelf = self  else {return }
            strongSelf.isPushPermissionGiven { (permission) in
                DispatchQueue.main.async {
                    strongSelf.isPushEnabledFromSettings = permission
                }
            }
        }
    }

    // MARK: Private Methods
    // Derives permission status from UNUserNotificationCenter
    func isPushPermissionGiven (permission:@escaping (Bool) -> ()) {
       
        let current = UNUserNotificationCenter.current()
        current.getNotificationSettings(completionHandler: {settings in
            switch settings.authorizationStatus {
            case .notDetermined:
                permission(false)
                break
                
            case .denied:
                permission(false)
                break
                
            case .authorized:
                permission(true)
                break
                
            case .provisional:
                permission(true)
                break
                
            default:
                permission(false)
            }
        })
    }
    
    // Navigation initiation for device settings.
    func navigateUesrToSettings (withTitle title:String = kNotificationSettingsTitle,
                                 withMessage message:String, isDisabled: Bool?) {
        if let isDisabled = isDisabled{
            if isDisabled{
                AnalyticsHelper.shared.triggerEvent(type: .P06)
            }else{
                AnalyticsHelper.shared.triggerEvent(type: .P05)
            }
        }
        
        if let rootController = UIApplication.shared.delegate?.window??.rootViewController {
            PopupHandler.showTwoButtonsPopup(title: title,
                                             message: message,
                                             on: rootController,
                                             firstButtonTitle: kCancelButton,
                                             firstAction: { },
                                             secondButtonTitle: kOpenSettings) {
                                                guard let _ = URL(string: UIApplication.openSettingsURLString) else {
                                                    return
                                                }
                if let isDisabled = isDisabled{
                    if isDisabled{
                        AnalyticsHelper.shared.triggerEvent(type: .P06A)
                    }else{
                        AnalyticsHelper.shared.triggerEvent(type: .P05A)
                    }
                }
                                                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!,
                                                                          completionHandler: { (success) in
                                                })
            }
        }
    }
}
