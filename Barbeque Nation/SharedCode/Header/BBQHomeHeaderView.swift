//
//  BBQHomeHeaderView.swift
//  Barbeque Nation
//
//  Created by Maya R on 24/07/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

protocol BBQHomeHeaderViewProtocol :class{
    func hamburgerClicked(actionSender:UIButton)
    func notificationClicked(actionSender:UIButton)
    func mapClicked(actionSender:UIButton)

}
class BBQHomeHeaderView: UIView {
    @IBOutlet var containerView: UIView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var pointsLabel: UILabel!

    let nibName = "BBQHomeHeaderView"
    weak var homeHeaderDelegate: BBQHomeHeaderViewProtocol? = nil



    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
        Bundle.main.loadNibNamed(nibName, owner: self, options: nil)
        addSubview(containerView!)
        containerView?.frame = self.bounds
        containerView?.autoresizingMask = .flexibleWidth
    }

    
    @IBAction func hamburgerButtonClicked(_ sender: UIButton) {
                guard let hamDelegate = self.homeHeaderDelegate else {
                    return
                }
                hamDelegate.hamburgerClicked(actionSender: sender)

    }
    @IBAction func notificationClicked(_ sender: UIButton) {
                guard let notfDelegate = self.homeHeaderDelegate else {
                    return
                }
                notfDelegate.notificationClicked(actionSender: sender)

    }
    @IBAction func locationCliked(_ sender: UIButton) {
                guard let mapDelegate = self.homeHeaderDelegate else {
                    return
                }
                mapDelegate.notificationClicked(actionSender: sender)

    }
 
    
  
   
    
}
