//
//  CustomImagePageControl.swift
//  test
//
//  Created by Maya R on 20/09/19.
//  Copyright © 2019 Maya R. All rights reserved.
//

import UIKit


class BBQCustomPageControl: UIStackView {
    
    @IBInspectable var currentPageImage: UIImage = UIImage(named: "icon_carousel_line")!
    @IBInspectable var pageImage: UIImage = UIImage(named: "icon_carousel_circle")!
    /**
     Sets how many page indicators will show
     */
    var numberOfPages = 3 {
        didSet {
            layoutIndicators()
        }
    }
    /**
     Sets which page indicator will be highlighted with the **currentPageImage**
     */
    var currentPage = 0 {
        didSet {
            setCurrentPageIndicator()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        axis = .horizontal
        distribution = .equalSpacing
        alignment = .center
        spacing = 5.0
        layoutIndicators()
    }
    
    private func layoutIndicators() {
        
        for i in 0..<numberOfPages {
            
            let imageView: UIImageView
            
            if i < arrangedSubviews.count {
                imageView = arrangedSubviews[i] as! UIImageView // reuse subview if possible
            } else {
                imageView = UIImageView()
                addArrangedSubview(imageView)
            }
            
            if i == currentPage {
                imageView.image = currentPageImage
            } else {
                imageView.image = pageImage
            }
            
        }
        
        // remove excess subviews if any
        let subviewCount = arrangedSubviews.count
        if numberOfPages < subviewCount {
            for _ in numberOfPages..<subviewCount {
                arrangedSubviews.last?.removeFromSuperview()
            }
        }
    }
    
    private func setCurrentPageIndicator() {
        
        for i in 0..<arrangedSubviews.count {
            
            let imageView = arrangedSubviews[i] as! UIImageView
            
            if i == currentPage {
                imageView.image = currentPageImage
            } else {
                imageView.image = pageImage
            }
        }
    }
}
