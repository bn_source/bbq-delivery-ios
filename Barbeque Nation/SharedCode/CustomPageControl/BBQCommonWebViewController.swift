//
 //  Created by Ajith CP on 04/12/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQCommonWebViewController.swift
 //

import UIKit
import WebKit
import Alamofire

// This class is not using for now. Use it later for webviewKits
class BBQCommonWebViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewForHeader: UIView!
    @IBOutlet weak var btnShare: UIButton!
    var URLSring: String = ""
    var strTitle: String?
    var data: Data?
    var orderID: String = ""
    var shareOption = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewForHeader.addShadow(offset: CGSize(width: 0, height: 2.0), opacity: 0.5, radius: 0, color: .darkGray)
        loadCommonWebview()
        self.btnShare.isHidden = !shareOption
    }
    
    private func loadCommonWebview() {
        lblTitle.text = strTitle
        if let url = URL(string: URLSring.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""){
            UIUtils.showLoader()
            Alamofire.request(url).responseData { (data) in
                UIUtils.hideLoader()
                if let data = data.result.value{
                    self.webView.load(data, mimeType: "application/pdf", characterEncodingName: "", baseURL: url)
                    self.data = data
                }else{
                    UIUtils.showToast(message: "Invoice not found.")
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    func loadCommonWebview(with URLSring: String, strTitle: String? = nil) {
        self.URLSring = URLSring
        self.strTitle = strTitle
    }
    
    @IBAction func onClickBtnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickBtnShare(_ sender: Any) {
        if let data = data{
            let activityViewController = UIActivityViewController(activityItems: ["BBQ-\(orderID).pdf", data], applicationActivities: nil)   // and present it
            present(activityViewController, animated: true) {() -> Void in }
        }
    }
    
}
extension BBQCommonWebViewController: WKUIDelegate{
    
}
