//
//  Created by Ajith CP on 29/09/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQSessionManager.swift
//

import UIKit
import Foundation

class BBQSessionManager: NSObject {
    
    // MARK:- API Calls
    
    class func refreshSessionTokens(completion: @escaping (_ isFetched: Bool)->()) {
        BBQLoginManager.refreshToken { (tokenModel, isFetched) in
            completion(isFetched)
        }
    }
    
    // MARK:- Class Methods
    
    class func isSessionExpired() -> Bool {
        let expiryDuration = BBQUserDefaults.sharedInstance.expiryDuration
        let accessTokenStartDate = BBQUserDefaults.sharedInstance.accessTokenDate
        
        let calendar = Calendar.current
        guard let expiryDate = calendar.date(byAdding: .second,
                                             value: Int(expiryDuration),
                                             to: accessTokenStartDate) else { return false}
        if expiryDate < Date() {
            return true
        }
        return false
    }
    
    class func isSessionNearToExpire() -> Bool {
        let currentDate = Date()
        if BBQUserDefaults.sharedInstance.expiryDuration != 0 {
            let expireDate = NSDate(timeIntervalSinceNow: TimeInterval(BBQUserDefaults.sharedInstance.expiryDuration))
            let diffInDays = Calendar.current.dateComponents([.day], from: currentDate, to: expireDate as Date).day
            if let days = diffInDays {
                if days <= 10 {
                    return true
                }
            }
        }
        return false
    }    
}
