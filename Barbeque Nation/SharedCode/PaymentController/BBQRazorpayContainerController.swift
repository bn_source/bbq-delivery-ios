//
 //  Created by Ajith CP on 30/08/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQRazorpayContainerController.swift
 //

import UIKit

import Razorpay
import WebKit

/// You can handle success or error events when a payment is completed by implementing onRazorpayPaymentError
/// and onRazorpayPaymentSuccess methods of this protocol
protocol BBQRazorpayControllerProtocol {
    
    /// Returns on failure of payment
    ///
    /// - Parameters:
    ///     - code: Possible values for failure code are:
    ///          0: Network error
    ///          1: Initialization failure / Unexpected behavior
    ///          2: Payment cancelled by user
    ///          3: Create order failed
    ///     - description: error description.
    func onRazorpayPaymentError(_ code: Int32, description str: String)
    
    
    /// Returns on success of payment
    ///
    /// - Parameters:
    ///     - payment_id: payment identifier that you can use later for capturing the payment.
    func onRazorpayPaymentSuccess(_ paymentData: BBQPaymentDataModel)
}

/// Class responsible for starting razorpay payment. Present this controller from were payment initaiates
/// Confirm BBQRazorpayControllerProtocol for receiving events on payment, will be dismissed automatically
/// on completion

class BBQRazorpayContainerController: UIViewController {
    
    private var razorpay: Razorpay!
    
    /// Delegate for recieving payment events.
    var payDelegate : BBQRazorpayControllerProtocol?
   
    /// Total amount proceeds for checkout
    private var totalAmount : String?
    
    /// Currency in which transaction is going on.
    private var currency : String?
    
    /// Title will set in header part of payment controller.
    private var titleString : String?
    
    /// Descriptin for payment will set in header part of payment controller.
    private var titleDescription : String?
    var webView: WKWebView!

    // MARK:- Life Cycle Methods

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.webView = WKWebView(frame: self.view.frame)

//        (razorPayAppKey as! String, andDelegateWithData: self)
    }
    
    // MARK:- Lazy Init Methods
    override func viewWillAppear(_ animated: Bool) {
        print("View will apear in bbqcontainer")
    }
    
    lazy private var paymentControllerViewModel : MakePaymentViewModel = {
        let paymentVM = MakePaymentViewModel()
        return paymentVM
    } ()
    
    // MARK:- Data Fetch Methods
    
    /// Method to initialize the payment controller
    ///
    /// - Parameters:
    ///     - amount: Amount by which transaction proceeds. If passes invalid amount create order API will fail,
    ///               onRazorpayPaymentError will be called then.
    ///     - currency: currency type for transaction.
    func createOrderForPayment(with amount: Double, currency: String, title: String,and description: String, bookingID: String?, loyalty_points: Int?)  {
        
        self.titleString = title
        self.titleDescription = description
        
        self.totalAmount = "\(amount)" //self.paymentControllerViewModel.formattedCheckoutAmount(amount: amount)
        self.currency = currency
        AnalyticsHelper.shared.triggerEvent(type: .booking_payment_status)
        self.paymentControllerViewModel.createPaymentOrder(with: self.totalAmount ?? "",
                                                           currency: currency,
                                                           bookingID: bookingID ?? "",
                                                           loyalty_points: loyalty_points) { (orderID) in
                                                            // Procceed only for valid order id
                                                            guard orderID.count > 0 else {
                                                                self.dismiss(animated: true, completion: {
                                                                    self.payDelegate?.onRazorpayPaymentError(3,
                                                                                                             description:kRazorpayCrateOrderFailedDescription)
                                                                })
                                                                return
                                                            }
                                                            let paymentInfo = self.getPaymentInformation(with: orderID)
                                                            self.openCheckoutPage(with: paymentInfo)
        }
    }
    
    func createOrderForCartPayment(with amount: Double, currency: String, title: String, and description: String, discount: String?, loyalty_points: String = "0", bar_code: String = "")  {
        
        self.titleString = title
        self.titleDescription = description
        
        self.totalAmount = "\(amount)"
        self.currency = currency
        
        var paymentDiscount = "0"
        if discount != "0" {
            paymentDiscount = discount ?? "0"
        }
        
        self.paymentControllerViewModel.createCartPaymentOrder(discount: paymentDiscount, loyalty_points: loyalty_points, bar_code: bar_code) { (orderID) in
            // Procceed only for valid order id
            guard orderID.count > 0 else {
                self.dismiss(animated: true, completion: {
                    self.payDelegate?.onRazorpayPaymentError(3, description: kRazorpayCrateOrderFailedDescription)
                })
                return
            }
            let paymentInfo = self.getPaymentInformation(with: orderID)
            self.openCheckoutPage(with: paymentInfo)
        }
    }
    
//    func openCheckoutPage(with paymentModel: BBQRazorPayRequestModel)  {
//        let options = self.paymentControllerViewModel.checkoutpageOptions(with: paymentModel)
//
//        self.razorpay.open(options, displayController: self)
//    }
    //MARK:- CustomUI
    func openCheckoutPage(with paymentModel: BBQRazorPayRequestModel)  {
        AnalyticsHelper.shared.triggerEvent(type: .pay_clicked)
        let options = self.paymentControllerViewModel.checkoutpageOptions(with: paymentModel)
        let vc = UIStoryboard.loadPaymentNav()
        vc.modalPresentationStyle = .fullScreen
        let paymenVc = vc.viewControllers.first as! BBQCustomPaymentViewController
        
        let webView = WKWebView.init(frame: self.view.frame)
        let razorPayAppKey = Bundle.main.object(forInfoDictionaryKey: Constants.Payment.razorpayPublicKey)
        self.razorpay = Razorpay.initWithKey(razorPayAppKey as! String, andDelegate: self, withPaymentWebView: webView)
        paymenVc.viewModel = CustomPaymentViewModel.init(razorPay: self.razorpay,razorpayOptions: options, delegate: self,webView: webView, response: nil)
        self.present(vc, animated: true, completion: nil);
    }
    
    // MARK:- Private Methods
    
    func getPaymentInformation(with orderID: String) -> BBQRazorPayRequestModel {
        let paymentModel = BBQRazorPayRequestModel(amount: self.totalAmount,
                                                   currency: self.currency,
                                                   description: self.titleDescription,
                                                   order_id: orderID,
                                                   image: UIImage(named: Constants.Payment.checkoutFormBBQIcon),
                                                   name: self.titleString,
                                                   prefill: [:],
                                                   theme: [
                                                    Constants.Payment.checkoutFormThemeCodeKey:
                                                        Constants.Payment.checkoutFormThemeCode])
        return paymentModel
    }
}


extension BBQRazorpayContainerController:  RazorpayPaymentCompletionProtocol {
    
    func onPaymentError(_ code: Int32, description str: String, andData response: [AnyHashable : Any]) {
        AnalyticsHelper.shared.triggerEvent(type: .booking_payment_failure_value)
        if self.presentedViewController != nil {
            self.presentedViewController?.dismiss(animated: true, completion: nil)
        }
        self.dismiss(animated: false, completion: {
            self.payDelegate?.onRazorpayPaymentError(code, description: str)
        })
    }
    
    func onPaymentSuccess(_ payment_id: String, andData response: [AnyHashable : Any]) {
        AnalyticsHelper.shared.triggerEvent(type: .booking_payment_success_value)
        let razorpayPaymentID = response["razorpay_payment_id"] as! String
        let razorpayOrderID = response["razorpay_order_id"] as! String
        let razorpaySignature = response["razorpay_signature"] as! String
      
        let paymentData = BBQPaymentDataModel(paymentID: razorpayPaymentID,
                                              razorpayOrderID: razorpayOrderID,
                                              razorpaySignature: razorpaySignature)
        
        if self.presentedViewController != nil {
            self.presentedViewController?.dismiss(animated: true, completion: nil)
        }
        
        self.dismiss(animated: false, completion: {
            self.payDelegate?.onRazorpayPaymentSuccess(paymentData)
        })
    }
    
    
}

typealias Razorpay = RazorpayCheckout
