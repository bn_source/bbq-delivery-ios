//
 //  Created by Chandan Singh on 11/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified UIFont+AppFont.swift
 //

import UIKit

extension UIFont {
    class func appThemeMediumWith(size: CGFloat) -> UIFont {
        return UIFont(name: Constants.App.BBQAppFont.ThemeMedium, size: size)!
    }
    
    class func appThemeBlackWith(size: CGFloat) -> UIFont {
        return UIFont(name: Constants.App.BBQAppFont.ThemeBlack, size: size)!
    }
    
    class func appThemeBlackItalicsWith(size: CGFloat) -> UIFont {
        return UIFont(name: Constants.App.BBQAppFont.ThemeBlackItalic, size: size)!
    }
    
    class func appThemeBoldWith(size: CGFloat) -> UIFont {
        return UIFont(name: Constants.App.BBQAppFont.ThemeBold, size: size)!
    }
    
    class func appThemeBoldItalicsWith(size: CGFloat) -> UIFont {
        return UIFont(name: Constants.App.BBQAppFont.ThemeBoldItalic, size: size)!
    }
    
    class func appThemeExtraBoldWith(size: CGFloat) -> UIFont {
        return UIFont(name: Constants.App.BBQAppFont.ThemeExtraBold, size: size)!
    }
    
    class func appThemeExtraBoldItalicsWith(size: CGFloat) -> UIFont {
        return UIFont(name: Constants.App.BBQAppFont.ThemeExtraBoldItalic, size: size)!
    }
    
    class func appThemeExtraLightWith(size: CGFloat) -> UIFont {
        return UIFont(name: Constants.App.BBQAppFont.ThemeExtraLight, size: size)!
    }
    
    class func appThemeExtraLightItalicsWith(size: CGFloat) -> UIFont {
        return UIFont(name: Constants.App.BBQAppFont.ThemeExtraLightItalic, size: size)!
    }
    
    class func appThemeItalicWith(size: CGFloat) -> UIFont {
        return UIFont(name: Constants.App.BBQAppFont.ThemeItalic, size: size)!
    }
    
    class func appThemeLightWith(size: CGFloat) -> UIFont {
        return UIFont(name: Constants.App.BBQAppFont.ThemeLight, size: size)!
    }
    
    class func appThemeLightItalicWith(size: CGFloat) -> UIFont {
        return UIFont(name: Constants.App.BBQAppFont.ThemeLightItalic, size: size)!
    }
    
    class func appThemeRegularWith(size: CGFloat) -> UIFont {
        return UIFont(name: Constants.App.BBQAppFont.ThemeRegular, size: size)!
    }
    
    class func appThemeSemiBoldWith(size: CGFloat) -> UIFont {
        return UIFont(name: Constants.App.BBQAppFont.ThemeSemiBold, size: size)!
    }
    
    class func appThemeSemiBoldItalicWith(size: CGFloat) -> UIFont {
        return UIFont(name: Constants.App.BBQAppFont.ThemeSemiBoldItalic, size: size)!
    }
    
}
