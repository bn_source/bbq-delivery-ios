//
 //  Created by Maya R on 29/08/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified String+Shortcuts.swift
 //

import Foundation
import UIKit

extension String {
    var nib:UINib {
        
        return UINib.init(nibName: self, bundle: Bundle.main)
    }
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    func isValidMobileNumber(countryCode: String, length: Int) -> Bool {
        if NSString(string: self).length > length &&
            countryCode == BBQUserDefaults.sharedInstance.phoneCode {
            return false
        }
        return true
    }
    
    var isvalidNumber:Bool {
        let invalidCharacters = CharacterSet(charactersIn: "0123456789").inverted
        return !(self.rangeOfCharacter(from: invalidCharacters, options: [], range: self.startIndex ..< self.endIndex) != nil) && self.count < 17
    }
    func isValidExpiry(length:Int) -> Bool {
        let stringlength = NSString(string: self).length
        if stringlength > length {
            return false
        }
        if(stringlength==1) {
            if let month = Int(self),month > 12 {
                return false
            }
        }
        
        if(stringlength == 5) {
            let yearStr = self.split(separator: "/").last ?? "00"
            let currentYear = Int(Date.init().string(format: "yy")) ?? 0
            if let year = Int(yearStr),year < currentYear {
                return false
            }
        }
        return true
    }
    func isValidCVV(length:Int) -> Bool {
        if self.isvalidNumber && self.count <= length {
            return true
        }
        return false
    }
    var isvalidName:Bool{
        let invalidCharacters = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ ").inverted
        return !(self.rangeOfCharacter(from: invalidCharacters, options: [], range: self.startIndex ..< self.endIndex) != nil) && self.count < 30
    }
    var isValidVPA:Bool {
        let emailRegEx = "([\\w.-]*[@][\\w]*)"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: self)

    }
    func isValidEmailID() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: self)
    }
    
    var htmlToAttributedString: NSAttributedString? {
        //Fixed by Sakir Sherasiya
        //Data converting issue
        
        if self == "" || self.count == 0 {
            return nil
        }
        
        guard let data = self.data(using: String.Encoding.unicode) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
        } catch {
            return nil
        }
    }
    
    var htmlToString: String {
        if let htmlString = htmlToAttributedString{
            return htmlString.string
        }
        return ""
    }
    
    func currencySymbol() -> String {
        switch self {
        case "USD":
            return "$"
        default:
            return "₹"
        }
    }
    
    func isPastTimeString() -> Bool {
        if self.isEmpty {
            return true // For empty etring time
        }
        
//        if dateFormattedTime().timeIntervalSinceNow > 0 {
//            return true
//        }
        
        switch getCurrentTime().compare(dateFormattedTime()) {
        case .orderedAscending:
            return false

        case .orderedDescending:
            return true

        case .orderedSame:
            return false
        }
    }
    
    func getTakeAwayString(format: String = "yyyy/MM/dd hh:mm a") -> (NSAttributedString?, String?)? {
        let string = self
        let date = string.toDate(format: "yyyy/MM/dd hh:mm a")
        if let date = date, date < Date(){
            return nil
        }
        var prefix = ""
        if string.isToday(format: "yyyy/MM/dd hh:mm a"){
            prefix = "Today, "
        }else if string.isTomorrow(format: "yyyy/MM/dd hh:mm a"){
            prefix = "Tomorrow, "
        }
        let nsAttributedString = NSMutableAttributedString(string: "Takeaway - ", attributes: [NSAttributedString.Key.font: UIFont.systemFontOfSize(size: 14.0, weight: .regular), NSAttributedString.Key.foregroundColor: UIColor(named: "TextColor") as Any])
        let subTitleString = NSAttributedString(string: "\(prefix)\(date?.string(format: "MMM dd 'at' hh:mm a") ?? "")", attributes: [NSAttributedString.Key.font: UIFont.systemFontOfSize(size: 14.0, weight: .bold), NSAttributedString.Key.foregroundColor: UIColor(named: "TextColor")!])
        nsAttributedString.append(subTitleString)
        return (nsAttributedString, subTitleString.string)
    }
    
    func getScheduleDeluveryString(isAfterPlaced: Bool, isNextLine: Bool, interval: TimeInterval, format: String = "yyyy/MM/dd hh:mm a", fontSize: CGFloat) -> (NSAttributedString?, String?) {
        let string = self
        let date = string.toDate(format: format)
        if let date = date, date < Date(){
            return (nil, nil)
        }
        var prefix = ""
        if string.isToday(format: "yyyy/MM/dd hh:mm a"){
            prefix = "Today"
        }else if string.isTomorrow(format: "yyyy/MM/dd hh:mm a"){
            prefix = "Tomorrow"
        }else{
            prefix = date?.string(format: "MMM dd") ?? ""
        }
        var strScheduleDelivery = "Schedule Delivery "
        if isAfterPlaced{
            strScheduleDelivery = "Scheduled Delivery on "
        }
        if isNextLine{
            strScheduleDelivery = "Scheduled Delivery on\n"
        }
        let nsAttributedString = NSMutableAttributedString(string: strScheduleDelivery, attributes: [NSAttributedString.Key.font: UIFont.appThemeRegularWith(size: fontSize), NSAttributedString.Key.foregroundColor: UIColor(named: "TextColor") as Any])
        
        let subTitleString = NSAttributedString(string: "\(prefix)(\(date?.string(format: "hh:mm a") ?? "") - \(date?.addingTimeInterval(interval).string(format: "hh:mm a") ?? ""))", attributes: [NSAttributedString.Key.font: UIFont.appThemeMediumWith(size: fontSize), NSAttributedString.Key.foregroundColor: UIColor(named: "TextColor")!])
        nsAttributedString.append(subTitleString)
        let strTimeSlot = String(format: "%@ - %@", date?.string(format: "hh:mm a") ?? "", date?.addingTimeInterval(interval).string(format: "hh:mm a") ?? "")
        return (nsAttributedString, strTimeSlot)
    }
    
    func isToday(format: String? = nil) -> Bool {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format ?? Constants.BBQBookingStatus.todaytomorrowDateTimeFormat
        if let date = dateFormatter.date(from: self) {
            return Calendar.current.isDateInToday(date)
        }
        return false
    }
    
    func isTomorrow(format: String? = nil) -> Bool {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format ?? Constants.BBQBookingStatus.todaytomorrowDateTimeFormat
        if let date = dateFormatter.date(from: self) {
            return Calendar.current.isDateInTomorrow(date)
        }
        return false
    }
    
    func toDate(format: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = .current
        guard let date = dateFormatter.date(from: self) else {
            return nil
        }
        return date
    }
    
    // MARK: Private Methods
    
    private func dateFormattedTime() -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        let date = dateFormatter.date(from: self)
        return date ?? Date()
    }
    
    private func getCurrentTime() -> Date {
        let currentHour = (Calendar.current.component(.hour, from: Date()))
        let currentMinute = (Calendar.current.component(.minute, from: Date()))
        let currentSecond = (Calendar.current.component(.second, from: Date()))
        
        let formattedCurrentTime = String(format: "%d:%d:%d", currentHour, currentMinute, currentSecond)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        let date = dateFormatter.date(from: formattedCurrentTime)
        return date ?? Date()
    }
    
    func convertDateFormater(toFormat: String, fromFormat: String) -> String?
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = toFormat
        if let date = dateFormatter.date(from: self){
            dateFormatter.dateFormat = fromFormat
            return  dateFormatter.string(from: date)
        }
        return nil
    }
    
    //Manual way of comparing the time. Currently not used. Use it if needed in the function isPastTimeString().
    //Currently conventional way of time comparision is used. If that is not working, we can use this function as "retutn self.compareHourAndMinute()".
    //It will return true if the time is in past and false otherwise.
    private func compareHourAndMinute() -> Bool {
        var isPastTime = false
        
        let currentHour = (Calendar.current.component(.hour, from: Date()))
        let currentMinute = (Calendar.current.component(.minute, from: Date()))
        
        let restaurentTimeArray = self.components(separatedBy: ":")
        if restaurentTimeArray.count > 0 { //Make sure array has value
            let restaurentTimeHour = Int(restaurentTimeArray[0])
            let restaurentTimeMinute = Int(restaurentTimeArray[1])
            
            if let restaurentHour = restaurentTimeHour { //Make sure String is properly converted to Int
                if currentHour < restaurentHour {//Future time
                    isPastTime = false
                } else if currentHour > restaurentHour {//Future time
                    isPastTime = true
                } else if currentHour == restaurentHour {//Hours are equal. Lets compare minutes now.
                    if let restaurentMinute = restaurentTimeMinute { //Make sure String is properly converted to Int
                        if currentMinute < restaurentMinute {//Future time
                            isPastTime = false
                        } else if currentMinute > restaurentMinute {//Past time
                            isPastTime = true
                        } else if currentMinute == restaurentMinute {//Hours and Minutes both are equal. Lets consider it past time.
                            isPastTime = true
                        }
                    }
                }
            }
        }
        
        return isPastTime
    }
    
    // MARK: Currency Formatting
    
    func toCurrencyFormat() -> String {
        if let intValue = Int(self){
            let numberFormatter = NumberFormatter()
            numberFormatter.locale = Locale.current
            numberFormatter.numberStyle = NumberFormatter.Style.decimal
            return numberFormatter.string(from: NSNumber(value: intValue)) ?? ""
        }
        return ""
    }
    
    var numeralsOnlyFromString: String {
        let pattern = UnicodeScalar("0")..."9"
        return String(unicodeScalars
                        .compactMap { pattern ~= $0 ? Character($0) : nil })
        
    }

    
}
// Print on console makes app slower, Please print only in case of debug
// In  release mode printing should be disabled
public func print(_ items: Any..., separator: String = " ", terminator: String = "\n") {
    #if DEBUG
    let output = items.map { "\($0)" }.joined(separator: separator)
    Swift.print(output, terminator: terminator)
    #endif
}
