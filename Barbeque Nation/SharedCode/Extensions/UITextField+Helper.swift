//
 //  Created by Ajith CP on 11/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified UITextField+Helper.swift
 //

import Foundation

import  UIKit

extension UITextField {
    func wholeString(with replacementString: String, in range: NSRange) -> NSString {
        
        let currentString : NSString = self.text! as NSString
        if Int(replacementString) != nil || replacementString == "" {
            let newString : NSString =
                currentString.replacingCharacters(in: range, with: replacementString) as NSString
            return newString
        }
        return ""
    }
}
