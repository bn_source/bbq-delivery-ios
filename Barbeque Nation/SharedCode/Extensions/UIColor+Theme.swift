//
//  UIColor+Theme.swift
//  BottomSheet
//
//  Created by Chandan Singh on 29/07/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

extension UIColor {
    
    // MARK: Private
    fileprivate static func rgba(_ r: CGFloat, _ g: CGFloat, _ b: CGFloat, _ a: CGFloat) -> UIColor {
        return UIColor(red: r/255, green: g/255, blue: b/255, alpha: a)
    }
    
    fileprivate static func rgb(_ r: CGFloat, _ g: CGFloat, _ b: CGFloat) -> UIColor {
        return rgba(r, g, b, 1.0)
    }
    
    // MARK: Bottom Sheet Colors
    static let addButtonBorderColor = UIColor.white
    
    static let menuOrangeColor = rgba(255.0, 99.0, 1.0, 1.0)
    static let menuCategoryButtonUnselectedBg = UIColor.white
    static let menuButtonUnselectedColor = rgba(112.0, 112.0, 112.0, 1.0)
    static let menuButtonSelectedColor = UIColor.black
    
    static let bottomSheetHandleColor = rgba(54.0, 64.0, 68.0, 1.0)
    static let promoOverlayColor = rgba(0.0, 0.0, 0.0, 0.6)
    static let paymentButtonOrangeColor = rgba(255.0, 99.0, 1.0, 0.8)
    
    static let profileDarkGreyColor = rgba(54.0, 64.0, 68.0, 1.0)
    
    static let alertTitleColor = rgba(58.0, 58.0, 58.0, 1.0)
    static let alertMessageColor = rgba(54.0, 64.0, 68.0, 1.0)
    static let alertButtonColor = rgba(54.0, 64.0, 68.0, 1.0)
    
    static let appliedPointsColor = rgba(247.0, 247.0, 247.0, 1.0)
    static let locationTextFieldBottonLineColor = rgba(54.0, 64.0, 68.0, 0.4)
    
    static let menuOverlayLeftColor = rgba(75.0, 75.0, 75.0, 0.67)
    static let menuOverlayRightColor = rgba(15.0, 15.0, 15.0, 1.0)
    
    static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
