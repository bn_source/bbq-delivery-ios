//
 //  Created by vinoth kumar on 09/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified UITableView+Extension.swift
 //

import Foundation
import UIKit
extension UITableView {
    func getCell<T:UITableViewCell>(cell:T.Type,indexPath:IndexPath) -> T {
          
          let string = String.init(describing: cell)
        let cell = self.dequeueReusableCell(withIdentifier: string, for: indexPath) as! T
          return cell
      }
}
extension UICollectionView {
    func registerCell(cell:AnyClass) {
        let string = String.init(describing: cell.self)
        self.register(string.nib, forCellWithReuseIdentifier: string)
    }
    func getCell<T:UICollectionViewCell>(cell:T.Type,indexPath:IndexPath) -> T {
        let string = String.init(describing: cell)
        let cell = self.dequeueReusableCell(withReuseIdentifier: string, for: indexPath) as! T
        return cell
    }
    func getCellSize<T:UICollectionViewCell>(cell:T.Type,section:CellSection) -> CGSize
    {
        
        let cell:T = .fromNib(nibName: String(describing: cell))

        let width = self.frame.width
        
        switch section.cellSize {
        case .cell:
            return cell.frame.size
        case .half:
            var size = cell.frame.size
            size.width =  width/2 - 4
            return size
            
        case .halfsqaure(let factor):
            let cellWidth = width/2 - 4
            let height = cellWidth * factor
            return CGSize.init(width: cellWidth, height: height)
            
        case .fullPro(let factor):
            let cellWidth = width
            let height = cellWidth * factor
            return CGSize.init(width: cellWidth, height: height)
            
        case .fillCell(let count):
            let cellWidth = (width / count) - 1
            let height = cell.frame.height
            return CGSize.init(width: cellWidth, height: height)
            
//        case .fillCellHeight(let count, let heightPad):
//            let cellWidth = (width / count) - 1
//            var height = cell.frame.height
//            switch heightPad {
//            case .collection(let pad):
//                height = self.frame.size.height - pad
//            case .factor(let factor):
//                height = cellWidth*factor
//            default:
//                break;
//            }
//            return CGSize.init(width: cellWidth, height: height)
            
        case .square(size:let size):
            return CGSize.init(width: size, height: size)
            
        case .single:
            return CGSize.init(width: self.frame.width, height: cell.frame.height)
            
        case .custom(let size):
            return size
//        case .text(let text, let font):
//            return CGSize.init(width: self.frame.width, height: text.height(font: font, width: self.frame.width))
        default:
            return cell.frame.size
        }
    }
}
