//
//  UIView+Features.swift
//  BottomSheet
//
//  Created by Chandan Singh on 25/07/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit
enum LINE_POSITION{
    case LINE_POSITION_TOP
    case LINE_POSITION_BOTTOM
}
struct CornerMask {
    static let topCornersMask: CACornerMask = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
    static let leftCornersMask: CACornerMask = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
    static let rightCornersMask: CACornerMask = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
    static let bottomCornersMask: CACornerMask = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
}

extension UIView {
    
    func maskByRoundingCorners(cornerRadius radius: CGFloat, maskedCorners cornerMask: CACornerMask) {
        self.clipsToBounds = true
        self.layer.cornerRadius = radius
        self.layer.maskedCorners = cornerMask
    }
    
    func roundCorners(cornerRadius radius: CGFloat, borderColor color: UIColor, borderWidth width: CGFloat) {
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
        self.layer.borderWidth = width
        self.layer.borderColor = color.cgColor
    }
    
    // MARK: - Shadow Effects
    
    func topLayerShadow(opacity: Float, shadowSize: CGFloat)  {
        let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,
                                                   y: -shadowSize / 2,
                                                   width: self.frame.size.width + shadowSize,
                                                   height: shadowSize))
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowOpacity = opacity
        self.layer.shadowPath = shadowPath.cgPath
    }
    
    // MARK: - addLine
    func addLine(position : LINE_POSITION, color: UIColor, width: Double) {
        let lineView = UIView()
        lineView.backgroundColor = color
        lineView.translatesAutoresizingMaskIntoConstraints = false // This is important!
        self.addSubview(lineView)
        
        let metrics = ["width" : NSNumber(value: width)]
        let views = ["lineView" : lineView]
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lineView]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
        
        switch position {
        case .LINE_POSITION_TOP:
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lineView(width)]", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        case .LINE_POSITION_BOTTOM:
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[lineView(width)]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        }
    }
    
    // Adding blurred effect for the view, controls the transperancy.
    func blurView(with opacity: CGFloat) {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.alpha = opacity
        blurEffectView.frame = self.bounds
        self.addSubview(blurEffectView)
    }
    
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.borderColor = UIColor.white.cgColor
        layer.borderWidth = 1.0
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.2
        layer.shadowOffset = .zero
        layer.shadowRadius = 1.0
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
  
    func imageViewShadow() {
        self.layer.masksToBounds = false
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 0.5
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        self.layer.shadowOpacity = 0.2
        self.layer.shadowRadius = 20
        self.layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: 5).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
    
    func pinEdgesToSuperView() {
        guard let superView = superview else { return }
        translatesAutoresizingMaskIntoConstraints = false
        topAnchor.constraint(equalTo: superView.topAnchor).isActive = true
        leftAnchor.constraint(equalTo: superView.leftAnchor).isActive = true
        bottomAnchor.constraint(equalTo: superView.bottomAnchor).isActive = true
        rightAnchor.constraint(equalTo: superView.rightAnchor).isActive = true
    }
}
