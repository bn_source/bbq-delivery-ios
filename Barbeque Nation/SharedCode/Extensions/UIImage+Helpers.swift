//
 //  Created by Ajith CP on 27/11/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified UIImage+Helpers.swift
 //

import Foundation
import UIKit

extension UIImage {
    
    enum ImageQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func compressedImage(_ imageQuality: ImageQuality) -> Data? {
        
        return jpegData(compressionQuality: imageQuality.rawValue)
    }
    
    func resized(withPercentage percentage: CGFloat, isOpaque: Bool = true) -> UIImage? {

        let actualHeight:CGFloat = self.size.height
        let actualWidth:CGFloat = self.size.width
        let imgRatio:CGFloat = actualWidth/actualHeight
        let maxWidth:CGFloat = 1024.0
        let resizedHeight:CGFloat = maxWidth/imgRatio
        let compressionQuality:CGFloat = 0.4

        let rect:CGRect = CGRect(x: 0, y: 0, width: maxWidth, height: resizedHeight)
        UIGraphicsBeginImageContext(rect.size)
        self.draw(in: rect)
        let img: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        let imageData:Data = img.jpegData(compressionQuality: compressionQuality)!
        UIGraphicsEndImageContext()

        return UIImage(data: imageData)!
        
       }
    
    func imageWithColor(tintColor: UIColor) -> UIImage {
        //            UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        //
        //            let context = UIGraphicsGetCurrentContext()!
        //            context.translateBy(x: 0, y: self.size.height)
        //            context.scaleBy(x: 1.0, y: -1.0);
        //            context.setBlendMode(.normal)
        //
        //            let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height) as CGRect
        //            context.clip(to: rect, mask: self.cgImage!)
        //            tintColor.setFill()
        //            context.fill(rect)
        //
        //            let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        //            UIGraphicsEndImageContext()
        //
        //            return newImage
        let maskImage = cgImage!
        let width = size.width
        let height = size.height
        let bounds = CGRect(x: 0, y: 0, width: width, height: height)
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)!
        context.clip(to: bounds, mask: maskImage)
        context.setFillColor(tintColor.cgColor)
        context.fill(bounds)
        if let cgImage = context.makeImage() {
            let coloredImage = UIImage(cgImage: cgImage)
            return coloredImage
        } else {
            return self
        }
    }
    
    
   
    }
