//
//  UIStoryboard+Loader.swift
//  MovieApp
//
//  Created by Chandan Singh on 05/03/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

fileprivate enum Storyboard : String {
    case launchScreen = "LaunchScreen"
    case login = "Login"
    case home = "Home"
    case menu = "Menu"
    case booking = "BookingScreen"
    case bookingConfirmation = "BookingConfirmation"
    case profile = "UserProfile"
    case cancellation = "BookingCancellation"
    case bookingHistory = "BookingHistory"
    case outletInfo = "OutletInformation"
    case outletAmenities = "OutletAmenities"
    case reviewAndSupport = "ReviewsAndSupport"
    case notification = "Notification"
    case feedback = "Feedback"
    case payment = "Payment"
    case deliveryHome = "DeliveryHome"
    case splashScreen = "SplashScreen"
    case appReferral = "AppReferral"
}

fileprivate extension UIStoryboard {
    
    static func loadFromLogin(_ identifier: String) -> UIViewController {
        return load(from: .login, identifier: identifier)
    }
    
    static func loadFromHome(_ identifier: String) -> UIViewController {
        return load(from: .home, identifier: identifier)
    }
    
    static func loadFromDeliveryHome(_ identifier: String) -> UIViewController {
        return load(from: .deliveryHome, identifier: identifier)
    }
    
    static func loadFromMenu(_ identifier: String) -> UIViewController {
        return load(from: .menu, identifier: identifier)
    }
    
    static func loadFromBooking(_ identifier: String) -> UIViewController {
           return load(from: .booking, identifier: identifier)
       }
    
    static func loadFromProfile(_ identifier: String) -> UIViewController {
        return load(from: .profile, identifier: identifier)
    }
    
    static func loadFromHomeOverlay(_ identifier: String) -> UIViewController {
        return load(from: .home, identifier: identifier)
    }
    
    
    static func load(from storyboard: Storyboard, identifier: String) -> UIViewController {
        let uiStoryboard = UIStoryboard(name: storyboard.rawValue, bundle: nil)
        return uiStoryboard.instantiateViewController(withIdentifier: identifier)
    }
    
    static func loadFromBookingCancellation(_ identifier: String) -> UIViewController {
        return load(from: .cancellation, identifier: identifier)
    }
    
    static func loadFromCancellationDone(_ identifier: String) -> UIViewController {
        return load(from: .cancellation, identifier: identifier)
    }
    
    static func loadFromBookingHistory(_ identifier: String) -> UIViewController {
        return load(from: .bookingHistory, identifier: identifier)
    }
    
    static func loadFromAppReferral(_ identifier: String) -> UIViewController {
        return load(from: .appReferral, identifier: identifier)
    }
    
    static func loadFromOTPScreen(_ identifier: String) -> UIViewController {
        return load(from: .login, identifier: identifier)
    }
    
    static func loadFromMenuScreen(_ identifier: String) -> UIViewController {
        return load(from: .menu, identifier: identifier)
    }
    
    static func loadFromOutletScreen(_ identifier: String) -> UIViewController {
        return load(from: .outletInfo, identifier: identifier)
    }
    
    static func loadFromOutletAmenitiesScreen(_ identifier: String) -> UIViewController {
        return load(from: .outletAmenities, identifier: identifier)
    }
    
    static func loadFromReviewSupportScreen(_ identifier: String) -> UIViewController {
        return load(from: .reviewAndSupport, identifier: identifier)
    }
    
    static func loadFromNotification(_ identifier: String) -> UIViewController {
        return load(from: .notification, identifier: identifier)
    }
    
    static func loadPointsFromHome(_ identifier: String) -> UIViewController {
        return load(from: .home, identifier: identifier)
    }
    static func loadPointsFromFeedback(_ identifier: String) -> UIViewController {
        return load(from: .feedback, identifier: identifier)
    }
    
    static func loadFromBookingConfirmation(_ identifier: String) -> UIViewController {
        return load(from: .bookingConfirmation, identifier: identifier)
    }
}

// MARK: App View Controllers

extension UIStoryboard {
    
    static func loadNavigationViewController() -> UIViewController{
        return load(from: .splashScreen, identifier: "SplashNavController")
    }
    
    //MARK: - Login
    static func loadLoginViewController() -> LoginBackground { //LoginController {
       // return loadFromLogin("LoginController") as! LoginController
        
        return loadFromLogin("LoginBackground") as! LoginBackground
        
    }
    
    static func loadLoginThroughPhoneController() -> BBQPhoneNumberViewController {
        return loadFromLogin("BBQPhoneNumberViewController") as! BBQPhoneNumberViewController
    }
    

    static func loadOTPReceiverViewController() -> BBQOTPViewController {
        return loadFromLogin("BBQOTPViewController") as! BBQOTPViewController
    }
    

    //MARK: - Home
    static func loadTabViewController() -> BBQTabbarController {
        return loadFromHome("BBQTabbarController") as! BBQTabbarController
    }
    
    static func loadHomeViewController() -> BBQHomeViewController {
        return loadFromHome("BBQHomeViewController") as! BBQHomeViewController
    }

    static func loadPickerViewController() -> PickerViewController {
        return loadFromLogin("PickerViewController") as! PickerViewController
    }
    
    //Menu
    static func loadMenuViewController() -> BBQMenuViewController {
        return loadFromMenu("tblView") as! BBQMenuViewController
    }
    
    static func loadRegistrationConfirmation() -> BBQRegistrationConfirmationView {
        return loadFromLogin("BBQRegistrationConfirmationView") as! BBQRegistrationConfirmationView
    }
    
    // Profile
    
    static func loadProfileViewController() -> BBQProfileViewController {
        return loadFromProfile("BBQProfileViewController") as! BBQProfileViewController
    }
    static func loadProfileEditViewController(model :BBQProfileViewModel) -> BBQProfileEditViewController {
        let vc = loadFromProfile("BBQProfileEditViewController") as! BBQProfileEditViewController
        vc.profileView = model
        return vc
    }
    
    static func loadBookingCancellation() -> BBQCancellationViewController {
        return loadFromBookingCancellation("BBQCancellationViewController") as! BBQCancellationViewController
    }
    
    static func loadCancellationDone() -> BBQCancellationConfirmationViewController {
        return loadFromBookingCancellation("BBQCancellationConfirmationViewController") as! BBQCancellationConfirmationViewController
    }
    
    
    static func loadupdateAppScreen() -> BBQUpdateAlertviewViewController {
        return loadFromHome("BBQUpdateAlertviewViewController") as! BBQUpdateAlertviewViewController
    }
    
    //MARK: - Booking Confirmed Screen
    static func pendingBookingVC() -> PendingBookingVC{
        return loadFromBookingConfirmation("PendingBookingVC") as! PendingBookingVC
    }
    
    static func refundBookingPaymentVC() -> RefundBookingPaymentVC{
        return loadFromBookingConfirmation("RefundBookingPaymentVC") as! RefundBookingPaymentVC
    }
  
    //MARK: - Booking Reservation history
    static func loadReservationViewController() -> BBQMyReservationVC {
        return loadFromBookingHistory("BBQMyReservationVC") as! BBQMyReservationVC
    }
    
    static func loadOrderHistoryViewController() -> OrderHistoryViewController {
        return loadFromDeliveryHome("OrderHistoryViewController") as! OrderHistoryViewController
    }
    
    static func loadCommonWebView() -> BBQCommonWebViewController {
        return loadFromBookingCancellation("BBQCommonWebViewController") as! BBQCommonWebViewController
    }
    //MARK: - Happiness card history
    static func loadHappinesscardViewController() -> BBQMyHappinesscardVC {
//        return loadFromBookingHistory("BBQMyHappinesscardViewController") as! BBQMyHappinesscardViewController
        return loadFromBookingHistory("BBQMyHappinesscardVC") as! BBQMyHappinesscardVC
    }
    static func loadHappinesscardDetailViewController() -> BBQHappinesscardDetailsVC {
        return loadFromBookingHistory("BBQHappinesscardDetailsVC") as! BBQHappinesscardDetailsVC
    }
    
    //MARK: - App Referral
    static func loadAppReferralFullScreen() -> BBQAppReferralFullVC {
        return loadFromAppReferral("BBQAppReferralFullVC") as! BBQAppReferralFullVC
    }
    
    //OTP screen
    static func loadOTPViewController() -> BBQOTPScreenViewController {
        return loadFromLogin("BBQOTPScreenViewController") as! BBQOTPScreenViewController
    }
    
    //OTP screen
    static func loadOTPForDeleteViewController() -> BBQOTPForDeleteAccountViewController {
        return loadFromLogin("BBQOTPForDeleteAccountViewController") as! BBQOTPForDeleteAccountViewController
    }
    static func loadOutletInfoViewController() -> BBQOutletInfoViewController {
        return loadFromOutletScreen("BBQOutletInfoViewController") as! BBQOutletInfoViewController
    }
    static func loadOutletPricingViewController() -> BBQPricingDetailsVC {
        return loadFromOutletScreen("BBQPricingDetailsVC") as! BBQPricingDetailsVC
    }
    static func loadOutletAmbienceViewController() -> BBQAmbienceVC? {
        return loadFromOutletScreen("BBQAmbienceVC") as? BBQAmbienceVC
    }
    static func loadOutletBranchMenuViewController() -> BBQBranchMenuVC? {
        return loadFromOutletScreen("BBQBranchMenuVC") as? BBQBranchMenuVC
    }
    
    static func loadOutletAmenitiesViewController() -> OutletAmenitiesViewController? {
        return loadFromOutletScreen("OutletAmenitiesViewController") as? OutletAmenitiesViewController
    }
    
    //MARK: Notification Screen
    static func loadNotificationViewController() -> BBQNotificationViewController {
        return loadFromNotification("BBQNotificationViewController") as! BBQNotificationViewController
    }
    
    static func loadAboutViewController() -> BBQAboutAppVC {
        return loadFromReviewSupportScreen("BBQAboutAppVC") as! BBQAboutAppVC
    }
    
    static func loadHelpSupportViewController() -> BBQHelpSupportViewController {
        return loadFromReviewSupportScreen("BBQHelpSupportViewController") as! BBQHelpSupportViewController
    }
    
    static func loadRatingsViewController() -> BBQRatingsViewController {
        return loadFromReviewSupportScreen("BBQRatingsViewController") as! BBQRatingsViewController
    }
    //MARK: Loyality Points Screen
    static func loadLoyalityPointsViewController() -> BBQSmileHistoryViewController {
        //return loadPointsFromHome("BBQLoyalityPointsController") as! BBQLoyalityPointsController
        return loadPointsFromHome("BBQSmileHistoryViewController") as! BBQSmileHistoryViewController
    }
    
    static func loadSmileHistoryViewController() -> BBQSmileHistoryViewController {
        return loadPointsFromHome("BBQSmileHistoryViewController") as! BBQSmileHistoryViewController
    }
    
    static func loadEnquireViewController() -> BBQMultiLocationRequirementDetailViewController {
        return loadFromDeliveryHome("BBQMultiLocationRequirementDetailViewController") as! BBQMultiLocationRequirementDetailViewController
    }
    static func loadMyBenefitsController() -> MyBenefitsVC {
        return loadPointsFromHome("MyBenefitsVC") as! MyBenefitsVC
    }
    
    static func loadLoginBackground() -> LoginBackground {
        return loadFromLogin("LoginBackground") as! LoginBackground
    }
    //Custom payment
    static func loadFromPayment(_ identifier: String) -> UIViewController {
        return load(from: .payment, identifier: identifier)
    }
    static func loadPaymentNav() -> UINavigationController {
        return (loadFromPayment("BBQCustomPaymentNavigation") as? UINavigationController)!

    }
    static func loadPayment() -> BBQCustomPaymentViewController {
        return loadFromPayment("BBQCustomPaymentViewController") as! BBQCustomPaymentViewController
    }
    static func loadEnterCard() -> BBQEnterCardViewController {
        return (loadFromPayment("BBQEnterCardViewController") as? BBQEnterCardViewController)!
    }
    static func loadMoreBanks() -> BBQMoreBanksViewController {
        return (loadFromPayment("BBQMoreBanksViewController") as? BBQMoreBanksViewController)!
    }
    static func loadMoreWallets() -> BBQMoreWalletsViewController {
        return (loadFromPayment("BBQMoreWalletsViewController") as? BBQMoreWalletsViewController)!
    }
    static func loadWelcomePage() -> BBQWelcomeViewController {
        return loadFromLogin("BBQWelcomeViewController") as! BBQWelcomeViewController
    }
    
    static func loadAfterDining() -> BBQAfterDiningViewController {
        return loadFromBooking("BBQAfterDiningViewController") as! BBQAfterDiningViewController
    }
    
    static func loadPromotionDetailScreen() -> BBQPromotionDetailScreen {
        return loadFromHome("BBQPromotionDetailScreen") as! BBQPromotionDetailScreen
    }
    
    static func loadSpecialPromotionDetailScreen() -> BBQSpecialPromoViewController {
        return loadFromHome("BBQSpecialPromoViewController") as! BBQSpecialPromoViewController
    }
    
    static func loadVoucherDetailScreen() -> BBQVoucherDetailScreen {
        return loadFromHome("BBQVoucherDetailScreen") as! BBQVoucherDetailScreen
    }
    
    // Terms & Conditions
    
    static func loadTermsAndConditionsScreen() -> BBQTermsAndConditionsViewController {
        return loadFromBooking("BBQTermsAndConditionsViewController") as! BBQTermsAndConditionsViewController
    }
    
    static func laodReferAndEarnScreen() -> BBQReferAndEarnController {
        return loadFromHome("BBQReferAndEarnController") as! BBQReferAndEarnController
    }
    
    // Voucher Gifting
    
    static func loadGiftVoucherScreen() -> BBQVoucherGiftingViewController {
        return loadFromBookingHistory("BBQVoucherGiftingViewController") as! BBQVoucherGiftingViewController
    }
    
    static func loadFeedBackViewController() -> FeedbackMainViewController {
        return loadPointsFromFeedback("FeedbackMainViewController") as! FeedbackMainViewController
    }
    
    static func loadFeedBackFromSideMenuViewController() -> BBQSideMenuFeedbackViewController {
        return loadPointsFromFeedback("BBQSideMenuFeedbackViewController") as! BBQSideMenuFeedbackViewController
    }
    
    static func loadFeedBackDetailsViewController() -> FeedbackDetailedViewController {
        return loadPointsFromFeedback("FeedbackDetailedViewController") as! FeedbackDetailedViewController
    }
    
    // Safety Feature
    static func loadSafetyFeaturesViewController() -> SafetyFeaturesVC {
        return loadFromDeliveryHome("SaferyFeaturesVC") as! SafetyFeaturesVC
    }
    
    static func loadFeedBackThanksViewController() -> BBQFeedbackThanksViewController {
        return loadPointsFromFeedback("BBQFeedbackThanksViewController") as! BBQFeedbackThanksViewController
    }
    
    static func loadSideMenuFeedBackThanksViewController() -> BBQSideMenuFeedBackThanksViewController {
        return loadPointsFromFeedback("BBQSideMenuFeedBackThanksViewController") as! BBQSideMenuFeedBackThanksViewController
    }
    
    static func loadDeliveryOfferListVC() -> DeliveryHomeOfferListVC {
        return loadFromDeliveryHome("DeliveryHomeOfferListVC") as! DeliveryHomeOfferListVC
    }
}
