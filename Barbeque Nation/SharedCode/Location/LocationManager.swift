//
//  LocationManager.swift
//  Barbeque Nation
//
//  Created by Abhijit on 20/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import Foundation
import CoreLocation

class LocationManager
{
    static let shared = LocationManager()
    var locationManager : CLLocationManager = CLLocationManager()

    
    init(){
    }
    
    var selectedOutlet: String? = nil
    
    
    
    func requestForLocation(){
        print("Location granted")
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
            @unknown default:
                return
            }
        } else {
            print("Location services are not enabled")
        }

    }
    
    func clearAllData(){
        selectedOutlet = nil
    
    }
}
