//
//  LocationManager.swift
//  Barbeque Nation
//
//  Created by Abhijit on 20/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

class BBQLocationManager:NSObject,CLLocationManagerDelegate {
    
    static let shared: BBQLocationManager = {
        let instance = BBQLocationManager()
        return instance
    }()
    
    var outlet_branch_id :String = "" {
        didSet {
            print("")
        }
    }
    
    var currentLocationManager : CLLocationManager = CLLocationManager()
    var outlet_branch_name :String = ""
    var user_current_latitude :Double = 0
    var user_current_longitude :Double = 0
    
    private override init(){}
    
    var selectedOutlet: String? = nil
    
    func updateUserLocationWithPermission() {
        currentLocationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        let status = CLLocationManager.authorizationStatus()
        self.handleLocationAuthorizationStatus(status: status)
        currentLocationManager.delegate = self
    }
    
    func handleLocationAuthorizationStatus(status: CLAuthorizationStatus){
        
        switch status {
        case .notDetermined:
            currentLocationManager.requestWhenInUseAuthorization()
            
        case .authorizedWhenInUse, .authorizedAlways:
            currentLocationManager.startUpdatingLocation()
            
        case .denied:
            BBQUserDefaults.sharedInstance.userDeniedLocation = true
            postLocationDeniedNotification()
        case .restricted: break
        @unknown default:
            return
        }
    }
    
    func clearAllData() {
        selectedOutlet = nil
    }
    
    func postLocationDeniedNotification(){
        let locationDict = ["latitude": 0.0,
                            "longitude": 0.0]
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.NotificationName.userLocationChanged), object: nil, userInfo: locationDict)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocationManager = manager
        let location = locations[0]
        currentLocationManager.stopUpdatingLocation()
        currentLocationManager.delegate = nil
        
        BBQLocationManager.shared.user_current_latitude = location.coordinate.latitude
        BBQLocationManager.shared.user_current_longitude = location.coordinate.longitude
        
        let locationDict = ["latitude": location.coordinate.latitude,
                            "longitude": location.coordinate.longitude]
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.NotificationName.userLocationChanged), object: nil, userInfo: locationDict)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse{
            currentLocationManager.startUpdatingLocation()
        }else if ((status == .denied)||(status == .notDetermined)){
            self.postLocationDeniedNotification()
        }
    }
}

