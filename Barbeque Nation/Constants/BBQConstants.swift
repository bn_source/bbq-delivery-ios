//
//  BBQConstants.swift
//  Barbeque Nation
//
//  Created by Rabindra L on 31/07/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

struct Constants {
    struct App {
        
        struct LoginCellHeight {
            static let hidingCellHeight: CGFloat = 0.0
            static let headerCellHeight: CGFloat = 108.0
            static let signUpCellHeight: CGFloat = 50.0
            static let socialCellHeight: CGFloat = 100.0
            static let phoneCellHeight: CGFloat = 72.0
            static let nameCellHeight: CGFloat = 78.0
            static let referralCellHeight: CGFloat =  0.0 // Hiding temporarly
            static let backButtonCellHeight: CGFloat = 75
            static let otpCellHeight: CGFloat = 120.0
            static let otpCellHeightCorporate:CGFloat = 130.0
        }
        
        struct BBQAppFont {
            static let NutinoSansBlack = "NunitoSans-Black"
            static let NutinoSansBlackItalic = "NunitoSans-BlackItalic"
            static let NutinoSansBold = "NunitoSans-Bold"
            static let NutinoSansBoldItalic = "NunitoSans-BoldItalic"
            
            static let NutinoSansExtraBold = "NunitoSans-ExtraBold"
            static let NutinoSansExtraBoldItalic = "NunitoSans-ExtraBoldItalic"
            static let NutinoSansExtraLight = "NunitoSans-ExtraLight"
            static let NutinoSansExtraLightItalic = "NunitoSans-ExtraLightItalic"
            static let NutinoSansItalic = "NunitoSans-Italic"
            
            static let NutinoSansLight = "NunitoSans-Light"
            static let NutinoSansLightItalic = "NunitoSans-LightItalic"
            static let NutinoSansRegular = "NunitoSans-Regular"
            static let NutinoSansSemiBold = "NunitoSans-SemiBold"
            static let NutinoSansSemiBoldItalic = "NunitoSans-SemiBoldItalic"
            
            static let ThemeBlack = "Poppins-Black"
            static let ThemeBlackItalic = "Poppins-BlackItalic"
            static let ThemeBold = "Poppins-Bold"
            static let ThemeBoldItalic = "Poppins-BoldItalic"
            static let ThemeExtraBold = "Poppins-ExtraBold"
            static let ThemeExtraBoldItalic = "Poppins-ExtraBoldItalic"
            static let ThemeExtraLight = "Poppins-ExtraLight"
            static let ThemeExtraLightItalic = "Poppins-ExtraLightItalic"
            static let ThemeItalic = "Poppins-Italic"
            static let ThemeLight = "Poppins-Light"
            static let ThemeLightItalic = "Poppins-LightItalic"
            static let ThemeMedium = "Poppins-Medium"
            static let ThemeMediumItalic = "Poppins-MediumItalic"
            static let ThemeRegular = "Poppins-Regular"
            static let ThemeSemiBold = "Poppins-SemiBold"
            static let ThemeSemiBoldItalic = "Poppins-SemiBoldItalic"
            static let ThemeThin = "Poppins-Thin"
            static let ThemeThinItalic = "Poppins-ThinItalic"
            
        }
        
        struct themeColor {
            static let white = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            static let grey = #colorLiteral(red: 0.2117647059, green: 0.2509803922, blue: 0.2666666667, alpha: 1)
            static let orange = #colorLiteral(red: 1, green: 0.3882352941, blue: 0.003921568627, alpha: 1)
            static let borderBlack = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1)
            static let selectTimeColor = #colorLiteral(red: 0.6980392157, green: 0.6980392157, blue: 0.6980392157, alpha: 1)
            static let corporateBackgroundColor = #colorLiteral(red: 0.968627451, green: 0.968627451, blue: 0.968627451, alpha: 1)
            static let green = #colorLiteral(red: 0.4078431373, green: 0.7803921569, blue: 0, alpha: 1)
        }
        
        struct ProfilePage {
            static let headerEstimatedRowHeight: CGFloat = 400.0
        }
        
        struct defaultValues {
            static let phoneNumberCode = "+91"
        }
        
        struct Validations {
            static let maxOTPTrial = 5
            static let maxResendOTP = 5
            static let nameMaxLength = 50
            static let nameMinLength = 3
            static let signupCount = 3
        }
        
        struct title {
            static let mr  = "Mr."
            static let mrs  = "Mrs."
            static let miss  = "Ms."
        }
        
        struct Splash {
            static let sessionExpired = "Session has expired please login"
        }
        
        struct NotificationPage {
            static let notificationActive = "NotificationActive"
            static let receiveNotification = "ReceiveNotification"
            static let refreshNotification = "refreshNotification"
            static let aps = "aps"
            static let alert = "alert"
            static let image = "image"
            static let title = "title"
            static let body = "body"
            static let token = "token"
            static let fcm_options = "fcm_options"
            static let gcmMessageIDKey = "gcmMessageIDKey"
            static let notification = "Notification"
            static let notifyImageViewZeroHeightConstraint : CGFloat = 0.0
            static let notificationImageZeroHeightConstraint : CGFloat = 0.0
            static let notifyImageViewHeightConstraint : CGFloat = 238.0
            static let notificationImageHeightConstraint : CGFloat = 238.0
            static let backGroundViewCornerRadius : CGFloat = 10.0
            //Channel Values
            static let booking = "booking"
            static let event = "event"
            static let loyalty = "loyalty"
            static let voucher = "voucher"
            static let coupen = "coupen"
            static let promotion = "promotion"
            static let offer = "offer"
            static let delivery = "delivery"
            // Deeplink KEYS
            static let channel = "channel"
            static let notification_event = "notification_event"
            static let notification_type = "notification_type"
            static let device_type = "device_type"
            static let expirytime = "expiry_time"
            static let tagline = "tag_line"
            static let deeplink = "deeplink"
            static let readunread = "readunread"
            static let invoice_url = "invoice_url"
            static let booking_status = "booking_status"
            static let entity_id = "entity_id"
            static let PRE_RECEIPT = "PRE_RECEIPT"
            static let SETTLED = "SETTLED"
            static let CONFIRMED = "CONFIRMED"
            static let CANCELLED = "CANCELLED"
            //Deeplink values
            static let bookingFinalPayment  = "booking/FinalPayment"
            //Images
            static let icon_offer = "icon_offer"
            static let icon_bbqn_points = "icon_bbqn_points"
            static let icon_celebration = "icon_celebration"
            static let icon_calendar_tick = "icon_calendar_tick"
            static let INR = "INR"
            static let Notification = "Notification"
            
        }
        
        struct LoyalitiPage {
            static let goldMemberLeadingConstant:CGFloat = 6.0
            static let borderWidth:CGFloat = 0.5
            static let referAndEarnRadius:CGFloat = 20.0
            static let greyBackgroundViewRadius:CGFloat = 10.0
            static let orangeColor = "#FF6301"
            static let greyColor = "#364044"
            static let greenColor = "#68C700"
            static let credit = "CREDIT"
            static let debit = "DEBIT"
            static let plus = "+"
            static let minus = "-"
            static let sortValue = "id,DESC"
            static let referAndEarn = "Hey, I am sending you 150 SMILES on BarbequeNation App.To accept, sign up on the app with the code "
            static let details = " & Enjoy!."
            static let referAndEarnUrl = "https://bit.ly/BNBBQN"
            static let referAndEarnShortLink = "https://bit.ly/BNBBQN"
            static let InviteFriends = "Hey! Want to relish the best BBQ's in town. Check out this latest app from Barbeque Nation & earn SMILES when you dine.To download click "
        }
    }
    
    struct CellIdentifiers {
        //Login
        static let loginHeaderCell  = "LoginHeaderCell"
        static let phoneNumberCell  = "PhoneNumberCell"
        static let socialLogoCell   = "SocialLogoCell"
        static let nameCell         = "NameCell"
        static let referralCell     = "ReferralCodeCell"
        static let backButtonCell   = "BackButtonCell"
        static let otpCell          = "OTPCell"
        static let signUpCell       = "signUP"
        static let welcomeCell      = "BBQWelcomeCell"

        
        //Home
        static let bottomSheetImageCell = "BottomSheetImageTableCell"
        static let bottomSheetFooterCell = "BottomSheetImageFooterView"
        static let bottomSheetHeaderCell = "BottomSheetImageHeaderView"
        
        //Payment
        static let paymentBottomSheetController = "BBQPaymentBottomSheetController"
        static let paymentBottomSheetHeaderCell = "BBQCollapsibleTableViewHeader"
        static let paymentAmountCell = "BBQPaymentAmountCell"
        static let paymentBottomSheetFooterCell = "BBQPaymentFooterView"
        
        // Profile
        static let profileHaderCellID   = "BBQProfileHaderCellID"
        static let profileEntriesCellID = "BBQProfileEntriesCellID"
        static let profileEntriesEditCellID = "BBQProfileEntriesEditCellID"
        
        static let verifierHeaderCellID      = "BBQMobileVerifierHeaderTableViewCellID"
        static let verifierPhoneNumberCellID = "PhoneNumberCell"
        static let verifierActionsCellID     = "BBQMobileVerifierActionTableViewCellID"
        static let verifiedMobileCellID      = "BBQMobileVerifiedTableViewCellID"
        
        //Booking Cancellation
        static let singleLabelCell = "SingleLabelCell"
        static let userDefinedReasonCell = "UserDefinedReasonCell"
        static let cancelButtonCell = "CancellationButtonCell"
        static let BBQHeaderCell = "BBQHeaderCell"
        static let BBQCancellationRefundTextCell = "BBQCancellationRefundTextCell"
        
        // Corporate Booking
        static let corporateOffersCell =  "CorporateOffersCell"
        
        //Home screen
        static let upcomingReservationCell = "UpcomingReservationCell"
        static let voucherCell = "VoucherCell"
        static let promotionCell = "PromotionCell"
        static let openOrderCell      = "OpenOrderCell"

        
        //Vouchers
        static let voucherDetailsControllerNib = "BBQVoucherDetailsController"
        static let voucherDetailsHeaderCell = "BBQVoucherDetailsTableHeader"
        static let voucherDetailsDataCell = "BBQVoucherDetailsDataCell"
        static let voucherDetailsTimingsCell = "BBQVoucherDetailsTimingCell"
        
        //Cart
        static let cartControllerNib = "BBQMyCartViewController"
        static let cartHeaderViewNib = "BBQMyCartHeaderView"
        static let cartFooterViewNib = "BBQMyCartFooterView"
        static let cartSubTotalCell = "BBQMyCartSubTotalCell"
        static let cartGSTCell = "BBQMyCartGSTCell"
        static let cartPointsCell = "BBQMyCartPointsCell"
        static let cartExtrasCell = "BBQCartExtrasCell"
        static let cartEstimatedTotalCell = "BBQCartEstimatedTotalCell"
        static let cartContentCell = "BBQCartContentCell"
        static let cartFinalCell = "BBQCartFinalCell"
        
        //Notification
        static let notificationCell = "NotificationCell"
        
        //Loyality Points
        static let pointsCell = "BBQPointsCell"
        static let cellIdentifer = "Cell"
        static let BBQTermsNConditionsTableViewCellID = "BBQTermsNConditionsTableViewCellID"
        static let BBQTermsNConditionsTableViewCell = "BBQTermsNConditionsTableViewCell"
        
        //About
        static let expandableCell = "BBQExpandableCell"
        static let questionnarieCell = "BBQQuestionnarieCell"
        static let helpSupportFooterCell = "BBQHelpSupportFooterCell"
        static let noResult = "NoResultTableViewCell"
        
        //Special Instructions
        static let specialInstructionCell = "InstructionCollectionCell"
        static let specialInstructionHeader = "InstructionHeaderView"
        static let joinedInstructionCell = "JoinedCollectionCell"


        //Booking
        static let foodTypeCell = "BottomSheetTimeSlotTableViewCell"
        static let timeSlotCell = "BottomSheetDateTimeTableViewCell"
        
        //Promotion Details
         static let BBQPromotionDetailCell = "BBQPromotionDetailCell"
         static let BBQPromotionDetailContentCell = "BBQPromotionDetailContentCell"
        //Voucher Details
        static let BBQVoucherDetailCell = "BBQVoucherDetailCell"
        static let BBQDescriptionCell = "BBQDescriptionCell"
    
    }
    
    struct TagValues {
        static let promotionCellTag  = 11
        static let voucherCellTag  = 22
        static let reservationCellTag  = 33
        static let openOrderCellTag  = 34
    }
    
    struct NibName {
        
        // Profile
        static let profileHaderCellNibName   = "BBQProfileHeaderTableViewCell"
        static let profileEntriesCellNibName = "BBQProfileEntriesTableViewCell"
        static let profileEntriesEditCellNibName = "BBQProfileEditEntryTableViewCell"
        
        static let verifierHeaderCell      = "BBQMobileVerifierHeaderTableViewCell"
        static let verifierPhoneNumberCell = "PhoneNumberCell"
        static let verifierActionsCell     = "BBQMobileVerifierActionTableViewCell"
        static let verifiedMobileCell      = "BBQMobileVerifiedTableViewCell"
        
        
        // Corporate Booking
        static let corporateOffersCellNibName =  "BBQAllOffersCell"
        
        //Notification
        static let notificationCellNibName = "BBQNotificationCell"
        
        //Special Instructions
        static let specialInstructionNibName = "BBQInstructionCollectionCell"
        static let instructionHeaderNibName = "BBQInstructionHeaderView"
        static let joinedCellNibName = "BBQJoinedCollectionCell"
        
        
        //Loyality Points
        static let pointsCellNibName = "BBQPointsCell"
        
        static let voucherCouponFooterNib = "BBQAllOffersFooterView"

    }
    
    struct AssetName {
        
        // Profile
        static let notifSwitchOn        = "BBQSwitchOn"
        static let notifSwitchOff       = "BBQSwitchOff"
        static let profileEditIcon      = "BBQEditIcon"
        static let editGreyIcon         = "BBQGreyEditIcon"
        static let blackLocationIcon    = "BBQBlackLocationIcon"
        static let blackMailIcon        = "ProfileMail"
        static let blackBirthdayIcon    = "ProfileGift"
        static let blackAnniversaryIcon = "ProfileGender"
        static let blackMobileIcon      = "ProfileCal"
        static let blackGenderIcon      = "ProfileGender"
        static let notificationIcon     = "ProfileNotification"
        static let orangeTickIcon       = "BBQOrangeTickIcon"
        static let emptyAvtarOne        = "ProfileImage"
        static let checkbox             = "checkBox"
        static let bbqLogo              = "bbq_logo_white"
        static let corporateLogoBig     = "icon_corporate_big"
        static let emptyAvtarTwo        = "ProfileImage"
        static let roundedTickOrange    = "icon_tick_orange"
        
        static let appVersion           = "icon_app_version"
        static let termsCondition       = "icon_terms"
        static let privacyPolicy        = "icon_privacy"
        static let bbqPoints            = "logo_bbqn_points"
        static let bbqAddEmailTitle     = "BBQAddEmail"
        static let kAddMobileLogo       = "BBQPhoneLogo"
        static let FeedDaileWager       = "DailyWeger"
        static let About                = "About"

        //Cart
        static let cartEmptyIcon = "icon_cart_empty"
        static let cartFilledIcon = "icon_cart_filled"
        
        //Corporate Offers
        static let corporateIcon = "BBQ_logo_welcome"
        static let corporateIconWrong = "icon_wrong"
        static let corporateIconTick = "icon_tick_green"
        
        //Notification
        static let notificationActive = "icon_notification_active"
        static let notification =  "Notification"
        
        //Home
        
        static let location = "icon_location_line"

    }
    
    struct InstagramSignup {
        
        // UI Constants
        
        static let loginStoryBoard = "Login"
        static let instaControllerClassID = "BBQInstagramLoginViewController"
        
        
        // Service Constants
        // TODO: Move to configuration part
        static let instagramAuthFormat = "%@?client_id=%@&client_secret=%@&redirect_uri=%@&response_type=token&scope=%@&DEBUG=True"
        static let instagramAuthURL = "https://api.instagram.com/oauth/authorize/"
        static let instagramUserPath = "users/self"
        static let instagramClientID = "f7ed21a4d0124843b15652271e44d103"
        static let instagramClientSecret = "e29645ac3da041819424426c381c938d"
        
        static let instagramRedirectURI = "https://www.barbequenation.com"
        static let instgramAccessToken = ""
        static let instagramScope = "basic"
    }
    
    struct OrderConfirm {
        
        static let orderConfirmPageNibID = "BBQBookingConfirmationVC"
        static let orderConfirmStoryBoard = "BookingConfirmation"
    }
    
    struct Payment {
        static let razorpayPublicKey   = "RazorpayPublicKey"
        static let razorpayPrivateKey  = "RazorpayPrivateKey"
        static let checkoutFormThemeCodeKey = "color"
        static let checkoutFormThemeCode    = "#FF6301"
        static let checkoutFormBBQIcon      = "BBQRazorpayLogo"
    }
    
    struct LocationNibName {
        // Location
        static let locationHeaderCellNibName   = "CountryTableViewCell"
    }
  
    struct NotificationName {
        static let companyNameSelectedNotification = "CompanyNameSelected"
        static let otpVeifiedNotification = "CompanyOtpVerified"
        static let animateTextField = "AnimateTextField"
        static let changeOuletCall = "changeOuletCall"
        static let changeDateCall = "changeDateCall"
        static let locationDataUpdate = "locationDataUpdate"
        static let locationDataUpdatedForFeedbackPurpose = "locationDataUpdatedForFeedbackPurpose"
        static let userLocationChanged = "userLocationChanged"
        static let userLoggedOut = "userLoggedOut"
        static let receiedUserProfileData = "receiedUserProfileData"
        static let dismissOutletSelection = "dismissOutletSelection"

        static let specialInstructionSelected = "specialInstructionSelected"
        static let specialInstructionNotSet = "specialInstructionNotSet"
        static let bookingConfirmed = "BookingConfirmed"
        static let bookingCancelled = "BookingCancelled"
        static let bookingFlowCancelled = "BookingFlowCancelled"
        static let showBookingScreen = "ShowBookingScreen"
        static let upadateHomeHeader = "UpadateHomeHeader"
        static let cartDataUpdated = "CartDataUpdated"
    }
    
    struct values {
        static let countryFlagStartValue = 127397
        static let alphabetAndNumberOnly = " ABCDEFGHIJKLMNOPQRSTUVWXYZ\n0123456789+"
        static let keyboardHeight = 351
        static let customerCareNumber = "telprompt://+918035002040"
        static let customerCareEmail = "feedback@barbequenation.com"
    }
    
    struct BBQBookingStatus {
        static let confirmedBooking = "BOOKING_CONFIRMED"
        static let pendingBooking = "PENDING"
        static let razorPayment = "RAZOR_PAY"
        static let packsCount = "packs"
        static let selectTime = "select time"
        static let nonVegFoodType = "NONVEG"
        static let vegFoodType = "VEG"
        static let todaytomorrowDateTimeFormat = "yyyy-MM-dd"
        static let todaytomorrowDisplayTimeFormat = "dd MMM, EEEE"
        static let timeTwentyFourHourFormat = "HH:mm"
        static let timeTwelveHourFormat = "hh:mma"
        static let calendarDateTimeFormat = "dd MMMM"
    }
    struct CurrencySymbol {
        static let rupeeSymbol = "\u{20B9} "
    }
    
    struct CountryCode {
        static let india = "+91"
        static let USA = "+1"
        static let UAE = "+971"
        static let oman = "+968"
        static let malaysia = "+60"
        static let nepal = "+977"
        static let UK = "+44"
        static let singapore = "+65"
        static let saudiArabia = "+966"
        
        struct ValidLength {
            static let india = 10
            static let USA = 10
            static let UAE = 9
            static let oman = 8
            static let malaysia = 9
            static let nepal = 10
            static let UK = 10
            static let singapore = 8
            static let saudiArabia = 9
            static let others = 15
        }
    }
}
var isClickedOnUbq = false
var isClickedOnHpc = false
var currentUserCity: String?
//A variable to mentain the user selected choice for delivery and take away
//When ever user choses any option will reflect on bothe the tabs cart and deliver
//Will be maintained until user in current session
var deliveryOrTakeAwayOptionSelected : TakeAwayDelivery = TakeAwayDelivery.Delivery
var deliveryCartAmount: String?
var deliveryCartCode: String?
var deliveryCartType: String?

//Variable to check access token expiry checked or not
var isTokenExpiredChecked = false
var referalCodeReceived = "referalCodeReceived"
//Variable for dining and reservation flag
var only_delivery = false
var only_dining = false
var localSavedBranches = [LocalBranch]()
var tabBarSelectedIndex: Int? = nil
var brandlistForHomePage = [BrandInfo]()
var brandSelectedLat: CGFloat?
var brandSelectedLong: CGFloat?
var tempSelectedBrandForDelivery: String?
var savedResponseOfBranches: BrandsData? = nil
var bookingIdForPrint: String?

//Variable To check if AppReferal Module is activated or not
var isAppReferAndEarnActivated: Bool = false
