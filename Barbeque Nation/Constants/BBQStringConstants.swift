//
//  BBQStringConstants.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 09/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import Foundation

// App update
let kAppUpdateTitle = NSLocalizedString("APP_UPDATE_TITLE", comment: "")
let kAppUpdateMessage = NSLocalizedString("APP_UPDATE_MESSAGE", comment: "")
let kAppUpdateButtonString = NSLocalizedString("APP_UPDATE", comment: "")

//Common String
let kConnectionTitleMessage = NSLocalizedString("CONNECTION_TITLE_MESSAGE", comment: "")
let kConnectionDescMessage = NSLocalizedString("CONNECTION_DESCRIPTION_MESSAGE", comment: "")
let kLoadingProgressString = NSLocalizedString("LOADING_PROGRESS_STRING", comment: "")

// Hamburger Menu Strings
let kHamMenuHomeTitle = NSLocalizedString("HAM_MENU_HOME_TITLE", comment: "Home")
let kHamMenuTransactionsTitle = NSLocalizedString("HAM_MENU_TRANSACTIONS_TITLE", comment: "My Transactions")
let kHamMenuReservationTitle = NSLocalizedString("HAM_MENU_RESERVATION_TITLE", comment: "My Reservations")
let kHamMenuHappinessCardTitle = NSLocalizedString("HAM_MENU_HAPPINESSCARD_TITLE", comment: "My Happiness card")
let kHamMenuOutletInfo = NSLocalizedString("HAM_MENU_OUTLET_INFO", comment: "Outlet Info")
let kHamMenuBookingsTitle = NSLocalizedString("HAM_MENU_BOOKINGS_TITLE", comment: "Bookings")
let kHamMenuNotificationsTitle = NSLocalizedString("HAM_MENU_NOTIFICATIONS_TITLE", comment: "Notifications")
let kHamMenuPromotionsAndOffersTitle = NSLocalizedString("HAM_MENU_PROMOTIONS&OFFERS_TITLE", comment: "Offers & Promotions")
let kHamMenuPromotionsAndOffersTitleUser = NSLocalizedString("HAM_MENU_SPECIALLY_USER", comment: "Promotions & Offers")
let kManageAddresses = NSLocalizedString("MANAGE_ADDRESSES", comment: "Manage Addresses")
let kDeliveryHistory = NSLocalizedString("DELIVER_HISTORY", comment: "My Orders")
let kHamMenuProfileTitle = NSLocalizedString("HAM_MENU_PROFILE_TITLE", comment: "Profile")
let kHamMenuHelpAndSupportTitle = NSLocalizedString("HAM_MENU_Help&Support_TITLE", comment: "Help & Support")
let kHamMenuAboutTitle = NSLocalizedString("HAM_MENU_ABOUT_TITLE", comment: "About")
let kHamMenuFeedbackTitle = NSLocalizedString("HAM_MENU_FEEDBACK_TITLE", comment: "Feedback")
let kHamMenuLogoutTitle = NSLocalizedString("HAM_MENU_LOGOUT_TITLE", comment: "Logout")
let kHamMenuLoginTitle = NSLocalizedString("HAM_MENU_LOGIN_TITLE", comment: "Login")
let kHamMenuLogoutSuccessTitle = NSLocalizedString("HAM_MENU_LOGOUT_SUCCESS_TITLE", comment: "Login")
let kHamMenuBookATableTitle = NSLocalizedString("HAM_MENU_BOOK_A_TABLE_TITLE", comment: "")
let kHamMenuInviteFriendsTitle = NSLocalizedString("HAM_MENU_INVITE_FRIENDS_TITLE", comment: "")
let kHamMenuTransactionLoginMessage = NSLocalizedString("HAM_MENU_TRANSACTION_LOGINI_MESSAGE", comment: "")
let kHamMenuOutletInfoMessage = NSLocalizedString("HAM_MENU_OUTLET_INFO_MESSAGE", comment: "")
let kHamMenuHomeImage = "SideMenuHome"
let kHamMenuReservationImage = "SideMenuReservation"
let kHamMenuHappinessCardImage = "SideMenuGiftCard"
let kHamMenuOutletInfoImage = "SideMenuRestuarentInfo"
let kHamMenuNotificationsImage = "SideMenuNotification"
let kHamMenuPromotionsAndOffersImage = ""
let kManageAddressesImage = "SideMenuAddress"
let kHamMenuProfileImage = "SideMenuProfile"
let kHamMenuHelpAndSupportImage = "SideMenuHelp"
let kHamMenuAboutImage = "SideMenuAbout"
let kHamMenuLogoutImage = "SideMenuLogout"
let kHamMenuInviteFriendsImage = "SideMenuInviteFriend"
let kHamMenuMyOrderImage = "SideMenuMyOrders"
let kHamMenuSpeciallyforUImage = "SideMenuSpecially"
let kHamMenuFeedbackforUImage = "smileUnSelectedTaste"

//Login Screen Strings
let kReEnterOTPMessage = NSLocalizedString("RE_ENTER_OTP_MESSAGE", comment: "")
let kEnterOTPMessage = NSLocalizedString("ENTER_OTP_MESSAGE", comment: "")
let kOTPErrorMessage = NSLocalizedString("RE_OTP_FAILED_MESSAGE", comment: "")
let kSignInMessage = NSLocalizedString("RE_SIGN_IN_MESSAGE", comment: "")
let kSignUpMessage = NSLocalizedString("RE_SIGN_UP_MESSAGE", comment: "")
let kSignUpFailedMessage = NSLocalizedString("RE_SIGN_UP_FAILED_MESSAGE", comment: "")
let kSignInLabel = NSLocalizedString("RE_SIGN_IN_LABEL", comment: "")
let kSignUpLabel = NSLocalizedString("RE_SIGN_UP_LABEL", comment: "")
let kValidPhoneNumberLabel = NSLocalizedString("RE_VALID_PHONE_NUMBER_LABEL", comment: "")
let kNoResultFound = NSLocalizedString("NO_RESULT_FOUND", comment: "")
let kMaxReqCrossedTitle = NSLocalizedString("MAX_REQ_CROSSED_TITLE", comment: "")
let kMaxReqCrossedDesc = NSLocalizedString("MAX_REQ_CROSSED_DESC", comment: "")
let kMaxAtmpReachTitle = NSLocalizedString("LAST_ATTEMPT_REACHED_TITLE", comment: "")
let kMaxAtmpReachDesc = NSLocalizedString("LAST_ATTEMPT_REACHED_DESC", comment: "")
let kMaxAtmpOtpReachDesc = NSLocalizedString("MAX_OTP_TRY_DESC", comment: "")
let kCorporateOffers = NSLocalizedString("RE_CORPORATE_OFFERS", comment: "")
let kOtpSentNotification = NSLocalizedString("OTP_SENT_NOTIFICATION", comment: "")
let kEmailSentNotification = NSLocalizedString("OTP_EMAIL_NOTIFICATION", comment: "")
let kUpdateMobileString = NSLocalizedString("UPDATE_MOBILE", comment: "")
let kDeleteAccountString = NSLocalizedString("DELETE_ACCOUNT", comment: "")

let kSignUp = NSLocalizedString("SIGN_UP", comment: "")
let kSignUpConfirmationTitleText = NSLocalizedString("SIGN_UP_CONFIRMATION_TITLE_TEXT", comment: "")
let kSignUpConfirmationNavigationText = NSLocalizedString("SIGN_UP_CONFIRMATION_NAVIGATION_TEXT", comment: "")
let kVoucherPurchase = NSLocalizedString("VOUCHER_PURCHASE", comment: "")
let kTitleLabelText = NSLocalizedString("TITLE_LABEL_TEXT", comment: "")
let kYourNameLabelText = NSLocalizedString("YOUR_NAME_LABEL_TEXT", comment: "")
let kSendOTPText = NSLocalizedString("SEND_OTP_TEXT", comment: "")
let kResendOTPText = NSLocalizedString("RESEND_OTP_TEXT", comment: "")
let kEnterInviteCode = NSLocalizedString("ENTER_INVITE_CODE", comment: "")
let kInvalidCode = NSLocalizedString("INVALID_CODE", comment: "")

//Home Promos & Vouchers String
let kHomeBottomSheetDefaultFooterTitle = NSLocalizedString("HOME_BOTTOM_SHEET_FOOTER_TITLE", comment: "")
let kSelectOneCupon = NSLocalizedString("SELECT_ONE_COUPON", comment: "")

let kHomeBottomSheetDefaultHeaderTitle = NSLocalizedString("HOME_BOTTOM_SHEET_HEADER_TITLE", comment: "")
let kHomeBottomSheetDefaultHeaderTitleUser = NSLocalizedString("HOME_BOTTOM_SHEET_SPECIALLY_USER", comment: "")
let kDefaultLocation = NSLocalizedString("DEFAULT_LOCATION", comment: "")
let kDefaultLunch = NSLocalizedString("DEFAULT_LUNCH", comment: "")
let kUpdateLocationTitle = NSLocalizedString("UPDATE_LOCATION_TITLE", comment: "")
let kUpdateLocationMessage = NSLocalizedString("UPDATE_LOCATION_MESSAGE", comment: "")
let kAlertAllow = NSLocalizedString("ALERT_ALLOW", comment: "")
let kAlertDeny = NSLocalizedString("ALERT_DENY", comment: "")
let kLocationDisabledTitle = NSLocalizedString("LOCATION_DISABLED_TITLE", comment: "")
let kLocationDisabledMessage = NSLocalizedString("LOCATION_DISABLED_MESSAGE", comment: "")
let kOpenSettings = NSLocalizedString("OPEN_SETTINGS", comment: "")
let kTableNearYou = NSLocalizedString("TABLE_AVAILABLE_NEAR_YOU", comment: "")
let kHeaderTitleGuest = NSLocalizedString("HEADER_GUEST", comment: "")
let kShareSmileTitle = NSLocalizedString("SHARE_A_SMILE_TITLE", comment: "")
let kShareSmileTitleUser = NSLocalizedString("SHARE_A_SMILE_TITLE_USER", comment: "")
let kReserveButtonText = NSLocalizedString("RESERVE_BUTTON_TITLE", comment: "")

//Network Strings
let kSuccessNetworkMessage = NSLocalizedString("SUCCESS_NETWORK_MESSAGE", comment: "")
let kAuthErrorNetworkMessage = NSLocalizedString("AUTH_ERROR_NETWORK_MESSAGE", comment: "")
let kBadReqNetworkMessage = NSLocalizedString("BAD_REQ_ERROR_NETWORK_MESSAGE", comment: "")
let kOutdatedErrorNetworkMessage = NSLocalizedString("OUTDATED_ERROR_NETWORK_MESSAGE", comment: "")
let kFailedNetworkMessage = NSLocalizedString("FAILED_ERROR_NETWORK_MESSAGE", comment: "")
let kNoDataErrorNetworkMessage = NSLocalizedString("NO_DATA_ERROR_NETWORK_MESSAGE", comment: "")
let kDecodeErrorNetworkMessage = NSLocalizedString("DECODE_ERROR_NETWORK_MESSAGE", comment: "")

//Payment
let kAdvancePayHeaderTitle = NSLocalizedString("ADVANCE_PAY_HEADER_TITLE", comment: "")
let kMinimumPaySectionTitle = NSLocalizedString("MINIMUM_PAY_SECTION_TITLE", comment: "")
let kOtherPaySectionTitle = NSLocalizedString("OTHER_PAY_SECTION_TITLE", comment: "")
let kFullPaySectionTitle = NSLocalizedString("FULL_PAY_SECTION_TITLE", comment: "")
let kMinimumPaySectionUnselectedTitle = NSLocalizedString("MINIMUM_PAY_SECTION_UNSELECTED_TITLE", comment: "")
let kFullPaySectionUnselectedTitle = NSLocalizedString("FULL_PAY_SECTION_UNSELECTED_TITLE", comment: "")
let kRemarksPlaceHolder = NSLocalizedString("REMARKS_PALCEHOLDER_TITLE", comment: "")
let kPaymentButtonTitle = NSLocalizedString("PAYMENT_BUTTON_TITLE", comment: "")
let kRazorpayPaymentPageTitle = NSLocalizedString("PAYMENTPAGE_TITLE", comment: "")
let kRazorpayPaymentPageDescription = NSLocalizedString("PAYMENTPAGE_DESCRIPTION", comment: "")
let kRazorpayCrateOrderFailedDescription = NSLocalizedString("CREATE_ORDER_FAILED", comment: "")
let kPaymentConfirmationTitleText = NSLocalizedString("PAYMENT_CONFIRMATION_TITLE_TEXT", comment: "")
let kPaymentCancelTitleText = NSLocalizedString("PAYMENT_CANCEL_TITLE", comment: "")
let kPaymentCancelDescriptionText = NSLocalizedString("PAYMENT_CANCEL_DESCRIPTION", comment: "")
let kPaymentCancelYesText = NSLocalizedString("PAYMENT_CANCEL_YES", comment: "")
let kPaymentCancelNoText = NSLocalizedString("PAYMENT_CANCEL_NO", comment: "")
let kPaymentSuccessText = NSLocalizedString("PAYMENT_SUCCESSFUL_TITLE", comment: "")

// Profile
let kMobileNumber = NSLocalizedString("MOBILE_NUMBER", comment: "")
let kEmail = NSLocalizedString("EMAIL", comment: "")
let kBirthDay = NSLocalizedString("BIRTHDAY", comment: "")
let kAnniversary = NSLocalizedString("ANNIVESARY", comment: "")
let kNotifications = NSLocalizedString("NOTIFICATIONS", comment: "")
let kCorpEmail = NSLocalizedString("CORP_EMAIL", comment: "")
let kBBQPoints = NSLocalizedString("BBQ_POINTS", comment: "")
let kPoints = NSLocalizedString("POINTS", comment: "")
let kUserProfileFormatted = NSLocalizedString("USER_PROFILE", comment: "")
let kStringHi = NSLocalizedString("Hey", comment: "")
let kStringEdit = NSLocalizedString("EDIT", comment: "")
let kStringUpdate = NSLocalizedString("UPDATE", comment: "")
let kStringGuestProfile = NSLocalizedString("GUEST_PROFILE", comment: "")
let kStringGuest = NSLocalizedString("GUEST", comment: "")
let kSignupForPointsMessage = NSLocalizedString("SIGNUP_POINTS_MESSAGE", comment: "")
let kPersonalEmail = NSLocalizedString("PERSONAL_EMAIL", comment: "")
let kCompanyEmail = NSLocalizedString("COMPANY_EMAIL", comment: "")
let kAddEmailString = NSLocalizedString("ADD_EMAIL", comment: "")
let kPointsTitleLabelString = NSLocalizedString("POINTS_TITLE_LABEL_TEXT", comment: "")
let kSaveChanges = NSLocalizedString("SAVE_CHAANGES", comment: "")
let kInValidMobile = NSLocalizedString("VALID_PHONE_REQUIRED", comment: "")
let kAlreadyRegistredMobile = NSLocalizedString("MOBILE_ALREADY_REGISTERED", comment: "")


let kUpdateEmailTitle        = NSLocalizedString("UPDATE_EMAIL", comment: "")
let kUpdateMobileNumberTitle = NSLocalizedString("UPDATE_MOBILE", comment: "")

let kAddAddLocationPlaceholder  = NSLocalizedString("LOCATION_PLACEHOLDER", comment: "")
let kAddBirthdayPlaceholder    = NSLocalizedString("BIRTHDAY_PLACEHOLDER", comment: "")
let kAddAnniversaryPlaceholder = NSLocalizedString("ANNIVERSARY_PLACEHOLDER", comment: "")
let kAddAddMobilePlaceholder   = NSLocalizedString("MOBILE_PLACEHOLDER", comment: "")
let kAddEmailPlaceholder       = NSLocalizedString("EMAIL_PLACEHOLDER", comment: "")
let kAddProfileMobileNumber    = NSLocalizedString("ADD_MOBILE_PLACEHOLDER", comment: "")
let kOTPSentNotificvation      = NSLocalizedString("OTP_SENT_MOBILE_NOTIFICATION", comment: "")

// Photo Picker
let kTakePhoto    = NSLocalizedString("TAKE_PHOTO", comment: "")
let kCameraRoll   = NSLocalizedString("CAMERA_ROLL", comment: "")
let kPhotoLibrary = NSLocalizedString("PHOTO_LIBRARY", comment: "")
let kCancelString = NSLocalizedString("CANCEL", comment: "")


// Location Strings
let kPleaseSelectACity = NSLocalizedString("Please_Select_A_City",comment: "Please Select a City:")
let kPopularChoices = NSLocalizedString("Popular_Choices", comment: "Popular choices")
let kPleaseSelectAnOutlet = NSLocalizedString("Please_select_an_outlet", comment: "please select an outlet:")
let kSelectedCity = NSLocalizedString("Selected_city", comment: "Selected city:")
let kIcoSearch = NSLocalizedString("Ico_Search", comment: "icon search:")
let kIcoEdit = NSLocalizedString("Ico_Edit", comment: "icon_edit:")
let kSelectLocation = NSLocalizedString("SELECT_LOCATION", comment: "")
let kNoDataFound = NSLocalizedString("NO_DATA_FOUND", comment: "")

let kKolkata = NSLocalizedString("Kolkata", comment: "Kolkata")
let kBangalore = NSLocalizedString("Bangalore", comment: "Bangalore")
let kChennai = NSLocalizedString("Chennai", comment: "Chennai")
let kMumbai = NSLocalizedString("Mumbai", comment: "Mumbai")
let kNewDelhi = NSLocalizedString("New Delhi", comment: "New Delhi")
let kGoa = NSLocalizedString("Goa", comment: "Goa")

let kMGRoad = NSLocalizedString("MGRoad", comment: "MGRoad")
let kKormangla = NSLocalizedString("Kormangla", comment: "Kormangla")
let kBallygunge = NSLocalizedString("Ballygunge", comment: "Ballygunge")
let kEsplanade = NSLocalizedString("Esplanade", comment: "Esplanade")
let kWhiteField = NSLocalizedString("WhiteField", comment: "WhiteField")
let kJPNagar = NSLocalizedString("JPNagar", comment: "JPNagar")

// Booking confirmation screen strings
//Table confirmation
let kUberTitle = NSLocalizedString("BOOKING_UBER_TITLE", comment: "")
let kOlaTitle = NSLocalizedString("BOOKING_OLA_TITLE", comment: "")
let kAppleMapTitle = NSLocalizedString("BOOKING_APPLE_MAP_TITLE", comment: "")
let kGoogleMapTitle = NSLocalizedString("BOOKING_GOOGLE_MAP_TITLE", comment: "")
let kCabIndicatorTitle = NSLocalizedString("BOOKING_CAB_INDICATOR_TITLE", comment: "")
let kCabIndicatorDescriptionTitle = NSLocalizedString("BOOKING_CAB_INDICATOR_DESCRIPTION_TITLE", comment: "")
let kMapIndicatorTitle = NSLocalizedString("BOOKING_MAP_INDICATOR_TITLE", comment: "")
let kMapIndicatorDescriptionTitle = NSLocalizedString("BOOKING_MAP_INDICATOR_DESCRIPTION_TITLE", comment: "")
let kNonVegBuffetTitle = NSLocalizedString("BOOKING_NON_VEG_BUFFET_TITLE", comment: "")
let kVegBuffetTitle = NSLocalizedString("BOOKING_VEG_BUFFET_TITLE", comment: "")
let kKidBuffetTitle = NSLocalizedString("BOOKING_KIDS_BUFFET_TITLE", comment: "")
let kBBQNPointsRedeemed = NSLocalizedString("BOOKING_BBQN_POINTS_REDEEMED_TITLE", comment: "")
let kEstimatedTotal = NSLocalizedString("BOOKING_ESTIMATED_TOTAL_TITLE", comment: "")
let kAdvanceAmountPaid = NSLocalizedString("BOOKING_ADVANCE_AMOUNT_PAID_TITLE", comment: "")
//Upcoming Reservation
let kUpcomingReservationTitle = NSLocalizedString("BOOKING_UPCOMING_RESERVATION_TITLE", comment: "")
let kUpcomingReservationEventType = NSLocalizedString("BOOKING_UPCOMING_RESERVATION_EVENT_TYPE", comment: "")
let kTableConfirmationTitle = NSLocalizedString("BOOKING_TABLE_CONFIRMATION_TITLE", comment: "")
let kTableConfirmationRescheduleTitle = NSLocalizedString("RESCHEDULE_TABLE_CONFIRMATION_TITLE", comment: "")

//Booking Cancellation
let kReasonForCancellation = NSLocalizedString("REASON_FOR_CANCELLATION", comment: "")
let kModeOfRefund = NSLocalizedString("MODE_OF_REFUND", comment: "")
let kMsgBBQPoints = NSLocalizedString("MSG_BBQ_POINTS", comment: "")
let kMsgOriginModePayment = NSLocalizedString("MSG_ORIGINAL_MODE_PAYMENT", comment: "")
let kMsgReedemNextVisit = NSLocalizedString("DESC_REEDEM_NEXT_VISIT", comment: "")
let kMsgDeductionCharge = NSLocalizedString("DESC_DESUCTION_CHARGE", comment: "")
let kTitleCancelConfirm = NSLocalizedString("TITLE_CANCEL_CONFIRM", comment: "")
let kInfoCancelConfirm  = NSLocalizedString("DESCR_CANCEL_CONFIRM", comment: "")
let kCancellationRefund  = NSLocalizedString("CANCELLATION_REFUND_MESSAGE", comment: "")
let kInfoCancelSorry    = NSLocalizedString("CANCEL_SORRY", comment: "")
let kQuestionReschedule = NSLocalizedString("RESCHEDULE_QUESTION", comment: "")
let kStringReschedule   = NSLocalizedString("RESCHEDULE", comment: "")
let kProceedCancel      = NSLocalizedString("PROCEED_CANCEL", comment: "")
let kAmount      = NSLocalizedString("AMOUNT", comment: "")
let kRefundText = NSLocalizedString("REFUND_TEXT", comment: "")

// Alert/ Button
let kOkButtonTitle = NSLocalizedString("OK_BUTTON", comment: "")
let kFailedTitle = NSLocalizedString("FAILED_TITLE", comment: "")
let kCancelButton = NSLocalizedString("CANCEL_BUTTON", comment: "")
let kDoneButton = NSLocalizedString("DONE_BUTTON", comment: "")
let kFailed = NSLocalizedString("FAILED_BUTTON", comment: "")
let kLoginFailed = NSLocalizedString("LOGIN_FAILED", comment: "")
let kOkButton = NSLocalizedString("OK_BUTTON", comment: "")
let kMaxOtpTryOkayButton = NSLocalizedString("MAXTRY_OK_BUTTON", comment: "")

let kYesButton = NSLocalizedString("YES_BUTTON", comment: "")
let kNoButton = NSLocalizedString("NO_BUTTON", comment: "")
let kLogoutConfirmationTitle = NSLocalizedString("LOGOUT_CONFIRMATION_TITLE", comment: "")
let kLogoutConfirmationMessage = NSLocalizedString("LOGOUT_CONFIRMATION_MESSAGE", comment: "")

let kRegistrationFailed = NSLocalizedString("REGISTRATION_FAILED", comment: "")
let kRetry = NSLocalizedString("RETRY", comment: "")
let kAPIFailed = NSLocalizedString("API_FAILED", comment: "")
let kAPISuccess = NSLocalizedString("API_SUCCESS", comment: "")
let kAPIUpdateEmailSuccess  = NSLocalizedString("EMAIL_UPDATED", comment: "")
let kAPIUpdateMobileSuccess = NSLocalizedString("MOBILE_UPDATED", comment: "")
let kAPIUpdateUserSuccess = NSLocalizedString("USER_UPDATED", comment: "")
let kAPIUpdateEmailFailed = NSLocalizedString("EMAIL_API_FAILED", comment: "")
let kAPIUpdateMbileFailed = NSLocalizedString("MOBILE_API_FAILED", comment: "")
let kAPIUpdateUserFailed  = NSLocalizedString("USER_API_FAILED", comment: "")
let kNotificationSettingsTitle = NSLocalizedString("NOTIF_TITLE", comment: "")
let kNotificationSettingsDescription = NSLocalizedString("NOTIF_DESCRIPTION", comment: "")

//CorporateBooking
let kSendOtp = NSLocalizedString("SEND_OTP", comment: "")
let kResendOtp = NSLocalizedString("RESEND_OTP", comment: "")
let kCorporateOffersTitle = NSLocalizedString("COPRPORATE_OFFERS", comment: "")
let kIncorrectEmail = NSLocalizedString("INCORRECT_EMAIL", comment: "")
let kEmailNotVerified = NSLocalizedString("EMAIL_NOT_VERIFIED", comment: "")
let kInvalidCorporateEmailID = NSLocalizedString("INVALID_CORPORATE_EMAILID", comment: "")
let kNoVoucherAvailable = NSLocalizedString("NO_VOUCHERS_AVAILABLE", comment: "")
let kNoCouponsAvailable = NSLocalizedString("NO_COUPONS_AVAILABLE", comment: "")
let kNoCorpOffersAvailable = NSLocalizedString("NO_CORPOFFERS_AVAILABLE", comment: "")
let kInvalidCouponCode = NSLocalizedString("INVALID_COUPON_CODE", comment: "")
let kEnterValidCoupon = NSLocalizedString("ENTER_VALID_COUPON", comment: "")


//Special Instructions
let kSpecialInstructionHeaderTitle = NSLocalizedString("SPECIAL_INSTRUCTION_HEADER_TITLE", comment: "")
let kCelebratingTitle = NSLocalizedString("CELEBRATING_FOR", comment: "")
let kJoinedByTitle = NSLocalizedString("JOINED_BY", comment: "")
let kNoSpecialRequest = NSLocalizedString("NO_SPEICAL_REQUEST", comment: "")
let kSpecialInstructionsPopUpTitle =  NSLocalizedString("SPECIAL_INSTRUCTIONS_POPUP_MESSAGE_TITLE", comment: "")
let kSpecialInstructionsPopUpMessage =  NSLocalizedString("SPECIAL_INSTRUCTIONS_POPUP_MESSAGE_BODY", comment: "")


//Create Booking , Slots , Food Type , Menu Branch Buffet
let kReserveTitle = NSLocalizedString("RESERVING_TABLE_TITLE", comment: "Reserving a table at")
let kSelectTime = NSLocalizedString("SELECT_TIME_TITLE", comment: "select time")
let kTodayButton = NSLocalizedString("TODAY_BUTTON", comment: "Today")
let kTomorrowButton = NSLocalizedString("TOMORROW_BUTTON", comment: "Tomorrow")
let kPickaDateButton = NSLocalizedString("PICKADATE_BUTTON", comment: "Pick a date")
let kSubTotalTitle = NSLocalizedString("SUBTOTAL_TITLE", comment: "Sub Total")
let kGSTTitle = NSLocalizedString("GST_TITLE", comment: "GST")
let kServiceChargeTitle = NSLocalizedString("SERVICE_CHARGE_TITLE", comment: "Service Charges")
let kBBQNPointsButton = NSLocalizedString("BBQN_POINTS_BUTTON", comment: "")
let kCouponsTitle = NSLocalizedString("COUPONS_TITLE", comment: "Coupons")
let kVouchersTitle = NSLocalizedString("VOUCHERS_TITLE", comment: "Vouchers")
let kCouponsVouchersValueAddTitle = NSLocalizedString("COUPONS_VOUCHERS_VALUE_ADD_TITLE", comment: "ADD")
let kCouponsVouchersAddTitle = NSLocalizedString("COUPONS_VOUCHERS_ADD_TITLE", comment: "ADD")

let kCouponsVouchersValueRemoveTitle = NSLocalizedString("COUPONS_VOUCHERS_VALUE_REMOVE_TITLE", comment: "REMOVE")

let kEstimatedTotalTitle = NSLocalizedString("ESTIMATED_TOTAL_TITLE", comment: "Estimated Total")
let kEstimateConditonTitle = NSLocalizedString("ESTIMATE_CONDITION_TITLE", comment: "*estimated prices and subject to change. all prices in ")
let kReserveTableButton = NSLocalizedString("RESERVE_TABLE_BUTTON", comment: "Reserve Table")
let kRescheduleResetAllTitle = NSLocalizedString("RESCHEDULE_RESET_ALL_TITLE", comment: "")
let kRescheduleResetAllDesc = NSLocalizedString("RESCHEDULE_RESET_ALL_DESC", comment: "")

//Vouchers
let kHeaderTitleVouchers = NSLocalizedString("HEADER_TITLE_VOUCHERS", comment: "")
let kHeaderTitleVouchersUser = NSLocalizedString("HEADER_TITLE_VOUCHERS_USER", comment: "")
let kHeaderTitleCorporateOffers = NSLocalizedString("HEADER_TITLE_CORPORATE_OFFERS", comment: "")
let kHeaderTitleCoupans = NSLocalizedString("HEADER_TITLE_COUPANS", comment: "")
let kTaxInfoText = NSLocalizedString("TAX_INFO_TEXT", comment: "")
let kBuyNowButtonText = NSLocalizedString("BUY_NOW_BUTTON_TEXT", comment: "")
let kSingleItemText = NSLocalizedString("SINGLE_ITEM_TEXT", comment: "")
let kMultipleItemText = NSLocalizedString("MULTIPLE_ITEM_TEXT", comment: "")
let kVoucherTimingsText = NSLocalizedString("VOUCHER_DETAILS_TIMINGS_TEXT", comment: "")
let kVoucherApplicableText = NSLocalizedString("VOUCHER_DETAILS_APPLICABLE_TEXT", comment: "")
let kVoucherCancellationText = NSLocalizedString("VOUCHER_DETAILS_CANCELLATION_TEXT", comment: "")
let kVoucherValidityText = NSLocalizedString("VOUCHER_DETAILS_VALIDITY_TEXT", comment: "")
let kVoucherHowToText = NSLocalizedString("VOUCHER_DETAILS_HOWTO_TEXT", comment: "")
let kVoucherThingsText = NSLocalizedString("VOUCHER_DETAILS_THINGS_TEXT", comment: "")
let kVoucherDataUnavailable = NSLocalizedString("VOUCHER_DATA_NOT_AVAILABLE", comment: "")
let kVoucherTimingsUnavailable = NSLocalizedString("VOUCHER_TIMINGS_NOT_AVAILABLE", comment: "")
let kVoucherValidForText = NSLocalizedString("VOUCHER_VALID_FOR", comment: "")
let kVoucherParticularlyOnText = NSLocalizedString("VOUCHER_PARTICULAR_ON", comment: "")
let kVouchersAndCoupons = NSLocalizedString("COUPONS_VOUCHERS", comment: "")
let kkVouchersAndCouponsBottomText =  NSLocalizedString("COUPONS_VOUCHERS_BOTTOM_TEXT", comment: "")
let kCouponsForYouText =  NSLocalizedString("COUPONS_FOR_YOU_TEXT", comment: "")
let kCouponsForYouTextUser =  NSLocalizedString("COUPONS_FOR_YOU_TEXT_USER", comment: "")
let kVouchersForYouText =  NSLocalizedString("VOUCHERS_FOR_YOU_TEXT", comment: "")
let kVouchersForYouTextUser =  NSLocalizedString("VOUCHERS_FOR_YOU_TEXT_USER", comment: "")
let kVouchersDetailsUnavailableText =  NSLocalizedString("VOUCHERS_DETAIL_UNAVAILABLE_TEXT", comment: "")
let kMaximumAmountReached = NSLocalizedString("MAXIMUM_AMOUNT_REACHED", comment: "")
let kCannotAddMoreVouchers = NSLocalizedString("CANT_ADD_MORE_VOUCHERS", comment: "")
let kInfoTitle = NSLocalizedString("VOUCHER_INFO_TITLE", comment: "")
//Voucher detail
let kdescriptionText =  NSLocalizedString("DESCRIPTION", comment: "")

//Cart
let kMyCartText = NSLocalizedString("MY_CART_TEXT", comment: "")
let kMyCartPriceTermsText = NSLocalizedString("MY_CART_PRICE_TERMS_TEXT", comment: "")
let kMyCartPaymentButtonText = NSLocalizedString("MY_CART_PAYMENT_BUTTON_TEXT", comment: "")
let kMyCartSubTotalText = NSLocalizedString("MY_CART_SUB_TOTAL_TEXT", comment: "")
let kMyCartEstimatedTotalText = NSLocalizedString("MY_CART_ESTIMATED_TOTAL_TEXT", comment: "")
let kMyCartCGSTText = NSLocalizedString("MY_CART_CGST_TEXT", comment: "")
let kMyCartSGSTText = NSLocalizedString("MY_CART_SGST_TEXT", comment: "")
let kMyCartBBQNPointText = NSLocalizedString("MY_CART_BBQ_POINT_TEXT", comment: "")
let kMyCartCouponsText = NSLocalizedString("MY_CART_COUPONS_TEXT", comment: "")
let kMyCartAddCouponsText = NSLocalizedString("MY_CART_ADD_COUPONS_TEXT", comment: "")
let kMyCartRemoveCouponsText = NSLocalizedString("MY_CART_REMOVE_COUPONS_TEXT", comment: "")
let kMyCartNoItemsText = NSLocalizedString("MY_CART_NO_ITEMS_TEXT", comment: "")
let kMyCartTransactionCancelTitle = NSLocalizedString("MY_CART_TRANSACTION_CANCEL_TITLE", comment: "")
let kMyCartTransactionCancelMessage = NSLocalizedString("MY_CART_TRANSACTION_CANCEL_MESSAGE", comment: "")
let kMyCartTransactionFailedTitle = NSLocalizedString("MY_CART_TRANSACTION_FAILED_TITLE", comment: "")
let kMyCartTransactionFailedMessage = NSLocalizedString("MY_CART_TRANSACTION_FAILED_MESSAGE", comment: "")
let kMyCartZeroPointMessage = NSLocalizedString("MY_CART_ZERO_POINT_MESSAGE", comment: "")
let kMyCartRemovePointTitle = NSLocalizedString("MY_CART_REMOVE_POINTS_TITLE", comment: "")
let kMyCartRemovePointDesc = NSLocalizedString("MY_CART_REMOVE_POINTS_DESCRIPTION", comment: "")
let kMyCartRemoveCouponTitle = NSLocalizedString("MY_CART_REMOVE_COUPON_TITLE", comment: "")
let kMyCartRemoveCouponDesc = NSLocalizedString("MY_CART_REMOVE_COUPON_DESCRIPTION", comment: "")
let kEmptyCartWarning = NSLocalizedString("EMPTY_CART_WARNING", comment: "")

let kAddressRemoveAlert = NSLocalizedString("MY_Address_REMOVE_ALERT_TITLE", comment: "")
let kMyAddressRemoveDesc = NSLocalizedString("REMOVE_ADDRESS_ALERT_DESC", comment: "")

//About
let kTermsCondition = NSLocalizedString("TERMS_&_CONDITIONS", comment: "")
let kPrivacyPolicy = NSLocalizedString("PRIVACY_POLICY", comment: "")
let kAppVersion = NSLocalizedString("APP_VERSION", comment: "")
let kAbtBbqNation = NSLocalizedString("ABOUT_BBQ_NATION", comment: "")
let kBuildLabel = NSLocalizedString("BUILD_LABEL", comment: "")
let kVersionLabel = NSLocalizedString("VERSION_LABEL", comment: "")

//Loyalty
let kPointsEarnedText = NSLocalizedString("POINTS_EARNED_TEXT", comment: "")
let kPointsSpentText = NSLocalizedString("POINTS_SPENT_TEXT", comment: "")
let kAvailablePointsText = NSLocalizedString("AVAILABLE_POINT_TEXT", comment: "")
let KReferAndEarnText = NSLocalizedString("REFER_AND_EARN", comment: "")
let KGoldMemberText = NSLocalizedString("GOLD_MEMBER", comment: "")
let KLoyaltyLoginText = NSLocalizedString("LOYALTY_LOGIN_MESSAGE", comment: "")

// Notifications
let kNoNewNotifications = NSLocalizedString("NO_NEW_NOTIFICATION", comment: "")

//Refer and Earn
let kReferFriends = NSLocalizedString("REFER_FRIENDS", comment: "")
let kReferralCode = NSLocalizedString("REFERRAL_CODE", comment: "")
let kTermsNConditions = NSLocalizedString("TERMSNCONDITIONS", comment: "")
let kInviteAFriend = NSLocalizedString("INVITE_A_FRIEND", comment: "")


//Outlet Info
let kAminitiesTitle = NSLocalizedString("OUTLET_INFO_AMINITIES_TITLE", comment: "")
let kAddressTitle = NSLocalizedString("OUTLET_INFO_ADDRESS_TITLE", comment: "")
let kOutletOpenStatusTitle = NSLocalizedString("OUTLET_INFO_OPEN_STATUS_TITLE", comment: "")
let kMenuTitle = NSLocalizedString("OUTLET_INFO_MENU_TITLE", comment: "")
let kOutletTitle = NSLocalizedString("OUTLET_INFO_OUTLET_TITLE", comment: "")
let kPromoAndOfferTitle = NSLocalizedString("OUTLET_INFO_PROMO_OFFER_TITLE", comment: "")
let kPromoAndOfferTitleUser = NSLocalizedString("OUTLET_INFO_SPECIALLY_USER", comment: "")
let kNoMenuAvailableText = NSLocalizedString("OUTLET_INFO_NO_MENU_AVALABLE_TEXT", comment: "")
let kNoAmbienceAvailableText = NSLocalizedString("OUTLET_INFO_NO_AMBIENCE_AVALABLE_TEXT", comment: "")
let kNoAminitiesAvailableText = NSLocalizedString("OUTLET_INFO_NO_AMINITIES_AVALABLE_TEXT", comment: "")

// Feedback & Rating
let kRatingImprove = NSLocalizedString("RATING_IMPROVE_TITLE", comment: "")
let kRatingExxperience = NSLocalizedString("RATING_EXP_TITLE", comment: "")
let kButtonOkay = NSLocalizedString("BUTTON_OK", comment: "")
let kButtonNotNow = NSLocalizedString("BUTTON_NOTNOW", comment: "")
let kButtonSkip = NSLocalizedString("BUTTON_SKIP", comment: "")
let kRatingImproveDesc = NSLocalizedString("RATING_IMPROVE_DESC", comment: "")
let kRatingAppstore = NSLocalizedString("RATE_APPSTORE", comment: "")
let kRatingThankYouTitle = NSLocalizedString("RATING_THANKYOU_TITLE", comment: "")
let kFeedbackSuccessful = NSLocalizedString("FEEDBACK_SUCCESS_MESSAGE", comment: "")

// After Dining
let kDiningPayAmount = NSLocalizedString("DINING_PAY_AMOUNT", comment: "")
let kDiningSubTotal = NSLocalizedString("DINING_SUBTOTAL", comment: "")
let kDiningGST = NSLocalizedString("DINING_GST", comment: "")
let kDiningServiceCharge = NSLocalizedString("SERVICE_CHARGE", comment: "")
let kDiningTaxServiceCharge = NSLocalizedString("TAX_SERVICE_CHARGE", comment: "")
let kDiningCoupon = NSLocalizedString("DINING_COUPONS", comment: "")
let kDiningVouchers = NSLocalizedString("DINING_VOUCHERS", comment: "")
let kDiningCorpOffers = NSLocalizedString("DINING_CORP_OFFERS", comment: "")
let kDiningCurrencyDesc = NSLocalizedString("DINING_CURR_DESC", comment: "")

//My Transaction
let kMyTransactionCheckingCancelledStatus = NSLocalizedString("MY_TRANSACTION_CANCELLED_STATUS", comment: "")
let kMyTransactionCheckingHistoryStatus = NSLocalizedString("MY_TRANSACTION_HISTORY_STATUS", comment: "")
let kMyTransactionCheckingUpcomingReservationStatus = NSLocalizedString("MY_TRANSACTION_UPCOMING_RESERVATION_STATUS", comment: "")
let kMyTransactionCheckingNoShowStatus = NSLocalizedString("MY_TRANSACTION_NO_SHOW_STATUS", comment: "")
let kMyTransactionCheckingCancelTextLabel = NSLocalizedString("MY_TRANSACTION_CANCELLED_STATUS_TEXT_LABEL", comment: "")
let kMyTransactionCheckingHistoryTextLabel = NSLocalizedString("MY_TRANSACTION_HISTORY_STATUS_TEXT_LABEL", comment: "")
let kMyTransactionReservationButtonTitle = NSLocalizedString("MY_TRANSACTION_RESERVATIONS_BUTTON_TITLE", comment: "")
let kMyTransactionVoucherButtonTitle = NSLocalizedString("MY_TRANSACTION_VOUCHERS_BUTTON_TITLE", comment: "")
let kMyTransactionRefundStatusText = NSLocalizedString("MY_TRANSACTION_REFUND_STATUS_TITLE", comment: "")
let kMyTransactionCheckingPreReceiptTextLabel = NSLocalizedString("MY_TRANSACTION_PRE_RECEIPT_STATUS_TEXT_LABEL", comment: "")
let kMyTransactionCheckingPreReceiptStatus = NSLocalizedString("MY_TRANSACTION_PRE_RECEIPT_STATUS", comment: "")
let kMyTransactionPaymentRedirectionMessage = NSLocalizedString("MY_TRANSACTION_PAYMENT_REDIRECTION_MESSAGE", comment: "")
let kMyTransactionAdvanceAmountTitle = NSLocalizedString("MY_TRANSACTION_ADVANCE_AMOUNT_TITLE", comment: "")

//Feedback
let kNoTasteData = NSLocalizedString("NO_Taste_Data_Message", comment: "")
let kNoPackageData = NSLocalizedString("NO_Package_Data_Message", comment: "")
let kNoDeliveryData = NSLocalizedString("NO_Delivery_Data_Message", comment: "")
let kFoodRated = NSLocalizedString("Food_Rated_Message", comment: "")







