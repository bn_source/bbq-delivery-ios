//
//  BottomSheetController.swift
//  BottomSheet
//
//  Created by Chandan Singh on 25/07/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

/// `BottomSheetController` is an object that can be used to present bottom sheets.
class BottomSheetController: NSObject {
    var isFromBookingScreen = false
    /**
     Initializes a `BottomSheetController` object with a configuration.
     
     - parameter configuration: The configuration struct that specifies how BottomSheet should be configured.
     */
    public init(configuration: BottomSheetConfiguration? = nil) {
        if let configuration = configuration {
            SharedConfiguration.shared = configuration
        }
        
        super.init()
    }
    
    /**
     Presents a bottom sheet view controller embedded in a navigation controller.
     
     - parameter viewController:          The presented view controller.
     - parameter containerViewController: The presenting view controller.
     */
    public func present(_ viewController: UIViewController, on containerViewController: UIViewController, viewHeight: CGFloat? = nil) {
        if viewController is UINavigationController {
            assertionFailure("Presenting 'UINavigationController' in a bottom sheet is not supported.")
            return
        }
        
        let width: CGFloat = UIScreen.main.bounds.width
        let maxHeight: CGFloat = UIScreen.main.bounds.height - SharedConfiguration.shared.minimumTopSpacing
        var height: CGFloat = maxHeight
        
        viewController.view.setNeedsLayout()
        viewController.view.layoutIfNeeded()
        let size = viewController.view.systemLayoutSizeFitting(CGSize(width: width, height: maxHeight))
        
        height = size.height <= maxHeight ? size.height : maxHeight
        
        // Increase height (only useful for the iPhone X for now)
        if #available(iOS 11.0, *) {
            guard let window = UIApplication.shared.keyWindow else {
                return
            }
            
            let bottomPadding = window.safeAreaInsets.bottom
            if height + bottomPadding <= maxHeight {
                height += bottomPadding
            }
        }
        if let viewHeight = viewHeight{
            height = viewHeight
        }
        let positionY = UIScreen.main.bounds.height - height
        viewController.view.frame = CGRect(x: 0.0, y: positionY, width: width, height: height)
        
        let bottomSheetTransitioningDelegate = BottomSheetTransitioningDelegate()
        viewController.transitioningDelegate = bottomSheetTransitioningDelegate
        viewController.modalPresentationStyle = .custom
        if let rootVC = UIApplication.shared.keyWindow?.rootViewController, isFromBookingScreen{
            rootVC.present(viewController, animated: true, completion: nil)
        }else{
            containerViewController.present(viewController, animated: true, completion: nil)
        }
        
    }
    
    func expandOrCollapse(viewController: UIViewController, withHeight: CGFloat, isExpand: Bool) {
        var vieHeight = viewController.view.frame.size.height
        
        UIView.animate(withDuration: 0.2) {
            if isExpand {
                let maxYPosition = UIScreen.main.bounds.height - (0.8 * UIScreen.main.bounds.height)
                var calculatedYPosition = viewController.view.frame.origin.y - withHeight
                //Limiting the expansion maximum to 80% of the screen size
                if calculatedYPosition < maxYPosition {
                    calculatedYPosition = maxYPosition
                    vieHeight += viewController.view.frame.origin.y - calculatedYPosition
                } else {
                    vieHeight += withHeight
                }
                viewController.view.frame = CGRect(x: viewController.view.frame.origin.x, y: calculatedYPosition, width: viewController.view.frame.size.width, height: vieHeight)
            } else {
                vieHeight -= withHeight
                viewController.view.frame = CGRect(x: viewController.view.frame.origin.x, y: viewController.view.frame.origin.y + withHeight, width: viewController.view.frame.size.width, height: vieHeight)
            }
            viewController.view.setNeedsLayout()
            viewController.view.layoutIfNeeded()
        }
    }
    
    func chanceTheHeight(viewController: UIViewController, withHeight: CGFloat) {
        var vieHeight = viewController.view.frame.size.height
        
        UIView.animate(withDuration: 0.2) {
            let maxYPosition = UIScreen.main.bounds.height - (0.8 * UIScreen.main.bounds.height)
            var calculatedYPosition = (UIApplication.shared.keyWindow?.frame.size.height ?? 0) - withHeight
            //Limiting the expansion maximum to 80% of the screen size
            if calculatedYPosition < maxYPosition {
                calculatedYPosition = maxYPosition
                vieHeight = (UIApplication.shared.keyWindow?.frame.size.height ?? 0) - calculatedYPosition
            } else {
                vieHeight = withHeight
            }
            viewController.view.frame = CGRect(x: viewController.view.frame.origin.x, y: calculatedYPosition, width: viewController.view.frame.size.width, height: vieHeight)
            
            viewController.view.setNeedsLayout()
            viewController.view.layoutIfNeeded()
        }
    }
    
    func dismissBottomSheet(on containerViewController: UIViewController, completion: ((_ success: Bool) -> Void)? = nil) {
        var rootVC = UIApplication.shared.keyWindow?.rootViewController ?? containerViewController
        if !isFromBookingScreen{
            rootVC = containerViewController
        }
        
        DispatchQueue.main.async {
            rootVC.dismiss(animated: true, completion: {
                if let completionHandler = completion {
                    completionHandler(true)
                }
            })
        }
    }
}
