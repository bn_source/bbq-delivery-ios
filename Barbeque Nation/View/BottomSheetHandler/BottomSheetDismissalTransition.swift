//
//  BottomSheetDismissalTransition.swift
//  BottomSheet
//
//  Created by Chandan Singh on 25/07/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

class BottomSheetDismissalTransition: NSObject, UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return SharedConfiguration.shared.animationDuration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
        
        let animationDuration = transitionDuration(using: transitionContext)
        
        UIView.animate(
            withDuration: animationDuration,
            delay: 0.0,
            usingSpringWithDamping: 1.0,
            initialSpringVelocity: 0.8,
            options: UIView.AnimationOptions.curveEaseOut,
            animations: {
                fromViewController.view.transform = CGAffineTransform(translationX: 0, y: fromViewController.view.frame.height)
        }, completion: { _ in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
        )
    }
}
