//
//  SharedConfiguration.swift
//  BottomSheet
//
//  Created by Chandan Singh on 25/07/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import Foundation

// Internal configuration object.
struct SharedConfiguration {
    
    // Singleton
    static var shared: BottomSheetConfiguration = BottomSheetConfiguration()
}
