//
//  BottomSheetConfiguration.swift
//  BottomSheet
//
//  Created by Chandan Singh on 25/07/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

/// Encapsulates configuration information for the behavior of BottomSheet.
struct BottomSheetConfiguration {
    
    /// The presentation and dismissal animation duration.
    var animationDuration: TimeInterval = 0.0
    
    /// The minimum spacing between the bottom sheet and the top of the screen.
    var minimumTopSpacing: CGFloat = 0.0
    
    /// The default dismissal of the view
    var defaultDismiss: Bool = true
    
    /// The default presentation and dismissal animation duration.
    static let defaultAnimationDuration: TimeInterval = 0.4
    
    /// The default minimum spacing between the bottom sheet and the top of the screen.
    static let defaultMinimumTopSpacing: CGFloat = 260.0
    
    /// The default dismissal of the view
    static let defaultTouchOrSwipeDismiss: Bool = true
    
    /**
     Initializes a `BottomSheetConfiguration` object with optionally customizable behaviors.
     
     - parameter animationDuration: The presentation and dismissal animation duration.
     - parameter minimumTopSpacing: The minimum spacing between the bottom sheet and the top of the screen.
     */
    init(animationDuration: TimeInterval = defaultAnimationDuration,
         minimumTopSpacing: CGFloat = defaultMinimumTopSpacing, touchDismiss: Bool = defaultTouchOrSwipeDismiss) {
        self.animationDuration = animationDuration
        self.minimumTopSpacing = minimumTopSpacing
        self.defaultDismiss = touchDismiss
    }
}
