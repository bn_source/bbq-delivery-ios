//
 //  Created by Arpana on 09/01/23
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2023.
 //  All rights reserved.
 //  Last modified BBQItemInCart.swift
 //

import UIKit


protocol ViewCartDelegate {

func viewCartForAddedItem()
}

class BBQItemInCart: UIView {
    
    let nibName = "BBQItemInCart"
    @IBOutlet weak var colorView : UIView!
    @IBOutlet weak var lblItem : UILabel!
    @IBOutlet weak var lblItemCharge : UILabel!
    @IBOutlet weak var btn: UIButton!
    var delegetToViewCart : ViewCartDelegate?

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        self.setShadow()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
        self.setShadow()
        self.colorView.setCornerRadius(8.0 )
    }
    
    
    func setBackGroundColor(color: UIColor){
        
        self.colorView.backgroundColor = color
        self.layoutIfNeeded()
//        lblItem.textColor = .deliveryThemeTextColor
//        lblItemCharge.textColor = .deliveryThemeTextColor
    }
    
    
    func commonInit() {
        let bundle = Bundle.init(for: BBQItemInCart.self)
        if let viewsToAdd = bundle.loadNibNamed(nibName, owner: self, options: nil), let contentView = viewsToAdd.first as? UIView {
            addSubview(contentView)
            contentView.frame = self.bounds
            contentView.autoresizingMask = [.flexibleHeight,
                                            .flexibleWidth]
            
            updateCartItems(amount: 0)
            
            
        }
    }
    
    func updateCartItems(amount : Int){
        
        
        if DeliveryTabBarController.cartItemCount == 1 {
            
            self.lblItem.text = String(format: "%d Item | %@%d",DeliveryTabBarController.cartItemCount ,getCurrency(), DeliveryTabBarController.cartItemPrice)
            return
        }
        self.lblItem.text = String(format: "%d Items | %@%d ",DeliveryTabBarController.cartItemCount , getCurrency() ,DeliveryTabBarController.cartItemPrice)
        
    }
    
    
    
    @IBAction func viewCartBtnPressed(_ sender : UIButton){
                       
        if delegetToViewCart != nil {
                    delegetToViewCart?.viewCartForAddedItem()
                }
    }
    
}
