//
//  CountryTableViewCell.swift
//  location
//
//  Created by Abhijit on 12/08/19.
//  Copyright © 2019 Abhijit. All rights reserved.
//

import UIKit

class CountryTableViewCell: UITableViewCell {

    @IBOutlet weak var countryLabel: UILabel!
}
