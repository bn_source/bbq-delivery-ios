//
//  BBQTabbarController.swift
//  Barbeque Nation
//
//  Created by Maya R on 30/07/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

protocol BBQTabbarControllerProtocol: AnyObject {
    func middleButtonAction(actionSender:UIButton)
}

class BBQTabbarController: UITabBarController {
    var middleBtn = UIButton()
    
    weak var tabDelegate: BBQTabbarControllerProtocol? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupMiddleButton()
        self.selectedIndex = 1
    }
    
    //mark - setupMiddleButton
    func setupMiddleButton() {
        middleBtn = UIButton(frame: CGRect(x: (self.view.bounds.width / 2)-32, y: -32, width: 64, height: 64))
        //STYLE THE BUTTON YOUR OWN WAY
        middleBtn.backgroundColor = UIColor(red: 255/255.0, green: 99/255.0, blue: 1/255.0, alpha: 1.0)
        middleBtn.setTitle("Reserve", for: .normal)
        middleBtn.layer.cornerRadius = 32
        middleBtn.layer.borderWidth = 0.5
        middleBtn.layer.borderColor = UIColor.blue.cgColor
        middleBtn.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.bold)
        middleBtn.addTarget(self, action: #selector(self.menuButtonAction), for: .touchUpInside)
        self.tabBar.addSubview(middleBtn)
        self.tabBar.bringSubviewToFront(middleBtn)
        self.view.layoutIfNeeded()
    }
    
    //mark - menuButtonAction
    @objc func menuButtonAction(sender: UIButton) {
        self.selectedIndex = 1
        guard let delegate = self.tabDelegate else {
            return
        }
        delegate.middleButtonAction(actionSender: sender)
        
        
    }
    
    
}
