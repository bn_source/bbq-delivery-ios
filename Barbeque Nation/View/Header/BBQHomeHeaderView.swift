//
//  BBQHomeHeaderView.swift
//  Barbeque Nation
//
//  Created by Maya R on 24/07/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

protocol BBQHomeHeaderViewProtocol :AnyObject{
    func hamburgerClicked(actionSender: UIButton)
    func notificationClicked(actionSender: UIButton)
    func mapClicked(actionSender: UIButton)
    func cartButtonClicked()
    func pointsClicked(actionSender: UIButton)
}

class BBQHomeHeaderView: UIView {
    // MARK: IBOutlet
    @IBOutlet var containerView: UIView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet weak var notificationButton:UIButton!
    //@IBOutlet weak var locationButton: UIButton?
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var cartButton: UIButton!
    @IBOutlet weak var pointsButton: UIButton!
    @IBOutlet weak var hamburgerButton: UIButton!
    //@IBOutlet weak var cartButtonTopContraint: NSLayoutConstraint!
    //@IBOutlet weak var goldMemberLeadingContraint: NSLayoutConstraint!
    @IBOutlet weak var goldMemberButton:UIButton!

    
    let nibName = "BBQHomeHeaderView"
    weak var homeHeaderDelegate: BBQHomeHeaderViewProtocol? = nil
    let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
    // MARK: Life Cycle Methods
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
        addTheInitialValues()
    }
    
    // MARK: commonInit
    private func commonInit(){
        Bundle.main.loadNibNamed(nibName, owner: self, options: nil)
        addSubview(containerView!)
        containerView?.frame = self.bounds
        containerView?.autoresizingMask = .flexibleWidth
        //locationButton?.roundCorners(cornerRadius: (locationButton?.frame.size.height)!/2, borderColor: .clear, borderWidth: 0.0)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.receivedNotification),
            name: NSNotification.Name(rawValue: Constants.App.NotificationPage.receiveNotification),
            object: nil)
    }
    
    @objc func receivedNotification(notification: NSNotification){
            if let checkNotification = UserDefaults.standard.value(forKey: Constants.App.NotificationPage.notificationActive) as? Bool {
                if checkNotification == true {
                    self.notificationButton.setImage(UIImage(named: Constants.AssetName.notificationActive), for: .normal)
                }else{
                    self.notificationButton.setImage(UIImage(named: Constants.AssetName.notification), for: .normal)
                }
            }
    }
    
    // MARK: addTheInitialValues
    func addTheInitialValues() {
        self.configureNotificationUI()
        self.configureNameUI()
        self.configureBBQPointUI()
        // self.goldMemberButton.setTitle(KGoldMemberText, for: .normal)
        self.goldMemberButton.isHidden = true
        self.goldMemberButton.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0.0)
    }
    
    private func configureNotificationUI() {
        if let checkNotification = UserDefaults.standard.value(forKey: Constants.App.NotificationPage.notificationActive) as? Bool {
            if checkNotification == true {
                self.notificationButton.setImage(UIImage(named: Constants.AssetName.notificationActive), for: .normal)
            }else{
                self.notificationButton.setImage(UIImage(named: Constants.AssetName.notification), for: .normal)
            }
        }
    }
    
     func configureNameUI() {
        var userNameString = ""
        if BBQUserDefaults.sharedInstance.UserName.count > 0 {
            if let firstName = BBQUserDefaults.sharedInstance.UserName.components(separatedBy: " ").first {
                let test = String(firstName.filter { !" \n".contains($0) })
                userNameString = test
            }
        } else {
            userNameString = kHeaderTitleGuest
        }
        /*let complete_text = "\(kStringHi) \(userNameString),"
        let nameText = userNameString
        let longestWordRange = (complete_text as NSString).range(of: nameText)
        let attributedString1 = NSMutableAttributedString(string:complete_text)
        attributedString1.addAttribute(NSAttributedString.Key.font, value: UIFont.appThemeBoldWith(size:16.0) as Any, range: longestWordRange)
        attributedString1.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black , range: longestWordRange)
        self.nameLabel.attributedText = attributedString1*/
        let complete_text = "Hello, \(userNameString) !!!"
        self.nameLabel.text = complete_text
    }
    
    func configureBBQPointUI() {
        let userLoyaltyPoints = BBQUserDefaults.sharedInstance.UserLoyaltyPoints
        let points_string = " \(kMsgBBQPoints)  \(userLoyaltyPoints)"
        let string_to_color = String(userLoyaltyPoints)
        let range = (points_string as NSString).range(of: string_to_color)
        let mainStringRange = (points_string as NSString).range(of: kMsgBBQPoints)
        let attributedString = NSMutableAttributedString(string: points_string)
        let attribute = [NSAttributedString.Key.font: UIFont.appThemeRegularWith(size: 12.0),
                         NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 0.3882352941, blue: 0.003921568627, alpha: 1)] as [NSAttributedString.Key : Any]
        attributedString.addAttributes(attribute, range: mainStringRange)
        
        let pointAttribute = [NSAttributedString.Key.font: UIFont.appThemeSemiBoldWith(size: 12.0),
                              NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 0.3882352941, blue: 0.003921568627, alpha: 1)] as [NSAttributedString.Key : Any]
        attributedString.addAttributes(pointAttribute, range: range)
        
        let imageAttachment = NSTextAttachment()
        imageAttachment.image = UIImage(named: "smiles_coins_logo")
        let iconsSize = CGRect(x: 0, y: -8, width: 28, height: 28)
        imageAttachment.bounds = iconsSize
        
        // wrap the attachment in its own attributed string so we can append it
        let imageString = NSMutableAttributedString(attachment: imageAttachment)
        imageString.append(attributedString)
        self.pointsButton.setAttributedTitle(imageString, for: .normal)
        pointsButton.layer.cornerRadius = 5
        pointsButton.setShadow()
    }
    
    // MARK: - hamburgerButtonClicked action

    @IBAction func hamburgerButtonClicked(_ sender: UIButton) {
        guard let hamDelegate = self.homeHeaderDelegate else {
            return
        }
        hamDelegate.hamburgerClicked(actionSender: sender)
        
    }
    
    // MARK: - notificationClicked action
    @IBAction func notificationClicked(_ sender: UIButton) {
        guard let notfDelegate = self.homeHeaderDelegate else {
            return
        }
        notfDelegate.notificationClicked(actionSender: sender)
    }
    
    // MARK: - locationCliked action
    @IBAction func locationCliked(_ sender: UIButton) {
        guard let mapDelegate = self.homeHeaderDelegate else {
            return
        }
        mapDelegate.mapClicked(actionSender: sender)
    }
    
    // MARK: - Loyality Points action
    @IBAction func pointsCliked(_ sender: UIButton) {
        guard let pointsDelegate = self.homeHeaderDelegate else {
            return
        }
        pointsDelegate.pointsClicked(actionSender: sender)
    }
    
    // MARK: - Cart Button Action
    @IBAction func cartButtonPressed(_ sender: UIButton) {
        guard let delegate = self.homeHeaderDelegate else {
            return
        }
        delegate.cartButtonClicked()
    }
    
    func setCartButtonIcon(dataAvailable: Bool) {
        if dataAvailable {
            cartButton.setImage(UIImage(named: Constants.AssetName.cartFilledIcon), for: .normal)
            //cartButtonTopContraint.constant = 4
        } else {
            cartButton.setImage(UIImage(named: Constants.AssetName.cartEmptyIcon), for: .normal)
            //cartButtonTopContraint.constant = 5
        }
        //layoutIfNeeded()
    }
}
