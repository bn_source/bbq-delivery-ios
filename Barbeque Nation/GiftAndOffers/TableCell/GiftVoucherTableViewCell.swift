//
 //  Created by Arpana on 15/12/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified GiftVoucherTableViewCell.swift
 //

import UIKit

class GiftVoucherTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var deactivatedViewForContainer: UIView!
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var viewForData: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var  expireStackView : UIStackView!
    @IBOutlet weak var btnTermAndCondition: UIButton!
    @IBOutlet weak var applyBtn: UIButton!
    var voucher: Coupon!
    var appliedCouoons = [ListTender]()
    var barcode = ""

    var indexPath: IndexPath!
    var delegate : DeliveryCouponCellDelegate?
    var amount = 0

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setTheme()
    }
    
    private func setTheme(){
        applyBtn.titleLabel?.font = UIFont(name: Constants.App.BBQAppFont.ThemeMedium, size: 11.0)
        applyBtn.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0.0)
        viewForContainer.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
        // viewForData.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
        viewForData.layer.maskedCorners = [ .layerMinXMaxYCorner, .layerMinXMaxYCorner]
        viewForData.clipsToBounds = true
        viewForData.layer.cornerRadius = 10
                
    }
    
    
    
    func setCellData(voucher: Coupon , indexPath: IndexPath) {
        expireStackView.isHidden = true
        self.voucher = voucher
        self.indexPath = indexPath
        lblName.text = voucher.title
        lblAmount.text = getCurrency() + String(voucher.denomination ?? 0)
        self.barcode = voucher.barcode
        self.amount = voucher.denomination
        expireStackView.isHidden = false
        lblDateTime.text = (voucher.validity ?? "")

    }
    

    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    private func getStatus() -> (String?, UIColor, Bool, Bool){
        if voucher?.status == "Active"{
            return ("Expires on \(voucher?.validity ?? "")", .themeYello, true, true)
        }else if voucher?.status == "Redeemed" || voucher?.status == "Gifted"{
            return (voucher?.status, UIColor.hexStringToUIColor(hex: "74342D"), false, false)
        }else if voucher?.status == "In-Active" || voucher?.status == "Expired"{
            return (voucher?.status, UIColor.hexStringToUIColor(hex: "74342D"), false, false)
        }
        return ("", .theme, false, false)
    }
    
    @IBAction func actionOnApplyBtn(_ sender: Any) {
        //if !used_in_mobile_app {
      
            delegate?.actionOnApplyBtn(code: barcode, amount: amount, tenderKey: "VOUCHER")
        }
        //}
    
    @IBAction func clickTermsAndCondition(_ sender: Any) {

            delegate?.actionOnTermsBtn(code: barcode,  tenderKey:   "VOUCHER", index: indexPath)
        
    }


    
    @IBAction func onClickBtnViewDetails(_ sender: Any) {
      //  delegate?.onClickBtnDetails(coupon: coupon, isEnabled: applyBtn.isEnabled, isApplied: !applyBtn.isUserInteractionEnabled)
    }
    

    
    private func checkForAppliedVoucher(coupon: Coupon) -> Bool{
        for voucher in appliedCouoons{
            if coupon.barcode == voucher.promocode{
                return true
            }
        }
        return false
    }
}


