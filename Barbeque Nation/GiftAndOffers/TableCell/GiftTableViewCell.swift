//
 //  Created by Arpana on 20/04/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified GiftTableViewCell.swift
 //

import UIKit

class GiftTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var imgBackground: UIImageView!
  //viewForData  @IBOutlet weak var viewForData: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var lblBarcode: UILabel!
    @IBOutlet weak var couponDescription: UILabel!
    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var lblDeliveryDineIn: UILabel!
    @IBOutlet weak var applyBtn: UIButton!
    weak var delegate: DeliveryCouponCellDelegate?
    var barcode = ""
    var tenderKey = ""
    var amount = 0
    var used_in_mobile_app = false
    var coupon: Coupon!
    var appliedCouoons = [ListTender]()
    
    var voucher: Vouchers!
    var indexPath: IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setTheme()
    }
    
    private func setTheme(){
        applyBtn.titleLabel?.font = UIFont(name: Constants.App.BBQAppFont.ThemeMedium, size: 11.0)
        applyBtn.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0.0)
        viewForContainer.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
        // viewForData.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
//        viewForData.layer.maskedCorners = [ .layerMinXMaxYCorner, .layerMinXMaxYCorner]
//        viewForData.clipsToBounds = true
//        viewForData.layer.cornerRadius = 10
        
        // viewForStatus.roundCorners(cornerRadius: 4.0, borderColor: .clear, borderWidth: 0.0)
        
    }
    
    func setUI() {
        
        let paddedWidth = lblDeliveryDineIn.intrinsicContentSize.width + 2 * 5
        let paddedHeight = lblDeliveryDineIn.intrinsicContentSize.height + 2 * 5

        lblDeliveryDineIn.widthAnchor.constraint(equalToConstant: paddedWidth).isActive = true
        lblDeliveryDineIn.heightAnchor.constraint(equalToConstant: paddedHeight).isActive = true
        lblDeliveryDineIn.layer.cornerRadius = 10
        lblDeliveryDineIn.layer.masksToBounds = true

    }
    
    
    func  setCellData(data: Coupon, transaction_type: String, order_type : String  ) {
        
        
        self.lblname.text = data.title
        self.couponDescription.text = data.couponDescription 
//        //   self.code.text = data.barcode
        self.priceLbl.text = "\(data.denomination)/-"
        
        if data.transaction_type == "PERCENT"{
            priceLbl.text = String(format: "%li%% off", data.denomination)
        }else{
            priceLbl.text = getCurrency() + String(data.denomination )
        }
        self.barcode = data.barcode
        self.lblBarcode.text = data.barcode
        self.amount = data.denomination
        self.tenderKey = data.voucher_type
        self.used_in_mobile_app = data.used_in_mobile_app
        self.coupon = data
       // self.applyBtn.applyMultiBrandTheme()
        if data.sales_sub_type != "" && (data.sales_sub_type != order_type && data.sales_sub_type != transaction_type){
            self.applyBtn.isEnabled = false
            self.applyBtn.alpha = 0.5
        }else{
            self.applyBtn.isEnabled = true
            self.applyBtn.alpha = 1.0
        }
        if checkForAppliedVoucher(coupon: data){
            self.applyBtn.setTitleColor(.white, for: .normal)
            self.applyBtn.backgroundColor = .darkGreen
            self.applyBtn.setTitle("APPLIED", for: .normal)
            self.applyBtn.isUserInteractionEnabled = false
        }else{
            self.applyBtn.setTitle("APPLY", for: .normal)
            self.applyBtn.isUserInteractionEnabled = true
        }
    }
    
    @IBAction func onClickBtnGift(_ sender: Any) {
        // delegate?.onClickGiftVoucher(indexPath: indexPath)
    }
    
    private func getStatus() -> (String?, UIColor, Bool, Bool){
        if voucher.status == "Active"{
            return ("Expires on \(voucher.validity ?? "")", .themeYello, true, true)
        }else if voucher.status == "Redeemed" || voucher.status == "Gifted"{
            return (voucher.status, .darkGreen, false, false)
        }else if voucher.status == "In-Active" || voucher.status == "Expired"{
            return (voucher.status, .darkGray, false, false)
        }
        return ("", .theme, false, false)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
        //contentView.setShadow()UIColor.hexStringToUIColor(hex: "18191F")
        mainView.setCornerRadius(12)
        mainView.setShadow(color: UIColor.hexStringToUIColor(hex: "55606D1A") , opacity: 0.2, offSet: .zero, radius: 7, scale: true)
        
    }
    
    @IBAction func actionOnApplyBtn(_ sender: Any) {
        //if !used_in_mobile_app {
        if tenderKey == "OFFER"{
            delegate?.actionOnApplyBtn(code: barcode, amount: amount, tenderKey: tenderKey)
        }else{
            delegate?.actionOnApplyBtn(code: barcode, amount: amount, tenderKey: tenderKey == "GC" ? "COUPON" : "VOUCHER")
        }
        //}
    }
    @IBAction func clickTermsAndCondition(_ sender: Any) {
        
        if tenderKey == "OFFER"{
            delegate?.actionOnTermsBtn(code: barcode, tenderKey: tenderKey,  index: indexPath)
        }else{
            delegate?.actionOnTermsBtn(code: barcode,  tenderKey: tenderKey == "GC" ? "COUPON" : "VOUCHER", index: indexPath)
        }
    }
    
    
    
    @IBAction func actionOnDeleteBtn(_ sender: Any) {
        delegate?.actionOnDeleteBtn(code: barcode, tenderKey: tenderKey == "GC" ? "COUPON" : "VOUCHER")
    }
    
    @IBAction func onClickBtnViewDetails(_ sender: Any) {
        delegate?.onClickBtnDetails(coupon: coupon, isEnabled: applyBtn.isEnabled, isApplied: !applyBtn.isUserInteractionEnabled)
    }
    
        @IBAction func onClickBtnCopy(_ sender: Any) {
            AnalyticsHelper.shared.triggerEvent(type: .MY03A)
            UIPasteboard.general.string = barcode
            let typeOfCode = tenderKey == "GC" ? "COUPON" : "VOUCHER"
            UIUtils.showToast(message: "\(typeOfCode) code: " + (lblBarcode.text ?? "") + " is copied")
        }
    
    private func checkForAppliedVoucher(coupon: Coupon) -> Bool{
        for voucher in appliedCouoons{
            if coupon.barcode == voucher.promocode{
                return true
            }
        }
        return false
    }
}


