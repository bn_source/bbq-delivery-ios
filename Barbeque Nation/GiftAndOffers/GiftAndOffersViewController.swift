//
 //  Created by Arpana on 20/04/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified GiftAndOffersViewController.swift
 //

import UIKit
import ShimmerSwift

enum ScrollableSegmentTabbarTag : Int {
    
    case Coupons = 0
    case Vouchers = 1
    case Offers = 2
}

class DataModel: NSObject {
    
    var isSelected = false
    var title = NSAttributedString(string:"")
    
   override init(){
        
        super.init()
    }
    init(title : NSAttributedString , isSelected : Bool){
        
        self.isSelected = isSelected
        self.title = title
    }
 
    
}

class GiftAndOffersViewController: BaseViewController {
    
    lazy var homeViewModel : HomeViewModel = {
        let homeModel = HomeViewModel()
        return homeModel
    }()
    var promoViewModel: PromosViewModel? = nil
    var promotionsArray : [PromotionModel] = []
    var vouchersArray : [VouchersModel] = []
    var itemsQuantity = [Int]()
    
    var coupons =  [Coupon]()
    var vouchers = [Coupon]()
    var offers = [Coupon]()
    
    
    var isForVoucher = false
    var order_type = "Delivery"
    var transaction_type = ""
    var appliedCouoons = [ListTender]()
    var isOfferApplied = false
    var orderId = ""
    var logo_url = ""
    var brand_id: String? = nil
    var bottomSheetController: BottomSheetController?

    let dataForSegment :[DataModel] =  [(DataModel(title: NSAttributedString(string: "Coupons"), isSelected: false)),(DataModel(title: NSAttributedString(string:"Vouchers"), isSelected: false)),(DataModel(title: NSAttributedString(string:"Offers"), isSelected: false))]//(DataModel(title: "Happiness Card", isSelected: false))
    
  
    @IBOutlet weak var segmentControl:DGScrollableSegmentControl!
    
    @IBOutlet weak var mainview: UIView!
    
    @IBOutlet weak var shimmerSupportView: UIView!
    @IBOutlet weak var shimmerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    var shimmerControl : ShimmeringView?
    
    var isShimmerRunning : Bool = false
    
    
    @IBOutlet weak var textFieldView: UIView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var applyBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        if let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
//            flowLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
//        }
        
        var frame = CGRect.zero
        frame.size.height = .leastNormalMagnitude
        tableView.tableHeaderView = UIView(frame: frame)

        
        self.segmentControl.delegate = self
        self.segmentControl.datasource = self
        
      //  collectionView.tag = Constants.TagValues.voucherCellTag
        mainview.layer.masksToBounds = false
        mainview.layer.shadowColor = UIColor.lightGray.cgColor
        mainview.layer.shadowOffset = CGSize(width: 0, height: 3)
        mainview.layer.shadowRadius = 2
        mainview.layer.shadowOpacity = 0.5
        
        segmentControl.clipsToBounds = true
        segmentControl.layer.cornerRadius = 12
        segmentControl.layer.maskedCorners = [ .layerMinXMinYCorner, .layerMinXMaxYCorner]
        if #available(iOS 13.0, *) {
            segmentControl.layer.cornerCurve = .continuous
        } else {
            // Fallback on earlier versions
        }
        
        textFieldView.layer.masksToBounds = false
        textFieldView.layer.shadowColor = UIColor.lightGray.cgColor
        textFieldView.layer.shadowOffset = CGSize(width: 0, height: 3)
        textFieldView.layer.shadowRadius = 2
        textFieldView.layer.shadowOpacity = 0.5
        applyBtn.applyMultiBrandTheme()
        
        
        
        self.view.layoutIfNeeded()
      //  self.isNeedLoadLoyalityPoints = false
        super.viewDidLoad()
        AnalyticsHelper.shared.triggerEvent(type: .HH01)
        self.shimmerControl = ShimmeringView(frame: shimmerSupportView.bounds)
        tableView.isHidden = false
        tableView.estimatedRowHeight = 100
        tableView.estimatedRowHeight = UITableView.automaticDimension

       // self.tableView.addSubview(self.refreshControl)
        getCoupons()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // hideNavigationBar(false, animated: true)
        
    }
    
    @IBAction func backButtonPressed(_ sender : UIButton){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func actionOnApplyButton(_ sender: Any) {
        if textField!.text!.isEmpty {
            UIUtils.showToast(message: "Please enter the coupon/voucher code")
        } else {
            validateCouponCode(barcode: textField.text ?? "", params_tender_key: "") { (error, response, isSuccess) in
                if isSuccess{
                    AnalyticsHelper.shared.triggerEvent(type: .delivery_manual_coupon_code_apply)
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    
    func validateCouponCode(barcode: String, params_tender_key: String?, completionHandler:@escaping (_ error: BBQError?, _ response: AddCouponResponseModel?, _ isSuccess: Bool) -> Void){
        triggerApplyEventClicked(tender_key: params_tender_key ?? "")
        UIUtils.showLoader()
        BBQServiceManager.getInstance().validatteVouchersOrCoupons(params: ["customer_id": BBQUserDefaults.sharedInstance.customerId,
                                                                            "branch_id": BBQUserDefaults.sharedInstance.DeliveryBranchIdValue,
                                                                            "barcode": barcode,
                                                                            "sales_sub_type": order_type,
                                                        "brand_id": brand_id]) { (error, response) in
            UIUtils.hideLoader()
            if let response = response {
                if response.message_type == "success" {
                    let dataTenderKey = response.data?.tender_type ?? params_tender_key ?? ""
                    //_ = String(format: "%li", response.data?.denomination ?? 0)
                    let tenderKey = dataTenderKey == "GC" ? "COUPON" : "VOUCHER"
                    let promocode = response.data?.promocode ?? barcode
                    let barcode = response.data?.bar_code ?? barcode
                    if tenderKey == "COUPON"{
                        self.removeAppliedCoupons()
                    }
                    AnalyticsHelper.shared.triggerEvent(type: tenderKey == "COUPON" ? .delivery_coupon_verify_success : .delivery_voucher_verify_success)
                    self.applyCouponCode(tenderKey: tenderKey, amount: response.data?.denomination ?? 0, barcode: barcode, promocode: promocode, dataTenderKey: dataTenderKey, params_tender_key: params_tender_key ?? "") { error, response, isAdded in
                        completionHandler(error, response, isAdded)
                    }
                } else {
                    let dataTenderKey = response.data?.tender_type ?? params_tender_key ?? ""
                    let tenderKey = dataTenderKey == "GC" ? "COUPON" : "VOUCHER"
                    self.triggerFailureEventClicked(tender_key: dataTenderKey, old_key: params_tender_key ?? "")
                    AnalyticsHelper.shared.triggerEvent(type: tenderKey == "COUPON" ? .delivery_coupon_verify_fail : .delivery_voucher_verify_fail)
                    UIUtils.showToast(message: response.message)
                }
            } else {
                self.triggerFailureEventClicked(tender_key: params_tender_key ?? "", old_key: params_tender_key ?? "")
                UIUtils.showToast(message: error?.description ?? "")
            }
        }
    }
    
    
    private func applyCouponCode(tenderKey: String, amount: Int, barcode: String, promocode: String, dataTenderKey: String, params_tender_key: String, completionHandler:@escaping (_ error: BBQError?, _ response: AddCouponResponseModel?, _ isSuccess: Bool) -> Void){
        UIUtils.showLoader()
        BBQServiceManager.getInstance().addVouchersOrCoupons(params: ["customer_id": BBQUserDefaults.sharedInstance.customerId,
                                                                      "branch_id": BBQUserDefaults.sharedInstance.DeliveryBranchIdValue,
                                                                      "tender_key": tenderKey,
                                                                      "amount": amount,
                                                                      "payment_id": "",
                                                                      "barcode": barcode,
                                                                      "promocode": promocode,
                                                                      "sales_sub_type": order_type,
                                                        "brand_id": brand_id]) { (error, response) in
            UIUtils.hideLoader()
            if let response = response {
                if response.message_type == "failed" {
                    AnalyticsHelper.shared.triggerEvent(type: tenderKey == "COUPON" ? .delivery_coupon_update_payment_fail : .delivery_voucher_update_payment_fail)
                    self.triggerFailureEventClicked(tender_key: dataTenderKey, old_key: params_tender_key )
                    UIUtils.showToast(message: response.message)
                } else {
                    AnalyticsHelper.shared.triggerEvent(type: tenderKey == "COUPON" ? .delivery_coupon_update_payment_success : .delivery_voucher_update_payment_success)
                    self.triggerSuccessEventClicked(tender_key: dataTenderKey, old_key: params_tender_key )
                    deliveryCartAmount = String(amount)
                    deliveryCartType = dataTenderKey
                    deliveryCartCode = promocode
                    UIUtils.showToast(message: "Coupon/Voucher Applied Successfully")
                    completionHandler(error, response, true)
                }
            } else {
                self.triggerFailureEventClicked(tender_key: dataTenderKey, old_key: params_tender_key )
                AnalyticsHelper.shared.triggerEvent(type: tenderKey == "COUPON" ? .delivery_coupon_update_payment_fail : .delivery_voucher_update_payment_fail)
                UIUtils.showToast(message: error?.message ?? UNEXPECTED_ERROR)
            }
        }
    }
    
    private func triggerSuccessEventClicked(tender_key: String, old_key: String){
        if old_key == ""{
            AnalyticsHelper.shared.triggerEvent(type: .C16A)
            AnalyticsHelper.shared.triggerEvent(type: .CC06A)
        }
        if tender_key == "GC" || tender_key == "COUPON"{
            AnalyticsHelper.shared.triggerEvent(type: .C12A)
            AnalyticsHelper.shared.triggerEvent(type: .CC04A)
        }else if tender_key == "GV" || tender_key == "VOUCHER"{
            AnalyticsHelper.shared.triggerEvent(type: .C14A)
            AnalyticsHelper.shared.triggerEvent(type: .CC05A)
        }else if tender_key == "OFFER"{
            AnalyticsHelper.shared.triggerEvent(type: .C20A)
            AnalyticsHelper.shared.triggerEvent(type: .CC09A)
        }
    }
    
    private func triggerFailureEventClicked(tender_key: String, old_key: String){
        if old_key == ""{
            AnalyticsHelper.shared.triggerEvent(type: .C16B)
            AnalyticsHelper.shared.triggerEvent(type: .CC06B)
        }
        if tender_key == "GC" || tender_key == "COUPON"{
            AnalyticsHelper.shared.triggerEvent(type: .C12B)
            AnalyticsHelper.shared.triggerEvent(type: .CC04B)
        }else if tender_key == "GV" || tender_key == "VOUCHER"{
            AnalyticsHelper.shared.triggerEvent(type: .C14B)
            AnalyticsHelper.shared.triggerEvent(type: .CC05B)
        }else if tender_key == "OFFER"{
            AnalyticsHelper.shared.triggerEvent(type: .C20B)
            AnalyticsHelper.shared.triggerEvent(type: .CC09B)
        }
    }
    
    
    private func triggerApplyEventClicked(tender_key: String){
        if tender_key == "GC" || tender_key == "COUPON"{
            AnalyticsHelper.shared.triggerEvent(type: .C12)
            AnalyticsHelper.shared.triggerEvent(type: .CC04)
        }else if tender_key == "GV" || tender_key == "VOUCHER"{
            AnalyticsHelper.shared.triggerEvent(type: .C14)
            AnalyticsHelper.shared.triggerEvent(type: .CC05)
        }else if tender_key == "OFFER"{
            AnalyticsHelper.shared.triggerEvent(type: .C20)
            AnalyticsHelper.shared.triggerEvent(type: .CC09)
        }else{
            AnalyticsHelper.shared.triggerEvent(type: .C16)
            AnalyticsHelper.shared.triggerEvent(type: .CC06)
        }
    }
}

   



extension GiftAndOffersViewController: UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    @objc func loadTable() {
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        switch (tableView.tag){
            
        case ScrollableSegmentTabbarTag.Coupons.rawValue:
            
            if coupons.count == 0 {
                   self.tableView.setEmptyMessage("No coupons available")
               } else {
                   self.tableView.restore()
               }
            return coupons.count
            
            
        case ScrollableSegmentTabbarTag.Vouchers.rawValue:
            if vouchers.count == 0 {
                   self.tableView.setEmptyMessage("No vouchers available")
               } else {
                   self.tableView.restore()
               }
            return vouchers.count
            
            
            
        case ScrollableSegmentTabbarTag.Offers.rawValue:
            
            if offers.count == 0 {
                   self.tableView.setEmptyMessage("No Offers available")
               } else {
                   self.tableView.restore()
               }
            return offers.count
            
            
        default:
            return 0
        }
    }
        
     
    

    // TODO: Refactor entire method, create viewmodel separately and draw logic from it.
    // Do data manipulation in cell itself using uypdate method.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "GiftTableViewCell", for: indexPath) as! GiftTableViewCell
        var data: Coupon
        cell.delegate = self
        cell.indexPath = indexPath
        cell.appliedCouoons = self.appliedCouoons
        switch (tableView.tag){
            
        case ScrollableSegmentTabbarTag.Coupons.rawValue:
            
            data = coupons[indexPath.row]
            break
            
        case ScrollableSegmentTabbarTag.Vouchers.rawValue:
            
            guard let cell  = tableView.dequeueReusableCell(withIdentifier: "GiftVoucherTableViewCell", for: indexPath)  as? GiftVoucherTableViewCell else { return UITableViewCell() }
            cell.delegate = self
            //cell.setCellData(voucher: vouchersData[indexPath.row], indexPath: indexPath)
            
            guard let model = vouchers[indexPath.row] as? Coupon else {
                return cell
            }
            cell.setCellData(voucher: model, indexPath:indexPath)
            
          //  cell.delegate = self
            
            
            return cell
            
        case ScrollableSegmentTabbarTag.Offers.rawValue:
            data = offers[indexPath.row]

            break
            
        default:
            return tableView.defaultCell(indexPath: indexPath)
        }
        
        cell.setCellData(data: data, transaction_type: transaction_type, order_type: order_type)
        return cell
      
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var data: Coupon?
        switch (tableView.tag){
            
        case ScrollableSegmentTabbarTag.Coupons.rawValue:
            
            data = coupons[indexPath.row]
            break
            
        case ScrollableSegmentTabbarTag.Vouchers.rawValue:
         //   data = vouchers[indexPath.row]
            
            break
            
        case ScrollableSegmentTabbarTag.Offers.rawValue:
            data = offers[indexPath.row]

            break
            
        default:
            data = nil
            print("Deafault")
        }
        
        if let data = data {
            if data.voucher_type == "GC"{
                AnalyticsHelper.shared.triggerEvent(type: .CC07)
            }else{
                AnalyticsHelper.shared.triggerEvent(type: .CC08)
            }
            let deliveryCouponDetailsView = DeliveryCouponDetailsView().initWith(coupon: data, isEnabled: true, isApplied: checkForAppliedVoucher(coupon: data), delegate: self, logo_url: logo_url)
            deliveryCouponDetailsView.addToSuperView(view: self)
        }
    }
}

extension GiftAndOffersViewController{
    // MARK: Shimmer Methods
    
    private func checkForAppliedVoucher(coupon: Coupon) -> Bool{
        for voucher in appliedCouoons{
            if coupon.barcode == voucher.promocode{
                return true
            }
        }
        return false
    }
    
    
    private func removeAppliedCoupons(){
        for tender in appliedCouoons{
            if tender.tender_key == "COUPON" || tender.tender_key == "OFFER"{
                self.removeCoupon(code: tender.barcode, tender_key: tender.tender_key)
            }
        }
    }
    
    private func removeCoupon(code: String, tender_key: String) {
//        UIUtils.showLoader()
        BBQServiceManager.getInstance().removeVouchers(params:
                                                        ["customer_id":BBQUserDefaults.sharedInstance.customerId,
                                                         "branch_id":BBQUserDefaults.sharedInstance.DeliveryBranchIdValue,
                                                         "tender_key":tender_key,
                                                         "barcode":code]) { (error, response) in
//            UIUtils.hideLoader()
        }
    }
    
    func getCoupons() {
        //self.couponsTableView.isHidden = true
        showShimmering()
        BBQServiceManager.getInstance().getCouponAndVouchers(params: getParams()) { (error, response) in
            self.stopShimmering()
            if error != nil {
                UIUtils.showToast(message: error?.message ?? "")
            } else {
                self.coupons = response?.data?.coupons ?? [Coupon]()
                self.vouchers = response?.data?.vouchers ?? [Coupon]()
                self.offers = response?.data?.offers ?? [Coupon]()
                if self.coupons.count > 0{
                    AnalyticsHelper.shared.triggerEvent(type: .CC02)
                }else{
                    AnalyticsHelper.shared.triggerEvent(type: .CC02A)
                }
                if self.vouchers.count > 0{
                    AnalyticsHelper.shared.triggerEvent(type: .CC03)
                }else{
                    AnalyticsHelper.shared.triggerEvent(type: .CC03A)
                }

                self.setSegmentControlDataForCount()
                }
            }
            
        
    }
    
    
    func setSegmentControlDataForCount(){

        self.dataForSegment.map { model in
            
            switch (model.title.string) {
                
            case "Coupons":
                
                let strAtr = NSMutableAttributedString()
                
                strAtr.append(NSMutableAttributedString(string: model.title.string, attributes: [NSAttributedString.Key.font: UIFont(name: Constants.App.BBQAppFont.ThemeRegular, size: 12)]))
                
                strAtr.append(NSMutableAttributedString(string: (String(format: " (%d) ", self.coupons.count ?? 0)), attributes: [NSAttributedString.Key.font: UIFont(name: Constants.App.BBQAppFont.ThemeSemiBold, size: 12), NSAttributedString.Key.foregroundColor: UIColor.hexStringToUIColor(hex: "8F3732") as Any]))
                
                model.title = strAtr

                break
                
            case "Offers":
               
                let strAtr = NSMutableAttributedString()
                
                strAtr.append(NSMutableAttributedString(string: model.title.string, attributes: [NSAttributedString.Key.font: UIFont(name: Constants.App.BBQAppFont.ThemeRegular, size: 12)]))
                
                strAtr.append(NSMutableAttributedString(string: (String(format: " (%d) ", self.offers.count ?? 0)) , attributes: [NSAttributedString.Key.font: UIFont(name: Constants.App.BBQAppFont.ThemeSemiBold, size: 12), NSAttributedString.Key.foregroundColor: UIColor.hexStringToUIColor(hex: "8F3732") as Any]))
                model.title = strAtr

                break

            case "Vouchers":
                let strAtr = NSMutableAttributedString()
                
                strAtr.append(NSMutableAttributedString(string: model.title.string, attributes: [NSAttributedString.Key.font: UIFont(name: Constants.App.BBQAppFont.ThemeRegular, size: 12)]))
                
                strAtr.append(NSMutableAttributedString(string: (String(format: " (%d) ", self.vouchers.count ?? 0)) , attributes: [NSAttributedString.Key.font: UIFont(name: Constants.App.BBQAppFont.ThemeSemiBold, size: 12), NSAttributedString.Key.foregroundColor: UIColor.hexStringToUIColor(hex: "8F3732") as Any]))
                model.title = strAtr

                break
                
            default:
                print("Default")
            }
        }

        
        
        self.segmentControl.reload()
        self.tableView.tag = ScrollableSegmentTabbarTag.Coupons.rawValue
            self.tableView.reloadData()
    }
    
    func getParams() -> [String: Any] {
        ["customer_id":BBQUserDefaults.sharedInstance.customerId,
         "order_id": orderId,
         "sales_sub_type": order_type,
         "brand_id": brand_id as Any]
    }



    private func showShimmering() {
        self.shimmerSupportView.isHidden = false
        
        if let shimmer = self.shimmerControl {
            self.shimmerSupportView.addSubview(shimmer)
            shimmer.contentView = shimmerView
            shimmer.shimmerAnimationOpacity = 0.2
            shimmer.shimmerSpeed = 500.00
            shimmer.isHidden = false
            shimmer.isShimmering = true
            self.isShimmerRunning = true
        }
    }
    
    private func stopShimmering() {
        self.shimmerSupportView.isHidden = true
        
        if let shimmer = self.shimmerControl {
            shimmer.isShimmering = false
            shimmer.isHidden = true
            self.isShimmerRunning = false
        }
    }
}


extension GiftAndOffersViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView.tag == Constants.TagValues.promotionCellTag) {
            return promotionsArray.count
        } else if(collectionView.tag == Constants.TagValues.voucherCellTag) {
            return vouchersArray.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell  = collectionView.defaultCell(indexPath: indexPath)
        if(collectionView.tag == Constants.TagValues.promotionCellTag){
            let promoCell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CellIdentifiers.promotionCell, for: indexPath) as! BBQPromotionCell
            if(promotionsArray.count>0){
                let  promodel:PromotionModel = promotionsArray[indexPath.row]
                promoCell.setPrmotionCellValues(promodel)
                
            }
            cell = promoCell
        } else  if(collectionView.tag == Constants.TagValues.voucherCellTag) {
            let voucherCell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CellIdentifiers.voucherCell, for: indexPath) as! BBQVoucherCell
            if (vouchersArray.count > 0) {
                let  vouchModel:VouchersModel = vouchersArray[indexPath.row]
                voucherCell.tag = indexPath.row
                //voucherCell.cartViewModel = self.cartViewModel
                voucherCell.setVoucherCellvalues(vouchModel, voucherQuatity: 0 )//self.itemsQuantity[indexPath.row])
//                voucherCell.voucherPurchaseView.loginDelegate = self
//                voucherCell.voucherPurchaseView.serviceDelegate = self
            }
            cell = voucherCell
        }
        return cell
    }
    
}

//extension GiftAndOffersViewController:UICollectionViewDelegateFlowLayout{
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let width = view.frame.size.width
//        if(collectionView.tag == Constants.TagValues.promotionCellTag){
//            // in case you you want the cell to be 40% of your controllers view
//            //return CGSize(width: width * 0.9, height: 238)
//            return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
//        } else if(collectionView.tag == Constants.TagValues.reservationCellTag){
//            return CGSize(width: collectionView.frame.width, height: 150)
//        }
//        else if(collectionView.tag == Constants.TagValues.openOrderCellTag){
//
//            return CGSize(width: (collectionView.frame.width - 10), height: (openOrdersCollectionView.frame.height  - 20))
//        }else{
//            return CGSize(width: collectionView.frame.width, height: 230)
//        }
//    }
//}

extension GiftAndOffersViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
     
    }
    
    func showPromotionScreen(selectedRow: Int, screenType: PromoScreenType) {
//        AnalyticsHelper.shared.triggerEvent(type: .H02)
//        if let promoData = self.promoViewModel?.promosArray {
//            let promotion = promoData[selectedRow]
//            if promotion.promotion_url.lowercased() == "ubq"{
//                if let tabBarController = navigationController?.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
//                    tabBarController.selectedIndex = 2
//                    isClickedOnUbq = true
//                }
//            }else if promotion.promotion_url.lowercased() == "res"{
//                NotificationCenter.default.post(name: Notification.Name(Constants.NotificationName.showBookingScreen),
//                                                object: nil)
//            }else if promotion.promotion_url.lowercased() == "hpc"{
//                homeScrollView.scrollRectToVisible(vouchersCollectionView.frame, animated: true)
//            }else{
//                let promotionDetailScreen = UIStoryboard.loadPromotionDetailScreen()
//                promotionDetailScreen.promotionDeelegate = self
//                promotionDetailScreen.promotionId = Int(promoData[selectedRow].promoId ?? "0") ?? 0
//                promotionDetailScreen.modalPresentationStyle = .fullScreen
//                self.present(promotionDetailScreen, animated: true, completion: nil)
//            }
//        }
    }
    
    func showVoucherScreen(selectedRow: Int) {
        let voucherDetailScreen = UIStoryboard.loadVoucherDetailScreen()
      //  voucherDetailScreen.delegate = self
        voucherDetailScreen.voucherId = Int(self.vouchersArray[selectedRow].voucherId ?? 0)
        voucherDetailScreen.modalPresentationStyle = .fullScreen
        self.present(voucherDetailScreen, animated: true, completion: nil)
    }
    
}


extension GiftAndOffersViewController :DGScrollableSegmentControlDataSource,DGScrollableSegmentControlDelegate {
    func numbersOfItem(_ sender: DGScrollableSegmentControl) -> Int {
        
        return dataForSegment.count
        
    }
    
    func itemfor(_ index: Int, sender: DGScrollableSegmentControl) -> DGItem {
                    
            let item = DGItem()
            
        item.setAttributedTitle(self.dataForSegment[index].title, for: .normal)
        item.selectedColor = .deliveryThemeColor
        item.selectedTitleColor = UIColor.white
        item.isSelected = dataForSegment[index].isSelected
        return item
            
        
    }
    func didSelect(_ item: DGItem, atIndex index: Int , sender: DGScrollableSegmentControl) {
        
        print("Selected Index \(index)")
        //self.segmentControl.reload()
        
        tableView.tag = index
        tableView.reloadData()
        
    }
    
}
    

extension GiftAndOffersViewController: DeliveryCouponCellDelegate, DeliveryCouponDetailsDelegate {
    func actionOnApplyBtn(code: String, amount: Int, tenderKey: String) {
        if tenderKey == "OFFER"{
            self.removeAppliedCoupons()
            applyCouponCode(tenderKey: tenderKey, amount: amount, barcode: code, promocode: code, dataTenderKey: "OFFER", params_tender_key: "OFFER") { error, response, isSuccess in
                if isSuccess{
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }else{
            validateCouponCode(barcode: code, params_tender_key: tenderKey) { (error, response, isSuccess) in
                if isSuccess{
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        

    }
    
    func actionOnDeleteBtn(code: String, tenderKey: String) {
        UIUtils.showLoader()
        BBQServiceManager.getInstance().removeVouchers(params:
                                                        ["customer_id":BBQUserDefaults.sharedInstance.customerId,
                                                         "branch_id":BBQUserDefaults.sharedInstance.DeliveryBranchIdValue,
                                                         "tender_key": tenderKey,
                                                         "barcode":code]) { (error, response) in
            UIUtils.hideLoader()
            if let response = response {
                if response.message_type == "failed" {
                    UIUtils.showToast(message: response.message)
                } else {
                    UIUtils.showToast(message: "Coupon/Voucher Removed Successfully")
                    self.navigationController?.popViewController(animated: true)
                }
            } else {
                UIUtils.showToast(message: error?.message ?? UNEXPECTED_ERROR)
            }

        }
    }
    func onClickBtnDetails(coupon: Coupon, isEnabled: Bool, isApplied: Bool) {
        if coupon.voucher_type == "GC"{
            AnalyticsHelper.shared.triggerEvent(type: .CC07)
        }else{
            AnalyticsHelper.shared.triggerEvent(type: .CC08)
        }
        let deliveryCouponDetailsView = DeliveryCouponDetailsView().initWith(coupon: coupon, isEnabled: isEnabled, isApplied: isApplied, delegate: self, logo_url: logo_url)
        deliveryCouponDetailsView.addToSuperView(view: self)
    }
    
    func actionOnTermsBtn(code: String, tenderKey: String , index: IndexPath){
        
        
        if tenderKey == "OFFER"{
            if let  termAndConditions =  self.offers[index.row].rule_description as? String{
                let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: 300)
                let BBQTermAndConditionsViewController = BBQTermAndConditionsViewController(nibName: "BBQTermAndConditionsViewController", bundle: nil)
                BBQTermAndConditionsViewController.termsInWebFormat = termAndConditions
                bottomSheetController = BottomSheetController(configuration: configuration)

                bottomSheetController?.present(BBQTermAndConditionsViewController, on: self)
                
            }
        }
        else if tenderKey == "COUPON" {
            
            
            if let  termAndConditions =  self.coupons[index.row].rule_description as? String {
                let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: 300)
                
                let BBQTermAndConditionsViewController = BBQTermAndConditionsViewController(nibName: "BBQTermAndConditionsViewController", bundle: nil)
                
                BBQTermAndConditionsViewController.termsInWebFormat = termAndConditions
                bottomSheetController = BottomSheetController(configuration: configuration)
                BBQTermAndConditionsViewController.view.layoutIfNeeded()
                bottomSheetController?.present(BBQTermAndConditionsViewController, on: self)
                
            }
        } else if tenderKey == "VOUCHER" {
            
            
            if let  termAndConditions =  self.vouchers[index.row].rule_description as? String {
                let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: 300)
                bottomSheetController = BottomSheetController(configuration: configuration)
                
                let BBQTermAndConditionsViewController = BBQTermAndConditionsViewController(nibName: "BBQTermAndConditionsViewController", bundle: nil)
                
                BBQTermAndConditionsViewController.termsInWebFormat = termAndConditions
                
                bottomSheetController?.present(BBQTermAndConditionsViewController, on: self)
                
            }
        }
    }
    
    func onHiddenView(view: DeliveryCouponDetailsView) {
        
    }
    func onClickBtnApplyCoupon(code: String, amount: Int, tenderKey: String) {
        validateCouponCode(barcode: code, params_tender_key: tenderKey) { (error, response, isSuccess) in
            if isSuccess{
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}
