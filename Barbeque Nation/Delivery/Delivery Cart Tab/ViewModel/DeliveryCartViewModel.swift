//
//  DeliveryCartViewModel.swift
//  BBQ
//
//  Created by Rahul S on 04/10/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import UIKit
protocol DeliveryCartViewModelDelegate: AnyObject {
    func showPaymentScreen()
    func refreshData()
    func paymentSuccess()
    func paymentFailed()
    func showOfferView(textMessageToDisplay : String?)

}

class DeliveryCartViewModel {
    
    var cartDetails: GetCartResponseModel?
    var addressViewModel = AddressViewModel()
    var paymentId = ""
    weak var delegate: DeliveryCartViewModelDelegate?
    
    func getCartDetails(transaction_type: Int, isFirstTimeLoading: Bool = false) {
        if  !UIUtils.loadingIndicator.isAnimating{
            UIUtils.showLoader()
        }
        
        BBQServiceManager.getInstance().getCartDetail(params: getCartItemsParams(transaction_type: transaction_type)) { (error, response) in
            UIUtils.hideLoader()
            if let response = response {
                self.cartDetails = response
                if isFirstTimeLoading{
                    AnalyticsHelper.shared.triggerEvent(type: .Delivery_Takeaway_Checkout, properties: [.Catalog_Cart_Checkout: self.getTotalQtyItems()])
                }
                if let backgroudColor = response.data?.backgroudColor, let textColor = response.data?.textColor{
                    UIColor.deliveryThemeColor = backgroudColor
                    UIColor.deliveryThemeTextColor = textColor
                }
                self.delegate?.refreshData()
            } else {
//                UIUtils.showToast(message: error?.message ?? UNEXPECTED_ERROR)
                UIUtils.showToast(message: "Loading...")
            }
        }
    }
    
//    func getAddresses() {
//        addressViewModel.getSavedAddress(customerId: BBQUserDefaults.sharedInstance.customerId,
//                                         branchId: BBQUserDefaults.sharedInstance.DeliveryBranchIdValue, filterWithID: nil) {
//        }
//    }
//    func getAddresses(completion: @escaping ( _ result: Address)->()) {
//        cartDetails?.data?.address = addressViewModel.getSavedAddress(customerId: BBQUserDefaults.sharedInstance.customerId,
//                                                                      branchId: BBQUserDefaults.sharedInstance.DeliveryBranchIdValue, filterWithID: nil) {
//        }
//    }
      
   func autoSelectAddress()-> Address?{
       
       print(addressViewModel.addresses?.description)
       if let address =  addressViewModel.addresses?.first(where: {$0.servicable == true}) {
           return address
       }
       return nil
        
    }
    
    
    func clearCart() {
        UIUtils.showLoader()
        BBQServiceManager.getInstance().clearCartItems(params: getCartItemsParams(transaction_type: 0)) { (error, response) in
            UIUtils.hideLoader()
            if let response = response {
                if response.message_type == "failed" {
                    UIUtils.showToast(message: response.message)
                } else {
                    self.getCartDetails(transaction_type: 0)
                }
                self.paymentId = ""
            } else {
                UIUtils.showToast(message: "Loading...")
            }

        }
        
    }
    
    func useAllSmiles(amount: Int? = nil) {
        AnalyticsHelper.shared.triggerEvent(type: .C10)
        if let amount = amount, BBQUserDefaults.sharedInstance.UserLoyaltyPoints != amount{
            AnalyticsHelper.shared.triggerEvent(type: .C10B)
        }else{
            AnalyticsHelper.shared.triggerEvent(type: .C10A)
        }
        UIUtils.showLoader()
        var payingPoints = amount ?? BBQUserDefaults.sharedInstance.UserLoyaltyPoints
        if let payableAmount = Int(cartDetails?.data?.final_break_up ?? "0"), payingPoints > payableAmount{
            payingPoints = payableAmount
        }
        BBQServiceManager.getInstance().useSmiles(params: getUseAllSmilesParams(amount: payingPoints)) { (error, response) in
            UIUtils.hideLoader()
            if let response = response {
                if response.message_type == "failed" {
                    UIUtils.showToast(message: response.message)
                    AnalyticsHelper.shared.triggerEvent(type: .C10D)
                } else {
                    AnalyticsHelper.shared.triggerEvent(type: .C10C)
                    self.cartDetails = response
                    AnalyticsHelper.shared.triggerEvent(type: .delivery_use_all_smiles)
                    AnalyticsHelper.shared.triggerEvent(type: .add_smiles_delivery)
                    deliveryCartAmount = String(format: "%li", amount ?? BBQUserDefaults.sharedInstance.UserLoyaltyPoints)
                    deliveryCartType = "SMILE"
                    deliveryCartCode = "Smiles"
                    self.delegate?.refreshData()
                }
            } else {
                AnalyticsHelper.shared.triggerEvent(type: .C10D)
                UIUtils.showToast(message: "Loading...")
            }

        }
    }
    
    func removeSmiles() {
        AnalyticsHelper.shared.triggerEvent(type: .C11)
        UIUtils.showLoader()
        BBQServiceManager.getInstance().removeVouchers(params: getRemoveSmilesParams()) { (error, response) in
            UIUtils.hideLoader()
            if let response = response {
                if response.message_type == "failed" {
                    AnalyticsHelper.shared.triggerEvent(type: .C11B)
                    UIUtils.showToast(message: response.message)
                } else {
                    AnalyticsHelper.shared.triggerEvent(type: .C11A)
                    self.cartDetails = response
                    AnalyticsHelper.shared.triggerEvent(type: .delivery_remove_smiles)
                    self.delegate?.refreshData()
                }
            } else {
                AnalyticsHelper.shared.triggerEvent(type: .C11B)
                UIUtils.showToast(message: "Loading...")
            }

        }
    }
    
    func getLoyaltyPointsCount(completion: @escaping ( _ result: Bool)->()) {
        
        // Blocking API call for guest user
        if BBQUserDefaults.sharedInstance.isGuestUser {
            completion(false)
        }
        
        let endPoint = BBQCartRouter.loyaltyPointsCount
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQCartManager.parsePointsCountData(data: responseData, error: responseError)
            if let value = modelValue?.remainingPoints {
                BBQUserDefaults.sharedInstance.UserLoyaltyPoints = value
                DispatchQueue.main.async {
                    completion(true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(false)
                }
            }
        }
    }
    
    func getCartDetailsParams(transactionType: Int, deliveryTime: String, ordering_name: String, ordering_phonenumber: String) -> [String: Any] {
        return ["customer_id": BBQUserDefaults.sharedInstance.customerId,
            "branch_id": BBQUserDefaults.sharedInstance.DeliveryBranchIdValue,
            "delivery_time": deliveryTime,
            "transaction_type": transactionType,
            "source": "iOS",
            "source_version": Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String, //UIDevice.current.systemVersion,
            "customer_name": BBQUserDefaults.sharedInstance.UserName,
            "ordering_name": ordering_name,
            "ordering_phonenumber": ordering_phonenumber]
    }
    
    func getCartItemsParams(transaction_type: Int) -> [String: Any] {
        if transaction_type == 0{
            return ["customer_id": BBQUserDefaults.sharedInstance.customerId,
                "branch_id": BBQUserDefaults.sharedInstance.DeliveryBranchIdValue,
                "source": "iOS",
                "source_version": Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String]
        }else{
            return ["customer_id": BBQUserDefaults.sharedInstance.customerId,
                "branch_id": BBQUserDefaults.sharedInstance.DeliveryBranchIdValue,
                "transaction_type": transaction_type,
                "source": "iOS",
                "source_version": Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String]
        }
        
    }
    
    func getUseAllSmilesParams(amount: Int? = nil) -> [String: Any] {
        return ["amount": amount ?? BBQUserDefaults.sharedInstance.UserLoyaltyPoints,
                "customer_id": BBQUserDefaults.sharedInstance.customerId,
                "branch_id": BBQUserDefaults.sharedInstance.DeliveryBranchIdValue,
                    "tender_key": "SMILES",
                    "payment_id": "",
                    "barcode": ""]
    }
    
    func getRemoveSmilesParams() -> [String: Any] {
        return ["customer_id": BBQUserDefaults.sharedInstance.customerId,
                "branch_id": BBQUserDefaults.sharedInstance.DeliveryBranchIdValue,
                    "tender_key": "SMILES",
                    "barcode": ""]
    }
    
    func getPaymentValidateIdParams() -> [String: Any] {
        return ["payment_id": paymentId,
                "customer_id": BBQUserDefaults.sharedInstance.customerId,
                "branch_id": BBQUserDefaults.sharedInstance.DeliveryBranchIdValue]
    }
    
    func getCheckDistanceParams(deliveryId: String?  ) -> [String: Any] {
        return ["branch_id": cartDetails?.data?.branch_id ?? "",
                "delivery_address_id": deliveryId == nil ?cartDetails?.data?.delivery_address_id ?? "" : deliveryId,
                "latitude": getLatitudeFromAddressId(),
                "longitude": getLongitudeFromAddressId()]
    }
    
    func getLatitudeFromAddressId() -> String {
        if let address =  addressViewModel.addresses?.first(where: {$0.addressId == cartDetails?.data?.delivery_address_id ?? "" }) {
            return address.latitude
        }
        return ""
    }
    
    func getLongitudeFromAddressId() -> String {
        if let address =  addressViewModel.addresses?.first(where: {$0.addressId == cartDetails?.data?.delivery_address_id ?? "" }) {
            return address.longitude
        }
        return ""
    }
    
    func checkForLoyalityPointCount(Completion:(@escaping (_ isValid: Bool) -> Void)) {
        if let tender = cartDetails?.data?.tender.first(where: {$0.tender_key == "SMILES"}) {
            if let smileAmount = tender.lstTender.first?.amount, smileAmount > 0 {
                getLoyaltyPointsCount { isChecked in
                    if smileAmount > BBQUserDefaults.sharedInstance.UserLoyaltyPoints{
                        self.removeSmiles()
                        Completion(false)
                    }else{
                        Completion(true)
                    }
                }
            }else{
                Completion(true)
            }
        }else{
            Completion(true)
        }
    }
    
    func generatePaymentId(transactionType: Int, deliveryTime: String, ordering_name: String, ordering_phonenumber: String) {
        UIUtils.showLoader()
//        checkForLoyalityPointCount { isValid in
//            if isValid{
                BBQServiceManager.getInstance().generatePaymentId(params: self.getCartDetailsParams(transactionType: transactionType, deliveryTime: deliveryTime, ordering_name: ordering_name, ordering_phonenumber: ordering_phonenumber)) { (error, response) in
                    UIUtils.hideLoader()
                    if let response = response {
                        if response.message_type == "failed" {
                            UIUtils.showToast(message: response.message)
                            AnalyticsHelper.shared.triggerEvent(type: .Delivery_Takeaway_Order_Status, properties: [.Delivery_Takeaway_Status: AnalyticsHelper.MoEngageAttrValue.Failed.rawValue, .Error: response.message ])
                        } else if response.data?.payment_order_id == "" {
                            self.getCartDetails(transaction_type: transactionType)
                            AnalyticsHelper.shared.triggerEvent(type: .Delivery_Takeaway_Order_Status, properties: [.Delivery_Takeaway_Status: AnalyticsHelper.MoEngageAttrValue.Success.rawValue, .Oderid: self.cartDetails?.data?.order_id ?? ""])
                        } else {
                            self.paymentId = response.data?.payment_order_id ?? ""
                            //self.delegate?.showPaymentScreen()
                        }
                    } else {
                        UIUtils.showToast(message: "Loading...")
                    }
                }
//            }else{
//                UIUtils.hideLoader()
//            }
//        }
    }
    
    func generatePaymentId(transactionType: Int, deliveryTime: String, ordering_name: String, ordering_phonenumber: String, completion: @escaping (_ paymentId: String?, _ errorString: String?) -> Void) {
        UIUtils.showLoader()
//        checkForLoyalityPointCount { isValid in
//            if isValid{
                BBQServiceManager.getInstance().generatePaymentId(params: self.getCartDetailsParams(transactionType: transactionType, deliveryTime: deliveryTime, ordering_name: ordering_name, ordering_phonenumber: ordering_phonenumber)) { (error, response) in
                    UIUtils.hideLoader()
                    if let response = response {
                        if response.message_type == "failed" {
                            UIUtils.showToast(message: response.message)
                            AnalyticsHelper.shared.triggerEvent(type: .Delivery_Takeaway_Order_Status, properties: [.Delivery_Takeaway_Status: AnalyticsHelper.MoEngageAttrValue.Failed.rawValue, .Error: response.message ])
                            completion(nil, response.message)
                        } else if response.data?.payment_order_id == "" {
                            AnalyticsHelper.shared.triggerEvent(type: .Delivery_Takeaway_Order_Status, properties: [.Delivery_Takeaway_Status: AnalyticsHelper.MoEngageAttrValue.Success.rawValue, .Oderid: self.cartDetails?.data?.order_id ?? ""])
                            completion(nil, response.message)
                        } else {
                            self.paymentId = response.data?.payment_order_id ?? ""
                            completion(self.paymentId, nil)
                        }
                    } else {
                        UIUtils.showToast(message: "Loading...")
                    }
                }
//            }else{
//                UIUtils.hideLoader()
//            }
//        }
    }
    
    func validatePaymentId(transaction_type: Int) {
        //UIUtils.showLoader()
        AnalyticsHelper.shared.triggerEvent(type: .C18)
        BBQServiceManager.getInstance().validatePaymentId(params: getPaymentValidateIdParams()) { (error, response) in
            //UIUtils.hideLoader()
            if let response = response {
                if response.message_type == "failed" {
                    AnalyticsHelper.shared.triggerEvent(type: .delivery_order_failed)
                    AnalyticsHelper.shared.triggerEvent(type: .C18B)
                    self.delegate?.paymentFailed()
                    UIUtils.showToast(message: response.message)
                    AnalyticsHelper.shared.triggerEvent(type: .Delivery_Takeaway_Order_Status, properties: [.Delivery_Takeaway_Status: AnalyticsHelper.MoEngageAttrValue.Failed.rawValue, .Error: error?.description as Any ])
                    DispatchQueue.main.async {
                        var input = self.getPaymentValidateIdParams()
                        input["order_id"] = self.cartDetails?.data?.order_id
                        FirestoreHelper.shared.uploadDataOrderFailed(input: input, output: ["serverError": response.toJSON()])
                    }
                } else {
                    UIUtils.showToast(message: "Order created successfully")
                    AnalyticsHelper.shared.triggerEvent(type: .delivery_order_success)
                    AnalyticsHelper.shared.triggerEvent(type: .C18A)
                    self.getCartDetails(transaction_type: transaction_type)
                    self.delegate?.paymentSuccess()
                    AnalyticsHelper.shared.triggerEvent(type: .Delivery_Takeaway_Order_Status, properties: [.Delivery_Takeaway_Status: AnalyticsHelper.MoEngageAttrValue.Success.rawValue, .Oderid: self.cartDetails?.data?.order_id as Any])
                }
                self.paymentId = ""
            } else {
                AnalyticsHelper.shared.triggerEvent(type: .delivery_order_failed)
                AnalyticsHelper.shared.triggerEvent(type: .C18B)
                self.delegate?.paymentFailed()
                UIUtils.showToast(message: "Loading...")
                AnalyticsHelper.shared.triggerEvent(type: .Delivery_Takeaway_Order_Status, properties: [.Delivery_Takeaway_Status: AnalyticsHelper.MoEngageAttrValue.Failed.rawValue, .Error: error?.description as Any ])
                DispatchQueue.main.async {
                    var input = self.getPaymentValidateIdParams()
                    input["order_id"] = self.cartDetails?.data?.order_id
                    FirestoreHelper.shared.uploadDataOrderFailed(input: input, output: ["serverError": error.debugDescription, "desc": error?.description as Any])
                }
            }
        }
    }
    
    func checkDistanceAPI(ordering_name: String, ordering_phonenumber: String, deliveryTime: String, deliveryId: String?, isClickedFromPayBtn : Bool = true, completion: @escaping(_ isServicable: Bool)-> ()) {
        
        
       //  isClickedFromPayBtn -> if api call from pay btn screen , take user to payment screen
        //If called when adding new address for delivery , just check if servicable aadd address to card cart adress lable
        //allow to proceed for payment
        
        // deliveryId -> while adding new address we check if newly added address is servicable or not
        // passes newly address id to get lat long from all address listing by comparing address id
        
        UIUtils.showLoader()
        BBQServiceManager.getInstance().checkDistance(params: getCheckDistanceParams(deliveryId: deliveryId ?? nil)) { (error, response) in
            UIUtils.hideLoader()
            if let response = response {
                if response.message_type == "failed" {
                    UIUtils.showToast(message: "Selected Address is not servicable")
                    AnalyticsHelper.shared.triggerEvent(type: .Delivery_Takeaway_Order_Status, properties: [.Delivery_Takeaway_Status: AnalyticsHelper.MoEngageAttrValue.Failed.rawValue, .Error: "Selected Address is not servicable"])
                } else {
                    if response.data?.servicable ?? false {
                        
                       if isClickedFromPayBtn == true {
                            self.delegate?.showPaymentScreen()
                        }
                        
                        //self.generatePaymentId(transactionType: 1, deliveryTime: deliveryTime, ordering_name: ordering_name, ordering_phonenumber: ordering_phonenumber)
                        completion(response.data?.servicable ?? false)
                    } else {
                        UIUtils.showToast(message: "selected address is not servicable")
                        AnalyticsHelper.shared.triggerEvent(type: .Delivery_Takeaway_Order_Status, properties: [.Delivery_Takeaway_Status: AnalyticsHelper.MoEngageAttrValue.Failed.rawValue, .Error: "selected address is not servicable"])
                    }
                }
                completion(response.data?.servicable ?? false)

            } else {
                UIUtils.showToast(message: "Loading...")
                completion(false)

            }
        }
    }
    
    func getBillAmount() -> String {
       let billAmount =  cartDetails?.data?.tax_break_up.first(where: { (taxbreakup) -> Bool in
            if taxbreakup.label == "Item Total" {
                return true
            }
            return false
        })
        return "\(getCurrency())\(billAmount?.value ?? "0.0")"
    }
    
    func getPackingCharges() -> String {
        let billAmount =  cartDetails?.data?.tax_break_up.first(where: { (taxbreakup) -> Bool in
             if taxbreakup.label == "Packaging" {
                 return true
             }
             return false
         })
         return "\(getCurrency())\(billAmount?.value ?? "0.0")"
    }
    
    func getGstCharges() -> String {
        let billAmount =  cartDetails?.data?.tax_break_up.first(where: { (taxbreakup) -> Bool in
             if taxbreakup.label == "GST" {
                 return true
             }
             return false
         })
         return "\(getCurrency())\(billAmount?.value ?? "0.0")"
    }
    
    func getDeliveryCharges() -> String {
        let billAmount =  cartDetails?.data?.tax_break_up.first(where: { (taxbreakup) -> Bool in
             if taxbreakup.label == "Delivery" {
                 return true
             }
             return false
         })
         return "\(getCurrency())\(billAmount?.value ?? "0.0")"
    }
    
    func getDeliveryAmount() -> Double {
        let billAmount =  cartDetails?.data?.tax_break_up.first(where: { (taxbreakup) -> Bool in
             if taxbreakup.label == "Delivery" {
                 return true
             }
             return false
         })
        return Double(billAmount?.value ?? "0.0") ?? 0.0
    }
    
    func getDiscountAmount() -> String {
        let billAmount =  cartDetails?.data?.tax_break_up.first(where: { (taxbreakup) -> Bool in
             if taxbreakup.label == "Discount/Promo" {
                 return true
             }
             return false
         })
        if let amount = Int(billAmount?.value ?? "0"), amount <= 0{
            if amount < 0{
                return "-\(getCurrency())\(-amount)"
            }
        }
         return "\(getCurrency())\(billAmount?.value ?? "0.0")"
    }
    
    func getDeliveryAddress() -> NSMutableAttributedString? {
        if cartDetails?.data?.address == nil{
            return nil
        }
        let nsAttributedString = NSMutableAttributedString(string: cartDetails?.data?.address?.getTagIcon(color: "theme").1 ?? "", attributes: [NSAttributedString.Key.font: UIFont.systemFontOfSize(size: 14.0, weight: .bold), NSAttributedString.Key.foregroundColor: UIColor(named: "TextColor") as Any])
        let subTitleString = NSAttributedString(string: "-\(cartDetails?.data?.address?.flatNo ?? ""), \(cartDetails?.data?.address?.address ?? "")", attributes: [NSAttributedString.Key.font: UIFont.systemFontOfSize(size: 14.0, weight: .regular), NSAttributedString.Key.foregroundColor: UIColor(named: "TextColor")!])
        nsAttributedString.append(subTitleString)
        return nsAttributedString
    }
    
    func getDeliveryAddressTupple() -> (String?, String?)? {
        if cartDetails?.data?.address == nil{
            return nil
        }
        return(cartDetails?.data?.address?.getTagIcon(color: "theme").1 ?? "", "\(cartDetails?.data?.address?.flatNo ?? ""), \(cartDetails?.data?.address?.address ?? "")")
    }
    
    func getDeliveryDistanceMessage() -> NSMutableAttributedString? {
        if cartDetails?.data?.address == nil{
            return nil
        }
        let nsAttributedString = NSMutableAttributedString(string: "You will visit the restaurant (", attributes: [NSAttributedString.Key.font: UIFont.systemFontOfSize(size: 14.0, weight: .regular), NSAttributedString.Key.foregroundColor: UIColor.black])
        if cartDetails?.data?.address?.distance_meters ?? 0 > 1000{
            let temoDistance = Double(cartDetails?.data?.address?.distance_meters ?? 0) / 1000.0
            let distance = String(format: "%0.2f", temoDistance)
            let subTitleString = NSAttributedString(string: "\(distance) km", attributes: [NSAttributedString.Key.font: UIFont.systemFontOfSize(size: 16.0, weight: .semibold), NSAttributedString.Key.foregroundColor: UIColor.black])
            nsAttributedString.append(subTitleString)
        }else{
            let subTitleString = NSAttributedString(string: "\(cartDetails?.data?.address?.distance_meters ?? 0) m", attributes: [NSAttributedString.Key.font: UIFont.systemFontOfSize(size: 16.0, weight: .semibold), NSAttributedString.Key.foregroundColor: UIColor.black])
            nsAttributedString.append(subTitleString)
        }
        let subTitleString = NSMutableAttributedString(string: " away) and collect the order yourself", attributes: [NSAttributedString.Key.font: UIFont.systemFontOfSize(size: 14.0, weight: .regular), NSAttributedString.Key.foregroundColor: UIColor.black])
        nsAttributedString.append(subTitleString)
        return nsAttributedString
    }
    
    func isItemsAvailableInCart() -> Bool {
        return cartDetails?.data?.items.count ?? 0 > 0 ? true : false
    }
    
    func getItemName(atIndex: Int) -> String {
        return cartDetails?.data?.items[atIndex].item_name ?? ""
    }
    
    func getIsCustomizable(atIndex: Int) -> Bool {
        return cartDetails?.data?.items[atIndex].option.count ?? 0 > 0
    }
    
    func getItemPrice(atIndex: Int) -> String {
        return "\(getCurrency())\(cartDetails?.data?.items[atIndex].total ?? "0.0")"
    }
    
    func getOriginalItemPrice(atIndex: Int) -> String {
        return "\(getCurrency())\(cartDetails?.data?.items[atIndex].price ?? "0.0")"
    }
    
    func getItemDescription(atIndex: Int) -> String {
        var description = ""//cartDetails?.data?.items[atIndex].item_desc ?? ""
        if let options = cartDetails?.data?.items[atIndex].option {
            options.forEach { (option) in
                if description != ""{
                    description.append("\n")
                }
                if let price = Int(option.price), price > 0{
                    description.append(String(format: "+ %@ %@%li", option.item_name, getCurrency(), price))
                }else{
                    description.append("\(option.item_name)")
                }
            }
        }
        
        return description
    }
    
    func getItemTypeImage(atIndex: Int) -> UIImage {
        return cartDetails?.data?.items[atIndex].food_type == .veg ? .veg : .nonVeg
    }
    
    func getItemQuantity(atIndex: Int) -> String {
        return cartDetails?.data?.items[atIndex].quantity ?? "0"
    }
    
    func getItemId(atIndex: Int) -> String {
        return cartDetails?.data?.items[atIndex].item_id ?? ""
    }
    
    func getItemDetailsId(atIndex: Int) -> Int {
        return cartDetails?.data?.items[atIndex].item_detail_id ?? 0
    }
    
    
    func addItemToCart(itemId: String, itemDetailsID: Int) {
        guard let item = cartDetails?.data?.items.first(where: {$0.item_id == itemId && $0.item_detail_id == itemDetailsID}) else {
            return
        }
        
        UIUtils.showLoader()
        let itemModel = AddOrRemoveItemRequest(item: item)
        itemModel.quantity = "1"
        if let item = cartDetails?.data?.items.first(where: {$0.item_id == item.item_id && $0.item_detail_id == itemDetailsID}),let  quantity = Int(item.quantity) {
            let quantity = quantity + 1
            itemModel.quantity = "\(quantity)"
        }
        itemModel.itemDetailId = item.item_detail_id
        let params = itemModel.toJSON()
        BBQServiceManager.getInstance().addOrRemoveItemToCart(params: params){ (error, response) in
            UIUtils.hideLoader()
            if let response = response {
                self.cartDetails = response
                item.isOfferAvailable = response.data?.last_offer_display
                self.delegate?.refreshData()
                item.isOfferAvailable = response.data?.last_offer_display
                self.delegate?.showOfferView(textMessageToDisplay: item.isOfferAvailable)
            } else {
                UIUtils.showToast(message: "Loading...")
            }
        }
    }
    
    func addSuggestedItemToCart(itemId: String, transaction_type: Int) {
        UIUtils.showLoader()
        let params = [ "customer_id": BBQUserDefaults.sharedInstance.customerId,
                       "branch_id": BBQUserDefaults.sharedInstance.DeliveryBranchIdValue,
                       "item_id": itemId,
                       "quantity": "1",
                       "item_details": [],
                       "item_detail_id": 0] as [String : Any]
        BBQServiceManager.getInstance().addOrRemoveItemToCart(params: params){ (error, response) in
            self.getCartDetails(transaction_type: transaction_type)
        }
    }
    
    func getTimeListCount() -> Int {
        return cartDetails?.data?.pickUpAvailabilityTime.first?.time.count ?? 0
    }
    
    func getTime(atIndex: Int) -> String {
        return cartDetails?.data?.pickUpAvailabilityTime.first?.time[atIndex] ?? ""
    }
    
    func removeItemFromCart(itemId: String, itemDetailsID: Int) {
        guard let item = cartDetails?.data?.items.first(where: {$0.item_id == itemId && $0.item_detail_id == itemDetailsID}) else {
            return
        }
        UIUtils.showLoader()
        let itemModel = AddOrRemoveItemRequest(item: item)
        itemModel.quantity = "0"
        if let item = cartDetails?.data?.items.first(where: {$0.item_id == item.item_id && $0.item_detail_id == itemDetailsID}),let  quantity = Int(item.quantity) {
            let quantity = quantity - 1
            itemModel.quantity = "\(quantity)"
        }
        itemModel.itemDetailId = item.item_detail_id
        let params = itemModel.toJSON()
        BBQServiceManager.getInstance().addOrRemoveItemToCart(params: params){ (error, response) in
            UIUtils.hideLoader()
            if let response = response {
                self.cartDetails = response
                self.delegate?.refreshData()
            } else {
                UIUtils.showToast(message: "Loading...")
            }
        }
    }
    
    
   func setOrderingForDetails(receiverName: String, receiverContactNo: String, itemId: String) {
        
        UIUtils.showLoader()
    
    let params =  ["customer_id":BBQUserDefaults.sharedInstance.customerId,
                   "ordering_for_number": receiverContactNo,
                   "order_id" : Int(itemId) ?? 0 ,
                   "ordering_for_name":receiverName] as [String : Any]
    
        BBQServiceManager.getInstance().setOrderingForDetailsToCart(params: params){ (error, response) in
            UIUtils.hideLoader()
            if response {
                self.cartDetails?.data?.orderingForName = receiverName
                self.cartDetails?.data?.orderingForNumber = receiverContactNo

                self.delegate?.refreshData()
            } else {
                //write respective to data comming
            }
        }
    }
    
    func getTotalQtyItems() -> Int{
        var item_count = 0
        for item in cartDetails?.data?.items ?? [ItemDetails](){
            item_count += Int(item.quantity) ?? 0
        }
        return item_count
    }
}
