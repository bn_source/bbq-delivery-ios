//
 //  Created by Rahul S on 16/12/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified AddCouponsViewController.swift
 //

import UIKit

class AddCouponsViewController: BaseViewController {

    @IBOutlet weak var textFieldView: UIView!
    @IBOutlet weak var couponsTableView:UITableView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var applyBtn: UIButton!
    var orderId = ""
    var coupons =  [Coupon]()
    var vouchers = [Coupon]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldView.setShadow(radius: 5)
         getCoupons()
    }
    
    @IBAction func actionOnBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func actionOnApplyButton(_ sender: Any) {
        if textField!.text!.isEmpty {
            UIUtils.showToast(message: "Please enter the coupon/voucher code")
        } else {
            validateCouponCode(barcode: textField.text ?? "", params_tender_key: "") { (error, response, isSuccess) in
                if isSuccess{
                    AnalyticsHelper.shared.triggerEvent(type: .delivery_manual_coupon_code_apply)
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    func validateCouponCode(barcode: String, params_tender_key: String?, completionHandler:@escaping (_ error: BBQError?, _ response: AddCouponResponseModel?, _ isSuccess: Bool) -> Void){
        UIUtils.showLoader()
        BBQServiceManager.getInstance().validatteVouchersOrCoupons(params: ["customer_id": BBQUserDefaults.sharedInstance.customerId,
                                                                            "branch_id": BBQUserDefaults.sharedInstance.DeliveryBranchIdValue,
                                                                            "barcode": barcode]) { (error, response) in
            UIUtils.hideLoader()
            if let response = response {
                if response.message_type == "success" {
                    let tenderKey = response.data?.tender_type ?? params_tender_key ?? "" == "GC" ? "COUPON" : "VOUCHER"
                    AnalyticsHelper.shared.triggerEvent(type: tenderKey == "COUPON" ? .delivery_coupon_verify_success : .delivery_voucher_verify_success)
                    UIUtils.showLoader()
                    BBQServiceManager.getInstance().addVouchersOrCoupons(params: ["customer_id": BBQUserDefaults.sharedInstance.customerId,
                                                                                  "branch_id": BBQUserDefaults.sharedInstance.DeliveryBranchIdValue,
                                                                                  "tender_key": tenderKey,
                                                                                  "amount": response.data?.denomination ?? 0,
                                                                                  "payment_id": "",
                                                                                  "barcode": response.data?.bar_code ?? barcode]) { (error, response) in
                        UIUtils.hideLoader()
                        if let response = response {
                            if response.message_type == "failed" {
                                AnalyticsHelper.shared.triggerEvent(type: tenderKey == "COUPON" ? .delivery_coupon_update_payment_fail : .delivery_voucher_update_payment_fail)
                                UIUtils.showToast(message: response.message)
                            } else {
                                AnalyticsHelper.shared.triggerEvent(type: tenderKey == "COUPON" ? .delivery_coupon_update_payment_success : .delivery_voucher_update_payment_success)
                                UIUtils.showToast(message: "Coupon/Voucher Applied Successfully")
                                completionHandler(error, response, true)
                            }
                        } else {
                            AnalyticsHelper.shared.triggerEvent(type: tenderKey == "COUPON" ? .delivery_coupon_update_payment_fail : .delivery_voucher_update_payment_fail)
                            UIUtils.showToast(message: error?.message ?? UNEXPECTED_ERROR)
                        }
                    }
                } else {
                    let tenderKey = response.data?.tender_type ?? params_tender_key ?? "" == "GC" ? "COUPON" : "VOUCHER"
                    AnalyticsHelper.shared.triggerEvent(type: tenderKey == "COUPON" ? .delivery_coupon_verify_fail : .delivery_voucher_verify_fail)
                    UIUtils.showToast(message: response.message)
                }
            } else {
                UIUtils.showToast(message: error?.description ?? "")
            }
        }
    }
    
    func getCoupons() {
        self.couponsTableView.isHidden = true
        UIUtils.showLoader()
        BBQServiceManager.getInstance().getCouponAndVouchers(params: getParams()) { (error, response) in
            UIUtils.hideLoader()
            if error != nil {
                UIUtils.showToast(message: error?.message ?? "")
            } else {
                self.coupons = response?.data?.coupons ?? [Coupon]()
                self.vouchers = response?.data?.vouchers ?? [Coupon]()
//                self.couponsTableView.reloadData()
                if self.coupons.count == 0 && self.vouchers.count == 0 {
                    self.couponsTableView.isHidden = true
                } else {
                    self.couponsTableView.isHidden = false
                    self.couponsTableView.reloadData()
                }
            }

        }
    }
    
    
    func getParams() -> [String: Any] {
        ["customer_id":BBQUserDefaults.sharedInstance.customerId,
         "order_id": orderId]
    }
}

extension AddCouponsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return vouchers.count
        }
        return coupons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddCouponTableViewCell", for: indexPath) as? AddCouponTableViewCell
        let data: Coupon
        if indexPath.section == 0 {
            data = vouchers[indexPath.row]
        } else {
            data = coupons[indexPath.row]
        }
        cell?.name.text = data.title
        cell?.couponDescription.text = data.couponDescription
        cell?.code.text = data.barcode
        cell?.priceLbl.text = "\(data.denomination)/-"
        cell?.barcode = data.barcode
        cell?.amount = data.denomination
        cell?.tenderKey = data.voucher_type
        cell?.used_in_mobile_app = data.used_in_mobile_app
        cell?.outerView.setShadow(radius: 5)
        if data.used_in_mobile_app {
            cell?.applyBtn.setTitle("APPLIED", for: .normal)
            cell?.deleteBtn.isHidden = false
            cell?.applyBtn.backgroundColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 1)
            cell?.applyBtn.tintColor =  .white
            
        } else {
            cell?.applyBtn.setTitle("APPLY CARD", for: .normal)
            cell?.deleteBtn.isHidden = true
            view.layer.masksToBounds = true
            cell?.applyBtn.setTitleColor(.theme, for: .normal)
            cell?.applyBtn.layer.borderWidth = 1.0;
            cell?.applyBtn.layer.borderColor = UIColor.theme.cgColor
            cell?.applyBtn.backgroundColor = .white
        }
        cell?.delegate = self
        return cell ?? UITableViewCell()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "AVAILABLE HAPPINESS CARD"
        } else {
            return "AVAILABLE COUPONS"
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel(frame: CGRect(x: 10, y: 0, width: UIScreen.main.bounds.width - 20, height: 20))
        let headerView = UIView()
        label.textColor = #colorLiteral(red: 0.5882352941, green: 0.3843137255, blue: 0.3411764706, alpha: 1)
        label.backgroundColor = UIColor.white
        if section == 0 {
            label.text = "AVAILABLE HAPPINESS CARD"
            headerView.addSubview(label)
            if vouchers.count == 0 {
                return UIView()
            }
            return headerView
        } else {
            label.text = "AVAILABLE COUPONS"
            headerView.addSubview(label)
            if coupons.count == 0 {
                return UIView()
            }
            return headerView
        }
    }
    
    
}


extension AddCouponsViewController: AddCouponTableViewCellDelegate {
    func actionOnApplyBtn(code: String, amount: Int, tenderKey: String) {
        validateCouponCode(barcode: code, params_tender_key: tenderKey) { (error, response, isSuccess) in
            if isSuccess{
                self.navigationController?.popViewController(animated: true)
            }
        }
//        UIUtils.showLoader()
//        BBQServiceManager.getInstance().addVouchersOrCoupons(params: ["customer_id": BBQUserDefaults.sharedInstance.customerId,
//                                                                      "branch_id": BBQUserDefaults.sharedInstance.branchIdValue,
//                                                                      "tender_key": tenderKey,
//                                                                      "amount": amount,
//                                                                      "payment_id": "",
//                                                                      "barcode": code]) { (error, response) in
//            UIUtils.hideLoader()
//            if let response = response {
//                if response.message_type == "failed" {
//                    UIUtils.showToast(message: response.message)
//                } else {
//                    UIUtils.showToast(message: "Coupon/Voucher Applied Successfully")
//                    self.navigationController?.popViewController(animated: true)
//                }
//            } else {
//                UIUtils.showToast(message: error?.message ?? UNEXPECTED_ERROR)
//            }
//        }
    }
    
    func actionOnDeleteBtn(code: String, tenderKey: String) {
        UIUtils.showLoader()
        BBQServiceManager.getInstance().removeVouchers(params:
                                                        ["customer_id":BBQUserDefaults.sharedInstance.customerId,
                                                         "branch_id":BBQUserDefaults.sharedInstance.DeliveryBranchIdValue,
                                                         "tender_key": tenderKey,
                                                         "barcode":code]) { (error, response) in
            UIUtils.hideLoader()
            if let response = response {
                if response.message_type == "failed" {
                    UIUtils.showToast(message: response.message)
                } else {
                    UIUtils.showToast(message: "Coupon/Voucher Removed Successfully")
                    self.navigationController?.popViewController(animated: true)
                }
            } else {
                UIUtils.showToast(message: error?.message ?? UNEXPECTED_ERROR)
            }

        }
    }
    
}
