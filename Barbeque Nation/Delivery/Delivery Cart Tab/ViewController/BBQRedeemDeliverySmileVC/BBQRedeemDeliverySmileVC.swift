//
 //  Created by Mahmadsakir on 16/12/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified BBQRedeemSmilesViewController.swift
 //

import UIKit

class BBQRedeemDeliverySmileVC: UIViewController {
    
    //MARK:- View Properties
    @IBOutlet weak var txtRedeemPoint: UITextField!
    @IBOutlet weak var btnRedeemPoint: UIButton!
    @IBOutlet weak var lblAvailableSmile: UILabel!
    
    //MARK:- Class properties
    var delegate: BBQRedeemDeliverySmileDelegate?
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        setTheme()
        setData()
    }

    private func setTheme() {
        btnRedeemPoint.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0)
        btnRedeemPoint.applyMultiBrandTheme()
        btnRedeemPoint.dropShadow()
    }
    
    private func setData(){
        lblAvailableSmile.text = "You have \(BBQUserDefaults.sharedInstance.UserLoyaltyPoints) smiles available"
    }
    
    private func unSelectButton(_ sender: UIButton){
        sender.isSelected = false
        sender.alpha = 0.5
    }
    
    private func selectButton(_ sender: UIButton){
        sender.isSelected = true
        sender.alpha = 1.0
    }
    
    //MARK:- Button Action
    
    @IBAction func onClickBtnRedeemNow(_ sender: Any) {
        guard let usingPoints = Int(txtRedeemPoint.text ?? "0") else{
            UIUtils.showToast(message: "Please enter smiles")
            return
        }
        
        if usingPoints <= BBQUserDefaults.sharedInstance.UserLoyaltyPoints, usingPoints > 0{
            self.dismiss(animated: true, completion: nil)
            self.delegate?.onSmileApplied(amount: usingPoints)
        }
    }
    @IBAction func onEditingOfRedeemAmount(_ sender: Any) {
        guard let usingPoints = Int(txtRedeemPoint.text ?? "0") else{
            btnRedeemPoint.isEnabled = false
            btnRedeemPoint.alpha = 0.5
            return
        }
        
        if usingPoints <= BBQUserDefaults.sharedInstance.UserLoyaltyPoints, usingPoints > 0{
            btnRedeemPoint.isEnabled = true
            btnRedeemPoint.alpha = 1.0
        }else{
            btnRedeemPoint.isEnabled = false
            btnRedeemPoint.alpha = 0.5
        }
    }
    
    @IBAction func onClickBtnClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

protocol BBQRedeemDeliverySmileDelegate{
    func cancel()
    func onSmileApplied(amount: Int)
}
