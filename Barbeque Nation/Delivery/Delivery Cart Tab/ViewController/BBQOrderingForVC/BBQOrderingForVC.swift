//
 //  Created by Mahmadsakir on 16/12/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified BBQRedeemSmilesViewController.swift
 //

import UIKit
import SkyFloatingLabelTextField
import ContactsUI

class BBQOrderingForVC: UIViewController {
    
    //MARK:- View Properties
    @IBOutlet weak var txtName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtMobile: SkyFloatingLabelTextField!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnMySelf: UIButton!
    @IBOutlet weak var lblContactInfo: UILabel!
    
    var orderingForName: String?
    var orderingForNumber: String?
    
    //MARK:- Class properties
    var delegate: BBQOrderingForDelegate?
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        setTheme()
        setData()
    }

    private func setTheme() {
        btnSave.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0)
        btnSave.applyMultiBrandTheme()
        btnMySelf.roundCorners(cornerRadius: 5.0, borderColor: .deliveryThemeColor, borderWidth: 1.0)
        btnMySelf.setTitleColor(.deliveryThemeColor, for: .normal)
        lblContactInfo.applyThemeOnText()
        
    }
    
    private func setData(){
        txtName.text = orderingForName
        if orderingForName == nil{
            txtName.text = BBQUserDefaults.sharedInstance.UserName
        }
        txtMobile.text = orderingForNumber
        if orderingForNumber == nil{
            txtMobile.text = SharedProfileInfo.shared.profileData?.mobileNumber
        }
        checkProfileData()
    }
    
    private func unSelectButton(_ sender: UIButton){
        sender.isSelected = false
        sender.alpha = 0.5
    }
    
    private func selectButton(_ sender: UIButton){
        sender.isSelected = true
        sender.alpha = 1.0
    }
    
    //MARK:- Button Action
    @IBAction func onClickBtnContacts(_ sender: Any) {
        let contactPicker = CNContactPickerViewController()
        contactPicker.delegate = self
        contactPicker.displayedPropertyKeys = [CNContactGivenNameKey, CNContactPhoneNumbersKey]
        self.present(contactPicker, animated: true, completion: nil)
    }
    @IBAction func onClickBtnClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickBtnConfirm(_ sender: Any) {
        if txtName.text != BBQUserDefaults.sharedInstance.UserName || txtMobile.text != SharedProfileInfo.shared.profileData?.mobileNumber{
            self.delegate?.onSelctedOrdering(name: txtName.text ?? "", number: txtMobile.text ?? "")
        }
        
        //On click of save changes call an api to save the info of orderring  for
        callApiForOrderingFor()
        self.dismiss(animated: true, completion: nil)
    }
    
    func callApiForOrderingFor() {
        
        if txtName.text != BBQUserDefaults.sharedInstance.UserName || txtMobile.text != SharedProfileInfo.shared.profileData?.mobileNumber {
            //call api to save orderingfor information
            self.delegate?.onSelectingOrderFor(name: txtName.text ?? "", number: txtMobile.text ?? "")
        }
        else{
            self.delegate?.onSelectingOrderFor(name: "", number: "")

        }
    }
    
    @IBAction func onEditingOfName(_ sender: Any) {
        checkProfileData()
    }
    @IBAction func onEditingOfNumber(_ sender: Any) {
        checkProfileData()
    }
    
    @IBAction func onClickBtnMySelf(_ sender: Any) {
        self.delegate?.onSelectedMySelf()
        self.dismiss(animated: true, completion: nil)
    }
    private func checkProfileData() {
        if txtName.text?.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "-", with: "").count ?? 0 > 0, txtMobile.text?.count ?? 0 >= 10{
            enableButton(isEnable: true)
        }else{
            enableButton(isEnable: false)
        }
    }
   
    private func enableButton(isEnable: Bool){
        if isEnable{
            btnSave.alpha = 1.0
        }else{
            btnSave.alpha = 0.5
        }
        btnSave.isEnabled = isEnable
    }
    
    //MARK:- Web Service
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension BBQOrderingForVC: CNContactPickerDelegate{
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        txtName.text = contact.givenName + " " + contact.familyName
        if contact.phoneNumbers.count > 0{
            txtMobile.text = contact.phoneNumbers[0].value.stringValue.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "-", with: "").replacingOccurrences(of: "(", with: "").replacingOccurrences(of: ")", with: "")
        }
        checkProfileData()
    }
}
extension BBQOrderingForVC: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if txtName == textField{
            if textField.text?.replacingOccurrences(of: " ", with: "").count ?? 0 > 0{
                txtMobile.becomeFirstResponder()
                return true
            }
            return false
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if txtMobile == textField{
            return textField.maxCharacter(maxLength: 10, shouldChangeCharactersIn: range, replacementString: string)
        }
        return true
    }
}

protocol BBQOrderingForDelegate{
    func onSelctedOrdering(name: String, number: String)
    func onSelectedMySelf()
    func onSelectingOrderFor(name: String, number: String)
    
}
