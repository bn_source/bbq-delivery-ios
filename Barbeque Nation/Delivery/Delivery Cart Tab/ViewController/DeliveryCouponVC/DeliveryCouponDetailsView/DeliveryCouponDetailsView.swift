//
 //  Created by Mahmadsakir on 11/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified AddressConfirmationVC.swift
 //

import UIKit

protocol DeliveryCouponDetailsDelegate{
    func onHiddenView(view: DeliveryCouponDetailsView)
    func onClickBtnApplyCoupon(code: String, amount: Int, tenderKey: String)
}

class DeliveryCouponDetailsView: UIView {

    //MARK:- Outlets
    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var backgroundAlpha: UIView!
    @IBOutlet weak var stackViewForContainer: UIStackView!
    @IBOutlet weak var imgCoupon: UIImageView!
    @IBOutlet weak var lblCouponTitlte: UILabel!
    @IBOutlet weak var lblCouponUse: UILabel!
    @IBOutlet weak var lblCouponCode: UILabel!
    @IBOutlet weak var lblCouponDetails: UILabel!
    @IBOutlet weak var heightForScrollView: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var btnApply: UIButton!
    
    //MARK:- Variable
    var coupon: Coupon!
    var delegate: DeliveryCouponDetailsDelegate?
    
    //MARK:- View Configuration
    func initWith(coupon: Coupon, isEnabled: Bool, isApplied: Bool, delegate: DeliveryCouponDetailsDelegate?, logo_url: String) -> DeliveryCouponDetailsView {
        let view = loadFromNib()!
        view.coupon = coupon
        view.delegate = delegate
        view.viewForContainer.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        view.setData(isEnabled: isEnabled, isApplied: isApplied, logo_url: logo_url)
        return view
    }
        
        
    func loadFromNib() -> DeliveryCouponDetailsView? {
        if let views = Bundle.main.loadNibNamed("DeliveryCouponDetailsView", owner: self, options: nil), let view = views[0] as? DeliveryCouponDetailsView{
            view.frame = UIApplication.shared.windows[0].bounds
            return view
        }
        return nil
    }
    
    func addToSuperView(view: UIViewController?) {
        if let tabBarController = view?.navigationController?.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
            tabBarController.tabBar.isHidden = true
        }
        
        if let view = view{
            view.view.addSubview(self)
        }else{
            getMainView().addSubview(self)
        }
        stackViewForContainer.isHidden = true
        self.backgroundAlpha.alpha = 0.0
        self.scrollView.isHidden = true
        UIView.animate(withDuration: 0.15, delay: 0, options: .curveLinear) {
            self.stackViewForContainer.isHidden = false
            self.backgroundAlpha.alpha = 0.5
        } completion: { (isCompleted) in
            self.stackViewForContainer.isHidden = false
            self.heightForScrollView.constant = self.lblCouponDetails.bounds.size.height
            UIView.animate(withDuration: 0.15, delay: 0, options: .curveLinear) {
                self.scrollView.isHidden = false
            } completion: { (isCompleted) in
                self.scrollView.isHidden = false
            }
            
        }
    }
    
    func removeToSuperView() {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
            self.stackViewForContainer.isHidden = true
            self.backgroundAlpha.alpha = 0.0
            self.layoutIfNeeded()
        } completion: { (isCompleted) in
            self.stackViewForContainer.isHidden = true
            self.backgroundAlpha.alpha = 0.0
            self.removeFromSuperview()
        }
    }
    
    func setData(isEnabled: Bool, isApplied: Bool, logo_url: String) {
        lblCouponTitlte.text = coupon.title
        lblCouponUse.text = coupon.couponDescription
        lblCouponCode.text = coupon.barcode
        
        lblCouponDetails.text = ""
        if logo_url != ""{
            imgCoupon.setImagePNG(url: logo_url)
        }
        
        guard let data = coupon.rule_description.data(using: String.Encoding.unicode) else { return }
        do {
            lblCouponDetails.attributedText = try NSAttributedString(data: data,
                                                                     options: [.documentType:NSAttributedString.DocumentType.html],
                                                                     documentAttributes: nil)
            //[NSAttributedString.Key.font: UIFont.systemFontOfSize(size: 14.0, weight: .regular)
        } catch (let errorStr) {
            print(errorStr.localizedDescription)
        }
        lblCouponDetails.textColor = .text
        
        if !isEnabled{
            btnApply.isEnabled = false
            btnApply.alpha = 0.5
        }else{
            btnApply.isEnabled = true
            btnApply.alpha = 1.0
        }
        
        if isApplied{
            btnApply.setTitleColor(.darkGreen, for: .normal)
            btnApply.setTitle("APPLIED", for: .normal)
            btnApply.isUserInteractionEnabled = false
        }else{
            btnApply.setTitleColor(.deliveryThemeColor, for: .normal)
            btnApply.setTitle("APPLY", for: .normal)
            btnApply.isUserInteractionEnabled = true
        }
    }
    
    //MARK:- Button Action Method
    @IBAction func onClickBtnClose(_ sender: Any) {
        delegate?.onHiddenView(view: self)
        removeToSuperView()
    }
    @IBAction func onClickBtnApply(_ sender: Any) {
        delegate?.onClickBtnApplyCoupon(code: coupon.barcode, amount: coupon.denomination, tenderKey: coupon.voucher_type == "GC" ? "COUPON" : "VOUCHER")
        removeToSuperView()
    }
    
}
