//
 //  Created by Rahul S on 16/12/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified AddCouponsViewController.swift
 //

import UIKit

class DeliveryCouponVC: BaseViewController {

    @IBOutlet weak var textFieldView: UIView!
    @IBOutlet weak var couponsTableView:UITableView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var applyBtn: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    var orderId = ""
    var coupons =  [Coupon]()
    var vouchers = [Coupon]()
    var offers = [Coupon]()
    var isForVoucher = false
    var order_type = "Delivery"
    var transaction_type = ""
    var appliedCouoons = [ListTender]()
    var isOfferApplied = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AnalyticsHelper.shared.triggerEvent(type: .CC01)
        textFieldView.setShadow(radius: 5)
        applyBtn.applyMultiBrandTheme()
        getCoupons()
        if isForVoucher{
            textField.placeholder = "Enter voucher code"
            lblTitle.text = "Apply Happiness card"
        }else{
            textField.placeholder = "Enter coupon/voucher code"
            lblTitle.text = "Apply Coupon/Happiness card"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.screenName(name: .Delivery_Coupon_Screen)
    }
    
    @IBAction func actionOnBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func actionOnApplyButton(_ sender: Any) {
        if textField!.text!.isEmpty {
            UIUtils.showToast(message: "Please enter the coupon/voucher code")
        } else {
            validateCouponCode(barcode: textField.text ?? "", params_tender_key: "") { (error, response, isSuccess) in
                if isSuccess{
                    AnalyticsHelper.shared.triggerEvent(type: .delivery_manual_coupon_code_apply)
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    private func triggerApplyEventClicked(tender_key: String){
        if tender_key == "GC" || tender_key == "COUPON"{
            AnalyticsHelper.shared.triggerEvent(type: .C12)
            AnalyticsHelper.shared.triggerEvent(type: .CC04)
        }else if tender_key == "GV" || tender_key == "VOUCHER"{
            AnalyticsHelper.shared.triggerEvent(type: .C14)
            AnalyticsHelper.shared.triggerEvent(type: .CC05)
        }else if tender_key == "OFFER"{
            AnalyticsHelper.shared.triggerEvent(type: .C20)
            AnalyticsHelper.shared.triggerEvent(type: .CC09)
        }else{
            AnalyticsHelper.shared.triggerEvent(type: .C16)
            AnalyticsHelper.shared.triggerEvent(type: .CC06)
        }
    }
    
    private func triggerSuccessEventClicked(tender_key: String, old_key: String){
        if old_key == ""{
            AnalyticsHelper.shared.triggerEvent(type: .C16A)
            AnalyticsHelper.shared.triggerEvent(type: .CC06A)
        }
        if tender_key == "GC" || tender_key == "COUPON"{
            AnalyticsHelper.shared.triggerEvent(type: .C12A)
            AnalyticsHelper.shared.triggerEvent(type: .CC04A)
        }else if tender_key == "GV" || tender_key == "VOUCHER"{
            AnalyticsHelper.shared.triggerEvent(type: .C14A)
            AnalyticsHelper.shared.triggerEvent(type: .CC05A)
        }else if tender_key == "OFFER"{
            AnalyticsHelper.shared.triggerEvent(type: .C20A)
            AnalyticsHelper.shared.triggerEvent(type: .CC09A)
        }
    }
    
    private func triggerFailureEventClicked(tender_key: String, old_key: String){
        if old_key == ""{
            AnalyticsHelper.shared.triggerEvent(type: .C16B)
            AnalyticsHelper.shared.triggerEvent(type: .CC06B)
        }
        if tender_key == "GC" || tender_key == "COUPON"{
            AnalyticsHelper.shared.triggerEvent(type: .C12B)
            AnalyticsHelper.shared.triggerEvent(type: .CC04B)
        }else if tender_key == "GV" || tender_key == "VOUCHER"{
            AnalyticsHelper.shared.triggerEvent(type: .C14B)
            AnalyticsHelper.shared.triggerEvent(type: .CC05B)
        }else if tender_key == "OFFER"{
            AnalyticsHelper.shared.triggerEvent(type: .C20B)
            AnalyticsHelper.shared.triggerEvent(type: .CC09B)
        }
    }
    
    func validateCouponCode(barcode: String, params_tender_key: String?, completionHandler:@escaping (_ error: BBQError?, _ response: AddCouponResponseModel?, _ isSuccess: Bool) -> Void){
        triggerApplyEventClicked(tender_key: params_tender_key ?? "")
        UIUtils.showLoader()
        BBQServiceManager.getInstance().validatteVouchersOrCoupons(params: ["customer_id": BBQUserDefaults.sharedInstance.customerId,
                                                                            "branch_id": BBQUserDefaults.sharedInstance.DeliveryBranchIdValue,
                                                                            "barcode": barcode,
                                                                            "sales_sub_type": order_type]) { (error, response) in
            UIUtils.hideLoader()
            if let response = response {
                if response.message_type == "success" {
                    let dataTenderKey = response.data?.tender_type ?? params_tender_key ?? ""
                    //_ = String(format: "%li", response.data?.denomination ?? 0)
                    let tenderKey = dataTenderKey == "GC" ? "COUPON" : "VOUCHER"
                    let promocode = response.data?.promocode ?? barcode
                    let barcode = response.data?.bar_code ?? barcode
                    if tenderKey == "COUPON"{
                        self.removeAppliedCoupons()
                    }
                    AnalyticsHelper.shared.triggerEvent(type: tenderKey == "COUPON" ? .delivery_coupon_verify_success : .delivery_voucher_verify_success)
                    self.applyCouponCode(tenderKey: tenderKey, amount: response.data?.denomination ?? 0, barcode: barcode, promocode: promocode, dataTenderKey: dataTenderKey, params_tender_key: params_tender_key ?? "") { error, response, isAdded in
                        completionHandler(error, response, isAdded)
                    }
                } else {
                    let dataTenderKey = response.data?.tender_type ?? params_tender_key ?? ""
                    let tenderKey = dataTenderKey == "GC" ? "COUPON" : "VOUCHER"
                    self.triggerFailureEventClicked(tender_key: dataTenderKey, old_key: params_tender_key ?? "")
                    AnalyticsHelper.shared.triggerEvent(type: tenderKey == "COUPON" ? .delivery_coupon_verify_fail : .delivery_voucher_verify_fail)
                    UIUtils.showToast(message: response.message)
                }
            } else {
                self.triggerFailureEventClicked(tender_key: params_tender_key ?? "", old_key: params_tender_key ?? "")
                UIUtils.showToast(message: error?.description ?? "")
            }
        }
    }
    
    private func applyCouponCode(tenderKey: String, amount: Int, barcode: String, promocode: String, dataTenderKey: String, params_tender_key: String, completionHandler:@escaping (_ error: BBQError?, _ response: AddCouponResponseModel?, _ isSuccess: Bool) -> Void){
        UIUtils.showLoader()
        BBQServiceManager.getInstance().addVouchersOrCoupons(params: ["customer_id": BBQUserDefaults.sharedInstance.customerId,
                                                                      "branch_id": BBQUserDefaults.sharedInstance.DeliveryBranchIdValue,
                                                                      "tender_key": tenderKey,
                                                                      "amount": amount,
                                                                      "payment_id": "",
                                                                      "barcode": barcode,
                                                                      "promocode": promocode,
                                                                      "sales_sub_type": order_type]) { (error, response) in
            UIUtils.hideLoader()
            if let response = response {
                if response.message_type == "failed" {
                    AnalyticsHelper.shared.triggerEvent(type: tenderKey == "COUPON" ? .delivery_coupon_update_payment_fail : .delivery_voucher_update_payment_fail)
                    self.triggerFailureEventClicked(tender_key: dataTenderKey, old_key: params_tender_key )
                    UIUtils.showToast(message: response.message)
                } else {
                    AnalyticsHelper.shared.triggerEvent(type: tenderKey == "COUPON" ? .delivery_coupon_update_payment_success : .delivery_voucher_update_payment_success)
                    self.triggerSuccessEventClicked(tender_key: dataTenderKey, old_key: params_tender_key )
                    deliveryCartAmount = String(amount)
                    deliveryCartType = dataTenderKey
                    deliveryCartCode = promocode
                    UIUtils.showToast(message: "Coupon/Voucher Applied Successfully")
                    completionHandler(error, response, true)
                }
            } else {
                self.triggerFailureEventClicked(tender_key: dataTenderKey, old_key: params_tender_key )
                AnalyticsHelper.shared.triggerEvent(type: tenderKey == "COUPON" ? .delivery_coupon_update_payment_fail : .delivery_voucher_update_payment_fail)
                UIUtils.showToast(message: error?.message ?? UNEXPECTED_ERROR)
            }
        }
    }
    
    private func removeAppliedCoupons(){
        for tender in appliedCouoons{
            if tender.tender_key == "COUPON" || tender.tender_key == "OFFER"{
                self.removeCoupon(code: tender.barcode, tender_key: tender.tender_key)
            }
        }
    }
    
    private func removeCoupon(code: String, tender_key: String) {
//        UIUtils.showLoader()
        BBQServiceManager.getInstance().removeVouchers(params:
                                                        ["customer_id":BBQUserDefaults.sharedInstance.customerId,
                                                         "branch_id":BBQUserDefaults.sharedInstance.DeliveryBranchIdValue,
                                                         "tender_key":tender_key,
                                                         "barcode":code]) { (error, response) in
//            UIUtils.hideLoader()
        }
    }
    
    func getCoupons() {
        //self.couponsTableView.isHidden = true
        UIUtils.showLoader()
        BBQServiceManager.getInstance().getCouponAndVouchers(params: getParams()) { (error, response) in
            UIUtils.hideLoader()
            if error != nil {
                UIUtils.showToast(message: error?.message ?? "")
            } else {
                self.coupons = response?.data?.coupons ?? [Coupon]()
                self.vouchers = response?.data?.vouchers ?? [Coupon]()
                self.offers = response?.data?.offers ?? [Coupon]()
                if self.coupons.count > 0{
                    AnalyticsHelper.shared.triggerEvent(type: .CC02)
                }else{
                    AnalyticsHelper.shared.triggerEvent(type: .CC02A)
                }
                if self.vouchers.count > 0{
                    AnalyticsHelper.shared.triggerEvent(type: .CC03)
                }else{
                    AnalyticsHelper.shared.triggerEvent(type: .CC03A)
                }
//                self.couponsTableView.reloadData()
                if self.coupons.count == 0 && self.vouchers.count == 0 && self.offers.count == 0 {
                    //self.couponsTableView.isHidden = true
                } else {
                    //self.couponsTableView.isHidden = false
                    self.couponsTableView.reloadData()
                }
            }
            
        }
    }
    
    
    func getParams() -> [String: Any] {
        ["customer_id":BBQUserDefaults.sharedInstance.customerId,
         "order_id": orderId,
         "sales_sub_type": order_type]
    }
}

extension DeliveryCouponVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isForVoucher{
            return vouchers.count
        }else{
            if section == 1{
                if offers.count > 0{
                    return offers.count
                }else{
                    return vouchers.count
                }
            }else if section == 2{
                return vouchers.count
            }
            return coupons.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DeliveryCouponCell", for: indexPath) as? DeliveryCouponCell
        let data: Coupon
        if isForVoucher {
            data = vouchers[indexPath.row]
        } else {
            if indexPath.section == 1{
                if offers.count > 0{
                    data = offers[indexPath.row]
                }else{
                    data = vouchers[indexPath.row]
                }
            }else if indexPath.section == 2{
                data = vouchers[indexPath.row]
            }else{
                data = coupons[indexPath.row]
            }
        }
        cell?.name.text = data.title
        cell?.couponDescription.text = data.couponDescription
        cell?.code.text = data.barcode
        cell?.priceLbl.text = "\(data.denomination)/-"
        cell?.barcode = data.barcode
        cell?.amount = data.denomination
        cell?.tenderKey = data.voucher_type
        cell?.used_in_mobile_app = data.used_in_mobile_app
        cell?.outerView.setShadow(radius: 5)
        cell?.coupon = data
        
        if data.sales_sub_type != "" && (data.sales_sub_type != order_type && data.sales_sub_type != transaction_type){
            cell?.applyBtn.isEnabled = false
            cell?.applyBtn.alpha = 0.5
        }else{
            cell?.applyBtn.isEnabled = true
            cell?.applyBtn.alpha = 1.0
        }
        
//        if data.used_in_mobile_app {
//            cell?.applyBtn.setTitle("APPLIED", for: .normal)
//            cell?.deleteBtn.isHidden = false
//            cell?.applyBtn.backgroundColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 1)
//            cell?.applyBtn.tintColor =  .white
//
//        } else {
//            cell?.applyBtn.setTitle("APPLY", for: .normal)
//            cell?.deleteBtn.isHidden = true
//            view.layer.masksToBounds = true
//            cell?.applyBtn.setTitleColor(.theme, for: .normal)
//            cell?.applyBtn.layer.borderWidth = 1.0;
//            cell?.applyBtn.layer.borderColor = UIColor.theme.cgColor
//            cell?.applyBtn.backgroundColor = .white
//        }
        if checkForAppliedVoucher(coupon: data){
            cell?.applyBtn.setTitleColor(.darkGreen, for: .normal)
            cell?.applyBtn.setTitle("APPLIED", for: .normal)
            cell?.applyBtn.isUserInteractionEnabled = false
        }else{
            cell?.applyBtn.setTitleColor(.deliveryThemeColor, for: .normal)
            cell?.applyBtn.setTitle("APPLY", for: .normal)
            cell?.applyBtn.isUserInteractionEnabled = true
        }
        cell?.delegate = self
        return cell ?? UITableViewCell()
    }
    
    private func checkForAppliedVoucher(coupon: Coupon) -> Bool{
        for voucher in appliedCouoons{
            if coupon.barcode == voucher.promocode{
                return true
            }
        }
        return false
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if isForVoucher{
            return 1
        }else{
            if offers.count > 0 && vouchers.count > 0{
                return 3
            }else if offers.count > 0 || vouchers.count > 0{
                return 2
            }
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if isForVoucher {
            return "AVAILABLE HAPPINESS CARD"
        } else {
            if section == 1{
                if offers.count > 0{
                    return "AVAILABLE OFFERS"
                }else{
                    return "AVAILABLE HAPPINESS CARD"
                }
            }else if section == 2{
                return "AVAILABLE HAPPINESS CARD"
            }
            return "AVAILABLE COUPONS"
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel(frame: CGRect(x: 10, y: 0, width: UIScreen.main.bounds.width - 20, height: 30))
        let headerView = UIView()
        label.textColor = UIColor.theme
        label.backgroundColor = UIColor.white
        label.font = UIFont.systemFontOfSize(size: 14.0, weight: .bold)
        if isForVoucher {
            label.text = "AVAILABLE HAPPINESS CARD (" + String(vouchers.count) + ")"
            headerView.addSubview(label)
            if vouchers.count == 0 {
                return UIView()
            }
            return headerView
        } else {
            if section == 1{
                if offers.count > 0{
                    label.text = "AVAILABLE OFFERS (" + String(offers.count) + ")"
                }else{
                    label.text = "AVAILABLE HAPPINESS CARD (" + String(vouchers.count) + ")"
                }
            }else if section == 2{
                label.text = "AVAILABLE HAPPINESS CARD (" + String(vouchers.count) + ")"
            }else{
                label.text = "AVAILABLE COUPONS (" + String(coupons.count) + ")"
            }
            headerView.addSubview(label)
            if coupons.count == 0 {
                return UIView()
            }
            let lblUnderLine = UILabel(frame: CGRect(x: 10, y: 29, width: UIScreen.main.bounds.width - 20, height: 1))
            lblUnderLine.backgroundColor = .theme
            headerView.addSubview(lblUnderLine)
            return headerView
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let label = UILabel(frame: CGRect(x: 10, y: 0, width: UIScreen.main.bounds.width - 20, height: 24))
        let headerView = UIView()
        label.backgroundColor = UIColor.clear
        if isForVoucher {
            label.text = ""
            headerView.addSubview(label)
            if vouchers.count == 0 {
                return UIView()
            }
            return headerView
        } else {
            label.text = ""
            headerView.addSubview(label)
            if coupons.count == 0 {
                return UIView()
            }
            return headerView
        }
    }
    
    
    
    
}


extension DeliveryCouponVC: DeliveryCouponCellDelegate, DeliveryCouponDetailsDelegate {
    func actionOnTermsBtn(code: String, tenderKey: String, index: IndexPath) {
        debugPrint("Used in GiftTableViewCell")
    }
    
    func actionOnApplyBtn(code: String, amount: Int, tenderKey: String) {
        if tenderKey == "OFFER"{
            self.removeAppliedCoupons()
            applyCouponCode(tenderKey: tenderKey, amount: amount, barcode: code, promocode: code, dataTenderKey: "OFFER", params_tender_key: "OFFER") { error, response, isSuccess in
                if isSuccess{
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }else{
            validateCouponCode(barcode: code, params_tender_key: tenderKey) { (error, response, isSuccess) in
                if isSuccess{
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        
//        UIUtils.showLoader()
//        BBQServiceManager.getInstance().addVouchersOrCoupons(params: ["customer_id": BBQUserDefaults.sharedInstance.customerId,
//                                                                      "branch_id": BBQUserDefaults.sharedInstance.branchIdValue,
//                                                                      "tender_key": tenderKey,
//                                                                      "amount": amount,
//                                                                      "payment_id": "",
//                                                                      "barcode": code]) { (error, response) in
//            UIUtils.hideLoader()
//            if let response = response {
//                if response.message_type == "failed" {
//                    UIUtils.showToast(message: response.message)
//                } else {
//                    UIUtils.showToast(message: "Coupon/Voucher Applied Successfully")
//                    self.navigationController?.popViewController(animated: true)
//                }
//            } else {
//                UIUtils.showToast(message: error?.message ?? UNEXPECTED_ERROR)
//            }
//        }
    }
    
    func actionOnDeleteBtn(code: String, tenderKey: String) {
        UIUtils.showLoader()
        BBQServiceManager.getInstance().removeVouchers(params:
                                                        ["customer_id":BBQUserDefaults.sharedInstance.customerId,
                                                         "branch_id":BBQUserDefaults.sharedInstance.DeliveryBranchIdValue,
                                                         "tender_key": tenderKey,
                                                         "barcode":code]) { (error, response) in
            UIUtils.hideLoader()
            if let response = response {
                if response.message_type == "failed" {
                    UIUtils.showToast(message: response.message)
                } else {
                    UIUtils.showToast(message: "Coupon/Voucher Removed Successfully")
                    self.navigationController?.popViewController(animated: true)
                }
            } else {
                UIUtils.showToast(message: error?.message ?? UNEXPECTED_ERROR)
            }

        }
    }
    func onClickBtnDetails(coupon: Coupon, isEnabled: Bool, isApplied: Bool) {
        if coupon.voucher_type == "GC"{
            AnalyticsHelper.shared.triggerEvent(type: .CC07)
        }else{
            AnalyticsHelper.shared.triggerEvent(type: .CC08)
        }
        let deliveryCouponDetailsView = DeliveryCouponDetailsView().initWith(coupon: coupon, isEnabled: isEnabled, isApplied: isApplied, delegate: self, logo_url: "")
        deliveryCouponDetailsView.addToSuperView(view: self)
    }
    func onHiddenView(view: DeliveryCouponDetailsView) {
        
    }
    func onClickBtnApplyCoupon(code: String, amount: Int, tenderKey: String) {
        validateCouponCode(barcode: code, params_tender_key: tenderKey) { (error, response, isSuccess) in
            if isSuccess{
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}
