//
 //  Created by Mahmadsakir on 06/08/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BillDetailsPopViewCell.swift
 //

import UIKit

class BillDetailsPopViewCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblValue: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setCellData(taxbreakup: TaxBreakUp) {
        lblTitle.text = taxbreakup.label
        if taxbreakup.value == "FREE"{
            lblValue.text = "FREE"
            lblValue.textColor = .darkGreen
        }else{
            lblValue.text = getCurrency() + taxbreakup.value
            lblValue.textColor = .text
        }
        
    }
    
}
