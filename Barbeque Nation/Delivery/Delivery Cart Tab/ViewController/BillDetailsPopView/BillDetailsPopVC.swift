//
 //  Created by Mahmadsakir on 06/08/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BillDetailsPopVC.swift
 //

import UIKit

class BillDetailsPopVC: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblBillDetails: UITableView!
    
    var taxBreakUp = [TaxBreakUp]()
    var strTitle = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = strTitle
        tblBillDetails.register(UINib(nibName: "BillDetailsPopViewCell", bundle: nil), forCellReuseIdentifier: "BillDetailsPopViewCell")
//        tblBillDetails.reloadData{
//            var frame = self.view.frame
//            frame.size.height = self.tblBillDetails.contentSize.height + 60
//            self.view.frame = frame
//        }
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        tblBillDetails.reloadData{
//            var frame = self.view.frame
//            frame.size.height = self.tblBillDetails.contentSize.height + 60
//            self.view.frame = frame
//        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension BillDetailsPopVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return taxBreakUp.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BillDetailsPopViewCell", for: indexPath) as! BillDetailsPopViewCell
        cell.setCellData(taxbreakup: taxBreakUp[indexPath.row])
        return cell
    }
}

