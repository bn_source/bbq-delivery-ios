//
 //  Created by Mahmadsakir on 17/02/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified ItemDetailViewController.swift
 //

import UIKit

protocol OfferAppliedViewelegate {
    func actionOnDone(view: OfferAppliedView)
}
class OfferAppliedView: UIView {

    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var backgroundAlpha: UIView!
    @IBOutlet weak var imgSuccess: UIImageView!
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var lblCoupunCode: UILabel!
    @IBOutlet weak var lblCouponAmount: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    
    var delegate: OfferAppliedViewelegate?
    var timer: Timer?
    
    
    
    
    //MARK:- View Configuration
    func initWith(code: String, amount: String, type: String, delegate: OfferAppliedViewelegate) -> OfferAppliedView {
        let view = loadFromNib()!
        view.delegate = delegate
        view.styleUI()
        view.setData(code: code, amount: amount, type: type)
        return view
    }
        
        
    func loadFromNib() -> OfferAppliedView? {
        if let views = Bundle.main.loadNibNamed("OfferAppliedView", owner: self, options: nil), let view = views[0] as? OfferAppliedView{
            view.frame = UIApplication.shared.windows[0].bounds
            return view
        }
        return nil
    }
    
    func addToSuperView(view: UIViewController?) {
        if let view = view{
            view.view.addSubview(self)
        }else{
            getMainView().addSubview(self)
        }
        self.backgroundAlpha.alpha = 0.0
        self.viewForContainer.isHidden = true
        imgBackground.image = nil
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn) {
            self.backgroundAlpha.alpha = 0.5
            self.viewForContainer.isHidden = false
        } completion: { (isCompleted) in
            self.viewForContainer.isHidden = false
            self.startAnimating()
        }
    }
    
    @objc func removeToSuperView() {
        delegate?.actionOnDone(view: self)
        if let timer = timer{
            timer.invalidate()
        }
        timer = nil
        imgSuccess.stopAnimatingGif()
        imgBackground.stopAnimatingGif()
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut) {
            self.viewForContainer.isHidden = true
            self.backgroundAlpha.alpha = 0.0
            self.layoutIfNeeded()
        } completion: { (isCompleted) in
            self.viewForContainer.isHidden = true
            self.backgroundAlpha.alpha = 0.0
            self.removeFromSuperview()
        }
    }
    
    func styleUI() {
        viewForContainer.roundCorners(cornerRadius: 20.0, borderColor: .clear, borderWidth: 0.0)
    }
    
    @IBAction func gestureRecognised(_ sender: Any) {
        self.removeToSuperView()
    }
    
    private func startAnimating(){
        imgSuccess.loadGif(name: "success")
        imgBackground.loadGif(name: "applied")
        timer = Timer.scheduledTimer(timeInterval: 3.5, target: self, selector: #selector(removeToSuperView), userInfo: nil, repeats: false)
    }
    
    private func setData(code: String, amount: String, type: String){
        lblCoupunCode.text = String(format: "%@ applied", code)
        lblCouponAmount.text = String(format: "You saved %@%@", getCurrency(), amount)
        lblMessage.text = ""
        if type == "SMILE"{
            //lblMessage.text = "You have applied smiles"
        }else if type == "GC"{
            lblMessage.text = "Coupon applied"
        }else if type == "GV"{
            lblMessage.text = "Voucher applied"
        }else if type == "OFFER"{
            lblMessage.text = "Offer applied"
        }
        
    }
}
