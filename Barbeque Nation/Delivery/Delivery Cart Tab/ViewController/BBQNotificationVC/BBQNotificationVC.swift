//
 //  Created by Mahmadsakir on 08/10/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQNotificationVC.swift
 //

import UIKit

class BBQNotificationVC: BaseViewController {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblBody: UILabel!
    @IBOutlet weak var noNotifications: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var notificationListArray:NSMutableArray? = nil
    let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
    var timer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnBack.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0.0)
        tableView.register(UINib(nibName: "BBQNotificationsCell", bundle: nil), forCellReuseIdentifier: "BBQNotificationsCell")
        noNotifications.text = kNoNewNotifications
        timer = Timer.scheduledTimer(withTimeInterval:  TimeInterval(Int.random(in: 0..<6) + 5), repeats: false, block: { _ in
            self.timer?.invalidate()
            self.timer = nil
            tabBarSelectedIndex = 0
            self.appDelegate?.loadHomeViewController()
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getDataFromPlistFile()
    }
    
    //load the notification data from the plist
    func getDataFromPlistFile() {
        if let appDelegate = appDelegate {
            self.notificationListArray = appDelegate.loadNotificationData()
        }
//
//        let timeIndexSet = NSMutableIndexSet()
//        if let notificationArray = self.notificationListArray {
//            for (index,data) in notificationArray.enumerated() {
//                let modelDict = data as! NSMutableDictionary
//                if let date_String = modelDict.value(forKey: Constants.App.NotificationPage.expirytime) as? String {
//                    print("date_String------->>",date_String)
//                    if let dateValue = Double(date_String) {
//                        let expiryDate = NSDate(timeIntervalSince1970: dateValue)
//                        if (expiryDate.timeIntervalSinceNow.sign == .minus) {
//                            print("********Expired********")
//                            timeIndexSet.add(index)
//                            modelDict.setValue(true, forKey: Constants.App.NotificationPage.readunread)
//                        }
//                        if (expiryDate.timeIntervalSinceNow.sign == .plus) {
//                            print("********Not Expired********")
//                        }
//                    }
//                }
//            }
//        }
//        if timeIndexSet.count > 0 {
//            // Delete the expired notifications
//            self.notificationListArray?.removeObjects(at: timeIndexSet as IndexSet)
//            if let appDelegate = appDelegate {
//                appDelegate.saveNotificationData(dataArray: self.notificationListArray ?? [])
//            }
//        }
        self.tableView.reloadData()
        //  Display no notifications if count is zero
        if let dataCount = self.notificationListArray?.count, dataCount > 0 {
            self.noNotifications.isHidden = true
        }else{
            self.noNotifications.isHidden = false
        }
    }

    @IBAction func onClickBtnBack(_ sender: Any) {
        self.timer?.invalidate()
        self.timer = nil
    }
    
    @IBAction func onClickBtnReserveTable(_ sender: Any) {
        self.timer?.invalidate()
        self.timer = nil
        tabBarSelectedIndex = 0
        appDelegate?.loadHomeViewController()
    }
    @IBAction func onClickBtnOrderNow(_ sender: Any) {
        self.timer?.invalidate()
        self.timer = nil
        tabBarSelectedIndex = 2
        appDelegate?.loadHomeViewController()
    }
    @IBAction func onClickBtnClearAll(_ sender: Any) {
        appDelegate?.saveNotificationData(dataArray: NSMutableArray())
        getDataFromPlistFile()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension BBQNotificationVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let data = self.notificationListArray else {
            return 0
        }
        return data.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "BBQNotificationsCell", for: indexPath) as? BBQNotificationsCell{
            guard let data = self.notificationListArray else {
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            //let model:NotificationModel
            let modelDict = data[indexPath.row] as! NSMutableDictionary
            if let titleString = modelDict.value(forKey: Constants.App.NotificationPage.title) as? String {
                cell.lblTitle.text = titleString
            }
            if let messageString = modelDict.value(forKey: Constants.App.NotificationPage.body) as? String {
                cell.lblBody.text = messageString
            }
            cell.lblTime.text = ""
            if let messageString = modelDict.value(forKey: "google.c.a.ts") as? CVarArg {
                let timeInterval = Int(String(format: "%@", messageString)) ?? 0
                let date = Date(timeIntervalSince1970: TimeInterval(timeInterval))
                cell.lblTime.text = date.string(format: "dd/MM/yyyy hh:mm a")
            }
            cell.dataDict = modelDict as? [String: Any]
            cell.delegate = self
            cell.imgSmiles.isHidden = true
//            if let channel = modelDict.value(forKey: Constants.App.NotificationPage.channel) as? String {
//                switch channel{
//                case Constants.App.NotificationPage.booking:
//                    cell.imgSmiles.isHidden = false
//                    cell.imgSmiles.image = UIImage(named: Constants.App.NotificationPage.icon_calendar_tick)
//                case Constants.App.NotificationPage.event:
//                    cell.imgSmiles.isHidden = false
//                    cell.imgSmiles.image = UIImage(named: Constants.App.NotificationPage.icon_celebration)
//                case Constants.App.NotificationPage.loyalty:
//                    cell.imgSmiles.isHidden = false
//                    cell.imgSmiles.image = UIImage(named: "image_smiles")
//                case Constants.App.NotificationPage.voucher,Constants.App.NotificationPage.coupen,Constants.App.NotificationPage.promotion,Constants.App.NotificationPage.offer:
//                    cell.imgSmiles.isHidden = false
//                    cell.imgSmiles.image = UIImage(named: Constants.App.NotificationPage.icon_offer)
//                default:
//                    print("")
//                }
//            }
            if let readunread = modelDict.value(forKey: Constants.App.NotificationPage.readunread) as? Bool{
                if readunread == true{
                    cell.lblDotView.isHidden = true
                }else if readunread == false{
                    cell.lblDotView.isHidden = false
                }
            }
            return cell
        }
        return UITableViewCell()
    }
}

extension BBQNotificationVC: BBQNotificationsCellDelegate{
    func deleteNotificationAt(index: Int) {
        appDelegate?.deleteNotificationAt(index: index)
        getDataFromPlistFile()
    }
}

extension AppDelegate{
    func deleteNotificationAt(index: Int){
        let mutableArray = loadNotificationData()
        if mutableArray.count > index{
            mutableArray.removeObject(at: index)
        }
        saveNotificationData(dataArray: mutableArray)
    }
}
