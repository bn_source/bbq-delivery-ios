//
 //  Created by Mahmadsakir on 29/10/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQNotificationCell.swift
 //

import UIKit

class BBQNotificationsCell: UITableViewCell {
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblBody: UILabel!
    @IBOutlet weak var imgSmiles: UIImageView!
    @IBOutlet weak var lblDotView: UILabel!
    @IBOutlet weak var viewForContainer: UIView!
    
    var delegate: BBQNotificationsCellDelegate?
    var dataDict: [String: Any]?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewForContainer.setCornerRadius(10.0)
        viewForContainer.dropShadow()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onClickBtnDelete(_ sender: Any) {
        if let index = dataDict?["index"] as? Int{
            delegate?.deleteNotificationAt(index: index)
        }
    }
}

protocol BBQNotificationsCellDelegate{
    func deleteNotificationAt(index: Int)
}
