//
 //  Created by Rahul S on 18/10/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified ChangeAddressViewController.swift
 //

import UIKit

class ChangeAddressViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textField: UITextField!
    var addressViewModel = AddressViewModel()
    var selectedAddressId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AnalyticsHelper.shared.triggerEvent(type: .CA01)
        // Do any additional setup after loading the view.
        tableView.separatorStyle = .none
        
        textField.setShadow()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.screenName(name: .Delivery_Address_Screen)
        addressViewModel.getSavedAddress(customerId: BBQUserDefaults.sharedInstance.customerId,
                                         branchId: BBQUserDefaults.sharedInstance.DeliveryBranchIdValue, filterWithID: selectedAddressId) {
            self.tableView.reloadData()
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func actionOnAddAddressButton(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .CA04)
        let vc = UIUtils.viewController(storyboardId: "DeliveryCart", vcId: "AddAddressVC")
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionOnBackButton(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .CA05)
        self.navigationController?.popViewController(animated: true)
    }
}

extension ChangeAddressViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if addressViewModel.addressNotDeliverTo.count > 0{
            return 2
        }
        return 1
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel()
        label.font = UIFont.systemFontOfSize(size: 16.0, weight: .bold)
        if section == 0{
            label.textColor = UIColor(red: 5.0/255.0, green: 166.0/255.0, blue: 96.0/255.0, alpha: 1.0)
            label.text = "DELIVERS TO"
        }else{
            label.textColor = UIColor(red: 229.0/255.0, green: 53.0/255.0, blue: 53.0/255.0, alpha: 1.0)
            label.text = "DOES NOT DELIVER TO"
        }
        label.backgroundColor = UIColor.white.withAlphaComponent(0.65)
        return label
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return addressViewModel.addressDeliverTo.count
        }else{
            return addressViewModel.addressNotDeliverTo.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChangeAddressTableViewCell") as! ChangeAddressTableViewCell
        var address: Address?
        if indexPath.section == 0{
            address = addressViewModel.addressDeliverTo[indexPath.row]
        }else{
            address = addressViewModel.addressNotDeliverTo[indexPath.row]
        }
        if let address = address {
            var  addressValue = ""
            if address.tagType == .home{
                addressValue = addressValue.appending("Home\n")
            }else if address.tagType == .work{
                addressValue = addressValue.appending("Work\n")
            }else if !address.tagName.isEmpty{
                addressValue = addressValue.appending("\(address.tagName)\n")
            }
            addressValue = addressValue.appending("\(address.flatNo)")
            if !address.landMark.isEmpty, address.landMark.replacingOccurrences(of: " ", with: "") != "" {
                addressValue = addressValue.appending(", \(address.landMark)")
            }
            if !address.address.isEmpty, address.address != "" {
                addressValue = addressValue.appending(", \(address.address)")
            }
            cell.addressLbl.text = addressValue
            if address.servicable{
                if selectedAddressId != address.addressId{
                    cell.selectedImageView.image = UIImage(named: "icon_unselected_radio_button")
                }else{
                    cell.selectedImageView.image = UIImage(named: "icon_selected_radio_button")
                }
            }else{
                cell.selectedImageView.image = UIImage(named: "icon_orange_warning")
            }
            cell.stackViewForNonServiceable.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0, self.addressViewModel.addressDeliverTo.count > indexPath.row{
            let address = self.addressViewModel.addressDeliverTo[indexPath.row]
            if address.servicable {
                AnalyticsHelper.shared.triggerEvent(type: .CA02)
                UIUtils.showLoader()
                BBQServiceManager.getInstance().updateAddress(params: ["customer_id" : BBQUserDefaults.sharedInstance.customerId,
                                                                            "branch_id": BBQUserDefaults.sharedInstance.DeliveryBranchIdValue,
                                                                            "address_id": address.addressId]) { (error, response) in
                    UIUtils.hideLoader()
                    if error != nil {
                        UIUtils.showToast(message: "An error occurred while updating address")
                    } else if response != nil {
                        AnalyticsHelper.shared.triggerEvent(type: .CA02A)
                        AnalyticsHelper.shared.triggerEvent(type: .C03A)
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            } else {
                AnalyticsHelper.shared.triggerEvent(type: .CA03)
                UIUtils.showToast(message: "Selected address is not servicable")
            }
        }else{
            AnalyticsHelper.shared.triggerEvent(type: .CA03)
            UIUtils.showToast(message: "Selected address is not servicable")
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 24
    }
}
