//
 //  Created by Mahmadsakir on 09/02/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQTakeAwayDayCell.swift
 //

import UIKit

class BBQScheduleDeliveryDayCell: UICollectionViewCell {
    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    private func isSelectedView(isSelected: Bool) {
        lblDay.textColor = isSelected ? .deliveryThemeTextColor : UIColor.text
        lblDate.textColor = isSelected ? .deliveryThemeTextColor : UIColor.text
        viewForContainer.backgroundColor = isSelected ? .deliveryThemeColor : .clear
        viewForContainer.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
    }
    
    func setCellData(isSelected: Bool, date: String, day: String) {
        isSelectedView(isSelected: isSelected)
        lblDay.text = day
        lblDate.text = date
    }

}
