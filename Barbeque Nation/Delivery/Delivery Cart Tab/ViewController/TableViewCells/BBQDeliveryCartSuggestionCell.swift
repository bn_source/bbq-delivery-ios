//
 //  Created by Mahmadsakir on 17/11/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQDeliveryCartSuggestionCell.swift
 //

import UIKit

protocol BBQDeliveryCartSuggestionCellDelegate: AnyObject {
    func actionOnAddItemButton(item: GetCartResponse.Suggestion, indexPath: IndexPath?)
    func actionOnRemoveItemButton(item: GetCartResponse.Suggestion, indexPath: IndexPath?)
    func actionOnImageTapped(item: GetCartResponse.Suggestion, indexPath: IndexPath?, isCameFromSearch: Bool)

}

class BBQDeliveryCartSuggestionCell: UICollectionViewCell {
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var itemDescriptionLabel: UILabel!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var vegNonVegImageView: UIImageView!
    @IBOutlet weak var itemPriceLabel: UILabel!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var customizableLabel: UILabel!
    @IBOutlet weak var lblOldPrice: UILabel!
    @IBOutlet weak var lblPromotion: UILabel!
    @IBOutlet weak var viewForPromotion: CustomDashedView!
    
    weak var delegate: BBQDeliveryCartSuggestionCellDelegate?
    private var item = GetCartResponse.Suggestion()
    private var indexPath: IndexPath?
    
    static func getCellIdentifier() -> String {
        return "BBQDeliveryCartSuggestionCell"
    }
    
    func updateCell(indexPath: IndexPath, item: GetCartResponse.Suggestion) {
        self.item = item
        self.indexPath = indexPath
        updateUI()
        itemNameLabel.text = item.item_name
        itemDescriptionLabel.text = item.item_desc
        itemImageView.setImage(url: item.item_image_path, placeholderImage: nil, isForPromotions: false)
        vegNonVegImageView.image = (item.food_type == .veg ? .veg : .nonVeg)
        itemPriceLabel.text = String(format:"%@%@",getCurrency(),item.item_price)
//        addBtn.backgroundColor = .theme
//        addBtn.isUserInteractionEnabled = true
        customizableLabel.isHidden = true
   
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        itemImageView.image = nil
    }
    
    private func updateUI() {
        
        outerView.setCornerRadius(10)
        outerView.setShadow()
        addBtn.setCornerRadius(5)
        addBtn.applyMultiBrandTheme()
        //addBtn.setShadow()
        updateOfferData()
        itemImageView.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
    }
    
    private func updateOfferData(){
            lblOldPrice.isHidden = true
            viewForPromotion.isHidden = true
    }
    
    @IBAction func actionOnAddButton(_ sender: Any) {
        delegate?.actionOnAddItemButton(item: item, indexPath: indexPath)
    }
    
    @IBAction func actionOnComboItemImageTapped(_ sender: Any) {
        delegate?.actionOnImageTapped(item: item, indexPath: indexPath, isCameFromSearch: false)
    }
}
