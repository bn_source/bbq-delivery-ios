//
 //  Created by Rahul S on 22/11/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified AddAddressTableViewCell.swift
 //

import UIKit

class AddAddressTableViewCell: UITableViewCell {

    @IBOutlet weak var addLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
