//
//  CouponsTableViewCell.swift
//  BBQ
//
//  Created by Rahul S on 06/10/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import UIKit
protocol  CouponsTableViewCellDelegate: AnyObject {
    func removeCoupon(code: String, tender_key: String)
}

class CouponsTableViewCell: UITableViewCell {

    @IBOutlet weak var couponNameLbl: UILabel!
    @IBOutlet weak var couponImage: UIImageView!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblNote: UILabel!
    @IBOutlet weak var btnRemove: UIButton!
    weak var delegate: CouponsTableViewCellDelegate?
    var barCode = ""
    var tender_key = "COUPON"
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func actionOnRemoveCoupon(_ sender: Any) {
        delegate?.removeCoupon(code: barCode, tender_key: tender_key)
    }
    
}
