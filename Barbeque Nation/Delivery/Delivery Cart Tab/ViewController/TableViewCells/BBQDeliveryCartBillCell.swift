//
 //  Created by Mahmadsakir on 06/08/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQDeliveryCartBillCell.swift
 //

import UIKit

class BBQDeliveryCartBillCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnDetails: UIButton!
    @IBOutlet weak var lblValue: UILabel!
    
    var index: Int?
    var taxBreakUp: TaxBreakUp?
    var delegate: BBQDeliveryCartBillCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setCellData(taxBreakUp: TaxBreakUp, index: Int) {
        self.taxBreakUp = taxBreakUp
        self.index = index
        lblTitle.text = taxBreakUp.label
        lblValue?.text = "\(getCurrency())0"
        if let amount = Int( taxBreakUp.value), amount <= 0{
            lblValue?.textColor = .darkGreen
            if amount < 0{
                lblValue?.text = "-\(getCurrency())\(-amount)"
                btnDetails.setTitle("-\(getCurrency())\(-amount)", for: .normal)
            }else if !taxBreakUp.label.lowercased().contains("smile"), !taxBreakUp.label.lowercased().contains("promo"), !taxBreakUp.label.lowercased().contains("discount"),!taxBreakUp.label.lowercased().contains("offer"){
                lblValue?.text = "FREE"
                btnDetails.setTitle("FREE", for: .normal)
            }
        }else{
            lblValue?.textColor = .text
            lblValue?.text = "\(getCurrency())\( taxBreakUp.value )"
            btnDetails.setTitle("\(getCurrency())\( taxBreakUp.value )", for: .normal)
        }
        
        if taxBreakUp.details.count > 0{
            btnDetails?.isHidden = false
            lblValue?.isHidden = true
            let string =  NSAttributedString(string: getCurrency()+taxBreakUp.value, attributes: [.underlineStyle : NSUnderlineStyle.single.rawValue])
            self.btnDetails.setAttributedTitle(string, for: .normal)
        }else{
            btnDetails?.isHidden = true
            lblValue?.isHidden = false
        }
    }
    
    @IBAction func onClickBtnDetails(_ sender: Any) {
        if let index = index, let taxBreakUp = taxBreakUp{
            delegate?.didTapOnBillDetails(cell: self, index: index, taxBreakup: taxBreakUp)
        }
    }
    
}

protocol BBQDeliveryCartBillCellDelegate {
    func didTapOnBillDetails(cell: BBQDeliveryCartBillCell, index: Int, taxBreakup: TaxBreakUp)
}
