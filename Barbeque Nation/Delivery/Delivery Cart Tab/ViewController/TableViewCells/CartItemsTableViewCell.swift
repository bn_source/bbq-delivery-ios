//
//  CartItemsTableViewCell.swift
//  BBQ
//
//  Created by Rahul S on 06/10/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import UIKit

protocol CartItemsTableViewCellDelegate: AnyObject {
    func addItem(itemId: String, itemDetailsID: Int)
    func reduceItem(itemId: String, itemDetailsID: Int)
}


class CartItemsTableViewCell: UITableViewCell {

    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var itemDescription: UILabel!
    @IBOutlet weak var foodTypeImage: UIImageView!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var quantityView: UIView!
    @IBOutlet weak var lblItemPrice: UILabel!
    @IBOutlet weak var viewforContainer: UIView!
    @IBOutlet weak var btnMinus: UIButton!
    @IBOutlet weak var btnPlus: UIButton!
    var itemId = ""
    var itemDetailsID = 0
    weak var delegate: CartItemsTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewforContainer.setCornerRadius(10.0)
        viewforContainer.setShadow(color: .lightGray, opacity: 0.5, offSet: .zero, radius: 10.0, scale: false)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setTheme(){
        quantityView.applyViewMultiBrandTheme()
        btnPlus.applyThemeOnText()
        btnMinus.applyThemeOnText()
        quantityLbl.textColor = .deliveryThemeTextColor
    }
    
    @IBAction func actionOnReduceQuantity(_ sender: Any) {
        delegate?.reduceItem(itemId: itemId, itemDetailsID: itemDetailsID)
    }
    
    @IBAction func actionOnIncreaseQuantity(_ sender: Any) {
        delegate?.addItem(itemId: itemId, itemDetailsID: itemDetailsID)
    }
    
}
