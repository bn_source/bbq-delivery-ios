//
 //  Created by Rahul S on 16/12/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified AddCouponTableViewCell.swift
 //

import UIKit

protocol AddCouponTableViewCellDelegate: AnyObject {
    func actionOnApplyBtn(code: String, amount: Int, tenderKey: String)
    func actionOnDeleteBtn(code: String, tenderKey: String)
}

class AddCouponTableViewCell: UITableViewCell {
    
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var couponDescription: UILabel!
    @IBOutlet weak var code: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var applyBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    weak var delegate: AddCouponTableViewCellDelegate?
    var barcode = ""
    var tenderKey = ""
    var amount = 0
    var used_in_mobile_app = false
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func actionOnApplyBtn(_ sender: Any) {
        if !used_in_mobile_app {
            delegate?.actionOnApplyBtn(code: barcode, amount: amount, tenderKey: tenderKey == "GC" ? "COUPON" : "VOUCHER")
        }
    }
    
    @IBAction func actionOnDeleteBtn(_ sender: Any) {
        delegate?.actionOnDeleteBtn(code: barcode, tenderKey: tenderKey == "GC" ? "COUPON" : "VOUCHER")
    }
    

}
