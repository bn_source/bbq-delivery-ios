//
 //  Created by Mahmadsakir on 17/02/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified ItemDetailViewController.swift
 //

import UIKit

protocol ItemOfferAppliedViewDelegate {
    func actionOnDone(view: ItemOfferAppliedView)
}
class ItemOfferAppliedView: UIView {

    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var lblMessage: UILabel!
    
    var delegate: ItemOfferAppliedViewDelegate?
    var timer: Timer?
    
    
    //MARK:- View Configuration
    func initWith(message: String, delegate: ItemOfferAppliedViewDelegate) -> ItemOfferAppliedView {
        let view = loadFromNib()!
        view.delegate = delegate
        view.styleUI()
        view.lblMessage.text = message
        return view
    }
        
    func setmessage(messageReceived: String){
        
        self.lblMessage.text = messageReceived
    }
        
    func loadFromNib() -> ItemOfferAppliedView? {
        if let views = Bundle.main.loadNibNamed("ItemOfferAppliedView", owner: self, options: nil), let view = views[0] as? ItemOfferAppliedView{
            var tempFrame = UIApplication.shared.windows[0].bounds
            tempFrame.origin.y = tempFrame.size.height - 400
            tempFrame.size.height = 400
            view.frame = tempFrame
            return view
        }
        return nil
    }
    
    func addToSuperView(view: UIViewController?) {
        if let view = view{
            view.view.addSubview(self)
        }else{
            getMainView().addSubview(self)
        }
        self.viewForContainer.isHidden = true
        imgBackground.image = nil
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn) {
            self.viewForContainer.isHidden = false
        } completion: { (isCompleted) in
            self.viewForContainer.isHidden = false
            self.startAnimating()
        }
    }
    
    @objc func removeToSuperView() {
        delegate?.actionOnDone(view: self)
        if let timer = timer{
            timer.invalidate()
        }
        timer = nil
        imgBackground.stopAnimatingGif()
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut) {
            self.viewForContainer.isHidden = true
            self.layoutIfNeeded()
        } completion: { (isCompleted) in
            self.viewForContainer.isHidden = true
            self.removeFromSuperview()
        }
    }
    
    func styleUI() {
        viewForContainer.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
        viewForContainer.setShadow()
        
    }
    
    @IBAction func gestureRecognised(_ sender: Any) {
        self.removeToSuperView()
    }
    
    private func startAnimating(){
        imgBackground.loadGif(name: "Item_offer")
        timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(removeToSuperView), userInfo: nil, repeats: false)
    }
    
}
//extension DeliveryCartViewController: ItemOfferAppliedViewDelegate{
//
//    func actionOnDone(view: ItemOfferAppliedView){
//
//        print("actionOnDone(view: ItemOfferAppliedView)")
//    }
//}
