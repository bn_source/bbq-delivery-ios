//
 //  Created by Mahmadsakir on 11/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified AddressConfirmationVC.swift
 //

import UIKit
import SkyFloatingLabelTextField

protocol AddressConfirmationViewDelegate {
    func onHiddenView(view: AddressConfirmationView)
    func onClickChangeAddress()
    func saveAddress(flat: String, landmark: String, tagType: String, tagName: String?, type: Address.TagType?)
}

class AddressConfirmationView: UIView {

    //MARK:- Outlets
    @IBOutlet weak var lblAddressTitle: UILabel!
    @IBOutlet weak var lblAddressDesc: UILabel!
    @IBOutlet weak var txtFlatDetails: SkyFloatingLabelTextField!
    @IBOutlet weak var txtLandamark: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAddressTag: SkyFloatingLabelTextField!
    @IBOutlet weak var viewForHome: UIView!
    @IBOutlet weak var viewForWork: UIView!
    @IBOutlet weak var viewForOtherTag: UIView!
    @IBOutlet weak var lblExtraTagSpace: UILabel!
    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var backgroundAlpha: UIView!
    @IBOutlet weak var stackViewForContainer: UIStackView!
    
    @IBOutlet weak var imgHome: UIImageView!
    @IBOutlet weak var lblHome: UILabel!
    @IBOutlet weak var imgWork: UIImageView!
    @IBOutlet weak var lblWork: UILabel!
    @IBOutlet weak var imgOther: UIImageView!
    @IBOutlet weak var lblOther: UILabel!
    @IBOutlet weak var btnSaveAddress: UIButton!

    
    //MARK:- Variable
    var delegate: AddressConfirmationViewDelegate?
    var selectedTag: Int? {
        didSet{
            enableSaveAddress()
            selectedTagtype = nil
            if let selectedTag = selectedTag{
                switch selectedTag {
                case 0:
                    selectedTagtype = .home
                    break
                    
                case 1:
                    selectedTagtype = .work
                    break
                    
                default:
                    selectedTagtype = .other
                }
            }
        }
    }
    var selectedTagtype: Address.TagType?
    
    //MARK:- View Configuration
    func initWith(addressTitle: String?, AddressString: String?, delegate: AddressConfirmationViewDelegate) -> AddressConfirmationView {
        let view = loadFromNib()!
        view.lblAddressTitle.text = addressTitle
        view.lblAddressDesc.text = AddressString
        view.delegate = delegate
        view.viewForContainer.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        view.styleUI()
        return view
    }
        
        
    func loadFromNib() -> AddressConfirmationView? {
        if let views = Bundle.main.loadNibNamed("AddressConfirmationView", owner: self, options: nil), let view = views[0] as? AddressConfirmationView{
            view.frame = UIApplication.shared.windows[0].bounds
            return view
        }
        return nil
    }
    
    func addToSuperView(view: UIViewController?) {
        if let tabBarController = view?.navigationController?.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
            tabBarController.tabBar.isHidden = true
        }
        
        if let view = view{
            view.view.addSubview(self)
        }else{
            getMainView().addSubview(self)
        }
        stackViewForContainer.isHidden = true
        self.backgroundAlpha.alpha = 0.0
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
            self.stackViewForContainer.isHidden = false
            self.backgroundAlpha.alpha = 0.5
        } completion: { (isCompleted) in
            self.stackViewForContainer.isHidden = false
        }
    }
    
    func removeToSuperView() {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
            self.stackViewForContainer.isHidden = true
            self.backgroundAlpha.alpha = 0.0
            self.layoutIfNeeded()
        } completion: { (isCompleted) in
            self.stackViewForContainer.isHidden = true
            self.backgroundAlpha.alpha = 0.0
            self.removeFromSuperview()
        }
    }
    
    func styleUI() {
        viewForHome.roundCorners(cornerRadius: viewForHome.frame.size.height/2.0, borderColor: .text, borderWidth: 1.0)
        viewForWork.roundCorners(cornerRadius: viewForWork.frame.size.height/2.0, borderColor: .text, borderWidth: 1.0)
        viewForOtherTag.roundCorners(cornerRadius: viewForOtherTag.frame.size.height/2.0, borderColor: .text, borderWidth: 1.0)
        btnSaveAddress.alpha = 0.5
        btnSaveAddress.isEnabled = false
    }
    
    //MARK:- Button Action Method
    @IBAction func onClickBtnConfirm(_ sender: Any) {
        var tagType = ""
        var tagName: String?
        if selectedTag == 0 {
            tagType = "home"
        }else if selectedTag == 1 {
            tagType = "work"
        }else if selectedTag == 2 {
            tagType = "other"
            tagName = txtAddressTag.text
        }
        if tagType == ""{
            return
        }
        delegate?.saveAddress(flat: txtFlatDetails.text ?? "", landmark: txtLandamark.text ?? "", tagType: tagType, tagName: tagName, type: selectedTagtype)
    }
    @IBAction func onClickBtnChange(_ sender: Any) {
        delegate?.onClickChangeAddress()
    }
    
    @IBAction func onClickBtnHome(_ sender: Any) {
        if let selectedTag = selectedTag, selectedTag == 0{
            return
        }
        enableDisabledTag(index: 0)
        selectedTag = 0
    }
    @IBAction func onClickBtnWork(_ sender: Any) {
        if let selectedTag = selectedTag, selectedTag == 1{
            return
        }
        enableDisabledTag(index: 1)
        selectedTag = 1
    }
    @IBAction func onClickBtnOtherTag(_ sender: Any) {
        if let selectedTag = selectedTag, selectedTag == 2{
            enableDisabledTag(index: 3)
            self.selectedTag = nil
        }else{
            enableDisabledTag(index: 2)
            selectedTag = 2
        }
        
    }
    
    
    @IBAction func onClickBtnClose(_ sender: Any) {
        delegate?.onHiddenView(view: self)
        removeToSuperView()
    }
    
    private func enableDisabledTag(isEnable: Bool, view: UIView, imgView: UIImageView, label: UILabel, index: Int){
        if isEnable{
            view.backgroundColor = .theme
            view.layer.borderColor = UIColor.clear.cgColor
            label.textColor = .white
            if index == 0{
                imgView.image = UIImage(named: "icon_address_home_white")
            }else if index == 1{
                imgView.image = UIImage(named: "icon_address_work_white")
            }else if index == 2{
                imgView.image = UIImage(named: "icon_address_other_white")
            }
        }else{
            view.backgroundColor = .clear
            view.layer.borderColor = UIColor.text.cgColor
            label.textColor = .text
            if index == 0{
                imgView.image = UIImage(named: "icon_address_home_black")
            }else if index == 1{
                imgView.image = UIImage(named: "icon_address_work_black")
            }else if index == 2{
                imgView.image = UIImage(named: "icon_address_other_black")
            }
        }
        
        
    }
    
    func enableDisabledTagAction(index: Int){
        enableDisabledTag(isEnable: false, view: viewForHome, imgView: imgHome, label: lblHome, index: 0)
        enableDisabledTag(isEnable: false, view: viewForWork, imgView: imgWork, label: lblWork, index: 1)
        enableDisabledTag(isEnable: false, view: viewForOtherTag, imgView: imgOther, label: lblOther, index: 2)
        txtAddressTag.isHidden = true
        lblExtraTagSpace.isHidden = false
        viewForHome.isHidden = false
        viewForWork.isHidden = false
        
        switch index {
        case 0:
            enableDisabledTag(isEnable: true, view: viewForHome, imgView: imgHome, label: lblHome, index: 0)
            break
        case 1:
            enableDisabledTag(isEnable: true, view: viewForWork, imgView: imgWork, label: lblWork, index: 1)
            break
        case 2:
            enableDisabledTag(isEnable: true, view: viewForOtherTag, imgView: imgOther, label: lblOther, index: 2)
            viewForWork.isHidden = true
            viewForHome.isHidden = true
            txtAddressTag.isHidden = false
            lblExtraTagSpace.isHidden = true
            break
        default: break
            
        }
    }
    
    private func enableDisabledTag(index: Int){
        if selectedTag ?? 0 == 2 || index == 2 {
            UIView.animate(withDuration: 0.2) {
                self.enableDisabledTagAction(index: index)
            }
        }else{
            self.enableDisabledTagAction(index: index)
        }
        
        
    }

    
    //MARK:- On Editing Of Textfield
    @IBAction func onEditingFlatDetails(_ sender: Any) {
        enableSaveAddress()
    }
    @IBAction func onEditingLandmark(_ sender: Any) {
        enableSaveAddress()
    }
    @IBAction func onEditingOtherTag(_ sender: Any) {
        enableSaveAddress()
    }
    
    private func enableSaveAddress(){
        if (txtLandamark.text?.replacingOccurrences(of: " ", with: "").count ?? 0 == 0) || (txtFlatDetails.text?.replacingOccurrences(of: " ", with: "").count ?? 0 == 0) || selectedTag == nil || (selectedTag == 2 && txtAddressTag.text?.replacingOccurrences(of: " ", with: "").count ?? 0 == 0){
            btnSaveAddress.alpha = 0.5
            btnSaveAddress.isEnabled = false
        }else{
            btnSaveAddress.alpha = 1.0
            btnSaveAddress.isEnabled = true
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
