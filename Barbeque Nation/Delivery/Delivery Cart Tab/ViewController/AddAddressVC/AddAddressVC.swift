//
//  AddAddressViewController.swift
//
//  Created by Mahmadsakir on 11/01/21
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
//  All rights reserved.

import UIKit
import GoogleMaps
import SwiftyJSON
import GooglePlaces


class AddAddressVC: BaseViewController, GMSMapViewDelegate, GMSAutocompleteViewControllerDelegate, LocationManagerDelegate {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var mapView: UIView!
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var pinImage: UIImageView!
    @IBOutlet weak var addressViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblFullAddress: UILabel!
    var cartScreenDelegate : AddAddressOnCartVCDelegate?
    public var longitude:Double = 77.38066792488098
    public var latitude:Double = 28.6517752463408
    var GoogleMapView:GMSMapView!
    var geoCoder :CLGeocoder!
    private var placesClient: GMSPlacesClient!
    var googlePlaces: [GooglePlaceRes] = []
    var addressViewModel = AddressViewModel()
    var locationManager = CLLocationManager()
    var isSelectedFromList = false
    var timer: Timer?
    var searchText = ""
    var addressSearchPlaceView: AddressSearchPlaceView?
    var addressConfirmationView: AddressConfirmationView?
    var address: Address?
    var mapSelectedAddress: String = ""
    var isSelectedFromMap = true
    var isFromCartScreen = false
    
    @IBOutlet weak var lblHeader: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        AnalyticsHelper.shared.triggerEvent(type: .AE01)
        if address == nil{
            AnalyticsHelper.shared.triggerEvent(type: .AE01A)
        }else{
            AnalyticsHelper.shared.triggerEvent(type: .AE01B)
        }
        
        if isFromCartScreen {
            lblHeader.text = "Your New Location"
        }else{
            lblHeader.text = "Your Current Location"
        }
        // Do any additional setup after loading the view.
        self.tabBarController?.hidesBottomBarWhenPushed = true
        self.navigationController?.navigationBar.isHidden = true
        
        if (locationManager.location?.coordinate.latitude ?? BBQUserDefaults.sharedInstance.currentLatitudeValue) != 0{
            latitude = locationManager.location?.coordinate.latitude ?? BBQUserDefaults.sharedInstance.currentLatitudeValue
        }
        if (locationManager.location?.coordinate.longitude ?? BBQUserDefaults.sharedInstance.currentLongitudeValue) != 0{
            longitude = locationManager.location?.coordinate.longitude ?? BBQUserDefaults.sharedInstance.currentLongitudeValue
        }
        
        
        geoCoder = CLGeocoder()
        handleSwipesUp()
        placesClient = GMSPlacesClient.shared()
        LocationManager.sharedInstance.delegate = self
        if let latitude = address?.latitude, let value =  Double(latitude), let longitude = address?.longitude, let value1 = Double(longitude){
            self.latitude = value
            self.longitude = value1
            self.isSelectedFromList = true
            lblAddress.text = address?.address.components(separatedBy: ",").first
            lblFullAddress.text = address?.address
            mapSelectedAddress = address?.address ?? ""
            SetUpMap(latitude: value, longitude: value1)
        }else{
            LocationManager.sharedInstance.initLocationManager()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.screenName(name: .Add_Edit_Address_Screen)
        addressView.setCornerRadius(20)
        addressView.setShadow(color: UIColor.hexStringToUIColor(hex: "55606D1A") , opacity: 0.2, offSet: .zero, radius: 7, scale: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let timer = timer{
            timer.invalidate()
        }
        
        timer = nil
    }
    
    
    @IBAction func backButtonPressed(_ sender : UIButton){
                self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionOnCurrentLocation() {
        LocationManager.sharedInstance.delegate = self
        LocationManager.sharedInstance.initLocationManager()
    }

    private func setupSwipeGestures() {
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipesUp))
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipesDown))

        swipeUp.direction = .up
        swipeDown.direction = .down

        addressView.addGestureRecognizer(swipeUp)
        addressView.addGestureRecognizer(swipeDown)
    }

    
    @objc func handleSwipesUp() {
        self.view.layoutIfNeeded()
        addressViewBottomConstraint.constant = 30

        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
                self.view.layoutIfNeeded()
            }, completion: nil)
    }
    
    @objc func handleSwipesDown() {
        self.view.layoutIfNeeded()
        addressViewBottomConstraint.constant = -300

        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
                self.view.layoutIfNeeded()
            }, completion: nil)
    }
    
    
    @IBAction func actionOnBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func actionOnAddAddressButton(_ sender: Any) {
//        UIUtils.showLoader()
//        BBQServiceManager.getInstance().AddCustomerAddress(params: getParams()) { (error, response) in
//            UIUtils.hideLoader()
//            if let response = response {
//                if response.message_type == "failed" {
//                    UIUtils.showToast(message: response.message)
//                } else {
//                    AnalyticsHelper.shared.triggerEvent(type: .added_address_delivery)
//                    UIUtils.showToast(message: "Address added Successfully")
//                    self.navigationController?.popViewController(animated: true)
//                }
//            } else {
//                UIUtils.showToast(message: error?.message ?? UNEXPECTED_ERROR)
//            }
//
//
//        }
        
        let vc = UIUtils.viewController(storyboardId: "DeliveryCart", vcId: "AddressDetailsViewController") as! AddressDetailsViewController
        vc.address = self.address
        vc.delegate = self
        vc.AddressTitleANdDesc = ( lblAddress.text ?? "" , lblFullAddress.text ?? "" )
        self.navigationController?.pushViewController(vc, animated: true)
        
      //  addressConfirmationView = AddressConfirmationView().initWith(addressTitle: lblAddress.text, AddressString: lblFullAddress.text, delegate: self)
        
        
//        if let address = address{
//            vc.txtFlatDetails.text = address.flatNo
//            vc.txtLandamark.text = address.landMark
//            vc.txtAddressTag.text = address.tagName
//            if address.tagType == .home{
//                vc.enableDisabledTagAction(index: 0)
//                vc.selectedTag = 0
//            }else if address.tagType == .work{
//                vc.enableDisabledTagAction(index: 1)
//                vc.selectedTag = 1
//            }else{
//                vc.enableDisabledTagAction(index: 2)
//                vc.selectedTag = 2
//            }
//        }
//        if isSelectedFromMap{
//            AnalyticsHelper.shared.triggerEvent(type: .AE02A)
//        }else{
//            AnalyticsHelper.shared.triggerEvent(type: .AE02B)
//        }
//        addressConfirmationView?.addToSuperView(view: self)
    
    
        return
        
        addressConfirmationView = AddressConfirmationView().initWith(addressTitle: lblAddress.text, AddressString: lblFullAddress.text, delegate: self)
//        if let address = address{
//            addressConfirmationView?.txtFlatDetails.text = address.flatNo
//            addressConfirmationView?.txtLandamark.text = address.landMark
//            addressConfirmationView?.txtAddressTag.text = address.tagName
//            if address.tagType == .home{
//                addressConfirmationView?.enableDisabledTagAction(index: 0)
//                addressConfirmationView?.selectedTag = 0
//            }else if address.tagType == .work{
//                addressConfirmationView?.enableDisabledTagAction(index: 1)
//                addressConfirmationView?.selectedTag = 1
//            }else{
//                addressConfirmationView?.enableDisabledTagAction(index: 2)
//                addressConfirmationView?.selectedTag = 2
//            }
//        }
        if isSelectedFromMap{
            AnalyticsHelper.shared.triggerEvent(type: .AE02A)
        }else{
            AnalyticsHelper.shared.triggerEvent(type: .AE02B)
        }
        addressConfirmationView?.addToSuperView(view: self)
    }
    @IBAction func onClickBtnChange(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .AE02)
        
        let vc = UIUtils.viewController(storyboardId: "DeliveryHome", vcId: "ChangeLocationViewController") as! ChangeLocationViewController
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
        
       // addressSearchPlaceView = AddressSearchPlaceView().initWith(delegate: self)
       // addressSearchPlaceView?.addToSuperView(view: self)
    }
    
    func getParams(flat: String, landmark: String, tagType: String, tagName: String?) -> [String: Any] {
        return ["address_id": address?.addressId ?? "",
                "customer_id": BBQUserDefaults.sharedInstance.customerId,
                "flat_no": flat,
                "address": mapSelectedAddress,
                "landmark": landmark,
                "phone_no": SharedProfileInfo.shared.profileData?.mobileNumber ?? "0",
                "latitude":"\(latitude)",
                "longitude": "\(longitude)",
                "pincode":  "",
                "tag_type": tagType,
                "tag_name": tagName ?? ""]
    }
    
    
    func SetUpMap(latitude:Double, longitude: Double) {
            let camera = GMSCameraPosition.camera(withLatitude:latitude, longitude: longitude, zoom: 18)
        GoogleMapView = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.mapView.frame.height), camera: camera)
            GoogleMapView.delegate = self
        GoogleMapView.settings.allowScrollGesturesDuringRotateOrZoom = true
        GoogleMapView.settings.compassButton = true
        GoogleMapView.settings.zoomGestures = true
        GoogleMapView.settings.scrollGestures = true
        GoogleMapView.mapType = .terrain
            self.mapView.addSubview(GoogleMapView)
            self.mapView.bringSubviewToFront(pinImage)
        }
    
    func setMapToLocation(latitude:Double, longitude: Double) {
        let camera = GMSCameraPosition.camera(withLatitude:latitude, longitude: longitude, zoom: 18)
    GoogleMapView = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.mapView.frame.height), camera: camera)
    }
    
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        let lat = position.target.latitude
        let lng = position.target.longitude
        
        // Create Location
        let location = CLLocation(latitude: lat, longitude: lng)
        
        // Geocode Location
        AnalyticsHelper.shared.triggerEvent(type: .location_places_api_called)
        geoCoder.reverseGeocodeLocation(location) { (placemarks, error) in
            if let placemarks = placemarks{
                if let location = placemarks.first?.location{
                    //self.addressTextField.text = (placemarks.first?.name ?? "")+" "+(placemarks.first?.subLocality ?? " ")
                    if let addressDict = placemarks.first?.addressDictionary {
                        let dict = JSON(addressDict)
                        self.longitude = Double(location.coordinate.longitude)
                        self.latitude = Double(location.coordinate.latitude)
                        if !self.isSelectedFromList{
                            var address:String = ""
                            for data in dict["FormattedAddressLines"].arrayValue{
                                if address == ""{
                                    address = data.stringValue
                                    continue
                                }
                                address = address+" "+data.stringValue
                            }
                            self.lblAddress.text = dict["Name"].rawString()
                            
                            if dict["FormattedAddressLines"].arrayValue.count >= 2{
                                self.lblFullAddress.text = dict["FormattedAddressLines"].arrayValue[1].stringValue
                            }else{
                                self.lblFullAddress.text = address
                            }
                            self.mapSelectedAddress = address
                            self.addressConfirmationView?.lblAddressTitle.text = dict["Name"].rawString()
                            self.addressConfirmationView?.lblAddressDesc.text = address
                            self.isSelectedFromMap = true
                        }
                        self.isSelectedFromList = false
                        
                     // here you will get the Address.
 
 
                    }
                }
            }
        
        }
        
    }
    
    func cameraMoveToLocation(toLocation: CLLocationCoordinate2D?) {
        if GoogleMapView == nil && toLocation != nil{
            SetUpMap(latitude: toLocation?.latitude ?? 0.0, longitude: toLocation?.longitude ?? 0.0)
        }
        
        if toLocation != nil{
            GoogleMapView.camera = GMSCameraPosition.camera(withTarget: toLocation!, zoom: 18)
        }
    }
    
    @objc private func searchTextFieldStr(){
        if searchText.count >= 3{
            searchPlaces(searchText)
        }
    }
    
    @objc func searchPlaces(_ searchStr: String) {
        AnalyticsHelper.shared.triggerEvent(type: .delivery_address_search_button_clicked)
        guard searchStr.count >= 3 else {
            return
        }
        let filter = GMSAutocompleteFilter()
        filter.country = "IN"
        let location = CLLocationManager().location ?? CLLocation(latitude: BBQUserDefaults.sharedInstance.currentLatitudeValue, longitude: BBQUserDefaults.sharedInstance.currentLongitudeValue)
        let bounds = GMSCoordinateBounds(coordinate: location.coordinate, coordinate: CLLocationCoordinate2D(latitude: BBQUserDefaults.sharedInstance.currentLatitudeValue, longitude: BBQUserDefaults.sharedInstance.currentLongitudeValue))
        filter.locationBias = GMSPlaceRectangularLocationOption(bounds.northEast,  bounds.southWest)
        filter.origin = location
        AnalyticsHelper.shared.triggerEvent(type: .address_places_api_called)
        filter.type = .establishment
        
        //AppDelegate.shared().gmsSessionToken
        placesClient.findAutocompletePredictions(fromQuery: searchStr, filter: filter,  sessionToken: nil) { (results, error) in
            if let error = error {
                print("Autocomplete error: \(error)")
                return
            }
            if let results = results {
                self.googlePlaces.removeAll()
                for result in results {
                    self.googlePlaces.append(GooglePlaceRes(placeName: result.attributedFullText, placeId: result.placeID))
                }
            }
            self.addressSearchPlaceView?.address = self.googlePlaces
            self.addressSearchPlaceView?.tableView.reloadData()
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func tracingLocation(location: Location) {
        SetUpMap(latitude: Double(location.latitude) ?? 0.00, longitude: Double(location.longitude) ?? 0.00)
    }
    
    func tracingFailureInLocation(error: NSError) {
        self.alert(message: error.localizedDescription, title: "Location update failed")
    }

}

extension AddAddressVC: AddressConfirmationViewDelegate{
    func onHiddenView(view: AddressConfirmationView) {
        addressConfirmationView = nil
    }
    
    func onClickChangeAddress() {
        onClickBtnChange(backButton as Any)
    }
    
    func saveAddress(flat: String, landmark: String, tagType: String, tagName: String?, type: Address.TagType?) {
        AnalyticsHelper.shared.triggerEvent(type: .AE03)
        if let type = type{
            type.triggerButtonEvent()
        }
        UIUtils.showLoader()
        BBQServiceManager.getInstance().AddCustomerAddress(params: getParams(flat: flat, landmark: landmark, tagType: tagType, tagName: tagName)) { (error, response) in
            UIUtils.hideLoader()
            if let response = response {
                if response.message_type == "failed" {
                    UIUtils.showToast(message: response.message)
                    AnalyticsHelper.shared.triggerEvent(type: .AE03B)
                    if let type = type{
                        type.triggerFailureEvent()
                    }
                } else {
                    AnalyticsHelper.shared.triggerEvent(type: .AE03A)
                    if let type = type{
                        type.triggerSuccessEvent()
                    }
                    AnalyticsHelper.shared.triggerEvent(type: .added_address_delivery)
                    UIUtils.showToast(message: "Address added Successfully")
                    self.navigationController?.popViewController(animated: true)
                    
                    //Came from cart screen , So go back to cart screen with added address
                    if self.isFromCartScreen == true {
                       
                        guard let fetchedAddress = response.data?.address.first else {
                            return
                        }
                        //TODO: update in another thread
                        
                        BBQServiceManager.getInstance().updateAddress(params: ["customer_id" : BBQUserDefaults.sharedInstance.customerId,
                                                                                    "branch_id": BBQUserDefaults.sharedInstance.DeliveryBranchIdValue,
                                                                                    "address_id": fetchedAddress.addressId]) { (error, response) in
                            if error != nil {
                                UIUtils.showToast(message: "An error occurred while updating address")
                            } else if response != nil {
                                AnalyticsHelper.shared.triggerEvent(type: .CA02A)
                                AnalyticsHelper.shared.triggerEvent(type: .C03A)
                            }
                        }
                        
//                        let vc = UIUtils.viewController(storyboardId: "DeliveryCart", vcId: "ChangeAddressViewController") as! ChangeAddressViewController
//                        vc.selectedAddressId = fetchedAddress.addressId
//                        self.navigationController?.pushViewController(vc, animated: true)

                        
                        if let tabBarController = self.navigationController?.viewControllers{
                            for vc in tabBarController {
                                
                                if vc.isKind(of: DeliveryTabBarController.self){
                                    self.cartScreenDelegate?.getAddedAddress(address: fetchedAddress)
                                    self.navigationController?.popToViewController(vc, animated: false)

                                    break
                                }
                                
                            }
                            
                        }

                                                            
                    
//                        CATransaction.begin()
//                        self.navigationController?.popViewController(animated: true)
//                        CATransaction.setCompletionBlock({ [weak self] in
//                                self?.cartScreenDelegate?.getAddedAddress(address: fetchedAddress) })
//                            CATransaction.commit()
                        
                    }
                    
                }
            } else {
                AnalyticsHelper.shared.triggerEvent(type: .AE03B)
                if let type = type{
                    type.triggerFailureEvent()
                }
                UIUtils.showToast(message: error?.message ?? UNEXPECTED_ERROR)
            }


        }
    }
    
    
}
extension UINavigationController {
    func popViewController(animated: Bool, completion: @escaping (() -> Void)) {
        popViewController(animated: animated)
        doAfterAnimatingTransition(animated: animated, completion: completion)
    }
    private func doAfterAnimatingTransition(animated: Bool, completion: @escaping (() -> Void)) {
        if let coordinator = transitionCoordinator, animated {
          coordinator.animate(alongsideTransition: nil, completion: { _ in
            completion()
          })
        } else {
          DispatchQueue.main.async {
            completion()
          }
        }
      }

}

extension AddAddressVC: AddressSearchPlaceViewDelegate{
    func onViewHidden(view: AddressSearchPlaceView) {
        addressSearchPlaceView = nil
    }
    
    func onSelectMyLocation() {
        LocationManager.sharedInstance.delegate = self
        LocationManager.sharedInstance.initLocationManager()
      //  addressSearchPlaceView = nil
        isSelectedFromMap = false
    }
    
    func onSelectLocation(googlePlaceRes: GooglePlaceRes) {
        self.addressViewModel.getPlaceLatLong(googlePlaceRes.placeId, completionHandler: { (place, error) in
            guard let location = place else { return }
            let lat = Double(location.latitude)
            let lon = Double(location.longitude)
            let place = CLLocationCoordinate2D(latitude:lat!
                                                  , longitude:lon!)
            
            self.lblAddress.text = googlePlaceRes.placeName.string.components(separatedBy: ",").first
            self.lblFullAddress.text = googlePlaceRes.placeName.string
            self.mapSelectedAddress = googlePlaceRes.placeName.string
            self.addressConfirmationView?.lblAddressTitle.text = googlePlaceRes.placeName.string.components(separatedBy: ",").first
            self.addressConfirmationView?.lblAddressDesc.text = googlePlaceRes.placeName.string
            self.isSelectedFromList = true
            self.cameraMoveToLocation(toLocation: place)
        })
        isSelectedFromMap = false
        addressSearchPlaceView = nil
    }
    
    func onStartSearching(text: String) {
        if let timer = timer{
            timer.invalidate()
        }
        
        timer = nil
        searchText = text
        if text.count >= 3{
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(searchTextFieldStr), userInfo: nil, repeats: false)
        }
    }
    func locationDisabled() {
        SetUpMap(latitude: 12.9716, longitude: 77.5946)
    }
}
