//
 //  Created by Mahmadsakir on 11/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified AddressSearchPlaceVC.swift
 //

import UIKit
import SkyFloatingLabelTextField

protocol AddressSearchPlaceViewDelegate {
    func onSelectMyLocation()
    func onSelectLocation(googlePlaceRes: GooglePlaceRes)
    func onStartSearching(text: String)
    func onViewHidden(view: AddressSearchPlaceView)
}

class AddressSearchPlaceView: UIView {
    
    //MARK:- Outlets
    @IBOutlet weak var viewForSearchView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var backgroundAlpha: UIView!
    @IBOutlet weak var stackViewForContainer: UIStackView!
    @IBOutlet weak var stackViewForSpacing: UIView!
    
    //MARK:- Variables
    var delegate: AddressSearchPlaceViewDelegate?
    var address = [GooglePlaceRes]()
    
    //MARK:- View Configuration
    func initWith(delegate: AddressSearchPlaceViewDelegate) -> AddressSearchPlaceView {
        let view = loadFromNib()!
        view.delegate = delegate
        view.styleUI()
        return view
    }
        
        
    func loadFromNib() -> AddressSearchPlaceView? {
        if let views = Bundle.main.loadNibNamed("AddressSearchPlaceView", owner: self, options: nil), let view = views[0] as? AddressSearchPlaceView{
            view.frame = UIApplication.shared.windows[0].bounds
            return view
        }
        return nil
    }
    
    func addToSuperView(view: UIViewController?) {
        if let tabBarController = view?.navigationController?.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
            tabBarController.tabBar.isHidden = true
        }
        
        if let view = view{
            view.view.addSubview(self)
        }else{
            getMainView().addSubview(self)
        }
        stackViewForContainer.isHidden = true
        self.backgroundAlpha.alpha = 0.0
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
            self.stackViewForContainer.isHidden = false
            self.backgroundAlpha.alpha = 0.5
        } completion: { (isCompleted) in
            self.stackViewForContainer.isHidden = false
        }
    }
    
    func removeToSuperView() {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
            self.stackViewForContainer.isHidden = true
            self.backgroundAlpha.alpha = 0.0
            self.layoutIfNeeded()
        } completion: { (isCompleted) in
            self.stackViewForContainer.isHidden = true
            self.backgroundAlpha.alpha = 0.0
            self.removeFromSuperview()
        }
    }
    
    func styleUI() {
        viewForContainer.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        tableView.register(UINib(nibName: "AddressSearchPlaceCell", bundle: nil), forCellReuseIdentifier: "AddressSearchPlaceCell")
        tableView.delegate = self
        tableView.dataSource = self
        viewForSearchView.roundCorners(cornerRadius: 5.0, borderColor: .text, borderWidth: 1.0)
        txtSearch.delegate = self
    }
    
    
    
    //MARK:- Button Actions
    @IBAction func onClickBtnMyLocation(_ sender: Any) {
        delegate?.onSelectMyLocation()
        removeToSuperView()
    }
    @IBAction func onClickBtnClose(_ sender: Any) {
        removeToSuperView()
    }
    
    @IBAction func onEditingOfSearch(_ sender: Any) {
        delegate?.onStartSearching(text: txtSearch.text ?? "")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddressSearchPlaceView: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return address.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressSearchPlaceCell", for: indexPath) as! AddressSearchPlaceCell
        cell.setCellData(address: address[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.onSelectLocation(googlePlaceRes: address[indexPath.row])
        removeToSuperView()
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        txtSearch.resignFirstResponder()
    }
}

extension AddressSearchPlaceView: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        //Changes by Arpana
        //stackViewForSpacing height was fixed 150, issue on SE
        //Chaged to proportional height, to fix the bug
        UIView.animate(withDuration: 0.2) {
            self.stackViewForSpacing.isHidden = false
        } completion: { (isCompleted) in
            self.stackViewForSpacing.isHidden = false
        }

        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.2) {
            self.stackViewForSpacing.isHidden = true
        } completion: { (isCompleted) in
            self.stackViewForSpacing.isHidden = true
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
}
