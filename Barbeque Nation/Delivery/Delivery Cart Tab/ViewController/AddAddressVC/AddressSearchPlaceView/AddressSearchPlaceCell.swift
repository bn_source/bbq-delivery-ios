//
 //  Created by Mahmadsakir on 12/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified AddressSearchPlaceCell.swift
 //

import UIKit

class AddressSearchPlaceCell: UITableViewCell {
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblFullAddress: UILabel!
    
    var googlePlaceRes: GooglePlaceRes!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setCellData(address: GooglePlaceRes) {
        self.googlePlaceRes = address
        self.lblAddress.text = address.placeName.string.components(separatedBy: ",").first
        self.lblFullAddress.text = address.placeName.string
    }
}
