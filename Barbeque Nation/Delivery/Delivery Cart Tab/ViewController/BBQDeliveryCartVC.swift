//
//  BBQDeliveryCartVC.swift
//
//  Created by Mahmadsakir on 11/01/21
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
//  All rights reserved.

import UIKit
import Razorpay
import SwiftGifOrigin
import WebKit
import PopMenu
import AMPopTip


enum TakeAwayDelivery : Int {
    
    case Delivery = 0
    case TakeAway = 1
}

protocol AddAddressOnCartVCDelegate: AnyObject {
    func getAddedAddress(address: Address)
}



class BBQDeliveryCartVC: BaseViewController  {
    
    @IBOutlet weak var emptyCartImage: UIImageView!
    @IBOutlet weak var emptyCartLabel: UILabel!
    @IBOutlet weak var couponsTableView: UITableView!
    @IBOutlet weak var couponsTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var itemsTableView: UITableView!
    @IBOutlet weak var itemsTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var deliveryAndTakeawaySegmentControl: UISegmentedControl!
    @IBOutlet weak var payButton: UIButton!
    @IBOutlet weak var totalAmountLbl: UILabel!
    @IBOutlet weak var restaurantLocationLbl: UILabel!
    @IBOutlet weak var restaurantAddressLbl: UILabel!
    let viewModel = DeliveryCartViewModel()
    @IBOutlet weak var viewForPaymentLoading: UIView!
    @IBOutlet weak var imgPaymentLoading: UIImageView!
    @IBOutlet weak var viewForPinView: UIView!
    @IBOutlet weak var stackViewForUseSmile: UIStackView!
    @IBOutlet weak var stackViewForUsedSmile: UIStackView!
    @IBOutlet weak var stackViewForUsePromocode: UIStackView!
    @IBOutlet weak var stackViewForUsedPromocode: UIStackView!
    @IBOutlet weak var lblAvailableSmile: UILabel!
    @IBOutlet weak var lblMessageForUseSmile: UILabel!
    @IBOutlet weak var btnUsePartialSmile: UIButton!
    @IBOutlet weak var btnUseAllSmile: UIButton!
    @IBOutlet weak var lblUsedSmiles: UILabel!
    @IBOutlet weak var btnUsedSmiles: UIButton!
    @IBOutlet weak var lblUsedPromocode: UILabel!
    @IBOutlet weak var lblUsedPromocodeMessage: UILabel!
    @IBOutlet weak var lblUsedPromocodeAmount: UILabel!
    @IBOutlet weak var billTableView: UITableView!
    @IBOutlet weak var heightForBillTableView: NSLayoutConstraint!
    @IBOutlet weak var lblToPay: UILabel!
    @IBOutlet weak var viewForAddress: UIView!
    @IBOutlet weak var imgDeliveryAddress: UIImageView!
    @IBOutlet weak var lblDeliveryAddress: UILabel!
    @IBOutlet weak var lblDeliveryName: UILabel!
    @IBOutlet weak var stackViewForOrderingFor: UIView!
    @IBOutlet weak var btnOrderingFor: UIButton!
    @IBOutlet weak var btnRestaurantLocation: UIButton!
    @IBOutlet weak var lblEarnSmile: UILabel!
    @IBOutlet weak var lblTakeAwayDistanceMessage: UILabel!
    @IBOutlet weak var lblTakeAwayMessage: UILabel!
    @IBOutlet weak var viewForTakeAwayMessage: UIView!
    @IBOutlet weak var voucherTableView: UITableView!
    @IBOutlet weak var voucherTableviewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var stackViewForUserVoucher: UIStackView!
    @IBOutlet weak var stackViewForDeliveryType: UIStackView!
    @IBOutlet weak var viewForInstantDelivery: UIView!
    @IBOutlet weak var viewForScheduleDelivery: UIView!
    @IBOutlet weak var lblInstantDeliveryTitle: UILabel!
    @IBOutlet weak var lblInstantDeliveryTime: UILabel!
    @IBOutlet weak var lblScheduleDeliveryType: UILabel!
    @IBOutlet weak var btnInstantDelivery: UIButton!
    @IBOutlet weak var btnScheduleDelivery: UIButton!
    @IBOutlet weak var lblScheduleDeliveryTime: UILabel!
    @IBOutlet weak var ViewForScheduleDetails: UIView!
    @IBOutlet weak var lblScheduleDetails: UILabel!
    @IBOutlet weak var lblApplyCouponTitle: UILabel!
    @IBOutlet weak var viewForItemSuggestion: UIView!
    @IBOutlet weak var lblTitleItemSuggestion: UILabel!
    @IBOutlet weak var collectionViewForItemSuggestion: UICollectionView!
    @IBOutlet weak var imgOrderType: UIImageView!
    @IBOutlet weak var viewForHeaderView: UIView!
    @IBOutlet weak var lblDAddressTitle: UILabel!
    @IBOutlet weak var viewForTakeawayOrder: UIView!
    @IBOutlet weak var stackViewForTakeAwayOrder: UIStackView!
    @IBOutlet weak var lblPickupSlotTime: UILabel!
    @IBOutlet weak var imgBrandLogo: UIImageView!
    
    var isScreenAppearedFromAddAddressView: Bool = false
    var addDeliveryAddress =  "Add delivery address"
    var webView: WKWebView!
    var bottomSheetController: BottomSheetController?
    var popTip = PopTip()
    var selectedTakeAwaySlot: String?
    var isInstantDelivery = true
    var selectedScheduleDeliverySlot: String?
    var isLoadingFirstTime = true
    
    var itemDetailsView: ItemDetailView?
    
    var razorpay: RazorpayCheckout!
    @IBOutlet weak var addItems: UIButton!
    var razorPayId = ""
    var isPaymentInProgress = false
    var arrCoupons = [ListTender]()
    var arrVouchers = [ListTender]()
    var paymentFetchMethods: [AnyHashable:Any]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AnalyticsHelper.shared.triggerEvent(type: .C01)
        self.hidesBottomBarWhenPushed = true
        deliveryAndTakeawaySegmentControl.selectedSegmentIndex = 0
        scrollView.isHidden = true
        emptyCartImage.isHidden = true
        emptyCartLabel.isHidden = true
        addItems.isHidden = true
        viewForAddress.isHidden = true
        // Do any additional setup after loading the view.
        //        razorpay = RazorpayCheckout.initWithKey("rzp_test_RxNhQ94zLTKNeq", andDelegate: self)
        webView = WKWebView.init(frame: self.view.frame)
        razorpay = RazorpayCheckout.initWithKey("rzp_live_woEpCznOt0mINU", andDelegate: self, withPaymentWebView: webView)
        viewModel.delegate = self
        
        
        viewForPinView.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0)
        
        applyBrandTheme()
        deliveryAndTakeawaySegmentControl.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.appThemeMediumWith(size: 10)], for: .normal)
        deliveryAndTakeawaySegmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.lightGray], for: .disabled)
        
        viewForAddress.setShadow()
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: self.view.frame.size.height - 258.0)
        bottomSheetController = BottomSheetController(configuration: configuration)
        popTip.bubbleColor = .white
        popTip.cornerRadius = 5.0
        popTip.shouldShowMask = true
        addItems.setCornerRadius(5.0)
        initiateThePaymentData()
    }
    
    private func applyBrandTheme(){
        emptyCartLabel.applyThemeOnText()
        addItems.applyMultiBrandTheme()
        deliveryAndTakeawaySegmentControl.backgroundColor = .deliveryThemeBackgroundColor
        deliveryAndTakeawaySegmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.deliveryThemeTextColor], for: .selected)
        deliveryAndTakeawaySegmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.deliveryThemeColor], for: .normal)
        btnOrderingFor.setTitleColor(.deliveryThemeColor, for: .normal)
        btnUseAllSmile.setTitleColor(.deliveryThemeColor, for: .normal)
        btnUsePartialSmile.setTitleColor(.deliveryThemeColor, for: .normal)
        payButton.applyMultiBrandTheme()
        if #available(iOS 13.0, *) {
            deliveryAndTakeawaySegmentControl.selectedSegmentTintColor = .deliveryThemeColor
        } else {
            deliveryAndTakeawaySegmentControl.tintColor = .deliveryThemeColor
        }
        btnUsePartialSmile.roundCorners(cornerRadius: 5.0, borderColor: .deliveryThemeColor, borderWidth: 1.0)
        btnUseAllSmile.roundCorners(cornerRadius: 5.0, borderColor: .deliveryThemeColor, borderWidth: 1.0)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.screenName(name: .Delivery_Cart_Screen)
        AnalyticsHelper.shared.triggerEvent(type: .C02)
        addObserver()
        hideNavigationBar(true, animated: true)
        self.tabBarController?.tabBar.isHidden = true
        AnalyticsHelper.shared.triggerEvent(type: .visited_delivery_cart)
//if isScreenAppearedFromAddAddressView != true {
            if !BBQUserDefaults.sharedInstance.customerId.isEmpty {
                viewModel.getCartDetails(transaction_type: deliveryOrTakeAwayOptionSelected == .Delivery ? 1:2, isFirstTimeLoading: true)
                getUpdatedPoints()
            } else {
                self.scrollView.isHidden = true
                viewForAddress.isHidden = true
                emptyCartImage.isHidden = false
                emptyCartLabel.isHidden = false
                addItems.isHidden = false
            }
      //  }
      //  isScreenAppearedFromAddAddressView = false
    }
    
    private func getUpdatedPoints() {
        if BBQUserDefaults.sharedInstance.accessToken != "" {
            self.viewModel.getLoyaltyPointsCount { (isSuccess) in
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObserver()
    }
    
    private func addObserver(){
        itemsTableView.addObserver(self, forKeyPath: "contentSize", options: .prior, context: nil)
        couponsTableView.addObserver(self, forKeyPath: "contentSize", options: .prior, context: nil)
        voucherTableView.addObserver(self, forKeyPath: "contentSize", options: .prior, context: nil)
        billTableView.addObserver(self, forKeyPath: "contentSize", options: .prior, context: nil)
    }
    
    private func removeObserver(){
        addObserver()
        itemsTableView.removeObserver(self, forKeyPath: "contentSize")
        couponsTableView.removeObserver(self, forKeyPath: "contentSize")
        voucherTableView.removeObserver(self, forKeyPath: "contentSize")
        billTableView.removeObserver(self, forKeyPath: "contentSize")
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize" {
            couponsTableViewHeightConstraint.constant = couponsTableView.contentSize.height
            itemsTableViewHeightConstraint.constant = itemsTableView.contentSize.height
            heightForBillTableView.constant = billTableView.contentSize.height
            voucherTableviewHeightConstraint.constant = voucherTableView.contentSize.height
        }
    }
    
    
    override func viewDidLayoutSubviews() {
        self.itemsTableViewHeightConstraint.constant = self.itemsTableView.contentSize.height
        self.couponsTableViewHeightConstraint.constant = self.couponsTableView.contentSize.height
        self.voucherTableviewHeightConstraint.constant = self.voucherTableView.contentSize.height
    }
    
    
    func navigateToAddAddressScreen() {
        let vc = UIUtils.viewController(storyboardId: "DeliveryCart", vcId: "AddAddressViewController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToAddCouponsScreen(isForVoucher: Bool) {
        
        let giftAndOffersViewController = UIUtils.viewController(storyboardId: "DeliveryCart", vcId: "GiftAndOffersViewController") as! GiftAndOffersViewController
        giftAndOffersViewController.orderId = viewModel.cartDetails?.data?.order_id ?? ""
        giftAndOffersViewController.logo_url = viewModel.cartDetails?.data?.brand_logo ?? ""
        giftAndOffersViewController.brand_id = viewModel.cartDetails?.data?.brand_id ?? ""
        giftAndOffersViewController.transaction_type = deliveryAndTakeawaySegmentControl.selectedSegmentIndex == 0 ? "Delivery" : ""
        giftAndOffersViewController.isForVoucher = isForVoucher
        giftAndOffersViewController.order_type =  deliveryAndTakeawaySegmentControl.selectedSegmentIndex == 0 ? isInstantDelivery ? "Instant" : "Schedule" : "Takeaway"
        giftAndOffersViewController.appliedCouoons = arrCoupons + arrVouchers
        //  vc. = arrCoupons
        //  vc.appliedCouoons = arrVouchers
        
        self.navigationController?.pushViewController(giftAndOffersViewController, animated: true)
        
    }
    
    @IBAction func actionOnHomeButton(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .C04)
        if let tabBarController = navigationController?.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
            tabBarController.selectedIndex = 2
        }
    }
    
    @IBAction func actionOnAddItems(_ sender: Any) {
        if let tabBarController = navigationController?.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
            tabBarController.selectedIndex = 2
        }
    }
    
    
    @IBAction func actionOnPayBtn(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .Delivery_Takeaway_Pay)
        if deliveryAndTakeawaySegmentControl.selectedSegmentIndex == 0 {
            if (viewModel.cartDetails?.data?.delivery ?? false) || !isInstantDelivery{
                if !isInstantDelivery && selectedScheduleDeliverySlot == nil{
                    openScheduleDeliveryPicker()
                    return
                }
                if viewModel.getDeliveryAddress() == nil {
                    actionOnAddOrEditAddress(self)
                    return
                }
                viewModel.checkDistanceAPI(ordering_name: viewModel.cartDetails?.data?.orderingForName  ?? BBQUserDefaults.sharedInstance.UserName, ordering_phonenumber: viewModel.cartDetails?.data?.orderingForNumber ?? SharedProfileInfo.shared.profileData?.mobileNumber ?? BBQUserDefaults.sharedInstance.customerId, deliveryTime: selectedScheduleDeliverySlot ?? "", deliveryId: nil) { isServicable in
                    print(isServicable)
                }
                //PayUPaymentHelper.shared.openPayment(on: self, params: PayUPaymentHelper.shared.getPayUParams(transactionId: viewModel.cartDetails?.data?.order_id, amount: viewModel.getBillAmount().replacingOccurrences(of: getCurrency(), with: "")))
            } else {
                AnalyticsHelper.shared.triggerEvent(type: .Delivery_Takeaway_Order_Status, properties: [.Delivery_Takeaway_Status: AnalyticsHelper.MoEngageAttrValue.Failed.rawValue, .Error: "Delivery is not available"])
                UIUtils.showToast(message: "Delivery is not available")
            }
        } else {
            if viewModel.cartDetails?.data?.takeaway ?? false {
                if selectedTakeAwaySlot == nil || selectedTakeAwaySlot == "" {
                    openTakeAwayPicker()
                } else {
//                    viewModel.generatePaymentId(transactionType: deliveryAndTakeawaySegmentControl.selectedSegmentIndex == 0 ? 1: 2, deliveryTime: selectedTakeAwaySlot ?? "", ordering_name: viewModel.cartDetails?.data?.orderingForName ?? BBQUserDefaults.sharedInstance.UserName, ordering_phonenumber: viewModel.cartDetails?.data?.orderingForNumber ?? SharedProfileInfo.shared.profileData?.mobileNumber ?? BBQUserDefaults.sharedInstance.customerId)
                    showPaymentScreen()
                }
            } else {
                UIUtils.showToast(message: "Takeaway is not available")
                AnalyticsHelper.shared.triggerEvent(type: .Delivery_Takeaway_Order_Status, properties: [.Delivery_Takeaway_Status: AnalyticsHelper.MoEngageAttrValue.Failed.rawValue, .Error: "Takeaway is not available"])
            }
        }
        
    }
    
    @IBAction func onClickBtnInstantDelivery(_ sender: Any) {
        isInstantDelivery = true
        selectedScheduleDeliverySlot = nil
        setupDeliveryType()
        removeAppliedCoupons()
    }
    
    @IBAction func onClickBtnScheduleDelivery(_ sender: Any) {
        openScheduleDeliveryPicker()
    }
    
    
    func getSelectedTime(time: String) -> String {
        
        let time = time.split(separator: ":")
        if time.count >= 2 {
            return "\(time[0]):\(time[1])"
        }
        return ""
    }
    
    private func openTakeAwayPicker() {
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: 80)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        let directionBottomSheet = BBQTakeAwayTimeVC(nibName: "BBQTakeAwayTimeVC", bundle: nil)
        directionBottomSheet.pickUpAvailabilityTime = viewModel.cartDetails?.data?.pickUpAvailabilityTime ?? [PickUpDateTime]()
        directionBottomSheet.delegate = self
        bottomSheetController.present(directionBottomSheet, on: self)
    }
    
    private func openScheduleDeliveryPicker() {
        AnalyticsHelper.shared.triggerEvent(type: .C09)
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: 80)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        let directionBottomSheet = BBQScheduleDeliveryVC(nibName: "BBQScheduleDeliveryVC", bundle: nil)
        directionBottomSheet.pickUpAvailabilityTime = viewModel.cartDetails?.data?.scheduleAvailablityTime ?? [PickUpDateTime]()
        directionBottomSheet.delegate = self
        bottomSheetController.present(directionBottomSheet, on: self)
    }
    
    private func setupTakeAwayView() {
        deliveryAndTakeawaySegmentControl.selectedSegmentIndex = 1
        imgDeliveryAddress.image = UIImage(named: "icon_takeaway_theme")
        stackViewForOrderingFor.isHidden = true
        stackViewForTakeAwayOrder.isHidden = false
        lblDAddressTitle.text = "Pickup Resturant"
        //btnRestaurantLocation.isHidden = false
        if let selectedTakeAwaySlot = selectedTakeAwaySlot, let string = selectedTakeAwaySlot.getTakeAwayString(){
            lblDeliveryAddress.attributedText = string.0
            lblPickupSlotTime.text = string.1
            viewForTakeawayOrder.roundCorners(cornerRadius: 5.0, borderColor: .deliveryThemeColor, borderWidth: 1.0)
            viewForTakeawayOrder.backgroundColor = .deliveryThemeColor
            lblPickupSlotTime.textColor = .deliveryThemeTextColor
        }else{
            lblDeliveryAddress.text = " Select pickup slot"
            lblPickupSlotTime.text = "Select pickup slot"
            lblDeliveryAddress.applyThemeOnText()
            lblPickupSlotTime.textColor = .lightGray
            lblDeliveryAddress.font = UIFont.systemFontOfSize(size: 14.0, weight: .semibold)
            selectedTakeAwaySlot = nil
            viewForTakeawayOrder.roundCorners(cornerRadius: 5.0, borderColor: .lightGray, borderWidth: 1.0)
            viewForTakeawayOrder.backgroundColor = .clear
        }
        deliveryOrTakeAwayOptionSelected = TakeAwayDelivery(rawValue: deliveryAndTakeawaySegmentControl.selectedSegmentIndex) ?? TakeAwayDelivery.Delivery
        isInstantDelivery = true
        selectedScheduleDeliverySlot = nil
        stackViewForDeliveryType.isHidden = true
        ViewForScheduleDetails.isHidden = true
    }
    
    private func setupDeliveryView() {
        deliveryAndTakeawaySegmentControl.selectedSegmentIndex = 0
        stackViewForOrderingFor.isHidden = false
        stackViewForTakeAwayOrder.isHidden = true
        lblDAddressTitle.text = "Delivery Address"
        //btnRestaurantLocation.isHidden = true
        if let address = viewModel.getDeliveryAddress(), let addressTupple = viewModel.getDeliveryAddressTupple(){
            lblDeliveryAddress.attributedText = address
            imgDeliveryAddress.image = viewModel.cartDetails?.data?.address?.getTagIcon(color: "theme").0
            restaurantLocationLbl.text = addressTupple.0
            restaurantAddressLbl.text = addressTupple.1
            restaurantLocationLbl.textColor = .darkText
        }else{
            imgDeliveryAddress.image = UIImage(named: "icon_address_other_theme")
            lblDeliveryAddress.text = addDeliveryAddress
            lblDeliveryAddress.applyThemeOnText()
            lblDeliveryAddress.font = UIFont.systemFontOfSize(size: 14.0, weight: .semibold)
            restaurantLocationLbl.applyThemeOnText()
            restaurantLocationLbl.text = addDeliveryAddress
            restaurantAddressLbl.text = ""
        }
        deliveryOrTakeAwayOptionSelected = TakeAwayDelivery(rawValue: deliveryAndTakeawaySegmentControl.selectedSegmentIndex) ?? TakeAwayDelivery.Delivery
        selectedTakeAwaySlot = nil
        stackViewForDeliveryType.isHidden = false
        setupDeliveryType()
    }
    
    private func setupDeliveryType(){
        UIView.animate(withDuration: 0.1) {
            self.setupDeliveryTypeDate()
        } completion: { isCompleted in
            self.setupDeliveryTypeDate()
        }
        
    }
    
    private func setupDeliveryTypeDate(){
        setupInstantDelivery()
        if isInstantDelivery{
            viewForInstantDelivery.roundCorners(cornerRadius: 5.0, borderColor: .deliveryThemeColor, borderWidth: 1.0)
            viewForScheduleDelivery.roundCorners(cornerRadius: 5.0, borderColor: .lightGray, borderWidth: 1.0)
            viewForInstantDelivery.backgroundColor = .deliveryThemeColor
            viewForScheduleDelivery.backgroundColor = .clear
            //lblScheduleDeliveryTime.isHidden = true
            lblScheduleDeliveryTime.text = "Schedule"
            ViewForScheduleDetails.isHidden = true
            lblInstantDeliveryTime.textColor = .deliveryThemeTextColor
            lblInstantDeliveryTitle.textColor = .darkText
            lblScheduleDeliveryTime.textColor = .lightGray
            lblScheduleDeliveryType.textColor = .lightGray
        }else{
            viewForScheduleDelivery.roundCorners(cornerRadius: 5.0, borderColor: .deliveryThemeColor, borderWidth: 1.0)
            viewForInstantDelivery.roundCorners(cornerRadius: 5.0, borderColor: .lightGray, borderWidth: 1.0)
            viewForInstantDelivery.backgroundColor = .clear
            viewForScheduleDelivery.backgroundColor = .deliveryThemeColor
            lblInstantDeliveryTime.textColor = .lightGray
            lblInstantDeliveryTitle.textColor = .lightGray
            lblScheduleDeliveryTime.textColor = .deliveryThemeTextColor
            lblScheduleDeliveryType.textColor = .darkText
            ViewForScheduleDetails.isHidden = false
            if let selectedScheduleDeliverySlot = selectedScheduleDeliverySlot{
                let data = selectedScheduleDeliverySlot.getScheduleDeluveryString(isAfterPlaced: false, isNextLine: false, interval: viewModel.cartDetails?.data?.lst_delivery_schedule_interval ?? 1800, fontSize: 14)
                if data.0 != nil{
                    lblScheduleDetails.attributedText = data.0
                    lblScheduleDeliveryTime.text = data.1
                    lblScheduleDeliveryTime.isHidden = false
                }else{
                    self.selectedScheduleDeliverySlot = nil
                    lblScheduleDetails.text = " Select schedule delivery slot"
                    lblScheduleDetails.applyThemeOnText()
                    lblScheduleDetails.font = UIFont.systemFontOfSize(size: 14.0, weight: .semibold)
                }
            }else{
                lblScheduleDetails.text = " Select schedule delivery slot"
                lblScheduleDetails.applyThemeOnText()
                lblScheduleDetails.font = UIFont.systemFontOfSize(size: 14.0, weight: .semibold)
            }
        }
    }
    
    private func setupInstantDelivery(){
        if let delivery = viewModel.cartDetails?.data?.delivery, !delivery{
            lblInstantDeliveryTime.text = "Not Available"
            isInstantDelivery = false
            btnInstantDelivery.isEnabled = false
        }else{
            lblInstantDeliveryTime.text = "40-50 mins"
            btnInstantDelivery.isEnabled = true
        }
    }
    
    @IBAction func valueChangedOnDeliveryAndTakeawaySegmentControl(_ sender: Any) {
        if deliveryOrTakeAwayOptionSelected != TakeAwayDelivery(rawValue: deliveryAndTakeawaySegmentControl.selectedSegmentIndex) ?? TakeAwayDelivery.Delivery{
            viewModel.getCartDetails(transaction_type: deliveryAndTakeawaySegmentControl.selectedSegmentIndex == 0 ? 1:2)
        }
        if deliveryAndTakeawaySegmentControl.selectedSegmentIndex == 1 {
            if viewModel.cartDetails?.data?.delivery ?? false, let address = viewModel.getDeliveryDistanceMessage(), (viewModel.cartDetails?.data?.address?.distance_meters ?? 0) > 0{
                lblTakeAwayDistanceMessage.attributedText = address
                viewForTakeAwayMessage.isHidden = false
                deliveryAndTakeawaySegmentControl.selectedSegmentIndex = 0
            }else{
                setupTakeAwayView()
                AnalyticsHelper.shared.triggerEvent(type: .C08)
            }
        } else {
            AnalyticsHelper.shared.triggerEvent(type: .C07)
            setupDeliveryView()
        }
        deliveryOrTakeAwayOptionSelected = TakeAwayDelivery(rawValue: deliveryAndTakeawaySegmentControl.selectedSegmentIndex) ?? TakeAwayDelivery.Delivery
        removeAppliedCoupons()
    }
    
    @IBAction func actionOnRemoveSmiles(_ sender: Any) {
        viewModel.removeSmiles()
    }
    @IBAction func actionOnUseAllSmiles(_ sender: Any) {
        viewModel.useAllSmiles()
    }
    @IBAction func onClickBtnTakeAway(_ sender: Any) {
        viewForTakeAwayMessage.isHidden = true
        AnalyticsHelper.shared.triggerEvent(type: .C08)
        setupTakeAwayView()
    }
    @IBAction func onClickBtnDelivery(_ sender: Any) {
        viewForTakeAwayMessage.isHidden = true
    }
    
    func onPaymentError(_ code: Int32, description str: String) {
        isPaymentInProgress = false
        AnalyticsHelper.shared.triggerEvent(type: .delivery_payment_failed)
        AnalyticsHelper.shared.triggerEvent(type: .C17B)
        AnalyticsHelper.shared.triggerEvent(type: .Delivery_Takeaway_Order_Status, properties: [.Delivery_Takeaway_Status: AnalyticsHelper.MoEngageAttrValue.Failed.rawValue, .Error: str ])
        print("payment error: \(code) \(str)")
    }
    
    func onPaymentSuccess(_ payment_id: String) {
        imgPaymentLoading.loadGif(name: "loader_food")
        viewForPaymentLoading.isHidden = false
        self.tabBarController?.tabBar.isHidden = true
        razorPayId = payment_id
        viewModel.paymentId = payment_id
        print("payment success")
        isPaymentInProgress = false
        AnalyticsHelper.shared.triggerEvent(type: .C17A)
        AnalyticsHelper.shared.triggerEvent(type: .delivery_payment_success)
        viewModel.validatePaymentId(transaction_type: deliveryAndTakeawaySegmentControl.selectedSegmentIndex == 0 ? 1:2)
    }
    
    func onRazorpayPaymentSuccess(_ paymentData: BBQPaymentDataModel) {
        //let razorpayModel = paymentData
        onPaymentSuccess(paymentData.paymentID ?? "")
    }
    
    private func initiateThePaymentData() {
        if razorpay == nil{
            let webView = WKWebView.init(frame: self.view.frame)
            let razorPayAppKey = Bundle.main.object(forInfoDictionaryKey: Constants.Payment.razorpayPublicKey)
            self.razorpay = Razorpay.initWithKey(razorPayAppKey as! String, andDelegate: self, withPaymentWebView: webView)
        }
        
        self.razorpay.getPaymentMethods(withOptions: nil) { (response) in
            self.paymentFetchMethods = response
        } andFailureCallback: { (error) in
            print("Error fetching payment methods:\(error)")
        }
    }
    
    internal func showPaymentForm() {
        AnalyticsHelper.shared.triggerEvent(type: .pay_clicked)
        AnalyticsHelper.shared.triggerEvent(type: .C17)
        isPaymentInProgress = true
        
        let paymenVc = UIStoryboard.loadPayment()
        let paymentData = getWebViewPaymentId(paymentVC: paymenVc)
        paymenVc.viewModel = CustomPaymentViewModel.init(razorPay: self.razorpay,razorpayOptions: paymentData.options ?? [:], delegate: paymenVc,webView: paymentData.webView, response: paymentFetchMethods)
        paymenVc.delegate = self
        self.navigationController?.pushViewController(paymenVc, animated: true)
        //        self.present(vc, animated: true, completion: nil);
        
        
        
        //CUSTOMUI
        //        razorpay.open(options)
        
        //                    "amount": "100", //This is in currency subunits. 100 = 100 paise= INR 1.
        //                    "currency": "INR",//We support more that 92 international currencies.
        //            "order_id": viewModel.paymentId,
        //            "image": UIImage(named: Constants.Payment.checkoutFormBBQIcon)!,
        //                    "prefill": ["name": BBQUserDefaults.sharedInstance.UserName, "email": SharedProfileInfo.shared.profileData?.email as Any, "contact": SharedProfileInfo.shared.profileData?.mobileNumber as Any],
        //                    "theme": [
        //                        Constants.Payment.checkoutFormThemeCodeKey:
        //                            Constants.Payment.checkoutFormThemeCode],
        //            "retry": false,
        //            "timeout": 300
        //                ]
        //        razorpay.open(options)
    }
    
    private func getWebViewPaymentId(paymentVC: BBQCustomPaymentViewController) -> (webView: WKWebView, options: [String: Any]?){
        let amount = Int(round((Float(self.viewModel.cartDetails?.data?.final_break_up ?? "0.0") ?? 0) * 100))
        let options: [String:Any] = [
            "amount": amount,
            "currency": "INR",
            "description": "",
            "order_id": viewModel.paymentId ,
            //            "name": paymentModel.name ?? "",
            "email" : SharedProfileInfo.shared.profileData?.email  ?? BBQUserDefaults.sharedInstance.customPaymentEmail,
            "contact": SharedProfileInfo.shared.profileData?.mobileNumber as Any,
            //Comment me for release
            //            "contact": 9844019983
            
        ]
        
        let webView = WKWebView.init(frame: self.view.frame)
        let razorPayAppKey = Bundle.main.object(forInfoDictionaryKey: Constants.Payment.razorpayPublicKey)
        self.razorpay = Razorpay.initWithKey(razorPayAppKey as! String, andDelegate: paymentVC, withPaymentWebView: webView)
        return (webView, options)
    }
    
    @IBAction func actionOnApplyCouponsBtn(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .C12S)
        navigateToAddCouponsScreen(isForVoucher: false)
    }
    @IBAction func actionOnApplyVouchersBtn(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .C12S)
        navigateToAddCouponsScreen(isForVoucher: true)
    }
    @IBAction func actionOnAddOrEditAddress(_ sender: Any) {
        if deliveryAndTakeawaySegmentControl.selectedSegmentIndex == 0{
            AnalyticsHelper.shared.triggerEvent(type: .clicked_dc_address_button)
            AnalyticsHelper.shared.triggerEvent(type: .C03)
            
            if lblDeliveryAddress.text  == addDeliveryAddress {
                self.payButton.isEnabled = true
                AnalyticsHelper.shared.triggerEvent(type: .CA04A)
                let vc = UIUtils.viewController(storyboardId: "DeliveryCart", vcId: "AddAddressVC") as! AddAddressVC
                isScreenAppearedFromAddAddressView = true
                vc.isFromCartScreen = true
                vc.cartScreenDelegate = self
                self.navigationController?.pushViewController(vc, animated: true)
            }

            else {
                let vc = UIUtils.viewController(storyboardId: "DeliveryCart", vcId: "ChangeAddressViewController") as! ChangeAddressViewController
                vc.selectedAddressId = viewModel.cartDetails?.data?.delivery_address_id
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else{
            openTakeAwayPicker()
        }
    }
    @IBAction func actionOnClearCart(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .delivery_clear_cart)
        viewModel.clearCart()
    }
    func setTableViewHeight() {
        self.itemsTableViewHeightConstraint.constant = self.itemsTableView.contentSize.height
        self.couponsTableViewHeightConstraint.constant = self.couponsTableView.contentSize.height
        voucherTableviewHeightConstraint.constant = voucherTableView.contentSize.height
        self.scrollView.layoutIfNeeded()
        viewDidLayoutSubviews()
    }
    
    
    func reloadData() {
        guard viewModel.isItemsAvailableInCart() else {
            self.scrollView.isHidden = true
            emptyCartImage.isHidden = false
            emptyCartLabel.isHidden = false
            addItems.isHidden = false
            viewForAddress.isHidden = true
            //In case of all items removed from cart , clear selected time slot too.
            lblDeliveryAddress.text = ""
            selectedTakeAwaySlot = nil
            return
        }
        if isLoadingFirstTime{
            AnalyticsHelper.shared.triggerEvent(type: .C02A)
            isLoadingFirstTime = false
        }
        
        viewForAddress.isHidden = false
        if let tender = viewModel.cartDetails?.data?.tender.first(where: {$0.tender_key == "SMILES"}) {
            if let smileAmount = tender.lstTender.first?.amount, smileAmount > 0 {
                if smileAmount > BBQUserDefaults.sharedInstance.UserLoyaltyPoints{
                    viewModel.removeSmiles()
                }
                stackViewForUseSmile.isHidden = true
                stackViewForUsedSmile.isHidden = false
            } else {
                stackViewForUseSmile.isHidden = false
                stackViewForUsedSmile.isHidden = true
            }
        }
        arrCoupons = [ListTender]()
        arrVouchers = [ListTender]()
        if let tenter = viewModel.cartDetails?.data?.tender.first(where: {$0.tender_key == "VOUCHER"}) {
            let tenderList = tenter.lstTender
            for i in 0..<tenderList.count {
                tenderList[i].tender_key = "VOUCHER"
            }
            arrVouchers.append(contentsOf: tenderList)
        }
        if let tenter = viewModel.cartDetails?.data?.tender.first(where: {$0.tender_key == "COUPON"}) {
            let tenderList = tenter.lstTender
            for i in 0..<tenderList.count {
                tenderList[i].tender_key = "COUPON"
            }
            arrCoupons.append(contentsOf: tenderList)
        }
        if let tenter = viewModel.cartDetails?.data?.tender.first(where: {$0.tender_key == "OFFER"}) {
            let tenderList = tenter.lstTender
            for i in 0..<tenderList.count {
                tenderList[i].tender_key = "OFFER"
            }
            arrCoupons.append(contentsOf: tenderList)
        }
        couponsTableView.isHidden = !(arrCoupons.count > 0)
        stackViewForUsePromocode.isHidden = arrCoupons.count > 0
        if let coupon_visibilty = viewModel.cartDetails?.data?.visible_apply_coupon, !coupon_visibilty{
            stackViewForUsePromocode.isHidden = true
        }
        if let voucher_visibilty = viewModel.cartDetails?.data?.visible_apply_voucher{
            stackViewForUserVoucher.isHidden = !voucher_visibilty
        }
        
        if stackViewForUserVoucher.isHidden{
            lblApplyCouponTitle.text = "Apply Coupon/Happiness card"
            if !stackViewForUsePromocode.isHidden{
                //                lblApplyCouponTitle.text = "Apply Coupon/Happiness card"
            }else{
                //                lblApplyCouponTitle.text = "Apply Happiness card"
                stackViewForUsePromocode.isHidden = false
            }
        }
        
        
        emptyCartImage.isHidden = true
        emptyCartLabel.isHidden = true
        addItems.isHidden = true
        self.scrollView.isHidden = false
        lblAvailableSmile.text = "Yay! You have \(BBQUserDefaults.sharedInstance.UserLoyaltyPoints) smiles available"
        restaurantLocationLbl.text = viewModel.cartDetails?.data?.branch_name ?? ""
        restaurantAddressLbl.text = viewModel.cartDetails?.data?.branch_address ?? ""
        restaurantLocationLbl.textColor = .darkText
        totalAmountLbl.text = "\(getCurrency())\(viewModel.cartDetails?.data?.final_break_up ?? "0.0")"
        lblToPay.text = "\(getCurrency())\(viewModel.cartDetails?.data?.final_break_up ?? "0.0")"
        lblDeliveryAddress.attributedText = viewModel.getDeliveryAddress()
        imgDeliveryAddress.image = viewModel.cartDetails?.data?.address?.getTagIcon(color: "theme").0
        lblEarnSmile.text = viewModel.cartDetails?.data?.smilesOnOrder
        //costBreakUpView.setShadow()
        itemsTableView.reloadData()
        couponsTableView.reloadData()
        voucherTableView.reloadData()
        setTableViewHeight()
        deliveryAndTakeawaySegmentControl.setEnabled(viewModel.cartDetails?.data?.takeaway ?? false, forSegmentAt: 1)
        if !(viewModel.cartDetails?.data?.delivery ?? false){
            deliveryAndTakeawaySegmentControl.setEnabled(viewModel.cartDetails?.data?.takeaway ?? false, forSegmentAt: 0)
        }else{
            deliveryAndTakeawaySegmentControl.setEnabled(viewModel.cartDetails?.data?.delivery ?? false, forSegmentAt: 0)
        }
        
        //        if let isDelivery = viewModel.cartDetails?.data?.delivery, !isDelivery{
        //            deliveryAndTakeawaySegmentControl.selectedSegmentIndex = (viewModel.cartDetails?.data?.delivery ?? false) ? 0 : 1
        //            deliveryOrTakeAwayOptionSelected = .TakeAway
        //        }else{
        deliveryAndTakeawaySegmentControl.selectedSegmentIndex = deliveryOrTakeAwayOptionSelected.rawValue
        //        }
        valueChangedOnDeliveryAndTakeawaySegmentControl(deliveryAndTakeawaySegmentControl as Any)
        for tender in viewModel.cartDetails?.data?.tender ?? [Tender](){
            if tender.tender_key == "SMILES"{
                if tender.lstTender.count > 0{
                    lblUsedSmiles.text = "\(tender.lstTender[0].amount) Smiles applied"
                    btnUsedSmiles.setTitleColor(.deliveryThemeColor, for: .normal)
                    stackViewForUseSmile.isHidden = true
                    stackViewForUsedSmile.isHidden = false
                    break
                }
                stackViewForUsedSmile.isHidden = true
                stackViewForUseSmile.isHidden = false
                if tender.message == ""{
                    lblMessageForUseSmile.text = ""
                    btnUseAllSmile.alpha = 1.0
                    btnUseAllSmile.isEnabled = true
                    btnUsePartialSmile.alpha = 1.0
                    btnUsePartialSmile.isEnabled = true
                }else{
                    lblMessageForUseSmile.text = tender.message
                    btnUseAllSmile.alpha = 0.5
                    btnUseAllSmile.isEnabled = false
                    btnUsePartialSmile.alpha = 0.5
                    btnUsePartialSmile.isEnabled = false
                }
                break
            }
        }
        if BBQUserDefaults.sharedInstance.UserLoyaltyPoints == 0 {
            stackViewForUseSmile.isHidden = true
        }
        
        
        if viewModel.cartDetails?.data?.orderingForName != "" && viewModel.cartDetails?.data?.orderingForNumber != "" {
            
            //there is details set for some other person
            let name = (viewModel.cartDetails?.data?.orderingForName ?? "") as String
            let number = (viewModel.cartDetails?.data?.orderingForNumber ?? "") as String
            lblDeliveryName.text = String(format: "%@ - %@", name, number)
        }else{
            lblDeliveryName.text = "ME - \(BBQUserDefaults.sharedInstance.customerId)"
        }
        
        billTableView.reloadData()
    }
    
    func addItems(itemId: String, itemDetailsID: Int) {
        AnalyticsHelper.shared.triggerEvent(type: .C06)
        viewModel.addItemToCart(itemId: itemId, itemDetailsID: itemDetailsID)
    }
    
    func removeItem(itemId: String, itemDetailsID: Int) {
        AnalyticsHelper.shared.triggerEvent(type: .C06A)
        viewModel.removeItemFromCart(itemId: itemId, itemDetailsID: itemDetailsID)
    }
    
    @IBAction func onClickBtnUserPartialSmile(_ sender: Any) {
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: 80)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        let directionBottomSheet = BBQRedeemDeliverySmileVC(nibName: "BBQRedeemDeliverySmileVC", bundle: nil)
        directionBottomSheet.delegate = self
        bottomSheetController.present(directionBottomSheet, on: self)
    }
    
    @IBAction func onClickBtnChangeDelivery(_ sender: Any) {
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: 80)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        let directionBottomSheet = BBQOrderingForVC(nibName: "BBQOrderingForVC", bundle: nil)
        directionBottomSheet.delegate = self
        directionBottomSheet.orderingForName = viewModel.cartDetails?.data?.orderingForName
        directionBottomSheet.orderingForNumber = viewModel.cartDetails?.data?.orderingForNumber
        bottomSheetController.present(directionBottomSheet, on: self)
    }
    @IBAction func onClickBtnRestaurantLocation(_ sender: Any) {
        if deliveryOrTakeAwayOptionSelected == .Delivery{
            actionOnAddOrEditAddress(self)
        }else{
            if let strLatitude = viewModel.cartDetails?.data?.branch_lat, let strLongitude = viewModel.cartDetails?.data?.branch_long, let latitude = Double(strLatitude), let longitude = Double(strLongitude) {
                let directionBottomSheet = Bundle.main.loadNibNamed("BookingRedirectionBottomSheet", owner: nil)!.first as! BookingRedirectionBottomSheet
                directionBottomSheet.latitude =  latitude
                directionBottomSheet.longitude =  longitude
                directionBottomSheet.bottomSheetType = .Direction
                directionBottomSheet.view.maskByRoundingCorners(cornerRadius: 10, maskedCorners: CornerMask.topCornersMask)
                if(UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
                    directionBottomSheet.isGoogleMapsAvailable = true
                }
                bottomSheetController!.present(directionBottomSheet, on: self)
            }
        }
        
    }
    @IBAction func onClickBtnPickupTimeSlot(_ sender: Any) {
        openTakeAwayPicker()
    }
    
}

extension BBQDeliveryCartVC: BBQRedeemDeliverySmileDelegate{
    func onSuccess() {
        
    }
    func cancel() {
        
    }
    func onSmileApplied(amount: Int) {
        viewModel.useAllSmiles(amount: amount)
    }
}



extension BBQDeliveryCartVC: DeliveryCartViewModelDelegate {
    func paymentFailed() {
        imgPaymentLoading.stopAnimatingGif()
        viewForPaymentLoading.isHidden = true
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func showPaymentScreen() {
        showPaymentForm()
    }
    
    func showOfferView(textMessageToDisplay : String?) {
        
        if textMessageToDisplay != nil &&  textMessageToDisplay != "" {
            
            let offer =  textMessageToDisplay
            
            let tempView = ItemOfferAppliedView().initWith(message: offer ?? "" , delegate: self)
            tempView.addToSuperView(view: nil)
        }
    }
    
    func refreshData() {
        self.reloadData()
        self.applyBrandTheme()
        if deliveryCartAmount != nil, deliveryCartAmount != "", deliveryCartAmount != "0"{
            let offerAppliedView = OfferAppliedView().initWith(code: deliveryCartCode ?? "", amount: deliveryCartAmount ?? "", type: deliveryCartType ?? "", delegate: self)
            offerAppliedView.addToSuperView(view: self)
        }
        deliveryCartCode = nil
        deliveryCartAmount = nil
        deliveryCartType = nil
        if let items = self.tabBarController?.tabBar.items {
            if let itemCount = viewModel.cartDetails?.data?.items, itemCount.count > 0 {
                var count = 0
                for item in viewModel.cartDetails?.data?.items ?? [ItemDetails](){
                    count += Int(item.quantity) ?? 0
                }
                items[1].badgeValue = "\(count)"
                items[1].badgeColor = UIColor.deliveryThemeColor
            } else {
                items[1].badgeValue = nil
            }
        }
        
        if deliveryAndTakeawaySegmentControl.selectedSegmentIndex == 1, let brand_logo = viewModel.cartDetails?.data?.brand_logo{
            imgBrandLogo.isHidden = false
            imgBrandLogo.setImagePNG(url: brand_logo)
            imgBrandLogo.setCornerRadius(5.0)
        }else{
            imgBrandLogo.isHidden = true
        }
        
        if (viewModel.cartDetails?.data?.suggestions.count ?? 0) > 0{
            viewForItemSuggestion.isHidden = false
            collectionViewForItemSuggestion.reloadData()
        }else{
            viewForItemSuggestion.isHidden = true
        }
    }
    
    func paymentSuccess() {
        
        let registrationConfirmationVC = UIStoryboard.loadRegistrationConfirmation()
        registrationConfirmationVC.modalPresentationStyle = .overFullScreen
        registrationConfirmationVC.titleText = kPaymentSuccessText
        registrationConfirmationVC.navigationText = kMyTransactionPaymentRedirectionMessage
        self.tabBarController?.tabBar.isHidden = false
        self.present(registrationConfirmationVC, animated: false) {
            sleep(UInt32(2.0))
            self.dismiss(animated: true) {
                if let tabBarController = self.navigationController?.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
                    tabBarController.selectedIndex = 2
                }
                
            }
        }
        
        if deliveryOrTakeAwayOptionSelected == .Delivery{
            if let selectedScheduleDeliverySlot = selectedScheduleDeliverySlot, selectedScheduleDeliverySlot != ""{
                AnalyticsHelper.shared.triggerEvent(type: .C18AB)
            }else{
                AnalyticsHelper.shared.triggerEvent(type: .C18AA)
            }
        }else{
            AnalyticsHelper.shared.triggerEvent(type: .C18AC)
        }
        selectedScheduleDeliverySlot = nil
        selectedTakeAwaySlot = nil
        imgPaymentLoading.stopAnimatingGif()
        viewForPaymentLoading.isHidden = true
    }
}


extension BBQDeliveryCartVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard viewModel.cartDetails != nil else {
            return 0
        }
        
        if tableView == itemsTableView {
            return viewModel.cartDetails?.data?.items.count ?? 0
        } else if tableView == voucherTableView {
            return arrVouchers.count
        } else if tableView == couponsTableView {
            return arrCoupons.count
        } else if tableView == billTableView{
            return viewModel.cartDetails?.data?.tax_break_up.count ?? 0
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == itemsTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CartItemsTableViewCell") as! CartItemsTableViewCell
            cell.setTheme()
            cell.itemName.text = viewModel.getItemName(atIndex: indexPath.row)
            if viewModel.getIsCustomizable(atIndex: indexPath.row){
                cell.itemDescription.text = viewModel.getItemDescription(atIndex: indexPath.row)
            }else{
                cell.itemDescription.text = ""
            }
            cell.foodTypeImage.image = viewModel.getItemTypeImage(atIndex: indexPath.row)
            //            cell.quantityLbl.text = viewModel.getItemQuantity(atIndex: indexPath.row)
            cell.priceLbl.text =  viewModel.getItemPrice(atIndex: indexPath.row)
            cell.quantityLbl.text = viewModel.getItemQuantity(atIndex: indexPath.row)
            cell.lblItemPrice.text = viewModel.getOriginalItemPrice(atIndex: indexPath.row)
            cell.quantityView.layer.cornerRadius = 5
            cell.quantityView.layer.shadowColor = #colorLiteral(red: 0.5141925812, green: 0.5142051578, blue: 0.5141984224, alpha: 1)
            cell.quantityView.layer.shadowOffset = CGSize(width: 0, height: 3)
            cell.quantityView.layer.shadowOpacity = 0.4
            cell.quantityView.layer.shadowRadius = 5
            cell.itemId = viewModel.getItemId(atIndex: indexPath.row)
            cell.itemDetailsID = viewModel.getItemDetailsId(atIndex: indexPath.row)
            cell.delegate = self
            return cell
            
        } else if tableView == voucherTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CouponsTableViewCell") as! CouponsTableViewCell
            cell.couponNameLbl.text = arrVouchers[indexPath.row].promocode
            cell.lblAmount.text = "-\(getCurrency())\(arrVouchers[indexPath.row].amount)"
            cell.barCode = arrVouchers[indexPath.row].barcode
            cell.tender_key = arrVouchers[indexPath.row].tender_key
            if cell.tender_key == "VOUCHER"{
                cell.lblNote.text = "Note: Happiness card value will be redeemed in full. Any remaining amount will get lapsed"
            }else{
                cell.lblNote.text = "Offer applied on the bill"
            }
            cell.btnRemove.setTitleColor(.deliveryThemeColor, for: .normal)
            cell.delegate = self
            return cell
        }else if tableView == couponsTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CouponsTableViewCell") as! CouponsTableViewCell
            cell.couponNameLbl.text = arrCoupons[indexPath.row].promocode
            cell.lblAmount.text = "-\(getCurrency())\(arrCoupons[indexPath.row].amount)"
            cell.barCode = arrCoupons[indexPath.row].barcode
            cell.tender_key = arrCoupons[indexPath.row].tender_key
            if cell.tender_key == "VOUCHER"{
                cell.lblNote.text = "Note: Happiness card value will be redeemed in full. Any remaining amount will get lapsed"
            }else if cell.tender_key == "COUPON" && arrCoupons[indexPath.row].sales_sub_type == "Takeaway" {
                cell.lblNote.text = "Offer applied on the takeaway order"
            }else if cell.tender_key == "COUPON" && arrCoupons[indexPath.row].sales_sub_type == "Delivery" {
                cell.lblNote.text = "Offer applied on the delivery order"
            }else{
                cell.lblNote.text = "Offer applied on the bill"
            }
            cell.btnRemove.setTitleColor(.deliveryThemeColor, for: .normal)
            cell.delegate = self
            return cell
        } else if tableView == billTableView{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BBQDeliveryCartBillCell", for: indexPath) as! BBQDeliveryCartBillCell
            guard let tax_break_up = viewModel.cartDetails?.data?.tax_break_up[indexPath.row] else { return UITableViewCell() }
            cell.setCellData(taxBreakUp: tax_break_up, index: indexPath.row)
            cell.delegate = self
            return cell
        }
        
        return UITableViewCell()
    }
    
    
}

extension BBQDeliveryCartVC: CartItemsTableViewCellDelegate {
    func addItem(itemId: String, itemDetailsID: Int) {
        self.addItems(itemId: itemId, itemDetailsID: itemDetailsID)
    }
    
    func reduceItem(itemId: String, itemDetailsID: Int) {
        self.removeItem(itemId: itemId, itemDetailsID: itemDetailsID)
    }
}

extension BBQDeliveryCartVC: CouponsTableViewCellDelegate {
    private func removeAppliedCoupons(){
        if let tenders = viewModel.cartDetails?.data?.tender{
            for tender in tenders {
                if tender.tender_key == "COUPON"{
                    for coupon in tender.lstTender {
                        if coupon.sales_sub_type == ""{
                            continue
                        }
                        if deliveryOrTakeAwayOptionSelected == .Delivery{
                            if isInstantDelivery{
                                if coupon.sales_sub_type != "Instant" && coupon.sales_sub_type != "Delivery"{
                                    removeCoupon(code: coupon.barcode, tender_key: coupon.tender_key)
                                }
                            }else{
                                if coupon.sales_sub_type != "Schedule" && coupon.sales_sub_type != "Delivery"{
                                    removeCoupon(code: coupon.barcode, tender_key: coupon.tender_key)
                                }
                            }
                        }else if coupon.sales_sub_type != "Takeaway" && deliveryOrTakeAwayOptionSelected == .TakeAway{
                            removeCoupon(code: coupon.barcode, tender_key: coupon.tender_key)
                        }
                    }
                    break
                }
            }
        }
    }
    
    func removeCoupon(code: String, tender_key: String) {
        if tender_key == "COUPON"{
            AnalyticsHelper.shared.triggerEvent(type: .C13)
        }else if tender_key == "OFFER"{
            AnalyticsHelper.shared.triggerEvent(type: .C21)
        }else{
            AnalyticsHelper.shared.triggerEvent(type: .C15)
        }
        UIUtils.showLoader()
        BBQServiceManager.getInstance().removeVouchers(params:
                                                        ["customer_id":BBQUserDefaults.sharedInstance.customerId,
                                                         "branch_id":BBQUserDefaults.sharedInstance.DeliveryBranchIdValue,
                                                         "tender_key":tender_key,
                                                         "barcode":code]) { (error, response) in
            UIUtils.hideLoader()
            if let response = response {
                if response.message_type == "failed" {
                    UIUtils.showToast(message: response.message)
                    if tender_key == "COUPON"{
                        AnalyticsHelper.shared.triggerEvent(type: .C13B)
                    }else if tender_key == "OFFER"{
                        AnalyticsHelper.shared.triggerEvent(type: .C21B)
                    }else{
                        AnalyticsHelper.shared.triggerEvent(type: .C15B)
                    }
                } else {
                    self.viewModel.cartDetails = response
                    self.viewModel.delegate?.refreshData()
                    if tender_key == "COUPON"{
                        AnalyticsHelper.shared.triggerEvent(type: .C13A)
                    }else if tender_key == "OFFER"{
                        AnalyticsHelper.shared.triggerEvent(type: .C21A)
                    }else{
                        AnalyticsHelper.shared.triggerEvent(type: .C15A)
                    }
                }
            } else {
                UIUtils.showToast(message: error?.message ?? UNEXPECTED_ERROR)
                if tender_key == "COUPON"{
                    AnalyticsHelper.shared.triggerEvent(type: .C13B)
                }else if tender_key == "OFFER"{
                    AnalyticsHelper.shared.triggerEvent(type: .C21B)
                }else{
                    AnalyticsHelper.shared.triggerEvent(type: .C15B)
                }
            }
            
        }
    }
    
}
extension BBQDeliveryCartVC: BBQTakeAwayTimeDelegate{
    func onSlotSelection(slot: String) {
        AnalyticsHelper.shared.triggerEvent(type: .C08A)
        if let data = slot.getTakeAwayString(){
            selectedTakeAwaySlot = slot
            lblDeliveryAddress.attributedText = data.0
            lblPickupSlotTime.textColor = .deliveryThemeColor
            lblPickupSlotTime.text = data.1
            viewForTakeawayOrder.roundCorners(cornerRadius: 5.0, borderColor: .deliveryThemeColor, borderWidth: 1.0)
            viewForTakeawayOrder.backgroundColor = .deliveryThemeColor
            lblPickupSlotTime.textColor = .deliveryThemeTextColor
        }
    }
}
extension BBQDeliveryCartVC: BBQOrderingForDelegate{
    func onSelctedOrdering(name: String, number: String) {
        
        lblDeliveryName.text = String(format: "%@ - %@", name, number)
    }
    func onSelectedMySelf() {
        
        lblDeliveryName.text = "ME - \(BBQUserDefaults.sharedInstance.customerId)"
        guard   let orderID = viewModel.cartDetails?.data?.order_id else{
            return
        }
        //call api to save orderingfor information
        viewModel.setOrderingForDetails(receiverName: "", receiverContactNo: "", itemId:  orderID)
    }
    
    func onSelectingOrderFor(name: String, number: String ){
        
        guard   let orderID = viewModel.cartDetails?.data?.order_id else{
            return
        }
        //call api to save orderingfor information
        viewModel.setOrderingForDetails(receiverName: name, receiverContactNo: number, itemId:  orderID)
    }
}
extension BBQDeliveryCartVC: OfferAppliedViewelegate{
    func actionOnDone(view: OfferAppliedView) {
        
    }
}
extension BBQDeliveryCartVC: BBQDeliveryCartBillCellDelegate{
    func didTapOnBillDetails(cell: BBQDeliveryCartBillCell, index: Int, taxBreakup: TaxBreakUp) {
        let popupVC = BillDetailsPopVC(nibName: "BillDetailsPopVC", bundle: nil)
        popupVC.modalPresentationStyle = UIModalPresentationStyle .popover
        popupVC.preferredContentSize = CGSize(width: 170, height: 130)
        popupVC.strTitle = taxBreakup.label
        popupVC.taxBreakUp = taxBreakup.details
        popupVC.view.frame.size.width = 200 > self.view.frame.size.width ? self.view.frame.size.width : 200
        popupVC.view.frame.size.height = CGFloat(60 + (taxBreakup.details.count * 26))
        var originFrame = cell.btnDetails.frame
        originFrame.size.width = 80
        originFrame.origin.x = cell.frame.width - 60
        let frame = cell.convert(originFrame, to: self.view)
        popTip.show(customView: popupVC.view, direction: .auto, in: self.view, from: frame)
        AnalyticsHelper.shared.triggerEvent(type: .C19)
    }
}
extension BBQDeliveryCartVC: BBQScheduleDeliveryDelegate{
    func onScheduleSlotSelection(slot: String) {
        selectedScheduleDeliverySlot = slot
        isInstantDelivery = false
        setupDeliveryType()
        AnalyticsHelper.shared.triggerEvent(type: .C09A)
        removeAppliedCoupons()
    }
}
extension BBQDeliveryCartVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.cartDetails?.data?.suggestions.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let suggestions = viewModel.cartDetails?.data?.suggestions, suggestions.count > indexPath.row{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BBQDeliveryCartSuggestionCell.getCellIdentifier(), for: indexPath) as! BBQDeliveryCartSuggestionCell
            cell.updateCell(indexPath: indexPath, item: suggestions[indexPath.row])
            cell.delegate = self
            return cell
        }
        return collectionView.defaultCell(indexPath: indexPath)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/2 - 20, height: 275)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}
extension BBQDeliveryCartVC: BBQDeliveryCartSuggestionCellDelegate{
    func actionOnAddItemButton(item: GetCartResponse.Suggestion, indexPath: IndexPath?) {
        viewModel.addSuggestedItemToCart(itemId: item.item_code, transaction_type: deliveryOrTakeAwayOptionSelected == .Delivery ? 1:2)
    }
    
    func actionOnRemoveItemButton(item: GetCartResponse.Suggestion, indexPath: IndexPath?) {
        
    }
    
    func actionOnImageTapped(item: GetCartResponse.Suggestion, indexPath: IndexPath?, isCameFromSearch: Bool) {
        let selectedItem = Item()
        selectedItem.itemPrice = Int(item.item_price) ?? 0
        selectedItem.id = item.item_code
        selectedItem.imageUrl = item.item_image_path
        selectedItem.foodType = item.food_type
        selectedItem.name = item.item_name
        selectedItem.description = item.item_desc
        selectedItem.isAvailable = true
        let itemDetailsView = ItemDetailView().initWith(item: selectedItem, indexPath: indexPath, isServicable: true, delegate: self)
        itemDetailsView.addToSuperView(view: self, isCameFromSearch: isCameFromSearch)
        self.itemDetailsView = itemDetailsView
    }
}

extension BBQDeliveryCartVC: ItemDetailViewDelegate{
    func actionOnDone(_ item: Item, indexPath: IndexPath?) {}
    
    func actionOnMultipleSelect(_ item: Item, indexPath: IndexPath?) {}
    
    func actionOnAddItemButton(item: Item, indexPath: IndexPath?) {
        viewModel.addSuggestedItemToCart(itemId: item.id, transaction_type: deliveryOrTakeAwayOptionSelected == .Delivery ? 1:2)
        itemDetailsView?.removeToSuperView()
        //        if let tabBarController = navigationController?.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
        //            tabBarController.tabBar.isHidden = false
        //        }
    }
    
    func actionOnRemoveItemButton(item: Item, indexPath: IndexPath?) {}
    
    func actionOnReduceItem(item: Item, indexPath: IndexPath?) {}
    
    func didHideItemDetails(isCameFromSearch: Bool) {
        //        if let tabBarController = navigationController?.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
        //            tabBarController.tabBar.isHidden = false
        //        }
        itemDetailsView = nil
    }
}

extension BBQDeliveryCartVC: RazorpayPaymentCompletionProtocol{
    func onPaymentSuccess(_ payment_id: String, andData response: [AnyHashable : Any]) {
        
        //        self.presentedViewController?.dismiss(animated: true, completion: nil)
        if let tabBarController = navigationController?.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
            self.navigationController?.popToViewController(tabBarController, animated: true)
        }
        razorPayId = payment_id
        viewModel.paymentId = payment_id
        print("payment success")
        isPaymentInProgress = false
        
        AnalyticsHelper.shared.triggerEvent(type: .C17A)
        AnalyticsHelper.shared.triggerEvent(type: .delivery_payment_success)
        imgPaymentLoading.loadGif(name: "loader_food")
        viewForPaymentLoading.isHidden = false
        self.tabBarController?.tabBar.isHidden = true
        
        viewModel.validatePaymentId(transaction_type: deliveryAndTakeawaySegmentControl.selectedSegmentIndex == 0 ? 1:2)
    }
    
    func onPaymentError(_ code: Int32, description str: String, andData response: [AnyHashable : Any]) {
        if let tabBarController = navigationController?.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
            self.navigationController?.popToViewController(tabBarController, animated: true)
        }
        isPaymentInProgress = false
        print("payment error: \(code) \(str)")
        switch code {
        case 0,1,3:
            PopupHandler.showSingleButtonsPopup(title: kFailedTitle,
                                                message: str,
                                                on: self, firstButtonTitle: kOkButtonTitle) { }
        case 2:
            self.showTransactionCancelled()
            
        default:
            print("");
        }
        
    }
    
    private func showTransactionCancelled() {
        PopupHandler.showSingleButtonsPopup(title: kMyCartTransactionCancelTitle,
                                            message: kMyCartTransactionCancelMessage,
                                            on: self,
                                            firstButtonTitle: kButtonOkay) { }
    }
    
}

extension BBQDeliveryCartVC: BBQCustomPaymentViewControllerDelegate{
    func createRazorPayPaymentId(paymentVC: BBQCustomPaymentViewController, completion: @escaping (WKWebView, [String : Any]?, String?, Razorpay?, String?) -> Void) {
        viewModel.generatePaymentId(transactionType: deliveryAndTakeawaySegmentControl.selectedSegmentIndex == 0 ? 1 : 2, deliveryTime: deliveryAndTakeawaySegmentControl.selectedSegmentIndex == 0 ? selectedScheduleDeliverySlot ?? "" : selectedTakeAwaySlot ?? "", ordering_name: viewModel.cartDetails?.data?.orderingForName ?? BBQUserDefaults.sharedInstance.UserName, ordering_phonenumber: viewModel.cartDetails?.data?.orderingForNumber ?? SharedProfileInfo.shared.profileData?.mobileNumber ?? BBQUserDefaults.sharedInstance.customerId) { paymentId, errorString in
            
            let data = self.getWebViewPaymentId(paymentVC: paymentVC)
            completion(data.webView, data.options, paymentId, self.razorpay,errorString)
            
        }
    }
}
extension BBQDeliveryCartVC: ItemOfferAppliedViewDelegate{
    
    func actionOnDone(view: ItemOfferAppliedView){
        
        print("actionOnDone(view: ItemOfferAppliedView)")
    }
}
extension BBQDeliveryCartVC: AddAddressOnCartVCDelegate {
    
    func getAddedAddress(address: Address){
        
        
        UIUtils.showLoader()
        
        viewModel.addressViewModel.getSavedAddress(customerId: BBQUserDefaults.sharedInstance.customerId,
                                                   branchId: BBQUserDefaults.sharedInstance.DeliveryBranchIdValue, filterWithID: nil) {
            
           UIUtils.hideLoader()
            
//            self.viewModel.checkDistanceAPI(ordering_name: self.viewModel.cartDetails?.data?.orderingForName  ?? BBQUserDefaults.sharedInstance.UserName, ordering_phonenumber: self.viewModel.cartDetails?.data?.orderingForNumber ?? SharedProfileInfo.shared.profileData?.mobileNumber ?? BBQUserDefaults.sharedInstance.customerId, deliveryTime: self.selectedScheduleDeliverySlot ?? "", deliveryId: address.addressId, isClickedFromPayBtn: false) { isServicable in
//                if(isServicable){
                    let nsAttributedString = NSMutableAttributedString(string: address.getTagIcon(color: "theme").1 , attributes: [NSAttributedString.Key.font: UIFont.systemFontOfSize(size: 14.0, weight: .bold), NSAttributedString.Key.foregroundColor: UIColor(named: "TextColor") as Any])
                    let subTitleString = NSAttributedString(string: "-\(address.flatNo ), \(address.address )", attributes: [NSAttributedString.Key.font: UIFont.systemFontOfSize(size: 14.0, weight: .regular), NSAttributedString.Key.foregroundColor: UIColor(named: "TextColor")!])
                    nsAttributedString.append(subTitleString)
                    
                    self.lblDeliveryAddress.attributedText = nsAttributedString
                    self.viewModel.cartDetails?.data?.address = address
                    
//                }else{
//                    UIUtils.showToast(message: "Delivery is not available")
//                    self.payButton.isEnabled = false
//                }
//            }
            
        }
        
        
    }
}
