//
 //  Created by Mahmadsakir on 16/12/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified BBQRedeemSmilesViewController.swift
 //

import UIKit

class BBQTakeAwayTimeVC: UIViewController {
    
    //MARK:- View Properties
    @IBOutlet weak var heightsForCollectionView: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewForTiming: UICollectionView!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    var pickUpAvailabilityTime = [PickUpDateTime]()
    var selectedIndex = 0
    var selectedTime: Int?
    //MARK:- Class properties
    var delegate: BBQTakeAwayTimeDelegate?
    var strTitle = "Select Takeaway Date & Time"
    var backgroundColor = UIColor.deliveryThemeColor
    var textColor = UIColor.deliveryThemeTextColor
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionViewForTiming.register(UINib(nibName: "BBQTakeAwayTimeCell", bundle: nil), forCellWithReuseIdentifier: "BBQTakeAwayTimeCell")
        collectionView.register(UINib(nibName: "BBQTakeAwayDayCell", bundle: nil), forCellWithReuseIdentifier: "BBQTakeAwayDayCell")
        collectionView.register(UINib(nibName: "BBQTakeAwayTimeCell", bundle: nil), forCellWithReuseIdentifier: "BBQTakeAwayTimeCell")
        self.view.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        setTheme()
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        collectionView.reloadData()
        collectionViewForTiming.reloadData()
        lblTitle.text = strTitle
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        collectionView.reloadData()
        collectionViewForTiming.reloadData()
        lblTitle.text = strTitle
    }
    
    private func setTheme() {
        btnConfirm.roundCorners(cornerRadius: btnConfirm.frame.size.height/2, borderColor: .clear, borderWidth: 0)
        btnConfirm.dropShadow()
    }
    
    private func unSelectButton(_ sender: UIButton){
        sender.isEnabled = false
        sender.alpha = 0.5
    }
    
    private func selectButton(_ sender: UIButton){
        sender.isEnabled = true
        sender.alpha = 1.0
    }
    
    //MARK:- Button Action
    @IBAction func onClickBtnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onClickBtnConfirm(_ sender: Any) {
        if let selectedTime = selectedTime, pickUpAvailabilityTime.count > selectedIndex, pickUpAvailabilityTime[selectedIndex].time.count > selectedTime{
            self.delegate?.onSlotSelection(slot: pickUpAvailabilityTime[selectedIndex].time[selectedTime])
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    //MARK:- Web Service
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension BBQTakeAwayTimeVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collectionView{
            return pickUpAvailabilityTime.count
        }else{
            if pickUpAvailabilityTime.count > section{
                return pickUpAvailabilityTime[selectedIndex].time.count
            }
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.collectionView{
            if pickUpAvailabilityTime.count <= indexPath.row{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BBQTakeAwayTimeCell", for: indexPath) as! BBQTakeAwayTimeCell
                return cell
            }
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BBQTakeAwayDayCell", for: indexPath) as! BBQTakeAwayDayCell
            cell.setCellData(isSelected: selectedIndex == indexPath.row ? true : false, date: pickUpAvailabilityTime[indexPath.row].dateLabel, day:  pickUpAvailabilityTime[indexPath.row].dayLabel, backgroundColor: backgroundColor, textColor: textColor)
            return cell
        }else if pickUpAvailabilityTime.count > selectedIndex, pickUpAvailabilityTime[selectedIndex].strTime.count > indexPath.row{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BBQTakeAwayTimeCell", for: indexPath) as! BBQTakeAwayTimeCell
            cell.setCellData(isSelected: selectedTime == indexPath.row ? true : false, time: pickUpAvailabilityTime[selectedIndex].strTime[indexPath.row], backgroundColor: backgroundColor, textColor: textColor)
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BBQTakeAwayTimeCell", for: indexPath) as! BBQTakeAwayTimeCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.collectionView{
            if pickUpAvailabilityTime.count > indexPath.row{
                var width = pickUpAvailabilityTime[indexPath.row].dayLabel.width(withConstrainedHeight: 20.0, font: UIFont.systemFontOfSize(size: 14.0, weight: .semibold))
                if width <= 35{
                    width = 35
                }
                return CGSize(width: width + 32.0, height: 76.0)
            }
            return CGSize(width: 78.0, height: 78.0)
        }else if pickUpAvailabilityTime[selectedIndex].strTime.count > indexPath.row{
            return CGSize(width: 84.0, height: 40)
        }
        return .zero
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.collectionView{
            if selectedIndex != indexPath.row{
                selectedIndex = indexPath.row
                selectedTime = nil
//                unSelectButton(btnConfirm)
            }
        }else{
            selectedTime = indexPath.row
//            selectButton(btnConfirm)
            if let selectedTime = selectedTime, pickUpAvailabilityTime.count > selectedIndex, pickUpAvailabilityTime[selectedIndex].time.count > selectedTime{
                self.delegate?.onSlotSelection(slot: pickUpAvailabilityTime[selectedIndex].time[selectedTime])
                self.dismiss(animated: true, completion: nil)
            }
        }
        collectionView.reloadData()
        collectionViewForTiming.reloadData()
    }
}

protocol BBQTakeAwayTimeDelegate{
    func onSlotSelection(slot: String)
}
