//
 //  Created by Mahmadsakir on 09/02/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQTakeAwayTimeCell.swift
 //

import UIKit

class BBQTakeAwayTimeCell: UICollectionViewCell {
    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var lblTime: UILabel!
    
    var time: String = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setCellData(isSelected: Bool, time: String, backgroundColor: UIColor, textColor: UIColor) {
        if isSelected{
            viewForContainer.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0.0)
            viewForContainer.backgroundColor = backgroundColor
            lblTime.textColor = textColor
        }else{
            viewForContainer.roundCorners(cornerRadius: 5.0, borderColor: .lightGray, borderWidth: 1.0)
            viewForContainer.backgroundColor = .clear
            lblTime.textColor = .text
        }
        self.time = time
        lblTime.text = time
    }

}
