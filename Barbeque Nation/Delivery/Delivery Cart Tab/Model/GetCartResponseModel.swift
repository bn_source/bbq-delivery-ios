//
//  GetCartResponseModel.swift
//  BBQ
//
//  Created by Rahul S on 04/10/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import UIKit
import ObjectMapper

final class GetCartResponseModel: Mappable {
    
    var message = ""
    var message_type = ""
    var data: GetCartResponse?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        message <- map["message"]
        message_type <- map["message_type"]
        data <- map["data"]
    }
}


final class GetCartResponse: Mappable {
    
    var order_id = ""
    var customer_id = ""
    var branch_id = ""
    var branch_name = ""
    var branch_address = ""
    var created_on = ""
    var delivery_address_id = ""
    var delivery_address = ""
    var items = [ItemDetails]()
    var order_status = ""
    var orderingForNumber = ""
    var orderingForName = ""
    var final_break_up = ""
    var tax_break_up = [TaxBreakUp]()
    var pickUpAvailabilityTime = [PickUpDateTime]()
    var scheduleAvailablityTime = [PickUpDateTime]()
    var lst_delivery_schedule_interval: TimeInterval = 1800
    var tender = [Tender]()
    var delivery = false
    var takeaway = false
    var address: Address?
    var smilesOnOrder = ""
    var branch_lat: String?
    var branch_long: String?
    var visible_apply_voucher = true
    var visible_apply_coupon = true
    var suggestions = [Suggestion]()
    var backgroudColor: UIColor = .deliveryThemeColor
    var textColor: UIColor = .deliveryThemeTextColor
    var colorCode = ""
    var brand_name = ""
    var brand_logo = ""
    var brand_id = ""
    var last_offer_display : String?
    
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        order_id <- map["order_id"]
        customer_id <- map["customer_id"]
        branch_id <- map["branch_id"]
        branch_name <- map["branch_name"]
        branch_address <- map["branch_address"]
        created_on <- map["created_on"]
        delivery_address_id <- map["delivery_address_id"]
        delivery_address <- map["delivery_address"]
        items <- map["item"]
        order_status <- map["order_status"]
        final_break_up <- map["final_break_up"]
        tax_break_up <- map["lst_tax_charges"]
        pickUpAvailabilityTime <- map["lst_delivery_dates"]
        scheduleAvailablityTime <- map["lst_delivery_schedule_dates"]
        lst_delivery_schedule_interval <- map["lst_delivery_schedule_interval"]
        tender <- map["tender"]
        delivery <- map["delivery"]
        takeaway <- map["takeaway"]
        address <- map["address"]
        smilesOnOrder <- map["smiles_on_order"]
        branch_lat <- map["latitude"]
        branch_long <- map["longitude"]
        visible_apply_voucher <- map["visible_apply_voucher"]
        visible_apply_coupon <- map["visible_apply_coupon"]
        orderingForName <- map["ordering_for_name"]
        orderingForNumber <- map["ordering_for_number"]
        suggestions <- map["suggestions"]
        colorCode <- map["brand_color_code"]
        brand_name <- map["brand_name"]
        brand_logo <- map["brand_logo"]
        brand_id <- map["brand_id"]
        last_offer_display <- map["last_offer_display"]
        
        let codes = colorCode.components(separatedBy: "|")
        if codes.count > 1{
            if let first = codes.first{
                backgroudColor = first.hexStringToUIColor()
            }
            if let last = codes.last{
                textColor = last.hexStringToUIColor()
            }
            
        }
        
        for pickupTime in pickUpAvailabilityTime {
            pickupTime.addToTakeawaySlots()
        }
        
        for pickupTime in scheduleAvailablityTime {
            pickupTime.addToScheduleSlots()
        }
    }
    
    
    final class Suggestion: Mappable {
        var item_image_path = ""
        var item_desc = ""
        var food_type = FoodType.veg
        var item_price = ""
        var item_code = ""
        var item_name = ""
        
        init() {}
        
        required init?(map: Map) {}
        
        func mapping(map: Map) {
            item_image_path <- map["item_image_path"]
            item_desc <- map["item_desc"]
            food_type <- map["food_type"]
            item_price <- map["item_price"]
            item_code <- map["item_code"]
            item_name <- map["item_name"]
        }
    }
}

final class PickUpDateTime: Mappable {
    var date = ""
    var time = [String]()
    var dayLabel = ""
    var dateLabel = ""
    var date_label = ""
    var strTime = [String]()
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        date <- map["date"]
        time <- map["time"]
        date_label <- map["date_label"]
        
        DispatchQueue.main.async { [self] in
            let arrData = date.components(separatedBy: "-")
            if date.isToday(format: "dd-MMM-yyyy"){
                dayLabel = "Today"
                dateLabel = arrData.first ?? ""
            }else if date.isTomorrow(format: "dd-MMM-yyyy"){
                dayLabel = "Tomorrow"
                dateLabel = arrData.first ?? ""
            }else{
                dateLabel = arrData.first ?? ""
                if arrData.count >= 2 {
                    dayLabel = arrData[1]
                }
            }
        }
        
    }
    
    func addToScheduleSlots() {
        DispatchQueue.main.async { [self] in
            strTime.removeAll()
            for timeLabel in time{
                let date = timeLabel.toDate(format: "yyyy/MM/dd hh:mm a")
                if let dateLabel = date?.addingTimeInterval(1800).string(format: "hh:mm a") {
                    strTime.append((date?.string(format: "hh:mm a") ?? timeLabel) + " - " + dateLabel)
                }else{
                    strTime.append(date?.string(format: "hh:mm a") ?? timeLabel)
                }
                
            }
        }
    }
    
    func addToTakeawaySlots() {
        DispatchQueue.main.async { [self] in
            strTime.removeAll()
            for timeLabel in time{
                let date = timeLabel.toDate(format: "yyyy/MM/dd hh:mm a")
                strTime.append(date?.string(format: "hh:mm a") ?? timeLabel)
            }
        }
    }
}

final class Tender: Mappable {
    var lstTender = [ListTender]()
    var tender_key = ""
    var message = "message"
    
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        lstTender <- map["lstTender"]
        tender_key <- map["tender_key"]
        message <- map["message"]
    }
}

final class ListTender: Mappable {
    var amount = 0
    var barcode = ""
    var payment_id = ""
    var tender_key = ""
    var promocode = ""
    var sales_sub_type = ""
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        amount <- map["amount"]
        barcode <- map["barcode"]
        promocode <- map["promocode"]
        payment_id <- map["payment_id"]
        sales_sub_type <- map["sales_sub_type"]
        if promocode == ""{
            promocode = barcode
        }
    }
}

final class TaxBreakUp: Mappable {
    var label = ""
    var value = ""
    var details = [TaxBreakUp]()
    
    required init?(map: Map) {}
    
    init(label: String, value: String) {
        self.label = label
        self.value = value
    }
    
    func mapping(map: Map) {
        label <- map["label"]
        value <- map["value"]
        details <- map["lst_tax_charges_detail"]
        self.covertValueToFree()
    }
    
    func covertValueToFree(){
        for i in 0..<details.count {
            if let amount = Int(details[i].value), amount == 0{
                if !details[i].label.lowercased().contains("smile"), !details[i].label.lowercased().contains("promo"), !details[i].label.lowercased().contains("discount"),!details[i].label.lowercased().contains("offer"){
                    self.details[i].value = "FREE"
                }
            }
        }
        
    }
}


final class ItemDetails: Mappable {
    var item_id = ""
    var item_name = ""
    var item_desc = ""
    var food_type = FoodType.veg
    var price = ""
    var quantity = ""
    var total = ""
    var option = [ItemOptions]()
    var item_detail_id: Int = 0
    var isOfferAvailable : String?

    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        item_id <- map["item_id"]
        item_name <- map["item_name"]
        item_desc <- map["item_desc"]
        food_type <- map["food_type"]
        price <- map["price"]
        quantity <- map["quantity"]
        total <- map["total"]
        option <- map["option"]
        item_detail_id <- map["item_detail_id"]
        price = "\(Int(Double(price) ?? 0))"
    }
}

final class ItemOptions: Mappable {
    var item_id = ""
    var item_name = ""
    var item_desc = ""
    var food_type = ""
    var price = ""
    var quantity = ""
    var total = ""
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        item_id <- map["item_id"]
        item_name <- map["item_name"]
        item_desc <- map["item_desc"]
        food_type <- map["food_type"]
        price <- map["price"]
        quantity <- map["quantity"]
        total <- map["total"]
    }
}


final class Coupon: Mappable {
    var barcode = ""
    var denomination = 0
    var status = ""
    var transaction_type = ""
    var couponDescription = ""
    var voucher_type = ""
    var used_in_mobile_app = false
    var title = ""
    var rule_description = ""
    var sales_sub_type = ""
    var validity : String?
    var voucherType : String?
    var voucher_head_id : Int?

    required init?(map: Map) {}
    
    func mapping(map: Map) {
        barcode <- map["barcode"]
        denomination <- map["denomination"]
        status <- map["status"]
        transaction_type <- map["transaction_type"]
        couponDescription <- map["description"]
        title <- map["title"]
        voucher_type <- map["voucher_type"]
        used_in_mobile_app <- map["used_in_mobile_app"]
        rule_description <- map["rule_description"]
        sales_sub_type <- map["sales_sub_type"]
       validity  <- map["validity"]
       voucher_head_id <- map["voucher_head_id"]
       voucherType  <- map["voucher_type"]
    }
    
}


final class CouponResponseModel: Mappable {
    
    var message = ""
    var message_type = ""
    var data: CouponsResponse?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        message <- map["message"]
        message_type <- map["message_type"]
        data <- map["data"]
    }
}


final class CouponsResponse: Mappable {
    
    var vouchers = [Coupon]()
    var coupons = [Coupon]()
    var offers = [Coupon]()
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        vouchers <- map["voucher"]
        coupons <- map["coupon"]
        offers <- map["offer"]
    }
}
