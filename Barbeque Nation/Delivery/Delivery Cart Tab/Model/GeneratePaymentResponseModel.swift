//
//  GeneratePaymentResponseModel.swift
//  BBQ
//
//  Created by Rahul S on 04/10/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import UIKit
import ObjectMapper

final class GeneratePaymentResponseModel: Mappable {
    
    var message = ""
    var message_type = ""
    var data: PaymentOrderId?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        message <- map["message"]
        message_type <- map["message_type"]
        data <- map["data"]
    }
}

final class PaymentOrderId: Mappable {

    var payment_order_id = ""
    required init?(map: Map) {}
    func mapping(map: Map) {
        payment_order_id <- map["payment_order_id"]
    }
}

final class ValidatePaymentResponseModel: Mappable {
    var message = ""
    var message_type = ""
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        message <- map["message"]
        message_type <- map["message_type"]
    }
}

final class CheckDistanceResponseModel: Mappable {
    var message = ""
    var message_type = ""
    var data: CheckDistance?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        message <- map["message"]
        message_type <- map["message_type"]
        data <- map["data"]
    }
}

final class CheckDistance: Mappable {

    var servicable = false
    var distance = ""
    var duration = ""
    
    required init?(map: Map) {}
    func mapping(map: Map) {
        servicable <- map["servicable"]
        distance <- map["distance"]
        duration <- map["duration"]
    }
}



final class AddAddressResponseModel: Mappable {
    
    var message = ""
    var message_type = ""
    var data: Addresses?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        message <- map["message"]
        message_type <- map["message_type"]
        data <- map["data"]
    }
}


final class Addresses: Mappable {

    var address = [Address]()
    
    required init?(map: Map) {}
    func mapping(map: Map) {
        address <- map["address"]
    }
}


final class AddCouponResponseModel: Mappable {
    var message = ""
    var message_type = ""
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        message <- map["message"]
        message_type <- map["message_type"]
    }
}


final class ValidateCouponResponseModel: Mappable {
    var message = ""
    var message_type = ""
    var data: ValidateCoupon?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        message <- map["message"]
        message_type <- map["message_type"]
        data <- map["data"]
    }

}



final class ValidateCoupon: Mappable {

    var denomination = 0
    var tender_type = ""
    var bar_code = ""
    var promocode = ""
    
    required init?(map: Map) {}
    func mapping(map: Map) {
        denomination <- map["denomination"]
        tender_type <- map["tender_type"]
        bar_code <- map["barcode"]
        promocode <- map["promocode"]
    }
}
