//
//  UIUtils.swift
//  BBQ
//
//  Created by Dharani Sadasivam on 11/08/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import Foundation
import UIKit

import Toast_Swift

class UIUtils {
    
    static var loadingIndicator: CircularProgressView = {
        let progress = CircularProgressView(colors: [.theme], lineWidth: 5)
        progress.translatesAutoresizingMaskIntoConstraints = false
       
        return progress
    }()
    
    static func viewController(storyboardId: String, vcId: String) -> UIViewController {
        let storyboard = UIStoryboard.init(name: storyboardId, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: vcId)
    }
    static func showLoader(in viewController:UIViewController) {
        viewController.view.addSubview(self.loadingIndicator);
        self.showLoader(message: "");
    }
    static func showLoader(message: String = "Loading...") {
//        if let view = UIApplication.shared.windows.first!.rootViewController?.view {
//            let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
//            loadingNotification.mode = MBProgressHUDMode.indeterminate
            /*loadingNotification.label.text = message
             loadingNotification.bezelView.color = UIColor.white
             loadingNotification.backgroundView.color = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3)*/
        if let window = UIApplication.shared.keyWindow ?? UIApplication.shared.windows.last ?? UIApplication.shared.windows.first{
            window.addSubview(loadingIndicator)
            loadingIndicator.isHidden = false
            loadingIndicator.superview?.isUserInteractionEnabled = false
            loadingIndicator.isAnimating = true
        }
        
      
//        }
    }
    
    static func hideLoader() {
        if loadingIndicator.superview != nil{
            loadingIndicator.superview?.isUserInteractionEnabled = true
            loadingIndicator.isAnimating = false
            loadingIndicator.isHidden = true
            loadingIndicator.removeFromSuperview()
        }
        else {
            
            loadingIndicator.superview?.isUserInteractionEnabled = true
            loadingIndicator.isAnimating = false
            loadingIndicator.isHidden = true

        }
        UIApplication.shared.windows.first!.rootViewController?.view.addSubview(loadingIndicator)

    }
    
    static func showToast(message: String) {
        if let window = UIApplication.shared.windows.last ?? UIApplication.shared.keyWindow ?? UIApplication.shared.windows.first{
            window.makeToast(message, position: .top)
        }
    }
    
    static func updateCartTabBadgeCount(_ itemIds: [String], count: Int) {
        DeliveryTabBarController.cartItemIds = itemIds
        DeliveryTabBarController.cartItemCount = count
        NotificationCenter.default.post(name: .cartTabBadgeCount, object: nil)
    }
    
    static func updateCartTabBadgeCountNew(count: Int) {
        DeliveryTabBarController.cartItemCount = count
        NotificationCenter.default.post(name: .cartTabBadgeCount, object: nil)
    }
    
    static func increaseCartTabBadgeCount(_ itemId: String) {
//        if !DeliveryTabBarController.cartItemIds.contains(where: {$0 == itemId}) {
//            DeliveryTabBarController.cartItemIds.append(itemId)
//            NotificationCenter.default.post(name: .cartTabBadgeCount, object: nil)
//        }
    }
    
    static func decreaseCartTabBadgeCount(_ itemId: String) {
//        if let index = DeliveryTabBarController.cartItemIds.firstIndex(where: {$0 == itemId}) {
//            DeliveryTabBarController.cartItemIds.remove(at: index)
//            NotificationCenter.default.post(name: .cartTabBadgeCount, object: nil)
//        }
    }
    
    static func callNumber(phoneNumber:String) {
        if let phoneCallURL = URL(string: "tel://\(phoneNumber)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL, options: [:], completionHandler: nil)
                } else {
                    application.openURL(phoneCallURL as URL)
                    
                }
            }
        }
    }
    
}

extension UITextField{
    func maxCharacter(maxLength length: Int, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = self.text as NSString? ?? ""
        if Int(string) != nil || string == "" {
            let newString1 = string.replacingOccurrences(of: " ", with: "")
            if newString1.count >= length{
                self.text = String(newString1.suffix(length))
                return false
            }
            
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            if isValidNumber(number: String(newString)) {
                return true
            } else {
                if newString.length > length{
                    if string == "" && range.length > 0{
                        return true
                    }
                    return false
                } else {
                    return true
                }
            }
        } else { // allow only digita
            let newString = string.replacingOccurrences(of: " ", with: "")
            if newString.count >= length{
                self.text = String(newString.suffix(length))
            }else if string == " "{
                return true
            }
            return false
        }
    }
    
    func isValidNumber(number: String) -> Bool {
        return BBQPhoneNumberValidation.isValidNumber(number: number, for: BBQUserDefaults.sharedInstance.phoneCode)
    }
}
