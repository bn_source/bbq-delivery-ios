//
 //  Created by Sakir on 20/05/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified DeliveryHostMenuViewController.swift
 //

import UIKit
import AKSideMenu


final class DeliveryHostMenuViewController:  AKSideMenu, AKSideMenuDelegate, BBQHostMenuDelegate {
    
    weak var navigationVC: UINavigationController? = nil
    
    override var prefersStatusBarHidden: Bool {
        return false
    }

    override public func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentViewShadowColor = .black
        self.contentViewShadowOffset = .zero
        self.contentViewShadowOpacity = 0.6
        self.contentViewShadowRadius = 12
        self.contentViewShadowEnabled = true
        self.delegate = self
        
        self.navigationVC = UIStoryboard.loadNavigationViewController() as? UINavigationController
        self.contentViewController = self.navigationVC
        self.rightMenuViewController = nil
        let menuVC = UIStoryboard.loadMenuViewController()
        menuVC.sideMenuDelegate = self
        menuVC.navigationVC = self.navigationVC
        self.leftMenuViewController = menuVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }


    func showSideMenu(viewController: UIViewController, delegate: BBQMenuViewControllerProtocol) {
        if let menuVC = self.leftMenuViewController as? BBQMenuViewController{
            menuVC.delegate = delegate
        }
        presentLeftMenuViewController()
    }
    
    private func contentControllers() -> [UIViewController] {
        return [UIStoryboard.loadNavigationViewController()]
    }
    
    // MARK: - <AKSideMenuDelegate>

    public func sideMenu(_ sideMenu: AKSideMenu, shouldRecognizeGesture recognizer: UIGestureRecognizer, simultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if let _ = navigationVC?.viewControllers.last(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
            return false
        }
        return true
    }
    
    public func sideMenu(_ sideMenu: AKSideMenu, gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        // return true or false based on your failure requirements. Returns false by default
        if let _ = navigationVC?.viewControllers.last(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
            return false
        }
        return true
    }
    
    public func sideMenu(_ sideMenu: AKSideMenu, gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        // return true or false based on your failure requirements. Returns false by default
        if let _ = navigationVC?.viewControllers.last(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
            return false
        }
        return true
    }

    
    public func sideMenu(_ sideMenu: AKSideMenu, willShowMenuViewController menuViewController: UIViewController) {
        debugPrint("willShowMenuViewController")
    }

    public func sideMenu(_ sideMenu: AKSideMenu, didShowMenuViewController menuViewController: UIViewController) {
        debugPrint("didShowMenuViewController")
    }

    public func sideMenu(_ sideMenu: AKSideMenu, willHideMenuViewController menuViewController: UIViewController) {
        debugPrint("willHideMenuViewController")
    }

    public func sideMenu(_ sideMenu: AKSideMenu, didHideMenuViewController menuViewController: UIViewController) {
        debugPrint("didHideMenuViewController")
    }

    func hideSideMenu() {
        hideMenuViewController()
    }
    
}
