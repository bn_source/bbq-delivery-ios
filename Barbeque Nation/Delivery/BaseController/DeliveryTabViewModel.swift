//
 //  Created by Mahmadsakir on 22/04/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified DeliveryTabViewModel.swift
 //

import Foundation
import UIKit

final class DeliveryTabViewModel {
    var isForceUpdate : Bool = false
    var isRecommendUpdate : Bool = false
    var isUpdateRequired : Bool = false
    
    
    func getAppupdateInformation(completion: @escaping (_ result: Bool)->()) {
        if let appVersion = UIApplication.appVersion {
            let deviceType = "IOS"
            BBQUserManager.getAppUpdateInformation(appVersion: appVersion,
                                                   deviceType: deviceType) { (model, isResult) in
                                                    if isResult {
                                                        self.isForceUpdate = model?.isForceUpdate ?? false
                                                        self.isRecommendUpdate = model?.isRecommendedUpdate ?? false
                                                        self.isUpdateRequired = self.isForceUpdate  || self.isRecommendUpdate
                                                        completion(self.isUpdateRequired)
                                                    } else {
                                                        self.isUpdateRequired = false
                                                        completion(self.isUpdateRequired)
                                                    }
            }
        } else {
            completion(false)
        }
    }
}
