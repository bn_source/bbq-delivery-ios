//
//  Created by Shareef on 11/10/20
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
//  All rights reserved.
//  Last modified DeliveryTabBarController.swift
//

import UIKit
import CoreLocation


class DeliveryTabBarController: UITabBarController {
    
    var navigationFromScreen: LoginNavigationScreen = .Splash
    static var cartItemIds = [String]()
    static var cartItemCount = 0
    static var cartItemPrice = 0
    var tabDelegate: DeliveryTabBarControllerDelegate?
    var userDeniedLocation = false
    var isAppViewWillAppearCalled = false
    
    lazy var cartViewModel : CartViewModel = {
        let viewModel = CartViewModel()
        return viewModel
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.getUpdatedPoints()
        tabBar.barTintColor = UIColor.white
        tabBar.tintColor = UIColor.theme
        tabBar.unselectedItemTintColor = .darkText
        tabBar.backgroundColor = .white
        
        if #available(iOS 13.0, *) {
            tabBar.standardAppearance.backgroundColor = .white
            
            let appearance = UITabBarAppearance()
            appearance.stackedLayoutAppearance.selected.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.theme]
            appearance.stackedLayoutAppearance.selected.titleTextAttributes = [NSAttributedString.Key.font: UIFont.appThemeMediumWith(size: 12)]
            appearance.stackedLayoutAppearance.normal.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.darkText]
            appearance.stackedLayoutAppearance.normal.titleTextAttributes = [NSAttributedString.Key.font: UIFont.appThemeRegularWith(size: 12)]
            tabBar.standardAppearance = appearance
        } else {
            UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.theme], for: .selected)
            UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.darkText], for: .normal)
            UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont.appThemeRegularWith(size: 12)], for: .normal)
            UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont.appThemeMediumWith(size: 12)], for: .selected)
        }

        
        //Reservation
        let reservationViewController = UIStoryboard.loadHomeViewController()
        reservationViewController.navigationFromScreen = navigationFromScreen
        let reservationTabBarItem = UITabBarItem(title: "Reservation", image: UIImage(named: "icon_tab_reservation"), selectedImage: UIImage(named: "icon_tab_reservation"))
        reservationViewController.tabBarItem = reservationTabBarItem
        reservationViewController.isNeedLoadLoyalityPoints = false
        //Cart
        let deliveryCartViewController = UIUtils.viewController(storyboardId: "DeliveryCart", vcId: "BBQDeliveryCartVC") as! BBQDeliveryCartVC
        let cartTabBarItem = UITabBarItem(title: "My Cart", image: UIImage(named: "icon_tab_cart"), selectedImage: UIImage(named: "icon_tab_cart"))
        deliveryCartViewController.tabBarItem = cartTabBarItem
        //Home
        let deliveryHomeViewController = UIUtils.viewController(storyboardId: "DeliveryHome", vcId: "DeliveryHomeViewController") as! DeliveryHomeViewController
        let deliveryTabBarItem = UITabBarItem(title: "Delivery", image: UIImage(named: "icon_tab_delivery"), selectedImage: UIImage(named: "icon_tab_delivery"))
        deliveryHomeViewController.tabBarItem = deliveryTabBarItem
        //self.tabDelegate = deliveryHomeViewController
        self.viewControllers = [reservationViewController, deliveryCartViewController, deliveryHomeViewController]
        self.selectedIndex = tabBarSelectedIndex ?? 0
        tabBarSelectedIndex = nil
        addShadow()
        addNotificationCenter()
//        checkAndUpdateHomeDataWithLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !isAppViewWillAppearCalled{
            checkAndUpdateHomeDataWithLocation()
            isAppViewWillAppearCalled = true
        }
        NotificationCenter.default.addObserver(self, selector: #selector(userUpdatedLocation(_:)), name: Notification.Name(rawValue:Constants.NotificationName.userLocationChanged), object:nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue:Constants.NotificationName.userLocationChanged), object: nil)
    }
    
    private func addShadow() {
        if #available(iOS 13.0, *) {
            let appearance = tabBar.standardAppearance
            appearance.shadowImage = nil
            appearance.shadowColor = nil
            tabBar.standardAppearance = appearance
        } else {
            tabBar.shadowImage = UIImage()
            tabBar.backgroundImage = UIImage()
        }
        tabBar.layer.shadowOffset = CGSize(width: 0, height: 0)
        tabBar.layer.shadowRadius = 6
        tabBar.layer.shadowColor = UIColor.black.cgColor
        tabBar.layer.shadowOpacity = 0.2
    }
    
    private func addNotificationCenter() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateCartTabBadgeCount), name: .cartTabBadgeCount, object: nil)
    }
    
    @objc private func updateCartTabBadgeCount(_ notification: NSNotification) {
        tabBar.items![1].badgeValue = (Self.cartItemCount > 0 ? String(Self.cartItemCount) : nil)
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

extension DeliveryTabBarController{
    
    func checkAndUpdateHomeDataWithLocation(){
        //Checking whether location is enabled in Settings
        //If location is allowed by user then get the nearby outlet for that lat and long else show the cutom alert

        var userLatitudeValue:Double = 0.0
        var userLongitudeValue:Double = 0.0
        if(BBQLocationManager.shared.currentLocationManager.location != nil){
            userLatitudeValue = (BBQLocationManager.shared.currentLocationManager.location?.coordinate.latitude) ?? 0
            userLongitudeValue = (BBQLocationManager.shared.currentLocationManager.location?.coordinate.longitude) ?? 0
            tabDelegate?.nearbyOutletsFrom(userLatitude: userLatitudeValue, userLongitude: userLongitudeValue)
        }else if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
               case .restricted, .denied:
                    showLocationAlert()
                    break
            case .authorizedAlways, .authorizedWhenInUse, .authorized:
                   break
            default:
                BBQLocationManager.shared.updateUserLocationWithPermission()
                break
            }
       }
    }
    
    // MARK: - showLocationAlert called when user not given location permission
    func showLocationAlert() {
        PopupHandler.showTwoButtonsPopup(title: kUpdateLocationTitle,
                                         message: kUpdateLocationMessage,
                                         image: UIImage(named: "icon_location_orange"),
                                         titleImageFrame: CGRect(x: 0, y: -1, width: 16, height: 20),
                                         on: self,
                                         firstButtonTitle: kAlertAllow, firstAction: {
                                            BBQLocationManager.shared.updateUserLocationWithPermission()
                                            
                                            if(self.userDeniedLocation){
                                                self.showLocationStatusDeniedAlert()
                                            }
        },
                                         secondButtonTitle: kAlertDeny) {
                                            if BBQUserDefaults.sharedInstance.DeliveryBranchIdValue != ""{
                                                self.tabDelegate?.promotionsAndVouchersFrom(branchLatitude: 0.0, branchLongitude: 0.0, branchID: BBQUserDefaults.sharedInstance.DeliveryBranchIdValue)
                                            }else{
                                                self.tabDelegate?.promotionsAndVouchersFrom(branchLatitude: 0.0, branchLongitude: 0.0, branchID: "")
                                            }
                                            
        }
    }
    
    // MARK: - showLocationStatusDeniedAlert called when user denied the location permission
    func showLocationStatusDeniedAlert() {
        PopupHandler.showTwoButtonsPopup(title: kLocationDisabledTitle,
                                         message: kLocationDisabledMessage,
                                         on: self,
                                         firstButtonTitle: kCancelString,
                                         firstAction: {
                                            if BBQUserDefaults.sharedInstance.DeliveryBranchIdValue != ""{
                                                self.tabDelegate?.promotionsAndVouchersFrom(branchLatitude: 0.0, branchLongitude: 0.0, branchID: BBQUserDefaults.sharedInstance.DeliveryBranchIdValue)
                                            }else{
                                                self.tabDelegate?.promotionsAndVouchersFrom(branchLatitude: 0.0, branchLongitude: 0.0, branchID: "")

                                            }
        },
                                         secondButtonTitle: kOpenSettings) {
                                            if #available(iOS 10.0, *) {
                                                let settingsURL = URL(string: UIApplication.openSettingsURLString)!
                                                UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
                                            } else {
                                                if let url = NSURL(string:UIApplication.openSettingsURLString) {
                                                    UIApplication.shared.openURL(url as URL)
                                                }
                                            }
        }
    }
    
    //MARK : - userUpdatedLocation called when location changes
    @objc func userUpdatedLocation(_ notification: Notification) {
        if let dictionary = notification.userInfo as NSDictionary? {
            let latValue = dictionary["latitude"] as! Double
            let longValue = dictionary["longitude"] as! Double
            if latValue == 0.0, longValue == 0.0 {
                BBQUserDefaults.sharedInstance.userDeniedLocation = true

                userDeniedLocation = true
                if BBQUserDefaults.sharedInstance.DeliveryBranchIdValue != ""{
                    tabDelegate?.promotionsAndVouchersFrom(branchLatitude: latValue, branchLongitude: longValue, branchID: BBQUserDefaults.sharedInstance.DeliveryBranchIdValue)
                }else{
                    tabDelegate?.nearbyOutletsFrom(userLatitude: latValue, userLongitude: longValue)
                }
            }else{
                userDeniedLocation = false
                tabDelegate?.nearbyOutletsFrom(userLatitude: latValue, userLongitude: longValue)
            }
        }
    }
    private func getUpdatedPoints() {
        if BBQUserDefaults.sharedInstance.accessToken != "" {
            self.cartViewModel.getLoyaltyPointsCount { (isSuccess) in
                if isSuccess {
                    if let remainingPointsModel = self.cartViewModel.getPointCountModel, let remainingPoints = remainingPointsModel.remainingPoints {
                        BBQUserDefaults.sharedInstance.UserLoyaltyPoints = remainingPoints
                    }
                }
            }
        }
    }
}

protocol DeliveryTabBarControllerDelegate {
    func nearbyOutletsFrom(userLatitude: Double, userLongitude: Double)
    func promotionsAndVouchersFrom(branchLatitude: Double, branchLongitude: Double, branchID: String)
}
