//
//  Created by Dharani Sadasivam on 31/10/20
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
//  All rights reserved.
//  Last modified OrderHistoryResponse.swift
//

import Foundation
import ObjectMapper

final class OrderHistoryResponse: Mappable {
    var message = ""
    var messageType = ""
    var data: OrderList?
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        message <- map["message"]
        messageType <- map["message_type"]
        data <- map["data"]
    }
    
}

final class OrderList: Mappable {
    
    var orderList: [Order] = []
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        orderList <- map["lst_orders"]
    }
    
}


final class OrderDetailsResponse: Mappable {
    var message = ""
    var messageType = ""
    var data: Order?
    required init?(map: Map) {}
    func mapping(map: Map) {
        message <- map["message"]
        messageType <- map["message_type"]
        data <- map["data"]
    }
}



final class TaxCharge: Mappable {
    var value = "0"
    var label = ""
    required init?(map: Map) {}
    
    func mapping(map: Map) {
       
        value <- map["value"]
        label <- map["label"]
        if value == ""{
            value = "0"
        }
    }
}
