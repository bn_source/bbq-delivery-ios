//
//  Created by Shareef on 28/10/20
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
//  All rights reserved.
//  Last modified CartItemsResponse.swift
//

import ObjectMapper

final class CartItemsResponse: Mappable {
    
    var message = ""
    var message_type = ""
    var data: CartItems?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        message <- map["message"]
        message_type <- map["message_type"]
        data <- map["data"]
    }
}

final class CartItems: Mappable {
    
    var items = [CartItem]()
    var onGoingOrders = [Order]()
    var totalItemsCount = 0
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        items <- map["lst_items"]
        onGoingOrders <- map["lst_running_orders"]
        totalItemsCount <- map["total_item_count"]
    }
}

final class CartItem: Mappable {
    
    var id = ""
    var quantity = ""
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id <- map["item_id"]
        quantity <- map["quantity"]
    }
}


final class Order: Mappable {
    var orderId: String = ""
    var state: String = ""
    var stateInfo: String = ""
    var eta_mins : Int?
    var current_eta : Int?
    var status: String = ""
    var createdOn: String = ""
    var branchName: String = ""
    var amount: String = ""
    var items: [Item] = []
    var branchAddress = ""
    var deliveryAddress = ""
    var listOfTaxCharges: [TaxBreakUp] = []
    var branchLatitude = ""
    var branchLongitude = ""
    var branchPhoneNumber = ""
    var ordeOTP = ""
    var RiderPhoneNumber = ""
    var finalAmount: String = ""
    var transaction_type: Int?
    var foodIsRated : Bool = false
    var deliveryStatus: DeliveryStatus { return DeliveryStatus(rawValue: status) ?? .delivered }
    var otp: String?
    var is_schedule_delivery = false
    var schedule_delivery_time = ""
    var lst_delivery_schedule_interval: TimeInterval = 1800
    var backgroudColor: UIColor = .deliveryThemeColor
    var textColor: UIColor = .deliveryThemeTextColor
    var colorCode = ""
    var brand_name = ""
    var brand_logo = ""
    var current_status: String = ""
    
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        orderId <- map["order_id"]
        state <- map["state"]
        stateInfo <- map["state_info"]
        branchName <- map["branch_name"]
        status <- map["status"]
        createdOn <- map["created_on"]
        amount <- map["amount"]
        items <- map["items"]
        eta_mins <- map["eta_mins"]
        current_eta <- map["current_eta"]
        branchAddress <- map["branch_address"]
        deliveryAddress <- map["delivery_address"]
        listOfTaxCharges <- map["lst_tax_charges"]
        finalAmount <- map["final_break_up"]
        transaction_type <- map["transaction_type"]
        foodIsRated <- map["food_is_rated"]
        otp <- map["otp"]
        is_schedule_delivery <- map["is_schedule_delivery"]
        schedule_delivery_time <- map["schedule_delivery_time"]
        lst_delivery_schedule_interval <- map["lst_delivery_schedule_interval"]
        branchLatitude <- map["branch_lat"]
        branchLongitude <- map["branch_long"]
        branchPhoneNumber <- map["branch_phone_no"]
        RiderPhoneNumber <- map["rider_phone_no"]
        ordeOTP <- map["order_otp"]
        colorCode <- map["brand_color_code"]
        brand_name <- map["brand_name"]
        brand_logo <- map["brand_logo"]
        current_status <- map["current_status"]
        let codes = colorCode.components(separatedBy: "|")
        if codes.count > 1{
            if let first = codes.first{
                backgroudColor = first.hexStringToUIColor()
            }
            if let last = codes.last{
                textColor = last.hexStringToUIColor()
            }
            
        }
        if otp == nil || otp == "" {
            if let otpNumber = map.JSON["otp"] as? Int{
                otp = String(format: "%li", otpNumber)
            }
        }
    }
}
