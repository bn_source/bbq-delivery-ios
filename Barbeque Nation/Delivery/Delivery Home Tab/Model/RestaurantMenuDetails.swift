//
//  RestaurantMenuDetails.swift
//  BBQ
//
//  Created by Dharani Sadasivam on 20/09/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import ObjectMapper

final class RestaurantMenuResponse: Mappable {
    
    var message = ""
    var message_type = ""
    var data: RestaurantMenuDetails?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        message <- map["message"]
        message_type <- map["message_type"]
        data <- map["data"]
    }
}

final class RestaurantMenuDetails: Mappable {
    
    var servicable = false
    var takeaway = false
    var delivery = false
    var branch_name : String?
    var distance = ""
    var duration = ""
    var box = [Menu]()
    var image = [Menu]()
    var text = [Menu]()
    var limited_offer = [Item]()
    var backgroudColor: UIColor = .deliveryThemeColor
    var textColor: UIColor = .deliveryThemeTextColor
    var colorCode = ""
    var brand_name = ""
    var brand_logo = ""
    var offers = [Offers]()
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        servicable <- map["servicable"]
        takeaway <- map["takeaway"]
        delivery <- map["delivery"]
        distance <- map["distance"]
        duration <- map["duration"]
        box <- map["box"]
        image <- map["image"]
        text <- map["text"]
        limited_offer <- map["limited_offer"]
        branch_name <- map["branch_name"]
        colorCode <- map["brand_color_code"]
        brand_name <- map["brand_name"]
        brand_logo <- map["brand_logo"]
        offers <- map["offers"]
        let codes = colorCode.components(separatedBy: "|")
        if codes.count > 1{
            if let first = codes.first{
                backgroudColor = first.hexStringToUIColor()
            }
            if let last = codes.last{
                textColor = last.hexStringToUIColor()
            }
            
        }
    }
}

final class Menu: Mappable {
    
    var categoryName = ""
    var categorySeq: Int = 0
    var subCategory = [SubCategory]()
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        categoryName <- map["category_desc"]
        let sequence = String(format: "%@", map.JSON["category_seq"] as? CVarArg ?? "0")
        categorySeq = Int(sequence) ?? 0
        subCategory <- map["subcategory"]
    }
}

final class SubCategory: Mappable {
    
    var items = [Item]()
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        items <- map["items"]
    }
}

final class Offers: Mappable {
    
    var id = ""
    var name = ""
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id <- map["offer_id"]
        name <- map["offer_name"]
    }
    
}



final class Item: Mappable {
    
    var id = ""
    var name = ""
    var description = ""
    var imageUrl = ""
    var originalPrice = 0
    var itemPrice = 0
    var isAvailable = false
    var quantity = 0
    var foodType = FoodType.veg
    var foodCategory = FoodCategory.item
    var customisations = [Customisation]()
    var allowMultiple = false
    var isCustomizable = false
    var price: String { "\(getCurrency())\(itemPrice)" }
    var total: Int = 0
    var default_select: Bool = false
    var promotion_message: String = ""
    var cms_offer_id: String = ""
    var end_date: String = ""
    var isOfferAvailable : String?

    init() {}
    
    //Cover page
    init(foodCategory: FoodCategory) {
        self.foodCategory = foodCategory
    }
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id <- map["item_code"]
        name <- map["item_name"]
        description <- map["item_desc"]
        imageUrl <- map["item_image_path"]
        originalPrice <- map["original_price"]
        itemPrice <- map["item_price"]
        isAvailable = ((map["avilability"].currentValue as? String) ?? "") == "1"
        foodType <- map["food_type"]
        customisations <- map["options"]
        isCustomizable = ((map["customize"].currentValue as? String) ?? "") == "1"
        allowMultiple <- map["allow_muliple"]
        quantity <- map["quantity"]
        total <- map["total"]
        default_select <- map["default_select"]
        promotion_message <- map["promotion_message"]
        cms_offer_id <- map["cms_offer_id"]
        end_date <- map["end_date"]
        if default_select{
            quantity = 1
        }
    }
}

final class Customisation: Mappable {
    
    var title = ""
    var threshold: String = ""
    var items = [Item]()
    var optionDesc = ""
    var maxQuantityLimit = 0
    var selectedItems = [Item]()
    
    required init?(map: Map) {}
        
    func mapping(map: Map) {
        title <- map["option_title"]
        threshold <- map["threshold"]
        items <- map["option_items"]
        optionDesc <- map["option_desc"]
        maxQuantityLimit = Int(threshold) ?? 0
        updateDefaultSelected()
    }
    
    func updateDefaultSelected() {
        for item in items{
            if item.default_select, item.quantity >= 1{
                selectedItems.append(item)
            }
        }
    }
}


final class MenuSection: Hashable {
    
    var name = ""
    var type = FoodCategory.item
    var categorySeq: Int = 0
    var itemCount = 0
    var isSelected = false
    
    init(name: String, type: FoodCategory, itemCount: Int, isSelected: Bool = false, categorySeq: Int) {
        self.name = name
        self.type = type
        self.itemCount = itemCount
        self.isSelected = isSelected
        self.categorySeq = categorySeq
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(name)
        hasher.combine(type)
    }
    
    static func == (lhs: MenuSection, rhs: MenuSection) -> Bool {
        return (lhs.name == rhs.name && lhs.type == rhs.type)
    }
}


enum FoodType: String {
    case veg = "0"
    case nonVeg = "1"
}

enum FoodCategory {
    case comboBox
    case comboItem
    case item
    
    func addToCartItemEvent() {
        AnalyticsHelper.shared.triggerEvent(type: .D11)
        AnalyticsHelper.shared.triggerEvent(type: .Delivery_Takeaway_Added, properties: [.Catalog_Product_Item_Cart_Selection: DeliveryTabBarController.cartItemCount + 1])
        switch self {
        case .comboBox:
            AnalyticsHelper.shared.triggerEvent(type: .D06)
            break
        case .comboItem:
            AnalyticsHelper.shared.triggerEvent(type: .D07)
            break
        case .item:
            AnalyticsHelper.shared.triggerEvent(type: .D08)
            break
        }
    }
    
    func increaseItemEvent() {
        AnalyticsHelper.shared.triggerEvent(type: .D11A)
        switch self {
        case .comboBox:
            AnalyticsHelper.shared.triggerEvent(type: .D06A)
            break
        case .comboItem:
            AnalyticsHelper.shared.triggerEvent(type: .D07A)
            break
        case .item:
            AnalyticsHelper.shared.triggerEvent(type: .D08A)
            break
        }
    }
    
    func decreaseItemEvent() {
        AnalyticsHelper.shared.triggerEvent(type: .D11B)
        switch self {
        case .comboBox:
            AnalyticsHelper.shared.triggerEvent(type: .D06B)
            break
        case .comboItem:
            AnalyticsHelper.shared.triggerEvent(type: .D07B)
            break
        case .item:
            AnalyticsHelper.shared.triggerEvent(type: .D08B)
            break
        }
    }
}
