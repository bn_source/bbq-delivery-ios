//
//  AddOrRemoveItemRequest.swift
//  BBQ
//
//  Created by Shareef on 20/09/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import UIKit
import ObjectMapper

final class AddOrRemoveItemRequest: Mappable {
    
    var customerId = ""
    var branchId = ""
    var itemId = ""
    var quantity = ""
    var customisationList: [ItemRequest] = []
    var itemDetailId = 0
    
    init(item: Item, isAdded: Bool) {
        customerId = BBQUserDefaults.sharedInstance.customerId
        branchId = BBQUserDefaults.sharedInstance.DeliveryBranchIdValue
        itemId = item.id
        quantity = isAdded ? "1" : "-1"
        
        for customisation in item.customisations {
            for selectedItem in customisation.selectedItems {
                customisationList.append(ItemRequest(item: selectedItem))
            }
        }
    }
    
    init(item: ItemDetails) {
        customerId = BBQUserDefaults.sharedInstance.customerId
        branchId = BBQUserDefaults.sharedInstance.DeliveryBranchIdValue
        itemId = item.item_id
        quantity = item.quantity
        for customisation in item.option {
            let item = Item()
            item.id = customisation.item_id
            item.quantity = Int(customisation.quantity) ?? 0
            customisationList.append(ItemRequest(item: item))
        }
    }
    
    required init?(map: Map) {}
        
    func mapping(map: Map) {
        customerId <- map["customer_id"]
        branchId <- map["branch_id"]
        itemId <- map["item_id"]
        quantity <- map["quantity"]
        customisationList <- map["item_details"]
        itemDetailId <- map["item_detail_id"]
    }
}

final class ItemRequest: Mappable {
    
    var itemId = ""
    var quantity = ""
    
    init(item: Item) {
           itemId = item.id
           quantity = String(item.quantity)
       }
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        itemId <- map["item_id"]
        quantity <- map["quantity"]
    }
}


