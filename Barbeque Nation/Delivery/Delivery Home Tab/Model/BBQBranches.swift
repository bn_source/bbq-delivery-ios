//
//  BBQBranches.swift
//  BBQ
//
//  Created by Dharani Sadasivam on 21/09/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import Foundation
import ObjectMapper
import UIKit

struct BrandInfo {
    var id: String = ""
    var name: String = ""
    var logo: String = ""
    var backgroudColor: UIColor = .deliveryThemeColor
    var textColor: UIColor = .deliveryThemeTextColor
    var colorCode = ""
    
    init(id: String, name: String, logo: String, colorCode: String) {
        self.id = id
        self.name = name
        self.logo = logo
        self.colorCode = colorCode
        let codes = colorCode.components(separatedBy: "|")
        if codes.count > 1{
            if let first = codes.first{
                self.backgroudColor = first.hexStringToUIColor()
            }
            if let last = codes.last{
                self.textColor = last.hexStringToUIColor()
            }
            
        }
    }
    
    init(brand: BBQBrands) {
        self.id = brand.id
        self.name = brand.name
        self.logo = brand.logo
        self.colorCode = brand.colorCode
        let codes = colorCode.components(separatedBy: "|")
        if codes.count > 1{
            if let first = codes.first{
                self.backgroudColor = first.hexStringToUIColor()
            }
            if let last = codes.last{
                self.textColor = last.hexStringToUIColor()
            }
            
        }
    }

}

final class BBQBrands: Mappable{
    var id = ""
    var name = ""
    var logo = ""
    var default_brand = false
    var branches: [BBQBranches]?
    var backgroudColor: UIColor = .deliveryThemeColor
    var textColor: UIColor = .deliveryThemeTextColor
    var colorCode = ""
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        logo <- map["logo"]
        branches <- map["data"]
        colorCode <- map["color_code"]
        default_brand <- map["default"]
        let codes = colorCode.components(separatedBy: "|")
        if codes.count > 1{
            if let first = codes.first{
                backgroudColor = first.hexStringToUIColor()
            }
            if let last = codes.last{
                textColor = last.hexStringToUIColor()
            }
            
        }
    }
    
    func getAllBranches() -> [Branch]{
        branches?.indices.forEach{ index in
            branches?[index].branches = branches?[index].branches?.map{
                $0.branchFullName = "\($0.branchName), \(branches?[index].cityName ?? "")"
                return $0
            }
        }
        let filteredBranches = branches?.compactMap{$0.branches}.flatMap{$0} ?? [Branch]()
        return filteredBranches 
    }
    
}

final class BBQBranches: Mappable {
    
    var cityName = ""
    var cityCode = ""
    var weight = 0
    var branches: [Branch]? 
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        cityName <- map["city_name"]
        cityCode <- map["city_code"]
        weight <- map["weight"]
        branches <- map["branches"]
    }
}



final class Branch: Mappable {
    
    var branchId = ""
    var branchName = ""
    var latitude = ""
    var longitude = ""
    var branchFullName = ""
    var only_delivery = false
    var only_dining = false
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        branchId <- map["branch_id"]
        branchName <- map["branch_name"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
    }
}

final class BrandsData: Mappable {
    
    var nearBy: [Branch] = []
    var brands: [BBQBrands] = []
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        nearBy <- map["near_by"]
        brands <- map["brands"]
    }
    
    func filterBranch(branchId: String) -> (brand: BBQBrands?,  branches: BBQBranches?, branch: Branch?) {
        if branchId == ""{
            return (nil, nil, nil)
        }
        for brand in brands {
            for cities in brand.branches ?? [BBQBranches]() {
                for branch in cities.branches ?? [Branch]() {
                    if branch.branchId == branchId {
                        return (brand, cities, branch)
                    }
                }
            }
        }
        return (nil, nil, nil)
    }
    
    func getCityCount(city_code: String) -> Int{
        var count = 0
        for brand in brands {
            for city in brand.branches ?? [BBQBranches](){
                if city.cityCode == city_code{
                    count += 1
                    break
                }
            }
        }
        return count
    }
}

final class BranchesData: Mappable {
    
    var nearBy: [Branch] = []
    var data: [BBQBranches] = []
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        nearBy <- map["near_by"]
        data <- map["data"]
    }
    
    func filterBranch(branchId: String) -> Branch? {
        if branchId == ""{
            return nil
        }
        for cities in data {
            for branch in cities.branches ?? [Branch]() {
                if branch.branchId == branchId {
                    return branch
                }
            }
        }
        return nil
    }
}

final class LocalBranch: Mappable{
    var branchId = ""
    var branchName = ""
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    var branchFullName = ""
    var only_delivery = false
    var only_dining = false
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        branchId <- map["store_code"]
        branchName <- map["name"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        only_delivery = String(format:"%@", map.JSON["only_delivery"] as? CVarArg ?? "0") == "1" ? true : false
        only_dining = String(format:"%@", map.JSON["only_dining"] as? CVarArg ?? "0") == "1" ? true : false
    }
}

final class LocalBranchData: Mappable{
    var branches: [LocalBranch] = [LocalBranch]()
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        branches <- map["branches"]
    }
}

func findBranchBy(id: String) -> LocalBranch? {
    for branch in localSavedBranches{
        if branch.branchId == id{
            return branch
        }
    }
    return nil
}

