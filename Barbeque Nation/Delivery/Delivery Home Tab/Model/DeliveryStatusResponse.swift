/*//
 //  Created by Dharani Sadasivam on 09/11/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified DeliveryStatusResponse.swift
 //

import Foundation
import ObjectMapper

final class DeliveryStatusResponse: Mappable {
    var message = ""
    var messageType = ""
    var data: DeliveryResponse?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        message <- map["message"]
        messageType <- map["message_type"]
        data <- map["data"]
    }
    
}

final class DeliveryResponse: Mappable {
    var address: String?
    var branchContactNo = ""
    var listOfPayment: [TaxCharge] = []
    var listOfOrderStatus: [OrderDeliveryStatus] = []
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        address <- map["address"]
        branchContactNo <- map["branch_contact_no"]
        listOfPayment <- map["lst_payment"]
        listOfOrderStatus <- map["lst_orders_status"]
    }
}

final class OrderDeliveryStatus: Mappable {
    var label: String = ""
    var value: String = ""
    var status: Bool = false
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        label <- map["label"]
        value <- map["value"]
        status <- map["current_status"]
       
    }
}
*/
/*
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

struct DeliveryStatusResponse : Mappable {
    var message : String?
    var message_type : String?
    var data : DeliveryResponse?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        message <- map["message"]
        message_type <- map["message_type"]
        data <- map["data"]
    }

}


struct DeliveryResponse : Mappable {
    var address : String?
    var branch_contact_no : String?
    var lst_payment : [Lst_payment]?
    var Lst_orders_status :  [Lst_orders_status]?
    var transaction_type : Int?
    var eta_mins : Int?
    var current_eta : Int?
    var order_id : Int?
    var order_time : String?
    var branch_name : String?
    var branch_address : String?
    var pickup_otp: String?
    var branch_lat : String?
    var branch_long : String?
    var delivery_time : String?
    var current_status : String?
    var received : Received?
    var accepted : Accepted?
    var prepared : Prepared?
    var dispatched : Dispatched?
    var delivered : Delivered?
    var lst_items : [Lst_items]?
    var lst_bill_details : [TaxBreakUp]?
    var bill_total : Int?
    var discount_text : String?
    var reschedule_order: Bool = false
    var backgroudColor: UIColor = .deliveryThemeColor
    var textColor: UIColor = .deliveryThemeTextColor
    var colorCode = ""
    var brand_name = ""
    var brand_logo = ""

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        address <- map["address"]
        branch_contact_no <- map["branch_contact_no"]
        lst_payment <- map["lst_payment"]
        Lst_orders_status <- map["lst_orders_status"]
        transaction_type <- map["transaction_type"]
        eta_mins <- map["eta_mins"]
        current_eta <- map["current_eta"]
        order_id <- map["order_id"]
        order_time <- map["order_time"]
        branch_name <- map["branch_name"]
        branch_address <- map["branch_address"]
        pickup_otp  <- map["pickup_otp"]
        branch_lat <- map["branch_lat"]
        branch_long <- map["branch_long"]
        delivery_time <- map["delivery_time"]
        current_status <- map["current_status"]
        received <- map["received"]
        accepted <- map["accepted"]
        prepared <- map["prepared"]
        dispatched <- map["dispatched"]
        delivered <- map["delivered"]
        lst_items <- map["lst_items"]
        lst_bill_details <- map["lst_bill_details"]
        bill_total <- map["bill_total"]
        discount_text <- map["discount_text"]
        reschedule_order <- map["reschedule_order"]
        colorCode <- map["brand_color_code"]
        brand_name <- map["brand_name"]
        brand_logo <- map["brand_logo"]
        let codes = colorCode.components(separatedBy: "|")
        if codes.count > 1{
            if let first = codes.first{
                backgroudColor = first.hexStringToUIColor()
            }
            if let last = codes.last{
                textColor = last.hexStringToUIColor()
            }
            
        }
    }
    
    mutating func addTotalInBreakUp(title: String = "Total") {
        lst_bill_details?.append(TaxBreakUp(label: title, value: String(format: "%li", bill_total ?? 0)))
    }

}

struct Delivered : Mappable {
    var time : String?
    var add_info : String?
    var description : String?
    var rider_number : String?
    var current_title: String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        time <- map["time"]
        add_info <- map["add_info"]
        description <- map["description"]
        rider_number <- map["rider_number"]
        current_title <- map["current_title"]

    }

}
struct Dispatched : Mappable {
    var time : String?
    var add_info : String?
    var description : String?
    var rider_number : String?
    var current_title: String?


    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        time <- map["time"]
        add_info <- map["add_info"]
        description <- map["description"]
        rider_number <- map["rider_number"]
        current_title <- map["current_title"]

    }

}
struct Lst_payment : Mappable {
    var label : String?
    var value : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        label <- map["label"]
        value <- map["value"]
    }

}
struct Lst_items : Mappable {
    var item_name : String?
    var quantity : String?
    var food_type : String?
    var amount : Int?
    var total : String?


    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        item_name <- map["item_name"]
        quantity <- map["quantity"]
        food_type <- map["food_type"]
        amount <- map["Amount"]
        total <- map["total"]


    }

}
struct Accepted : Mappable {
    var time : String?
    var add_info : String?
    var description : String?
    var rider_number : String?
    var current_title: String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        time <- map["time"]
        add_info <- map["add_info"]
        description <- map["description"]
        rider_number <- map["rider_number"]
        current_title <- map["current_title"]

    }

}
struct Lst_bill_details : Mappable {
    var label : String?
    var value : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        label <- map["label"]
        value <- map["value"]
    }

}
struct Prepared : Mappable {
    var time : String?
    var add_info : String?
    var description : String?
    var rider_number : String?
    var current_title: String?
    var vaccination_link: String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        time <- map["time"]
        add_info <- map["add_info"]
        description <- map["description"]
        rider_number <- map["rider_number"]
        current_title <- map["current_title"]
        vaccination_link <- map["vaccination_link"]
    }

}
struct listOfOrderStatus : Mappable {
    var label : String?
    var value : String?
    var current_status : Bool?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        label <- map["label"]
        value <- map["value"]
        current_status <- map["current_status"]
    }

}
struct Received : Mappable {
    var time : String?
    var add_info : String?
    var description : String?
    var rider_number : String?
    var current_title: String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        time <- map["time"]
        add_info <- map["add_info"]
        description <- map["description"]
        rider_number <- map["rider_number"]
        current_title <- map["current_title"]

    }

}

struct Lst_orders_status : Mappable {
    var label : String?
    var value : String?
    var current_status : Bool?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        label <- map["label"]
        value <- map["value"]
        current_status <- map["current_status"]
    }

}
