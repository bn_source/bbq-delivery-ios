//
 //  Created by Mahmadsakir on 23/03/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified SafetyHygiene.swift
 //

import Foundation
import UIKit
import ObjectMapper

struct SafetyHygieneResponse : Mappable {
    var safety_and_hygiene_banner : Safety_and_hygiene_banner?
    var safety_and_hygiene_summary : Safety_and_hygiene_summary?
    var safety_and_hygiene_temperature_checks : Safety_and_hygiene_temperature_checks?
    var safety_and_hygiene_emergency_info : Safety_and_hygiene_emergency_info?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        safety_and_hygiene_banner <- map["safety_and_hygiene_banner"]
        safety_and_hygiene_summary <- map["safety_and_hygiene_summary"]
        safety_and_hygiene_temperature_checks <- map["safety_and_hygiene_temperature_checks"]
        safety_and_hygiene_emergency_info <- map["safety_and_hygiene_emergency_info"]
    }
}

struct Safety_and_hygiene_banner : Mappable {
    var title : String?
    var sub_title : String?
    var description : String?
    var image : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        title <- map["title"]
        sub_title <- map["sub_title"]
        description <- map["description"]
        image <- map["image"]
    }

}

struct Safety_and_hygiene_emergency_info : Mappable {
    var title : String?
    var description : String?
    var image : String?
    var list_points = [String]()

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        title <- map["title"]
        description <- map["description"]
        image <- map["image"]
        list_points <- map["list_points"]
    }
}

struct Safety_and_hygiene_summary : Mappable {
    var title : String?
    var list_points = [String]()

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        title <- map["title"]
        list_points <- map["list_points"]
    }
}

struct Safety_and_hygiene_temperature_checks : Mappable {
    var title : String?
    var description : String?
    var image : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        title <- map["title"]
        description <- map["description"]
        image <- map["image"]
    }


}
