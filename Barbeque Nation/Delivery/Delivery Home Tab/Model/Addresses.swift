//
//  Addresses.swift
//  BBQ
//
//  Created by Dharani Sadasivam on 22/09/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import Foundation
import ObjectMapper

final class AddressesData: Mappable {
    
    var message = ""
    var messageType = ""
    var data: AddressResponse?
    
    
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        message <- map["message"]
        messageType <- map["message_type"]
        data <- map["data"]
    }
}


final class AddressResponse: Mappable {
    var addresses: [Address] = []
    required init?(map: Map) {}
    
    func mapping(map: Map) {
       
        addresses <- map["address"]
    }
}

final class Address: Mappable {
    var customerId = ""
    var addressId = ""
    var tagType: TagType = .other
    var tagName = ""
    var flatNo = ""
    var address = ""
    var landMark = ""
    var pincode = ""
    var phoneNumber = ""
    var latitude = ""
    var longitude = ""
    var servicable: Bool = false
    var distance_meters: Int = 0
    
    enum TagType: String {
        case home = "home"
        case work = "work"
        case other = "other"
        
        func triggerButtonEvent() {
            switch self {
            case .home:
                AnalyticsHelper.shared.triggerEvent(type: .AE04)
                break
            case .work:
                AnalyticsHelper.shared.triggerEvent(type: .AE05)
                break
            case .other:
                AnalyticsHelper.shared.triggerEvent(type: .AE06)
                break
            }
        }
        
        func triggerSuccessEvent() {
            switch self {
            case .home:
                AnalyticsHelper.shared.triggerEvent(type: .AE04A)
                break
            case .work:
                AnalyticsHelper.shared.triggerEvent(type: .AE05A)
                break
            case .other:
                AnalyticsHelper.shared.triggerEvent(type: .AE06A)
                break
            }
        }
        
        func triggerFailureEvent() {
            switch self {
            case .home:
                AnalyticsHelper.shared.triggerEvent(type: .AE04B)
                break
            case .work:
                AnalyticsHelper.shared.triggerEvent(type: .AE05B)
                break
            case .other:
                AnalyticsHelper.shared.triggerEvent(type: .AE06B)
                break
            }
        }
    }
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        customerId <- map["customer_id"]
        addressId <- map["address_id"]
        flatNo <- map["flat_no"]
        address <- map["address"]
        landMark <- map["landmark"]
        pincode <- map["pincode"]
        phoneNumber <- map["phone_no"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        servicable <- map["servicable"]
        tagType = TagType(rawValue: String(format: "%@", map.JSON["tag_type"] as? CVarArg ?? "other").lowercased()) ?? .other
        tagName <- map["tag_name"]
        distance_meters <- map["distance_meters"]
    }
    
    func getTagIcon(color: String) -> (UIImage?, String) {
        if tagType == .home{
            return (UIImage(named: "icon_address_home_\(color)"), "Home")
        }else if tagType == .work{
            return(UIImage(named: "icon_address_work_\(color)"), "Work")
        }else{
            if tagName != ""{
                return(UIImage(named: "icon_address_other_\(color)"), tagName)
            }else{
                return(UIImage(named: "icon_address_other_\(color)"), "Other")
            }
        }
    }
}
