//
//  Promotions.swift
//  BBQ
//
//  Created by Shareef on 20/09/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import UIKit
import ObjectMapper

final class PromotionsResponse: Mappable {
    
    var data: [Promotion] = [Promotion]()
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        data <- map["data"]
    }
}

final class Promotion: Mappable {
    
    var imageUrl = ""
    var delivery_item_number = ""
    var promotion_title = ""
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        imageUrl <- map["mobile_promotion.promotion_image"]
        delivery_item_number <- map["delivery_item_number"]
        promotion_title <- map["mobile_promotion.promotion_title"]
    }
}
