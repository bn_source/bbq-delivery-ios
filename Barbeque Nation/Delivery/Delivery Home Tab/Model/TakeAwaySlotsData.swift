//
 //  Created by Mahmadsakir on 07/05/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified TakeAwaySlotsData.swift
 //

import Foundation
import ObjectMapper


struct TakeAwaySlotsData : Mappable {
    var message : String?
    var message_type : String?
    var data : [PickUpDateTime] = [PickUpDateTime]()

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        message <- map["message"]
        message_type <- map["message_type"]
        data <- map["data"]
        
        for pickupTime in data{
            pickupTime.addToTakeawaySlots()
        }
    }

}


