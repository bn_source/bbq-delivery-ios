//
//  AddressViewModel.swift
//  BBQ
//
//  Created by Dharani Sadasivam on 22/09/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import Foundation
import ObjectMapper
import GooglePlaces


final class AddressViewModel  {
    var addresses: [Address]?
    var addressDeliverTo = [Address]()
    var addressNotDeliverTo = [Address]()
    var googlePlaces: [GooglePlaceRes] = []
    private var placesClient: GMSPlacesClient = GMSPlacesClient.shared()
    
    func getSavedAddress(customerId: String, branchId: String, filterWithID: String?, completionHandler: @escaping() -> Void) {
            UIUtils.showLoader()
        BBQServiceManager.getInstance().getSavedAddress(params: ["customer_id": customerId, "branch_id": branchId]) { (error, response) in
                UIUtils.hideLoader()
            self.addresses = response?.data?.addresses
            self.addressDeliverTo = [Address]()
            self.addressNotDeliverTo = [Address]()
            if let addresses = self.addresses{
                for address in addresses{
                    if address.servicable{
                        if let filterWithID = filterWithID, address.addressId == filterWithID{
                            self.addressDeliverTo.insert(address, at: 0)
                        }else{
                            self.addressDeliverTo.append(address)
                        }
                    }else{
                        self.addressNotDeliverTo.append(address)
                    }
                }
            }
            completionHandler()
        }
    }
    
    
    func getSearchAddress(_ searchStr: String, completionHandler: @escaping() -> Void) {
        
    }
    
    func getPlaceLatLong(_ placeId: String, completionHandler: @escaping(_ place: Location?, _ error: Error?) -> Void) {
        
        placesClient.fetchPlace(fromPlaceID: placeId, placeFields: GMSPlaceField(rawValue: UInt(GMSPlaceField.coordinate.rawValue) | UInt(GMSPlaceField.addressComponents.rawValue)) , sessionToken: nil) { (place, error) in
            if let error = error {
                completionHandler(nil, error)
                return
            }
            if let place = place {
                var location = Location()
                               location.latitude = "\(place.coordinate.latitude)"
                               location.longitude = "\(place.coordinate.longitude)"
                               
                for addressComponent in (place.addressComponents)! {
                    print(addressComponent.types.description)
                    for type in (addressComponent.types){
                        
                        switch(type){
                            case "sublocality":
                                location.city = addressComponent.name
                                
                            case "locality":
                                location.state = addressComponent.name
                        default:
                            break
                        }

                    }
                }
                
            completionHandler(location, nil)
            }
        }
    }
}
  
