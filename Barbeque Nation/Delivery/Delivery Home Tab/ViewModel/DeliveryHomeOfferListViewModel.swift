//
 //  Created by Mahmadsakir on 26/05/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified DeliveryHomeOfferListViewModel.swift
 //

import Foundation
import ObjectMapper

final class DeliveryHomeOfferListViewModel {
    
    var categoryList = [MenuSection]()
    var menuDict = [MenuSection: [Item]]()
    var originalCategoryList = [MenuSection]()
    var originalMenuDict = [MenuSection: [Item]]()
    weak var tableView: UITableView?
    var isServicable: Bool = false
    var delegate: DeliveryHomeOfferListViewModelDelegate?
    var branchID: String? = "15"
    var cms_offer_id: String = ""
    var image_url: String = ""
    var totalItemPrice = DeliveryTabBarController.cartItemPrice


    private func refreshData(isLoadingFirstTime: Bool = true, indexPath: IndexPath?, isForceFully: Bool = false, isCameFromFilter: Bool = false) {
        if let indexPath = indexPath, tableView?.cellForRow(at: indexPath) != nil{
            tableView?.reloadRows(at: [indexPath], with: .none)
        }else if let items = self.tableView?.indexPathsForVisibleRows, items.count > 0{
            var isVisible = true
            for indexPath in items{
                if tableView?.cellForRow(at: indexPath) == nil{
                    isVisible = false
                    break
                }
            }
            if isVisible{
                self.tableView?.reloadRows(at: items, with: .none)
            }else{
                self.tableView?.reloadData()
            }
        }else{
            tableView?.reloadData()
        }
    }
    
    //MARK:- API Calls
    
    func getRestaurantMenus(isLoadingFirsttime: Bool) {
        delegate?.startLoading()
        BBQServiceManager.getInstance().getRestaurantMenus(params: ["date": Date().getFormattedDate(), "branch_id": BBQUserDefaults.sharedInstance.DeliveryBranchIdValue]) { (error, response) in
            if let response = response {
                self.processData(response)
            } else {
                UIUtils.showToast(message: "Loading...")
            }
            self.delegate?.stopLoading()
        }
    }
    
    
    
    
    func addItemToCart(item: Item, isRepeatedItem: Bool, indexPath: IndexPath?) {
        UIUtils.increaseCartTabBadgeCount(item.id)
        var params = AddOrRemoveItemRequest(item: item, isAdded: true).toJSON()
        if isRepeatedItem || !item.isCustomizable{
            params["item_details"] = []
        }
        BBQServiceManager.getInstance().addOrRemoveItemToCart(params: params) { (error, response) in
            if response == nil {
                item.quantity -= 1
                UIUtils.showToast(message: error?.message ?? UNEXPECTED_ERROR)
                self.refreshData(indexPath: indexPath)
                if item.quantity == 0 {
                    UIUtils.decreaseCartTabBadgeCount(item.id)
                }
              
            }else{
                item.isOfferAvailable = response?.data?.last_offer_display
                self.delegate?.showOfferView(textMessageToDisplay: item.isOfferAvailable)
                if item.quantity == 1{
                    item.foodCategory.addToCartItemEvent()
                }else{
                    item.foodCategory.increaseItemEvent()
                }
                self.totalItemPrice = self.totalItemPrice + item.originalPrice

                AnalyticsHelper.shared.triggerEvent(type: .delivery_add_to_cart)
                AnalyticsHelper.shared.triggerEvent(type: .added_to_cart_delivery)
                UIUtils.updateCartTabBadgeCountNew(count: DeliveryTabBarController.cartItemCount + 1)
              
            }
        }
    }
    
    func removeItemFromCart(item: Item, indexPath: IndexPath?) {
        if item.quantity == 0 {
            UIUtils.decreaseCartTabBadgeCount(item.id)
        }
        var params = AddOrRemoveItemRequest(item: item, isAdded: false).toJSON()
        params["item_details"] = []
        BBQServiceManager.getInstance().addOrRemoveItemToCart(params: params) { (error, response) in
            if response == nil {
                item.quantity += 1
                UIUtils.showToast(message: error?.message ?? UNEXPECTED_ERROR)
                self.refreshData(indexPath: indexPath)
                UIUtils.increaseCartTabBadgeCount(item.id)
            }else{
                UIUtils.updateCartTabBadgeCountNew(count: DeliveryTabBarController.cartItemCount - 1 )
                item.foodCategory.decreaseItemEvent()
            }
            self.totalItemPrice = self.totalItemPrice - item.originalPrice


        }
    }
    
    func removeAllSelectedItems(_ item: Item) {
        for customisation in item.customisations {
            customisation.selectedItems.removeAll()
        }
    }
    
    //MARK:- Data Processing
    private func processData(_ restaurantMenuResponse: RestaurantMenuResponse) {
        categoryList.removeAll()
        menuDict.removeAll()
        if let restaurantMenuDetails = restaurantMenuResponse.data {
            isServicable = restaurantMenuDetails.servicable
            processMenuCategories(menus: restaurantMenuDetails.box, foodCategory: .comboBox)
            processMenuCategories(menus: restaurantMenuDetails.image, foodCategory: .comboItem)
            processMenuCategories(menus: restaurantMenuDetails.text, foodCategory: .item)
            originalCategoryList = categoryList
            originalMenuDict = menuDict
        }
        refreshData(indexPath: nil)
    }
    
    private func processMenuCategories(menus: [Menu], foodCategory: FoodCategory) {
        for menu in menus {
            let section = MenuSection(name: menu.categoryName, type: foodCategory, itemCount: 0, categorySeq: menu.categorySeq)
            var count = 0
            for subCategory in menu.subCategory {
                for item in subCategory.items {
                    item.foodCategory = foodCategory
                    if menuDict[section] == nil {
                        menuDict[section] = [item]
                    } else {
                        menuDict[section]?.append(item)
                    }
                }
                count += subCategory.items.count
            }
            section.itemCount = count
            categoryList.append(section)
        }
        categoryList.sort { section1, section2 in
            return section1.categorySeq < section2.categorySeq
        }
    }
    
}
protocol DeliveryHomeOfferListViewModelDelegate {
    func startLoading()
    func stopLoading()
    func showOfferView(textMessageToDisplay : String?)

}
