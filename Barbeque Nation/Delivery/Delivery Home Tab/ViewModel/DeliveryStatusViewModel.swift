//
 //  Created by Dharani Sadasivam on 09/11/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified DeliveryStatusViewModel.swift
 //

import Foundation
import UIKit
final class DeliveryStatusViewModel {
    
    var orderDeliveryStatus: DeliveryStatusResponse?
    var pickSlots: TakeAwaySlotsData?
    var lastAPICall: Date?
    
    //MARK:- API Calls
    func getDeliveryStatus(orderId: String, isRequiredToShowLoader:Bool , completionHandler: @escaping() -> Void) {
        if isRequiredToShowLoader {
            UIUtils.showLoader()
        }
        BBQServiceManager.getInstance().getDeliveryStatusList(params: ["customer_id": BBQUserDefaults.sharedInstance.customerId, "order_id": orderId]) { (error, response) in
            UIUtils.hideLoader()
            if let response = response {
                
                if response.message_type == "success" {
                    
                    AnalyticsHelper.shared.triggerEvent(type: .OS04A)
                }else{
                    AnalyticsHelper.shared.triggerEvent(type: .OS04B)
                    
                }
                self.orderDeliveryStatus = response
                completionHandler()
            }
        }
    }
    
    func getPickSlotsAvail(orderId: String, completionHandler: @escaping() -> Void) {
        if pickSlots?.data != nil, let lastAPICall = lastAPICall, lastAPICall.addingTimeInterval(300) > Date() {
            completionHandler()
            return
        }else{
            lastAPICall = Date()
            pickSlots = nil
        }
        
        UIUtils.showLoader()
        BBQServiceManager.getInstance().getTakeAwaySlots(params: ["customer_id": BBQUserDefaults.sharedInstance.customerId, "order_id": Int64(orderId) as Any]) { (error, response) in
            UIUtils.hideLoader()
            if let response = response {
                
                if response.message_type == "success" {
                    self.pickSlots = response
                }else{
                    if let message = response.message{
                        ToastHandler.showToastWithMessage(message: message)
                    }else{
                        ToastHandler.showToastWithMessage(message: "Please try again")
                    }
                }
                completionHandler()
            }
        }
    }
    
    func updatePickSlot(orderId: String, slot: String, completionHandler: @escaping(Bool) -> Void) {
        UIUtils.showLoader()
        BBQServiceManager.getInstance().updateTakeAwaySlot(params: ["customer_id": BBQUserDefaults.sharedInstance.customerId, "order_id": Int64(orderId) as Any, "delivery_time": slot.convertDateFormater(toFormat: "yyyy/MM/dd hh:mm a", fromFormat: "dd-MMM-yyyy HH:mm") as Any]) { (error, response) in
            UIUtils.hideLoader()
            if let response = response {
                if response.message_type == "success" {
                    completionHandler(true)
                }else{
                    if let message = response.message{
                        ToastHandler.showToastWithMessage(message: message)
                    }else{
                        ToastHandler.showToastWithMessage(message: "Please try again")
                    }
                    completionHandler(false)
                }
                
            }
        }
    }
}
