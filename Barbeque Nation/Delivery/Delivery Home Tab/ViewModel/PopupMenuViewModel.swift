//
//  PopupMenuViewModel.swift
//  BBQ
//
//  Created by Dharani Sadasivam on 12/09/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import Foundation
import UIKit

class MenuViewModel {
    var item: Item?
    var updateItem = Item()
    
    init(_ item: Item) {
        self.item = item
    }
    
     func getMenuTitleAt(_ section: Int) -> String {
        return self.item?.customisations[section].title ?? ""
    }
    
    func getMenuDescAt(_ section: Int) -> String {
        return self.item?.customisations[section].optionDesc ?? ""
    }
    
    func getMenuItemsAt(_ section: Int) -> [Item] {
        return self.item?.customisations[section].items ?? []
    }
    
    func getCustomisations() -> [Customisation] {
        return self.item?.customisations ?? []
    }
    
    func getCustomisationsAt(_ section: Int) -> Customisation {
        return getCustomisations()[section]
    }
    
    func getFoodTypeIcon() -> UIImage  {
        return self.item?.foodType == FoodType.veg ? #imageLiteral(resourceName: "Group 846") : #imageLiteral(resourceName: "Group 845")
    }
    
    func getTotalThreshold() -> Int {
        let customisations = getCustomisations()
        let totalCount = customisations.map{$0.maxQuantityLimit}.reduce(0, +)
        return totalCount
    }
    
    func getAllSelectedItems() -> [Item] {
       return getCustomisations().flatMap{$0.selectedItems}
    }
    
    func getAllSelectedItemsWithOutAddOn() -> [Item] {
        return getCustomisations().filter{$0.maxQuantityLimit != 0}.flatMap{$0.selectedItems}
    }
    
    
    func removeAllSelectedItems() {
        for customisation in getCustomisations() {
            customisation.selectedItems.removeAll()
            customisation.updateDefaultSelected()
        }
    }
    
    func makeDefaultSelectItem() {
        let customisation = getCustomisations()
        for i in 0..<customisation.count {
            for j in 0..<customisation[i].items.count{
                if customisation[i].items[j].default_select{
                    customisation[i].items[j].quantity = 1
                }
            }
            item?.customisations[i].selectedItems = customisation[i].items
        }
        item?.customisations = customisation
    }
    
    func canAddItem(_ section: Int) -> Bool {
        let customisation = getCustomisationsAt(section)
        return (customisation.selectedItems.count < customisation.maxQuantityLimit) || (customisation.maxQuantityLimit == 0)
    }
    
    func addSelectedItem(_ section: Int, item: Item)  {
        getCustomisationsAt(section).selectedItems.append(item)
    }
    
    func removeSelectedItem(_ section: Int, item: Item) {
        let index = getCustomisationsAt(section).selectedItems.firstIndex(where: {$0.id == item.id})
        guard let selectedIndex  = index else { return }
        getCustomisationsAt(section).selectedItems.remove(at: selectedIndex)
        print(getCustomisationsAt(section).selectedItems.description)
    }
    
    func addOrRemoveCartItem(item: Item) {
        let params: [String: Any] = [:]
        BBQServiceManager.getInstance().addOrRemoveItemToCart(params: params) { (error, response) in
            if response == nil {
                item.quantity = 0
                UIUtils.showToast(message: error?.message ?? UNEXPECTED_ERROR)
//                self.collectionView?.reloadData()
            }
        }
    }
}
