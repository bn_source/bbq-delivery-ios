//
 //  Created by Mahmadsakir on 23/03/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified SafetyHygieneViewModel.swift
 //

import Foundation
import UIKit
import ObjectMapper

final class SafetyHygieneViewModel {
    
    var safetyHygieneResponse: SafetyHygieneResponse?
    
    //MARK:- API Calls
    func getSafetyHygieneDetails(completionHandler: @escaping() -> Void) {
        UIUtils.showLoader()
        BBQServiceManager.getInstance().getSafetyHygieneDetails { (error, response) in
            UIUtils.hideLoader()
            if let response = response {
                self.safetyHygieneResponse = response
                completionHandler()
            }
        }
    }
}
