//
//  DeliveryHomeViewModel.swift
//  BBQ
//
//  Created by Shareef on 05/09/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import ObjectMapper
import ObjectiveC
import Darwin

final class DeliveryHomeViewModel {
    
    var categoryList = [MenuSection]()
    var menuDict = [MenuSection: [Item]]()
    var originalCategoryList = [MenuSection]()
    var originalMenuDict = [MenuSection: [Item]]()
    var promotionsList = [Promotion]()
    var offers = [Offers]()
    weak var collectionView: UICollectionView?
    weak var statusCollectionView: UICollectionView?
    weak var pageControl : UIPageControl?
    weak var tableView: UITableView?
    weak var coverPageCollectionViewClass: CoverPageCollectionViewClass?
    weak var smallMenuView: UIView?
    var selectedBranch: Branch?
    var currentLocation: Location?
    var brands = [BBQBrands]()
    var selectedBrand: BrandInfo?
    var isTakeAway: Bool = false
    var isServicable: Bool = false
    var isDelivery: Bool = false
    var isNonVegSelected = true
    var openOrders = [Order]()
    var delegate: DeliveryHomeViewModelDelegate?
    var branchID: String?
    var isLimitedOfferAvailable = false
    var headerHeight: CGFloat = 383+50
    var userLocation: Location?
    var isMultipleBrands = false
    var isMultipleBrandsDisplayed = false
    var offerView : ItemOfferAppliedView?
    var priceForItemInCart = DeliveryTabBarController.cartItemPrice
    private func refreshData(isLoadingFirstTime: Bool = true, indexPath: IndexPath?, isForceFully: Bool = false, isCameFromFilter: Bool = false) {
        if isLimitedOfferAvailable{
            headerHeight = 574+50
        }else{
            headerHeight = 383+50
        }
        statusCollectionView?.isHidden = !(openOrders.count > 0)
        pageControl?.numberOfPages = openOrders.count 
        if (openOrders.count > 0) {
                statusCollectionView?.reloadData()
        }

        smallMenuView?.isHidden = !(categoryList.count > 1)
        if !isNonVegSelected && !isCameFromFilter{
            filterMenuItems(isShowVeg: true)
            return
        }
        if let branchID = branchID, branchID != BBQUserDefaults.sharedInstance.DeliveryBranchIdValue{
            collectionView?.setContentOffset(CGPoint(x: 0, y: -headerHeight), animated: true)
            collectionView?.reloadData()
        }else if isLoadingFirstTime{
            if isForceFully{
                collectionView?.reloadData()
            }else{
                if let indexPath = indexPath, categoryList.count > indexPath.section, (menuDict[categoryList[indexPath.section]]?.count ?? 0) > indexPath.row, collectionView?.cellForItem(at: indexPath) != nil{
                    collectionView?.reloadItems(at: [indexPath])
                }else if let items = self.collectionView?.indexPathsForVisibleItems, items.count > 0{
                    var isVisible = true
                    for indexPath in items{
                        if categoryList.count < indexPath.section || (menuDict[categoryList[indexPath.section]]?.count ?? 0) < indexPath.row{
                            isVisible = false
                            break
                        }
                        
                        if collectionView?.cellForItem(at: indexPath) == nil{
                           isVisible = false
                           break
                       }
                    }
                    if isVisible{
                        self.collectionView?.reloadData() //.reloadItems(at: items)
                    }else{
                        collectionView?.setContentOffset(CGPoint(x: 0, y: -headerHeight), animated: true)
                        collectionView?.reloadData()
                    }
                }else{
                    collectionView?.reloadData()
                }
            }
            delegate?.scrollToCollectionView(isForceFully: true)
        }
        branchID = BBQUserDefaults.sharedInstance.DeliveryBranchIdValue
        tableView?.reloadData()
        statusCollectionView?.reloadData()
        delegate?.setCollectionHeaderSize(isLimitedOfferAvailable: isLimitedOfferAvailable)
        
        
    }
    
    //MARK:- API Calls
//    func getAllNearByRestaurant(_ location: Location, completionHandler: @escaping() -> Void) {
//        delegate?.startLoading(model: self, isRequired: true)
//        collectionView?.setContentOffset(CGPoint(x: 0, y: -headerHeight), animated: false)
//        coverPageCollectionViewClass?.showShimmering()
//        BBQServiceManager.getInstance().getAllNearByRestaurant(params: ["lat": location.latitude, "long": location.longitude,"type": "delivery"]) { (error, response) in
//            if let response = response {
//                self.branches = response.data
//                if let branch = response.nearBy.first {
//                    self.selectedBranch = branch
//                    BBQUserDefaults.sharedInstance.branchIdValue = branch.branchId
//                    BBQUserDefaults.sharedInstance.CurrentSelectedLocation = branch.branchName
//                    only_delivery = branch.only_delivery
//                    only_dining = branch.only_dining
//                    self.getRestaurantMenus(isLoadingFirsttime: true)
//                } else {
//                    if let branch = response.filterBranch(branchId: BBQUserDefaults.sharedInstance.branchIdValue){
//                        BBQUserDefaults.sharedInstance.CurrentSelectedLocation = branch.branchName
//                        BBQUserDefaults.sharedInstance.branchIdValue = branch.branchId
//                        only_delivery = branch.only_delivery
//                        only_dining = branch.only_dining
//                        self.selectedBranch = branch
//                    }
//                    self.selectedBranch?.branchId = BBQUserDefaults.sharedInstance.branchIdValue
//                    self.selectedBranch?.branchName = BBQUserDefaults.sharedInstance.CurrentSelectedLocation
//                    self.getRestaurantMenus(isLoadingFirsttime: true)
//                }
//            } else {
//                UIUtils.showToast(message: LOADING_ERROR)
//            }
//            completionHandler()
//        }
//    }
    func getAllBrandsInfo() -> [BrandInfo] {
        var brandsInfo = [BrandInfo]()
        for brand in brands {
            brandsInfo.append(BrandInfo(brand: brand))
        }
        return brandsInfo
    }
    
    func setBrandToSelect(byID brand_id: String) {
        guard brand_id != selectedBrand?.id else { return }
        let selectedBrand = getAllBrandsInfo().first(where: { $0.id == brand_id })
        self.selectedBrand = selectedBrand
    }
    
    func getAllNearByRestaurantV1(_ location: Location, isClickedFromHome: Bool = false, completionHandler: @escaping() -> Void) {
        delegate?.startLoading(model: self, isRequired: true)
        collectionView?.setContentOffset(CGPoint(x: 0, y: -headerHeight), animated: false)
        coverPageCollectionViewClass?.showShimmering()
        self.isMultipleBrands = false
        BBQServiceManager.getInstance().getAllNearByRestaurantV1(params: ["lat": location.latitude, "long": location.longitude,"type": "delivery", "brand_id": tempSelectedBrandForDelivery ?? selectedBrand?.id ?? ""], isClickedFromHome: isClickedFromHome) { (error, response) in
            if let response = response {
                self.brands = response.brands
                if let branch = response.nearBy.first {
                    self.selectedBranch = branch
                    BBQUserDefaults.sharedInstance.DeliveryBranchIdValue = branch.branchId
                    BBQUserDefaults.sharedInstance.DeliveryCurrentSelectedLocation = branch.branchName
                    only_delivery = branch.only_delivery
                    only_dining = branch.only_dining
                    let data = response.filterBranch(branchId: branch.branchId)
                    if let city_code = data.branches?.cityCode{
                        self.setCityBrandCount(city_count: response.getCityCount(city_code: city_code))
                    }
                    
                    if let brand = data.brand{
                        self.selectedBrand = BrandInfo(brand: brand)
                    }
                    self.getRestaurantMenus(isLoadingFirsttime: true)
                } else {
                    let data = response.filterBranch(branchId: BBQUserDefaults.sharedInstance.DeliveryBranchIdValue)
                    if let city_code = data.branches?.cityCode{
                        self.setCityBrandCount(city_count: response.getCityCount(city_code: city_code))
                    }
                    if let selectedBrand = self.selectedBrand, let brand = data.brand, selectedBrand.id == brand.id, let branch = data.branch{
                        self.makeBrandSelected(brand: brand, branch: branch)
                    }else if self.selectedBrand == nil, let branch = data.branch, let brand = data.brand{
                        self.makeBrandSelected(brand: brand, branch: branch)
                    }else if let brand_id = self.selectedBrand?.id, let brand = self.getBrandsBy(id: brand_id), let branch = brand.branches?.first?.branches?.first{
                        self.makeBrandSelected(brand: brand, branch: branch)
                    }else{
                        if let brand = self.brands.first, let branch = brand.branches?.first?.branches?.first{
                            self.makeBrandSelected(brand: brand, branch: branch)
                        }
                    }
                    self.delegate?.noNearbyBranchFound(model: self)
                    self.getRestaurantMenus(isLoadingFirsttime: true)
                }
            } else {
                UIUtils.showToast(message: LOADING_ERROR)
            }
            completionHandler()
        }
    }
    
    private func setCityBrandCount(city_count: Int){
        self.isMultipleBrands = city_count > 1 ? true : false
    }
    
    private func makeBrandSelected(brand: BBQBrands, branch: Branch){
        BBQUserDefaults.sharedInstance.DeliveryCurrentSelectedLocation = branch.branchName
        BBQUserDefaults.sharedInstance.DeliveryBranchIdValue = branch.branchId
        only_delivery = branch.only_delivery
        only_dining = branch.only_dining
        self.selectedBranch = branch
        self.selectedBrand = BrandInfo(brand: brand)
        UIColor.deliveryThemeColor = brand.backgroudColor
        UIColor.deliveryThemeTextColor = brand.textColor
    }
    
    private func getBrandsBy(id: String) -> BBQBrands?{
        for brand in brands{
            if brand.id == id{
                return brand
            }
        }
        return nil
    }
    
    func getRestaurantMenus(isLoadingFirsttime: Bool, isForceAPICall: Bool = false) {
        let currentLoc = self.currentLocation
        if currentLocation != nil{
            
        }
        if BBQUserDefaults.sharedInstance.DeliveryBranchIdValue == ""{
            return
        }
            
        if let branchID = branchID, branchID != BBQUserDefaults.sharedInstance.DeliveryBranchIdValue{
            delegate?.startLoading(model: self, isRequired: false)
            coverPageCollectionViewClass?.showShimmering()
            collectionView?.setContentOffset(CGPoint(x: 0, y: -headerHeight), animated: false)
        }else if let branchID = branchID, branchID == BBQUserDefaults.sharedInstance.DeliveryBranchIdValue, (isServicable || isTakeAway), !isForceAPICall{
             coverPageCollectionViewClass?.stopShimmering()
            self.getPreviousSelectedItems()
            return
        }
        BBQServiceManager.getInstance().getRestaurantMenus(params: ["date": Date().getFormattedDate(), "branch_id": BBQUserDefaults.sharedInstance.DeliveryBranchIdValue, "latitude": currentLoc?.latitude as Any, "longitude": currentLoc?.longitude as Any]) { (error, response) in
            if let response = response {
                if let backgroudColor = response.data?.backgroudColor, let textColor = response.data?.textColor{
                    UIColor.deliveryThemeColor = backgroudColor
                    UIColor.deliveryThemeTextColor = textColor
                }
                self.coverPageCollectionViewClass?.limitedOffers = response.data?.limited_offer ?? [Item]()
                self.offers = response.data?.offers ?? [Offers]()
                self.coverPageCollectionViewClass?.pageControl.numberOfPages = self.coverPageCollectionViewClass?.limitedOffers.count ?? 0
                if response.data?.limited_offer.count ?? 0 > 0{
                    self.isLimitedOfferAvailable = true
                }else{
                    self.isLimitedOfferAvailable = false
                }
                self.processData(response)
                self.getPreviousSelectedItems()
                self.getDeliveryPromotions(isLoadingFirsttime: isLoadingFirsttime)
                if !self.isMultipleBrandsDisplayed && self.isMultipleBrands{
                    self.isMultipleBrandsDisplayed = true
                    self.delegate?.showIntroMultiBrand()
                }
            } else {
                UIUtils.showToast(message: LOADING_ERROR)
            }
        }
    }
    
    private func getDeliveryPromotions(isLoadingFirsttime: Bool = true) {
        BBQServiceManager.getInstance().getDeliveryPromotions { [self] (error, response) in
            if let response = response {
                self.promotionsList = [Promotion]()
                if response.data.count > 0{
                    self.promotionsList = response.data
                }
                self.coverPageCollectionViewClass?.updateView(imageUrls: self.promotionsList, branch: self.selectedBranch, offers: self.offers, isDelivery: self.isDelivery, isTakeAway: self.isTakeAway, isNonVegSelected: self.isNonVegSelected, isLimitedOfferAvailable: self.isLimitedOfferAvailable, isServicable: self.isServicable, selectedBrand: selectedBrand?.name ?? "BBQN")
            } else {
                UIUtils.showToast(message: LOADING_ERROR)
            }
        }
    }
    
    private func getPreviousSelectedItems() {
        guard !BBQUserDefaults.sharedInstance.accessToken.isEmpty && (isServicable || isTakeAway) else {
            self.refreshData(isLoadingFirstTime: true, indexPath: nil)
            self.delegate?.stopLoading(model: self)
            return }
        BBQServiceManager.getInstance().getPreviousSelectedItems(customerId: BBQUserDefaults.sharedInstance.customerId, branchId: BBQUserDefaults.sharedInstance.DeliveryBranchIdValue) { (error, response) in
            if let cartItems = response {
                self.updateQuantityOnItems(cartItems: cartItems)

            } else {
                UIUtils.showToast(message: LOADING_ERROR)
            }
            self.refreshData(isLoadingFirstTime: true, indexPath: nil)
            self.delegate?.stopLoading(model: self)
            self.delegate?.updateUIOnOpenOrders(isAvailable: self.openOrders.count > 0 ? true : false)

        }
    }
    
    func getLoyaltyPointsCount(completion: @escaping ( _ result: Bool)->()) {
        
        // Blocking API call for guest user
        if BBQUserDefaults.sharedInstance.isGuestUser {
            completion(false)
        }
        
        let endPoint = BBQCartRouter.loyaltyPointsCount
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQCartManager.parsePointsCountData(data: responseData, error: responseError)
            if modelValue != nil {
                DispatchQueue.main.async {
                    completion(true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(false)
                }
            }
        }
    }
    
    private func updateQtyLimitedOffer(selectedItem: Item) {
        if let offeredItem = coverPageCollectionViewClass?.limitedOffers{
            if let matchedItem = offeredItem.first(where: {$0.id == selectedItem.id}) {
                matchedItem.quantity = Int(selectedItem.quantity)
                self.coverPageCollectionViewClass?.collectionView.reloadData()
            }
        }
    }
    
    func updateQtyFromHeader(selectedItem: Item){
        for (_, items) in menuDict {
            if let matchedItem = items.first(where: {$0.id == selectedItem.id}) {
                matchedItem.quantity = Int(selectedItem.quantity)
            }
        }
        self.refreshData(indexPath: nil)
    }
    
    func addItemToCart(item: Item, isRepeatedItem: Bool, indexPath: IndexPath?) {
        UIUtils.increaseCartTabBadgeCount(item.id)
        var params = AddOrRemoveItemRequest(item: item, isAdded: true).toJSON()
        if isRepeatedItem || !item.isCustomizable{
            params["item_details"] = []
        }
        self.updateQtyLimitedOffer(selectedItem: item)
        BBQServiceManager.getInstance().addOrRemoveItemToCart(params: params) { [self] (error, response) in
            if response == nil {
                item.quantity -= 1
                UIUtils.showToast(message: error?.message ?? LOADING_ERROR)
                self.refreshData(indexPath: indexPath)
                if item.quantity == 0 {
                    UIUtils.decreaseCartTabBadgeCount(item.id)
                }
                self.updateQtyLimitedOffer(selectedItem: item)
                self.updateQtyFromHeader(selectedItem: item)

            }else{
                
                item.isOfferAvailable = response?.data?.last_offer_display
                self.delegate?.showOfferView(textMessageToDisplay: item.isOfferAvailable)
                if item.quantity == 1{
                    item.foodCategory.addToCartItemEvent()
                    priceForItemInCart = priceForItemInCart + Int(item.originalPrice)

                }else{
                    item.foodCategory.increaseItemEvent()
                    priceForItemInCart = priceForItemInCart + Int(item.originalPrice)

                }
                
                
                AnalyticsHelper.shared.triggerEvent(type: .delivery_add_to_cart)
                AnalyticsHelper.shared.triggerEvent(type: .added_to_cart_delivery)
                UIUtils.updateCartTabBadgeCountNew(count: DeliveryTabBarController.cartItemCount + 1)

            }
          

        }
    }
    
    func removeItemFromCart(item: Item, indexPath: IndexPath?) {
        if item.quantity == 0 {
            UIUtils.decreaseCartTabBadgeCount(item.id)
            DeliveryTabBarController.cartItemPrice = 0
        }
        self.updateQtyLimitedOffer(selectedItem: item)
        var params = AddOrRemoveItemRequest(item: item, isAdded: false).toJSON()
        params["item_details"] = []
        BBQServiceManager.getInstance().addOrRemoveItemToCart(params: params) { [self] (error, response) in
            if response == nil {
                item.quantity += 1
                UIUtils.showToast(message: error?.message ?? LOADING_ERROR)
                self.refreshData(indexPath: indexPath)
                UIUtils.increaseCartTabBadgeCount(item.id)
//                priceForItemInCart = priceForItemInCart - Int(item.originalPrice)
                self.updateQtyLimitedOffer(selectedItem: item)
                self.updateQtyFromHeader(selectedItem: item)
            }else{
                priceForItemInCart = priceForItemInCart - Int(item.originalPrice)
                UIUtils.updateCartTabBadgeCountNew(count: DeliveryTabBarController.cartItemCount - 1)
                item.foodCategory.decreaseItemEvent()
            }
    

        }
    }
    
    //MARK:- Data Processing
    private func processData(_ restaurantMenuResponse: RestaurantMenuResponse) {
        categoryList.removeAll()
        menuDict.removeAll()
        if let restaurantMenuDetails = restaurantMenuResponse.data {
            isServicable = restaurantMenuDetails.servicable
            isTakeAway = restaurantMenuDetails.takeaway
            isDelivery = restaurantMenuDetails.delivery
            selectedBranch?.branchName = restaurantMenuDetails.branch_name ?? BBQUserDefaults.sharedInstance.DeliveryCurrentSelectedLocation
            processMenuCategories(menus: restaurantMenuDetails.image, foodCategory: .comboItem)
            processMenuCategories(menus: restaurantMenuDetails.box, foodCategory: .comboBox)
            processMenuCategories(menus: restaurantMenuDetails.text, foodCategory: .item)
            originalCategoryList = categoryList
            originalMenuDict = menuDict
            if selectedBranch == nil{
                selectedBranch = Branch.init(JSON: ["branch_id": BBQUserDefaults.sharedInstance.DeliveryBranchIdValue, "branch_name": restaurantMenuDetails.branch_name ?? BBQUserDefaults.sharedInstance.DeliveryCurrentSelectedLocation])
            }
            self.coverPageCollectionViewClass?.selectedBrandInfo = self.selectedBrand
            self.coverPageCollectionViewClass?.updateView(branch: self.selectedBranch ?? selectedBranch, isDelivery: self.isDelivery, isTakeAway: self.isTakeAway, isNonVegSelected: self.isNonVegSelected)
        }
    }
    
    private func processMenuCategories(menus: [Menu], foodCategory: FoodCategory) {
        for menu in menus {
            let section = MenuSection(name: menu.categoryName, type: foodCategory, itemCount: 0, categorySeq: menu.categorySeq)
            var count = 0
            for subCategory in menu.subCategory {
                for item in subCategory.items {
                    item.foodCategory = foodCategory
                    if menuDict[section] == nil {
                        menuDict[section] = [item]
                    } else {
                        menuDict[section]?.append(item)
                    }
                }
                count += subCategory.items.count
            }
            section.itemCount = count
            categoryList.append(section)
        }
        categoryList.sort { section1, section2 in
            return section1.categorySeq < section2.categorySeq
        }
    }
    
    private func getCartItemCount(cart: GetCartResponseModel) {
        UIUtils.updateCartTabBadgeCountNew(count: cart.data?.items.count ?? 0)
    }
    
    private func updateQuantityOnItems(cartItems: CartItems) {
        self.openOrders = cartItems.onGoingOrders
        
        for (_, items) in menuDict {
            items.forEach { item in
                item.quantity = 0
            }
        }
        
        coverPageCollectionViewClass?.limitedOffers.forEach { item in
            item.quantity = 0
        }
        
        for selectedItem in cartItems.items {
            for (_, items) in menuDict {
                if let matchedItem = items.first(where: {$0.id == selectedItem.id}) {
                    matchedItem.quantity = Int(selectedItem.quantity) ?? 0
                   // priceArray.append(matchedItem.originalPrice * matchedItem.quantity)
                }
            }
            if let offeredItem = coverPageCollectionViewClass?.limitedOffers{
                if let matchedItem = offeredItem.first(where: {$0.id == selectedItem.id}) {
                    matchedItem.quantity = Int(selectedItem.quantity) ?? 0
//                    priceArray.append(matchedItem.itemPrice * matchedItem.quantity)

                }
            }
        }
        UIUtils.updateCartTabBadgeCountNew(count: DeliveryTabBarController.cartItemCount )

        let itemIds = cartItems.items.map({$0.id})
//        priceForItemInCart = priceArray.reduce(0, +)
        UIUtils.updateCartTabBadgeCountNew(count: DeliveryTabBarController.cartItemCount)
        UIUtils.updateCartTabBadgeCount(itemIds, count: cartItems.totalItemsCount)
        self.coverPageCollectionViewClass?.collectionView.reloadData()
    }
    
    func filterMenuItems(isShowVeg: Bool) {
        if isShowVeg {
            for (key, _) in menuDict {
                menuDict[key]?.removeAll(where: {$0.foodType == .nonVeg})
                if menuDict[key]?.count == 0 {
                    categoryList.removeAll(where: {$0.name == key.name})
                }
            }
        } else {
            categoryList = originalCategoryList
            menuDict = originalMenuDict
        }
        refreshData(indexPath: nil, isForceFully: true, isCameFromFilter: true)
    }
    
    func getAllBranches() -> [Branch] {
        if let selectedBrandId = selectedBrand?.id{
            for brand in brands {
                if selectedBrandId == brand.id{
                    return brand.getAllBranches()
                }
            }
        }
        return [Branch]()
        
    }
    
    func removeAllSelectedItems(_ item: Item) {
        for customisation in item.customisations {
            customisation.selectedItems.removeAll()
        }
    }
}
protocol DeliveryHomeViewModelDelegate {
    func startLoading(model: DeliveryHomeViewModel, isRequired: Bool)
    func scrollToCollectionView(isForceFully: Bool)
    func stopLoading(model: DeliveryHomeViewModel)
    func updateUIOnOpenOrders(isAvailable: Bool)
    func setCollectionHeaderSize(isLimitedOfferAvailable: Bool)
    func noNearbyBranchFound(model: DeliveryHomeViewModel)
    func showIntroMultiBrand()
    func showOfferView(textMessageToDisplay : String?)
}
