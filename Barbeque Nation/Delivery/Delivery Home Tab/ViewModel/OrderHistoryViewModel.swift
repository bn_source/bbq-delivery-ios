//
 //  Created by Dharani Sadasivam on 31/10/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified OrderHistoryViewModel.swift
 //

import Foundation
import UIKit

final class OrderHistoryViewModel {
    var orderList: [Order]?
    
    //MARK:- API Calls
    func getAllOrders(completionHandler: @escaping() -> Void) {
        UIUtils.showLoader()
        BBQServiceManager.getInstance().getOrderHistory(params: ["customer_id": BBQUserDefaults.sharedInstance.customerId]) { (error, response) in
            UIUtils.hideLoader()
            if let response = response {
                print(response)
                self.orderList = response.data?.orderList
                completionHandler()
            }
        }
    }
    
    
    func getOrderDetails(_ orderId: String, completionHandler: @escaping(_ orderDetail: Order) -> Void) {
        UIUtils.showLoader()
        BBQServiceManager.getInstance().getOrderDetails(params: ["customer_id": BBQUserDefaults.sharedInstance.customerId, "order_id": orderId]) { (error, response) in
            UIUtils.hideLoader()
            if let orderDetail = response?.data {
                print(orderDetail)
                
                completionHandler(orderDetail)
            }
        }
    }
    
    func getDeliveryStatusImg(_ deliveryStatus: DeliveryStatus) -> UIImage {
        switch(deliveryStatus) {
        case .acknowledged:
            return #imageLiteral(resourceName: "BBQInfoIcon")
        case .pending:
            return #imageLiteral(resourceName: "Inprogress.png")
        case .delivered:
            return #imageLiteral(resourceName: "icon_success_green")
        case .cancelled:
            return #imageLiteral(resourceName: "Cancelled")
        }
    }
    
    func getTextAndImageColorDependingOnStatus(_ deliveryStatus: DeliveryStatus) -> (textColor:UIColor , BoorderColor:UIColor){
        switch(deliveryStatus) {
        case .acknowledged:
            return (UIColor.black, UIColor.clear)
        case .pending:
            return (UIColor.hexStringToUIColor(hex: "F58840"), UIColor.hexStringToUIColor(hex: "FBD0A5"))
        case .delivered:
            return (UIColor.hexStringToUIColor(hex: "2B9C4C"), UIColor.hexStringToUIColor(hex: "BFE1C9"))
        case .cancelled:
            return  (UIColor.hexStringToUIColor(hex: "E01818"), UIColor.hexStringToUIColor(hex: "FF9D90"))
            
        }
    }
}

enum DeliveryStatus: String {
    case delivered = "Delivered"
    case acknowledged = "Acknowledged"
    case cancelled = "Cancelled"
    case pending = "In-Progress"
}
