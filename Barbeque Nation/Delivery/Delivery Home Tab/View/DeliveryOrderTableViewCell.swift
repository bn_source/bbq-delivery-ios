//
 //  Created by Dharani Sadasivam on 09/11/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified DeliveryOrderTableViewCell.swift
 //

import UIKit

protocol DeliveryOrderTableViewCellDelegate: AnyObject {
    func actionOnHome()
    func actionOnCall()
}

class DeliveryOrderTableViewCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var deliveredBulletHCons: NSLayoutConstraint!
    @IBOutlet weak var dispatchBulletHCons: NSLayoutConstraint!
    @IBOutlet weak var preparingBulletHCons: NSLayoutConstraint!
    @IBOutlet weak var orderBulletHCons: NSLayoutConstraint!
    @IBOutlet weak var callShadowView: UIView!
    @IBOutlet weak var deliveredDescHCons: NSLayoutConstraint!
    @IBOutlet weak var dispatchedDescHCons: NSLayoutConstraint!
    @IBOutlet weak var preparingDescHCons: NSLayoutConstraint!
    @IBOutlet weak var orderedDescHCons: NSLayoutConstraint!
    @IBOutlet weak var callBtn: UIButton!
    @IBOutlet weak var homeBtn: UIButton!
    @IBOutlet weak var deliveredDesc: UILabel!
    @IBOutlet weak var deliveredTitle: UILabel!
    @IBOutlet weak var dispatchedDesc: UILabel!
    @IBOutlet weak var dispatchedTitle: UILabel!
    @IBOutlet weak var preparingDesc: UILabel!
    @IBOutlet weak var preparingTitle: UILabel!
    @IBOutlet weak var orderDesc: UILabel!
    @IBOutlet weak var orderTitle: UILabel!
    @IBOutlet weak var deliveredBulletView: UIView!
    @IBOutlet weak var dispatchedBulletView: UIView!
    @IBOutlet weak var preparingBulletView: UIView!
    @IBOutlet weak var orderedBulletView: UIView!
    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var headerView: UIView!
    var deliveryStatusLis: DeliveryStatusResponse?
    weak var deliveryDelegate: DeliveryOrderTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
        
    }

    override func layoutSubviews() {
        super.layoutSubviews()
//        orderedDescHCons.constant = (deliveryStatusLis?.data?.listOfOrderStatus.filter{$0.label == "Ordered"}.first?.status ?? false) ? 60 : 30
//        preparingDescHCons.constant = (deliveryStatusLis?.data?.listOfOrderStatus.filter{$0.label == "Preparing"}.first?.status ?? false) ? 60 : 30
//        dispatchedDescHCons.constant = (deliveryStatusLis?.data?.listOfOrderStatus.filter{$0.label == "Dispatched"}.first?.status ?? false) ? 60 : 30
//        deliveredDescHCons.constant = (deliveryStatusLis?.data?.listOfOrderStatus.filter{$0.label == "Delivered"}.first?.status ?? false) ? 60 : 30
//
//        orderBulletHCons.constant = (deliveryStatusLis?.data?.listOfOrderStatus.filter{$0.label == "Ordered"}.first?.status ?? false) ? 30 : 20
//
//        preparingBulletHCons.constant = (deliveryStatusLis?.data?.listOfOrderStatus.filter{$0.label == "Preparing"}.first?.status ?? false) ? 30 : 20
//
//        dispatchBulletHCons.constant = (deliveryStatusLis?.data?.listOfOrderStatus.filter{$0.label == "Dispatched"}.first?.status ?? false) ? 30 : 20
//
//        deliveredBulletHCons.constant = (deliveryStatusLis?.data?.listOfOrderStatus.filter{$0.label == "Delivered"}.first?.status ?? false) ? 30 : 20
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
       contentView.setShadow()
       mainView.setCornerRadius(12)
    }
    
    func setupView() {
        orderedBulletView.setCornerRadius(orderedBulletView.frame.height/2)
        preparingBulletView.setCornerRadius(preparingBulletView.frame.height/2)
        dispatchedBulletView.setCornerRadius(dispatchedBulletView.frame.height/2)
        deliveredBulletView.setCornerRadius(deliveredBulletView.frame.height/2)
        callShadowView.setShadow()
        homeBtn.setCornerRadius(homeBtn.frame.height/2)
        callBtn.setCornerRadius(callBtn.frame.height/2)
        
        let callImage = #imageLiteral(resourceName: "call").withRenderingMode(.alwaysTemplate)
        callBtn.tintColor = .theme
        callBtn.setImage(callImage, for: .normal)
    }
    
    
    func fillUI(_ deliveryStatusLis: DeliveryStatusResponse ) {
        self.deliveryStatusLis = deliveryStatusLis
//        orderDesc.text = (deliveryStatusLis.data?.listOfOrderStatus.filter{$0.label == "Ordered"}.first?.status ?? false) ? deliveryStatusLis.data?.listOfOrderStatus.filter{$0.label == "Ordered"}.first?.value : ""
//        preparingDesc.text = (deliveryStatusLis.data?.listOfOrderStatus.filter{$0.label == "Preparing"}.first?.status ?? false) ? (deliveryStatusLis.data?.listOfOrderStatus.filter{$0.label == "Preparing"}.first?.value) : ""
//        
//        dispatchedDesc.text = (deliveryStatusLis.data?.listOfOrderStatus.filter{$0.label == "Dispatched"}.first?.status ?? false) ? (deliveryStatusLis.data?.listOfOrderStatus.filter{$0.label == "Dispatched"}.first?.value) : ""
//        
//        deliveredDesc.text = (deliveryStatusLis.data?.listOfOrderStatus.filter{$0.label == "Delivered"}.first?.status ?? false) ? (deliveryStatusLis.data?.listOfOrderStatus.filter{$0.label == "Delivered"}.first?.value) : ""
//        
//        orderTitle.font = (deliveryStatusLis.data?.listOfOrderStatus.filter{$0.label == "Ordered"}.first?.status ?? false) ? orderTitle.font.withSize(18) : orderTitle.font
//        orderTitle.textColor = (deliveryStatusLis.data?.listOfOrderStatus.filter{$0.label == "Ordered"}.first?.status ?? false) ? .theme : .text
//        
//        preparingDesc.text = (deliveryStatusLis.data?.listOfOrderStatus.filter{$0.label == "Preparing"}.first?.status ?? false) ? (deliveryStatusLis.data?.listOfOrderStatus.filter{$0.label == "Preparing"}.first?.value) : ""
//        preparingTitle.font = (deliveryStatusLis.data?.listOfOrderStatus.filter{$0.label == "Preparing"}.first?.status ?? false) ? preparingTitle.font.withSize(18) : preparingTitle.font
//        preparingTitle.textColor = (deliveryStatusLis.data?.listOfOrderStatus.filter{$0.label == "Preparing"}.first?.status ?? false) ? .theme : .text
//        
//        dispatchedDesc.text = (deliveryStatusLis.data?.listOfOrderStatus.filter{$0.label == "Dispatched"}.first?.status ?? false) ? (deliveryStatusLis.data?.listOfOrderStatus.filter{$0.label == "Dispatched"}.first?.value) : ""
//        dispatchedTitle.font = (deliveryStatusLis.data?.listOfOrderStatus.filter{$0.label == "Dispatched"}.first?.status ?? false) ? dispatchedTitle.font.withSize(18) : dispatchedTitle.font
//        dispatchedTitle.textColor = (deliveryStatusLis.data?.listOfOrderStatus.filter{$0.label == "Dispatched"}.first?.status ?? false) ? .theme : .text
//        
//        deliveredDesc.text = (deliveryStatusLis.data?.listOfOrderStatus.filter{$0.label == "Delivered"}.first?.status ?? false) ? (deliveryStatusLis.data?.listOfOrderStatus.filter{$0.label == "Delivered"}.first?.value) : ""
//        deliveredTitle.font = (deliveryStatusLis.data?.listOfOrderStatus.filter{$0.label == "Delivered"}.first?.status ?? false) ? deliveredTitle.font.withSize(18) : deliveredTitle.font
//        deliveredTitle.textColor = (deliveryStatusLis.data?.listOfOrderStatus.filter{$0.label == "Delivered"}.first?.status ?? false) ? .theme : .text
//        
//        
//        orderedBulletView.backgroundColor = (deliveryStatusLis.data?.listOfOrderStatus.filter{$0.label == "Ordered"}.first?.status ?? false) ? .theme : .text
//        preparingBulletView.backgroundColor = (deliveryStatusLis.data?.listOfOrderStatus.filter{$0.label == "Preparing"}.first?.status ?? false) ? .theme : .text
//        dispatchedBulletView.backgroundColor = (deliveryStatusLis.data?.listOfOrderStatus.filter{$0.label == "Dispatched"}.first?.status ?? false) ? .theme : .text
//        deliveredBulletView.backgroundColor = (deliveryStatusLis.data?.listOfOrderStatus.filter{$0.label == "Delivered"}.first?.status ?? false) ? .theme : .text
        setNeedsLayout()
        layoutSubviews()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func actionOnHomeBtn(_ sender: Any) {
        self.deliveryDelegate?.actionOnHome()
    }
    @IBAction func actionOnCallBtn(_ sender: Any) {
        self.deliveryDelegate?.actionOnCall()
    }
    
}
