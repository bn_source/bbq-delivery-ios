//
 //  Created by Mahmadsakir on 25/05/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified LimitedOfferCell.swift
 //

import UIKit

class LimitedOfferCell: UICollectionViewCell {
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var lblItemDesc: UILabel!
    @IBOutlet weak var lblOldPrice: UILabel!
    @IBOutlet weak var lblNewPrice: UILabel!
    @IBOutlet weak var viewForQuantity: UIView!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var lblCustomization: UILabel!
    @IBOutlet weak var viewForContainer: UIView!
    
    var item: Item!
    var indexPath: IndexPath!
    var delegate: LimitedOfferCellDelegate?

    
    func setCellData(item: Item, indexPath: IndexPath, isServicable: Bool) {
        self.item = item
        self.indexPath = indexPath
        updateUI()
        lblItemName.text = item.name
        lblItemDesc.text = item.description
        imgBackground.setImage(url: item.imageUrl, placeholderImage: nil, isForPromotions: false)
        lblNewPrice.text = item.price
        //updateQuantity(count: item.quantity)
        btnAdd.backgroundColor = (isServicable && item.isAvailable) ? .theme : .lightGray
        btnAdd.isUserInteractionEnabled = (isServicable && item.isAvailable)
        viewForQuantity.isUserInteractionEnabled = (isServicable && item.isAvailable)
        lblCustomization.isHidden = !item.isCustomizable
        updateQuantity()
    }

    private func updateUI() {
        imgBackground.setCornerRadius(10)
        viewForContainer.setCornerRadius(10)
        btnAdd.setCornerRadius(btnAdd.frame.height *  0.45)
        btnAdd.setShadow()
        viewForQuantity.setCornerRadius(viewForQuantity.frame.height * 0.45)
        viewForQuantity.setShadow()
        lblQuantity.font = UIFont.appThemeExtraBoldWith(size: 14.0)
        updateOfferData()
    }
    
    private func updateOfferData(){
        if item.originalPrice != item.itemPrice{
            lblOldPrice.isHidden = false
            let attrString = NSAttributedString(string: String(format: "%@%li", getCurrency(), item.originalPrice), attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue])
            lblOldPrice.attributedText = attrString
        }else{
            lblOldPrice.isHidden = true
        }
    }
    
    func updateQuantity() {
        btnAdd.isHidden = item.quantity > 0
        viewForQuantity.isHidden = item.quantity == 0
        lblQuantity.text = String(item.quantity)
    }
    
    @IBAction func onClickBtnAdd(_ sender: Any) {
        delegate?.actionOnAddItemButton(item: item, indexPath: nil)
    }
    @IBAction func onClickBtnPlus(_ sender: Any) {
        delegate?.actionOnAddItemButton(item: item, indexPath: nil)
    }
    @IBAction func onClickBtnMinus(_ sender: Any) {
        item.quantity -= 1
        updateQuantity()
        delegate?.actionOnRemoveItemButton(item: item, indexPath: nil)
    }
}

protocol LimitedOfferCellDelegate: AnyObject {
    func actionOnAddItemButton(item: Item, indexPath: IndexPath?)
    func actionOnRemoveItemButton(item: Item, indexPath: IndexPath?)
    func actionOnImageTapped(item: Item, indexPath: IndexPath?, isCameFromSearch: Bool)
}
