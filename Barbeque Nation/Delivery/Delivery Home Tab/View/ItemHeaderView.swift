//
//  ItemHeaderView.swift
//  BBQ
//
//  Created by Shareef on 05/09/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import UIKit

class ItemHeaderView: UICollectionReusableView {
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var lblItems: UILabel!
    
    
    
    static func getCellIdentifier() -> String {
        return "ItemHeaderView"
    }
    
    func updateView(section: MenuSection, count: Int) {
        lblItems.setShadow()
        headerLabel.text = section.name
        headerLabel.textColor = UIColor.black
        lblItems.applyThemeOnText()
        lblItems.text = "\(count) Items"
    }
}
