//
//  ResLocTableViewCell.swift
//  BBQ
//
//  Created by Dharani Sadasivam on 11/08/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import UIKit

class ResLocTableViewCell: UITableViewCell {

    @IBOutlet weak var searchLbl: UILabel!
    @IBOutlet weak var imgBrandLogo: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
