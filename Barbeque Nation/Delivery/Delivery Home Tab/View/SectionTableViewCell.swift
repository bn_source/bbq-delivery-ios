//
//  SectionTableViewCell.swift
//  BBQ
//
//  Created by Shareef on 27/09/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import UIKit

class SectionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var selectedImageView: UIImageView!
    @IBOutlet weak var lblItemCount: UILabel!
    
    static func getCellIdentifier() -> String {
        return "SectionTableViewCell"
    }
    
    func updateCell(section: MenuSection, count: Int) {
        titleLabel.text = section.name.uppercased()
        lblItemCount.font = UIFont.appThemeMediumWith(size: 12)
        if section.isSelected{
            titleLabel.font = UIFont.appThemeMediumWith(size: 14)
        }else{
            titleLabel.font = UIFont.appThemeRegularWith(size: 14)
        }
        //selectedImageView.isHidden = !section.isSelected
        lblItemCount.text = String(format: "%li Items", count)
        lblItemCount.applyThemeOnText()
    }
}
