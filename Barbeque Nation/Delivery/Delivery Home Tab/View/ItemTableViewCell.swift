//
 //  Created by Dharani Sadasivam on 02/11/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified ItemTableViewCell.swift
 //

import UIKit

class ItemTableViewCell: UITableViewCell {

    @IBOutlet weak var itemAmountLbl: UILabel!
    @IBOutlet weak var itemNmLbl: UILabel!
    @IBOutlet weak var itemTypeImgView: UIImageView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var lblItemCount: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblItemCount.setCornerRadius(5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setupItem(_ item: Item) {
        itemAmountLbl.text = "\(getCurrency())\(item.total)"
        itemNmLbl.text = "\(item.name)" // x \(item.quantity)"
        itemTypeImgView.image = item.foodType == FoodType.nonVeg ? #imageLiteral(resourceName: "nonvegIcon") : #imageLiteral(resourceName: "veg")
        lblItemCount.text = "\(item.quantity)"
    }
    override func layoutSubviews() {
        super.layoutSubviews()
  
       // contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
        //contentView.setShadow()UIColor.hexStringToUIColor(hex: "18191F")
        mainView.setCornerRadius(10)
        mainView.setShadow(color: UIColor.hexStringToUIColor(hex: "55606D1A") , opacity: 0.2, offSet: .zero, radius: 7, scale: true)
        mainView.layer.masksToBounds = true
       // itemTypeImgView.setCornerRadius(12)
    }
}
