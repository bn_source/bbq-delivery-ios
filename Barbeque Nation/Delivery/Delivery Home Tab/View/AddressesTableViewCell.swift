//
//  AddressesTableViewCell.swift
//  BBQ
//
//  Created by Dharani Sadasivam on 17/08/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import UIKit

class AddressesTableViewCell: UITableViewCell {

    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var imgAddressTag: UIImageView!
    @IBOutlet weak var lblTag: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCellData(address: Address){
        addressLbl.text = address.address
        if address.tagType == .home{
            lblTag.text = "Home"
            imgAddressTag.image = UIImage(named: "icon_address_home_black")
        }else if address.tagType == .work{
            lblTag.text = "Work"
            imgAddressTag.image = UIImage(named: "icon_address_work_black")
        }else{
            imgAddressTag.image = UIImage(named: "icon_address_other_black")
            if address.tagName != ""{
                lblTag.text = address.tagName
            }else{
                lblTag.text = "Other"
            }
        }
    }

}

class GoogleAddressTableViewCell: UITableViewCell {
    @IBOutlet weak var addressLbl: UILabel!
    
    override func awakeFromNib() {
           super.awakeFromNib()
           // Initialization code
       }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
           super.setSelected(selected, animated: animated)

           // Configure the view for the selected state
       }
}
