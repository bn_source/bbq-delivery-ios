//
 //  Created by Dharani Sadasivam on 31/10/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified OrderHistoryTableViewCell.swift
 //

import UIKit

protocol OrderHistoryTableViewCellDelegate: AnyObject {
    func tableViewCellSelect(_ orderId: String, _ createdOn: String)
    func clickedOnTrackOrder(_ orderId: String)
    func clickedOnRateOrder(_ orderId: String , selectedIndex: Int)
    func clickedOnReOrder(_ orderId: String)


}
class OrderHistoryTableViewCell: UITableViewCell {

   // @IBOutlet weak var reOrderBtn: UIButton!  //will be used in future
    @IBOutlet weak var deliveryStatusLbl: UILabel!
    @IBOutlet weak var deliveryStatusImgView: UIImageView!
    @IBOutlet weak var totalAmntLbl: UILabel!
   // @IBOutlet weak var itemsTableView: UITableView!
    @IBOutlet weak var ticketIDLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
   // @IBOutlet weak var orderId: UILabel!
    @IBOutlet weak var branchName: UILabel!
    @IBOutlet weak var itemListLbl: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var btnTrackOrder: UIButton!
    @IBOutlet weak var btnRateYourOrder: UIButton!
   // @IBOutlet  var heightForButtonStack: NSLayoutConstraint!
    @IBOutlet weak var buttonsStackView: UIStackView!
   // @IBOutlet  var cellBottomSpaceToButtonStack: NSLayoutConstraint!
    @IBOutlet weak var viewForScheduleOrder: UIView!
    @IBOutlet weak var lblScheduleOrder: UILabel!
    
    @IBOutlet weak var lblPickUpOrDelivery: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!
    var order: Order?
    weak var orderHistoryCellDelegate: OrderHistoryTableViewCellDelegate?
    @IBOutlet weak var stackViewDelivery: UIStackView!
    
    @IBOutlet weak var stackViewCancelled: UIStackView!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var SlefPickUPOrDeliveryStackView: UIStackView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()

     //   reOrderBtn.setCornerRadius(reOrderBtn.frame.height/2)
        btnTrackOrder.setCornerRadius(btnTrackOrder.frame.height/2)
        
        btnRateYourOrder.setCornerRadius(btnRateYourOrder.frame.height/2)
        btnRateYourOrder.layer.borderColor = UIColor.hexStringToUIColor(hex: "BFE1C9").cgColor
        btnRateYourOrder.setTitleColor(UIColor.hexStringToUIColor(hex:"2B9C4C"), for: .normal)
        btnRateYourOrder.layer.borderWidth = 1.5
        btnRateYourOrder.backgroundColor = UIColor.white
        
        
        btnTrackOrder.layer.borderColor = UIColor(named: "ThemeColor")?.cgColor
      //  reOrderBtn.layer.borderWidth = 1.0
        btnTrackOrder.layer.borderWidth = 1.0
        viewForScheduleOrder.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0)
        //    reOrderBtn.layer.borderColor = UIColor(named: "ThemeColor")?.cgColor

       // logoImageView.setCornerRadius(3.0)
    }
    
    
    func setButtonVisibility (reorderActive: Bool, rateorderActive: Bool, trackorderActive: Bool){
        
        //initially all hidden
      //  reOrderBtn.layer.borderColor = UIColor(named: "ThemeColor")?.cgColor
      //  btnRateYourOrder.layer.borderColor = UIColor.hexStringToUIColor(hex: "BFE1C9").cgColor
     //   btnRateYourOrder.setTitleColor(.white, for: .normal)

        btnRateYourOrder.layer.borderColor = UIColor.hexStringToUIColor(hex: "BFE1C9").cgColor
        btnRateYourOrder.setTitleColor(UIColor.hexStringToUIColor(hex:"2B9C4C"), for: .normal)
        btnRateYourOrder.layer.borderWidth = 1.5
        btnRateYourOrder.backgroundColor = UIColor.clear
        
        btnTrackOrder.layer.borderColor = UIColor(named: "ThemeColor")?.cgColor
      //  reOrderBtn.layer.borderWidth = 0.0
   
        btnTrackOrder.layer.borderWidth = 0.0
        btnTrackOrder.setTitleColor(.white, for: .normal)
      //  reOrderBtn.setTitleColor(.white, for: .normal)
        btnRateYourOrder.setImage(nil, for: .normal)
        btnRateYourOrder.isEnabled = false
        btnTrackOrder.isEnabled = false
       // reOrderBtn.isEnabled = false
       
        if trackorderActive == true {
            
            //means order is not yet completed
            //we can not rate food
         //   reOrderBtn.layer.borderColor = UIColor(named: "ThemeColor")?.cgColor
            btnRateYourOrder.layer.borderColor = UIColor.hexStringToUIColor(hex: "10A3FF").cgColor
            btnTrackOrder.layer.borderColor = UIColor.hexStringToUIColor(hex: "#FBD0A5").cgColor
          //  reOrderBtn.layer.borderWidth = 1.0
            btnRateYourOrder.layer.borderWidth = 0.0
            btnTrackOrder.layer.borderWidth = 1.5
            btnTrackOrder.setTitleColor( UIColor.hexStringToUIColor(hex: "F04B24"), for: .normal)
            btnRateYourOrder.setTitleColor(.white, for: .normal)
          //  reOrderBtn.setTitleColor( UIColor(named: "ThemeColor"), for: .normal)
            btnRateYourOrder.isEnabled = false
            btnTrackOrder.isEnabled = true
          //  reOrderBtn.isEnabled = true
            
        }else {
            
            //if order is complete. rate you food and reoprder both are active
            
            if rateorderActive == true {
                // order is  complete, we can rate and reorder
            //    reOrderBtn.layer.borderColor = UIColor(named: "ThemeColor")?.cgColor
             //   btnRateYourOrder.layer.borderColor = UIColor.hexStringToUIColor(hex: "10A3FF").cgColor
                btnTrackOrder.layer.borderColor = UIColor(named: "ThemeColor")?.cgColor
            //    reOrderBtn.layer.borderWidth = 1.0
              //  btnRateYourOrder.layer.borderWidth = 1.0
                btnTrackOrder.layer.borderWidth = 0.0
                btnTrackOrder.setTitleColor(.white, for: .normal)
              //  btnRateYourOrder.setTitleColor( UIColor.hexStringToUIColor(hex: "10A3FF"), for: .normal)
             //   reOrderBtn.setTitleColor( UIColor(named: "ThemeColor"), for: .normal)
                btnRateYourOrder.isEnabled = true
                btnTrackOrder.isEnabled = false
              //  reOrderBtn.isEnabled = true
                
                if order?.foodIsRated == true {
                    
                    //when food is already rated, show different UI
                    btnRateYourOrder.layer.borderColor = UIColor.clear.cgColor//UIColor.hexStringToUIColor(hex: "05A660").cgColor
                    btnRateYourOrder.setTitleColor(UIColor.white, for: .normal) //UIColor.hexStringToUIColor(hex: "05A660"
                    btnRateYourOrder.setImage(UIImage(named: "FoodRatedStar"), for: .normal) //"Delivered"
                    btnRateYourOrder.backgroundColor = UIColor.hexStringToUIColor(hex: "2B9C4C")
                    btnRateYourOrder.setTitle("FOOD RATED", for: .normal)
                    btnRateYourOrder.centerTextAndImage(spacing: 5)
                }
                
            }else{
               // only reorder is active
                
               // reOrderBtn.layer.borderColor = UIColor(named: "ThemeColor")?.cgColor
                btnRateYourOrder.layer.borderColor = UIColor.hexStringToUIColor(hex: "10A3FF").cgColor
                btnTrackOrder.layer.borderColor = UIColor(named: "ThemeColor")?.cgColor
             //   reOrderBtn.layer.borderWidth = 1.0
                btnRateYourOrder.layer.borderWidth = 0.0
                btnTrackOrder.layer.borderWidth = 0.0
                btnTrackOrder.setTitleColor(.white, for: .normal)
                btnRateYourOrder.setTitleColor(.white, for: .normal)
            //    reOrderBtn.setTitleColor( UIColor(named: "ThemeColor"), for: .normal)
                btnRateYourOrder.isEnabled = false
                btnTrackOrder.isEnabled = false
            //    reOrderBtn.isEnabled = true
                
            }
        }
        
   

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

    func setupOrder(_ order: Order, deliveryImg: UIImage , colors: (UIColor, UIColor)) {
        ticketIDLbl.textColor = order.backgroudColor
        
        
        stackViewCancelled.isHidden = true
        stackViewDelivery.isHidden = false
        self.order = order
        branchName.text  = order.branchName
        totalAmntLbl.text = "\(getCurrency()) \(order.amount) "//" +  " >
        logoImageView.setImagePNG(url: order.brand_logo, placeholderImage: nil)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy hh:mma"
        let dateFromString = dateFormatter.date(from: order.createdOn)
        
        dateFormatter.dateFormat = " EEEE MMM dd , yyyy"
        
        if let date = dateFromString {
            dateLbl.text = dateFormatter.string(from: date)
        }
        
        dateFormatter.dateFormat = "hh:mm a"
        if let date = dateFromString {
            lblTime.text = dateFormatter.string(from: date)
        }
        
        deliveryStatusLbl.text = order.status
        deliveryStatusLbl.textColor = colors.0 //textcolor
        deliveryStatusImgView.image = deliveryImg
        deliveryStatusImgView.contentMode = .scaleAspectFit
        viewForScheduleOrder.isHidden = true
        buttonsStackView.isHidden = false
        

        ticketIDLbl.text = "#" + order.orderId
        
        ///////////////////////
      //  btnTrackOrder.isHidden = true
        if order.deliveryStatus == .pending{

         //   heightForButtonStack.constant = 38
          //  cellBottomSpaceToButtonStack.constant = 20
            
            for btn in buttonsStackView.subviews {
                
                if btn == btnTrackOrder {
                    
                    buttonsStackView.removeArrangedSubview(btn)
                    buttonsStackView.setNeedsLayout()
                    buttonsStackView.layoutIfNeeded()
                    
                    buttonsStackView.insertArrangedSubview(btn, at: 0)
                    buttonsStackView.setNeedsLayout()
                }
            }
            
            setButtonVisibility(reorderActive: false, rateorderActive: false, trackorderActive: true)
        } else if order.deliveryStatus == .cancelled{

         //   heightForButtonStack.constant = 0
         //   cellBottomSpaceToButtonStack.constant = 10

           // buttonsStackView.isHidden = true
            for btn in buttonsStackView.subviews {
                
                if btn == btnTrackOrder {
                    
                    buttonsStackView.removeArrangedSubview(btn)
                    buttonsStackView.setNeedsLayout()
                    buttonsStackView.layoutIfNeeded()
                    
                    buttonsStackView.insertArrangedSubview(btn, at: 0)
                    buttonsStackView.setNeedsLayout()
                }
            }
            
            stackViewCancelled.isHidden = false
            stackViewDelivery.isHidden = true
            
            //In case of cancelled order show rate and tract  button in disabled mode
            
//            btnRateYourOrder.layer.borderColor = colors.1.cgColor
//            btnRateYourOrder.setTitleColor(colors.0, for: .normal)
//            btnRateYourOrder.layer.borderWidth = 1.5
//            btnRateYourOrder.backgroundColor = UIColor.clear
  //         btnRateYourOrder.isHidden = true
//
//            btnTrackOrder.layer.borderColor = colors.1.cgColor
//            btnTrackOrder.setTitleColor(colors.0, for: .normal)
//            btnTrackOrder.layer.borderWidth = 1.5
//            btnTrackOrder.backgroundColor = UIColor.clear
//            btnTrackOrder.isHidden = true
 //           buttonsStackView.frame.size.height = 0
          //  setButtonVisibility(reorderActive: false, rateorderActive: false, trackorderActive: false)
        } else{
          
         //   heightForButtonStack.constant = 38
           // cellBottomSpaceToButtonStack.constant = 20

            
            for btn in buttonsStackView.subviews {
                
                if btn == btnRateYourOrder {
                    
                    buttonsStackView.removeArrangedSubview(btn)
                    buttonsStackView.setNeedsLayout()
                    buttonsStackView.layoutIfNeeded()
                    
                    buttonsStackView.insertArrangedSubview(btn, at: 0)
                    buttonsStackView.setNeedsLayout()
                }
            }
            setButtonVisibility(reorderActive: false, rateorderActive: true, trackorderActive: false)
        }
       // itemListLbl.text  // order.items[0].name
        
        //Separator String
        let separator = ", "

        //flatMap skips the nil values and then joined combines the non nil elements with the separator
        let joinedString = order.items.compactMap{ $0.name  + "  x " + String($0.quantity) }.joined(separator: separator)

        lblScheduleOrder.isHidden = false
        itemListLbl.text  = joinedString
        lblPickUpOrDelivery.text = "Self Pickup on :"

        if order.is_schedule_delivery{
            viewForScheduleOrder.isHidden = false
            btnTrackOrder.isHidden = true
            btnRateYourOrder.isHidden = true
            lblScheduleOrder.attributedText = order.schedule_delivery_time.getScheduleDeluveryString(isAfterPlaced: true, isNextLine: true, interval: order.lst_delivery_schedule_interval, format: "MM/dd/yyyy hh:mm:ss a", fontSize: 14).0
            lblPickUpOrDelivery.text = "Shipping Address :"
            
        }
        
        
        if order.transaction_type == 1{
            
        //delivery order
            lblPickUpOrDelivery.text = "Shipping Address :"
            branchName.text = order.deliveryAddress
            
        }else{
            
           // takeaway order
            lblPickUpOrDelivery.text = "Self pickup on :"
            branchName.text = order.branchName


        }
       // itemsTableView.reloadData()
    }
    @IBAction func actionOnReorderBtn(_ sender: Any) {
        if let orderid = order?.orderId{
        orderHistoryCellDelegate?.clickedOnReOrder(orderid)
        }
    }
    
    @IBAction func actionOnRateYourFoodBtn(_ sender: Any) {
        
        //Fix - condition need check whether already food is rated or not
        if order?.foodIsRated == true {
            UIUtils.showToast(message: kFoodRated)
            return
        }

        if let orderid = order?.orderId{
            orderHistoryCellDelegate?.clickedOnRateOrder(orderid , selectedIndex: btnRateYourOrder.tag)
        }
    }
    @IBAction func onClickBtnTrackOrder(_ sender: Any) {
        if let orderid = order?.orderId{
            orderHistoryCellDelegate?.clickedOnTrackOrder(orderid)
        }

    }
    override func layoutSubviews() {
        super.layoutSubviews()
  
       // contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
        //contentView.setShadow()UIColor.hexStringToUIColor(hex: "18191F")
        mainView.setCornerRadius(12)
        mainView.setShadow(color: UIColor.hexStringToUIColor(hex: "55606D1A") , opacity: 0.2, offSet: .zero, radius: 7, scale: true)
        
    }
}

extension UIButton {
    func centerTextAndImage(spacing: CGFloat) {
        let insetAmount = spacing / 2
        imageEdgeInsets = UIEdgeInsets(top: 0, left: -insetAmount, bottom: 0, right: insetAmount)
        titleEdgeInsets = UIEdgeInsets(top: 0, left: insetAmount, bottom: 0, right: -insetAmount)
        contentEdgeInsets = UIEdgeInsets(top: 0, left: insetAmount, bottom: 0, right: insetAmount)
    }
}
