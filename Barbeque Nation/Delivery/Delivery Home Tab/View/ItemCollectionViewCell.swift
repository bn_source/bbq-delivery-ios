//
//  ItemCollectionViewCell.swift
//  BBQ
//
//  Created by Shareef on 05/09/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import UIKit

protocol ItemCollectionViewCellDelegate: AnyObject {
    func actionOnAddItemButton(item: Item, indexPath: IndexPath?)
    func actionOnRemoveItemButton(item: Item, indexPath: IndexPath?)
    func updateCellHeight(item: Item, indexPath: IndexPath?, height: CGFloat)
}

class ItemCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var itemDescriptionLabel: UILabel!
    @IBOutlet weak var vegNonVegImageView: UIImageView!
    @IBOutlet weak var itemPriceLabel: UILabel!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var quantityView: UIView!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var lblOldPrice: UILabel!
    @IBOutlet weak var lblPromotion: UILabel!
    @IBOutlet weak var viewForPromotion: CustomDashedView!
    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var btnMinus: UIButton!
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var btnMore: UIButton!
    
    weak var delegate: ItemCollectionViewCellDelegate?
    private var item = Item()
    private var indexPath: IndexPath?
    
    static func getCellIdentifier() -> String {
        return "ItemCollectionViewCell"
    }
    
    func updateCell(indexPath: IndexPath, item: Item, isServicable: Bool, isHeightOpened: Bool) {
        self.item = item
        self.indexPath = indexPath
        updateUI()
        itemNameLabel.text = item.name
        itemDescriptionLabel.text = item.description
        vegNonVegImageView.image = (item.foodType == .veg ? .veg : .nonVeg)
        itemPriceLabel.text = item.price
        updateQuantity(count: item.quantity)
        addBtn.applyMultiBrandThemeWith(isServicable: isServicable, isAvailable: item.isAvailable)
        addBtn.isUserInteractionEnabled = (isServicable && item.isAvailable)
        quantityView.isUserInteractionEnabled = (isServicable && item.isAvailable)
        quantityView.applyViewMultiBrandTheme()
        if !isHeightOpened{
            countLableSize()
        }
    }
    
    private func updateUI() {
        addBtn.setCornerRadius(addBtn.frame.width * 0.15)
        addBtn.setShadow()
        quantityView.setCornerRadius(quantityView.frame.width * 0.15)
        quantityView.setShadow()
        quantityLabel.font = UIFont.appThemeMediumWith(size: 14.0)
        updateOfferData()
        viewForContainer.setCornerRadius(10)
        viewForContainer.setShadow()
        btnPlus.setTitleColor(.deliveryThemeTextColor, for: .normal)
        quantityLabel.textColor = .deliveryThemeTextColor
        btnMinus.setTitleColor(.deliveryThemeTextColor, for: .normal)
    }
    
    private func updateOfferData(){
        if item.originalPrice != item.itemPrice{
            lblOldPrice.isHidden = false
            let attrString = NSAttributedString(string: String(format: "%@%li", getCurrency(), item.originalPrice), attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue])
            lblOldPrice.attributedText = attrString
        }else{
            lblOldPrice.isHidden = true
        }
        
        if item.promotion_message != ""{
            lblPromotion.text = item.promotion_message
            viewForPromotion.isHidden = false
        }else{
            viewForPromotion.isHidden = true
        }
    }
    
    private func countLableSize() {
        let height = item.description.height(withConstrainedWidth: itemDescriptionLabel.frame.size.width, font: itemDescriptionLabel.font)
        btnMore.isHidden = height <= itemDescriptionLabel.frame.size.height + 5
    }
    
    func updateQuantity(count: Int) {
        addBtn.isHidden = count > 0
        quantityLabel.text = String(count)
    }
    
    @IBAction func actionOnAddButton(_ sender: Any) {
        delegate?.actionOnAddItemButton(item: item, indexPath: indexPath)
    }
    @IBAction func onClickBtnMore(_ sender: Any) {
        let height = item.description.height(withConstrainedWidth: itemDescriptionLabel.frame.size.width, font: itemDescriptionLabel.font)
        btnMore.isHidden = true
        delegate?.updateCellHeight(item: item, indexPath: indexPath, height: height - itemDescriptionLabel.frame.size.height)
    }
    @IBAction func actionOnRemoveButton(_ sender: Any) {
        item.quantity -= 1
        updateQuantity(count: item.quantity)
        delegate?.actionOnRemoveItemButton(item: item, indexPath: indexPath)
    }
}
