//
//  MenuItemSelectionTableViewCell.swift
//  BBQ
//
//  Created by Dharani Sadasivam on 02/09/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import UIKit
protocol MenuItemSelectionTableViewCellDelegate: AnyObject {
    func actionOnMenuSelection(_ row: Int, section: Int)
}
class MenuItemSelectionTableViewCell: UITableViewCell {

    @IBOutlet weak var menuSelectionBtn: UIButton!
    @IBOutlet weak var menuItemLbl: UILabel!
    weak var menuItemSelectionDelegate: MenuItemSelectionTableViewCellDelegate?
    var row: Int?
    var section: Int?
    var viewModel: MenuViewModel?
    var item = Item()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func actionOnMenuSelection(_ sender: Any) {
        guard let section = section, let row = row, let _ = viewModel else { return }
        
        
       
       // updateSelection()
        menuItemSelectionDelegate?.actionOnMenuSelection(row, section: section)
    }
    
    func updateCell(_ item: Item) {
        menuItemLbl.text = item.name
        self.item = item
       updateSelection()
    }
    
    func updateSelection() {
        guard let section = section, let customisation = viewModel?.getCustomisationsAt(section) else { return }
        
        menuSelectionBtn.setImage(customisation.selectedItems.contains{$0.id == item.id} ? #imageLiteral(resourceName: "Group 813") : #imageLiteral(resourceName: "Rectangle 351"), for: .normal)
    }
}

