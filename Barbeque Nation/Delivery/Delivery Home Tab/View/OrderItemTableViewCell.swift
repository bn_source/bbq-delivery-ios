//
 //  Created by Dharani Sadasivam on 31/10/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified OrderItemTableViewCell.swift
 //

import UIKit

class OrderItemTableViewCell: UITableViewCell {

    var item: Item?
    
    @IBOutlet weak var itemLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setupView(_ item: Item) {
        self.item = item
        itemLbl.text = "\(item.name)    x \(item.quantity)"
    }
}



