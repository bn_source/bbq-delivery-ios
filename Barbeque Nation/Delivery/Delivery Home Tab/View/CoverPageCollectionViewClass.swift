//
//  CoverPageCollectionViewCell.swift
//  BBQ
//
//  Created by Shareef on 05/09/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import UIKit
import ShimmerSwift

protocol CoverPageCollectionViewClassDelegate: AnyObject {
    func actionOnLocationSearch()
    func actionOnRestaurantSearch()
    func actionOnSafetyCheckView()
    func vegNonVegSwitchChanged(_ uiSwitch: UISwitch)
    func valueChangedOnDeliveryAndTakeawaySegmentControl(_ uiSegmentControl: UISegmentedControl, isChanged: Bool)
    func actionOnSearchItem()
    func actionOnAddItemButton(item: Item, indexPath: IndexPath?)
    func actionOnRemoveItemButton(item: Item, indexPath: IndexPath?)
    func actionOnImageTapped(item: Item, indexPath: IndexPath?, isCameFromSearch: Bool)
    func updateQtyFromHeader(item: Item)
    func loadOfferDetails(cms_offer_id: String, image_url: String)
    func onClickIntroScheduleDelivery()
    func onClickBtnSmiles()
    func onClickBtnEnquireNow()
}

class CoverPageCollectionViewClass: UIView {
    
    @IBOutlet weak var deliveryTakeAwaySegment: UISegmentedControl!
    @IBOutlet weak var btnSmile: UIButton!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var restaurantImageView: UIImageView!
    @IBOutlet weak var safetyCheckView: UIView!
    @IBOutlet weak var vegNonVegSwitch: UISwitch!
    @IBOutlet weak var coverImageScrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var windlessView: UIView!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var windlessContainerView: UIView!
    @IBOutlet weak var ViewForSearch: UIView!
    @IBOutlet weak var heightConstrain: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControlForPromotions: UIPageControl!
    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var stackViewForLimitedOffer: UIStackView!
    @IBOutlet weak var ViewForIntroducingDelivery: UIView!
    @IBOutlet weak var btnHowItWorks: UIButton!
    @IBOutlet weak var btnEnquireNows: UIButton!
    @IBOutlet weak var lblScheduleDelivery: UILabel!
    @IBOutlet weak var collectionViewForOffers: UICollectionView!
    @IBOutlet weak var lblMultipleLoc : UILabel!
    @IBOutlet weak var viewBelwoStackForOffer: UIView!
    var selectedBrandInfo : BrandInfo?
    @IBOutlet var blurViewArray: [UIVisualEffectView]!
    @IBOutlet var imgViewArray : [UIImageView]!
    @IBOutlet weak var viewSaftyWork : UIView!
    @IBOutlet weak var  viewMultiLocation : UIView!
    var shimmerView : ShimmeringView?
    var offerCellSize = CGSize(width: 145, height: 173)
    var offerCellSpace = 16
    var isLimitedOfferAvailable = false
    var limitedOffers = [Item]()
    var isServicable = true
    var promotions = [Promotion]()
    var offers = [Offers]()
    var timer: Timer?
    private var timerCount: Int64 = 0
    
    weak var delegate: CoverPageCollectionViewClassDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //updateUI()
    }
    
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        //updateUI()
    }
    
    static func getCellIdentifier() -> String {
        return "CoverPageCollectionViewCell"
    }
    
    func initTimer()  {
        deinitTimer()
        if let index = limitedOfferCellIndex() {
            if let endDate = limitedOffers[index].end_date.toDate(format: "dd-MMM-yyyy hh:mm"){
                timerCount = Int64(endDate.timeIntervalSince(Date()))
                if timerCount <= 0{
                    lblTimer.isHidden = true
                    return
                }
                timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { repeating in
                    self.lblTimer.text = String(format: "%02li:%02li:%02li", self.timerCount/3600,((self.timerCount-1)/60)%60,self.timerCount%60)
                    if self.timerCount <= 0{
                        self.deinitTimer()
                    }
                    self.timerCount = self.timerCount - 1
                })
                lblTimer.text = String(format: "%02li:%02li:%02li", self.timerCount/3600,((self.timerCount-1)/60)%60,self.timerCount%60)
                lblTimer.isHidden = false
            }
        }
    }
    
    func deinitTimer() {
        lblTimer.isHidden = true
        timer?.invalidate()
        timer = nil
    }
    
    private func limitedOfferCellIndex() -> Int?{
        if collectionView.indexPathsForVisibleItems.count > 0{
            let count = Int(collectionView.contentOffset.x / collectionView.frame.size.width)
            if count >= limitedOffers.count{
                return limitedOffers.count - 1
            }
            return count
        }
        return nil
    }
    
    func updateUserPoints() {
        //btnSmile.setTitle("\(BBQUserDefaults.sharedInstance.UserLoyaltyPoints)", for: .normal)
        let userLoyaltyPoints = BBQUserDefaults.sharedInstance.UserLoyaltyPoints
        let points_string = " \(kMsgBBQPoints)  \(userLoyaltyPoints)"
        let string_to_color = String(userLoyaltyPoints)
        let range = (points_string as NSString).range(of: string_to_color)
        let mainStringRange = (points_string as NSString).range(of: kMsgBBQPoints)
        let attributedString = NSMutableAttributedString(string: points_string)
        let attribute = [NSAttributedString.Key.font: UIFont.appThemeRegularWith(size: 12.0),
                         NSAttributedString.Key.foregroundColor: UIColor.deliveryThemeColor] as [NSAttributedString.Key : Any]
        attributedString.addAttributes(attribute, range: mainStringRange)
        
        let pointAttribute = [NSAttributedString.Key.font: UIFont.appThemeSemiBoldWith(size: 12.0),
                              NSAttributedString.Key.foregroundColor: UIColor.deliveryThemeColor] as [NSAttributedString.Key : Any]
        attributedString.addAttributes(pointAttribute, range: range)
        
        let imageAttachment = NSTextAttachment()
        imageAttachment.image = UIImage(named: "smiles_coins_logo")
        let iconsSize = CGRect(x: 0, y: -8, width: 28, height: 28)
        imageAttachment.bounds = iconsSize
        
        // wrap the attachment in its own attributed string so we can append it
        let imageString = NSMutableAttributedString(attachment: imageAttachment)
        imageString.append(attributedString)
        self.btnSmile.setAttributedTitle(imageString, for: .normal)
        btnSmile.layer.cornerRadius = 5
        btnSmile.setShadow()
    }
    
    
    func updateView(imageUrls: [Promotion], branch: Branch?, offers: [Offers], isDelivery: Bool, isTakeAway: Bool, isNonVegSelected: Bool, isLimitedOfferAvailable: Bool, isServicable: Bool, selectedBrand: String) {
        self.promotions = imageUrls
        self.offers = offers
        self.isLimitedOfferAvailable = isLimitedOfferAvailable
        self.isServicable = isServicable
        updateUI()
        updateCoverImages(imageUrls: promotions)
        updateView(branch: branch, isDelivery: isDelivery, isTakeAway: isTakeAway, isNonVegSelected: isNonVegSelected)
       // lblScheduleDelivery.text = String(format: "%@ 'Your Delivery Your Time'.", selectedBrand)
        stopShimmering()
    }
    
    func updateView(branch: Branch?, isDelivery: Bool, isTakeAway: Bool, isNonVegSelected: Bool) {
//        updateUI()
        guard let branch = branch else { return }
        locationLabel.text = branch.branchName
        vegNonVegSwitch.isOn = !isNonVegSelected
        if isDelivery || isTakeAway {
            deliveryTakeAwaySegment.setEnabled(isDelivery, forSegmentAt: 0)
            deliveryTakeAwaySegment.setEnabled(isTakeAway, forSegmentAt: 1)
            if !isDelivery{
                deliveryTakeAwaySegment.setEnabled(isTakeAway, forSegmentAt: 0)
            }
            deliveryTakeAwaySegment.selectedSegmentIndex = (isDelivery ? 0 : 1)
            deliveryTakeAwaySegment.isEnabled = true
            //If Selected option is Delivery , check if delivery segment is active or not
            //If yes set it
            if deliveryOrTakeAwayOptionSelected == .TakeAway && isTakeAway == true {
                
                deliveryTakeAwaySegment.selectedSegmentIndex = deliveryOrTakeAwayOptionSelected.rawValue

            }else  if deliveryOrTakeAwayOptionSelected == .Delivery && isDelivery == true {
                
                deliveryTakeAwaySegment.selectedSegmentIndex = deliveryOrTakeAwayOptionSelected.rawValue

            }else{
                deliveryTakeAwaySegment.selectedSegmentIndex = (isDelivery ) ? 0 : 1
            }
            setSegmentControl(deliveryTakeAwaySegment!)
        } else {
            deliveryTakeAwaySegment.isEnabled = false
        }
        
        
        
        let strAtr = NSMutableAttributedString()
        
        strAtr.append(NSMutableAttributedString(string: "Schedule a Delivery @ ", attributes: [NSAttributedString.Key.font: UIFont.appThemeMediumWith(size: 11.0), NSAttributedString.Key.foregroundColor: UIColor.hexStringToUIColor(hex: "303030")]))
        
        strAtr.append(NSMutableAttributedString(string: "Your Day Your Time", attributes: [NSAttributedString.Key.font: UIFont.appThemeMediumWith(size: 11.0), NSAttributedString.Key.foregroundColor: UIColor.deliveryThemeColor] ))
        
        lblScheduleDelivery.attributedText = strAtr
        updateMultiLocationText()
        
    }
    
    private func updateUI() {
        restaurantImageView.setCircleView()
        ViewForSearch.setCornerRadius(ViewForSearch.frame.size.height/2.0)
        ViewForSearch.setShadow()
//        safetyCheckView.setCornerRadius(17)
//        safetyCheckView.setShadow()
        
        viewSaftyWork.setCornerRadius(10)
        viewSaftyWork.clipsToBounds = true
        viewSaftyWork.setShadow()
        
        viewMultiLocation.setCornerRadius(10)
        viewMultiLocation.setShadow()
        
        self.layoutSubviews()
        
        deliveryTakeAwaySegment.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.appThemeMediumWith(size: 10)], for: .normal)
        deliveryTakeAwaySegment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.deliveryThemeTextColor], for: .selected)
        deliveryTakeAwaySegment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.deliveryThemeColor], for: .normal)
        deliveryTakeAwaySegment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.lightGray], for: .disabled)
        deliveryTakeAwaySegment.addTarget(self, action: #selector(valueChangeOnDeliveryAndTakeawaySegmentControl(_:)), for: .valueChanged)
        deliveryTakeAwaySegment.backgroundColor = .deliveryThemeBackgroundColor
        deliveryTakeAwaySegment.selectedSegmentIndex = deliveryOrTakeAwayOptionSelected.rawValue
        if #available(iOS 13.0, *) {
            deliveryTakeAwaySegment.selectedSegmentTintColor = .deliveryThemeColor
        } else {
            deliveryTakeAwaySegment.tintColor = .deliveryThemeColor
        }
       // lblScheduleDelivery.applyThemeOnText()
       // lblMultipleLoc.applyThemeOnText()
        
      
        //BBQN 'Your Delivery Your Time'.
        setSegmentControl(deliveryTakeAwaySegment!)
        lblTimer.setCornerRadius(5)

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(actionOnSafetyCheckView))
        safetyCheckView.addGestureRecognizer(tapGestureRecognizer)
        vegNonVegSwitch.setShadow()
        vegNonVegSwitch.addTarget(self, action: #selector(vegNonVegSwitchChanged(_:)), for: .valueChanged)
        pageControl.addTarget(self, action: #selector(pageControlChanged(_:)), for: .valueChanged)
        ViewForIntroducingDelivery.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0)
        btnHowItWorks.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.bottomCornersMask)
        btnEnquireNows.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.bottomCornersMask)
        ViewForIntroducingDelivery.backgroundColor = .deliveryThemeBackgroundColor
        btnHowItWorks.applyMultiBrandTheme()
        btnEnquireNows.applyMultiBrandTheme()
        
        for blur in blurViewArray {
            blur.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0)

            blur.isHidden = true
        }
        for blur in imgViewArray {
            blur.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0)

            blur.isHidden = true

        }
        
        viewSaftyWork.backgroundColor =   .deliveryThemeBackgroundColor
        viewMultiLocation.backgroundColor = .deliveryThemeBackgroundColor

        btnSmile.imageView?.contentMode = .scaleAspectFit
        btnSmile.setImage(nil, for: .normal)
        btnSmile.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0)
        stackViewForLimitedOffer.isHidden = !isLimitedOfferAvailable
        pageControl.isHidden = true
        if isLimitedOfferAvailable{
            heightConstrain.constant = 571+50+100
            if collectionView.indexPathsForVisibleItems.count <= 0{
                collectionView.reloadData {
                    self.initTimer()
                }
            }else{
                self.initTimer()
            }
        }else{
            heightConstrain.constant = 363+50+100
        }
        updateMultiLocationText()
        updateUserPoints()
        collectionViewForOffers.reloadData()
        
        viewBelwoStackForOffer.layer.cornerRadius = 8
        viewBelwoStackForOffer.layer.masksToBounds = true
        viewBelwoStackForOffer.clipsToBounds = true
        
        var color =  UIColor.lightGray
        
        if #available(iOS 13.0, *) {
             color = UIColor.systemGray6
        }
    }
    private func updateMultiLocationText(){
        let strAtrForMultiLocation = NSMutableAttributedString()
        
        strAtrForMultiLocation.append(NSMutableAttributedString(string: String(format: "Gift a %@ @ ", self.selectedBrandInfo?.name ?? "" ), attributes: [NSAttributedString.Key.font: UIFont.appThemeMediumWith(size: 11.0), NSAttributedString.Key.foregroundColor: UIColor.hexStringToUIColor(hex: "303030")]))
        
        strAtrForMultiLocation.append(NSMutableAttributedString(string: "Multiple Locations in India", attributes: [NSAttributedString.Key.font: UIFont.appThemeMediumWith(size: 11.0), NSAttributedString.Key.foregroundColor: UIColor.deliveryThemeColor] ))
        
        lblMultipleLoc.attributedText = strAtrForMultiLocation
        
    }
    private func updateCoverImages(imageUrls: [Promotion]) {
        let pageCount = imageUrls.count
        coverImageScrollView.layoutIfNeeded()
        offerCellSize.width = coverImageScrollView.frame.width - (CGFloat(offerCellSpace) * 2.0)
        offerCellSize.height = coverImageScrollView.frame.height
        pageControlForPromotions.numberOfPages = imageUrls.count
        var frame = CGRect(origin: CGPoint.zero, size: CGSize(width: offerCellSize.width, height: coverImageScrollView.frame.height))
        for index in 0..<pageCount {
            
            frame.origin.x = (offerCellSize.width * CGFloat(index)) + CGFloat(index * offerCellSpace * 2) + 16.0
            let bannerImageView = UIImageView(frame: frame)
            bannerImageView.contentMode = .scaleToFill
            bannerImageView.setImage(url: imageUrls[index].imageUrl, placeholderImage: nil, isForPromotions: true)
            bannerImageView.setCornerRadius(4)
            self.addTapGestureToOffer(imageView: bannerImageView, index: index)
            coverImageScrollView.addSubview(bannerImageView)
        }
        coverImageScrollView.contentSize = CGSize(width: offerCellSize.width * CGFloat(pageCount) + CGFloat(pageCount * offerCellSpace * 2), height: coverImageScrollView.frame.height)
    }
    
    private func addTapGestureToOffer(imageView: UIImageView, index: Int){
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(cellTappedMethod(sender:)))
        imageView.isUserInteractionEnabled = true
        imageView.tag = index
        imageView.addGestureRecognizer(tapGestureRecognizer)
    }

    @objc func cellTappedMethod(sender: UITapGestureRecognizer){
        if let index = sender.view?.tag, promotions.count > index{
            delegate?.loadOfferDetails(cms_offer_id: promotions[index].delivery_item_number, image_url: promotions[index].imageUrl)
        }
    }
    
    func showShimmering() {
        
        self.windlessView.isHidden = false
        self.windlessContainerView.isHidden = false
        
        if let shimmer = self.shimmerView{
            self.windlessContainerView.addSubview(shimmer)
            shimmer.contentView = windlessView
            
            shimmer.shimmerAnimationOpacity = 0.2
            shimmer.shimmerSpeed = 500.00
            shimmer.isShimmering = true
        }else{
            shimmerView = ShimmeringView(frame: windlessContainerView.frame)
            if let shimmer = self.shimmerView{
                self.windlessContainerView.addSubview(shimmer)
                shimmer.contentView = windlessView
                
                shimmer.shimmerAnimationOpacity = 0.2
                shimmer.shimmerSpeed = 500.00
                shimmer.isShimmering = true
            }
        }
    }
    
    func stopShimmering() {

        if let shimmer = self.shimmerView {
            shimmer.isShimmering = false
        }
        self.windlessView.isHidden = true
        self.windlessContainerView.isHidden = true
    }
    
    @IBAction func actionOnLocationSearch(_ sender: Any) {
        delegate?.actionOnRestaurantSearch()
    }
    
    @IBAction func actionOnRestaurantSearch(_ sender: Any) {
        delegate?.actionOnRestaurantSearch()
    }
    
    @objc func actionOnSafetyCheckView() {
        delegate?.actionOnSafetyCheckView()
    }
    
    @objc func vegNonVegSwitchChanged(_ uiSwitch: UISwitch) {
        delegate?.vegNonVegSwitchChanged(uiSwitch)
    }
    
    @IBAction func onClickBtnSearch(_ sender: Any) {
        delegate?.actionOnSearchItem()
    }
    
    @IBAction func onClickBtnSafetyFeatures(_ sender: Any) {
        //delegate?.actionOnSafetyCheckView()
    }
    
    @IBAction func onClickBtnSmiles(_ sender: Any) {
        delegate?.onClickBtnSmiles()
    }
    @IBAction func onClickBtnEnquireNow(_ sender : Any){
        delegate?.onClickBtnEnquireNow()
        
    }
    @IBAction func onClickBtnHowItWork(_ sender: Any) {
        delegate?.onClickIntroScheduleDelivery()
    }
    @objc func valueChangeOnDeliveryAndTakeawaySegmentControl(_ uiSegmentControl: UISegmentedControl) {
        delegate?.valueChangedOnDeliveryAndTakeawaySegmentControl(uiSegmentControl, isChanged: true)
    }
    
    private func setSegmentControl(_ uiSegmentControl: UISegmentedControl){
        delegate?.valueChangedOnDeliveryAndTakeawaySegmentControl(uiSegmentControl, isChanged: false)
    }
    
    @objc func pageControlChanged(_ uiPageControl: UIPageControl) {
        let xPosition = CGFloat(pageControl.currentPage) * coverImageScrollView.frame.size.width
        coverImageScrollView.setContentOffset(CGPoint(x: xPosition, y: 0), animated: true)
    }

}


extension CoverPageCollectionViewClass: UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == collectionView{
            let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
            pageControl.currentPage = Int(pageNumber)
            initTimer()
        }else{
            let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
            pageControlForPromotions.currentPage = Int(pageNumber)
        }
    }
}

extension CoverPageCollectionViewClass: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionViewForOffers{
            return offers.count
        }
        return limitedOffers.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionViewForOffers{
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OfferCategoryCell", for: indexPath) as? OfferCategoryCell, offers.count > indexPath.row else { return collectionView.defaultCell(indexPath: indexPath)}
            cell.setCellData(offer: offers[indexPath.row])
            return cell
        }
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LimitedOfferCell", for: indexPath) as? LimitedOfferCell else { return collectionView.defaultCell(indexPath: indexPath)}
        cell.setCellData(item: limitedOffers[indexPath.row], indexPath: indexPath, isServicable: isServicable)
        cell.delegate = self
        initTimer()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row < offers.count{
            delegate?.loadOfferDetails(cms_offer_id: offers[indexPath.row].id, image_url: "")
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionViewForOffers{
            if offers.count > indexPath.row{
                let width = offers[indexPath.row].name.width(withConstrainedHeight: 30, font: UIFont.appThemeRegularWith(size: 14.0))
                return CGSize(width: width+50, height: 40)
            }
            return CGSize(width: 0, height: 0)
        }
        if limitedOffers.count > 1 {
            return CGSize(width: collectionView.frame.size.width - 36, height: collectionView.frame.size.height)
        }else{
            return CGSize(width: collectionView.frame.size.width - 4, height: collectionView.frame.size.height)
        }
    }
}

extension CoverPageCollectionViewClass: LimitedOfferCellDelegate{
    func actionOnAddItemButton(item: Item, indexPath: IndexPath?) {
        delegate?.updateQtyFromHeader(item: item)
        delegate?.actionOnAddItemButton(item: item, indexPath: indexPath)
    }
    
    func actionOnRemoveItemButton(item: Item, indexPath: IndexPath?) {
        delegate?.updateQtyFromHeader(item: item)
        delegate?.actionOnRemoveItemButton(item: item, indexPath: indexPath)
    }
    
    func actionOnImageTapped(item: Item, indexPath: IndexPath?, isCameFromSearch: Bool) {
        delegate?.actionOnImageTapped(item: item, indexPath: indexPath, isCameFromSearch: isCameFromSearch)
    }
}
