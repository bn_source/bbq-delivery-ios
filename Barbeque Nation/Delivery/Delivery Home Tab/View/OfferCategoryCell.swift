//
 //  Created by Sakir Sherasiya on 27/10/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified OfferCategoryCell.swift
 //

import UIKit

class OfferCategoryCell: UICollectionViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var viewForImage: UIView!
    func setCellData(offer: Offers){
        
        var color = UIColor.deliveryThemeColor
        if #available(iOS 13.0, *) {
            color = UIColor.systemGray4
        }
        viewForContainer.roundCorners(cornerRadius: (viewForContainer.frame.size.height/2), borderColor: color, borderWidth: 1.0)
       // viewForContainer.dropShadow()
        lblTitle.text = offer.name
        //lblTitle.textColor = UIColor.deliveryThemeColor
    }
    
}
