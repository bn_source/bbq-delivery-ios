//
 //  Created by Mahmadsakir on 26/05/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified DeliveryHomeOfferListCell.swift
 //

import UIKit

class DeliveryHomeOfferListCell: UITableViewCell {
    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var imgFoodType: UIImageView!
    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var lblItemDesc: UILabel!
    @IBOutlet weak var lblItemPrice: UILabel!
    @IBOutlet weak var imgItem: UIImageView!
    @IBOutlet weak var viewForQuantity: UIView!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var lblCustmisation: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var lblOldPrice: UILabel!
    @IBOutlet weak var lblPromotion: UILabel!
    @IBOutlet weak var viewForPromotion: CustomDashedView!
    @IBOutlet weak var btnMinus: UIButton!
    @IBOutlet weak var btnPlus: UIButton!
    
    weak var delegate: DeliveryHomeOfferListCellDelegate?
    private var item: Item!
    private var indexPath: IndexPath!
    var isUpdated = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCellData(item: Item, indexPath: IndexPath, isServicable: Bool) {
        self.item = item
        self.indexPath = indexPath
        
        updateUI()
        lblItemName.text = item.name
        lblItemDesc.text = item.description
        imgItem.setImage(url: item.imageUrl, placeholderImage: nil, isForPromotions: false)
        imgFoodType.image = (item.foodType == .veg ? .veg : .nonVeg)
        lblItemPrice.text = item.price
        updateQuantity(count: item.quantity)
        btnAdd.applyMultiBrandThemeWith(isServicable: isServicable, isAvailable: item.isAvailable)
        btnAdd.isUserInteractionEnabled = (isServicable && item.isAvailable)
        viewForQuantity.isUserInteractionEnabled = (isServicable && item.isAvailable)
        lblCustmisation.isHidden = !item.isCustomizable
        viewForQuantity.applyViewMultiBrandTheme()
        btnPlus.setTitleColor(.deliveryThemeTextColor, for: .normal)
        lblQuantity.textColor = .deliveryThemeTextColor
        btnMinus.setTitleColor(.deliveryThemeTextColor, for: .normal)
    }
    
    /*
    if let search = search, search.count > 0{
        var strString = search
        if !item.name.contains(search), let range = item.name.range(of: search, options: .caseInsensitive){
            strString = String(item.name[range])
        }
        let arrData = item.name.components(separatedBy: strString)
        let strAtr = NSMutableAttributedString()
        for i in 0..<arrData.count{
            if i != 0{
                strAtr.append(NSMutableAttributedString(string: strString, attributes: [NSAttributedString.Key.font: UIFont.systemFontOfSize(size: 16.0, weight: .semibold), NSAttributedString.Key.foregroundColor: UIColor.theme]))
            }
            strAtr.append(NSMutableAttributedString(string: arrData[i], attributes: [NSAttributedString.Key.font: UIFont.systemFontOfSize(size: 14.0, weight: .semibold), NSAttributedString.Key.foregroundColor: UIColor(named: "TextColor") as Any]))
        }
        itemNameLabel.attributedText = strAtr
        
    }
    */
 
    private func updateUI() {
        imgItem.setCornerRadius(11)
        btnAdd.setCornerRadius(5.0)
        btnAdd.setShadow()
        viewForQuantity.setCornerRadius(5.0)
        viewForQuantity.setShadow()
        viewForContainer.setCornerRadius(10.0)
        viewForContainer.setShadow()
        imgItem.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.rightCornersMask)
        lblQuantity.font = UIFont.appThemeExtraBoldWith(size: 14.0)
        updateOfferData()
    }
    
    private func updateOfferData(){
        if item.originalPrice != item.itemPrice{
            lblOldPrice.isHidden = false
            let attrString = NSAttributedString(string: String(format: "%@%li", getCurrency(), item.originalPrice), attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue])
            lblOldPrice.attributedText = attrString
        }else{
            lblOldPrice.isHidden = true
        }
        
        if item.promotion_message != ""{
            lblPromotion.text = item.promotion_message
            viewForPromotion.isHidden = false
        }else{
            viewForPromotion.isHidden = true
        }
    }
    
    //MARK:- Button Actions
    
    @IBAction func onClickBtnImage(_ sender: Any) {
        delegate?.actionOnImageTapped(item: item, indexPath: indexPath)
        AnalyticsHelper.shared.triggerEvent(type: .DS06)
    }
    
    @IBAction func onClickBtnMinus(_ sender: Any) {
        //decreaseItemEvent(type: item.foodCategory)
        item.quantity -= 1
        delegate?.actionOnRemoveItemButton(item: item, indexPath: indexPath)
        updateQuantity(count: item.quantity)
    }
    
    @IBAction func onClickBtnPlus(_ sender: Any) {
        //increaseItemEvent(type: item.foodCategory)
        delegate?.actionOnAddItemButton(item: item, indexPath: indexPath)
        updateQuantity(count: item.quantity)
    }
    
    @IBAction func onClickBtnAdd(_ sender: Any) {
        //addToCartItemEvent(type: item.foodCategory)
        isUpdated = true
        delegate?.actionOnAddItemButton(item: item, indexPath: indexPath)
        updateQuantity(count: item.quantity)
    }
    
    
    
    func updateQuantity(count: Int){
        btnAdd.isHidden = count > 0 ? true : false
        viewForQuantity.isHidden = count > 0 ? false : true
        lblQuantity.text = String(count)
    }
    
    
    private func addToCartItemEvent(type: FoodCategory) {
        switch type {
        case .comboBox:
            AnalyticsHelper.shared.triggerEvent(type: .DS03)
            break
        case .comboItem:
            AnalyticsHelper.shared.triggerEvent(type: .DS04)
            break
        case .item:
            AnalyticsHelper.shared.triggerEvent(type: .DS05)
            break
        }
    }
    
    private func increaseItemEvent(type: FoodCategory) {
        switch type {
        case .comboBox:
            AnalyticsHelper.shared.triggerEvent(type: .DS03A)
            break
        case .comboItem:
            AnalyticsHelper.shared.triggerEvent(type: .DS04A)
            break
        case .item:
            AnalyticsHelper.shared.triggerEvent(type: .DS05A)
            break
        }
    }
    
    private func decreaseItemEvent(type: FoodCategory) {
        switch type {
        case .comboBox:
            AnalyticsHelper.shared.triggerEvent(type: .DS03B)
            break
        case .comboItem:
            AnalyticsHelper.shared.triggerEvent(type: .DS04B)
            break
        case .item:
            AnalyticsHelper.shared.triggerEvent(type: .DS05B)
            break
        }
    }
    
}


    
protocol DeliveryHomeOfferListCellDelegate: AnyObject {
    func actionOnDone(_ item: Item, indexPath: IndexPath?)
    func actionOnAddItemButton(item: Item, indexPath: IndexPath?)
    func actionOnRemoveItemButton(item: Item, indexPath: IndexPath?)
    func actionOnImageTapped(item: Item, indexPath: IndexPath?)
}


