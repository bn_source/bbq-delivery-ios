//
//  MenuTableViewCell.swift
//  BBQ
//
//  Created by Dharani Sadasivam on 23/08/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import UIKit
protocol MenuTableViewCellDelegate: AnyObject {
    func actionOnAddItemButton(item: Item, section: Int, row: Int)
    func actionOnRemoveItemButton(item: Item, section: Int, row: Int)
}
class MenuTableViewCell: UITableViewCell {
    
    @IBOutlet weak var addQuantityCornerView: UIView!
    @IBOutlet weak var plusMinusView: UIView!
    @IBOutlet weak var addQuantityView: UIView!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var menuNameLbl: UILabel!
    @IBOutlet weak var menuIcon: UIImageView!
    private var item =  Item()
    weak var delegate: MenuTableViewCellDelegate?
    var section: Int?
    var row: Int?
    var menuViewModel: MenuViewModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        addBtn.backgroundColor = .theme
        addBtn.isUserInteractionEnabled = true
    }
    @IBAction func actionOnPlusBtn(_ sender: Any) {
        guard let viewModel = menuViewModel, let section = section else { return }
        if viewModel.canAddItem(section) && item.allowMultiple {
            viewModel.addSelectedItem(section, item: item)
            item.quantity += 1
            updateQuantity(count: item.quantity)
            guard  let row = row else { return }
            delegate?.actionOnAddItemButton(item: item, section: section, row: row)
        }
    }
    
    @IBAction func actionOnMinusBtn(_ sender: Any) {
        guard let viewModel = menuViewModel, let section = section else { return }
        if item.quantity > 0 {
        item.quantity -= 1
        viewModel.removeSelectedItem(section, item: item)
        updateQuantity(count: item.quantity)
        guard let row = row else { return }
        delegate?.actionOnRemoveItemButton(item: item, section: section, row: row)
        }
    }
    
    @IBAction func actionOnAddBtn(_ sender: Any) {
        guard let viewModel = menuViewModel, let section = section else { return }
        if viewModel.canAddItem(section) {
            viewModel.addSelectedItem(section, item: item)
            item.quantity += 1
            activatePlusMinusView(true)
            guard let row = row else { return }
            delegate?.actionOnAddItemButton(item: item, section: section, row: row)
        }
    }
    
    func styleUI() {
        addQuantityCornerView.setShadow()
        addQuantityView.setCornerRadius(self.addQuantityView.frame.height/2)
    }
    
    func activatePlusMinusView(_ activate: Bool) {
        plusMinusView.isHidden = !activate
        addBtn.isHidden = activate
    }
    
    func updateCell(_ item: Item) {
        self.item = item
        menuNameLbl.text = item.name
        menuIcon.image = item.foodType == FoodType.veg ? #imageLiteral(resourceName: "Group 846") : #imageLiteral(resourceName: "Group 845")
        guard let viewModel = menuViewModel, let section = section else { return }
        let selectedItem = viewModel.getCustomisationsAt(section).selectedItems
        print(selectedItem.description)
        if (selectedItem.count == (Int(viewModel.getCustomisationsAt(section).threshold)  ?? 0)) &&  (Int(viewModel.getCustomisationsAt(section).threshold)  ?? 0) > 0 {
            
            addBtn.backgroundColor = selectedItem.contains{$0.id == item.id} ?  .theme : .lightGray
            addBtn.isUserInteractionEnabled = selectedItem.contains{$0.id == item.id}
        }
        updateQuantity(count: selectedItem.filter{$0.id == item.id}.count)
    }
    
    func updateQuantity(count: Int) {
        activatePlusMinusView(count > 0)
        if count > 0 {
        quantityLbl.text = String(count)
        }
    }
    
    
}
