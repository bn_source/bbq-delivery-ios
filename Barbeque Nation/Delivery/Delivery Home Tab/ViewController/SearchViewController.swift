//
//  SearchViewController.swift
//  BBQ
//
//  Created by Dharani Sadasivam on 09/08/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import UIKit
protocol SearchViewControllerDelegate: AnyObject {
    func didSelectRestaurant(_ branch: Branch)
    func dismissLocationViewController()
}

class SearchViewController: BaseViewController {
    
    @IBOutlet weak var searchTextBorder: UIView!
    @IBOutlet weak var tableViewBorder: UIView!
    @IBOutlet weak var locationTxtField: UITextField!
    @IBOutlet weak var searchTableViewHCons: NSLayoutConstraint!
    @IBOutlet weak var searchTableView: UITableView!
    @IBOutlet weak var lblNoData: UILabel!
    
    var brand_logo_string: String = ""
    var data: [Branch] = []
    weak var delegate: SearchViewControllerDelegate?
    var searchDataSource: SearchDataSource?
    var cellHeight = 70
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchDataSource = SearchDataSource([])
        searchTableView.dataSource = searchDataSource
        searchTableView.delegate = self
        styleUI()
        searchDataSource?.locations = data
        searchDataSource?.brand_logo_string = brand_logo_string
        updateSearchResultUI()
        searchTableView.reloadData()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        delegate?.dismissLocationViewController()
    }
    override func viewDidLayoutSubviews() {
        
    }
    
    func styleUI() {
//        tableViewBorder.layer.cornerRadius = 16
//        tableViewBorder.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 1)
//        tableViewBorder.layer.borderWidth = 0.25
        
        searchTextBorder.setShadow(color: .lightGray, opacity: 0.2, offSet: .zero, radius: 5.0, scale: false)
        self.navigationController?.navigationBar.tintColor = UIColor(named: "TextColor")
        self.navigationController?.navigationBar.topItem?.title = ""
        updateSearchResultUI()
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        button.setImage(#imageLiteral(resourceName: "Icon material-my-location"), for: .normal)
        locationTxtField.addRightView(button)
        locationTxtField.delegate = self
        
        self.navigationController?.view.backgroundColor = .white
        
    }
    
    func updateSearchResultUI() {
        if (searchDataSource?.locations?.count ?? 0) > 0 {
//            searchTextBorder.drawBorder(edges: [.bottom], borderWidth: 0.25, color: #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 1))
        }
        let tableVCons = (CGFloat((searchDataSource?.locations?.count ?? 0) * cellHeight) + CGFloat(cellHeight))
        let newHeight = ((self.view.frame.height) - (self.locationTxtField.frame.origin.y + self.locationTxtField.frame.height) - 60)
        
        //MARK: - DEL-1564 Fix for layout issue
        
        //        searchTableViewHCons.constant =  tableVCons > newHeight ? newHeight : tableVCons
        
        //END

        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
    }
}

extension SearchViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if searchDataSource?.locations?.count ?? 0 <= indexPath.row{
            self.dismiss(animated: true, completion: nil)
            return
        }
        let location = searchDataSource?.locations?[indexPath.row]
        guard let branch = location else { return }
        self.dismiss(animated: true, completion: nil)
        self.delegate?.didSelectRestaurant(branch)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return CGFloat(cellHeight)
    }
}


extension SearchViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textVal = NSString(string: textField.text!).replacingCharacters(in: range, with: string).lowercased()
        if textVal.count == 0{
            searchDataSource?.locations = data
        }else{
            searchDataSource?.locations = data.filter{$0.branchFullName.lowercased().contains(textVal.lowercased())}.map{$0}
        }
        updateSearchResultUI()
        if searchDataSource?.locations?.count ?? 0 > 0{
            searchTableView.isHidden = false
            lblNoData.isHidden = true
        }else{
            //MARK: - DEL-1564 Fix for layout issue
            
            //            searchTableView.isHidden = true
            
            //END
            lblNoData.isHidden = false
        }
        searchTableView.reloadData()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("textfieldendediting")
    }
}

class SearchDataSource: NSObject, UITableViewDataSource {
    var locations: [Branch]?
    var brand_logo_string: String = ""
    var brand_logo_image: UIImage?
    
    init(_ loc: [Branch]) {
        self.locations = loc
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.locations?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ResLocTableViewCell", for: indexPath) as? ResLocTableViewCell, indexPath.row < self.locations?.count ?? 0{
            cell.searchLbl.text = self.locations?[indexPath.row].branchFullName
            if let image = brand_logo_image{
                cell.imgBrandLogo.image = image
            }else if let image = cell.imgBrandLogo.getLocalImage(imageUrl: brand_logo_string){
                cell.imgBrandLogo.image = image
                self.brand_logo_image = image
            }else{
                cell.imgBrandLogo.setImagePNG(url: brand_logo_string, placeholderImage: nil)
            }
            
            return cell
        }
        return UITableViewCell()
    }
    
}



