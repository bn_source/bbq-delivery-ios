//
 //  Created by Mahmadsakir on 16/08/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified FeatureInfoView.swift
 //

import UIKit


class FeatureInfoView: UIView {
    

    @IBOutlet weak var backgroundAlpha1: UIView!
    @IBOutlet weak var backgroundAlpha2: UIView!
    @IBOutlet weak var backgroundAlpha3: UIView!
    @IBOutlet weak var backgroundAlpha4: UIView!
    @IBOutlet weak var lblMessageData: UILabel!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var heightForTopView: NSLayoutConstraint!
    @IBOutlet weak var stackViewForSpacing: UIStackView!
    @IBOutlet weak var widthForSpacing: NSLayoutConstraint!
    @IBOutlet weak var heightForStackView: NSLayoutConstraint!
    
    
    
    var superVC: UIViewController?
    
    
    //MARK:- View Configuration
    func initWith(rect: CGRect, message: String) -> FeatureInfoView {
        let view = loadFromNib()!
        view.styleUI(rect: rect, message: message)
        return view
    }
        
        
    func loadFromNib() -> FeatureInfoView? {
        if let views = Bundle.main.loadNibNamed("FeatureInfoView", owner: self, options: nil), let view = views[0] as? FeatureInfoView{
            view.frame = UIApplication.shared.windows[0].bounds
            return view
        }
        return nil
    }
    
    
    func addToSuperView(view: UIViewController?) {
        superVC = view
        superVC?.tabBarController?.tabBar.isHidden = true
        
        if let view = view{
            view.view.addSubview(self)
        }else{
            getMainView().addSubview(self)
        }
        self.setAlphaBack(alpha: 0)
        self.setHiddenBack(isHidden: true)
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
            self.setAlphaBack(alpha: 0.8)
            self.setHiddenBack(isHidden: false)
        } completion: { (isCompleted) in
            self.setAlphaBack(alpha: 0.8)
            self.setHiddenBack(isHidden: false)
        }
    }
    
    private func setAlphaBack(alpha: CGFloat){
        self.backgroundAlpha1.alpha = alpha
        self.backgroundAlpha2.alpha = alpha
        self.backgroundAlpha3.alpha = alpha
        self.backgroundAlpha4.alpha = alpha
    }
    
    private func setHiddenBack(isHidden: Bool){
        self.lblMessageData.isHidden = isHidden
        self.btnDone.isHidden = isHidden
    }
    
    func removeToSuperView() {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
            self.setAlphaBack(alpha: 0)
            self.setHiddenBack(isHidden: true)
        } completion: { (isCompleted) in
            self.setAlphaBack(alpha: 0)
            self.setHiddenBack(isHidden: true)
            self.removeFromSuperview()
        }
        
        superVC?.tabBarController?.tabBar.isHidden = false
        superVC = nil
    }
    
    func styleUI(rect: CGRect, message: String) {
        btnDone.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0)
        heightForTopView.constant = rect.origin.y
        widthForSpacing.constant = rect.origin.x
        stackViewForSpacing.spacing = rect.size.width
        heightForStackView.constant = rect.size.height
        lblMessageData.text = message
    }
    
    @IBAction func onClickOnBackground(_ sender: Any) {
        //removeToSuperView()
    }
    @IBAction func onClickBtnDone(_ sender: Any) {
        removeToSuperView()
    }
}

