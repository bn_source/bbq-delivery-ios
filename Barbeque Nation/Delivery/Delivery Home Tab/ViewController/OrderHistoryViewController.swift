//
 //  Created by Dharani Sadasivam on 31/10/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified OrderHistoryViewController.swift
 //

import UIKit

class OrderHistoryViewController: BaseViewController {

    @IBOutlet weak var orderHistoryTableView: UITableView!
    @IBOutlet weak var stackViewForEmpty: UIStackView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    var orderViewModel: OrderHistoryViewModel?
    var orderHeights: [Int: CGFloat] = [:]
    
    var orderViewModelCategoryWise: OrderHistoryViewModel?
    
    var bottomSheetController: BottomSheetController?

    var foodratedOrderID: String = ""
    override var isViewLoaded: Bool {
        setupViews()
        return super.isViewLoaded
      }

    
    override func viewDidLoad() {
        setupViews()
        super.viewDidLoad()
        AnalyticsHelper.shared.triggerEvent(type: .DH01)
        self.orderHistoryTableView.estimatedRowHeight = CGFloat(88.0)
        self.orderHistoryTableView.rowHeight = UITableView.automaticDimension
        orderViewModel?.getAllOrders {
            if self.orderViewModel?.orderList?.count ?? 0 > 0{
                self.orderViewModelCategoryWise = OrderHistoryViewModel()
                self.orderViewModelCategoryWise?.orderList = self.orderViewModel?.orderList
                AnalyticsHelper.shared.triggerEvent(type: .DH02)
                self.orderHistoryTableView.isHidden = false
                self.stackViewForEmpty.isHidden = true
            }else{
                AnalyticsHelper.shared.triggerEvent(type: .DH02A)
                self.orderHistoryTableView.isHidden = true
                self.stackViewForEmpty.isHidden = false
            }
            self.orderHeights.removeAll()
            self.orderHistoryTableView.reloadData()
        }
        
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: 80)
               bottomSheetController = BottomSheetController(configuration: configuration)
    }
    
    func setUI(){
        segmentControl.backgroundColor = UIColor.hexStringToUIColor(hex: "FEF1E4")
        
        segmentControl.tintColor =  UIColor.hexStringToUIColor(hex: "FEF1E4")
          // segmentedControl.backgroundColor = background
        
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.appThemeRegularWith(size: 12)], for: .normal)
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.appThemeSemiBoldWith(size: 12)], for: .selected)
        
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.hexStringToUIColor(hex: "F04B24")], for: .normal)
        
        fixBackgroundSegmentControl(segmentControl)
        //segmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.lightGray], for: .disabled)
        segmentControl.addTarget(self, action: #selector(valueChangedOnDeliveryAndTakeawaySegmentControl(_:)), for: .valueChanged)
       // valueChangedOnDeliveryAndTakeawaySegmentControl(segmentControl!)
    }
    
    func fixBackgroundSegmentControl( _ segmentControl: UISegmentedControl){
        if #available(iOS 13.0, *) {
            //just to be sure it is full loaded
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                for i in 0...(segmentControl.numberOfSegments-1)  {
                    let backgroundSegmentView = segmentControl.subviews[i]
                    //it is not enogh changing the background color. It has some kind of shadow layer
                    backgroundSegmentView.isHidden = true
                }
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        hideNavigationBar(true, animated: true)
        super.viewWillAppear(true)
        self.screenName(name: .Delivery_History_Screen)
        if foodratedOrderID != "" {
            
            //when a order is rated and comming back to this screen
            //Find out index using orderID, make changes in rate button
            getIndexOfOrderDependOnID()
        }
        setUI()
    }
    
    //get orderindex depending on orderID which food id rated, now and change its  food rated button
    func getIndexOfOrderDependOnID()  {
        
        orderViewModel?.orderList?.first { $0.orderId == foodratedOrderID }?.foodIsRated = true
        
        orderHistoryTableView.reloadData()

    }
    override func viewWillDisappear(_ animated: Bool) {
        hideNavigationBar(true, animated: true)
        super.viewWillDisappear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       // hideNavigationBar(false, animated: false)
    }
    
    func setupViews() {
        self.navigationController?.view.backgroundColor = .white
        self.navigationController?.navigationBar.tintColor = UIColor(named: "TextColor")
        self.navigationController?.navigationBar.topItem?.title = "My Orders"
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.theme]
    }
    @IBAction func onClickBtnOrderNow(_ sender: Any) {
        self.navigateToHomePage()
        if let tabBarController = navigationController?.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
            tabBarController.selectedIndex = 2
        }
    }
    
    @IBAction func backButtonPressed(_ sender : UIButton){
        
        self.navigationController?.popViewController(animated: true)
    }
}


extension OrderHistoryViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderViewModelCategoryWise?.orderList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderHistoryTableViewCell", for: indexPath) as! OrderHistoryTableViewCell
        cell.orderHistoryCellDelegate = self
        if let orderViewModel = orderViewModelCategoryWise, let order = orderViewModelCategoryWise?.orderList?[indexPath.row] {
            cell.btnRateYourOrder.tag = indexPath.row
            cell.setupOrder(order, deliveryImg: orderViewModel.getDeliveryStatusImg(order.deliveryStatus) , colors:  orderViewModel.getTextAndImageColorDependingOnStatus(order.deliveryStatus ))
        }
        
        return cell
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//
//        return 240
//    }
    
      func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        AnalyticsHelper.shared.triggerEvent(type: .DH03)
        let orderDetailsVC = UIUtils.viewController(storyboardId: "DeliveryHome", vcId: "OrderDetailsViewController") as! OrderDetailsViewController
        orderDetailsVC.orderViewModel = orderViewModelCategoryWise
        orderDetailsVC.orderIdVal = orderViewModelCategoryWise?.orderList?[indexPath.row].orderId
        orderDetailsVC.orderCreatedOn = orderViewModelCategoryWise?.orderList?[indexPath.row].createdOn
        if let status = orderViewModelCategoryWise?.orderList?[indexPath.row].deliveryStatus{
            switch status {
            case .acknowledged:
                AnalyticsHelper.shared.triggerEvent(type: .DH03A)
                break
            case .pending:
                AnalyticsHelper.shared.triggerEvent(type: .DH03B)
                break
            case .delivered:
                AnalyticsHelper.shared.triggerEvent(type: .DH03C)
                break
            case .cancelled:
                AnalyticsHelper.shared.triggerEvent(type: .DH03D)
                break
            
            }
        }
        self.navigationController?.pushViewController(orderDetailsVC, animated: true)
    }
    
}

extension OrderHistoryViewController: OrderHistoryTableViewCellDelegate {
    
    
    @objc func valueChangedOnDeliveryAndTakeawaySegmentControl(_ uiSegmentControl: UISegmentedControl){
        
        switch (uiSegmentControl.selectedSegmentIndex){
        case 0:
            //All orders
            orderViewModelCategoryWise?.orderList = orderViewModel?.orderList
            
        case 1:
            //Show only in progress order
            orderViewModelCategoryWise?.orderList =    orderViewModel?.orderList?.filter({ model in
                model.deliveryStatus == .pending
            })
            
        case 2:
            // show only delivered orders
            orderViewModelCategoryWise?.orderList = orderViewModel?.orderList?.filter({ model in
                model.deliveryStatus == .delivered
            })
        default:
            orderHistoryTableView.reloadData()
            
        }
        if orderViewModelCategoryWise?.orderList?.count == 0{
            self.orderHistoryTableView.isHidden = true
            self.stackViewForEmpty.isHidden = false
        }else{
            self.orderHistoryTableView.isHidden = false
            self.stackViewForEmpty.isHidden = true
            orderHistoryTableView.reloadData()
            
        }
    }

    
    func clickedOnReOrder(_ orderId: String) {
        
        let deliveryOrderDetailsVC = UIUtils.viewController(storyboardId: "DeliveryHome", vcId: "BBQReorderDetailsViewController") as! BBQReorderDetailsViewController
      
       // deliveryOrderDetailsVC.orderViewModel = DeliveryStatusResponse ?? nil
        deliveryOrderDetailsVC.orderIdVal = orderId
         self.navigationController?.pushViewController(deliveryOrderDetailsVC, animated: true)
    }
    
    func tableViewCellSelect(_ orderId: String, _ createdOn: String) {
        let orderDetailsVC = UIUtils.viewController(storyboardId: "DeliveryHome", vcId: "OrderDetailsViewController") as! OrderDetailsViewController
        orderDetailsVC.orderViewModel = orderViewModel
        orderDetailsVC.orderIdVal = orderId
        orderDetailsVC.orderCreatedOn = createdOn
        self.navigationController?.pushViewController(orderDetailsVC, animated: true)
    }
    func clickedOnTrackOrder(_ orderId: String) {
        AnalyticsHelper.shared.triggerEvent(type: .DH04)

        
        let deliveryOrderDetailsVC = UIUtils.viewController(storyboardId: "OrderStatus", vcId: "BBQOrderStatusVIewControllerViewController") as! BBQOrderStatusVIewControllerViewController
         deliveryOrderDetailsVC.viewModel = DeliveryStatusViewModel()
         deliveryOrderDetailsVC.orderId = orderId
         self.navigationController?.pushViewController(deliveryOrderDetailsVC, animated: true)
        
        
    }
    
    func clickedOnRateOrder(_ orderId: String , selectedIndex: Int) {
        
        
        
        
       // bottomSheetController = BottomSheetController(configuration: configuration)
        let rateViewController = UIStoryboard.loadFeedBackViewController()
        if let orderViewModel = orderViewModelCategoryWise, let order = orderViewModel.orderList?[selectedIndex] {
            rateViewController.orderID = order.orderId
            rateViewController.transactionType = order.transaction_type ?? 1
            rateViewController.branchName = order.branchName
            rateViewController.brandlogo = order.brand_logo
            rateViewController.orderDate = order.createdOn
            rateViewController.deliveryThemeColor = order.backgroudColor
            rateViewController.deliveryThemeTextColor = order.textColor
            rateViewController.feedbackDelegate = self
            rateViewController.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: view.frame.width, height: 480)
           bottomSheetController?.present(rateViewController, on: self, viewHeight: 480)
        }
//        let  rateViewController = UIStoryboard.loadFeedBackViewController()
//        rateViewController.orderID = orderId
//
//        if let orderViewModel = orderViewModel, let order = orderViewModel.orderList?[selectedIndex] {
//            rateViewController.orderID = order.orderId
//            rateViewController.transactionType = order.transaction_type ?? 1
//            rateViewController.branchName = order.branchName
//            self.navigationController?.pushViewController(rateViewController, animated: true)
//
//        }
      
    }
}
extension OrderHistoryViewController :FeedbackDetailedDelegate {
    
    func actionOnDone(viewController : FeedbackDetailedViewController){
    
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
