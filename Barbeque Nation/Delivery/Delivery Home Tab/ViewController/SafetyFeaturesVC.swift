//
 //  Created by Mahmadsakir on 19/03/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified SafetyFeaturesVC.swift
 //

import UIKit

class SafetyFeaturesVC: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var bannerView: SafetyHygieneBannerView!
    @IBOutlet weak var summaryView: SafetyHygieneSummaryView!
    @IBOutlet weak var temperatureView: SafetyHygieneTemperatureChecksView!
    @IBOutlet weak var emergencyInfoView: SafetyHygieneEmergencyInfoView!
    @IBOutlet weak var lblLiner: UILabel!
    @IBOutlet weak var viewForTitle: UIView!
    
    var viewModel = SafetyHygieneViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        styleUI()
        fetchData()
    }
    
    func styleUI() {
        bannerView.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
        summaryView.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
        emergencyInfoView.viewForContact.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
        temperatureView.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
        emergencyInfoView.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
        
        bannerView.dropShadow()
        summaryView.dropShadow()
        emergencyInfoView.viewForContact.dropShadow()
        temperatureView.dropShadow()
        emergencyInfoView.dropShadow()
    }
    
    @IBAction func onClickBtnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func fetchData() {
        viewModel.getSafetyHygieneDetails {
            if let safety_and_hygiene_banner = self.viewModel.safetyHygieneResponse?.safety_and_hygiene_banner{
                self.bannerView.isHidden = false
                self.bannerView.lblTitle.text = safety_and_hygiene_banner.title
                self.bannerView.lblMessage.text = safety_and_hygiene_banner.description
                if let image = safety_and_hygiene_banner.image{
                    self.bannerView.imgView.setImage(url: image, isForPromotions: true)
                }
            }else{
                self.bannerView.isHidden = true
            }
            
            if let safety_and_hygiene_summary = self.viewModel.safetyHygieneResponse?.safety_and_hygiene_summary{
                self.summaryView.lblTitle.text = safety_and_hygiene_summary.title
                self.summaryView.arrData = safety_and_hygiene_summary.list_points
                self.summaryView.reloadData()
                self.summaryView.isHidden = false
            }else{
                self.summaryView.isHidden = true
            }
            
            if let safety_and_hygiene_emergency_info = self.viewModel.safetyHygieneResponse?.safety_and_hygiene_emergency_info{
                self.emergencyInfoView.lblTitle.text = safety_and_hygiene_emergency_info.title
                self.emergencyInfoView.lblMessage.text = safety_and_hygiene_emergency_info.description
                if let image = safety_and_hygiene_emergency_info.image{
                    self.emergencyInfoView.imgView.setImage(url: image, isForPromotions: true)
                }
                self.emergencyInfoView.arrData = safety_and_hygiene_emergency_info.list_points
                self.emergencyInfoView.reloadData()
                self.emergencyInfoView.isHidden = false
            }else{
                self.emergencyInfoView.isHidden = true
            }
            
            if let safety_and_hygiene_temperature_checks = self.viewModel.safetyHygieneResponse?.safety_and_hygiene_temperature_checks{
                self.temperatureView.lblTitle.text = safety_and_hygiene_temperature_checks.title
                self.temperatureView.lblMessage.text = safety_and_hygiene_temperature_checks.description
                if let image = safety_and_hygiene_temperature_checks.image{
                    self.temperatureView.imgView.setImage(url: image, isForPromotions: true)
                }
                self.temperatureView.isHidden = false
            }else{
                self.temperatureView.isHidden = true
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SafetyFeaturesVC: UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
            let offset = lblHeader.frame.size.height + 36
            if scrollView.contentOffset.y > offset{
                if lblTitle.isHidden{
                    UIView.animate(withDuration: 0.2) {
                        self.lblTitle.isHidden = false
                        self.lblLiner.isHidden = false
                    } completion: { (isCompleted) in
                        self.lblTitle.isHidden = false
                        self.lblLiner.isHidden = false
                    }
                }
            }else{
                if !lblTitle.isHidden{
                    UIView.animate(withDuration: 0.2) {
                        self.lblTitle.isHidden = true
                        self.lblLiner.isHidden = true
                    } completion: { (isCompleted) in
                        self.lblTitle.isHidden = true
                        self.lblLiner.isHidden = true
                    }
                }
            }
    }
}





class SafetyHygieneBannerView: UIView {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var imgView: UIImageView!
}
class SafetyHygieneSummaryView: UIView {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var heightForTableView: NSLayoutConstraint!
    var arrData = [String]()
    
    func reloadData() {
        tableView.reloadData {
            self.heightForTableView.constant = self.tableView.contentSize.height
        }
    }
}
class SafetyHygieneTemperatureChecksView: UIView {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var imgView: UIImageView!
}
class SafetyHygieneEmergencyInfoView: UIView {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var viewForContact: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var heightForTableView: NSLayoutConstraint!
    var arrData = [String]()
    
    func reloadData() {
        tableView.reloadData {
            self.heightForTableView.constant = self.tableView.contentSize.height
        }
    }
}

class SafetyHygieneSummaryCell: UITableViewCell {
    @IBOutlet weak var lblText: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

class SafetyHygieneEmergencyInfoCell: UITableViewCell {
    @IBOutlet weak var lblText: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

extension SafetyHygieneSummaryView: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SafetyHygieneSummaryCell", for: indexPath) as! SafetyHygieneSummaryCell
        cell.lblText.text = arrData[indexPath.row]
        return cell
    }
}

extension SafetyHygieneEmergencyInfoView: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SafetyHygieneEmergencyInfoCell", for: indexPath) as! SafetyHygieneEmergencyInfoCell
        cell.lblText.text = arrData[indexPath.row]
        return cell
    }
}
