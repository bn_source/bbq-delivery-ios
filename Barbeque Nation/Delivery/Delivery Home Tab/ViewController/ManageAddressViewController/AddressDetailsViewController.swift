//
 //  Created by Arpana on 18/04/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified AddressDetailsViewController.swift
 //

import UIKit

class AddressDetailsViewController: BaseViewController {

    @IBOutlet weak var lblAddressTitle: UILabel!
    @IBOutlet weak var lblAddressDesc: UILabel!
    @IBOutlet weak var txtFlatDetails: UITextField!
    @IBOutlet weak var txtLandamark: UITextField!
    @IBOutlet weak var txtAddressTag: UITextField!
    @IBOutlet weak var viewForHome: UIView!
    @IBOutlet weak var viewForWork: UIView!
    @IBOutlet weak var viewForOtherTag: UIView!
    
    @IBOutlet weak var otherLocationStackView: UIStackView!
    @IBOutlet weak var imgHome: UIImageView!
    @IBOutlet weak var lblHome: UILabel!
    @IBOutlet weak var imgWork: UIImageView!
    @IBOutlet weak var lblWork: UILabel!
    @IBOutlet weak var imgOther: UIImageView!
    @IBOutlet weak var lblOther: UILabel!
    @IBOutlet weak var btnSaveAddress: UIButton!
    
    var AddressTitleANdDesc: (String , String) = ("", "")
    var address: Address?

    //MARK:- Variable
    var delegate: AddressConfirmationViewDelegate?
    
    var selectedTag: Int? {
        didSet{
            enableSaveAddress()
            selectedTagtype = nil
            if let selectedTag = selectedTag{
                switch selectedTag {
                case 0:
                    selectedTagtype = .home
                    break
                    
                case 1:
                    selectedTagtype = .work
                    break
                    
                default:
                    selectedTagtype = .other
                }
            }
        }
    }
    
    var selectedTagtype: Address.TagType?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    func setData(){
        
        self.lblAddressTitle.text = AddressTitleANdDesc.0
        self.lblAddressDesc.text = AddressTitleANdDesc.1
        
        if let address = address{
            
           self.txtFlatDetails.text = address.flatNo
           self.txtLandamark.text = address.landMark
            self.txtAddressTag.text = address.tagName
            if address.tagType == .home{
                self.enableDisabledTagAction(index: 0)
                self.selectedTag = 0
            }else if address.tagType == .work{
                self.enableDisabledTagAction(index: 1)
                self.selectedTag = 1
            }else{
                self.enableDisabledTagAction(index: 2)
                self.selectedTag = 2
            }
        }
//                if isSelectedFromMap{
//                    AnalyticsHelper.shared.triggerEvent(type: .AE02A)
//                }else{
//                    AnalyticsHelper.shared.triggerEvent(type: .AE02B)
//                }
//                addressConfirmationView?.addToSuperView(view: self)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        styleUI()
        setData()
    }

    
    
    func styleUI() {
        viewForHome.setCornerRadius(8)
        viewForWork.setCornerRadius(8)
        viewForOtherTag.setCornerRadius(8)
        btnSaveAddress.alpha = 0.5
        btnSaveAddress.isEnabled = false
        btnSaveAddress.setCornerRadius(8)
        txtLandamark.setCornerRadius(12)
        txtAddressTag.setCornerRadius(12)
        txtFlatDetails.setCornerRadius(12)
        txtLandamark.addLeftView( UIView(frame: CGRect(x: 0, y: 0, width: 15, height: txtLandamark.frame.height)))
        txtFlatDetails.addLeftView( UIView(frame: CGRect(x: 0, y: 0, width: 15, height: txtFlatDetails.frame.height)))
        txtAddressTag.addLeftView( UIView(frame: CGRect(x: 0, y: 0, width: 15, height: txtAddressTag.frame.height)))

        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.hexStringToUIColor(hex: "#666666") ,
                            .font: UIFont(name:Constants.App.BBQAppFont.ThemeRegular, size: 16.0)]

        txtFlatDetails.attributedPlaceholder = NSAttributedString(string: "House no. / Flat no. / Floor / Building",
                                                                  attributes: attributes as [NSAttributedString.Key : Any])
        txtAddressTag.attributedPlaceholder = NSAttributedString(string: "Enter Location name",
                                                                 attributes: attributes as [NSAttributedString.Key : Any])
       txtLandamark.attributedPlaceholder = NSAttributedString(string: "Enter nearest land mark",
                                                               attributes: attributes as [NSAttributedString.Key : Any])


        
    }
    
    @IBAction func backButtonPressed(_ sender : UIButton){
                self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- Button Action Method
    @IBAction func onClickBtnConfirm(_ sender: Any) {
        var tagType = ""
        var tagName: String?
        if selectedTag == 0 {
            tagType = "home"
        }else if selectedTag == 1 {
            tagType = "work"
        }else if selectedTag == 2 {
            tagType = "other"
            tagName = txtAddressTag.text
        }
        if tagType == ""{
            return
        }
        delegate?.saveAddress(flat: txtFlatDetails.text ?? "", landmark: txtLandamark.text ?? "", tagType: tagType, tagName: tagName, type: selectedTagtype)
    }
    @IBAction func onClickBtnChange(_ sender: Any) {
        let vc = UIUtils.viewController(storyboardId: "DeliveryHome", vcId: "ChangeLocationViewController") as! ChangeLocationViewController
       //
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickBtnHome(_ sender: Any) {
        if let selectedTag = selectedTag, selectedTag == 0{
            return
        }
        enableDisabledTag(index: 0)
        selectedTag = 0
    }
    @IBAction func onClickBtnWork(_ sender: Any) {
        if let selectedTag = selectedTag, selectedTag == 1{
            return
        }
        enableDisabledTag(index: 1)
        selectedTag = 1

    }
    
    @IBAction func onClickBtnOtherTag(_ sender: Any) {
        
        lblOther.textAlignment = .center

        if let selectedTag = selectedTag, selectedTag == 2{
            enableDisabledTag(index: 3)
            self.selectedTag = nil
        }else{
            enableDisabledTag(index: 2)
            selectedTag = 2
        }
        
    }
    private func enableDisabledTag(index: Int){
        if selectedTag ?? 0 == 2 || index == 2 {
            UIView.animate(withDuration: 0.2) {
                self.enableDisabledTagAction(index: index)
            }
        }else{
            self.enableDisabledTagAction(index: index)
        }
        
        
    }

    
    
    @IBAction func onClickBtnClose(_ sender: Any) {
//        delegate?.onHiddenView(view: self)
//        removeToSuperView()
    }
    
    private func enableSaveAddress(){
        if (txtLandamark.text?.replacingOccurrences(of: " ", with: "").count ?? 0 == 0) || (txtFlatDetails.text?.replacingOccurrences(of: " ", with: "").count ?? 0 == 0) || selectedTag == nil || (selectedTag == 2 && txtAddressTag.text?.replacingOccurrences(of: " ", with: "").count ?? 0 == 0){
            btnSaveAddress.alpha = 0.5
            btnSaveAddress.isEnabled = false
        }else{
            btnSaveAddress.alpha = 1.0
            btnSaveAddress.isEnabled = true
        }
    }
    
    func enableDisabledTagAction(index: Int){
        enableDisabledTag(isEnable: false, view: viewForHome, imgView: imgHome, label: lblHome, index: 0)
        enableDisabledTag(isEnable: false, view: viewForWork, imgView: imgWork, label: lblWork, index: 1)
        enableDisabledTag(isEnable: false, view: viewForOtherTag, imgView: imgOther, label: lblOther, index: 2)
        otherLocationStackView.isHidden = true
       // lblExtraTagSpace.isHidden = false
        viewForHome.isHidden = false
        viewForWork.isHidden = false
        lblOther.textAlignment = .center

        switch index {
        case 0:
            enableDisabledTag(isEnable: true, view: viewForHome, imgView: imgHome, label: lblHome, index: 0)
            break
        case 1:
            enableDisabledTag(isEnable: true, view: viewForWork, imgView: imgWork, label: lblWork, index: 1)
            break
        case 2:
            enableDisabledTag(isEnable: true, view: viewForOtherTag, imgView: imgOther, label: lblOther, index: 2)
            viewForWork.isHidden = true
            viewForHome.isHidden = true
            otherLocationStackView.isHidden = false
            lblOther.textAlignment = .left
           // lblExtraTagSpace.isHidden = true
            break
        default: break
            
        }
    }
    
    
    private func enableDisabledTag(isEnable: Bool, view: UIView, imgView: UIImageView, label: UILabel, index: Int){
        if isEnable{
            view.backgroundColor = UIColor.hexStringToUIColor(hex: "FEF1E4")
            view.layer.borderColor = UIColor.hexStringToUIColor(hex: "F04B24").cgColor
            label.textColor = UIColor.hexStringToUIColor(hex: "#F04B24")
            view.layer.borderWidth = 1.0
            //Selected Images
            if index == 0{
                imgView.image = UIImage(named: "HomeTagSelected")
            }else if index == 1{
                imgView.image = UIImage(named: "WorkTag")
            }else if index == 2{
                imgView.image = UIImage(named: "LocationOrange")
            }
        }else{
            view.backgroundColor =  UIColor.hexStringToUIColor(hex: "F5F5F5")
            view.layer.borderColor = UIColor.text.cgColor
            label.textColor =   UIColor.hexStringToUIColor(hex: "666666")
            view.layer.borderWidth = 0.0
            //Unselected images
            if index == 0{
                imgView.image = UIImage(named: "HomeTagUnselected")
            }else if index == 1{
                imgView.image = UIImage(named: "WorkTagUnselected")
            }else if index == 2{
                imgView.image = UIImage(named: "LocationGray")
            }
        }
        
        
    }
    
    //MARK:- On Editing Of Textfield
    @IBAction func onEditingFlatDetails(_ sender: Any) {
        enableSaveAddress()
    }
    @IBAction func onEditingLandmark(_ sender: Any) {
        enableSaveAddress()
    }
    @IBAction func onEditingOtherTag(_ sender: Any) {
        enableSaveAddress()
    }
    
}

extension AddressDetailsViewController :AddressSearchPlaceViewDelegate{
    func onViewHidden(view: AddressSearchPlaceView) {
       //addressSearchPlaceView = nil
    }
    
    func onSelectMyLocation() {
//        LocationManager.sharedInstance.delegate = self
//        LocationManager.sharedInstance.initLocationManager()
//      //  addressSearchPlaceView = nil
//        isSelectedFromMap = false
    }
    
    func onSelectLocation(googlePlaceRes: GooglePlaceRes) {
           
    }
    
    func onStartSearching(text: String) {
     
    }
    func locationDisabled() {
    }
}
