//
//  SearchLocationViewController.swift
//  BBQ
//
//  Created by Dharani Sadasivam on 16/08/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import UIKit
import GooglePlaces
import PopMenu

class ManageAddressViewController: BaseViewController {
    
    @IBOutlet weak var addressTableView: UITableView!
    var addressViewModel = AddressViewModel()
    var selectedBranchId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AnalyticsHelper.shared.triggerEvent(type: .MA01)
        styleUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.screenName(name: .Manage_Address_Screen)
        getAddress()
    }
    
    private func styleUI() {
        addressTableView.tableFooterView = UIView()
        addressTableView.alwaysBounceVertical = false
        addressTableView.estimatedRowHeight = UITableView.automaticDimension
        
        addressTableView.contentInset = .zero

    }
    
    private func getAddress() {
        UIUtils.showLoader()
        addressViewModel.getSavedAddress(customerId: BBQUserDefaults.sharedInstance.customerId,
                                         branchId: BBQUserDefaults.sharedInstance.DeliveryBranchIdValue, filterWithID: nil) {
            self.addressTableView.reloadData()
            UIUtils.hideLoader()
        }
    }
    
    func deleteAddress(address: Address) {
        UIUtils.showLoader()
        BBQServiceManager.getInstance().DeleteCustomerAddress(params: getParams(address_id: address.addressId)) { (error, response) in
            UIUtils.hideLoader()
            if let response = response {
                if response.message_type == "failed" {
                    UIUtils.showToast(message: response.message)
                    AnalyticsHelper.shared.triggerEvent(type: .MA04B)
                } else {
                    AnalyticsHelper.shared.triggerEvent(type: .MA04A)
                    self.getAddress()
                }
            } else {
                AnalyticsHelper.shared.triggerEvent(type: .MA04B)
                UIUtils.showToast(message: error?.message ?? UNEXPECTED_ERROR)
            }


        }
    }
    
    private func getParams(address_id: String) -> [String: Any]{
        return ["address_id": address_id,"customer_id": BBQUserDefaults.sharedInstance.customerId]
    }
    
    @IBAction func onClickBtnAddAddress(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .MA02)
        let vc = UIUtils.viewController(storyboardId: "DeliveryCart", vcId: "AddAddressVC")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func backButtonPressed(_ sender : UIButton){
                self.navigationController?.popViewController(animated: true)
    }
    
}


extension ManageAddressViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.addressViewModel.addresses?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ManageAddressViewCell", for: indexPath) as! ManageAddressViewCell
        if let address = self.addressViewModel.addresses?[indexPath.row]{
            cell.setCellData(address: address)
            cell.btnOption.tag = indexPath.row
            cell.btnOption.addTarget(self, action: #selector(didTapBtnOptionMore(sender:)), for: .touchUpInside)
            cell.btnOption.addTarget(self, action: #selector(didTapBtnOptionMore(sender:)), for: .touchUpOutside)
        }else{
            return UITableViewCell()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    @objc func didTapBtnOptionMore(sender: UIButton) {
        
        var actions: [(String, UIAlertAction.Style)] = []
        actions.append(("Edit Location", UIAlertAction.Style.default))
        actions.append(("Remove Location", UIAlertAction.Style.destructive))
        actions.append(("Cancel" , UIAlertAction.Style.cancel))
        
             //self = ViewController
            Alerts.showActionsheet(viewController: self, title: "", message: "", actions: actions) { (index) in
                print("call action \(index)")
                
                switch index{
                    
                case 0:
                    AnalyticsHelper.shared.triggerEvent(type: .MA03)
                    let vc = UIUtils.viewController(storyboardId: "DeliveryCart", vcId: "AddAddressVC") as! AddAddressVC
                    vc.address = self.addressViewModel.addresses?[sender.tag]
                    vc.isSelectedFromList = true
                    self.navigationController?.pushViewController(vc, animated: true)

                break
                    
                case 1:
               
                    AnalyticsHelper.shared.triggerEvent(type: .MA04)
                    if let address = self.addressViewModel.addresses?[sender.tag]{
                        
                        let alert = UIAlertController(title: "Remove Location", message: "Are you sure that you want to remove the location?", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Remove", style: .default, handler: { action in
                            
                            self.deleteAddress(address: address)

                        }))
                        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                            
                            
                        }))
                        alert.setMessage(font: UIFont(name: Constants.App.BBQAppFont.ThemeLight,  size: 14.0), color: UIColor.hexStringToUIColor(hex: "#666666"))
                        alert.setTitle(font:  UIFont(name: Constants.App.BBQAppFont.ThemeMedium,  size: 16.0), color: UIColor.hexStringToUIColor(hex: "#2B3849"))
                        alert.setTint(color: UIColor.hexStringToUIColor(hex: "#F04B24"))
                        self.present(alert, animated: true, completion: nil)

                    }
                    break
                    
                default:
                    print("wrong index")
                }
                /*
                 results
                 call action 0
                 call action 1
                 call action 2
                 */
            }
        
        /*
        
        var actions = [PopMenuAction]()
        actions.append(PopMenuDefaultAction(title: "Edit", image: UIImage(named: "icon_menu_pencil"), color: .theme, didSelect: { (select) in
            AnalyticsHelper.shared.triggerEvent(type: .MA03)
            let vc = UIUtils.viewController(storyboardId: "DeliveryCart", vcId: "AddAddressVC") as! AddAddressVC
            vc.address = self.addressViewModel.addresses?[sender.tag]
            vc.isSelectedFromList = true
            self.navigationController?.pushViewController(vc, animated: true)
        }))
        actions.append(PopMenuDefaultAction(title: "Delete", image: UIImage(named: "icon_menu_trash"), color: .theme, didSelect: { (select) in
            AnalyticsHelper.shared.triggerEvent(type: .MA04)
            if let address = self.addressViewModel.addresses?[sender.tag]{
                
                var dialogueViewController : PopupDialog?
                if dialogueViewController == nil {
                    dialogueViewController =  PopupHandler.showTwoButtonsPopupWithReturnController(title: kAddressRemoveAlert,
                                                                                                   message: kMyAddressRemoveDesc,
                                                                                                   on: self,
                                                                                                   firstButtonTitle: kCancelButton, firstAction: {
                                                                                                    if dialogueViewController != nil {
                                                                                                        if self.view.subviews.contains(dialogueViewController!.view) {
                                                                                                            dialogueViewController!.view.removeFromSuperview()
                                                                                                        }
                                                                                                        
                                                                                                    }
                                                                                                   },
                                                                                                   secondButtonTitle: kOkButton) {
                        if dialogueViewController != nil {
                            if self.view.subviews.contains(dialogueViewController!.view) {
                                dialogueViewController!.view.removeFromSuperview()
                            }
                            
                        }
                        self.deleteAddress(address: address)
                        
                        
                    }
                }
                self.view.addSubview(dialogueViewController?.view! ?? UIView())
                
                
            }
        }))
        let menuAppearance = PopMenuAppearance()
        menuAppearance.popMenuColor = PopMenuColor.configure(background: PopMenuActionBackgroundColor.solid(fill: UIColor.white), action: PopMenuActionColor.tint(UIColor.theme))
        menuAppearance.popMenuBackgroundStyle = .dimmed(color: UIColor.black, opacity: 0.05)
        menuAppearance.popMenuPresentationStyle = .near(.bottom)
        let popMenuVC = PopMenuViewController(sourceView: sender, actions: actions, appearance: menuAppearance)
        present(popMenuVC, animated: true, completion: nil)*/
    }
}

