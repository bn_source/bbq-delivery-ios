//
 //  Created by Arpana on 17/04/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified ChangeLocationViewController.swift
 //

import UIKit
import GoogleMaps
import SwiftyJSON
import GooglePlaces

class ChangeLocationViewController: UIViewController  {

    public var longitude:Double = 77.38066792488098
    public var latitude:Double = 28.6517752463408
    var GoogleMapView:GMSMapView!
    var geoCoder :CLGeocoder!
    private var placesClient: GMSPlacesClient!
    var googlePlaces: [GooglePlaceRes] = []
    var addressViewModel = AddressViewModel()
    var locationManager = CLLocationManager()
    var isSelectedFromList = false
    var timer: Timer?
    var searchText = ""
   @IBOutlet weak var  addressSearchPlaceView: UIView!
    var addressConfirmationView: AddressConfirmationView?
   // var address: Address?
    var address = [GooglePlaceRes]()
    var mapSelectedAddress: String = ""
    var isSelectedFromMap = true
    var delegate: AddressSearchPlaceViewDelegate?
    
    //MARK:- Outlets
    @IBOutlet weak var viewForSearchView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var stackViewForContainer: UIStackView!
    @IBOutlet weak var stackViewForSpacing: UIView!
    
    @IBOutlet weak var locateOnMapView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (locationManager.location?.coordinate.latitude ?? BBQUserDefaults.sharedInstance.currentLatitudeValue) != 0{
            latitude = locationManager.location?.coordinate.latitude ?? BBQUserDefaults.sharedInstance.currentLatitudeValue
        }
        if (locationManager.location?.coordinate.longitude ?? BBQUserDefaults.sharedInstance.currentLongitudeValue) != 0{
            longitude = locationManager.location?.coordinate.longitude ?? BBQUserDefaults.sharedInstance.currentLongitudeValue
        }
        
        
        geoCoder = CLGeocoder()
       
        placesClient = GMSPlacesClient.shared()
        LocationManager.sharedInstance.delegate = self
//        if let latitude = address?.latitude, let value =  Double(latitude), let longitude = address?.longitude, let value1 = Double(longitude){
//            self.latitude = value
//            self.longitude = value1
//            self.isSelectedFromList = true
//           // lblAddress.text = address?.address.components(separatedBy: ",").first
//           // lblFullAddress.text = address?.address
//           // mapSelectedAddress = address?.address ?? ""
//           // SetUpMap(latitude: value, longitude: value1)
//        }else{
            LocationManager.sharedInstance.initLocationManager()
       // }

        styleUI()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    func styleUI() {
     //   viewForContainer.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        tableView.register(UINib(nibName: "AddressSearchPlaceCell", bundle: nil), forCellReuseIdentifier: "AddressSearchPlaceCell")
        tableView.delegate = self
        tableView.dataSource = self
        
        locateOnMapView.setCornerRadius(12)
        viewForSearchView.setCornerRadius(12)
        
        txtSearch.delegate = self
        
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        button.setImage(#imageLiteral(resourceName: "CurrentLocationGray"), for: .normal)
        button.addTarget(self, action: #selector(onClickBtnMyLocation), for: .touchUpInside)

        txtSearch.addRightView(button)
        txtSearch.addLeftView( UIView(frame: CGRect(x: 0, y: 0, width: 15, height: txtSearch.frame.height)))
        txtSearch.leftViewMode = .always
        
        txtSearch.delegate = self
    }
    
    @IBAction func backButtonPressed(_ sender : UIButton){
                self.navigationController?.popViewController(animated: true)
    }
    
        // Do any additional setup after loading the view.
        
        //MARK:- Button Actions
       @objc func onClickBtnMyLocation() {
            delegate?.onSelectMyLocation()
           self.navigationController?.popViewController(animated: true)

        }
    

        @IBAction func onEditingOfSearch(_ sender: Any) {
            onStartSearching(text: txtSearch.text ?? "")
            
            
        }
    
    
    @IBAction func selectLocationOnMapBtnClicked(_ sender: Any) {
        
        
        if let viewControllers = self.navigationController?.viewControllers {
            
            for vc: UIViewController? in viewControllers {
                
                if (vc is AddAddressVC) {
                    if let aVc = vc {
                        //View is present in navigation stack
                        self.navigationController?.popToViewController(aVc, animated: false)
                        return
                    }
                }
                
            }
            let vc = UIUtils.viewController(storyboardId: "DeliveryCart", vcId: "AddAddressVC")
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
//    func SetUpMap(latitude:Double, longitude: Double) {
//            let camera = GMSCameraPosition.camera(withLatitude:latitude, longitude: longitude, zoom: 18)
//        GoogleMapView = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.mapView.frame.height), camera: camera)
//            GoogleMapView.delegate = self
//        GoogleMapView.settings.allowScrollGesturesDuringRotateOrZoom = true
//        GoogleMapView.settings.compassButton = true
//        GoogleMapView.settings.zoomGestures = true
//        GoogleMapView.settings.scrollGestures = true
//        GoogleMapView.mapType = .terrain
//            self.mapView.addSubview(GoogleMapView)
//            self.mapView.bringSubviewToFront(pinImage)
//        }



extension ChangeLocationViewController : LocationManagerDelegate {
    
    func tracingLocation(location: Location) {
       // SetUpMap(latitude: Double(location.latitude) ?? 0.00, longitude: Double(location.longitude) ?? 0.00)
    }
    
    func tracingFailureInLocation(error: NSError) {
        self.alert(message: error.localizedDescription, title: "Location update failed")
    }
    
}

extension ChangeLocationViewController:   AddressSearchPlaceViewDelegate {
    func onViewHidden(view: AddressSearchPlaceView) {
        addressSearchPlaceView = nil
    }
    
    func onSelectMyLocation() {
        LocationManager.sharedInstance.delegate = self
        LocationManager.sharedInstance.initLocationManager()
        addressSearchPlaceView = nil
        isSelectedFromMap = false
    }
    
    func onSelectLocation(googlePlaceRes: GooglePlaceRes) {
        self.addressViewModel.getPlaceLatLong(googlePlaceRes.placeId, completionHandler: { (place, error) in
            guard let location = place else { return }
            let lat = Double(location.latitude)
            let lon = Double(location.longitude)
            let place = CLLocationCoordinate2D(latitude:lat!
                                                  , longitude:lon!)
//
//            self.lblAddress.text = googlePlaceRes.placeName.string.components(separatedBy: ",").first
//            self.lblFullAddress.text = googlePlaceRes.placeName.string
//            self.mapSelectedAddress = googlePlaceRes.placeName.string
//            self.addressConfirmationView?.lblAddressTitle.text = googlePlaceRes.placeName.string.components(separatedBy: ",").first
//            self.addressConfirmationView?.lblAddressDesc.text = googlePlaceRes.placeName.string
//            self.isSelectedFromList = true
//            self.cameraMoveToLocation(toLocation: place)
        })
        isSelectedFromMap = false
        addressSearchPlaceView = nil
    }
    
    func onStartSearching(text: String) {
        if let timer = timer{
            timer.invalidate()
        }
        
        timer = nil
        searchText = text
        if text.count >= 3{
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(searchTextFieldStr), userInfo: nil, repeats: false)
        }
    }
    func locationDisabled() {
     //   SetUpMap(latitude: 12.9716, longitude: 77.5946)
    }
    @objc private func searchTextFieldStr(){
        if searchText.count >= 3{
            searchPlaces(searchText)
        }
    }
    @objc func searchPlaces(_ searchStr: String) {
            AnalyticsHelper.shared.triggerEvent(type: .delivery_address_search_button_clicked)
            guard searchStr.count >= 3 else {
                return
            }
            let filter = GMSAutocompleteFilter()
            filter.country = "IN"
            let location = CLLocationManager().location ?? CLLocation(latitude: BBQUserDefaults.sharedInstance.currentLatitudeValue, longitude: BBQUserDefaults.sharedInstance.currentLongitudeValue)
            let bounds = GMSCoordinateBounds(coordinate: location.coordinate, coordinate: CLLocationCoordinate2D(latitude: BBQUserDefaults.sharedInstance.currentLatitudeValue, longitude: BBQUserDefaults.sharedInstance.currentLongitudeValue))
            filter.locationBias = GMSPlaceRectangularLocationOption(bounds.northEast,  bounds.southWest)
            filter.origin = location
            AnalyticsHelper.shared.triggerEvent(type: .address_places_api_called)
            filter.type = .establishment
            
            //AppDelegate.shared().gmsSessionToken
            placesClient.findAutocompletePredictions(fromQuery: searchStr, filter: filter,  sessionToken: nil) { (results, error) in
                if let error = error {
                    print("Autocomplete error: \(error)")
                    return
                }
                if let results = results {
                    self.googlePlaces.removeAll()
                    for result in results {
                        self.googlePlaces.append(GooglePlaceRes(placeName: result.attributedFullText, placeId: result.placeID))
                    }
                }
                self.address = self.googlePlaces
                self.tableView.reloadData()
                self.view.setNeedsLayout()
                self.view.layoutIfNeeded()
            }
        }
    
}


extension ChangeLocationViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
    
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return "Other Locations"
//    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0,
                                                  y: 0,
                                                  width: self.tableView.frame.width,
                                                  height: 50))
        
        let label =  UILabel.init(frame: headerView.frame)
        label.text = "Other Locations"
        label.textColor = UIColor.hexStringToUIColor(hex: "#666666")
        label.font =  UIFont(name: Constants.App.BBQAppFont.ThemeRegular, size: 14.0)
        headerView.addSubview(label)
        return headerView
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return address.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressSearchPlaceCell", for: indexPath) as! AddressSearchPlaceCell
        cell.setCellData(address: address[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //select address and take it to previous view controller
        
        if delegate != nil {
            
            delegate?.onSelectLocation(googlePlaceRes: address[indexPath.row])
            self.navigationController?.popViewController(animated: true)
            
        }else {
            if let viewControllers = self.navigationController?.viewControllers {
                
                for vc: UIViewController? in viewControllers {
                    
                    if (vc is AddAddressVC) {
                        if vc != nil {
                            //View is present in navigation stack
                            (vc as? AddAddressVC)?.onSelectLocation(googlePlaceRes: address[indexPath.row])
                            self.navigationController?.popToViewController(vc!, animated: true)
                            return
                        }
                    }
                    
                }
                
                let vc = UIUtils.viewController(storyboardId: "DeliveryCart", vcId: "AddAddressVC") as? AddAddressVC
                vc?.onSelectLocation(googlePlaceRes: address[indexPath.row])
                self.navigationController?.pushViewController(vc!, animated: true)
                
            }
        }
        
        
        

       // removeToSuperView()
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        txtSearch.resignFirstResponder()
    }
}

extension ChangeLocationViewController: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        //Changes by Arpana
        //stackViewForSpacing height was fixed 150, issue on SE
        //Chaged to proportional height, to fix the bug
        UIView.animate(withDuration: 0.2) {
            self.stackViewForSpacing.isHidden = false
        } completion: { (isCompleted) in
            self.stackViewForSpacing.isHidden = false
        }

        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.2) {
            self.stackViewForSpacing.isHidden = true
        } completion: { (isCompleted) in
            self.stackViewForSpacing.isHidden = true
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
}
