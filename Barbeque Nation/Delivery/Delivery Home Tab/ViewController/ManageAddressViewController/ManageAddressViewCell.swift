//
 //  Created by Mahmadsakir on 13/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified ManageAddressViewCell.swift
 //

import UIKit

class ManageAddressViewCell: UITableViewCell {
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblTag: UILabel!
    @IBOutlet weak var btnOption: UIButton!
    @IBOutlet weak var mainView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setCellData(address: Address){
        lblAddress.text = String(format: "%@, %@", address.flatNo, address.address)
        if address.tagType == .home{
            lblTag.text = "Home"
            imgIcon.image = UIImage(named: "HomeAddress")
        }else if address.tagType == .work{
            lblTag.text = "Work"
            imgIcon.image = UIImage(named: "OfficeAddress")
        }else{
            imgIcon.image = UIImage(named: "OfficeSecond")
            if address.tagName != ""{
                lblTag.text = address.tagName
            }else{
                lblTag.text = "Other"
            }
        }
    }
    override func layoutSubviews() {
        super.layoutSubviews()
  
       // contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
        //contentView.setShadow()UIColor.hexStringToUIColor(hex: "18191F")
        mainView.setCornerRadius(12)
        mainView.setShadow(color: UIColor.hexStringToUIColor(hex: "55606D1A") , opacity: 0.2, offSet: .zero, radius: 7, scale: true)
        
    }
}
