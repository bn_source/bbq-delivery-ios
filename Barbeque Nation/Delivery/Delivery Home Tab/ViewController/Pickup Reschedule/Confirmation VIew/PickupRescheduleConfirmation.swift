//
 //  Created by Mahmadsakir on 29/12/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified PickupRescheduleConfirmation.swift
 //

import UIKit


class PickupRescheduleConfirmation: UIView {
    
    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var backgroundAlpha: UIView!
    @IBOutlet weak var lblOrderId: UILabel!
    @IBOutlet weak var lblPickupTime: UILabel!
    @IBOutlet weak var btnCanel: UIButton!
    @IBOutlet weak var btnReschedule: UIButton!
    
    var slot: String = ""
    var delegate: PickupRescheduleConfirmationDelegate?
    var backColor = UIColor.deliveryThemeColor
    var textColor = UIColor.deliveryThemeTextColor
    
    
    //MARK:- View Configuration
    func initWith(delegate: PickupRescheduleConfirmationDelegate) -> PickupRescheduleConfirmation {
        let view = loadFromNib()!
        view.styleUI()
        view.delegate = delegate
        return view
    }
        
        
    func loadFromNib() -> PickupRescheduleConfirmation? {
        if let views = Bundle.main.loadNibNamed("PickupRescheduleConfirmation", owner: self, options: nil), let view = views[0] as? PickupRescheduleConfirmation{
            view.frame = UIApplication.shared.windows[0].bounds
            return view
        }
        return nil
    }
    
    
    func addToSuperView(view: UIViewController?) {
        lblPickupTime.attributedText = slot.getTakeAwayString()?.0
        if let view = view{
            view.view.addSubview(self)
        }else{
            getMainView().addSubview(self)
        }
        self.backgroundAlpha.alpha = 0.0
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
            self.backgroundAlpha.alpha = 0.5
        } completion: { (isCompleted) in
            self.backgroundAlpha.alpha = 0.5
        }
    }
    
    func removeToSuperView() {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
            self.backgroundAlpha.alpha = 0.0
        } completion: { (isCompleted) in
            self.backgroundAlpha.alpha = 0.0
            self.removeFromSuperview()
        }
    }
    
    func styleUI() {
        viewForContainer.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0)
        btnReschedule.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0)
        btnCanel.roundCorners(cornerRadius: 5.0, borderColor: backColor, borderWidth: 1.0)
        lblOrderId.textColor = backColor
        btnCanel.setTitleColor(backColor, for: .normal)
        btnReschedule.setTitleColor(textColor, for: .normal)
        btnReschedule.backgroundColor = backColor
    }
    
    @IBAction func onClickOnBackground(_ sender: Any) {
        removeToSuperView()
        self.delegate?.cancel(view: self)
    }
    
    @IBAction func onClickBtnReschedule(_ sender: Any) {
        removeToSuperView()
        self.delegate?.confirm(view: self, slot: slot)
    }
}

protocol PickupRescheduleConfirmationDelegate {
    func cancel(view: PickupRescheduleConfirmation)
    func confirm(view: PickupRescheduleConfirmation, slot: String)
}
