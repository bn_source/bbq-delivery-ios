//
 //  Created by Mahmadsakir on 29/12/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified PickupRescheduleConfirmation.swift
 //

import UIKit


class PickupRescheduleSuccess: UIView {
    
    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var backgroundAlpha: UIView!
    
    
    
    
    //MARK:- View Configuration
    func initWith() -> PickupRescheduleSuccess {
        let view = loadFromNib()!
        view.styleUI()
        return view
    }
        
        
    func loadFromNib() -> PickupRescheduleSuccess? {
        if let views = Bundle.main.loadNibNamed("PickupRescheduleSuccess", owner: self, options: nil), let view = views[0] as? PickupRescheduleSuccess{
            view.frame = UIApplication.shared.windows[0].bounds
            return view
        }
        return nil
    }
    
    
    func addToSuperView(view: UIViewController?) {
        if let view = view{
            view.view.addSubview(self)
        }else{
            getMainView().addSubview(self)
        }
        self.backgroundAlpha.alpha = 0.0
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
            self.backgroundAlpha.alpha = 0.5
        } completion: { (isCompleted) in
            self.backgroundAlpha.alpha = 0.5
        }
    }
    
    func removeToSuperView() {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
            self.backgroundAlpha.alpha = 0.0
        } completion: { (isCompleted) in
            self.backgroundAlpha.alpha = 0.0
            self.removeFromSuperview()
        }
    }
    
    func styleUI() {
        viewForContainer.roundCorners(cornerRadius: 20.0, borderColor: .clear, borderWidth: 0)
    }
    
    @IBAction func onClickOnBackground(_ sender: Any) {
        removeToSuperView()
    }
}
