//
//  PopupMenuViewController.swift
//  BBQ
//
//  Created by Dharani Sadasivam on 31/08/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import UIKit
protocol PopupMenuDelegate: AnyObject {
    func dismissPopupMenu()
    func actionOnDone(_ item: Item, indexPath: IndexPath?)
}


class PopupMenuViewController: UIViewController {
    
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var foodName: UILabel!
    @IBOutlet weak var foodTypeIcon: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var closeBtn: UIButton!
    weak var delegate: PopupMenuDelegate?
    var popupMenuViewModel: MenuViewModel?
    var updatedItem: Item?
    var indexPath: IndexPath?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let item = updatedItem {
            popupMenuViewModel = MenuViewModel(item)
        }
        let gesture = UIPanGestureRecognizer.init(target: self, action: #selector(self.panGesture))
        gesture.delegate = self
        view.addGestureRecognizer(gesture)
        tableView.tableFooterView = UIView(frame: .zero)
        setupViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // prepareBackgroundView()
    }
    
    @IBAction func actionOnAddBtn(_ sender: Any) {
        guard let item = updatedItem, let viewModel = popupMenuViewModel else { return }
        item.customisations = viewModel.getCustomisations()
        item.quantity += 1
        self.delegate?.actionOnDone(item, indexPath: indexPath)
    }
    
    @IBAction func actionOnCloseBtn(_ sender: Any) {
        popupMenuViewModel?.removeAllSelectedItems()
        self.delegate?.dismissPopupMenu()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.6, animations: { [weak self] in
            let frame = self?.view.frame
            self?.view.frame = CGRect(x: 0, y: (frame?.origin.y)! + 40, width: frame!.width, height: frame!.height - 40)
            self?.view.layoutIfNeeded()
        })
    }
    
    @objc func panGesture(_ recognizer: UIPanGestureRecognizer) {
        
    }
    
    func setupViews() {
        view.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        addBtn.setCornerRadius(5.0)
        addBtn.setTitle("Add to cart", for: .normal)
        tableView.reloadData()
        fillUI()
    }
    
    func fillUI() {
        foodName.text = popupMenuViewModel?.item?.name ?? ""
        foodTypeIcon.image = popupMenuViewModel?.item?.foodType == FoodType.veg ? #imageLiteral(resourceName: "Group 846") : #imageLiteral(resourceName: "Group 845")
        AddButtonEnable()
    }
}


extension PopupMenuViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let viewModel = popupMenuViewModel else {
            return 0
        }
        return viewModel.getCustomisations().count
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let viewModel = popupMenuViewModel else {
            return 0
        }
        return viewModel.getMenuItemsAt(section).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuItemSelectionTableViewCell", for: indexPath) as! MenuItemSelectionTableViewCell
        guard let viewModel = popupMenuViewModel else {
            return cell
        }
        cell.section = indexPath.section
        cell.row = indexPath.row
        cell.viewModel = viewModel
        cell.menuItemSelectionDelegate = self
        cell.updateCell(viewModel.getMenuItemsAt(indexPath.section)[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let nsAttributedString = NSMutableAttributedString(string: popupMenuViewModel?.getMenuTitleAt(section) ?? "", attributes: [NSAttributedString.Key.font: UIFont.systemFontOfSize(size: 16.0, weight: .semibold), NSAttributedString.Key.foregroundColor: UIColor(named: "TextColor") as Any])
        //        if (viewModel?.item?.quantity ?? 0) > 0 {
        let subTitleString = NSAttributedString(string: " (\(popupMenuViewModel?.getMenuDescAt(section) ?? ""))", attributes: [NSAttributedString.Key.font: UIFont.systemFontOfSize(size: 12.0, weight: .semibold), NSAttributedString.Key.foregroundColor: UIColor(named: "TextColor")!])
        nsAttributedString.append(subTitleString)
        let label = UILabel()
        label.attributedText = nsAttributedString
        return label
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        actionOnMenuSelection(indexPath.row, section: indexPath.section)
    }
    private func AddButtonEnable(){
        guard let viewModel = popupMenuViewModel else { return }
        if viewModel.getTotalThreshold() <= viewModel.getAllSelectedItemsWithOutAddOn().count{
            addBtn.isEnabled = true
            addBtn.alpha = 1.0
        }else{
            addBtn.isEnabled = false
            addBtn.alpha = 0.5
        }
    }
}

extension PopupMenuViewController: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    
}

extension PopupMenuViewController: MenuItemSelectionTableViewCellDelegate {
    func actionOnMenuSelection(_ row: Int, section: Int) {
        guard let viewModel = popupMenuViewModel else { return }
        let customisation = viewModel.getCustomisationsAt(section)
        let item = viewModel.getMenuItemsAt(section)[row]
        if customisation.selectedItems.contains(where: {$0.id == item.id}) {
            viewModel.removeSelectedItem(section, item: item)
        } else if viewModel.canAddItem(section) {
            viewModel.addSelectedItem(section, item: item)
        } else {
            UIUtils.showToast(message: "Remove selected items and add new items")
        }
        self.AddButtonEnable()
        tableView.reloadData()
    }
}
