//
 //  Created by Arpana Rani on 15/03/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQReorderDetailsViewController.swift
 //

import UIKit
import AMPopTip

class BBQReorderDetailsViewController: BaseViewController {

    @IBOutlet weak var tableView:  UITableView!
    var orderIdVal: String?
    var orderViewModel: OrderHistoryViewModel?
    var orderDetails: DeliveryStatusResponse? //Order?
    var popTip = PopTip()
    
    @IBOutlet weak var totalLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerTableViewCell()
        orderDetails?.data?.addTotalInBreakUp()
        //self.homeAddressView.isHidden = orderDetail.deliveryAddress != "" ? false : true
        self.tableView.reloadData()
        {
            
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            
        }
        popTip.bubbleColor = .white
        popTip.cornerRadius = 5.0
        popTip.shouldShowMask = true
        
     //   self.totalLabel.text =  (String((getCurrency())\(orderDetails?.data?.bill_total ?? "0")"
        // }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
    }

    func registerTableViewCell(){
        
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        tableView.sectionHeaderHeight = 70
        tableView.separatorStyle = .none
        tableView.register(UINib(nibName: "BBQItemDetailUITableViewCell", bundle: nil), forCellReuseIdentifier: "BBQItemDetailUITableViewCell")
        tableView.register(UINib(nibName: "BBQTaxTableViewCell", bundle: nil), forCellReuseIdentifier: "BBQTaxTableViewCell")
    }
    
    @IBAction func backBtnPressed(_ sender : UIButton){
        
        self.navigationController?.popViewController(animated: true)
    }

}

extension BBQReorderDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (orderDetails?.data?.lst_items?.count ?? 0) + (orderDetails?.data?.lst_bill_details?.count ?? 0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print(indexPath.row)
        if indexPath.row >= ((orderDetails?.data?.lst_items?.count ?? 0)), let data = orderDetails?.data?.lst_bill_details?[indexPath.row - (orderDetails?.data?.lst_items?.count ?? 0)] {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "BBQTaxTableViewCell", for: indexPath) as? BBQTaxTableViewCell else { return UITableViewCell() }
            cell.setCellData(taxBreakUp: data, index: indexPath.row - (orderDetails?.data?.lst_items?.count ?? 0), isLastCell: (indexPath.row - (orderDetails?.data?.lst_items?.count ?? 0) + 1 == (orderDetails?.data?.lst_bill_details?.count ?? 0) ? true : false))
            cell.delegate = self
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "BBQItemDetailUITableViewCell", for: indexPath) as? BBQItemDetailUITableViewCell else { return UITableViewCell() }
            if let item = orderDetails?.data?.lst_items?[indexPath.row] {
                cell.itemNmLbl.text = item.item_name
                cell.itemAmountLbl.text = "\(getCurrency())\(item.total ?? "0")  "
                cell.itemTypeImgView.image = item.food_type == "0" ? #imageLiteral(resourceName: "Group 846") : #imageLiteral(resourceName: "Group 845")
              //  cell.setupItem(item)
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row >= ((orderDetails?.data?.lst_items?.count ?? 0)) {
            return 30
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "ItemTableViewCell") as? ItemTableViewCell, let item = orderDetails?.data?.lst_items?[indexPath.row] {
                cell.itemAmountLbl.text = "\(String(describing: item.amount))"
                cell.itemNmLbl.text = "\(String(describing: item.item_name))   x \(item.quantity ?? "0")"
                cell.layoutIfNeeded()
                let height = "\(item.item_name ?? "")   x \(item.quantity ?? "0")".height(withConstrainedWidth: cell.itemNmLbl.frame.width, font: cell.itemNmLbl.font)
                return (height + 40)
                
            }
            
            return 30
        }
    }
    
}




extension BBQReorderDetailsViewController: BBQTaxTableViewCellDelegate{
    func didTapOnBillDetails(cell: BBQTaxTableViewCell, index: Int, taxBreakup: TaxBreakUp) {
        let popupVC = BillDetailsPopVC(nibName: "BillDetailsPopVC", bundle: nil)
        popupVC.modalPresentationStyle = UIModalPresentationStyle .popover
        popupVC.preferredContentSize = CGSize(width: 170, height: 130)
        popupVC.strTitle = taxBreakup.label
        popupVC.taxBreakUp = taxBreakup.details
        popupVC.view.frame.size.width = 200 > self.view.frame.size.width ? self.view.frame.size.width : 200
        popupVC.view.frame.size.height = CGFloat(60 + (taxBreakup.details.count * 26))
        var originFrame = cell.btnDetails.frame
        originFrame.size.width = 80
        originFrame.origin.x = cell.frame.width - 60
        let frame = cell.convert(originFrame, to: self.view)
        popTip.show(customView: popupVC.view, direction: .auto, in: self.view, from: frame)
        AnalyticsHelper.shared.triggerEvent(type: .C19)
    }
}
