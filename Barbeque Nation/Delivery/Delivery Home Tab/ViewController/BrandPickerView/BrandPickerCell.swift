//
 //  Created by Sakir Sherasiya on 02/08/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified BrandPickerCell.swift
 //

import UIKit

class BrandPickerCell: UITableViewCell {
    
    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewForContainer.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0)
        viewForContainer.dropShadow()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    
    
    func setCellData(name: String, logo: String) {
        self.imgLogo.setImagePNG(url: logo, placeholderImage: nil)
        self.lblName.text = name
    }
    
}
