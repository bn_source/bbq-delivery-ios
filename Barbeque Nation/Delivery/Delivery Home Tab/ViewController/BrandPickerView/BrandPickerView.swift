//
 //  Created by Mahmadsakir on 29/12/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified PickupRescheduleConfirmation.swift
 //

import UIKit


class BrandPickerView: UIView {
    
    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var backgroundAlpha: UIView!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var heightForTableVIew: NSLayoutConstraint!
    
    
    var data = [BrandInfo]()
    var delegate: BrandPickerViewDelegate?
    var superVC: UIViewController?
    
    
    //MARK:- View Configuration
    func initWith(data: [BrandInfo], delegate: BrandPickerViewDelegate) -> BrandPickerView {
        let view = loadFromNib()!
        view.styleUI()
        view.data = data
        view.delegate = delegate
        view.tableview.reloadData{
            view.heightForTableVIew.constant = view.tableview.contentSize.height
        }
        return view
    }
        
        
    func loadFromNib() -> BrandPickerView? {
        if let views = Bundle.main.loadNibNamed("BrandPickerView", owner: self, options: nil), let view = views[0] as? BrandPickerView{
            view.frame = UIApplication.shared.windows[0].bounds
            return view
        }
        return nil
    }
    
    
    func addToSuperView(view: UIViewController?) {
        AnalyticsHelper.shared.triggerEvent(type: .D14)
        superVC = view
        superVC?.tabBarController?.tabBar.isHidden = true
        
        self.backgroundAlpha.alpha = 0.0
        self.viewForContainer.isHidden = true
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
            self.backgroundAlpha.alpha = 0.5
            self.viewForContainer.isHidden = false
        } completion: { (isCompleted) in
            self.backgroundAlpha.alpha = 0.5
            self.viewForContainer.isHidden = false
            self.tableview.reloadData{
                self.heightForTableVIew.constant = self.tableview.contentSize.height
            }
        }
        
        self.heightForTableVIew.constant = 0
        tableview.reloadData{
            self.heightForTableVIew.constant = self.tableview.contentSize.height
        }
        
        if let view = view{
            view.view.addSubview(self)
        }else{
            getMainView().addSubview(self)
        }
    }
    
    func removeToSuperView() {
        self.viewForContainer.isHidden = false
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
            self.backgroundAlpha.alpha = 0.0
            self.viewForContainer.isHidden = true
        } completion: { (isCompleted) in
            self.backgroundAlpha.alpha = 0.0
            self.removeFromSuperview()
        }
        
        superVC?.tabBarController?.tabBar.isHidden = false
        superVC = nil
    }
    
    func styleUI() {
        tableview.register(UINib(nibName: "BrandPickerCell", bundle: nil), forCellReuseIdentifier: "BrandPickerCell")
        viewForContainer.roundCorners(cornerRadius: 5, borderColor: .clear, borderWidth: 0)
        tableview.delegate = self
        tableview.dataSource = self
    }
    
    @IBAction func onClickOnBackground(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .D14B)
        removeToSuperView()
    }
}

protocol BrandPickerViewDelegate {
    func confirm(view: BrandPickerView, brandInfo: BrandInfo)
}

extension BrandPickerView: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row >= data.count{
            return UITableViewCell()
        }
        guard let cell = tableview.dequeueReusableCell(withIdentifier: "BrandPickerCell", for: indexPath) as? BrandPickerCell else { return UITableViewCell()}
        cell.setCellData(name: data[indexPath.row].name, logo: data[indexPath.row].logo)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        AnalyticsHelper.shared.triggerEvent(type: .D14A)
        if indexPath.row < data.count{
            delegate?.confirm(view: self, brandInfo: data[indexPath.row])
        }
        removeToSuperView()
    }
}
