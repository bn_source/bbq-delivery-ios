//
 //  Created by Dharani Sadasivam on 11/10/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified AlertViewController.swift
 //

import UIKit
protocol AlertViewControllerDelegate: AnyObject {
    func actionOnNoBtn(_ item: Item, indexPath: IndexPath?)
    func actionOnYesBtn(_ item: Item, indexPath: IndexPath?)
}

class AlertViewController: UIViewController {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var yesBtn: UIButton!
    @IBOutlet weak var noBtn: UIButton!
    @IBOutlet weak var contentLbl: UILabel!
    @IBOutlet weak var alertView: UIView!
    var item: Item?
    weak var delegate: AlertViewControllerDelegate?
    var indexPath: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        noBtn.backgroundColor = .lightThemeBackground.withAlphaComponent(0.3)
        noBtn.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0.0)
        yesBtn.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0.0)
        alertView.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
    }
    @IBAction func actionOnNoBtn(_ sender: Any) {
        
        guard let item = item else { return }
        delegate?.actionOnNoBtn(item, indexPath: indexPath)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionOnCloseBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionOnYesBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            guard let item = self.item else { return }
            self.delegate?.actionOnYesBtn(item, indexPath: self.indexPath)
        })
        
    }
}
