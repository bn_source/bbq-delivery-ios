//
 //  Created by Mahmadsakir on 12/08/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified ScheduleDeliveryIntroVC.swift
 //

import UIKit

class ScheduleDeliveryIntroVC: UIViewController {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    
    var brand_name = "BBQN"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AnalyticsHelper.shared.triggerEvent(type: .D13)
        view.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        lblTitle.text = String(format: "%@ Scheduled Delivery", brand_name)
        lblMessage.text = String(format: "How does %@ scheduled\ndelivery work?", brand_name)
        //How does Barbeque Nation scheduled
        //delivery work?
    }

    @IBAction func onClickBtnClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
