//
//  DeliveryMenuViewController.swift
//  BBQ
//
//  Created by Dharani Sadasivam on 23/08/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import UIKit

protocol MenuViewControllerDelegate: AnyObject {
    func actionOnDone(_ item: Item, indexPath: IndexPath?)

}
class DeliveryMenuViewController: UIViewController {

    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var progressPercentLbl: UILabel!
    @IBOutlet weak var foodTypeIcon: UIImageView!
    @IBOutlet weak var progressShadowView: UIView!
    @IBOutlet weak var progressCircularView: UIView!
    @IBOutlet weak var progressView: ProgressView!
    @IBOutlet weak var menusTitleLbl: UILabel!
    @IBOutlet weak var menuTableView: UITableView!
    @IBOutlet weak var menuDescLbl: UILabel!
    @IBOutlet weak var menuTitleLbl: UILabel!
    @IBOutlet weak var menuIcon: UIImageView!
    var viewModel: MenuViewModel?
    weak var menuDelegate: MenuViewControllerDelegate?
    var isUpdated = false
    var item: Item?
    var indexPath: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let item = item else { return}
        viewModel = MenuViewModel(item)
        viewModel?.makeDefaultSelectItem()
        styleUI()
    }
    
    func styleUI() {
        menuIcon.setCircleView()
        progressView.setShadow()
        progressView.layer.sublayers![1].cornerRadius = 4
        progressView.subviews[1].clipsToBounds = true
        progressCircularView.setCircleView()
        progressShadowView.setShadow()
       
        progressView.setProgress(0, animated: true)
        menuIcon.layer.borderWidth = 1
        menuIcon.layer.borderColor = UIColor(named: "TextColor")?.cgColor
        self.navigationController?.view.backgroundColor = .white
        
        self.navigationController?.navigationBar.tintColor = UIColor(named: "TextColor")
        self.navigationController?.navigationBar.topItem?.title = ""
       
        fillUI()
        self.menuTableView.tableFooterView = UIView(frame: .zero)
        self.menuTableView.reloadData()
    }
    
    func fillUI() {
        updateProgress()
        menuTitleLbl.text = viewModel?.item?.name
        menuDescLbl.text = viewModel?.item?.description
        foodTypeIcon.image = viewModel?.getFoodTypeIcon()
        menuIcon.setImage(url: viewModel?.item?.imageUrl ?? "", isForPromotions: false)
    }
    override func viewWillDisappear(_ animated: Bool) {
        if !isUpdated {
            viewModel?.removeAllSelectedItems()
        }
    }
    func updateDoneBtn() {
        let color = ((viewModel?.getAllSelectedItemsWithOutAddOn().count ?? 0) == (viewModel?.getTotalThreshold() ?? 0)) ? UIColor(named: "ThemeColor") : #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        doneBtn.setTitleColor(color, for: .normal)
        doneBtn.isUserInteractionEnabled = ((viewModel?.getAllSelectedItemsWithOutAddOn().count ?? 0) == (viewModel?.getTotalThreshold() ?? 0))
    }
    
    func updateProgress() {
        let totalCustomisation = viewModel?.getTotalThreshold() ?? 0
        let selectedItemCount = viewModel?.getAllSelectedItemsWithOutAddOn().count ?? 0
        
        let progress = (totalCustomisation > 0) && (selectedItemCount > 0) ? Float(selectedItemCount)/Float(totalCustomisation) : 0
        progressView.setProgress(progress, animated: true)
        let percentage = Int((progress)  * 100)
        progressPercentLbl.text = "Progress \(percentage)%"
        updateDoneBtn()
    }
    
    @IBAction func actionOnDoneBtn(_ sender: Any) {
        isUpdated = true
        guard let item  = item else { return }
        item.quantity += 1
        menuDelegate?.actionOnDone(item, indexPath: indexPath)
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension DeliveryMenuViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel?.getCustomisations().count ?? 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let nsAttributedString = NSMutableAttributedString(string: viewModel?.getMenuTitleAt(section) ?? "", attributes: [NSAttributedString.Key.font: UIFont.systemFontOfSize(size: 16.0, weight: .semibold), NSAttributedString.Key.foregroundColor: UIColor(named: "TextColor") as Any])
//        if (viewModel?.item?.quantity ?? 0) > 0 {
            let subTitleString = NSAttributedString(string: " (\(viewModel?.getMenuDescAt(section) ?? ""))", attributes: [NSAttributedString.Key.font: UIFont.systemFontOfSize(size: 12.0, weight: .semibold), NSAttributedString.Key.foregroundColor: UIColor(named: "TextColor")!])
               nsAttributedString.append(subTitleString)
//        }
        let label = UILabel()
        label.attributedText = nsAttributedString
        return label
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.getMenuItemsAt(section).count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath) as! MenuTableViewCell
        cell.styleUI()
        cell.activatePlusMinusView(false)
        guard let item = viewModel?.getMenuItemsAt(indexPath.section)[indexPath.row] else {
            return cell
        }
        cell.section = indexPath.section
        cell.row = indexPath.row
        cell.delegate = self
        cell.menuViewModel = viewModel
        print("Reload rows \(item.id)")
        cell.updateCell(item)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
}


public class ProgressView: UIProgressView {
    var height: CGFloat = 8.0
    public override func sizeThatFits(_ size: CGSize) -> CGSize {
        return CGSize(width: size.width, height: height) // We can set the required height
    }
}

extension DeliveryMenuViewController: MenuTableViewCellDelegate {
    func actionOnRemoveItemButton(item: Item, section: Int, row: Int) {
        if (viewModel?.getCustomisationsAt(section).maxQuantityLimit ?? 0) > 0 {
        updateProgress()
        }
        menuTableView.reloadData()
    }
    
    func actionOnAddItemButton(item: Item, section: Int, row: Int) {
        if (viewModel?.getCustomisationsAt(section).maxQuantityLimit ?? 0) > 0 {
            updateProgress()
        }
        menuTableView.reloadData()
    }
    
    
}
