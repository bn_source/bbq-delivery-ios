//
 //  Created by Mahmadsakir on 29/12/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified ItemDetailViewController.swift
 //

import UIKit

protocol ItemSearchViewDelegate: AnyObject {
    func actionOnDone(view: ItemSearchView, item: Item, indexPath: IndexPath?)
    func actionOnAddItemButton(view: ItemSearchView, item: Item, indexPath: IndexPath?)
    func actionOnRemoveItemButton(view: ItemSearchView, item: Item, indexPath: IndexPath?)
    func actionOnImageTapped(item: Item, indexPath: IndexPath?, isCameFromSearch: Bool)
    func didHideItemSearchView()
}
class ItemSearchView: UIView {
    
    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var backgroundAlpha: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var viewForSearch: UIView!
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var viewForCollectionView: UIView!
    
    var arrItems = [Item]()
    var arrComboBox = [Item]()
    var viewModel = DeliveryHomeViewModel()
    var searchOperation: Operation?
    var delegate: ItemSearchViewDelegate?
    
    
    
    //MARK:- View Configuration
    func initWith(delegate: ItemSearchViewDelegate) -> ItemSearchView {
        let view = loadFromNib()!
        view.styleUI()
        view.delegate = delegate
        return view
    }
        
        
    func loadFromNib() -> ItemSearchView? {
        if let views = Bundle.main.loadNibNamed("ItemSearchView", owner: self, options: nil), let view = views[0] as? ItemSearchView{
            view.frame = UIApplication.shared.windows[0].bounds
            return view
        }
        return nil
    }
    
    private func filterComboBox(){
        for category in self.viewModel.originalCategoryList{
            if category.type == .comboBox{
                for item in self.viewModel.originalMenuDict[category] ?? [Item](){
                    self.arrComboBox.append(item)
                }
            }
        }
        self.viewForCollectionView.isHidden = false
        self.collectionView.reloadData()
    }
    
    func addToSuperView(view: UIViewController?) {
        AnalyticsHelper.shared.triggerEvent(type: .DS01)
        self.screenName(name: .Delivery_Item_Search_Screen)
        if let tabBarController = view?.navigationController?.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
            tabBarController.tabBar.isHidden = true
        }
        
        if let view = view{
            view.view.addSubview(self)
        }else{
            getMainView().addSubview(self)
        }
        viewForContainer.isHidden = true
        self.backgroundAlpha.alpha = 0.0
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
            self.viewForContainer.isHidden = false
            self.backgroundAlpha.alpha = 0.5
        } completion: { (isCompleted) in
            self.viewForContainer.isHidden = false
        }
        filterComboBox()
    }
    
    func removeToSuperView() {
        self.endEditing(true)
        AnalyticsHelper.shared.triggerEvent(type: .DS07)
        self.screenName(name: .Delivery_Home_Screen)
        delegate?.didHideItemSearchView()
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
            self.viewForContainer.isHidden = true
            self.backgroundAlpha.alpha = 0.0
            self.layoutIfNeeded()
        } completion: { (isCompleted) in
            self.viewForContainer.isHidden = true
            self.backgroundAlpha.alpha = 0.0
            self.removeFromSuperview()
        }
    }
    
    func styleUI() {
        viewForContainer.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        viewForSearch.setCornerRadius(10.0)
        viewForSearch.dropShadow()
        tableView.delegate = self
        tableView.dataSource = self
        collectionView.delegate = self
        collectionView.dataSource = self
        txtSearch.delegate = self
        txtSearch.clearButtonMode = .whileEditing
        txtSearch.keyboardType = .alphabet
        fillUI()
    }
    
    func updateQuantity(item: Item?, indexPath: IndexPath?){
        if !viewForCollectionView.isHidden{
            updateCollectionQuantity(item: item, indexPath: indexPath)
            return
        }
        if let item = item{
            if let indexPath = indexPath, arrItems.count > indexPath.row, arrItems[indexPath.row].id == item.id{
                arrItems[indexPath.row] = item
            }else{
                for index in 0 ..< arrItems.count{
                    if item.id == arrItems[index].id{
                        arrItems[index] = item
                        break
                    }
                }
            }
        }
        tableView.reloadData()
    }
    
    @IBAction func onClickedRemoveView(_ sender: Any) {
        removeToSuperView()
    }
    @IBAction func searchingOfItem(_ sender: Any) {
        if let searchOperation = searchOperation{
            searchOperation.cancel()
        }
        AnalyticsHelper.shared.triggerEvent(type: .DS02)
        if (txtSearch.text ?? "").replacingOccurrences(of: " ", with: "").count > 2{
            self.viewForCollectionView.isHidden = true
            var text = txtSearch.text ?? ""
            text = text.lowercased().replacingOccurrences(of: " ", with: "")
            searchOperation = BlockOperation {
                self.arrItems.removeAll()
                for category in self.viewModel.originalCategoryList{
                    for item in self.viewModel.originalMenuDict[category] ?? [Item](){
                        if item.name.replacingOccurrences(of: " ", with: "").lowercased().contains(text){
                            self.arrItems.append(item)
                        }
                    }
                }
                if self.arrItems.count > 0{
                    self.lblNoData.isHidden = true
                    AnalyticsHelper.shared.triggerEvent(type: .DS02A)
                }else{
                    self.lblNoData.isHidden = false
                    AnalyticsHelper.shared.triggerEvent(type: .DS02B)
                }
                self.reloadTable()
            }
            OperationQueue.main.addOperation(searchOperation!)
            //searchOperation?.start()
        }else{
            AnalyticsHelper.shared.triggerEvent(type: .DS02C)
            arrItems.removeAll()
            reloadTable()
            lblNoData.isHidden = true
        }
        
    }
    
    private func reloadTable(){
        self.tableView.reloadData()
    }
    
    func fillUI() {
        tableView.register(UINib(nibName: "ItemSearchViewCell", bundle: nil), forCellReuseIdentifier: "ItemSearchViewCell")
        collectionView.register(UINib(nibName: ItemSearchBoxCell.getCellIdentifier(), bundle: nil), forCellWithReuseIdentifier: ItemSearchBoxCell.getCellIdentifier())
    }
    
}
extension ItemSearchView: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemSearchViewCell", for: indexPath) as! ItemSearchViewCell
        if arrItems.count > indexPath.row{
            cell.setCellData(item: arrItems[indexPath.row], indexPath: indexPath, isServicable: viewModel.isServicable, search: txtSearch.text)
        }
        cell.delegate = self
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 152.0
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if txtSearch.isFirstResponder{
            txtSearch.resignFirstResponder()
        }
    }
}
extension ItemSearchView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrComboBox.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ItemSearchBoxCell.getCellIdentifier(), for: indexPath) as! ItemSearchBoxCell
        if arrComboBox.count > indexPath.row{
            cell.updateCell(indexPath: indexPath, item: arrComboBox[indexPath.row], isServicable: viewModel.isServicable)
            cell.delegate = self
        }
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
extension ItemSearchView: ItemSearchViewCellDelegate{
    func actionOnDone(_ item: Item, indexPath: IndexPath) {
        if txtSearch.isFirstResponder{
            txtSearch.resignFirstResponder()
        }
        delegate?.actionOnDone(view: self, item: item, indexPath: nil)
    }
    
    func actionOnAddItemButton(item: Item, indexPath: IndexPath) {
        if txtSearch.isFirstResponder{
            txtSearch.resignFirstResponder()
        }
        delegate?.actionOnAddItemButton(view: self, item: item, indexPath: nil)
    }
    
    func actionOnRemoveItemButton(item: Item, indexPath: IndexPath) {
        delegate?.actionOnRemoveItemButton(view: self, item: item, indexPath: nil)
    }
    
    func actionOnImageTapped(item: Item, indexPath: IndexPath) {
        if txtSearch.isFirstResponder{
            txtSearch.resignFirstResponder()
        }
        delegate?.actionOnImageTapped(item: item, indexPath: indexPath, isCameFromSearch: true)
    }
}
extension ItemSearchView: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
extension ItemSearchView: ItemSearchBoxCellDelegate{
    func actionOnAddItemButton(item: Item, indexPath: IndexPath?) {
        if txtSearch.isFirstResponder{
            txtSearch.resignFirstResponder()
        }
        delegate?.actionOnAddItemButton(view: self, item: item, indexPath: nil)
        updateCollectionQuantity(item: item, indexPath: indexPath, qty: 1)
    }
    
    func actionOnRemoveItemButton(item: Item, indexPath: IndexPath?) {
        delegate?.actionOnRemoveItemButton(view: self, item: item, indexPath: nil)
    }
    
    func actionOnImageTapped(item: Item, indexPath: IndexPath?, isCameFromSearch: Bool) {
        if txtSearch.isFirstResponder{
            txtSearch.resignFirstResponder()
        }
        delegate?.actionOnImageTapped(item: item, indexPath: indexPath, isCameFromSearch: true)
    }
    private func updateCollectionQuantity(item: Item, indexPath: IndexPath?, qty: Int){
        if let indexPath = indexPath, arrComboBox.count > indexPath.row, arrComboBox[indexPath.row].id == item.id{
            arrComboBox[indexPath.row] = item
        }else{
            for i in 0..<arrComboBox.count{
                if arrComboBox[i].id == item.id{
                    arrComboBox[i] = item
                }
            }
        }
        collectionView.reloadData()
    }
    private func updateCollectionQuantity(item: Item?, indexPath: IndexPath?){
        if let indexPath = indexPath, let item = item, arrComboBox.count > indexPath.row, arrComboBox[indexPath.row].id == item.id{
            arrComboBox[indexPath.row] = item
        }else if let item = item{
            for i in 0..<arrComboBox.count{
                if arrComboBox[i].id == item.id{
                    arrComboBox[i] = item
                }
            }
        }
        collectionView.reloadData()
    }
}
