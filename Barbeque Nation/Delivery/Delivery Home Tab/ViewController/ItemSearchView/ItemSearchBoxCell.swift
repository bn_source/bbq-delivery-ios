//
//  ComboBoxCollectionViewCell.swift
//  BBQ
//
//  Created by Shareef on 05/09/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import UIKit

protocol ItemSearchBoxCellDelegate: AnyObject {
    func actionOnAddItemButton(item: Item, indexPath: IndexPath?)
    func actionOnRemoveItemButton(item: Item, indexPath: IndexPath?)
    func actionOnImageTapped(item: Item, indexPath: IndexPath?, isCameFromSearch: Bool)

}

class ItemSearchBoxCell: UICollectionViewCell {
    
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var itemDescriptionLabel: UILabel!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var vegNonVegImageView: UIImageView!
    @IBOutlet weak var itemPriceLabel: UILabel!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var quantityView: UIView!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var customizableLabel: UILabel!
    @IBOutlet weak var lblOldPrice: UILabel!
    @IBOutlet weak var lblPromotion: UILabel!
    @IBOutlet weak var viewForPromotion: CustomDashedView!
    @IBOutlet weak var btnMinus: UIButton!
    @IBOutlet weak var btnPlus: UIButton!
    
    weak var delegate: ItemSearchBoxCellDelegate?
    private var item = Item()
    private var indexPath: IndexPath?
    
    static func getCellIdentifier() -> String {
        return "ItemSearchBoxCell"
    }
    
    func updateCell(indexPath: IndexPath, item: Item, isServicable: Bool) {
        self.item = item
        self.indexPath = indexPath
        updateUI()
        itemNameLabel.text = item.name
        itemDescriptionLabel.text = item.description
        itemImageView.setImage(url: item.imageUrl, placeholderImage: nil, isForPromotions: false)
        vegNonVegImageView.image = (item.foodType == .veg ? .veg : .nonVeg)
        itemPriceLabel.text = String(format: "%@%li", getCurrency(), item.itemPrice)
        updateQuantity(count: item.quantity)
        addBtn.applyMultiBrandThemeWith(isServicable: isServicable, isAvailable: item.isAvailable)
        addBtn.isUserInteractionEnabled = (isServicable && item.isAvailable)
        quantityView.isUserInteractionEnabled = (isServicable && item.isAvailable)
        customizableLabel.isHidden = !item.isCustomizable
        quantityView.applyViewMultiBrandTheme()
        btnPlus.setTitleColor(.deliveryThemeTextColor, for: .normal)
        quantityLabel.textColor = .deliveryThemeTextColor
        btnMinus.setTitleColor(.deliveryThemeTextColor, for: .normal)
   
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        itemImageView.image = nil
    }
    
    private func updateUI() {
        outerView.setShadow()
        outerView.setCornerRadius(20)
        itemImageView.setCornerRadius(20)
        addBtn.setCornerRadius(5)
        addBtn.setShadow()
        quantityView.setCornerRadius(5)
        quantityView.setShadow()
        quantityLabel.font = UIFont.appThemeExtraBoldWith(size: 14.0)
        updateOfferData()
//
//        quantityLabel.backgroundColor =  UIColor(named: "ThemeColor")
//        quantityLabel.textColor =  .white //UIColor(named: "TextColor")
//        quantityLabel.setCornerRadius(quantityLabel.frame.width / 3)
    }
    
    private func updateOfferData(){
        if item.originalPrice != item.itemPrice{
            lblOldPrice.isHidden = false
            let attrString = NSAttributedString(string: String(format: "%@%li", getCurrency(), item.originalPrice), attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue])
            lblOldPrice.attributedText = attrString
        }else{
            lblOldPrice.isHidden = true
        }
        
        if item.promotion_message != ""{
            lblPromotion.text = item.promotion_message
            viewForPromotion.isHidden = false
        }else{
            viewForPromotion.isHidden = true
        }
    }
    
    func updateQuantity(count: Int) {
        addBtn.isHidden = count > 0
        quantityLabel.text = String(count)
    }
    
    @IBAction func actionOnAddButton(_ sender: Any) {
        delegate?.actionOnAddItemButton(item: item, indexPath: indexPath)
    }
    
    @IBAction func actionOnRemoveButton(_ sender: Any) {
        item.quantity -= 1
        updateQuantity(count: item.quantity)
        delegate?.actionOnRemoveItemButton(item: item, indexPath: indexPath)
    }
    
    @IBAction func actionOnComboItemImageTapped(_ sender: Any) {
        delegate?.actionOnImageTapped(item: item, indexPath: indexPath, isCameFromSearch: false)
    }
}
