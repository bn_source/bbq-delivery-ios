//
 //  Created by Mahmadsakir on 11/03/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified ItemSearchViewCell.swift
 //

import UIKit

class ItemSearchViewCell: UITableViewCell {
    
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var itemDescriptionLabel: UILabel!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var vegNonVegImageView: UIImageView!
    @IBOutlet weak var itemPriceLabel: UILabel!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var bottomLineLabel: UILabel!
    @IBOutlet weak var quantityView: UIView!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var customizableLabel: UILabel!
    @IBOutlet weak var lblOldPrice: UILabel!
    @IBOutlet weak var lblPromotion: UILabel!
    @IBOutlet weak var viewForPromotion: CustomDashedView!
    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var btnMinus: UIButton!
    @IBOutlet weak var btnPlus: UIButton!
    
    weak var delegate: ItemSearchViewCellDelegate?
    private var item: Item!
    private var indexPath: IndexPath!
    var isUpdated = false

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    func setCellData(item: Item, indexPath: IndexPath, isServicable: Bool, search: String?) {
        self.item = item
        self.indexPath = indexPath
        
        updateUI()
        itemNameLabel.text = item.name
        itemDescriptionLabel.text = item.description
        itemImageView.setImage(url: item.imageUrl, placeholderImage: nil, isForPromotions: false)
        vegNonVegImageView.image = (item.foodType == .veg ? .veg : .nonVeg)
        itemPriceLabel.text = item.price
        updateQuantity(count: item.quantity)
        addBtn.applyMultiBrandThemeWith(isServicable: isServicable, isAvailable: item.isAvailable)
        addBtn.isUserInteractionEnabled = (isServicable && item.isAvailable)
        quantityView.isUserInteractionEnabled = (isServicable && item.isAvailable)
        customizableLabel.isHidden = !item.isCustomizable
        if let search = search, search.count > 0{
            var strString = search
            if !item.name.contains(search), let range = item.name.range(of: search, options: .caseInsensitive){
                strString = String(item.name[range])
            }
            let arrData = item.name.components(separatedBy: strString)
            let strAtr = NSMutableAttributedString()
            for i in 0..<arrData.count{
                if i != 0{
                    strAtr.append(NSMutableAttributedString(string: strString, attributes: [NSAttributedString.Key.font: UIFont.systemFontOfSize(size: 16.0, weight: .semibold), NSAttributedString.Key.foregroundColor: UIColor.theme]))
                }
                strAtr.append(NSMutableAttributedString(string: arrData[i], attributes: [NSAttributedString.Key.font: UIFont.systemFontOfSize(size: 14.0, weight: .semibold), NSAttributedString.Key.foregroundColor: UIColor(named: "TextColor") as Any]))
            }
            itemNameLabel.attributedText = strAtr
            
        }
    }

    private func updateUI() {
        itemImageView.setCornerRadius(11)
        addBtn.setCornerRadius(5)
        addBtn.setShadow()
        quantityView.setCornerRadius(5)
        quantityView.setShadow()
        viewForContainer.setCornerRadius(10.0)
        viewForContainer.setShadow()
        itemImageView.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.rightCornersMask)
        quantityLabel.font = UIFont.appThemeExtraBoldWith(size: 14.0)
        updateOfferData()
        quantityView.applyViewMultiBrandTheme()
        btnPlus.setTitleColor(.deliveryThemeTextColor, for: .normal)
        quantityLabel.textColor = .deliveryThemeTextColor
        btnMinus.setTitleColor(.deliveryThemeTextColor, for: .normal)
    }
    
    private func updateOfferData(){
        if item.originalPrice != item.itemPrice{
            lblOldPrice.isHidden = false
            let attrString = NSAttributedString(string: String(format: "%@%li", getCurrency(), item.originalPrice), attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue])
            lblOldPrice.attributedText = attrString
        }else{
            lblOldPrice.isHidden = true
        }
        
        if item.promotion_message != ""{
            lblPromotion.text = item.promotion_message
            viewForPromotion.isHidden = false
        }else{
            viewForPromotion.isHidden = true
        }
    }
    
    func updateQuantity(count: Int){
        addBtn.isHidden = count > 0 ? true : false
        quantityView.isHidden = count > 0 ? false : true
        quantityLabel.text = String(count)
    }
    
    //MARK:- Button Actions
    @IBAction func actionOnDoneBtn(_ sender: Any) {
        addToCartItemEvent(type: item.foodCategory)
        isUpdated = true
        delegate?.actionOnAddItemButton(item: item, indexPath: indexPath)
        updateQuantity(count: item.quantity)
        //self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickOnBtnPlus(_ sender: Any) {
        //item?.quantity += 1
        increaseItemEvent(type: item.foodCategory)
        delegate?.actionOnAddItemButton(item: item, indexPath: indexPath)
        updateQuantity(count: item.quantity)
    }
    
    @IBAction func onClickBtnMinus(_ sender: Any) {
        decreaseItemEvent(type: item.foodCategory)
        item.quantity -= 1
        delegate?.actionOnRemoveItemButton(item: item, indexPath: indexPath)
        updateQuantity(count: item.quantity)
    }
    
    @IBAction func actionOnComboItemImageTapped(_ sender: Any) {
        delegate?.actionOnImageTapped(item: item, indexPath: indexPath)
        AnalyticsHelper.shared.triggerEvent(type: .DS06)
    }
    
    private func addToCartItemEvent(type: FoodCategory) {
        switch type {
        case .comboBox:
            AnalyticsHelper.shared.triggerEvent(type: .DS03)
            break
        case .comboItem:
            AnalyticsHelper.shared.triggerEvent(type: .DS04)
            break
        case .item:
            AnalyticsHelper.shared.triggerEvent(type: .DS05)
            break
        }
    }
    
    private func increaseItemEvent(type: FoodCategory) {
        switch type {
        case .comboBox:
            AnalyticsHelper.shared.triggerEvent(type: .DS03A)
            break
        case .comboItem:
            AnalyticsHelper.shared.triggerEvent(type: .DS04A)
            break
        case .item:
            AnalyticsHelper.shared.triggerEvent(type: .DS05A)
            break
        }
    }
    
    private func decreaseItemEvent(type: FoodCategory) {
        switch type {
        case .comboBox:
            AnalyticsHelper.shared.triggerEvent(type: .DS03B)
            break
        case .comboItem:
            AnalyticsHelper.shared.triggerEvent(type: .DS04B)
            break
        case .item:
            AnalyticsHelper.shared.triggerEvent(type: .DS05B)
            break
        }
    }
    
}


    
protocol ItemSearchViewCellDelegate: AnyObject {
    func actionOnDone(_ item: Item, indexPath: IndexPath)
    func actionOnAddItemButton(item: Item, indexPath: IndexPath)
    func actionOnRemoveItemButton(item: Item, indexPath: IndexPath)
    func actionOnImageTapped(item: Item, indexPath: IndexPath)
}
