//
 //  Created by Mahmadsakir on 30/12/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified ItemDetailsTableViewCell.swift
 //

import UIKit

class ItemDetailsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var menuNameLbl: UILabel!
    @IBOutlet weak var menuIcon: UIImageView!
    private var item =  Item()
    weak var delegate: MenuTableViewCellDelegate?
    var section: Int?
    var row: Int?
    var menuViewModel: MenuViewModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    
    func updateCell(_ item: Item) {
        self.item = item
        menuNameLbl.text = item.name
        menuIcon.image = item.foodType == FoodType.veg ? #imageLiteral(resourceName: "Group 846") : #imageLiteral(resourceName: "Group 845")
        guard let viewModel = menuViewModel, let section = section else { return }
        let selectedItem = viewModel.getCustomisationsAt(section).selectedItems
        print(selectedItem.description)
    }

    
}

