//
 //  Created by Mahmadsakir on 29/12/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified ItemDetailViewController.swift
 //

import UIKit

protocol ItemDetailViewDelegate: AnyObject {
    func actionOnDone(_ item: Item, indexPath: IndexPath?)
    func actionOnMultipleSelect(_ item: Item, indexPath: IndexPath?)
    func actionOnAddItemButton(item: Item, indexPath: IndexPath?)
    func actionOnRemoveItemButton(item: Item, indexPath: IndexPath?)
    func actionOnReduceItem(item: Item, indexPath: IndexPath?)
    func didHideItemDetails(isCameFromSearch: Bool)
}
class ItemDetailView: UIView {

    @IBOutlet weak var foodTypeIcon: UIImageView!
    @IBOutlet weak var menusTitleLbl: UILabel!
    @IBOutlet weak var menuTableView: UITableView!
    @IBOutlet weak var menuDescLbl: UILabel!
    @IBOutlet weak var menuTitleLbl: UILabel!
    @IBOutlet weak var menuIcon: UIImageView!
    @IBOutlet weak var btnAddToCart: UIButton!
    @IBOutlet weak var lblLiner: UILabel!
    @IBOutlet weak var stackViewForQuantity: UIStackView!
    @IBOutlet weak var stackViewForQty: UIStackView!
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var btnMinus: UIButton!
    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var heightsOfTableView: NSLayoutConstraint!
    @IBOutlet weak var imageRatio: NSLayoutConstraint!
    @IBOutlet weak var backgroundAlpha: UIView!
    @IBOutlet weak var viewForImageView: UIView!
    @IBOutlet weak var lblPromotion: UILabel!
    @IBOutlet weak var viewForPromotion: CustomDashedView!
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    
    
    var viewModel: MenuViewModel?
    weak var menuDelegate: ItemDetailViewDelegate?
    var isUpdated = false
    var item: Item?
    var isAnimationCompleted = true
    var imageHeight: CGFloat = 0
    var tableViewHeight: CGFloat = 0
    var indexPath: IndexPath?
    var isCameFromSearch = false
    var isServicable: Bool = false
    
    
    //MARK:- View Configuration
    func initWith(item: Item?, indexPath: IndexPath?, isServicable: Bool, delegate: ItemDetailViewDelegate) -> ItemDetailView {
        let view = loadFromNib()!
        view.item = item
        view.indexPath = indexPath
        view.isServicable = isServicable
        view.menuTableView.register(UINib(nibName: "ItemDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "ItemDetailsTableViewCell")
        view.menuDelegate = delegate
        if let item = item {
            view.viewModel = MenuViewModel(item)
        }
        view.viewForContainer.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        view.styleUI()
        return view
    }
        
        
    func loadFromNib() -> ItemDetailView? {
        if let views = Bundle.main.loadNibNamed("ItemDetailsView", owner: self, options: nil), let view = views[0] as? ItemDetailView{
            view.frame = UIApplication.shared.windows[0].bounds
            return view
        }
        return nil
    }
    
    func addToSuperView(view: UIViewController?, isCameFromSearch: Bool) {
        if let tabBarController = view?.navigationController?.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
            tabBarController.tabBar.isHidden = true
        }
        self.isCameFromSearch = isCameFromSearch
        if let view = view{
            view.view.addSubview(self)
        }else{
            getMainView().addSubview(self)
        }
        let height =  frame.size.height - menuIcon.frame.size.height - 216
        imageHeight = menuIcon.frame.size.height
        tableViewHeight = menuTableView.contentSize.height > height ? height : menuTableView.contentSize.height
        heightsOfTableView.constant = tableViewHeight + 36
        viewForContainer.isHidden = true
        self.backgroundAlpha.alpha = 0.0
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
            self.viewForContainer.isHidden = false
            self.backgroundAlpha.alpha = 1
        } completion: { (isCompleted) in
            self.viewForContainer.isHidden = false
        }
    }
    
    func removeToSuperView() {
        menuDelegate?.didHideItemDetails(isCameFromSearch: isCameFromSearch)
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
            self.viewForContainer.isHidden = true
            self.backgroundAlpha.alpha = 0.0
            self.layoutIfNeeded()
        } completion: { (isCompleted) in
            self.viewForContainer.isHidden = true
            self.backgroundAlpha.alpha = 0.0
            self.removeFromSuperview()
        }
    }
    
    func styleUI() {
//        menuIcon.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        menuIcon.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0)
        btnAddToCart.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0)
       
        fillUI()
        self.menuTableView.delegate = self
        self.menuTableView.dataSource = self
        self.menuTableView.tableFooterView = UIView(frame: .zero)
        self.menuTableView.tableHeaderView = UIView(frame: .zero)
        self.menuTableView.sectionFooterHeight = 0.0
        self.menuTableView.reloadData()
        
        if let item = item{
            btnAddToCart.isHidden = item.quantity > 0 ? true : false
            stackViewForQuantity.isHidden = item.quantity > 0 ? false : true
            lblQuantity.text = String(item.quantity)
            
            menuTableView.isHidden = false
            menuDescLbl.isHidden = false
//            let height = ((100/207) * menuIcon.frame.size.height) - menuIcon.frame.size.width
            if item.foodCategory == .comboBox{
                if let count = viewModel?.getCustomisations().count, count > 0{
                    menuDescLbl.isHidden = true
                }else{
                    menuTableView.isHidden = true
                }
                //imageRatio.constant = height / 4.0
                
            }else{
                if !item.isCustomizable{
                    menuTableView.isHidden = true
                }
                //imageRatio.constant = height
            }
            btnAddToCart.isUserInteractionEnabled = (isServicable && item.isAvailable)
            btnAddToCart.applyMultiBrandThemeWith(isServicable: isServicable, isAvailable: item.isAvailable)
        }
        updateOfferData()
    }
    
    private func updateOfferData(){
        if item?.promotion_message != ""{
            lblPromotion.text = item?.promotion_message
            viewForPromotion.isHidden = false
        }else{
            viewForPromotion.isHidden = true
        }
    }
    
    
    func fillUI() {
        menuTitleLbl.text = viewModel?.item?.name
        menuDescLbl.text = viewModel?.item?.description
        foodTypeIcon.image = viewModel?.getFoodTypeIcon()
        menuIcon.setLocalStoredImage(url: viewModel?.item?.imageUrl ?? "")
        //menuIcon.setImage(imageUrl: viewModel?.item?.imageUrl ?? "", placeholderImage: menuIcon.image)
        stackViewForQty.applyViewMultiBrandTheme()
        btnPlus.setTitleColor(.deliveryThemeTextColor, for: .normal)
        lblQuantity.textColor = .deliveryThemeTextColor
        btnMinus.setTitleColor(.deliveryThemeTextColor, for: .normal)
        if let url = URL(string: viewModel?.item?.imageUrl ?? ""){
            menuIcon.sd_setImage(with: url) { image, error, cache, url in
                if let image = image{
                    self.imageHeightConstraint.constant = (image.size.height * self.menuIcon.frame.size.width) / image.size.width
                    self.menuIcon.image = image
                    self.menuIcon.isHidden = false
                }else{
                    self.menuIcon.isHidden = true
                }
            }
        }else{
            self.menuIcon.isHidden = true
        }
    }
    
    
    func updateQuantity(count: Int){
        btnAddToCart.isHidden = count > 0 ? true : false
        stackViewForQuantity.isHidden = count > 0 ? false : true
        lblQuantity.text = String(count)
    }
    
    @IBAction func actionOnDoneBtn(_ sender: Any) {
        isUpdated = true
        if let item = item{
            menuDelegate?.actionOnAddItemButton(item: item, indexPath: indexPath)
        }
        updateQuantity(count: item?.quantity ?? 0)
        //self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickOnBtnPlus(_ sender: Any) {
        //item?.quantity += 1
        if let item = item{
            menuDelegate?.actionOnAddItemButton(item: item, indexPath: indexPath)
        }
        updateQuantity(count: item?.quantity ?? 0)
    }
    
    @IBAction func onClickBtnMinus(_ sender: Any) {
        item?.quantity -= 1
        if let item = item{
            menuDelegate?.actionOnRemoveItemButton(item: item, indexPath: indexPath)
            if isCameFromSearch{
                menuDelegate?.actionOnReduceItem(item: item, indexPath: indexPath)
            }
        }
        updateQuantity(count: item?.quantity ?? 0)
    }
    @IBAction func gestureRecognised(_ sender: Any) {
        self.removeToSuperView()
    }
    @IBAction func onSwipeUpGesture(_ sender: Any) {
        if !viewForImageView.isHidden, menuTableView.frame.size.height < menuTableView.contentSize.height{
            hideAndShow(isHidden: true)
        }
    }
    
    
}

extension ItemDetailView: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel?.getCustomisations().count ?? 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let nsAttributedString = NSMutableAttributedString(string: viewModel?.getMenuTitleAt(section) ?? "", attributes: [NSAttributedString.Key.font: UIFont.systemFontOfSize(size: 16.0, weight: .semibold), NSAttributedString.Key.foregroundColor: UIColor(named: "TextColor") as Any])
//        if (viewModel?.item?.quantity ?? 0) > 0 {
            let subTitleString = NSAttributedString(string: " (\(viewModel?.getMenuDescAt(section) ?? ""))", attributes: [NSAttributedString.Key.font: UIFont.systemFontOfSize(size: 12.0, weight: .semibold), NSAttributedString.Key.foregroundColor: UIColor(named: "TextColor")!])
               nsAttributedString.append(subTitleString)
//        }
        let label = UILabel()
        label.attributedText = nsAttributedString
        label.frame.size.height = 30
        return label
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.getMenuItemsAt(section).count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemDetailsTableViewCell", for: indexPath) as! ItemDetailsTableViewCell
        guard let item = viewModel?.getMenuItemsAt(indexPath.section)[indexPath.row] else {
            return cell
        }
        cell.section = indexPath.section
        cell.row = indexPath.row
        cell.delegate = self
        cell.menuViewModel = viewModel
        print("Reload rows \(item.id)")
        cell.updateCell(item)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if velocity.y <= 0 && scrollView.contentOffset.y <= 0, viewForImageView.isHidden{
                hideAndShow(isHidden: false)
            }
        }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y: CGFloat = scrollView.contentOffset.y
        if y > 0{
            if !viewForImageView.isHidden, menuTableView.frame.size.height < menuTableView.contentSize.height{
                hideAndShow(isHidden: true)
            }
        }else{
            if viewForImageView.isHidden, menuTableView.frame.size.height < menuTableView.contentSize.height{
                hideAndShow(isHidden: false)
            }
        }
    }
    
    func hideAndShow(isHidden: Bool) {
        self.layoutIfNeeded()
            if isAnimationCompleted{
                isAnimationCompleted = false
                UIView.animate(withDuration: 0.3, animations: {
                    self.viewForImageView.isHidden = isHidden
                    self.lblLiner.isHidden = !isHidden
                    self.heightsOfTableView.constant = isHidden ? self.tableViewHeight + self.imageHeight : self.tableViewHeight
                    self.layoutIfNeeded()
                }) { (isCompleted) in
                    self.viewForImageView.isHidden = isHidden
                    self.lblLiner.isHidden = !isHidden
                    self.finishAnimation(isHidden: isHidden)
                }
            }
        }
    private func finishAnimation(isHidden: Bool) {
        UIView.animate(withDuration: 0.1, animations: {
            self.heightsOfTableView.constant = isHidden ? self.tableViewHeight + self.imageHeight + 36 : self.tableViewHeight + 36
            self.layoutIfNeeded()
        }) { (isCompleted) in
            self.isAnimationCompleted = true
            self.heightsOfTableView.constant = isHidden ? self.tableViewHeight + self.imageHeight + 36 : self.tableViewHeight + 36
        }
    }
}

extension ItemDetailView: MenuTableViewCellDelegate {
    func actionOnRemoveItemButton(item: Item, section: Int, row: Int) {
        if (viewModel?.getCustomisationsAt(section).maxQuantityLimit ?? 0) > 0 {
        }
        menuTableView.reloadData()
    }
    
    func actionOnAddItemButton(item: Item, section: Int, row: Int) {
        if (viewModel?.getCustomisationsAt(section).maxQuantityLimit ?? 0) > 0 {
        }
        menuTableView.reloadData()
    }
    
    
}

func getMainView() -> UIView {
    return UIApplication.shared.keyWindow ?? UIApplication.shared.windows.last ?? UIApplication.shared.windows[0]
}
