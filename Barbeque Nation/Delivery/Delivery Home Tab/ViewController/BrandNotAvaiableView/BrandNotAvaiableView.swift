//
 //  Created by Mahmadsakir on 08/08/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified BrandNotAvaiableView.swift
 //

import UIKit


class BrandNotAvaiableView: UIView {
    
    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var backgroundAlpha: UIView!
    @IBOutlet weak var imgBrandLogo: UIImageView!
    @IBOutlet weak var btnSelectBranch: UIButton!
    @IBOutlet weak var btnSelectBrand: UIButton!
    @IBOutlet weak var lblMessageData: UILabel!
    
    
    
    var data: BrandInfo?
    var delegate: BrandNotAvaiableViewDelegate?
    var superVC: UIViewController?
    
    
    //MARK:- View Configuration
    func initWith(data: BrandInfo?, delegate: BrandNotAvaiableViewDelegate) -> BrandNotAvaiableView {
        let view = loadFromNib()!
        view.data = data
        view.styleUI()
        view.delegate = delegate
        return view
    }
        
        
    func loadFromNib() -> BrandNotAvaiableView? {
        if let views = Bundle.main.loadNibNamed("BrandNotAvaiableView", owner: self, options: nil), let view = views[0] as? BrandNotAvaiableView{
            view.frame = UIApplication.shared.windows[0].bounds
            return view
        }
        return nil
    }
    
    
    func addToSuperView(view: UIViewController?) {
        AnalyticsHelper.shared.triggerEvent(type: .D15)
        superVC = view
        superVC?.tabBarController?.tabBar.isHidden = true
        
        if let view = view{
            view.view.addSubview(self)
        }else{
            getMainView().addSubview(self)
        }
        self.backgroundAlpha.alpha = 0.0
        self.viewForContainer.isHidden = true
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
            self.backgroundAlpha.alpha = 0.5
            self.viewForContainer.isHidden = false
        } completion: { (isCompleted) in
            self.backgroundAlpha.alpha = 0.5
            self.viewForContainer.isHidden = false
        }
    }
    
    func removeToSuperView() {
        self.viewForContainer.isHidden = false
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
            self.backgroundAlpha.alpha = 0.0
            self.viewForContainer.isHidden = true
        } completion: { (isCompleted) in
            self.backgroundAlpha.alpha = 0.0
            self.removeFromSuperview()
        }
        
        superVC?.tabBarController?.tabBar.isHidden = false
        superVC = nil
    }
    
    func styleUI() {
        viewForContainer.roundCorners(cornerRadius: 5, borderColor: .clear, borderWidth: 0)
        btnSelectBranch.setTitleColor(.deliveryThemeTextColor, for: .normal)
        btnSelectBranch.backgroundColor = .deliveryThemeColor
        btnSelectBrand.setTitleColor(.deliveryThemeColor, for: .normal)
        btnSelectBrand.backgroundColor = .clear
        btnSelectBrand.roundCorners(cornerRadius: 5.0, borderColor: .deliveryThemeColor, borderWidth: 1)
        btnSelectBranch.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0)
        imgBrandLogo.setImagePNG(url: data?.logo ?? "")
    }
    
    @IBAction func onClickOnBackground(_ sender: Any) {
        //removeToSuperView()
    }
    @IBAction func onClickBrnSelectBranch(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .D15A)
        removeToSuperView()
        delegate?.clickOnSelectBranch(view: self)
        
    }
    @IBAction func onClickBtnSelectBrand(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .D15B)
        removeToSuperView()
        delegate?.clickOnSelectBrand(view: self)
    }
}

protocol BrandNotAvaiableViewDelegate {
    func clickOnSelectBranch(view: BrandNotAvaiableView)
    func clickOnSelectBrand(view: BrandNotAvaiableView)
}
