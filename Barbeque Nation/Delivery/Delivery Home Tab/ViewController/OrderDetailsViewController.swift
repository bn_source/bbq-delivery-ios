//
 //  Created by Dharani Sadasivam on 01/11/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified OrderDetailsViewController.swift
 //

import UIKit
import MapKit
import ShimmerSwift
import AMPopTip

class OrderDetailsViewController: BaseViewController {

    @IBOutlet weak var orderStatusIcon: UIImageView!
    @IBOutlet  var mainViewHCons: NSLayoutConstraint!
    @IBOutlet weak var totalAmountView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet  var homeAddressHCons: NSLayoutConstraint!
    @IBOutlet  var restAddrHCons: NSLayoutConstraint!
    @IBOutlet  var billDetailsHCons: NSLayoutConstraint!
    @IBOutlet weak var totalAmount: UILabel!
    @IBOutlet weak var itemTableView: UITableView!
    @IBOutlet weak var billDetailsTableView: UITableView!
    @IBOutlet weak var restAddressLbl: UILabel!
    @IBOutlet weak var restaurantLbl: UILabel!
    @IBOutlet weak var homeAddressLbl: UILabel!
    @IBOutlet weak var resAddressView: UIView!
    @IBOutlet weak var homeAddressView: UIView!
    @IBOutlet weak var orderStatus: UILabel!
    @IBOutlet weak var orderId: UILabel!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lblHomeAddressTag: UILabel!
    @IBOutlet weak var BranchPhoneNoLabel: UILabel!
    @IBOutlet weak var DeliveryOTPTextLabel: UILabel!

    @IBOutlet weak var viewForDeliveryTime: UIView!
    @IBOutlet weak var btnTrackOrder: UIButton!
    @IBOutlet weak var viewForScheduleOrder: UIView!
    @IBOutlet weak var lblScheduleOrder: UILabel!
    @IBOutlet weak var lblPhoneNumber : UILabel!
    @IBOutlet weak var imgViewFoodPackage : UIImageView!// To display vertical image for takaway order
    @IBOutlet weak var ViewMap: UIView!
    @IBOutlet weak var mkMapView: MKMapView!
    @IBOutlet weak var transactionTypeView: UIView! // hide when it is deliveryorder
    @IBOutlet weak var viewStatus: UIView!
    @IBOutlet weak var scheduledDeliveryStackView: UIStackView!
    @IBOutlet weak var btnRateUs: UIButton!
    @IBOutlet weak var locationView : UIView!
    @IBOutlet weak var imgCallDelivery: UIImageView!
    
    @IBOutlet weak var LocationAndCallStackView: UIStackView!
    @IBOutlet weak var deliveryOTP: UILabel!
    @IBOutlet weak var lblDeliveryDateandTime: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var stackViewForBilling: UIStackView!
    @IBOutlet weak var heightForItemTableVIew: NSLayoutConstraint!
    @IBOutlet weak var brandLogo: UIImageView!
    @IBOutlet weak var imgHeaderBrandLogo: UIImageView!
    @IBOutlet weak var lblOrderId: UILabel!
    @IBOutlet weak var lblOrderDate: UILabel!
    
    
    @IBOutlet weak var labelForOrderReceived: UILabel!
    @IBOutlet weak var lineForOrderReceived: UIImageView!
    @IBOutlet weak var imgForOrderReceived: UIImageView!

    @IBOutlet weak var labelForOrderConfirmed: UILabel!
    @IBOutlet weak var lineForOrderCofirmed: UIImageView!
    @IBOutlet weak var imgForOrderCofirmed: UIImageView!

    @IBOutlet weak var labelForOrderPrepared: UILabel!
    @IBOutlet weak var lineForOrderPrepared: UIImageView!
    @IBOutlet weak var imgForOrderPrepared: UIImageView!

    @IBOutlet weak var labelForOrderDispatched: UILabel!
    @IBOutlet weak var lineForOrderDispatched: UIImageView!
    @IBOutlet weak var imgForOrderDispatched: UIImageView!

    @IBOutlet weak var labelForOrderDelivered: UILabel!
    @IBOutlet weak var lineForOrderDelivered: UIImageView!
    @IBOutlet weak var imgForOrderDelivered: UIImageView!

    
    var orderViewModel: OrderHistoryViewModel?
    var orderIdVal: String?
    var orderCreatedOn: String?
    var orderDetails: Order?
    
    @IBOutlet weak var greenLineAfterPickedUpLabelStatusView: UIImageView!
    @IBOutlet weak var deliveredStatusView: UIView!
    @IBOutlet weak var preparedStatusView: UIView!
    var bottomSheetController: BottomSheetController?
    var popTip = PopTip()

    
    //The flow fie;ld is required for location on map
    private let locationManager = CLLocationManager()
    private var currentCoordinate: CLLocationCoordinate2D?
    @IBOutlet weak var bottomconstraintToRateView: NSLayoutConstraint!
    
    @IBOutlet weak var shimmerContainer: UIView!
    
    var shimmerView : ShimmeringView?
    @IBOutlet weak var shimmerViewDetails: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        shimmerView = ShimmeringView(frame: self.shimmerViewDetails.bounds)
        shimmerView?.center.x = self.shimmerViewDetails.center.x // for horizontal
        
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: 80)
        bottomSheetController = BottomSheetController(configuration: configuration)
        popTip.bubbleColor = .white
        popTip.cornerRadius = 5.0
        popTip.shouldShowMask = true
        self.brandLogo.roundCorners(cornerRadius: 5, borderColor: .clear, borderWidth: 0)
        configureLocationServices()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize"{
            self.heightForItemTableVIew.constant = self.itemTableView.contentSize.height
            self.billDetailsHCons.constant = self.billDetailsTableView.contentSize.height
            self.view.layoutSubviews()
        }
    }
    
    
    private func showShimmering() {
        self.shimmerView?.isHidden = false
        shimmerContainer.isHidden = false
        shimmerViewDetails.isHidden = false
        if let shimmer = self.shimmerView, !shimmerContainer.subviews.contains(shimmer){
            self.shimmerContainer.addSubview(shimmer)
            shimmer.contentView = shimmerViewDetails
            shimmer.shimmerAnimationOpacity = 0.2
            shimmer.shimmerSpeed = 500.00
            shimmer.isShimmering = true
        }

    }

        private func stopShimmering() {
        
            if let shimmer = self.shimmerView {
                shimmer.isShimmering = false
                shimmer.isHidden = true
            }
            
            self.shimmerView?.isHidden = true
            shimmerContainer.isHidden = true
            shimmerViewDetails.isHidden = true

        }
    
    
    private func addAnnotations() {
        
        let sourceAnnotation = MKPointAnnotation()
        sourceAnnotation.title = "Source"
        let currentLocation = getCurrentLocation()
        sourceAnnotation.coordinate = currentLocation
        
        let destinationAnnotation = MKPointAnnotation()
        destinationAnnotation.title = "Destination"
       
        sourceAnnotation.coordinate = currentLocation

        if orderDetails != nil {
            
            if orderDetails?.transaction_type == 1 {
                
                //delivery order
                sourceAnnotation.coordinate = currentLocation
                destinationAnnotation.coordinate = CLLocationCoordinate2D(latitude:  Double(orderDetails!.branchLatitude) ?? 0.0 , longitude:  Double(orderDetails!.branchLongitude) ?? 0.0 )
                
            }else{
                
                //pick up order
                destinationAnnotation.coordinate = currentLocation
                sourceAnnotation.coordinate = CLLocationCoordinate2D(latitude:  Double(orderDetails!.branchLongitude) ?? 0.0 , longitude:  Double(orderDetails!.branchLongitude) ?? 0.0 )
            }
           
            mkMapView.addAnnotation(destinationAnnotation)

        }
        mkMapView.addAnnotation(sourceAnnotation)
        
        
        
        // NYC
         let p1 = MKPlacemark(coordinate: sourceAnnotation.coordinate)

         // Boston
         let p2 = MKPlacemark(coordinate:  destinationAnnotation.coordinate)

         let request = MKDirections.Request()
         request.source = MKMapItem(placemark: p1)
         request.destination = MKMapItem(placemark: p2)
         request.transportType = .automobile

         let directions = MKDirections(request: request)
         directions.calculate { response, error in
           guard let route = response?.routes.first else { return }
             self.mkMapView.addOverlay(route.polyline)
             self.mkMapView.setVisibleMapRect(
             route.polyline.boundingMapRect,
             edgePadding: UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20),
             animated: true)
         }
        
        zoomToLatestLocation(with: destinationAnnotation.coordinate)

    }
    
    private func setHeaderData(){
        let orderNumber = NSMutableAttributedString(string: "ORDER", attributes: [NSAttributedString.Key.font: UIFont.appThemeRegularWith(size: 16.0), NSAttributedString.Key.foregroundColor: UIColor.text])
        let orderNumberID =
        NSAttributedString(string: " #" + (String(orderDetails?.orderId ?? "")), attributes: [NSAttributedString.Key.font: UIFont.appThemeMediumWith(size: 16.0), NSAttributedString.Key.foregroundColor: orderDetails?.backgroudColor ?? UIColor.deliveryThemeColor] )
      
        orderNumber.append(orderNumberID)
        lblOrderId.attributedText = orderNumber
        lblOrderDate.text = orderCreatedOn
        imgHeaderBrandLogo.setImagePNG(url: orderDetails?.brand_logo ?? "")
    }

    func setStatusView(){
        
        //create two dummy locations , change it with server data
        let loc1 =  CLLocationCoordinate2D(latitude: Double(orderDetails!.branchLongitude) ?? 0.0 , longitude: Double(orderDetails!.branchLongitude) ?? 0.0 )
        
        let loc2 = getCurrentLocation()

        mkMapView.delegate = self

        //find route
        showRouteOnMap(pickupCoordinate: loc1, destinationCoordinate: loc2)
        addAnnotations()
        setHeaderData()
        self.orderId.text = String(format: "#%@", orderDetails?.orderId ?? "")
        self.lblTotalAmount.text = "\(getCurrency())\(orderDetails?.finalAmount ?? "0.0")"
        self.stackViewForBilling.isHidden = false
        
        deliveredStatusView.isHidden = false
        greenLineAfterPickedUpLabelStatusView.isHidden = false
        
        
        if orderDetails?.is_schedule_delivery ?? false{
            imgForOrderReceived.image = UIImage(named: "StatusDoing")
            imgForOrderCofirmed.image = UIImage(named: "StatusNotAttended")
            imgForOrderPrepared.image = UIImage(named: "StatusNotAttended")
            imgForOrderDispatched.image = UIImage(named: "StatusNotAttended")
            imgForOrderDelivered.image = UIImage(named: "StatusNotAttended")
            mkMapView.isHidden = true
            btnRateUs.isHidden = true
        }
        setTitleForOrderDetails()

        if orderDetails?.deliveryStatus ?? .pending == .cancelled  {
            // if order status is cancell , hide all information accept item details table
            resAddressView.isHidden = false
            mkMapView.isHidden = true
            LocationAndCallStackView.isHidden = false
            btnRateUs.isHidden = true
         //   return
        }
        
            //This will be changed once we get correct data from server
        if orderDetails?.transaction_type == nil {
            orderDetails?.transaction_type = 2
        }
        if orderDetails?.transaction_type == 1 {
            
            //Its delivery order
            transactionTypeView.isHidden = true
            viewStatus.isHidden = false
            imgViewFoodPackage.isHidden = true
            resAddressView.isHidden = false
            locationView.isHidden = true
            deliveryOTP.text =  String(format: "%@",  orderDetails?.ordeOTP ?? "" )
            lblDeliveryDateandTime.text =  String(format: "Delivery at : %@",  orderDetails?.schedule_delivery_time ?? "" )
            labelForOrderDispatched.text = "Dispatched"
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"
            let dateFromString = dateFormatter.date(from: orderDetails?.schedule_delivery_time ?? "")
            if orderDetails?.RiderPhoneNumber   != "" {
            lblPhoneNumber.text =  orderDetails?.RiderPhoneNumber
            }
            dateFormatter.dateFormat = " EEEE MMM dd , yyyy"
            
            if dateFromString != nil  {
                
                var dateRetrived = ""
                if let date = dateFromString {
                    dateRetrived = dateFormatter.string(from: date)
                }
                
                dateFormatter.dateFormat = "hh:mm a"
                if let date = dateFromString {
                    lblDeliveryDateandTime.text = String(format: "Delivery at : %@ %@",   dateRetrived , dateFormatter.string(from: date) )
                }
            }
            
        }else{
            
            //its takeway order
            self.orderId.text = String(format: "#%@", orderDetails?.orderId ?? "")
            resAddressView.isHidden = true
            homeAddressView.isHidden = false
            transactionTypeView.isHidden = false
            // in takeaway case also show view status , only hide last field delivered
            viewStatus.isHidden = false
            greenLineAfterPickedUpLabelStatusView.isHidden = true
            deliveredStatusView.isHidden = true
            preparedStatusView.isHidden = true
            imgViewFoodPackage.isHidden = false
            locationView.isHidden = false // if take way order, show branch location on map
            labelForOrderDispatched.text = "Picked-up"

        }
        
        if orderDetails?.branchPhoneNumber  != "" {
            
            BranchPhoneNoLabel.text = orderDetails?.branchPhoneNumber
        }
        
        
        if let number = orderDetails?.RiderPhoneNumber, number.count > 0 {
            imgCallDelivery.isHidden = false
        }else{
            imgCallDelivery.isHidden = true
        }
       

            self.itemTableView.reloadData{
                self.heightForItemTableVIew.constant = self.itemTableView.contentSize.height
            }
            self.billDetailsTableView.reloadData {
                //
                self.billDetailsTableView.layoutSubviews()
                self.view.setNeedsLayout()
                self.view.layoutIfNeeded()
                self.setContentViewRadius()
            }
        
        self.viewDidLayoutSubviews()
        
    }
    
    func setTitleForOrderDetails() {


       // var txt =  "accepted"
        switch  (orderDetails?.current_status.lowercased()) {

        case "cancelled":
            AnalyticsHelper.shared.triggerEvent(type: .OS05B)
            labelForOrderReceived.text = "Cancelled"
            imgForOrderCofirmed.image = UIImage(named: "StatusNotAttended")
            imgForOrderPrepared.image = UIImage(named: "StatusNotAttended")
            imgForOrderDelivered.image = UIImage(named: "StatusNotAttended")
            imgForOrderReceived.image = orderViewModel?.getDeliveryStatusImg(DeliveryStatus(rawValue: orderDetails?.status ?? "Cancelled") ?? .cancelled )
            imgForOrderDispatched.image = UIImage(named: "StatusNotAttended")
            mkMapView.isHidden = true
            LocationAndCallStackView.isHidden = false
            btnRateUs.isHidden = true
            self.stackViewForBilling.isHidden = false
            
            break
            
        case "received" :
            
            AnalyticsHelper.shared.triggerEvent(type: .OS05A)

            imgForOrderCofirmed.image = UIImage(named: "StatusNotAttended")
            imgForOrderPrepared.image = UIImage(named: "StatusNotAttended")
            imgForOrderDelivered.image = UIImage(named: "StatusNotAttended")
            imgForOrderReceived.image = UIImage(named: "StatusDoing")
            imgForOrderDispatched.image = UIImage(named: "StatusNotAttended")
            mkMapView.isHidden = true
             btnRateUs.isHidden = true
            break


        case "accepted":
            AnalyticsHelper.shared.triggerEvent(type: .OS05B)

            imgForOrderCofirmed.image = UIImage(named: "StatusDoing")
            imgForOrderPrepared.image = UIImage(named: "StatusNotAttended")
            imgForOrderDelivered.image = UIImage(named: "StatusNotAttended")
            imgForOrderReceived.image = UIImage(named: "StatusDone")
            imgForOrderDispatched.image = UIImage(named: "StatusNotAttended")
            mkMapView.isHidden = true
             btnRateUs.isHidden = true

            break
        case "prepared":

            AnalyticsHelper.shared.triggerEvent(type: .OS05C)
            imgForOrderCofirmed.image = UIImage(named: "StatusDone")
            imgForOrderPrepared.image = UIImage(named: "StatusDoing")
            imgForOrderDelivered.image = UIImage(named: "StatusNotAttended")
            imgForOrderReceived.image = UIImage(named: "StatusDone")
            imgForOrderDispatched.image = UIImage(named: "StatusNotAttended")
            mkMapView.isHidden = false
            btnRateUs.isHidden = true

            break

        case "dispatched":

            AnalyticsHelper.shared.triggerEvent(type: .OS05D)

            imgForOrderCofirmed.image = UIImage(named: "StatusDone")
            imgForOrderPrepared.image = UIImage(named: "StatusDone")
            imgForOrderDelivered.image = UIImage(named: "StatusNotAttended")
            imgForOrderReceived.image = UIImage(named: "StatusDone")
            imgForOrderDispatched.image = UIImage(named: "StatusDoing")
            mkMapView.isHidden = false
            btnRateUs.isHidden = true
            break


        case "delivered" :

            AnalyticsHelper.shared.triggerEvent(type: .OS05E)

            imgForOrderCofirmed.image = UIImage(named: "StatusDone")
            imgForOrderPrepared.image = UIImage(named: "StatusDone")
            imgForOrderDelivered.image = UIImage(named: "StatusDoing")
            imgForOrderReceived.image = UIImage(named: "StatusDone")
            imgForOrderDispatched.image = UIImage(named: "StatusDone")

           if orderDetails?.transaction_type == 1 {
               mkMapView.isHidden = false
           }else {
                    //takeaway order
                   imgForOrderDispatched.image = UIImage(named: "StatusDoing")

           }
            btnRateUs.isHidden = orderDetails?.foodIsRated == true ?true :false
       
            bottomconstraintToRateView.constant = orderDetails?.foodIsRated == true ? 0 :80
            break

        default:
            print("This condition should not occur")
        }

    }
    func styleUI() {
        contentView.setCornerRadius(12)
        viewForScheduleOrder.setCornerRadius(5.0)
    }
    
    func setContentViewRadius() {
        contentView.backgroundColor = nil

        contentView.layer.backgroundColor = UIColor.white.cgColor
        contentView.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.29)
        contentView.layer.shadowOffset = CGSize(width: 0, height: 3)
        contentView.layer.shadowOpacity =  0.5
        contentView.layer.shadowRadius = 6
        self.contentView.layer.shadowPath = UIBezierPath(roundedRect: self.contentView.bounds, cornerRadius: 22).cgPath
        self.contentView.layer.shouldRasterize = true
        self.contentView.layer.rasterizationScale = UIScreen.main.scale
    }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)

        billDetailsTableView.addObserver(self, forKeyPath: "contentSize", options: .prior, context: nil)
        itemTableView.addObserver(self, forKeyPath: "contentSize", options: .prior, context: nil)

        guard let orderIdVal = orderIdVal, let orderViewModel = orderViewModel else {
            return
        }
        showShimmering()
        orderViewModel.getOrderDetails(orderIdVal) { [self] (orderDetail) in
            print("order detail \(orderDetail)")
            stopShimmering()
            self.brandLogo.setImagePNG(url: orderDetail.brand_logo, placeholderImage: nil)
            self.orderDetails = orderDetail
            self.restAddressLbl.text = orderDetail.branchAddress
            self.homeAddressLbl.text = orderDetail.deliveryAddress
         //   self.totalAmount.text = "\(getCurrency())\(orderDetail.finalAmount)"
          //  self.orderStatusIcon.setImage(orderViewModel.getDeliveryStatusImg(orderDetail.deliveryStatus))
            self.restaurantLbl.text = orderDetail.branchName
            self.mkMapView.isHidden = orderDetail.deliveryStatus == .pending ?false :true
            if orderDetail.deliveryAddress != ""{
                
            }
            self.homeAddressView.isHidden = orderDetail.deliveryAddress != "" ? false : true
            lblPhoneNumber.text = ""
           setStatusView()
            
            
        
            
//            self.btnTrackOrder.isHidden = true
//            if let status = self.orderDetails?.deliveryStatus, status == .pending{
//                self.btnTrackOrder.isHidden = false
//            }
            
            if let is_schedule_delivery = self.orderDetails?.is_schedule_delivery, is_schedule_delivery{
                self.scheduledDeliveryStackView.isHidden = false
                self.viewForDeliveryTime.isHidden = true
//                self.btnTrackOrder.isHidden = true
                //btnRateYourOrder.isHidden = true
                lblScheduleOrder.attributedText = self.orderDetails?.schedule_delivery_time.getScheduleDeluveryString(isAfterPlaced: true, isNextLine: true, interval: self.orderDetails?.lst_delivery_schedule_interval ?? 1800, format: "MM/dd/yyyy hh:mm:ss a", fontSize: 14).0
            }else{
                self.scheduledDeliveryStackView.isHidden = true
                self.viewForDeliveryTime.isHidden = false
            }
        }
        hideNavigationBar(true, animated: true)

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        billDetailsTableView.removeObserver(self, forKeyPath: "contentSize")
        itemTableView.removeObserver(self, forKeyPath: "contentSize")

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        hideNavigationBar(true, animated: true)
    }
    
    override func viewDidLayoutSubviews() {
        
        super.viewDidLayoutSubviews()
    }
    
    @IBAction func onClickBtnTrackOrder(_ sender: Any) {
       
        let deliveryOrderDetailsVC = UIUtils.viewController(storyboardId: "OrderStatus", vcId: "BBQOrderStatusVIewControllerViewController") as! BBQOrderStatusVIewControllerViewController
         deliveryOrderDetailsVC.viewModel = DeliveryStatusViewModel()
         deliveryOrderDetailsVC.orderId = orderDetails?.orderId ?? ""
       // deliveryOrderDetailsVC.orderViewModel = orderViewModel
         self.navigationController?.pushViewController(deliveryOrderDetailsVC, animated: true)
        
    }
    
    @IBAction func RateYourOrder(_ sender: Any) {
        
        let rateViewController = UIStoryboard.loadFeedBackViewController()
        if let order = orderDetails {
            rateViewController.orderID = order.orderId
            rateViewController.transactionType = order.transaction_type ?? 1
            rateViewController.branchName = order.branchName
            rateViewController.brandlogo = order.brand_logo
            rateViewController.orderDate = order.createdOn
            rateViewController.deliveryThemeColor = order.backgroudColor
            rateViewController.deliveryThemeTextColor = order.textColor
            rateViewController.feedbackDelegate = self
            rateViewController.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: view.frame.width, height: 480)
           bottomSheetController?.present(rateViewController, on: self, viewHeight: 480)
        }
        
    }
    
    @IBAction func backButtonPressed(_ sender : UIButton){
        
        self.navigationController?.popViewController(animated: true)
    }
}

extension OrderDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == itemTableView{
            return orderDetails?.items.count ?? 0
        }else if tableView == billDetailsTableView{
            return (orderDetails?.listOfTaxCharges.count ?? 0)
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == itemTableView{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ItemTableViewCell", for: indexPath) as? ItemTableViewCell else { return UITableViewCell() }
            if let item = orderDetails?.items[indexPath.row] {
                cell.setupItem(item)
            }
            return cell
        }else if tableView == billDetailsTableView{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ItemTaxTableViewCell", for: indexPath) as? ItemTaxTableViewCell, let data = orderDetails?.listOfTaxCharges[indexPath.row] else{ return UITableViewCell() }
            cell.setCellData(taxBreakUp: data, index: indexPath.row)
            cell.delegate = self
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == itemTableView {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "ItemTableViewCell") as? ItemTableViewCell, let item = orderDetails?.items[indexPath.row] {
                cell.itemAmountLbl.text = "\(getCurrency())\(item.total)"
                cell.itemNmLbl.text = "\(item.name)   x \(item.quantity)"
                cell.layoutIfNeeded()
                let height = "\(item.name)   x \(item.quantity)".height(withConstrainedWidth: cell.itemNmLbl.frame.width, font: cell.itemNmLbl.font)
                return (height + 42)

            }
        }
        return  36
    }
}
extension OrderDetailsViewController  : MKMapViewDelegate{
    
    
    func getCurrentLocation() -> CLLocationCoordinate2D{
        //Checking whether location is enabled in Settings

        var userLatitudeValue:Double = 0.0
        var userLongitudeValue:Double = 0.0
        if(BBQLocationManager.shared.currentLocationManager.location != nil){
            userLatitudeValue = (BBQLocationManager.shared.currentLocationManager.location?.coordinate.latitude) ?? 0
            userLongitudeValue = (BBQLocationManager.shared.currentLocationManager.location?.coordinate.longitude) ?? 0
        }else if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
               case .restricted, .denied:
                  //  showLocationAlert()
                    break
            case .authorizedAlways, .authorizedWhenInUse, .authorized:
                   break
            default:
                break
            }
       }
        return CLLocationCoordinate2D(latitude: userLatitudeValue, longitude: userLongitudeValue)
    }
    
    
    func showRouteOnMap(pickupCoordinate: CLLocationCoordinate2D, destinationCoordinate: CLLocationCoordinate2D) {
        
        let request = MKDirections.Request()
        request.source = MKMapItem(placemark: MKPlacemark(coordinate: pickupCoordinate, addressDictionary: nil))
        request.destination = MKMapItem(placemark: MKPlacemark(coordinate: destinationCoordinate, addressDictionary: nil))
        request.requestsAlternateRoutes = true
        request.transportType = .automobile
        
        let directions = MKDirections(request: request)
        
        directions.calculate { [unowned self] response, error in
            guard let unwrappedResponse = response else { return }
            
            //for getting just one route
            if let route = unwrappedResponse.routes.first {
                //show on map
                self.mkMapView.addOverlay(route.polyline)
                //set the map area to show the route
                self.mkMapView.setVisibleMapRect(route.polyline.boundingMapRect, edgePadding: UIEdgeInsets.init(top: 80.0, left: 20.0, bottom: 100.0, right: 20.0), animated: true)
            }
            
            //if you want to show multiple routes then you can get all routes in a loop in the following statement
            //for route in unwrappedResponse.routes {}
        }
    }
    private func zoomToLatestLocation(with coordinate: CLLocationCoordinate2D) {
        let zoomRegion = MKCoordinateRegion(center: coordinate, latitudinalMeters: 10000, longitudinalMeters: 10000)
        mkMapView.setRegion(zoomRegion, animated: true)
    }
    
    private func configureLocationServices() {
        locationManager.delegate = self
        let status = CLLocationManager.authorizationStatus()
        
        if status == .notDetermined {
            locationManager.requestAlwaysAuthorization()
        } else if status == .authorizedAlways || status == .authorizedWhenInUse {
            beginLocationUpdates(locationManager: locationManager)
        }
    }
    
    private func beginLocationUpdates(locationManager: CLLocationManager) {
        mkMapView.showsUserLocation = true
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.blue
        renderer.lineWidth = 3.0
        return renderer
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "AnnotationView")
        
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "AnnotationView")
        }
        
        if let title = annotation.title, title == "Source" {
            annotationView?.image = UIImage(named: "SourceLocation")
        } else if let title = annotation.title, title == "Destination" {
            annotationView?.image = UIImage(named: "DestinationLocatiopn")
        }
        
        annotationView?.canShowCallout = true
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        print("The annotation was selected: \(String(describing: view.annotation?.title))")
    }
    
}
    
extension OrderDetailsViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("Did get latest location")
        
        guard let latestLocation = locations.first else { return }
        
        if currentCoordinate == nil {
            addAnnotations()
            zoomToLatestLocation(with: latestLocation.coordinate)

        }
        
        currentCoordinate = latestLocation.coordinate
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print("The status changed")
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            beginLocationUpdates(locationManager: manager)
        }
    }
}
extension OrderDetailsViewController :FeedbackDetailedDelegate {
    
    func actionOnDone(viewController : FeedbackDetailedViewController){
    
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
extension OrderDetailsViewController {
    
    
    @IBAction func onClickBtnRestaurantLocation(_ sender: Any) {
        if let strLatitude = orderDetails?.branchLatitude, let strLongitude = orderDetails?.branchLatitude, let latitude = Double(strLatitude), let longitude = Double(strLongitude) {
            
            
            let directionBottomSheet = Bundle.main.loadNibNamed("BookingRedirectionBottomSheet", owner: nil)!.first as! BookingRedirectionBottomSheet
            directionBottomSheet.latitude =   Double(strLatitude)
            directionBottomSheet.longitude =  Double(strLongitude)
            directionBottomSheet.bottomSheetType = .Direction
            directionBottomSheet.view.maskByRoundingCorners(cornerRadius: 10, maskedCorners: CornerMask.topCornersMask)
            if(UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
                directionBottomSheet.isGoogleMapsAvailable = true
            }
            bottomSheetController!.present(directionBottomSheet, on: self)
        }
    }
    @IBAction func callButton(_ sender: UIButton) {
        AnalyticsHelper.shared.triggerEvent(type: .OS06)
        //From api retrieve phone numbert
        if let number = orderDetails?.RiderPhoneNumber, number.count > 0 {
            callNumber(phone: number )
        }else{
            
            UIUtils.showToast(message: "Rider phone number not available")
        }
    }
    @IBAction  func callBranch(_ sender: UIButton) {
       // AnalyticsHelper.shared.triggerEvent(type: .OS06)
        //From api retrieve phone numbert
        if let number = orderDetails?.branchPhoneNumber, number.count > 0 {
            callNumber(phone: number )
        }else{
            
            UIUtils.showToast(message: "Branch phone number not available")
        }
    }
    
    func callNumber(phone : String)
    {
        let phoneUrl = URL(string: "telprompt://\(phone.replacingOccurrences(of: " ", with: ""))")
        let phoneFallbackUrl = URL(string: "tel://\(phone.replacingOccurrences(of: " ", with: ""))")
        if(phoneUrl != nil && UIApplication.shared.canOpenURL(phoneUrl!)) {
            UIApplication.shared.open(phoneUrl!) { (success) in
                if(!success) {
                    // Show an error message: Failed opening the url
                }
            }
        } else if(phoneFallbackUrl != nil && UIApplication.shared.canOpenURL(phoneFallbackUrl!)) {
            UIApplication.shared.open(phoneFallbackUrl!) { (success) in
                if(!success) {
                }
            }
        } else {
        }
    }
    }

extension OrderDetailsViewController: ItemTaxTableViewCellDelegate{
    func didTapOnBillDetails(cell: ItemTaxTableViewCell, index: Int, taxBreakup: TaxBreakUp) {
        let popupVC = BillDetailsPopVC(nibName: "BillDetailsPopVC", bundle: nil)
        popupVC.modalPresentationStyle = UIModalPresentationStyle .popover
        popupVC.preferredContentSize = CGSize(width: 170, height: 130)
        popupVC.strTitle = taxBreakup.label
        popupVC.taxBreakUp = taxBreakup.details
        popupVC.view.frame.size.width = 200 > self.view.frame.size.width ? self.view.frame.size.width : 200
        popupVC.view.frame.size.height = CGFloat(60 + (taxBreakup.details.count * 26))
        var originFrame = cell.btnDetails.frame
        originFrame.size.width = 80
        originFrame.origin.x = cell.frame.width - 60
        let frame = cell.convert(originFrame, to: self.view)
        popTip.show(customView: popupVC.view, direction: .auto, in: self.view, from: frame)
        AnalyticsHelper.shared.triggerEvent(type: .C19)
    }
}
class billDetailsTableView: UITableView {
    override func layoutSubviews() {
        if (self.window == nil) {
            return
        }
        super.layoutSubviews()
    }

}
extension UIView {

  func rootView() -> UIView {
     var view = self
     while view.superview != nil {
         view = view.superview!
     }
     return view
  }

  var isOnWindow: Bool {
     return self.rootView() is UIWindow
    }
  }
