//
 //  Created by Mahmadsakir on 26/05/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified DeliveryHomeOfferListVC.swift
 //

import UIKit

class DeliveryHomeOfferListVC: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    
    var viewModel = DeliveryHomeOfferListViewModel()
    var cms_offer_id: String = ""
    var offerListItems = [Item]()
    var isServicable: Bool = false
    var itemDetailsView: ItemDetailView?
    var bottomSheetController: BottomSheetController?
    var popupMenuView: UIViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: 80)
        bottomSheetController = BottomSheetController(configuration: configuration)
        
        viewModel.tableView = tableView
        viewModel.delegate = self
        viewModel.isServicable = isServicable
        lblTitle.text = offerListItems.first?.promotion_message
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @IBAction func onClickBtnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DeliveryHomeOfferListVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return offerListItems.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DeliveryHomeOfferListCell", for: indexPath) as! DeliveryHomeOfferListCell
        if offerListItems.count > indexPath.row{
            cell.setCellData(item: offerListItems[indexPath.row], indexPath: indexPath, isServicable: viewModel.isServicable)
            cell.delegate = self
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 152.0
    }
}

extension DeliveryHomeOfferListVC: DeliveryHomeOfferListViewModelDelegate{
    func startLoading() {
        
    }
    func stopLoading() {
        
    }
    
  
    
    func showOfferView(textMessageToDisplay : String?){
        
        if textMessageToDisplay != nil &&  textMessageToDisplay != "" {
            
            let offer =  textMessageToDisplay
            
            let tempView = ItemOfferAppliedView().initWith(message: offer ?? "" , delegate: self)
            tempView.addToSuperView(view: nil)
        }
    }

}

extension DeliveryHomeOfferListVC: DeliveryHomeOfferListCellDelegate{
    
    func actionOnRemoveItemButton(item: Item, indexPath: IndexPath?) {
        if item.quantity == 0 && item.isCustomizable {
            viewModel.removeAllSelectedItems(item)
        }
        viewModel.removeItemFromCart(item: item, indexPath: indexPath)
        tableView.reloadData()
    }
    
    func actionOnAddItemButton(item: Item, indexPath: IndexPath?) {
        guard !(BBQUserDefaults.sharedInstance.accessToken.isEmpty) else {
//            let controller = UIStoryboard.loadLoginViewController()
//            controller.delegate = self
//            controller.shouldCloseTouchingOutside = true
//            controller.currentTheme = .dark
//            controller.navigationFromScreen = .DeliveryHome
//            controller.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//            self.present(controller, animated: true, completion: nil)
            let loginViewcontroller = UIStoryboard.loadLoginThroughPhoneController()
            loginViewcontroller.navigationFromScreen = .DeliveryHome
            loginViewcontroller.delegate = self
            
        self.navigationController?.pushViewController(loginViewcontroller, animated: false)
            return
        }
        if (item.isCustomizable && item.quantity < 1) {
            moveToMenu(item, indexPath: indexPath)
        } else if item.isCustomizable && item.quantity >= 1 {
            let alertVc = UIUtils.viewController(storyboardId: "DeliveryHome", vcId: "AlertViewController") as! AlertViewController
            alertVc.item = item
            alertVc.delegate = self
            alertVc.indexPath = indexPath
            alertVc.modalPresentationStyle = .overCurrentContext
            self.present(alertVc, animated: true, completion: nil)
        } else {
            item.quantity += 1
            viewModel.addItemToCart(item: item, isRepeatedItem: false, indexPath: indexPath)
            itemDetailsView?.updateQuantity(count: item.quantity)
            tableView.reloadData()
        }
    }
    
    func actionOnImageTapped(item: Item, indexPath: IndexPath?) {
        let itemDetailsView = ItemDetailView().initWith(item: item, indexPath: indexPath, isServicable: isServicable, delegate: self)
        itemDetailsView.addToSuperView(view: self, isCameFromSearch: false)
        self.itemDetailsView = itemDetailsView
    }
    
    func moveToMenu(_ item: Item, indexPath: IndexPath?) {
        switch item.foodCategory {
        case .comboBox:
            openComboBox(item, indexPath: indexPath)
        case .comboItem:
            guard popupMenuView == nil else {
                dismissPopupMenu()
                openComboItem(item, indexPath: indexPath)
                return
            }
            openComboItem(item, indexPath: indexPath)
        default:
            print("openComboItem")
        }
    }
    
    func openComboBox(_ item: Item, indexPath: IndexPath?) {
        let vC = UIUtils.viewController(storyboardId: "DeliveryHome", vcId: "DeliveryMenuViewController") as! DeliveryMenuViewController
        vC.item = item
        vC.menuDelegate = self
        vC.indexPath = indexPath
        self.present(vC, animated: true, completion: nil)
    }
    
    func openComboItem(_ item: Item, indexPath: IndexPath?) {
        let vC = UIUtils.viewController(storyboardId: "DeliveryHome", vcId: "PopupMenuViewController") as! PopupMenuViewController
        vC.updatedItem = item
        vC.delegate = self
        vC.indexPath = indexPath
        self.popupMenuView = vC
        vC.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: view.frame.width, height: 450)
        bottomSheetController?.present(vC, on: self, viewHeight: 450)
    }
}

extension DeliveryHomeOfferListVC: ItemDetailViewDelegate, MenuViewControllerDelegate{
    func actionOnDone(_ item: Item, indexPath: IndexPath?) {
        viewModel.addItemToCart(item: item, isRepeatedItem: false, indexPath: indexPath)
        itemDetailsView?.updateQuantity(count: item.quantity)
        tableView.reloadData()
        if item.foodCategory == .comboItem {
            hidePopupMenu()
        }
    }
    
    func actionOnReduceItem(item: Item, indexPath: IndexPath?) {
        
    }
    
    func actionOnMultipleSelect(_ item: Item, indexPath: IndexPath?) {
        actionOnAddItemButton(item: item, indexPath: indexPath)
    }
    
    func didHideItemDetails(isCameFromSearch: Bool) {
        if !isCameFromSearch{
            if let tabBarController = navigationController?.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
                tabBarController.tabBar.isHidden = false
            }
        }
        itemDetailsView = nil
    }
    
    func hidePopupMenu() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func backBtnPressed(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
}

extension DeliveryHomeOfferListVC: AlertViewControllerDelegate {
    func actionOnNoBtn(_ item: Item, indexPath: IndexPath?) {
        item.quantity += 1
        viewModel.addItemToCart(item: item, isRepeatedItem: true, indexPath: indexPath)
        itemDetailsView?.updateQuantity(count: item.quantity)
        tableView.reloadData()
    }
    
    func actionOnYesBtn(_ item: Item, indexPath: IndexPath?) {
        clearSelectedItemAndMoveMenu(item, indexPath: indexPath)
    }
    func clearSelectedItemAndMoveMenu(_ item: Item, indexPath: IndexPath?) {
        viewModel.removeAllSelectedItems(item)
        moveToMenu(item, indexPath: indexPath)
    }
}

extension DeliveryHomeOfferListVC: PopupMenuDelegate {
    func dismissPopupMenu() {
        hidePopupMenu()
    }
}
extension DeliveryHomeOfferListVC: LoginControllerDelegate {
    
}



extension DeliveryHomeOfferListVC: ItemOfferAppliedViewDelegate{
    
    func actionOnDone(view: ItemOfferAppliedView){
        
        print("actionOnDone(view: ItemOfferAppliedView)")
    }
}

extension DeliveryHomeOfferListVC : ViewCartDelegate {
    

    func viewCartForAddedItem() {
        self.navigationController?.popViewController(animated: true)
        if let tabBarController = navigationController?.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
            tabBarController.selectedIndex = 1
        }
    }
    
}
