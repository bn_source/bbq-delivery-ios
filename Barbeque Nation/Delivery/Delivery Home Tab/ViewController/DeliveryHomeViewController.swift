//
//  DeliveryHomeViewController.swift
//  BBQ
//
//  Created by Rahul S on 08/08/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import UIKit
import ShimmerSwift

class DeliveryHomeViewController: BaseViewController {
    
    //MARK:- IBActions
    @IBOutlet weak var itemsCollectionView: UICollectionView!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var menuTableView: UITableView!
    @IBOutlet weak var menuSectionView: UIView!
    @IBOutlet weak var smallMenuView: UIView!
    @IBOutlet weak var bigMenuView: UIView!
    @IBOutlet weak var menuSectionTableViewWidthCons: NSLayoutConstraint!
    @IBOutlet weak var menuSectionTableViewHeightCons: NSLayoutConstraint!
    @IBOutlet weak var cycleImgView: UIImageView!
    @IBOutlet weak var viewForShimmering: UIView!
    @IBOutlet weak var viewForShimmerContainer: UIView!
    @IBOutlet weak var heightForCollectionView: NSLayoutConstraint!
    @IBOutlet weak var headerCoverPageView: CoverPageCollectionViewClass!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var statusCollectionView: UICollectionView!
    @IBOutlet weak var viewForHeaderSection: UIView!
    @IBOutlet weak var viewForSearch: UIView!
    @IBOutlet  var menubootomContraint: NSLayoutConstraint!
    @IBOutlet weak var switchForVeg: UISwitch!
    @IBOutlet weak var lblSectionHeader: UILabel!
    @IBOutlet weak var stackViewForHeader: UIView!
    @IBOutlet weak var pageControl: UIPageControl! //to show number of orders
    @IBOutlet weak var btnSmiles: UIButton!
    @IBOutlet weak var lblSectionHeaderCount: UILabel!
    @IBOutlet weak var lblBrandLogo: UIImageView!
    @IBOutlet weak var viewForBrand: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var statusCollectionViewHeightCons: NSLayoutConstraint!
    
    
    private var viewModel = DeliveryHomeViewModel()
    var bluredView: UIView?
    var headerViewSize: CGFloat = 363+50
    var popupMenuView: UIViewController?
    var itemDetailsView: ItemDetailView?
    var itemSearchView: ItemSearchView?
    var bottomSheetController: BottomSheetController?
    var shimmerView : ShimmeringView?
    var brandPickerView: BrandPickerView?
    var brandNotAvaiableView: BrandNotAvaiableView?
    var featureInfoView: FeatureInfoView?
    var isNonVegSelected = true
    var isLoadingFirstTime = true
    var isAnimationCompleted = true
    var isLimitedOfferAvailable = false
    var isCameFromLimitedOffer = false
    var moreButtonIndexes: (height: CGFloat, indexPath: IndexPath)?
    
    @IBOutlet var menuViewBottomViewHeight: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        AnalyticsHelper.shared.triggerEvent(type: .D01)
        setupUIStyle()
        setupUIContent()
        registerCells()
        addNotificationCenter()
        if #available(iOS 11.0, *) {
            itemsCollectionView.contentInsetAdjustmentBehavior = .never
        }
        viewModel.collectionView = itemsCollectionView
        viewModel.statusCollectionView = statusCollectionView
        viewModel.pageControl = pageControl
        viewModel.tableView = menuTableView
        statusCollectionView.dataSource = self
        statusCollectionView.delegate = self
        let layout = statusCollectionView.collectionViewLayout as? UICollectionViewFlowLayout
        layout?.sectionHeadersPinToVisibleBounds = true
        headerCoverPageView.delegate = self
        viewModel.coverPageCollectionViewClass = headerCoverPageView
        viewModel.smallMenuView = smallMenuView
        viewModel.delegate = self
        LocationManager.sharedInstance.delegate = self
        LocationManager.sharedInstance.initLocationManager()
        
        shimmerView = ShimmeringView(frame: viewForShimmering.bounds)
        
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: 80)
        bottomSheetController = BottomSheetController(configuration: configuration)
        setCollectionHeader()
        //BBQServiceManager.getInstance().getLocalSavedBranches {}
        AnalyticsHelper.shared.triggerEvent(type: .Home_Delivery_Takeaway)
    }
    
    private func setCollectionHeader() {
        if isLimitedOfferAvailable{
            headerViewSize = 571+50
        }else{
            headerViewSize = 363+50
        }
        itemsCollectionView.contentInset = UIEdgeInsets(top: headerViewSize, left: 0, bottom: 0, right: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        menuViewBottomViewHeight.constant = 0

        super.viewWillAppear(animated)
        self.screenName(name: .Delivery_Home_Screen)

        self.statusCollectionView.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
        self.statusCollectionView.setShadow()
        pageControl.hidesForSinglePage = true

        AnalyticsHelper.shared.triggerEvent(type: .D02)
        addObserver()
        self.hideNavigationBar(true, animated: true)
        self.tabBarController?.tabBar.isHidden = false
        AnalyticsHelper.shared.triggerEvent(type: .visited_delivery_home)
        AnalyticsHelper.shared.triggerEvent(type: .visited_delivery_home_screen)
        if !isLoadingFirstTime{
            if checkForBrandSelection(){
                
            }else if checkForBrnachDelivery(){
                if viewModel.currentLocation != nil {
                    viewModel.getRestaurantMenus(isLoadingFirsttime: isLoadingFirstTime)
                }else if BBQUserDefaults.sharedInstance.DeliveryBranchIdValue != ""{
                    viewModel.getRestaurantMenus(isLoadingFirsttime: true)
                }
            }
            getUpdatedPoints()
        }
        scrollToCollectionView()
        isLoadingFirstTime = false
        headerCoverPageView.initTimer()
        

        //checkForBrnachDelivery()
    }
    
    
    private func openBrandIntroduction(){
        if BBQUserDefaults.sharedInstance.CanMultiBrandIntroOpen{
            let rect = headerView.convert(viewForBrand.frame, to: self.view)
            featureInfoView = FeatureInfoView().initWith(rect: rect, message: "Click to explore other brands & Place orders.")
            featureInfoView?.addToSuperView(view: self)
        }
    }
    
    private func checkForBrandSelection() -> Bool{
        if let brand_id = tempSelectedBrandForDelivery{
            viewModel.setBrandToSelect(byID: brand_id)
                var location = viewModel.userLocation
                if location == nil{
                    location?.latitude = "\(BBQUserDefaults.sharedInstance.currentLatitudeValue)"
                    location?.longitude = "\(BBQUserDefaults.sharedInstance.currentLongitudeValue)"
                }
                refreshHomeBy(location ?? Location())
                
            
            return true
        }
        return false
    }
    
    private func checkForBrnachDelivery() -> Bool{
        if let branch = findBranchBy(id: BBQUserDefaults.sharedInstance.DeliveryBranchIdValue), branch.only_dining{
            //MARK: - Fix for DEL-1778 (showing the delivery not available alert instead of fetching nearby locations)
            //            let location = Location(tag: "", city: "", venue: "", street: "", state: "", country: "", latitude: String(format: "%f", branch.latitude), longitude: String(format: "%f", branch.longitude), zip: "", address: "", stateId: "", cityId: "")
            //            viewModel.getAllNearByRestaurant(location) {
            //
            //            }
            showOnlyDeliveryAlert()
            //END

            return false
        }
        return true
    }
    
    private func getUpdatedPoints() {
        if BBQUserDefaults.sharedInstance.accessToken != "" {
            self.viewModel.getLoyaltyPointsCount { (isSuccess) in
                self.headerCoverPageView.updateUserPoints()
            }
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObserver()
        headerCoverPageView.deinitTimer()
    }
    
    private func addObserver(){
//        itemsCollectionView.addObserver(self, forKeyPath: "contentSize", options: .prior, context: nil)
//        itemsCollectionView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
    }
    
    private func removeObserver(){
//        addObserver()
//        itemsCollectionView.removeObserver(self, forKeyPath: "contentSize")
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize" && heightForCollectionView.constant != itemsCollectionView.contentSize.height{
            heightForCollectionView.constant = itemsCollectionView.contentSize.height
        }
    }
    
    deinit {
        removeNotificationCenter()

    }
    
    // MARK: - showOnlyDeliveryAlert called when user select only delivery branch
    func showOnlyDeliveryAlert() {
        PopupHandler.showTwoButtonsPopup(title: "Delivery not available",
                                         message: String(format: "%@ not having delivery facility please select other branch", BBQUserDefaults.sharedInstance.DeliveryCurrentSelectedLocation),
                                         image: nil,
                                         titleImageFrame: CGRect(x: 0, y: -1, width: 16, height: 20),
                                         on: self,
                                         firstButtonTitle: "Change Branch", firstAction: {
                                            self.actionOnRestaurantSearch()
        },
                                         secondButtonTitle: "Goto Reservation") {
            if let tabBarController = self.navigationController?.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
                tabBarController.selectedIndex = 0
            }
        }
    }
    
    //MARK:- IBActions
    @IBAction func onClickBtnBrand(_ sender: Any) {
        if brandPickerView != nil {
            brandPickerView?.addToSuperView(view: self)
        }else{
            brandPickerView = BrandPickerView().initWith(data: viewModel.getAllBrandsInfo(), delegate: self)
            brandPickerView?.addToSuperView(view: self)
        }
    }
    @IBAction func actionOnBtnSearch(_ sender: Any) {
        actionOnSearchItem()
    }
    @IBAction func valueChangeOfVegSwitch(_ uiSwitch: UISwitch) {
        isNonVegSelected = !uiSwitch.isOn
        headerCoverPageView.vegNonVegSwitch.isOn = uiSwitch.isOn
        enableSwitch(uiSwitch)
        enableSwitch(headerCoverPageView.vegNonVegSwitch)
        viewModel.isNonVegSelected = isNonVegSelected
        viewModel.filterMenuItems(isShowVeg: uiSwitch.isOn)
    }
    
    private func enableSwitch(_ uiSwitch: UISwitch){
        uiSwitch.onTintColor = uiSwitch.isOn ? UIColor.darkGreen : UIColor.lightGray
        if uiSwitch.isOn{
            AnalyticsHelper.shared.triggerEvent(type: .D05)
        }else{
            AnalyticsHelper.shared.triggerEvent(type: .D05A)
        }
    }
    
    @IBAction func actionOnSearchBtn(_ sender: Any) {
        let searchVC = UIUtils.viewController(storyboardId: "DeliveryHome", vcId: "SearchViewController") as! SearchViewController
        self.present(searchVC, animated: true, completion: nil)
    }
    
    @IBAction func actionOnSearchLoc(_ sender: Any) {
        moveToSearchLoc()
    }
    
    @IBAction func actionOnMenuSectionView(_ sender: Any) {
        actionOnMenuView()
    }
    
    @IBAction func actionOnMainMenu(_ sender: Any) {
        if let rootVC = UIApplication.shared.keyWindow?.rootViewController as? DeliveryHostMenuViewController{
            rootVC.showSideMenu(viewController: self, delegate: self)
        }
//        let menuViewController = UIStoryboard.loadMenuViewController()
//        menuViewController.delegate = self
//        self.addChild(menuViewController)
//        self.view.addSubview(menuViewController.view)
//        self.view.endEditing(true)
//        menuViewController.didMove(toParent: self)
    }
    
    //MARK:- UI
    private func setupUIStyle() {
        viewForSearch.setCornerRadius(viewForSearch.frame.size.height/2.0)
        viewForSearch.setShadow()
        stackViewForHeader.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.bottomCornersMask)
        viewForHeaderSection.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.bottomCornersMask)
        viewForHeaderSection.setShadow(color: .darkGray, opacity: 0.5, offSet: .zero, radius: 10.0, scale: true)
        viewForHeaderSection.backgroundColor = .white
        smallMenuView.setCornerRadius(30)
        smallMenuView.applyViewMultiBrandTheme()
        smallMenuView.setShadow(color: .deliveryThemeColor, opacity: 0.5, offSet: .zero, radius: 30, scale: true)
        viewForBrand.setCornerRadius(5.0)
        viewForBrand.setShadow(color: .lightGray, opacity: 0.5, offSet: .zero, radius: 5.0, scale: false)
        bigMenuView.setCornerRadius(15)
        menuTableView.setCornerRadius(13)
    }
    
    private func setupUIContent() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(actionOnMenuView))
        smallMenuView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    private func showShimmering() {
        self.viewForShimmering.isHidden = false
        
        if let shimmer = self.shimmerView{
            self.viewForShimmering.addSubview(shimmer)
            shimmer.contentView = viewForShimmerContainer
            shimmer.shimmerAnimationOpacity = 0.2
            shimmer.shimmerSpeed = 500.00
            shimmer.isShimmering = true
        }
    }
    
    private func stopShimmering() {

        if let shimmer = self.shimmerView {
            shimmer.isShimmering = false
        }
        self.viewForShimmering.isHidden = true
    }
    
    @objc func actionOnMenuView() {
        if menuSectionView.isHidden {
            if viewModel.categoryList.count > 1 {
//                if !viewModel.categoryList.contains(where: {$0.isSelected}) {
//                    viewModel.categoryList[1].isSelected = true
//                }
                menuTableView.reloadData()
//                let rowIndex: Int = (viewModel.categoryList.firstIndex(where: {$0.isSelected}) ?? 0) - 1
//                menuTableView.scrollToRow(at: IndexPath(row: rowIndex, section: 0), at: .middle, animated: false)
            }
        }
        //Animation
        if menuSectionView.isHidden {
            AnalyticsHelper.shared.triggerEvent(type: .explored_menu)
            menuSectionView.alpha = 1.0
            let size = CGSize(width: menuSectionTableViewWidthCons.constant, height: menuSectionTableViewHeightCons.constant)
            menuSectionTableViewWidthCons.constant = 0
            menuSectionTableViewHeightCons.constant = 0
            menuTableView.layoutIfNeeded()
            menuSectionView.isHidden.toggle()
            menuSectionTableViewWidthCons.constant = size.width
            menuSectionTableViewHeightCons.constant = size.height
            UIView.animate(withDuration: 0.1, delay: 0, options: .curveEaseInOut, animations: {
                self.menuTableView.layoutIfNeeded()
                self.menuTableView.flashScrollIndicators()
            }, completion: nil)
            
        } else {
            menuSectionView.isHidden.toggle()
        }
    }
    
    @objc func actionOnDeliveryStatus() {


        let deliveryOrderDetailsVC = UIUtils.viewController(storyboardId: "OrderStatus", vcId: "BBQOrderStatusVIewControllerViewController") as! BBQOrderStatusVIewControllerViewController
         deliveryOrderDetailsVC.viewModel = DeliveryStatusViewModel()
         deliveryOrderDetailsVC.orderId = viewModel.openOrders.first?.orderId ?? ""
        //  deliveryOrderDetailsVC.orderViewModel = orderViewModel
         self.navigationController?.pushViewController(deliveryOrderDetailsVC, animated: true)
    }
    private func registerCells() {
        itemsCollectionView.registerHeaderView(nibName: ItemHeaderView.getCellIdentifier(), kind: UICollectionView.elementKindSectionHeader)
        statusCollectionView.registerHeaderView(nibName: ItemHeaderView.getCellIdentifier(), kind: UICollectionView.elementKindSectionHeader)
        //itemsCollectionView.registerCell(nibName: CoverPageCollectionViewCell.getCellIdentifier())
        itemsCollectionView.register(UINib(nibName: ComboBoxCollectionViewCell.getCellIdentifier(), bundle: nil), forCellWithReuseIdentifier: ComboBoxCollectionViewCell.getCellIdentifier())
        itemsCollectionView.register(UINib(nibName: ComboItemCollectionViewCell.getCellIdentifier(), bundle: nil), forCellWithReuseIdentifier: ComboItemCollectionViewCell.getCellIdentifier())
        itemsCollectionView.register(UINib(nibName: ItemCollectionViewCell.getCellIdentifier(), bundle: nil), forCellWithReuseIdentifier: ItemCollectionViewCell.getCellIdentifier())
    }
    
    
    func moveToSearchLoc() {
        AnalyticsHelper.shared.triggerEvent(type: .D04)
        let searchLocVC = UIUtils.viewController(storyboardId: "DeliveryHome", vcId: "SearchLocationViewController") as! SearchLocationViewController
        searchLocVC.addressViewModel = AddressViewModel()
        searchLocVC.selectedBranchId = viewModel.selectedBranch?.branchId
        searchLocVC.delegate = self
        self.navigationController?.pushViewController(searchLocVC, animated: true)
    }
    
    func refreshHomeBy(_ location: Location) {
        viewModel.currentLocation = location
        BBQUserDefaults.sharedInstance.currentLatitudeValue = brandSelectedLat ?? Double(location.latitude) ?? 0.00
        BBQUserDefaults.sharedInstance.currentLongitudeValue = brandSelectedLong ?? Double(location.longitude) ?? 0.00
        viewModel.userLocation = location
        var tempLocation = location
        var isClickedFromHome = false
        if let brandSelectedLat = brandSelectedLat, let brandSelectedLong = brandSelectedLong{
            tempLocation.latitude = "\(brandSelectedLat)"
            tempLocation.longitude = "\(brandSelectedLong)"
            isClickedFromHome = true
        }
        viewModel.getAllNearByRestaurantV1(tempLocation, isClickedFromHome: isClickedFromHome) {
            tempSelectedBrandForDelivery = nil
            brandSelectedLat = nil
            brandSelectedLong = nil
            if let url = self.viewModel.selectedBrand?.logo{
                self.lblBrandLogo.setImagePNG(url: url, placeholderImage: nil)
            }
            self.brandPickerView = BrandPickerView().initWith(data: self.viewModel.getAllBrandsInfo(), delegate: self)
            if let tag = location.tag{
                self.locationLbl.text = tag
            }else{
                self.locationLbl.text = "\(location.city), \(location.state)"
            }
        }
    }
    
    func moveToMenu(_ item: Item, indexPath: IndexPath?) {
        switch item.foodCategory {
        case .comboBox:
            openComboBox(item, indexPath: indexPath)
        case .comboItem:
            guard popupMenuView == nil else {
                dismissPopupMenu()
                openComboItem(item, indexPath: indexPath)
                return
            }
            openComboItem(item, indexPath: indexPath)
        default:
            print("openComboItem")
        }
    }
    
    func openComboItem(_ item: Item, indexPath: IndexPath?) {
        let vC = UIUtils.viewController(storyboardId: "DeliveryHome", vcId: "PopupMenuViewController") as! PopupMenuViewController
        vC.updatedItem = item
        vC.delegate = self
        vC.indexPath = indexPath
        self.popupMenuView = vC
        vC.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: view.frame.width, height: 450)
        bottomSheetController?.present(vC, on: self, viewHeight: 450)
    }
    
    func openComboBox(_ item: Item, indexPath: IndexPath?) {
        let vC = UIUtils.viewController(storyboardId: "DeliveryHome", vcId: "DeliveryMenuViewController") as! DeliveryMenuViewController
        vC.item = item
        vC.menuDelegate = self
        vC.indexPath = indexPath
        self.present(vC, animated: true, completion: nil)
    }
    
    func hidePopupMenu() {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func addNotificationCenter() {
        NotificationCenter.default.addObserver(self, selector: #selector(scrollMenuToTop), name: .menuScrollToTop, object: nil)
    }
    
    @objc private func scrollMenuToTop() {
        itemsCollectionView.setContentOffset(CGPoint(x: 0, y: -headerViewSize), animated: true)
        scrollViewDidScroll(itemsCollectionView)
    }
    
    private func removeNotificationCenter() {
        NotificationCenter.default.removeObserver(self)
    }
}

//MARK:- UICollectionViewDelegates
extension DeliveryHomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func loadPromotionData() {
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if(collectionView.tag == Constants.TagValues.openOrderCellTag){
            
            return 1
        }else{
            return  viewModel.categoryList.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if(collectionView.tag != Constants.TagValues.openOrderCellTag){
            
            switch kind {
            case UICollectionView.elementKindSectionHeader:
                let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: ItemHeaderView.getCellIdentifier(), for: indexPath) as! ItemHeaderView
                var count = 0
                if viewModel.categoryList.count > indexPath.section{
                    count = (viewModel.menuDict[viewModel.categoryList[indexPath.section]]?.count ?? 0)
                }
                headerView.updateView(section: viewModel.categoryList[indexPath.section], count: count)
                return headerView
            default:  fatalError("Unexpected element kind")
            }
        }else{
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: ItemHeaderView.getCellIdentifier(), for: indexPath) as! ItemHeaderView
            
            return headerView
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        if(collectionView.tag == Constants.TagValues.openOrderCellTag){
            return CGSize.zero
        }
        let height: CGFloat = section == 0 ? 25 : 41
        return CGSize(width: collectionView.frame.width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView.tag == Constants.TagValues.openOrderCellTag){
            return viewModel.openOrders.count
        }else{
        return viewModel.menuDict[viewModel.categoryList[section]]?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if(collectionView.tag == Constants.TagValues.openOrderCellTag){
            
            let openOrderCell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CellIdentifiers.openOrderCell, for: indexPath) as! BBQOpenOrderCollectionViewCell
            if (viewModel.openOrders.count > 0) {
                let  orderModel:Order = (viewModel.openOrders[indexPath.row])
                openOrderCell.tag = indexPath.row
                openOrderCell.openCellDelegate = self
                openOrderCell.rateButton.tag = indexPath.row
                openOrderCell.setData(orderDetails: orderModel)
                
                //update the collection view color depending on status

                switch  (orderModel.state.lowercased() ) {
                
                case  "received" :  openOrderCell.backgroundColor = UIColor.hexStringToUIColor(hex: "#F7B030")
                    openOrderCell.imageView.image = UIImage(named: "StatusAccepted")
                    
                    break
                case  "dispatched" :  openOrderCell.backgroundColor = UIColor.hexStringToUIColor(hex: "#F58840")
                    openOrderCell.imageView.image = UIImage(named: "StatusDispatched")
                    break
                
                case  "delivered"  : openOrderCell.backgroundColor = UIColor.hexStringToUIColor(hex: "#2B9C4C")
                    openOrderCell.imageView.image = UIImage(named: "StatusDelivered")
                    break
                 
                case  "prepared" : openOrderCell.backgroundColor = UIColor.hexStringToUIColor(hex: "#F58840")
                    openOrderCell.imageView.image = UIImage(named: "StatusDispatched")
                    break
                    
                case  "accepted" : openOrderCell.backgroundColor = UIColor.hexStringToUIColor(hex: "#F04B24")
                    openOrderCell.imageView.image = UIImage(named: "StatusAccepted")
                    break
                    
                default:
                    
                    print("Dont change color")
                }
                return openOrderCell

            }
            return openOrderCell
            
        }else {
            if viewModel.categoryList.count > indexPath.section, (viewModel.menuDict[viewModel.categoryList[indexPath.section]]?.count ?? 0) > indexPath.row{
                
            }else{
                return collectionView.defaultCell(indexPath: indexPath)
            }
            guard let item = viewModel.menuDict[viewModel.categoryList[indexPath.section]]?[indexPath.row] else {
                return collectionView.defaultCell(indexPath: indexPath)
            }
            var isHeightOpened = false
            if let index = moreButtonIndexes?.indexPath, index == indexPath{
                isHeightOpened = true
            }
            switch item.foodCategory {
            case .comboBox:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ComboBoxCollectionViewCell.getCellIdentifier(), for: indexPath) as! ComboBoxCollectionViewCell
                cell.updateCell(indexPath: indexPath, item: item, isServicable: viewModel.isServicable, isHeightOpened: isHeightOpened)
                cell.delegate = self
                return cell
            case .comboItem:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ComboItemCollectionViewCell.getCellIdentifier(), for: indexPath) as! ComboItemCollectionViewCell
                cell.updateCell(indexPath: indexPath, item: item, isServicable: viewModel.isServicable, isHeightOpened: isHeightOpened)
                cell.delegate = self
                //cell.bottomLineLabel.isHidden = !(item.name == viewModel.menuDict[viewModel.categoryList[indexPath.section]]?.last?.name)
                return cell
            case .item:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ItemCollectionViewCell.getCellIdentifier(), for: indexPath) as! ItemCollectionViewCell
                cell.updateCell(indexPath: indexPath, item: item, isServicable: viewModel.isServicable, isHeightOpened: isHeightOpened)
                cell.delegate = self
                return cell
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (collectionView.tag == Constants.TagValues.openOrderCellTag){
            if viewModel.openOrders.count < indexPath.row{
                return
            }
            if viewModel.openOrders[indexPath.row].is_schedule_delivery{
                return
            }
            
            //If order is already delivered , do not show status view controller,
            //instead directly open rating page
            if viewModel.openOrders[indexPath.row].state == "Delivered" {
                
                AnalyticsHelper.shared.triggerEvent(type: .DH05)
                let  rateViewController = UIStoryboard.loadFeedBackViewController()
                let order = viewModel.openOrders[indexPath.row]
                rateViewController.orderID = order.orderId
                rateViewController.transactionType = order.transaction_type ?? 1
                rateViewController.feedbackDelegate = self
                rateViewController.branchName = order.branchName
                rateViewController.brandlogo = order.brand_logo
                rateViewController.orderDate = order.createdOn
                rateViewController.deliveryThemeColor = order.backgroudColor
                rateViewController.deliveryThemeTextColor = order.textColor
                bottomSheetController?.present(rateViewController, on: self, viewHeight: 480)
                
                return
            }
            
            let deliveryOrderDetailsVC = UIUtils.viewController(storyboardId: "OrderStatus", vcId: "BBQOrderStatusVIewControllerViewController") as! BBQOrderStatusVIewControllerViewController
             deliveryOrderDetailsVC.viewModel = DeliveryStatusViewModel()
            if (viewModel.openOrders.count > 0) {
                guard viewModel.openOrders.count > indexPath.row else {
                   return
                }
                deliveryOrderDetailsVC.orderId =  viewModel.openOrders[indexPath.row].orderId
                deliveryOrderDetailsVC.orderTransactionType =  viewModel.openOrders[indexPath.row].transaction_type ?? 1

            }
             self.navigationController?.pushViewController(deliveryOrderDetailsVC, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if(collectionView.tag == Constants.TagValues.openOrderCellTag){
            
            return collectionView.frame.size
            
        }else{
            //let item = viewModel.menuDict[viewModel.categoryList[indexPath.section]]![indexPath.row]
            if viewModel.categoryList.count > indexPath.section, let dataCol = viewModel.menuDict[viewModel.categoryList[indexPath.section]], dataCol.count > indexPath.row{
                let item = dataCol[indexPath.row]
                var extraHeight: CGFloat = 0
                if let index = moreButtonIndexes, index.indexPath == indexPath{
                    extraHeight = index.height
                }
                switch item.foodCategory {
                case .comboBox:
//                    if indexPath.section == 0{
//                        return CGSize(width: collectionView.frame.width, height: 275)
//                    }
                    if extraHeight <= 0, let index = moreButtonIndexes, index.indexPath.section == indexPath.section{
                        if indexPath.row % 2 == 1, index.indexPath.row == indexPath.row - 1{
                            extraHeight = index.height
                        }else if indexPath.row % 2 == 0, index.indexPath.row == indexPath.row + 1{
                            extraHeight = index.height
                        }
                    }
                    
                    return CGSize(width: collectionView.frame.width/2, height: 275.0 + extraHeight)
                case .comboItem:
                    return CGSize(width: collectionView.frame.width, height: 152.5 + extraHeight)
                case .item:
                    return CGSize(width: collectionView.frame.width, height: 127.5 + extraHeight)
                }
            }  else{
                return CGSize.zero
            }
        }
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
            pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        
    }
    
    private func setSelectedCategory(sectionIndex: Int) {
        viewModel.categoryList.forEach({$0.isSelected = false})
        viewModel.categoryList[sectionIndex].isSelected = true
        self.lblSectionHeader.text = viewModel.categoryList[sectionIndex].name
        self.lblSectionHeaderCount.text = String(format: "%li Items", viewModel.menuDict[viewModel.categoryList[sectionIndex]]?.count ?? 0)
        self.lblSectionHeaderCount.applyThemeOnText()
    }
}

extension DeliveryHomeViewController: CoverPageCollectionViewClassDelegate, ComboBoxCollectionViewCellDelegate, ComboItemCollectionViewCellDelegate, ItemCollectionViewCellDelegate {
    func updateCellHeight(item: Item, indexPath: IndexPath?, height: CGFloat) {
        if let indexPath = indexPath, height > 0{
            moreButtonIndexes = (height, indexPath)
            itemsCollectionView.reloadData()
        }
    }
    
    
    func onClickBtnSmiles() {
        if (BBQUserDefaults.sharedInstance.accessToken.count == 0) {
            //guest user
            ToastHandler.showToastWithMessage(message: KLoyaltyLoginText)
        }else{
            //let  loyalityPointsViewController = UIStoryboard.loadLoyalityPointsViewController()
            let smileHistoryViewController = UIStoryboard.loadSmileHistoryViewController()
            self.navigationController?.pushViewController(smileHistoryViewController, animated: true)
        }
    }
    
    func onClickBtnEnquireNow() {
        
        AnalyticsHelper.shared.triggerEvent(type: .ML04)

        let enquireNowViewController = UIStoryboard.loadEnquireViewController()
        self.navigationController?.pushViewController(enquireNowViewController, animated: true)
    }
    
    func onClickIntroScheduleDelivery() {
        AnalyticsHelper.shared.triggerEvent(type: .D12)
        let popupVC = ScheduleDeliveryIntroVC(nibName: "ScheduleDeliveryIntroVC", bundle: nil)
        popupVC.brand_name = viewModel.selectedBrand?.name ?? "BBQN"
        bottomSheetController?.present(popupVC, on: self, viewHeight: 420)
    }
    
    func loadOfferDetails(cms_offer_id: String, image_url: String) {
        if cms_offer_id == ""{
            return
        }
        var arrItems = [Item]()
        for category in self.viewModel.originalCategoryList{
            for item in self.viewModel.originalMenuDict[category] ?? [Item](){
                if item.cms_offer_id != "", item.cms_offer_id == cms_offer_id {
                    arrItems.append(item)
                }
            }
        }
        
        if arrItems.count <= 0{
            return
        }
        
        
        let offerListVC = UIStoryboard.loadDeliveryOfferListVC()
        offerListVC.cms_offer_id = cms_offer_id
//        offerListVC.image_url = image_url
        offerListVC.isServicable = viewModel.isServicable
        UIUtils.showLoader()
        
        UIUtils.hideLoader()
        offerListVC.offerListItems = arrItems
        self.navigationController?.pushViewController(offerListVC, animated: true)
    }
    
    
    func updateQtyFromHeader(item: Item) {
        isCameFromLimitedOffer = true
    }
    
    
    func actionOnSearchItem() {
        self.itemSearchView = ItemSearchView().initWith(delegate: self)
        itemSearchView?.viewModel = viewModel
        itemSearchView?.addToSuperView(view: self)
    }
    
    func actionOnLocationSearch() {
        moveToSearchLoc()
    }
    
    func actionOnRestaurantSearch() {
        AnalyticsHelper.shared.triggerEvent(type: .D03)
        let searchVC = UIUtils.viewController(storyboardId: "DeliveryHome", vcId: "SearchViewController") as! SearchViewController
        searchVC.data = viewModel.getAllBranches()
        searchVC.brand_logo_string = viewModel.selectedBrand?.logo ?? ""
        searchVC.delegate = self
        self.present(searchVC, animated: true, completion: nil)
    }
    
    func actionOnSafetyCheckView() {
        self.navigationController?.pushViewController(UIStoryboard.loadSafetyFeaturesViewController(), animated: true)
    }
    
    func vegNonVegSwitchChanged(_ uiSwitch: UISwitch) {
        isNonVegSelected = !uiSwitch.isOn
        switchForVeg.isOn = uiSwitch.isOn
        enableSwitch(uiSwitch)
        enableSwitch(switchForVeg)
        viewModel.isNonVegSelected = isNonVegSelected
        viewModel.filterMenuItems(isShowVeg: uiSwitch.isOn)
    }
    
    func valueChangedOnDeliveryAndTakeawaySegmentControl(_ uiSegmentControl: UISegmentedControl, isChanged: Bool){
        deliveryOrTakeAwayOptionSelected = TakeAwayDelivery(rawValue: uiSegmentControl.selectedSegmentIndex) ?? TakeAwayDelivery.Delivery
        if !isChanged{
            return
        }
        if uiSegmentControl.selectedSegmentIndex == 0{
            AnalyticsHelper.shared.triggerEvent(type: .D09)
        }else{
            AnalyticsHelper.shared.triggerEvent(type: .D10)
        }
        //AnalyticsHelper.shared.triggerEvent(type: .Home_Delivery_Takeaway)
    }

    
    func actionOnAddItemButton(item: Item, indexPath: IndexPath?) {
      
        guard !(BBQUserDefaults.sharedInstance.accessToken.isEmpty) else {
//            let controller = UIStoryboard.loadLoginViewController()
//            controller.delegate = self
//            controller.shouldCloseTouchingOutside = true
//            controller.currentTheme = .dark
//            controller.navigationFromScreen = .DeliveryHome
//            controller.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//            self.present(controller, animated: true, completion: nil)
            let viewController = UIStoryboard.loadLoginThroughPhoneController()
            viewController.delegate = self
            viewController.navigationFromScreen = .DeliveryHome
           self.navigationController?.pushViewController(viewController, animated: false)
            
            return
        }
        if (item.isCustomizable && item.quantity < 1) {
            moveToMenu(item, indexPath: indexPath)
        } else if item.isCustomizable && item.quantity >= 1 {
            let alertVc = UIUtils.viewController(storyboardId: "DeliveryHome", vcId: "AlertViewController") as! AlertViewController
            alertVc.item = item
            alertVc.delegate = self
            alertVc.indexPath = indexPath
            alertVc.modalPresentationStyle = .overCurrentContext
            self.present(alertVc, animated: true, completion: nil)
        } else {
            item.quantity += 1
            viewModel.addItemToCart(item: item, isRepeatedItem: false, indexPath: indexPath)
            itemDetailsView?.updateQuantity(count: item.quantity)
            itemSearchView?.updateQuantity(item: item, indexPath: indexPath)
            if isCameFromLimitedOffer{
                isCameFromLimitedOffer = false
                viewModel.updateQtyFromHeader(selectedItem: item)
            }
            colletionViewReloadData(indexPath: indexPath)
        }
            

    }
    func actionOnImageTapped(item: Item, indexPath: IndexPath?, isCameFromSearch: Bool) {
        
        let itemDetailsView = ItemDetailView().initWith(item: item, indexPath: indexPath, isServicable: viewModel.isServicable, delegate: self)
        itemDetailsView.addToSuperView(view: self, isCameFromSearch: isCameFromSearch)
        self.itemDetailsView = itemDetailsView
        AnalyticsHelper.shared.triggerEvent(type: .Delivery_Takeaway_Product_Item_info, properties: [.Catalog_Product_Item : item.name])
    }
    
    func clearSelectedItemAndMoveMenu(_ item: Item, indexPath: IndexPath?) {
        viewModel.removeAllSelectedItems(item)
        moveToMenu(item, indexPath: indexPath)
    }
    func actionOnRemoveItemButton(item: Item, indexPath: IndexPath?) {
        if item.quantity == 0 && item.isCustomizable {
            viewModel.removeAllSelectedItems(item)
        }
        if isCameFromLimitedOffer{
            isCameFromLimitedOffer = false
            viewModel.updateQtyFromHeader(selectedItem: item)
        }
        viewModel.removeItemFromCart(item: item, indexPath: indexPath)
        colletionViewReloadData(indexPath: indexPath)
    }
}

extension DeliveryHomeViewController: SearchViewControllerDelegate {
    func dismissLocationViewController() {
        _ = checkForBrnachDelivery()
    }
    
    func didSelectRestaurant(_ branch: Branch) {
        AnalyticsHelper.shared.triggerEvent(type: .D03A)
        BBQUserDefaults.sharedInstance.DeliveryBranchIdValue = branch.branchId
        BBQUserDefaults.sharedInstance.DeliveryCurrentSelectedLocation = branch.branchName
        only_delivery = branch.only_delivery
        only_dining = branch.only_dining
        viewModel.selectedBranch = branch
        showShimmering()
        viewModel.getRestaurantMenus(isLoadingFirsttime: true)
        _ = checkForBrnachDelivery()
    }
}

extension DeliveryHomeViewController: PopupMenuDelegate {
    func dismissPopupMenu() {
        hidePopupMenu()
    }
}

extension DeliveryHomeViewController: AlertViewControllerDelegate {
    func actionOnNoBtn(_ item: Item, indexPath: IndexPath?) {
        item.quantity += 1
        viewModel.addItemToCart(item: item, isRepeatedItem: true, indexPath: indexPath)
        itemDetailsView?.updateQuantity(count: item.quantity)
        itemSearchView?.updateQuantity(item: item, indexPath: indexPath)
        if isCameFromLimitedOffer{
            isCameFromLimitedOffer = false
            viewModel.updateQtyFromHeader(selectedItem: item)
        }
        colletionViewReloadData(indexPath: indexPath)
    }
    
    func actionOnYesBtn(_ item: Item, indexPath: IndexPath?) {
        clearSelectedItemAndMoveMenu(item, indexPath: indexPath)
    }
}

extension DeliveryHomeViewController: LocationManagerDelegate {
    func tracingLocation(location: Location) {
        //MARK: - Fix for DEL-1778
        checkForBrnachDelivery()
        //END
        refreshHomeBy(location)
    }
    
    func tracingFailureInLocation(error: NSError) {
        self.alert(message: error.localizedDescription, title: "Location update failed")
    }
    func locationDisabled() {
        refreshHomeBy(Location())
    }
}

extension DeliveryHomeViewController: SearchLocationViewControllerDelegate {
    func getSelectedAddress(_ location: Location) {
        AnalyticsHelper.shared.triggerEvent(type: .D04A)
        isLoadingFirstTime = true
        refreshHomeBy(location)
    }
}

//MARK:- UITableViewDelegates
extension DeliveryHomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.categoryList.count > 0 ? viewModel.categoryList.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SectionTableViewCell.getCellIdentifier()) as! SectionTableViewCell
        var count = 0
        if viewModel.categoryList.count > indexPath.row{
            count = (viewModel.menuDict[viewModel.categoryList[indexPath.row]]?.count ?? 0)
        }
        cell.updateCell(section: viewModel.categoryList[indexPath.row], count: count)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedIndexPath = IndexPath(row: 0, section: indexPath.row)
        setSelectedCategory(sectionIndex: selectedIndexPath.section)
        actionOnMenuView()
        AnalyticsHelper.shared.triggerEvent(type: .Delivery_Takeaway_Catalog, properties: [.Location: BBQUserDefaults.sharedInstance.DeliveryCurrentSelectedLocation, .Catalog_Items: viewModel.categoryList[indexPath.section].name, .Delivery_Takeaway: headerCoverPageView.deliveryTakeAwaySegment.selectedSegmentIndex == 0 ? AnalyticsHelper.MoEngageAttrValue.User_Selected_Delivery.rawValue : AnalyticsHelper.MoEngageAttrValue.User_Selected_Takeaway.rawValue])
        if let attributes = itemsCollectionView.layoutAttributesForSupplementaryElement(ofKind: UICollectionView.elementKindSectionHeader, at: selectedIndexPath) {
            let topOfHeader = CGPoint(x: 0, y: attributes.frame.origin.y - itemsCollectionView.contentInset.top + headerViewSize + 30.0)
            itemsCollectionView.setContentOffset(topOfHeader, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let rowHeight: CGFloat = 50
        if viewModel.categoryList.last == viewModel.categoryList[indexPath.row] {
            return rowHeight + 15
        }
        return rowHeight
    }
}

extension DeliveryHomeViewController: MenuViewControllerDelegate {
    
    func colletionViewReloadData(indexPath: IndexPath?) {
        if let branchID = viewModel.branchID, branchID != BBQUserDefaults.sharedInstance.DeliveryBranchIdValue{
            itemsCollectionView.setContentOffset(CGPoint(x: 0, y: -headerViewSize), animated: true)
            itemsCollectionView.reloadData()
        }else if let indexPath = indexPath, viewModel.categoryList.count > indexPath.section, (viewModel.menuDict[viewModel.categoryList[indexPath.section]]?.count ?? 0) > indexPath.row, itemsCollectionView.cellForItem(at: indexPath) != nil{
            itemsCollectionView.reloadItems(at: [indexPath])
        }else if itemsCollectionView.indexPathsForVisibleItems.count > 0{
            var isVisible = true
            for indexPath in itemsCollectionView.indexPathsForVisibleItems{
                if viewModel.categoryList.count < indexPath.section || (viewModel.menuDict[viewModel.categoryList[indexPath.section]]?.count ?? 0) < indexPath.row{
                    isVisible = false
                    break
                }
                
                if itemsCollectionView.cellForItem(at: indexPath) == nil{
                   isVisible = false
                   break
               }
            }
            if isVisible{
                self.itemsCollectionView.reloadItems(at: self.itemsCollectionView.indexPathsForVisibleItems)
            }else{
                itemsCollectionView.setContentOffset(CGPoint(x: 0, y: -headerViewSize), animated: true)
                itemsCollectionView.reloadData()
            }
        }else{
            itemsCollectionView.reloadData()
        }
        viewModel.branchID = BBQUserDefaults.sharedInstance.DeliveryBranchIdValue
    }
    
    func actionOnDone(_ item: Item, indexPath: IndexPath?) {
    
        viewModel.addItemToCart(item: item, isRepeatedItem: false, indexPath: indexPath)
        itemDetailsView?.updateQuantity(count: item.quantity)
        itemSearchView?.updateQuantity(item: item, indexPath: indexPath)
        if isCameFromLimitedOffer{
            isCameFromLimitedOffer = false
            viewModel.updateQtyFromHeader(selectedItem: item)
        }
        colletionViewReloadData(indexPath: indexPath)
        if item.foodCategory == .comboItem {
            hidePopupMenu()
        }
    }
    
}


extension DeliveryHomeViewController: BBQMenuViewControllerProtocol {
    func postUserAccountProcessing() {
        viewModel.getRestaurantMenus(isLoadingFirsttime: true)
    }
    
    func showPromotionsAndOffers() {
        BBQActivityIndicator.showIndicator(view: self.view)
        let homeModel = HomeViewModel()
        homeModel.getPromosVouchers(latitide: 0.0, longitude: 0.0, brach_ID: BBQUserDefaults.sharedInstance.DeliveryBranchIdValue) { (isSuccess) in
            
            BBQActivityIndicator.hideIndicator(from: self.view)
            
            let promosArray = homeModel.getPromotionsDataData()
            let promoViewModel = PromosViewModel(promosDataArray: promosArray)
            
            let viewController = PromotionOfffersVC()
            viewController.promoDelegate = self as? BottomSheetImagesViewControllerProtocol
            viewController.promoScreenType = .Hamburger
            viewController.promosData = promoViewModel.promosArray
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func showBookingScreen() {
        if let tabBarController = navigationController?.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
            tabBarController.selectedIndex = 0
        }
        NotificationCenter.default.post(name: Notification.Name(Constants.NotificationName.showBookingScreen),
                                        object: nil)
    }
    
    
}

extension DeliveryHomeViewController: LoginControllerDelegate {
    
    func gotoHomeScreen(isExistingUser: Bool, navigationFrom: LoginNavigationScreen) {
       
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
extension DeliveryHomeViewController: ItemDetailViewDelegate{
    func actionOnMultipleSelect(_ item: Item, indexPath: IndexPath?) {
        actionOnAddItemButton(item: item, indexPath: indexPath)
    }
    func didHideItemDetails(isCameFromSearch: Bool) {
        if !isCameFromSearch{
            if let tabBarController = navigationController?.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
                tabBarController.tabBar.isHidden = false
            }
        }
        itemDetailsView = nil
    }
    func actionOnReduceItem(item: Item, indexPath: IndexPath?) {
        itemSearchView?.updateQuantity(item: item, indexPath: indexPath)
    }
}
extension DeliveryHomeViewController: DeliveryHomeViewModelDelegate{
    func showIntroMultiBrand() {
        openBrandIntroduction()
    }
    
    func setCollectionHeaderSize(isLimitedOfferAvailable: Bool) {
        self.isLimitedOfferAvailable = isLimitedOfferAvailable
        smallMenuView.applyViewMultiBrandTheme()
        smallMenuView.setShadow(color: .deliveryThemeColor, opacity: 0.5, offSet: .zero, radius: 30, scale: true)
        setCollectionHeader()
    }
    
    func startLoading(model: DeliveryHomeViewModel, isRequired: Bool) {
        showShimmering()
    }
    func stopLoading(model: DeliveryHomeViewModel) {
        stopShimmering()
    }
    func noNearbyBranchFound(model: DeliveryHomeViewModel){
        brandNotAvaiableView = BrandNotAvaiableView().initWith(data: viewModel.selectedBrand, delegate: self)
        brandNotAvaiableView?.addToSuperView(view: self)
    }
    func updateUIOnOpenOrders(isAvailable: Bool){
        
        
        //bring the menu button and collection view up respective to the height of collectionview
        if isAvailable{
            menubootomContraint.isActive = false
            menubootomContraint.constant =  64
            menubootomContraint.isActive = true
            menuViewBottomViewHeight.isActive = false
            menuViewBottomViewHeight.constant = 64
            menuViewBottomViewHeight.isActive = true
            self.view.layoutIfNeeded()
        }else{
            menubootomContraint.isActive = false
            menubootomContraint.constant = 24
            menubootomContraint.isActive = true
            menuViewBottomViewHeight.isActive = true
            menuViewBottomViewHeight.constant = 0
            menuViewBottomViewHeight.isActive = true
            self.view.layoutIfNeeded()
        }
    }
    func scrollToCollectionView(isForceFully: Bool = false) {
        if isClickedOnUbq{
            if itemsCollectionView.numberOfSections > 1, itemsCollectionView.numberOfItems(inSection: 1) > 0{
                itemsCollectionView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
                isClickedOnUbq = false
            }
        }
        if isForceFully{
            isClickedOnUbq = false
        }
    }
    
    func showOfferView(textMessageToDisplay : String?) {
        
        if textMessageToDisplay != nil &&  textMessageToDisplay != "" {
            
            let offer =  textMessageToDisplay
            
            let tempView = ItemOfferAppliedView().initWith(message: offer ?? "" , delegate: self)
            tempView.addToSuperView(view: nil)
        }
    }
}

extension DeliveryHomeViewController: BrandNotAvaiableViewDelegate{
    func clickOnSelectBranch(view: BrandNotAvaiableView) {
        actionOnRestaurantSearch()
    }
    
    func clickOnSelectBrand(view: BrandNotAvaiableView) {
        onClickBtnBrand(self)
    }
}

extension DeliveryHomeViewController: UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if itemsCollectionView == scrollView{
            
            guard viewModel.categoryList.count > 1 else { return }
            if itemsCollectionView.indexPathsForVisibleItems.count > 0 {
                let indexPaths = itemsCollectionView.indexPathsForVisibleItems.sorted()
                setSelectedCategory(sectionIndex: indexPaths[0].section)
            }

            let offset = scrollView.contentOffset.y + headerViewSize
            self.scrollView.setContentOffset(CGPoint(x: 0, y: offset), animated: false)
            if scrollView.contentOffset.y > 20{
                UIView.animate(withDuration: 0.2) {
                    self.stackViewForHeader.isHidden = false
                } completion: { (isCompleted) in
                    self.stackViewForHeader.isHidden = false
//                    if scrollView.contentOffset.y > scrollView.contentSize.height/1.8{
//
//                        if DeliveryTabBarController.cartItemCount > 0 {
//                            //The below code will bring collectionview bottom above view cart screen
//                            self.bottomConstraintWhenNoItemCartView.isActive = false
//                            self.bottomConstraintWhenNoItemCartView.priority = .defaultLow
//                            self.bottomConstraintWhenItemCartViewVisible.isActive = true
//                            self.bottomConstraintWhenItemCartViewVisible.priority = .defaultHigh
//                        }
//                    }
                }
            }else{
                UIView.animate(withDuration: 0.2) {
                    self.stackViewForHeader.isHidden = true
                } completion: { (isCompleted) in
                    self.stackViewForHeader.isHidden = true
                    if DeliveryTabBarController.cartItemCount > 0 {
                        
                        //Initially The below code will keep collectionview bottom below view cart screen till its bottom
//                        self.bottomConstraintWhenItemCartViewVisible.isActive = false
//                        self.bottomConstraintWhenItemCartViewVisible.priority = .defaultLow
//                        self.bottomConstraintWhenNoItemCartView.isActive = true
//                        self.bottomConstraintWhenNoItemCartView.priority = .defaultHigh
                        //show cart

                        
                    }
                }
            }
        }
    }
}
extension DeliveryHomeViewController: ItemSearchViewDelegate{
    func actionOnDone(view: ItemSearchView, item: Item, indexPath: IndexPath?) {
        self.actionOnAddItemButton(item: item, indexPath: nil)
    }
    
    func actionOnAddItemButton(view: ItemSearchView, item: Item, indexPath: IndexPath?) {
        self.actionOnAddItemButton(item: item, indexPath: nil)
        
        if item.isOfferAvailable != nil &&  item.isOfferAvailable  != "" {
            
            let tempView = ItemOfferAppliedView().initWith(message: item.isOfferAvailable ?? "" , delegate: self)
            tempView.addToSuperView(view: nil)
        }
    }
    
    
    func actionOnRemoveItemButton(view: ItemSearchView, item: Item, indexPath: IndexPath?) {
        self.actionOnRemoveItemButton(item: item, indexPath: nil)
    }
    
    func didHideItemSearchView() {
        self.colletionViewReloadData(indexPath: nil)
        if let tabBarController = navigationController?.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
            tabBarController.tabBar.isHidden = false
        }
        itemSearchView = nil
    }
    
    
}
extension DeliveryHomeViewController : OpenOrderCellDelegate {
    
    func clickedOnRateOrder( selectedIndex: Int ) {
        
        AnalyticsHelper.shared.triggerEvent(type: .DH05)
        let  rateViewController = UIStoryboard.loadFeedBackViewController()
        if viewModel.openOrders.count > selectedIndex{
            let order = viewModel.openOrders[selectedIndex]
            rateViewController.orderID = order.orderId
            rateViewController.transactionType = order.transaction_type ?? 1
            rateViewController.branchName = order.branchName
            rateViewController.brandlogo = order.brand_logo
            rateViewController.orderDate = order.createdOn
            rateViewController.deliveryThemeColor = order.backgroudColor
            rateViewController.deliveryThemeTextColor = order.textColor
            
            bottomSheetController?.present(rateViewController, on: self, viewHeight: 480)
        }
    }
}
extension DeliveryHomeViewController: DeliveryTabBarControllerDelegate{
    func nearbyOutletsFrom(userLatitude: Double, userLongitude: Double) {}
    
    func promotionsAndVouchersFrom(branchLatitude: Double, branchLongitude: Double, branchID: String) {}
}


extension DeliveryHomeViewController: BrandPickerViewDelegate{
    func confirm(view: BrandPickerView, brandInfo: BrandInfo){
        viewModel.selectedBrand = brandInfo
        var location = viewModel.userLocation
        if location == nil{
            location?.latitude = "\(BBQUserDefaults.sharedInstance.currentLatitudeValue)"
            location?.longitude = "\(BBQUserDefaults.sharedInstance.currentLongitudeValue)"
        }
        refreshHomeBy(location ?? Location())
    }
}
//MARK: On click of ratings dismiss the rate main view and go to rating detail page
extension DeliveryHomeViewController :FeedbackDetailedDelegate {
    
    func actionOnDone(viewController : FeedbackDetailedViewController){
    
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}


extension DeliveryHomeViewController: ItemOfferAppliedViewDelegate{
    
    func actionOnDone(view: ItemOfferAppliedView){
        
        print("actionOnDone(view: ItemOfferAppliedView)")
    }
}

