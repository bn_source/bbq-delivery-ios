//
//  SearchLocationViewController.swift
//  BBQ
//
//  Created by Dharani Sadasivam on 16/08/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import UIKit
import GooglePlaces

protocol SearchLocationViewControllerDelegate: AnyObject {
    func getSelectedAddress(_ location: Location)
}

class SearchLocationViewController: BaseViewController {
    
    @IBOutlet weak var savedAddressLblHCons: NSLayoutConstraint!
    @IBOutlet weak var savedAddressLbl: UILabel!
    @IBOutlet weak var addressTableView: UITableView!
    @IBOutlet weak var searchLocShadowView: UIView!
    @IBOutlet weak var searchLocTextField: UITextField!
    private var placesClient: GMSPlacesClient!
    var googlePlaces: [GooglePlaceRes] = []
    var addressViewModel = AddressViewModel()
    var selectedBranchId: String?
    weak var delegate: SearchLocationViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        styleUI()
        

        addressViewModel.getSavedAddress(customerId: BBQUserDefaults.sharedInstance.customerId,
                                         branchId: BBQUserDefaults.sharedInstance.DeliveryBranchIdValue, filterWithID: nil) {
            self.savedAddressLbl.isHidden = (self.addressViewModel.addresses?.count ?? 0) == 0
            self.addressTableView.reloadData()
        }
    }
    
    private func styleUI() {
        searchLocShadowView.setShadow()
        placesClient = GMSPlacesClient.shared()
        self.navigationController?.view.backgroundColor = .white
        
        self.navigationController?.navigationBar.tintColor = UIColor(named: "TextColor")
        self.navigationController?.navigationBar.topItem?.title = ""
        
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        button.addTarget(self, action: #selector(self.actionOnCurrentLocation), for: .touchUpInside)
        button.setImage(#imageLiteral(resourceName: "Icon material-my-location"), for: .normal)
        searchLocTextField.addRightView(button)
        searchLocTextField.delegate = self
        addressTableView.tableFooterView = UIView()
        addressTableView.alwaysBounceVertical = false
        addressTableView.estimatedRowHeight = UITableView.automaticDimension
        savedAddressLblHCons.constant = googlePlaces.count == 0 ? 20 : 0
    }
    
    @objc func actionOnCurrentLocation() {
        LocationManager.sharedInstance.delegate = self
        LocationManager.sharedInstance.initLocationManager()
    }
    @IBAction func onClickBtnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}


extension SearchLocationViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        guard !text.isEmpty else {
            self.googlePlaces.removeAll()
            self.addressTableView.reloadData()
            savedAddressLblHCons.constant =  googlePlaces.count == 0 ? 20 : 0
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            return true
        }
        searchPlaces(text)
        
        return true
    }
    
    func searchPlaces(_ searchStr: String) {
        guard searchStr.count >= 5 else {
            return
        }
        let filter = GMSAutocompleteFilter()
        filter.type = .address
        filter.country = "IN"
        //AppDelegate.shared().gmsSessionToken
        placesClient.findAutocompletePredictions(fromQuery: searchStr, filter: filter, sessionToken: nil ) { (results, error) in
            if let error = error {
                print("Autocomplete error: \(error)")
                return
            }
            if let results = results {
                self.googlePlaces.removeAll()
                for result in results {
                    self.googlePlaces.append(GooglePlaceRes(placeName: result.attributedFullText, placeId: result.placeID))
                }
            }
            self.addressTableView.reloadData()
            self.savedAddressLblHCons.constant =  self.googlePlaces.count == 0 ? 20 : 0
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }
    }
}

extension SearchLocationViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.googlePlaces.count == 0 ? (self.addressViewModel.addresses?.count ?? 0) : self.googlePlaces.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.googlePlaces.count == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddressesTableViewCell", for: indexPath) as! AddressesTableViewCell
            if let address = self.addressViewModel.addresses?[indexPath.row] {
                cell.setCellData(address: address)
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "GoogleAddressTableViewCell", for: indexPath) as! GoogleAddressTableViewCell
            cell.addressLbl.attributedText = self.googlePlaces[indexPath.row].placeName
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.googlePlaces.count == 0 {
                self.navigationController?.popViewController(animated: true)
            guard let address  = self.addressViewModel.addresses?[indexPath.row] else {
                return
            }
            var location = Location()
            location.longitude = address.longitude
            location.latitude = address.latitude
            location.city = String(address.address.split(separator: ",").first ?? "")
            if address.tagType == .home{
                location.tag = "Home"
            }else if address.tagType == .work{
                location.tag = "Work"
            }else if address.tagName != ""{
                location.tag = address.tagName
            }
            self.delegate?.getSelectedAddress(location)
        } else {
            self.addressViewModel.getPlaceLatLong(self.googlePlaces[indexPath.row].placeId, completionHandler: { (place, error) in
                self.navigationController?.popViewController(animated: true)
                guard let location = place else { return }
                self.delegate?.getSelectedAddress(location)
            })
        }
    }
}


extension SearchLocationViewController: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
}

enum AddressType {
    case savedAddress
    case googleAddress
}

struct GooglePlaceRes {
    var placeName: NSAttributedString
    var placeId: String
}


extension NSAttributedString {
    
    func height(containerWidth: CGFloat) -> CGFloat {
        
        let rect = self.boundingRect(with: CGSize.init(width: containerWidth, height: CGFloat.greatestFiniteMagnitude),
                                     options: [.usesLineFragmentOrigin, .usesFontLeading],
                                     context: nil)
        return ceil(rect.size.height)
    }
    
    func width(containerHeight: CGFloat) -> CGFloat {
        
        let rect = self.boundingRect(with: CGSize.init(width: CGFloat.greatestFiniteMagnitude, height: containerHeight),
                                     options: [.usesLineFragmentOrigin, .usesFontLeading],
                                     context: nil)
        return ceil(rect.size.width)
    }
}

extension SearchLocationViewController: LocationManagerDelegate {
    func tracingLocation(location: Location) {
        self.navigationController?.popViewController(animated: true)
        self.delegate?.getSelectedAddress(location)
    }
    
    func tracingFailureInLocation(error: NSError) {
        self.alert(message: error.localizedDescription, title: "Location update failed")
    }
    func locationDisabled() {
        
    }
    
}
