//
 //  Created by Dharani Sadasivam on 09/11/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified DeliveryOrderDetailsViewController.swift
 //

import UIKit

class DeliveryOrderDetailsViewController: BaseViewController {

    @IBOutlet weak var orderDetailsTableView: UITableView!
    var viewModel: DeliveryStatusViewModel?
    var orderId: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let orderId = orderId else { return }
        viewModel?.getDeliveryStatus(orderId: orderId,isRequiredToShowLoader: true, completionHandler: {
            self.orderDetailsTableView.reloadData()
        })
        
        hideNavigationBar(false, animated: true)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        hideNavigationBar(false, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        hideNavigationBar(false, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        hideNavigationBar(true, animated: true)
    }

}

extension DeliveryOrderDetailsViewController: UITableViewDelegate, UITableViewDataSource  {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DeliveryOrderTableViewCell", for: indexPath) as! DeliveryOrderTableViewCell
        cell.deliveryDelegate = self
        guard let deliveryStatus = viewModel?.orderDeliveryStatus else {
            return cell
        }
        cell.fillUI(deliveryStatus)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 420
    }
    
}

extension DeliveryOrderDetailsViewController: DeliveryOrderTableViewCellDelegate {
    func actionOnHome() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func actionOnCall() {
     //   UIUtils.callNumber(phoneNumber: viewModel?.orderDeliveryStatus?.data?.branchContactNo ?? "")
    }
}
