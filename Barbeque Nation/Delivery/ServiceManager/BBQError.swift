//
//  BBQError.swift
//  BBQ
//
//  Created by Shareef on 20/09/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import UIKit

class BBQError: NSObject {
    
    var errorCode = 0
    var message = ""
    
    init(errorCode: Int = 0, message: String = UNEXPECTED_ERROR) {
        self.errorCode = errorCode
        self.message = message
    }
}
