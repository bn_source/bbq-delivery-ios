//
//  MatchFindAPI.swift
//  MatchFind
//
//  Created by Rahul S on 22/09/19.
//  Copyright © 2019 Rahul S. All rights reserved.
//

import ObjectMapper
import Alamofire

class BBQServiceManager {
    
    //MARK:- MatchFindAPI variable Initialization
    private static let instance = BBQServiceManager()
    var alamofireManager: SessionManager
    
    //MARK:- Instance and Initialization Methods
    static func getInstance() -> BBQServiceManager {
        return instance
    }
    
    init() {
        let sessionConfig = URLSessionConfiguration.ephemeral
        let manager = ServerTrustPolicyManager(policies: [DOMAIN: ServerTrustPolicy.disableEvaluation])
        alamofireManager = SessionManager(configuration: sessionConfig, serverTrustPolicyManager: manager)
    }
    
    private static var baseURL: String {
        return EnviornmentConfiguration().environment.baseURL
    }
    
    private static var baseURLForNewServer: String {
        return EnviornmentConfiguration().environment.baseURLForNewServer
    }
    
    private var headers: HTTPHeaders {
        var commonHeaders =  HTTPHeaders()
        //commonHeaders["Content-Type"] = "application/json"
        //commonHeaders["Accept"] = "application/json"
        let config = EnviornmentConfiguration().environment
        let accessToken = BBQUserDefaults.sharedInstance.accessToken
        if !accessToken.isEmpty {
            commonHeaders["Authorization"] =  config.authorization
        } else {
            commonHeaders["BBQ-Client-Id"] = config.clientID
            commonHeaders["BBQ-Client-Secret"] = config.clientSecret
        }
        
        if !(BBQUserDefaults.sharedInstance.isGuestUser) {
                   
                   if SharedProfileInfo.shared.profileData?.mobileNumber == "" ||  SharedProfileInfo.shared.profileData?.mobileNumber == nil {
                       commonHeaders["User-Agent"] = String(format: "Ios-%@",  UIDevice.current.identifierForVendor?.uuidString ?? "")
                   }else{
                       
                       commonHeaders["User-Agent"] = String(format: "User-%@", SharedProfileInfo.shared.profileData?.mobileNumber ?? ( UIDevice.current.identifierForVendor?.uuidString ?? ""))
                   }
               }else{
                 commonHeaders["User-Agent"] =  String(format: "Ios-%@", UIDevice.current.identifierForVendor?.uuidString ?? "")
             }
        return commonHeaders
    }
    
    //MARK:- Alamofire Request method for json response
    static func requestJson(_ url: String,_ method: Alamofire.HTTPMethod, parameters: [String: Any]? = nil, encoding: ParameterEncoding = JSONEncoding.default, headers: HTTPHeaders? = nil, useBaseUrl: Bool = true, completionHandler:@escaping (_ error: BBQError?, _ result: [String:Any]?)-> Void) {
        let urlBase: URLConvertible = useBaseUrl ? (baseURL + url) : url
        print("Request Url: \(urlBase)")
        print("Request Method Type: \(method.rawValue)")
        if let parameters = parameters {
            print("Request Parameters: \(String(describing: parameters))")
        }
        BBQServiceManager.getInstance().alamofireManager.request(urlBase, method: method, parameters: parameters, encoding: encoding, headers: headers).responseJSON { (response) in
            DispatchQueue.main.async {
                let statusCode = response.response?.statusCode ?? 000
                print("Response StatusCode: \(statusCode)")
                print("Response: \(String(describing: response))")
                
                switch response.result {
                case .success(let value):
                    switch statusCode {
                    case 200, 201:
                        completionHandler(nil, value as? [String: Any])
                    case 419:
                        print("Token Expired")
                        completionHandler(nil, nil)
                    default:
                        print("SOMETHING WENT WRONG IN FETCHING THE RESPONSE")
                        completionHandler(nil, nil)
                    }
                case . failure(let error):
                    completionHandler(BBQError(errorCode: statusCode, message: error.localizedDescription), nil)
                }
            }
        }
    }
    
    
    
    //MARK:- Alamofire Request method for json response
    static func requestJsonWithNewServer(_ url: String,_ method: Alamofire.HTTPMethod, parameters: [String: Any]? = nil, encoding: ParameterEncoding = JSONEncoding.default, headers: HTTPHeaders? = nil, useBaseUrl: Bool = true, completionHandler:@escaping (_ error: BBQError?, _ result: [String:Any]?)-> Void) {
        let urlBase: URLConvertible = useBaseUrl ? (baseURLForNewServer + url) : url
        print("Request Url: \(urlBase)")
        print("Request Method Type: \(method.rawValue)")
        if let parameters = parameters {
            print("Request Parameters: \(String(describing: parameters))")
        }
        BBQServiceManager.getInstance().alamofireManager.request(urlBase, method: method, parameters: parameters, encoding: encoding, headers: headers).responseJSON { (response) in
            DispatchQueue.main.async {
                let statusCode = response.response?.statusCode ?? 000
                print("Response StatusCode: \(statusCode)")
                print("Response: \(String(describing: response))")
                
                switch response.result {
                case .success(let value):
                    switch statusCode {
                    case 200, 201:
                        completionHandler(nil, value as? [String: Any])
                    case 419:
                        print("Token Expired")
                        completionHandler(nil, nil)
                    default:
                        print("SOMETHING WENT WRONG IN FETCHING THE RESPONSE")
                        completionHandler(nil, nil)
                    }
                case . failure(let error):
                    completionHandler(BBQError(errorCode: statusCode, message: error.localizedDescription), nil)
                }
            }
        }
    }
    //MARK:- Alamofire request method for getting array response
    static func requestArray(_ url: String,_ method: Alamofire.HTTPMethod, parameters: [String: Any]? = nil, encoding: ParameterEncoding = JSONEncoding.default, headers: HTTPHeaders? = nil, useBaseUrl: Bool = true, completionHandler:@escaping (_ error: BBQError?, _ result: [[String:Any]]?)-> Void) {
        let urlBase: URLConvertible = useBaseUrl ? (baseURL + url) : url
        print("Request Url: \(urlBase)")
        print("Request Method Type: \(method.rawValue)")
        if let parameters = parameters {
            print("Request Parameters: \(String(describing: parameters))")
        }
        BBQServiceManager.getInstance().alamofireManager.request(urlBase, method: method, parameters: parameters, encoding: encoding, headers: headers).responseJSON { (response) in
            DispatchQueue.main.async {
                let statusCode = response.response?.statusCode ?? 000
                print("Response StatusCode: \(statusCode)")
                print("Response: \(String(describing: response))")
                
                switch response.result {
                case .success:
                    switch statusCode {
                    case 200:
                        print("response received")
                        
                    case 419:
                        print("Token Expired")
                        completionHandler(nil, nil)
                    default:
                        print("SOMETHING WENT WRONG IN FETCHING THE RESPONSE")
                    }
                case . failure(let error):
                    completionHandler(BBQError(errorCode: statusCode, message: error.localizedDescription), nil)
                }
            }
        }
    }
    
    //MARK:- Alamofire Request method for json response
    static func requestData(_ url: String,_ method: Alamofire.HTTPMethod, parameters: [String: Any]? = nil, encoding: ParameterEncoding = JSONEncoding.default, headers: HTTPHeaders? = nil, useBaseUrl: Bool = true, completionHandler:@escaping (_ error: BBQError?, _ result: Data?)-> Void) {
        let urlBase: URLConvertible = useBaseUrl ? (baseURL + url) : url
        print("Request Url: \(urlBase)")
        print("Request Method Type: \(method.rawValue)")
        if let parameters = parameters {
            print("Request Parameters: \(String(describing: parameters))")
        }
        BBQServiceManager.getInstance().alamofireManager.request(urlBase, method: method, parameters: parameters, encoding: encoding, headers: headers).responseJSON { (response) in
            DispatchQueue.main.async {
                let statusCode = response.response?.statusCode ?? 000
                print("Response StatusCode: \(statusCode)")
                print("Response: \(String(describing: response))")
                
                switch response.result {
                case .success(_):
                    switch statusCode {
                    case 200, 201:
                        completionHandler(nil, response.data)
                    case 419:
                        print("Token Expired")
                        completionHandler(nil, nil)
                    default:
                        print("SOMETHING WENT WRONG IN FETCHING THE RESPONSE")
                        completionHandler(nil, nil)
                    }
                case . failure(let error):
                    completionHandler(BBQError(errorCode: statusCode, message: error.localizedDescription), nil)
                }
            }
        }
    }
    
    func getDeliveryPromotions(completionHandler:@escaping (_ error: BBQError?, _ response: PromotionsResponse?) -> Void) {
        //BBQServiceManager.requestJson(deliveryPromotions, .get, parameters: ["branch_id": 15], headers: headers) { (error, response)  in
        //TODO: Add branch id to request
        let reqUrl = deliveryPromotions + String(format: "?branch_id=%@", BBQUserDefaults.sharedInstance.DeliveryBranchIdValue)
        BBQServiceManager.requestJson(reqUrl, .get, headers: headers) { (error, response)  in
            guard let response = response else {
                completionHandler(error, nil)
                return
            }
            completionHandler(error, PromotionsResponse(JSON: response))
        }
    }
    
    func getRestaurantMenus(params: [String: Any], completionHandler:@escaping (_ error: BBQError?, _ response: RestaurantMenuResponse?) -> Void) {
        BBQServiceManager.requestJson(deliveryCatalog, .post, parameters: params, headers: headers) { (error, response) in
            guard let menuDetails = response else {
                completionHandler(error, nil)
                return
            }
            completionHandler(error, RestaurantMenuResponse(JSON: menuDetails))
        }
    }
    
    func getPreviousSelectedItems(customerId: String, branchId: String, completionHandler:@escaping (_ error: BBQError?, _ response: CartItems?) -> Void) {
        BBQServiceManager.requestJson(getCartHomepage, .post, parameters: ["customer_id": customerId, "branch_id": branchId], headers: headers) { (error, response) in
            guard let cartItemsResponse = response else {
                completionHandler(error, nil)
                return
            }
            completionHandler(error, CartItemsResponse(JSON: cartItemsResponse)?.data)
        }
    }

    func addOrRemoveItemToCart(params: [String: Any], completionHandler:@escaping (_ error: BBQError?, _ response: GetCartResponseModel?) -> Void) {
        BBQServiceManager.requestJson(addOrRemoveItem, .post, parameters: params, headers: headers) { (error, response) in
            guard let response = response else {
                completionHandler(error, nil)
                return
            }
            completionHandler(error, GetCartResponseModel(JSON: response))
        }
    }
    
    func getAllNearByRestaurant(params: [String: Any], completionHandler:@escaping (_ error: BBQError?, _ response: BranchesData?) -> Void) {
        BBQServiceManager.requestJson(getAllNearByBranches, .post, parameters: params, headers: headers) { (error, response) in
            guard let result = response else {
                completionHandler(error, nil)
                return
            }
            let data = BranchesData(JSON: result)
            completionHandler(error, data)
        }
    }
    
    func getAllNearByRestaurantV1(params: [String: Any], isClickedFromHome: Bool = false, completionHandler:@escaping (_ error: BBQError?, _ response: BrandsData?) -> Void) {
        if let tempData = savedResponseOfBranches, tempData.brands.count > 0, !isClickedFromHome{
            completionHandler(nil, tempData)
            savedResponseOfBranches = nil
            return
        }
        savedResponseOfBranches = nil
        BBQServiceManager.requestJson(getAllNearByBranchesV1, .post, parameters: params, headers: headers) { (error, response) in
            guard let result = response else {
                completionHandler(error, nil)
                return
            }
            let data = BrandsData(JSON: result)
//            brandlistForHomePage.removeAll()
//            brandlistForHomePage.append(BrandInfo(id: "", name: "Barbeque\nNation", logo: "https://gvgc.bnhl.in/delivery/bbqLogo.png", colorCode: "#FF6301|#FFFFFF"))
//            for brand in data?.brands ?? [BBQBrands](){
//                brandlistForHomePage.append(BrandInfo(brand: brand))
//            }
            completionHandler(error, data)
        }
    }
    
    func getLocalSavedBranches(completion: @escaping () -> ()) {
        BBQServiceManager.requestJson(branches, .get, parameters: nil, headers: headers) { (error, response) in
            guard let result = response else {
                return
            }
            let data = LocalBranchData(JSON: result)
            if let branch = data?.branches, branch.count > 0{
                localSavedBranches = branch
            }
            completion()
        }
    }
    
    func getSavedAddress(params: [String: Any], completionHandler:@escaping (_ error: BBQError?, _ response: AddressesData?) -> Void) {
//        #warning("Needs to change")
//        var commonHeaders =  HTTPHeaders()
//        commonHeaders["Authorization"] = BBQUserDefaults.sharedInstance.accessToken
        BBQServiceManager.requestJson(getCustomerAddress, .post, parameters: params, headers: headers) { (error, response) in
            guard let result = response else {
                completionHandler(error, nil)
                return
            }
            completionHandler(error, AddressesData(JSON: result))
        }
    }
    
    
    func getCartDetail(params: [String: Any], completionHandler:@escaping (_ error: BBQError?, _ response: GetCartResponseModel?) -> Void) {
        
        BBQServiceManager.requestJson(getCartDetails, .post, parameters: params, headers: headers) { (error, response) in
            guard let result = response else {
                completionHandler(error, nil)
                return
            }
            completionHandler(error, GetCartResponseModel(JSON: result))
        }
    }
    
    
    func clearCartItems(params: [String: Any], completionHandler:@escaping (_ error: BBQError?, _ response: ValidatePaymentResponseModel?) -> Void) {
        
        BBQServiceManager.requestJson(clearCart, .post, parameters: params, headers: headers) { (error, response) in
            guard let result = response else {
                completionHandler(error, nil)
                return
            }
            completionHandler(error, ValidatePaymentResponseModel(JSON: result))
        }
    }
    
    func useSmiles(params: [String: Any], completionHandler:@escaping (_ error: BBQError?, _ response: GetCartResponseModel?) -> Void) {
        
        BBQServiceManager.requestJson(useAllSmiles, .post, parameters: params, headers: headers) { (error, response) in
            guard let result = response else {
                completionHandler(error, nil)
                return
            }
            completionHandler(error, GetCartResponseModel(JSON: result))
        }
    }
    
    func removeVouchers(params: [String: Any], completionHandler:@escaping (_ error: BBQError?, _ response: GetCartResponseModel?) -> Void) {
        
        BBQServiceManager.requestJson(removeVoucherOrCoupons, .post, parameters: params, headers: headers) { (error, response) in
            guard let result = response else {
                completionHandler(error, nil)
                return
            }
            completionHandler(error, GetCartResponseModel(JSON: result))
        }
    }
    
    func addVouchersOrCoupons(params: [String: Any], completionHandler:@escaping (_ error: BBQError?, _ response: AddCouponResponseModel?) -> Void) {
        
        BBQServiceManager.requestJson(addVoucherOrCoupon, .post, parameters: params, headers: headers) { (error, response) in
            guard let result = response else {
                completionHandler(error, nil)
                return
            }
            completionHandler(error, AddCouponResponseModel(JSON: result))
        }
    }
    
    func validatteVouchersOrCoupons(params: [String: Any], completionHandler:@escaping (_ error: BBQError?, _ response: ValidateCouponResponseModel?) -> Void) {
        
        BBQServiceManager.requestJson(validateCoupon, .post, parameters: params, headers: headers) { (error, response) in
            guard let result = response else {
                completionHandler(error, nil)
                return
            }
            completionHandler(error, ValidateCouponResponseModel(JSON: result))
        }
    }
    
    
    func AddCustomerAddress(params: [String: Any], completionHandler:@escaping (_ error: BBQError?, _ response: AddAddressResponseModel?) -> Void) {
        
        BBQServiceManager.requestJson(addAddress, .post, parameters: params, headers: headers) { (error, response) in
            guard let result = response else {
                completionHandler(error, nil)
                return
            }
            completionHandler(error, AddAddressResponseModel(JSON: result))
        }
    }
    
    func DeleteCustomerAddress(params: [String: Any], completionHandler:@escaping (_ error: BBQError?, _ response: AddAddressResponseModel?) -> Void) {
        
        BBQServiceManager.requestJson(deletAddress, .post, parameters: params, headers: headers) { (error, response) in
            guard let result = response else {
                completionHandler(error, nil)
                return
            }
            completionHandler(error, AddAddressResponseModel(JSON: result))
        }
    }
    
    
    
    
    
    func generatePaymentId(params: [String: Any], completionHandler:@escaping (_ error: BBQError?, _ response: GeneratePaymentResponseModel?) -> Void) {
        
        BBQServiceManager.requestJson(generatePayment, .post, parameters: params, headers: headers) { (error, response) in
            guard let result = response else {
                completionHandler(error, nil)
                return
            }
            completionHandler(error, GeneratePaymentResponseModel(JSON: result))
        }
    }
    
    func validatePaymentId(params: [String: Any], completionHandler:@escaping (_ error: BBQError?, _ response: ValidatePaymentResponseModel?) -> Void) {
        
        BBQServiceManager.requestJson(validatePayment, .post, parameters: params, headers: headers) { (error, response) in
            guard let result = response else {
                completionHandler(error, nil)
                return
            }
            completionHandler(error, ValidatePaymentResponseModel(JSON: result))
        }
    }
    
    
    func getOrderHistory(params: [String: Any], completionHandler:@escaping (_ error: BBQError?, _ response: OrderHistoryResponse?) -> Void) {
        print(BBQUserDefaults.sharedInstance.accessToken)
        BBQServiceManager.requestJson(orderHistory, .post, parameters: params, headers: headers) { (error, response) in
            guard let result = response else {
                completionHandler(error, nil)
                return
            }
            completionHandler(error, OrderHistoryResponse(JSON: result))
        }
    }
    
   
    func getDeliveryStatusList(params: [String: Any], completionHandler:@escaping (_ error: BBQError?, _ response: DeliveryStatusResponse?) -> Void) {
       
        BBQServiceManager.requestJson(getDeliveryStatus, .post, parameters: params, headers: headers) { (error, response) in
            guard let result = response else {
                completionHandler(error, nil)
                return
            }
            completionHandler(error, DeliveryStatusResponse(JSON: result))
        }
    }
    
    func getTakeAwaySlots(params: [String: Any], completionHandler:@escaping (_ error: BBQError?, _ response: TakeAwaySlotsData?) -> Void) {
       
        BBQServiceManager.requestJson(getTakeawySlots, .post, parameters: params, headers: headers) { (error, response) in
            guard let result = response else {
                completionHandler(error, nil)
                return
            }
            completionHandler(error, TakeAwaySlotsData(JSON: result))
        }
    }
    
    func updateTakeAwaySlot(params: [String: Any], completionHandler:@escaping (_ error: BBQError?, _ response: TakeAwaySlotsData?) -> Void) {
       
        BBQServiceManager.requestJson(updateTakewaySlot, .post, parameters: params, headers: headers) { (error, response) in
            guard let result = response else {
                completionHandler(error, nil)
                return
            }
            completionHandler(error, TakeAwaySlotsData(JSON: result))
        }
    }
    
    func getSafetyHygieneDetails(completionHandler:@escaping (_ error: BBQError?, _ response: SafetyHygieneResponse?) -> Void) {
       
        BBQServiceManager.requestJson(safetyHygiene, .get, parameters: nil, headers: headers) { (error, response) in
            guard let result = response else {
                completionHandler(error, nil)
                return
            }
            completionHandler(error, SafetyHygieneResponse(JSON: result))
        }
    }
    
    func updateAddress(params: [String: Any], completionHandler:@escaping (_ error: BBQError?, _ response: OrderHistoryResponse?) -> Void) {
       
        BBQServiceManager.requestJson(updateOrderAddress, .post, parameters: params, headers: headers) { (error, response) in
            guard let result = response else {
                completionHandler(error, nil)
                return
            }
            completionHandler(error, OrderHistoryResponse(JSON: result))
        }
    }
    

    func checkDistance(params: [String: Any], completionHandler:@escaping (_ error: BBQError?, _ response: CheckDistanceResponseModel?) -> Void) {
        
        BBQServiceManager.requestJson(checkDistanceForDelivery, .post, parameters: params, headers: headers) { (error, response) in
            guard let result = response else {
                completionHandler(error, nil)
                return
            }
            completionHandler(error, CheckDistanceResponseModel(JSON: result))
        }
    }

    func getOrderDetails(params: [String: Any], completionHandler:@escaping (_ error: BBQError?, _ response: OrderDetailsResponse?) -> Void) {
       
        BBQServiceManager.requestJson(orderDetail, .post, parameters: params, headers: headers) { (error, response) in

            guard let result = response else {
                completionHandler(error, nil)
                return
            }

            completionHandler(error, OrderDetailsResponse(JSON: result))

        }
    
    }
    
    func getOrderFeedBackDetails(params: [String: Any], completionHandler:@escaping (_ error: BBQError?, _ response: FeedbackModel?) -> Void) {
       
        BBQServiceManager.requestJson(getOrderFeedback, .post, parameters: params, headers: headers) { (error, response) in

            guard let result = response else {
                completionHandler(error, nil)
                return
            }

            completionHandler(error, FeedbackModel(JSON: result))

        }
    
    }
    
    
    func getSubmitFeedBackResponse(params: [String: Any], completionHandler:@escaping (_ error: BBQError?, _ response: FeedbackModel?) -> Void) {
       
        BBQServiceManager.requestJson(submitOrderFeedback, .post, parameters: params, headers: headers) { (error, response) in

            guard let result = response else {
                completionHandler(error, nil)
                return
            }

            completionHandler(error, FeedbackModel(JSON: result))

        }
    
    }
    func getCouponAndVouchers(params: [String: Any], completionHandler:@escaping (_ error: BBQError?, _ response: CouponResponseModel?) -> Void) {
       
        BBQServiceManager.requestJson(getCoupons, .post, parameters: params, headers: headers) { (error, response) in

            guard let result = response else {
                completionHandler(error, nil)
                return
            }

            completionHandler(error, CouponResponseModel(JSON: result))

        }
    
    }
    
    
    func setOrderingForDetailsToCart(params: [String: Any], completionHandler:@escaping (_ error: BBQError?, _ result: Bool) -> Void) {
        BBQServiceManager.requestJson(orderingForDetail, .post, parameters: params, headers: headers) { (error, response) in
            guard response != nil else {
                completionHandler(error, false)
                return
            }
            completionHandler(error, true)
        }
    }
    
    
    func submitFeedbackResponseFromSideMenu(params: [String: Any], completionHandler:@escaping (_ error: BBQError?, _ response: SubmitFeedBackResponseModel?) -> Void) {
       
        BBQServiceManager.requestJson(submitFeedbackFromSideMenu, .post, parameters: params, headers: headers) { (error, response) in

            guard let result = response else {
                completionHandler(error, nil)
                return
            }
            completionHandler(error, SubmitFeedBackResponseModel(JSON: result))

        }
    
    }
    
    
    func feedbackReasonListFromSideMenu( completionHandler:@escaping (_ error: BBQError?, _ response: FeedbackReasonListModel?) -> Void) {
       
        BBQServiceManager.requestJson(feedbackFromSideMenuReasonList, .get, parameters: nil, headers: headers) { (error, response) in

            guard let result = response else {
                completionHandler(error, nil)
                return
            }

            print(result)
            completionHandler(error, FeedbackReasonListModel(JSON: result))

        }
    
    }
    

    
    func feedbackReasonListFromSideMenuForBulkOrder( completionHandler:@escaping (_ error: BBQError?, _ response: FeedbackReasonListModel?) -> Void) {
       
        BBQServiceManager.requestJson(feedbackFromSideMenuReasonListForBulkOrder, .get, parameters: nil, headers: headers) { (error, response) in

            guard let result = response else {
                completionHandler(error, nil)
                return
            }

            print(result)
            completionHandler(error, FeedbackReasonListModel(JSON: result))

        }
    
    }
    func getReferralCodeToShare(params: [String: Any], completionHandler:@escaping (_ error: BBQError?, _ response: ReferalModel?) -> Void) {
       
        BBQServiceManager.requestJsonWithNewServer(yourReferalCodeToShare, .post, parameters: params, headers: headers) { (error, response) in

            guard let result = response else {
                completionHandler(error, nil)
                return
            }

            completionHandler(error, ReferalModel(JSON: result))

        }
    
    }
    
    func ShareYourReferralCodeToShare(params: [String: Any], completionHandler:@escaping (_ error: BBQError?, _ response: ReferalModel?) -> Void) {
       
        BBQServiceManager.requestJsonWithNewServer(referalCodeToUsedForSignUp, .post, parameters: params, headers: headers) { (error, response) in

            guard let result = response else {
                completionHandler(error, nil)
                return
            }

            completionHandler(error, ReferalModel(JSON: result))

        }
    
    }
    
    
}

