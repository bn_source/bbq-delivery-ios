//
//  LocationManager.swift

import Foundation
import CoreLocation
import UIKit

protocol LocationManagerDelegate: AnyObject {
    func tracingLocation(location: Location)
    func tracingFailureInLocation(error: NSError)
    func locationDisabled()
}

class LocationManager: NSObject, CLLocationManagerDelegate  {
    static let sharedInstance = LocationManager()
    private let locationManager = CLLocationManager()
    private let geoCoder = CLGeocoder()
    weak var delegate: LocationManagerDelegate?

    func initLocationManager() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.distanceFilter = kCLDistanceFilterNone
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        } else {
            locationPermissionAlert()
        }
    }

    func startUpdateLocation() {
        locationManager.startUpdatingLocation()
    }

    func stopUpdateLocation() {
        locationManager.stopUpdatingLocation()
    }

    func locationPermissionAlert() {
        /*let alert = UIAlertController(title: "Location Permission Denied", message: "Please switch on the Location permission from your phone's app settings", preferredStyle: .alert)
        let callAction = UIAlertAction(title: "OK", style: .default, handler: { action in
            if let settings = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(settings, options: [:], completionHandler: nil)
            }
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        alert.addAction(callAction)
        alert.show()*/
        delegate?.locationDisabled()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            self.startUpdateLocation()
        } else if status == .denied || status == .restricted {
            self.stopUpdateLocation()
            locationPermissionAlert()
//            alert.show()
        
    }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            getPlacemark(currentLocation: location)
        }
    }

    func locationManager(_ manager: CLLocationManager, didFinishDeferredUpdatesWithError error: Error?) {
        if let delegate = delegate {
            delegate.tracingFailureInLocation(error: error! as NSError)
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }

     func getPlacemark(currentLocation: CLLocation) {
        geoCoder.reverseGeocodeLocation(currentLocation) { (placemark, error) in
            if let err = error {
                print("error found while get place mark in LocationManager: \(err.localizedDescription)")
            } else {
                var location = Location()
                var addressStringArray = [String]()
                if let placemark = placemark {
                    if let street = placemark.first?.thoroughfare {
                        location.street = street
                        addressStringArray.append(street)
                    }

                    if let city = placemark.first?.locality {
                        location.city = city
                        addressStringArray.append(city)
                    }

                    if let state = placemark.first?.administrativeArea {
                        location.state = state
                        addressStringArray.append(state)
                    }

                    if let country = placemark.first?.country {
                        location.country = country
                        addressStringArray.append(country)
                    }

                    if let zip = placemark.first?.postalCode {
                        location.zip = zip
                        addressStringArray.append(zip)
                    }

                    if addressStringArray.count > 0 {
                        location.address = addressStringArray.joined(separator: ", ")
                    }
                }

                location.latitude = String(currentLocation.coordinate.latitude)
                location.longitude = String(currentLocation.coordinate.longitude)
                if let delegate = self.delegate {
                    self.stopUpdateLocation()
                    delegate.tracingLocation(location: location)
                    
                }
            }
        }
    }

}


struct Location {
    var tag: String?
    var city: String = ""
    var venue: String = ""
    var street: String?
    var state: String = ""
    var country: String?
    var latitude: String = ""
    var longitude: String = ""
    var zip: String?
    var address: String = ""
    var stateId: String?
    var cityId: String?
}

    /*let keyWindow = UIApplication.shared.connectedScenes
    .lazy
    .filter { $0.activationState == .foregroundActive }
    .compactMap { $0 as? UIWindowScene }
    .first?
    .windows
    .first { $0.isKeyWindow }*/
