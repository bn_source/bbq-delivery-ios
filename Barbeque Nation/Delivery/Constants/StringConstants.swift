//
//  StringConstants.swift
//  BBQ
//
//  Created by Rahul S on 08/08/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import Foundation

let GOOGLE_PLACES_API_KEY = "AIzaSyAxRnmryymFC2PbQY8fPfMJWNm2E9JFNIw"
let GOOGLE_MAPS_API_KEY = "AIzaSyAxRnmryymFC2PbQY8fPfMJWNm2E9JFNIw"
let ACCESS_TOKEN = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6Ijk3YjJhMTY5ZWMwNGNkNzEwNThiNWE1MjNmMTMwM2VhYWZmYmFmODI4NTNkMzViNzE5YWEwZmIyNGExNWRiODdmMzJjYjI5MTMwM2RlNTg2In0.eyJhdWQiOiIzNjYzNGI2NC1kNGZlLTQ2NTktYTA2Yi04YzBiMjNiMTYzMzUiLCJqdGkiOiI5N2IyYTE2OWVjMDRjZDcxMDU4YjVhNTIzZjEzMDNlYWFmZmJhZjgyODUzZDM1YjcxOWFhMGZiMjRhMTVkYjg3ZjMyY2IyOTEzMDNkZTU4NiIsImlhdCI6MTYwMTc2OTA0OCwibmJmIjoxNjAxNzY5MDQ4LCJleHAiOjE2MDQzNjEwNDgsInN1YiI6IjE1NzIiLCJzY29wZXMiOlsiYXV0aGVudGljYXRlZCIsImN1c3RvbWVyIl0sIm1vYmlsZV9udW1iZXIiOiI5MDMyMjkyNzUwIiwiY291bnRyeV9jb2RlIjoiKzkxIiwiYXVkX2siOiJ1cmM0eFljNGVGMHMyOEdQMkZRUmFRPT0ifQ.BUXY-A8kx0W_U6553F76TfjQ9g0dWX7j0qAjINqvbsfDGv1UuJCxhR1x7EE7ivlu8Ul_B-qgqLNPBxjKKVst4kSypQ8Q8uvxwzZZs0ChAdOG8_-hba6I2z-V0BjfZ1PVfnJvPWckGYrI_Jki6XSd3wqVNx7A2EUnR3adhDKKMCVMMidi5ex305ddnGARxBNLEjxqRymk1OMIYnMw7haNFyB6bQAkZAtM8VLD_U_n7C5dsldRBhwtfTH-4Xzmi3iwzxdD-OQUGvtJIbC3A02f0ZU85aU-KqbqejRHe3NFvkb4icmdVMVSHx4hyY9wwDWwT6o04Ya7aBWThefIMukkYtYnj29O4ctXFiz6d_nC-pWGpEvhDFypMrbKZjIGQINZ-i8hc2rno03AwYPb-P_yefXNcIQW8igj4-5OcK5CsiSAjEPH3_kDcAu6mIWbax9LR5-10Le1krKuR2UuR_zsNHCp6HgIJVSmGP6ypc2iF8V4sgfRs5t3o8RKlkF12zd8W4Uu5GhDTe077YEcUVqDuJ83Wt8dU3Cg5mr3CYjXPhBcQYQm7PrktC1EEJ738rCKXjlPhihB5YVYW9ZGhiqjnnGNoAO2eyP0aQIpNkrccjRCP_pPll4e_du9fW784_OGtxhS9kjchavimr-Y7SkYKZbrQq8QfBWmSONU03HdDLQ"
let MOENGAGE_KEY = "8IZ5V9MC7LBK43EVNKGXKKFX"
let MOENGAGE_DEBUG_KEY = "8IZ5V9MC7LBK43EVNKGXKKFX_DEBUG"
//let BASE_URL = "https://bbq-stage-mobile.bnhl.in/api/v1"
let BASE_URL = "https://api.barbequenation.com/api/v1"
let DOMAIN = "https://api.barbequenation.com"

let CLIENT_ID = "36634b64-d4fe-4659-a06b-8c0b23b16335"
let CLIENT_SECRET = "k!w6hr5+ZJ"


//API Request
let deliveryPromotions = "/delivery-promotions"
let deliveryCatalog = "/delivery/delivery-catalog"
let getCartHomepage = "/delivery/get-cart-homepage"
let addOrRemoveItem = "/delivery/cart-item"
let getAllNearByBranches = "/get-all-nearby-branches"
let getAllNearByBranchesV1 = "/get-all-nearby-branches-v1"
let branches = "/branches"
let getCustomerAddress = "/delivery/get-address"
let getCartDetails = "/delivery/get-cart-details"
let generatePayment = "/delivery/generate-payment"
let validatePayment = "/delivery/validate-payment"
let checkDistanceForDelivery = "/delivery/check-distance"
let clearCart = "/delivery/delete-cart"
let addAddress = "/delivery/add-address"
let deletAddress = "/delivery/delete-address"
let orderHistory = "/delivery/order-history-list"
let orderDetail = "/delivery/order-history-detail"
let updateOrderAddress = "/delivery/update-order-address"
let useAllSmiles = "/delivery/update-payment"
let removeVoucherOrCoupons = "/delivery/remove-order-payment"
let getDeliveryStatus = "/delivery/get-order-status"
let getTakeawySlots = "/delivery/get-takeaway-time"
let updateTakewaySlot = "/delivery/update-takeaway-time"
let getCoupons = "/delivery/get-voucher-coupon"
let addVoucherOrCoupon = "/delivery/update-payment"
let validateCoupon = "/delivery/validate-voucher-coupon"
let safetyHygiene = "/safety-and-hygiene"
let getOrderFeedback = "/feedback/get-feedback"
let submitOrderFeedback = "/feedback/save-feedback"
let orderingForDetail = "/delivery/update-cart-details"
let submitFeedbackFromSideMenu = "/delivery/feedback"
let feedbackFromSideMenuReasonList = "/delivery/feedback-question?isBulkOrder=false"
let feedbackFromSideMenuReasonListForBulkOrder = "/delivery/feedback-question?isBulkOrder=true"
let yourReferalCodeToShare = "/referral-code"
let referalCodeToUsedForSignUp = "/receiver-code"


//Error message
let UNEXPECTED_ERROR = "Unexpected error!"
let LOADING_ERROR = "Loading..."
