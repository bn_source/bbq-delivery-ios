//
//  UIFont+Extensions.swift
//  BBQ
//
//  Created by Dharani Sadasivam on 23/08/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import Foundation
import UIKit

enum FontWeight: String {
    case regular = "Poppins-Regular"
    case semibold = "Poppins-SemiBold"
    case bold = "Poppins-Bold"
    case light = "Poppins-Light"
    case medium  = "Poppins-Medium"
}


extension UIFont {
    class func systemFontOfSize(size: CGFloat, weight: FontWeight) -> UIFont {
        return UIFont(name: weight.rawValue, size: size)!
    }
}

extension UILabel{
    func setTheme(size: CGFloat, weight: FontWeight, color: UIColor = .text) {
        self.font = UIFont.systemFontOfSize(size: size, weight: weight)
        self.textColor = color
    }
    
    // With the code below setting your margins is as easy as label.setMargins(15).
    
    func setMargins(_ margin: CGFloat = 10) {
        if let textString = self.text {
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.firstLineHeadIndent = margin
            paragraphStyle.headIndent = margin
            paragraphStyle.tailIndent = -margin
            let attributedString = NSMutableAttributedString(string: textString)
            attributedString.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedString.length))
            attributedText = attributedString
        }
    }
}

extension UIButton{
    func setTheme(size: CGFloat, weight: FontWeight, color: UIColor = .text) {
        self.titleLabel?.font = UIFont.systemFontOfSize(size: size, weight: weight)
        self.setTitleColor(color, for: .normal)
    }
}

