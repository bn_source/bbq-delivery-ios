//
//  UIImageView+Extension.swift
//  BBQ
//
//  Created by Shareef on 05/09/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import UIKit
import SDWebImage

extension UIImageView {
    
    func setImage(url: String, placeholderImage: UIImage? = UIImage.itemPlaceholderImage, isForPromotions: Bool) {
        
        self.image = nil
        if let data = self.readImageFromLocalServer(url: url), let content = UIImage(data: data){
            
            self.image = content
            
            return
        }else{
            self.sd_imageIndicator = SDWebImageActivityIndicator.gray
        }
        //        sd_setImage(with: URL(string: url), placeholderImage: UIImage())
        
        self.sd_setImage(with: URL(string:url), placeholderImage: placeholderImage, options: .highPriority, progress: nil
                         , completed: { (image, error, cacheType, response_url) in
            
            DispatchQueue.main.async {
                if isForPromotions{
                    if let data = image?.jpegData(compressionQuality: 1.0){
                        self.saveImageFileToLocalStorage(url: url, data: data)
                        
                    }
                }else{
                    if let data = image?.jpegData(compressionQuality: 0.001){
                        
                        self.saveImageFileToLocalStorage(url: url, data: data)
                        
                    }
                }
                
            }
        })
        
    }
    
    func setImagePNG(url: String, placeholderImage: UIImage? = UIImage.itemPlaceholderImage) {
        
        self.image = nil
        self.backgroundColor = .white
        self.setCornerRadius(self.frame.size.width/2.0)
        if let data = self.readImageFromLocalServer(url: url), let content = UIImage(data: data){
            
            self.image = content
            
            return
        }else{
            self.sd_imageIndicator = SDWebImageActivityIndicator.gray
        }
        //        sd_setImage(with: URL(string: url), placeholderImage: UIImage())
        
        self.sd_setImage(with: URL(string:url), placeholderImage: placeholderImage, options: .highPriority, progress: nil
                         , completed: { (image, error, cacheType, response_url) in
            
            DispatchQueue.main.async {
                if let data = image?.pngData(){
                    self.saveImageFileToLocalStorage(url: url, data: data)
                    
                }
            }
        })
        
    }
    
    func setImage(imageUrl: String, placeholderImage: UIImage? = UIImage.itemPlaceholderImage) {
        self.image = nil
        self.sd_setImage(with: URL(string:imageUrl), placeholderImage: placeholderImage, options: .highPriority, progress: nil, completed: nil)
        
    }
    
    func getLocalImage(imageUrl: String) -> UIImage? {
        if let data = self.readImageFromLocalServer(url: imageUrl), let content = UIImage(data: data){
            return content
        }
        return nil
    }
    
    private func saveImageFileToLocalStorage(url: String, data: Data){
        if fileExistsonPath(url: url){
            return
        }
        let directoryPath =  NSHomeDirectory().appending("/Documents/")
        if !FileManager.default.fileExists(atPath: directoryPath) {
            do {
                try FileManager.default.createDirectory(at: NSURL.fileURL(withPath: directoryPath), withIntermediateDirectories: true, attributes: nil)
            } catch {
                print(error)
            }
        }
        let filepath = directoryPath.appending(url.replacingOccurrences(of: "://", with: "_").replacingOccurrences(of: "/", with: "_"))
        let url = NSURL.fileURL(withPath: filepath)
        do {
            try data.write(to: url, options: .atomic)
        } catch {
            print(error)
            print("file cant not be save at path \(filepath), with error : \(error)")
        }
    }
    
    private func fileExistsonPath(url: String) -> Bool{
        let directoryPath =  NSHomeDirectory().appending("/Documents/").appending(url.replacingOccurrences(of: "://", with: "_").replacingOccurrences(of: "/", with: "_"))
        if FileManager.default.fileExists(atPath: directoryPath) {
            return true
        }
        return false
    }
    
    func setLocalStoredImage(url: String){
        if let data = self.readImageFromLocalServer(url: url), let content = UIImage(data: data){
            self.image = content
        }else{
            self.sd_imageIndicator = SDWebImageActivityIndicator.gray
        }
    }
    
    private func readImageFromLocalServer(url: String) -> Data?{
        let directoryPath =  NSHomeDirectory().appending("/Documents/")
        if !FileManager.default.fileExists(atPath: directoryPath) {
            return nil
        }
        let filepath = directoryPath.appending(url.replacingOccurrences(of: "://", with: "_").replacingOccurrences(of: "/", with: "_"))
        let url = NSURL.fileURL(withPath: filepath)
        do {
            return try Data(contentsOf: url)
        } catch {
            return nil
        }
    }
    
}
