//
 //  Created by cumulations on 22/06/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified Date+BBQNFormattedString.swift
 //

import Foundation


extension Date
{
    var bbqnDOBFormattedDateString  : String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: self)
    }
    
    var bbqnPresentedFormattedDateString  : String
    {
        let dateFormatter = DateFormatter()
         dateFormatter.dateFormat = "MMM d, yyyy"
        return dateFormatter.string(from: self)
    }
    
    static func date(from bbqnDOBFormattedDateString:String) -> Date?
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.date(from: bbqnDOBFormattedDateString)!
    }
    
}
