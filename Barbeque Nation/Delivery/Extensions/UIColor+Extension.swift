//
//  UIColor+Extension.swift
//  BBQ
//
//  Created by Shareef on 05/09/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import UIKit

extension UIColor {
    
    static let theme = UIColor(named: "ThemeColor")!
    static let text = UIColor(named: "TextColor")!
    static let darkText = UIColor(named: "DarkTextColor")!
    static let darkGreen = UIColor(named: "Green")!
    static let themeYello = UIColor(red: 255.0/255.0, green: 136.0/255.0, blue: 0.0/255.0, alpha: 1.0)
    static let blue = UIColor(red: 0.0/255.0, green: 116.0/255.0, blue: 189.0/255.0, alpha: 1.0)
    static let lightBackground = UIColor(red: 249.0/255.0, green:249.0/255.0, blue:249.0/255.0, alpha:1.0)
    static let lightThemeBackground = UIColor(red: 251.0/255.0, green:208.0/255.0, blue:165.0/255.0, alpha:1.0)
    
    static var deliveryThemeColor = UIColor.theme//UIColor(red: 242.0/255.0, green:196.0/255.0, blue:79.0/255.0, alpha:1.0)
    static var deliveryThemeTextColor = UIColor.white
    //static var deliveryThemeBackgroundColor  = UIColor(red: 0/255.0, green:51.0/255.0, blue:25.0/255.0, alpha:0.15)
    static var deliveryThemeBackgroundColor: UIColor {
        get {
            return UIColor.deliveryThemeColor.withAlphaComponent(0.15)
        }
    }
}

extension String{
    func hexStringToUIColor() -> UIColor {
        var cString:String = self.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

extension NSMutableAttributedString {

   public func setAsLink(textToFind:String, linkURL:String) -> Bool {

       let foundRange = self.mutableString.range(of: textToFind)
       if foundRange.location != NSNotFound, let url = URL(string: linkURL) {
           self.addAttribute(NSAttributedString.Key.link, value: url, range: foundRange)
           return true
       }
       return false
   }
}
