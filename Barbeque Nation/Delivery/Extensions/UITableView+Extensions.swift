//
 //  Created by Arpana on 15/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified UITableView+Extensions.swift
 //

import UIKit

extension UITableView {
    func getCell<T:UITableViewCell>(cell:T.Type,indexPath:IndexPath) -> T {
          
          let string = String.init(describing: cell)
        let cell = self.dequeueReusableCell(withIdentifier: string, for: indexPath) as! T
          return cell
      }
    func updateRowHeightsWithoutReloadingRows(animated: Bool = false) {
        let block = {
            self.beginUpdates()
            self.endUpdates()
        }
        
        if animated {
            block()
        }
        else {
            UIView.performWithoutAnimation {
                block()
            }
        }
    }
    func addDefaultCell()  {
        self.register(UITableViewCell.self, forCellReuseIdentifier: "default")
    }
    
    func defaultCell(indexPath: IndexPath) -> UITableViewCell {
        addDefaultCell()
        return self.dequeueReusableCell(withIdentifier: "default", for: indexPath)
    }
    
    
    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont(name: Constants.App.BBQAppFont.ThemeRegular, size: 18)
        messageLabel.sizeToFit()

        self.backgroundView = messageLabel
        self.separatorStyle = .none
    }

    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
}
extension UITableViewCell {
    var tableView: UITableView? {
        return (next as? UITableView) ?? (parentViewController as? UITableViewController)?.tableView
    }
}
//extension Collection {
//  subscript(safe index: Index) -> Iterator.Element? {
//    guard indices.contains(index) else { return nil }
//    return self[index]
//  }
//}
