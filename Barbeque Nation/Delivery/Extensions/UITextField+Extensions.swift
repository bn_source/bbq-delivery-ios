//
//  UITextField+Extensions.swift
//  BBQ
//
//  Created by Dharani Sadasivam on 11/08/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    /**
     Add right view to UITextField
     - Parameter view: View to be added in right side
     */
    func addRightView(_ view: UIView) {
        self.rightView = view
        self.rightViewMode = .always
    }

    /**
     Add left view to UITextField
     - Parameter view: View  to be added in left side
     */
    func addLeftView(_ view: UIView) {
        self.leftView = view
        self.leftViewMode = .always
    }
    
    /**
     Add left view with image to UITextField
     - Parameter image: image to be added in left side
     */
    func addLeftViewWith(_ image: UIImage) {
        let view = UIView()
        view.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let imageView = UIImageView(image: image)
        imageView.frame = CGRect(x: 0, y: 5, width: 20, height: 20)
        imageView.contentMode = .scaleAspectFit
        view.addSubview(imageView)
        self.addLeftView(view)
    }

    /**
     Add right view with image and custome tintcolor to UITextField
     - Parameter image: image to be added in left side
     - Parameter color: tint color of image
     */
    func addRightViewWith(_ image: UIImage, color: UIColor) {
        let view = UIView()
        view.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = color
        imageView.frame = CGRect(x: 0, y: 5, width: 20, height: 20)
        view.addSubview(imageView)
        self.addRightView(imageView)
    }
    func setRightButton(_ image: UIImage) {
        
            
        let iconView = UIButton(type: .custom)
        iconView.setImage(image, for: .normal)
        iconView.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

        iconView.frame =  CGRect(x:  0, y: 5, width: 30, height: 30)
        
        let iconContainerView: UIView = UIView(frame:
                                                CGRect(x: 20, y: 0, width: 40, height: 40))
        iconContainerView.addSubview(iconView)
        rightView = iconContainerView
        rightViewMode = .always
    }
    
    func maxCharacter(shouldChangeCharactersIn range: NSRange, replacementString string: String, maxLength: Int) -> Bool  {
        let maxLength = 1
        let currentString = (text ?? "") as NSString
        let newString = currentString.replacingCharacters(in: range, with: string)
        return newString.count <= maxLength
    }
}
