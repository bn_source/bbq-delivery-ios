//
//  UIViewController+Extensions.swift
//  BBQ
//
//  Created by Dharani Sadasivam on 21/09/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import Foundation
import UIKit
extension UIViewController {

 
    func alert(message: String, title: String = "") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    var statusBarHeight:CGFloat {
       return UIApplication.shared.statusBarFrame.height

    }
    @objc @IBAction func backAction() {
        if(self.navigationController != nil){
            self.navigationController?.popViewController(animated: true)
        }
        else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    func addHeader(data:CellDataProvider) -> BBQPaymentHeaderView {
        let header:BBQPaymentHeaderView = .fromNib()
        header.didSetData(data: data)
        header.backButton.addTarget(self, action: #selector(backAction), for: UIControl.Event.touchUpInside)
        
        header.frame = CGRect.init(x: 0, y: statusBarHeight, width: self.view.frame.width, height: 55)
        self.view.addSubview(header)
        return header;
    }
    
    func navigateToHomePage() {
        var isFound = false
        if let rootVC = UIApplication.shared.keyWindow?.rootViewController as? DeliveryHostMenuViewController, let navVC = rootVC.contentViewController as? UINavigationController {
            let tabBarController = navVC.viewControllers
            for vc in tabBarController{
                if vc.isKind(of: DeliveryTabBarController.self){
                    navVC.popToViewController(vc, animated: true)
                    isFound = true
                    break
                }
            }
        }
        if !isFound{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func navigateToLoginForceFull() {
        if let rootVC = UIApplication.shared.keyWindow?.rootViewController as? DeliveryHostMenuViewController, let navVC = rootVC.contentViewController as? UINavigationController {
            if let tabBarController = navVC.viewControllers.first(where: {$0 is LoginBackground}) as? LoginBackground {
                navVC.popToViewController(tabBarController, animated: true)
            }else{
                let tabViewController = UIStoryboard.loadLoginBackground()
                navVC.setViewControllers([tabViewController], animated: true)
            }
        }
    }
    
    func navigateToHomePageForceFull() {
        if let rootVC = UIApplication.shared.keyWindow?.rootViewController as? DeliveryHostMenuViewController, let navVC = rootVC.contentViewController as? UINavigationController {
            if let tabBarController = navVC.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
                navVC.popToViewController(tabBarController, animated: true)
            }else{
                let tabViewController = UIStoryboard.loadLoginBackground()
                navVC.setViewControllers([tabViewController], animated: true)
            }
        }
    }
    
    func getTabBarVC() -> DeliveryTabBarController? {
        if let rootVC = UIApplication.shared.keyWindow?.rootViewController as? DeliveryHostMenuViewController, let navVC = rootVC.contentViewController as? UINavigationController, let tabBarController = navVC.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
            return tabBarController
        }
        return nil
    }
    
    func openReservationController(){
//        navigateToHomePage()
        if let tabBarController = navigationController?.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
            tabBarController.selectedIndex = 0
        }
        
        NotificationCenter.default.post(name: Notification.Name(Constants.NotificationName.showBookingScreen),
                                        object: nil)
    }
    
    func openHappinesscardController(){
        navigateToHomePage()
        if let tabBarController = navigationController?.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
            tabBarController.selectedIndex = 0
            isClickedOnHpc = true
        }
    }
    
    func openBIBDeliveryHomepage(){

//        let storyboard = UIStoryboard(name: "DeliveryHome", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "DeliveryHomeViewController") as! DeliveryHomeViewController
//        // Alternative way to present the new view controller
//        self.navigationController?.pushViewController(vc, animated: true)
                
        if let tabBarController = navigationController?.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
                  tabBarController.selectedIndex = 2
                  self.navigationController?.setViewControllers([tabBarController], animated: true)
              }else{
                  
                  let tabBarController = DeliveryTabBarController()
                  tabBarController.selectedIndex = 2
                  self.navigationController?.setViewControllers([tabBarController], animated: true)

              }
    }
    
    func goToHomePage(selectedIndex: Int? = nil) {
        if let tabBarController = navigationController?.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
            self.navigationController?.popToViewController(tabBarController, animated: true)
            if let selectedIndex = selectedIndex{
                tabBarController.selectedIndex = selectedIndex
            }
        }else{
            if let navigationVC = self.navigationController{
                let tabBarController = DeliveryTabBarController()
                navigationVC.setViewControllers([tabBarController], animated: true)
            }else if let hostVC = UIApplication.shared.keyWindow?.rootViewController as? DeliveryHostMenuViewController, let navigationVC = hostVC.navigationVC{
                let tabBarController = DeliveryTabBarController()
                navigationVC.setViewControllers([tabBarController], animated: true)
            }
        }
    }
    
    func goToLoginPage() {
        if let tabBarController = navigationController?.viewControllers.first(where: {$0 is LoginBackground}) as? LoginBackground {
            self.navigationController?.popToViewController(tabBarController, animated: true)
        }else{
            if let navigationVC = self.navigationController{
                let tabViewController = UIStoryboard.loadLoginBackground()
                navigationVC.setViewControllers([tabViewController], animated: true)
            }else if let hostVC = UIApplication.shared.keyWindow?.rootViewController as? DeliveryHostMenuViewController, let navigationVC = hostVC.navigationVC{
                let tabViewController = UIStoryboard.loadLoginBackground()
                navigationVC.setViewControllers([tabViewController], animated: true)
            }
        }
    }
    
    
    
    /// Get top view controller in window.
     class func getTopViewControllerInWindow() -> UIViewController? {
        guard let window = UIApplication.shared.keyWindow else { return nil }
        
        return topViewControllerWithRootViewController(rootViewController: window.rootViewController)
    }
    
    /// Get top view controller.
       fileprivate static func topViewControllerWithRootViewController(rootViewController: UIViewController!) -> UIViewController! {
           // Tab Bar View Controller
           if rootViewController is UITabBarController {
               let tabbarController =  rootViewController as! UITabBarController
               
               return topViewControllerWithRootViewController(rootViewController: tabbarController.selectedViewController)
           }
           // Navigation ViewController
           if rootViewController is UINavigationController {
               let navigationController = rootViewController as! UINavigationController
               
               return topViewControllerWithRootViewController(rootViewController: navigationController.visibleViewController)
           }
           // Presented View Controller
           if let controller = rootViewController.presentedViewController {
               return topViewControllerWithRootViewController(rootViewController: controller)
           } else {
               return rootViewController
           }
       }
}
