//
//  UIImage+Extension.swift
//  BBQ
//
//  Created by Shareef on 05/09/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import UIKit

extension UIImage {

    static let veg = UIImage(named: "veg")!
    static let nonVeg = UIImage(named: "nonVeg")!
    static let itemPlaceholderImage = UIImage(named: "demo_black")!
    
    func grayscaleImage() -> UIImage? {
        let ciImage = CIImage(image: self)
        guard let grayscale = ciImage?.applyingFilter("CIColorControls",
                                                      parameters: [ kCIInputSaturationKey: 0.0 ]) else {
            return nil
        }
        return UIImage(ciImage: grayscale)
    }
}
