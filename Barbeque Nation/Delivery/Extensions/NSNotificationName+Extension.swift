//
 //  Created by Shareef on 08/11/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified NSNotificationName+Extension.swift
 //

import UIKit

extension NSNotification.Name {
    
    static let menuScrollToTop = Notification.Name(rawValue: "menuScrollToTop")
    
    static let cartTabBadgeCount = Notification.Name(rawValue: "cartTabBadgeCount")
}
