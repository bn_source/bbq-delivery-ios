//
//  UIView+Extensions.swift
//  BBQ
//
//  Created by Dharani Sadasivam on 11/08/20.
//  Copyright © 2020 Rahul S. All rights reserved.
//

import Foundation
import UIKit

extension UIView {

    @IBInspectable var iCornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            //layer.masksToBounds = newValue > 0
        }
    }

    @IBInspectable var iBorderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }

    @IBInspectable var iBorderColor: UIColor? {
        get {
            if let borderColor = layer.borderColor{
                return UIColor(cgColor: borderColor)
            }
            return nil
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    func addCornerRadius(corners: CACornerMask, radius: CGFloat) {
        self.clipsToBounds = false
        self.layer.cornerRadius = radius
        self.layer.maskedCorners = corners
    }
    
    
    func drawBorder(edges: [UIRectEdge], borderWidth: CGFloat, color: UIColor) {
        for item in edges {
            let borderLayer: CALayer = CALayer()
            borderLayer.borderColor = color.cgColor
            borderLayer.borderWidth = borderWidth
            
            switch item {
            case .top:
                borderLayer.frame = CGRect(x: 0, y: 0, width: frame.width, height: borderWidth)
            case .left:
                borderLayer.frame =  CGRect(x: 0, y: 0, width: borderWidth, height: frame.height)
            case .bottom:
                borderLayer.frame = CGRect(x: 0, y: frame.height - borderWidth, width: frame.width, height: borderWidth)
            case .right:
                borderLayer.frame = CGRect(x: frame.width - borderWidth, y: 0, width: borderWidth, height: frame.height)
            case .all:
                drawBorder(edges: [.top, .left, .bottom, .right], borderWidth: borderWidth, color: color)
            default:
                break
            }
            self.layer.addSublayer(borderLayer)
        }
    }
    
    func setCircleView() {
        setCornerRadius(frame.height/2)
    }
    
    func setCornerRadius(_ radius: CGFloat) {
        layer.cornerRadius = radius
        layer.masksToBounds = true
    }
    
    func setShadow(color: UIColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.29), opacity: Float = 0.5, offSet: CGSize = CGSize(width: 0, height: 3), radius: CGFloat = 6, scale: Bool = true) {
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        layer.masksToBounds = false
        
//        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
//        layer.shouldRasterize = true
//        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
}
    var parentViewController: UIViewController? {
        if let nextResponder = self.next as? UIViewController {
            return nextResponder
        } else if let nextResponder = self.next as? UIView {
            return nextResponder.parentViewController
        } else {
            return nil
        }
    }
    class func fromNib<T: UIView>(nibName:String? = nil) -> T {
        var nib = String(describing: T.self)
        if(nibName != nil){
            nib = nibName!
        }
        return Bundle(for: BBQBaseViewController.self).loadNibNamed(nib, owner: nil, options: nil)![0] as! T
    }
    func applyViewMultiBrandTheme() {
        backgroundColor = .deliveryThemeColor
    }
}

extension UIButton{
    func adjustImageAndTitleOffsets(spacing: CGFloat = 4.0) {
        
        self.imageView?.contentMode = .scaleAspectFit
        let imageWidth = (self.imageView?.image?.size.width ?? 0) - frame.size.height + spacing * 2
        self.titleEdgeInsets = UIEdgeInsets(top: 0, left: -imageWidth/2.0 + spacing, bottom: 0, right: 0)
        self.imageEdgeInsets = UIEdgeInsets(top: spacing*1.5, left: -spacing, bottom: spacing*1.5, right: 0)
        self.titleLabel?.adjustsFontSizeToFitWidth = true
        self.titleLabel?.minimumScaleFactor = 0.5
   }
    
    func applyAddRemoveButtonTheme(title: String, color: UIColor) {
        let string =  NSAttributedString(string: title, attributes: [.underlineStyle : NSUnderlineStyle.single.rawValue])
        self.setAttributedTitle(string, for: .normal)
        self.setTitleColor(color, for: .normal)
    }
    
    func applyMultiBrandTheme() {
        applyViewMultiBrandTheme()
        setTitleColor(.deliveryThemeTextColor, for: .normal)
    }
    
    func applyMultiBrandTheme(backgroundColor: UIColor, textColor: UIColor) {
        self.backgroundColor = backgroundColor
        setTitleColor(textColor, for: .normal)
    }
    
    func applyMultiBrandThemeWith(isServicable: Bool, isAvailable:Bool) {
        self.backgroundColor = (isServicable && isAvailable) ? .deliveryThemeColor : .lightGray
        self.setTitleColor((isServicable && isAvailable) ? .deliveryThemeTextColor : .white, for: .normal)
    }
    
    func applyThemeOnText() {
        setTitleColor(.deliveryThemeTextColor, for: .normal)
    }
}

extension UILabel{
    func applyMultiBrandTheme() {
         applyViewMultiBrandTheme()
         textColor = .deliveryThemeTextColor
    }
    
    func applyThemeOnText() {
         textColor = .deliveryThemeColor
    }
}

