//
 //  Created by Arpana Rani on 19/02/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQImageTableViewCell.swift
 //

import UIKit

class BBQImageTableViewCell: UITableViewCell {

    var imageArray = [String]()
    @IBOutlet weak var collectionForrOrderDispatch: UICollectionView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpImage(){
        
        collectionForrOrderDispatch.reloadData()
    }

}

extension BBQImageTableViewCell : UICollectionViewDelegate, UICollectionViewDataSource  {
    
    // MARK: CollectionView Methods

    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return  imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return UICollectionViewCell()

    }
    
   

    
    
}
