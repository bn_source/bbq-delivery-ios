//
 //  Created by Arpana Rani on 19/02/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQDeliveryTableViewCell.swift
 //

import UIKit

protocol  BBQDeliveryTableViewCellCallDelegate {
    func call()
}

class BBQDeliveryTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var BtnCall: UIButton!
    var callDelegate : BBQDeliveryTableViewCellCallDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setData()  {
        
        labelTitle.text = "adsac"
        
    }
    @IBAction func callBtnPressed (_ sender : Any){
        
        callDelegate?.call()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
