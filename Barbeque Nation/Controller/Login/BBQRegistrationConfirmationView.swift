//
//  BBQRegistrationConfirmationView.swift
//  Barbeque Nation
//
//  Created by Rabindra L on 12/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit
import FirebaseAnalytics

class BBQRegistrationConfirmationView: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var navigationTextLabel: UILabel!
    
    var titleText: String = ""
    var navigationText: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        AnalyticsHelper.shared.screenName(name: "RegistrationConfirmationScreen", className: "BBQRegistrationConfirmationView")
        Analytics.logEvent("RegistrationConfirmation", parameters: [
            "name": "RegistrationConfirmation" as NSObject,
            "full_text": "RegistrationConfirmation page" as NSObject
            ])
        
        // Do any additional setup after loading the view.
        self.titleLabel.text = titleText
        self.navigationTextLabel.text = navigationText
    }
}
