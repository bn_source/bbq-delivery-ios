//
//  PickerViewController.swift
/*
 *  Created by Rabindra L on 05/08/19.
 *  Copyright © BARBEQUE NATION HOSPITALITY LIMITED 2019. All rights reserved.
 *  Last modified 03/09/19
 
 * CHANGE LOG
 * —------------------------------------------------------------------------------
 * ID          Bug ID      Author Name          Date                  Description
 * —------------------------------------------------------------------------------
 * 1            NA         Rabindra L           05/08/19        Initial Version
 * 2            1          Rabindra L           03/09/19        Review comments
 */

import UIKit

protocol PickerViewDelegate: AnyObject {
    func countryCodeSelected(phoneCode: String)
    func titleSelected(title: String)
    func pickerClosed()
}

enum pickerType: Int
{
    case countryCode = 0
    case title = 1
    
}
class PickerViewController: UIViewController {
    
    enum searchBarSize:CGFloat {
        case width = 160.0
        case height = 40.0
    }
    var type: pickerType? = .title
    var countryList:[CountryCodeModel]?
    var totalCountryList:[CountryCodeModel]?
    var keyboardHeight:CGFloat = 0
    var searchQuery = ""
    let messagelabel:UILabel = UILabel()
    
    @IBOutlet weak var searchBarConst: NSLayoutConstraint!
    
    @IBOutlet weak var toolbar: UIToolbar!
    @IBOutlet weak var pickerView: UIPickerView!
    let searchBar = UISearchBar(frame: CGRect(x: 0, y: 0,
                                              width: Int(UIScreen.main.bounds.width-searchBarSize.width.rawValue),
                                              height: Int(searchBarSize.height.rawValue)))
    
    weak var delegate:PickerViewDelegate? = nil
    private var index = 0
    var isComingFromProfile = false
    var titleArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.done
        if isComingFromProfile == true {            
        }else {
            blurView()
        }
        toolBarSetUp()
        
        let model = CountryCodeViewModel()
        countryList = model.getCountryList()
        totalCountryList = countryList
        titleArray = model.getTitleList()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        
        let keyboardTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        self.view.addGestureRecognizer(keyboardTap)
        
        if type! == .countryCode {
            selectCurrentCountry()
        } else {
            index = 0
        }
        
        addLabelToPickerView()
    }
    
    func addLabelToPickerView() {
        messagelabel.text = kNoResultFound
        messagelabel.textAlignment = .center
        messagelabel.font = UIFont.appThemeBoldWith(size: 17.0)
        
        let width:CGFloat = pickerView.frame.width
        let textHeight = (messagelabel.text?.height(withConstrainedWidth: width, font: messagelabel.font))!
        let positionY:CGFloat = (pickerView.frame.height/2 - textHeight/2)
        messagelabel.frame = CGRect(x: 0,
                                    y: positionY,
                                    width: width,
                                    height: textHeight)
        self.pickerView.addSubview(messagelabel)
        messagelabel.isHidden = true
    }
    
    private func selectCurrentCountry() {
        for country in 0 ..< countryList!.count {
            if countryList?[country].phoneNumberCode.uppercased() == BBQUserDefaults.sharedInstance.phoneCode {
                index = country
                break
            }
        }
        
        self.pickerView.selectRow(index, inComponent: 0, animated: true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        doneButtonTapped()
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            keyboardHeight = keyboardRectangle.height
        }
        
        searchBarConst.constant = keyboardHeight
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
        searchBarConst.constant = 0
    }
    
    func blurView() {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(blurEffectView)
        self.view.addSubview(pickerView)
        self.view.addSubview(toolbar)
    }
    
    func toolBarSetUp() {
        
        // let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let cancelButton = UIBarButtonItem(title: kCancelButton,
                                           style: .plain,
                                           target: self,
                                           action: #selector(PickerViewController.cancelButtonTapped))
        let doneButton = UIBarButtonItem(title: kDoneButton,
                                         style: .plain,
                                         target: self,
                                         action: #selector(PickerViewController.doneButtonTapped))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        switch type! {
        case .countryCode:
            
            let search = UIBarButtonItem(customView: searchBar)
            
            toolbar.setItems([search,doneButton,cancelButton], animated: false)
            
            
        case .title:
            toolbar.setItems([doneButton,flexibleSpace,cancelButton], animated: false)
            
        }
        
        toolbar.isUserInteractionEnabled = true
        
        
    }
    
    @objc func doneButtonTapped() {
        
        switch type! {
        case .countryCode:
            if let delegate = self.delegate {
                if countryList?.count ?? 0 > 0 {
                    let countryListModel = countryList?[index]
                    let countrycode:String  = countryListModel?.phoneNumberCode ?? ""
                    delegate.countryCodeSelected(phoneCode: countrycode)
                } else {
                    delegate.countryCodeSelected(phoneCode: BBQUserDefaults.sharedInstance.phoneCode)
                }
            }
        case .title:
            if let delegate = self.delegate {
                delegate.titleSelected(title: titleArray[index])
            }
        }
        
        
        self.closePickerView()
    }
    
    @objc func cancelButtonTapped() {
        self.view.endEditing(true)
        self.closePickerView()
        if let delegate = self.delegate {
            delegate.pickerClosed()
        }
    }
    
    
    @objc func dismissViewController() {
        
        self.closePickerView()
        if let delegate = self.delegate {
            delegate.pickerClosed()
        }
    }
    
    func closePickerView() {
        self.dismiss(animated: true, completion: nil)
    }
}


extension PickerViewController:UIPickerViewDelegate, UIPickerViewDataSource, UISearchBarDelegate {
    
    func updateSearchResults(for searchController: UISearchController) {
        //if you want to start searching on each keystroke, you implement this method. I'm going to wait until they click Search.
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if type! == .countryCode {
            if countryList!.count > 0 {
                messagelabel.isHidden = true
            } else {
                messagelabel.isHidden = false
            }
            return countryList!.count
        }
        else {
            return titleArray.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        if type! == .countryCode {
            let countryCell = Bundle.main.loadNibNamed("CountryCodeCell", owner: self, options: nil)?.first as? CountryCodeCell
            if let model = countryList?[row] {
                countryCell?.countryFlag.text = model.flag
                countryCell?.countryName.text = model.countryName
                countryCell?.ISDCode.text = String(model.phoneNumberCode)
            }
            
            countryCell?.frame.size.width = self.view.frame.size.width;
            return countryCell!
        }
        else {
            let label = UILabel()
            label.text = titleArray[row]
            label.textAlignment = .center
            return label
        }
        
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        index = row
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchQuery = searchText
        let withPlusQuery = "+" + searchQuery
        if searchQuery.count == 0 {
            countryList = totalCountryList
        } else {
            countryList = []
            for country in totalCountryList! {
                if country.countryName.capitalized.hasPrefix(searchQuery.capitalized) {
                    countryList?.append(country)
                } else if country.phoneNumberCode.hasPrefix(searchQuery) {
                    countryList?.append(country)
                } else if country.phoneNumberCode.hasPrefix(withPlusQuery) {
                    countryList?.append(country)
                }
                if let list = countryList {
                    index = list.count / 2 // Selecting middle of result list by default
                } else {
                    index = 0
                }
                
            }
        }
        self.pickerView.reloadAllComponents()

        self.pickerView.selectRow(index, inComponent: 0, animated: true)
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let set = NSCharacterSet(charactersIn: Constants.values.alphabetAndNumberOnly)
        let inverted = set.inverted
        let filtered = text.capitalized.components(separatedBy: inverted).joined(separator: "")
        return filtered == text.capitalized
    }
}

