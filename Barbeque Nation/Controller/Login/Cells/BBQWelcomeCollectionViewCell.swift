//
 //  Created by Sridhar on 17/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQWelcomeCollectionViewCell.swift
 //

import UIKit

class BBQWelcomeCollectionViewCell: UICollectionViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
    }

}
