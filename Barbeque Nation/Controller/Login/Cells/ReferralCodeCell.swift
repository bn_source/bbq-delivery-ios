//
 //  Created by Maya R on 26/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified ReferralCodeCell.swift
 //

import UIKit

protocol ReferralCellDelegate {
    func ReferralCodeEntered(length: Int, text: String)
    //Called when keyboard return key pressed

    func didReferralReturnKeyPressed()

}
class ReferralCodeCell: UITableViewCell {
    @IBOutlet weak var referralTitleLabel: UILabel!
    @IBOutlet weak var referralTextField: UITextField!
    @IBOutlet weak var referralErrorLabel: UILabel!
    var currentTheme = theme.white {
        didSet {
            if currentTheme == .white {
                self.contentView.backgroundColor = UIColor.clear
                self.referralTextField.textColor = Constants.App.themeColor.white
                self.referralTitleLabel.textColor = Constants.App.themeColor.white
                self.referralErrorLabel.textColor = Constants.App.themeColor.white
                self.referralTextField.addLine(position: .LINE_POSITION_BOTTOM, color: .white, width: 1)
                let attributes = self.getTheAttributeWIthGivenColor(color: Constants.App.themeColor.white)
                self.referralTextField.attributedPlaceholder = NSAttributedString(string: kEnterInviteCode, attributes: attributes)
            } else {
                self.contentView.backgroundColor = UIColor.white
                referralTextField.textColor = Constants.App.themeColor.grey
                referralErrorLabel.textColor = Constants.App.themeColor.grey
                referralTitleLabel.textColor = Constants.App.themeColor.grey
                referralTextField.addLine(position: .LINE_POSITION_BOTTOM, color: .black, width: 1)

                let attributes = self.getTheAttributeWIthGivenColor(color: Constants.App.themeColor.grey)
                self.referralTextField.attributedPlaceholder = NSAttributedString(string: kEnterInviteCode, attributes: attributes)
            }
        }
    }
    var delegate: ReferralCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.red.withAlphaComponent(0)
        // Initialization code
        self.referralTitleLabel.text = kEnterInviteCode
        self.referralErrorLabel.text = kInvalidCode
        self.referralErrorLabel.isHidden = true
        referralTitleLabel.isHidden = true

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func getTheAttributeWIthGivenColor(color:UIColor)->[NSAttributedString.Key : Any]{
        let attributes = [
            NSAttributedString.Key.foregroundColor: color,
            NSAttributedString.Key.font : UIFont(name: Constants.App.BBQAppFont.NutinoSansRegular, size: 15)! // Note the !
        ]
    return attributes
    }

}
extension ReferralCodeCell:UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {    //delegate method
        self.referralTitleLabel.isHidden = false
        let attributes = self.getTheAttributeWIthGivenColor(color: Constants.App.themeColor.grey)
        if textField.text == ""{
            self.referralTextField.attributedPlaceholder = NSAttributedString(string: "", attributes: attributes)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
       if textField.text == ""{
            if currentTheme == .white {
                let attributes = self.getTheAttributeWIthGivenColor(color: Constants.App.themeColor.white)

                self.referralTextField.attributedPlaceholder = NSAttributedString(string: kEnterInviteCode, attributes: attributes)
            } else {
                let attributes = self.getTheAttributeWIthGivenColor(color: Constants.App.themeColor.grey)
                self.referralTextField.attributedPlaceholder = NSAttributedString(string: kEnterInviteCode, attributes: attributes)
            }
            self.referralTitleLabel.isHidden = true

        }else{
            self.referralTitleLabel.isHidden = false

        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.referralErrorLabel.isHidden = true

        let currentString: NSString = textField.text! as NSString
        let newString: String =
            (currentString.replacingCharacters(in: range, with: string) as NSString) as String
        
        if newString.contains("  ") {
            return false
        } else if currentString.length == 0 && newString == " "{
            return false
        }
        
        delegate?.ReferralCodeEntered(length: newString.count, text: newString)
        
        return (newString.count>Constants.App.Validations.nameMaxLength ? false:true)
    }
   
    func textFieldShouldReturn(_ scoreText: UITextField) -> Bool {
        delegate?.didReferralReturnKeyPressed()
        return true
    }
}
