//
//  LoginHeaderCell.swift
//  Barbeque Nation
//
//  Created by Rabindra L on 26/07/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

class LoginHeaderCell: UITableViewCell {

    @IBOutlet weak var bbqLogo: UIImageView!
    @IBOutlet weak var greetingLabel: UILabel!
    var imageName = Constants.AssetName.corporateLogoBig {
        didSet {
            bbqLogo.image = UIImage(named: imageName)
        }
    }
    var currentTheme = theme.white {
        didSet{
            if currentTheme == theme.white {
                bbqLogo.setImageColor(color: Constants.App.themeColor.white)
                greetingLabel.textColor = Constants.App.themeColor.white
            } else if currentTheme == theme.dark {
                bbqLogo.setImageColor(color: Constants.App.themeColor.grey)
                greetingLabel.textColor = Constants.App.themeColor.grey
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = UIColor.red.withAlphaComponent(0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension UIImageView {
    func setImageColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
}
