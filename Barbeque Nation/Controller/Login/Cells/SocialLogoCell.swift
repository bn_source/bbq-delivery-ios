//
//  SocialLogoCell.swift
//  Barbeque Nation
//
//  Created by Rabindra L on 29/07/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

class SocialLogoCell: UITableViewCell {

    @IBOutlet weak var googlePlusButton: UIButton!
    @IBOutlet weak var orLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var instagramButton: UIButton!
    @IBOutlet weak var googleButton: UIButton!
    var currentTheme = theme.white {
        didSet{
            if currentTheme == .white {
                orLabel.textColor = Constants.App.themeColor.white
                let yourAttributes: [NSAttributedString.Key: Any] = [
                    .foregroundColor: Constants.App.themeColor.white,
                    .underlineStyle: NSUnderlineStyle.single.rawValue]
                let attributeString = NSMutableAttributedString(string: "SKIP",
                                                                attributes: yourAttributes)
                skipButton.setAttributedTitle(attributeString, for: .normal)
                
                facebookButton.setBackgroundImage(UIImage(named: "facebook"), for: .normal)
                instagramButton.setBackgroundImage(UIImage(named: "instagram"), for: .normal)
                nextButton.setBackgroundImage(UIImage(named: "icon_Next"), for: .normal)
                googleButton.setBackgroundImage(UIImage(named: "google"), for: .normal)
            } else {

                orLabel.textColor = Constants.App.themeColor.grey
                
                let yourAttributes: [NSAttributedString.Key: Any] = [
                    .foregroundColor: Constants.App.themeColor.grey,
                    .underlineStyle: NSUnderlineStyle.single.rawValue]
                let attributeString = NSMutableAttributedString(string: "SKIP",
                                                                attributes: yourAttributes)
                skipButton.setAttributedTitle(attributeString, for: .normal)
                
                facebookButton.setBackgroundImage(UIImage(named: "facebook_dark"), for: .normal)
                instagramButton.setBackgroundImage(UIImage(named: "insta_dark"), for: .normal)
                nextButton.setBackgroundImage(UIImage(named: "icon_next_dark"), for: .normal)
                googleButton.setBackgroundImage(UIImage(named: "google_dark"), for: .normal)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let yourAttributes: [NSAttributedString.Key: Any] = [
            .foregroundColor: UIColor.white,
            .underlineStyle: NSUnderlineStyle.single.rawValue]
        let attributeString = NSMutableAttributedString(string: "SKIP",
                                                        attributes: yourAttributes)
        skipButton.setAttributedTitle(attributeString, for: .normal)
        
        self.backgroundColor = UIColor.red.withAlphaComponent(0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
