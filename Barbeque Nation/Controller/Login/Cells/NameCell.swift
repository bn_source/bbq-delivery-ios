//
//  NameCell.swift
//  Barbeque Nation
//
//  Created by Rabindra L on 29/07/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

protocol NameCellDelegate {
    func nameEntered(length: Int, text: String)
    //Called when keyboard return key pressed
    func didNameReturnKeyPressed()
}

class NameCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var yourNameLabel: UILabel!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var titleButton: UIButton!
    @IBOutlet weak var nameTopConst: NSLayoutConstraint!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var titleTopConst: NSLayoutConstraint!
    @IBOutlet weak var nameErrorMessage: UILabel!
    @IBOutlet weak var titleErrorMessage: UILabel!
    var currentTheme = theme.white {
        didSet {
            if currentTheme == .white {
                titleButton.setTitleColor(Constants.App.themeColor.white, for: .normal)
                textField.textColor = Constants.App.themeColor.white
                nameErrorMessage.textColor = .red
                titleErrorMessage.textColor = Constants.App.themeColor.white
                
                titleLabel.textColor = Constants.App.themeColor.white
                yourNameLabel.textColor = Constants.App.themeColor.white
                view1.backgroundColor = Constants.App.themeColor.white
                view2.backgroundColor = Constants.App.themeColor.white
                
            } else {
                titleButton.setTitleColor(Constants.App.themeColor.grey, for: .normal)
                textField.textColor = Constants.App.themeColor.grey
                nameErrorMessage.textColor = .red
                titleErrorMessage.textColor = Constants.App.themeColor.grey
                
                titleLabel.textColor = Constants.App.themeColor.grey
                yourNameLabel.textColor = Constants.App.themeColor.grey
                view1.backgroundColor = Constants.App.themeColor.grey
                view2.backgroundColor = Constants.App.themeColor.grey
            }
            titleLabel.text = kTitleLabelText
            yourNameLabel.text = kYourNameLabelText
        }
    }
    
    var delegate: NameCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = UIColor.red.withAlphaComponent(0)
        textField.delegate = self
        nameErrorMessage.isHidden = true
        titleErrorMessage.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {    //delegate method
        changeLabelPosition(position: 5.0)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text?.count == 0 {
            changeLabelPosition(position: 30.0)
        }
    }
    
    func changeLabelPosition(position: CGFloat) {
        nameTopConst.constant = position
        
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
            self.layoutIfNeeded()
        }, completion: { finished in
            
        })
    }
    
    let allowedCharacters = CharacterSet(charactersIn:"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvxyz ").inverted

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = textField.text! as NSString
        let newString: String =
            (currentString.replacingCharacters(in: range, with: string) as NSString) as String
        
        if newString.contains("  ") {
            return false
        } else if currentString.length == 0 && newString == " "{
            return false
        }
        
        let components = string.components(separatedBy: allowedCharacters)
            let filtered = components.joined(separator: "")
            
            if string != filtered {
                                
                return false
            }
        
        delegate?.nameEntered(length: newString.count, text: newString)
        
        return (newString.count>Constants.App.Validations.nameMaxLength ? false:true)
    }
    
    func textFieldShouldReturn(_ scoreText: UITextField) -> Bool {
        delegate?.didNameReturnKeyPressed()
        return true
    }
}
