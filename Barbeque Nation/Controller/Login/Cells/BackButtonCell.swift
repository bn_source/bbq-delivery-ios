//
//  BackButtonCell.swift
//  Barbeque Nation
//
//  Created by Rabindra L on 29/07/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

enum styleEnum: String {
    case send = "send"
    case resend = "resend"
}

class BackButtonCell: UITableViewCell {

    @IBOutlet weak var sendOTPButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    private var yourAttributes:[NSAttributedString.Key: Any] = [.foregroundColor: Constants.App.themeColor.white]
    var currentTheme = theme.white {
        didSet {
            if currentTheme == .white {
                sendOTPButton.setTitleColor(Constants.App.themeColor.white, for: .normal)
                backButton.setTitleColor(Constants.App.themeColor.white, for: .normal)
                
                yourAttributes = [
                .foregroundColor: Constants.App.themeColor.white,
                .underlineStyle: NSUnderlineStyle.single.rawValue]
                
                backButton.setBackgroundImage(UIImage(named: "icon_previous"), for: .normal)
            } else {
                sendOTPButton.setTitleColor(Constants.App.themeColor.grey, for: .normal)
                backButton.setTitleColor(Constants.App.themeColor.grey, for: .normal)
                yourAttributes = [
                    .foregroundColor: Constants.App.themeColor.grey,
                    .underlineStyle: NSUnderlineStyle.single.rawValue]
                backButton.setBackgroundImage(UIImage(named: "icon_previous_dark"), for: .normal)
            }
        }
    }
    
    var style:styleEnum! {
        didSet {
            if style == .send {
                let attributeString = NSMutableAttributedString(string: kSendOTPText,
                                                                attributes: yourAttributes)
                sendOTPButton.setAttributedTitle(attributeString, for: .normal)
            } else {
                let attributeString = NSMutableAttributedString(string: kResendOTPText,
                                                                attributes: yourAttributes)
                sendOTPButton.setAttributedTitle(attributeString, for: .normal)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = UIColor.red.withAlphaComponent(0)
        style = styleEnum.send
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
