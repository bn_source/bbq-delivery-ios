//
//  OTPCell.swift
//  Barbeque Nation
//
//  Created by Rabindra L on 30/07/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

protocol OTPCellProtocol: AnyObject {
    func didFinishEnteringOTP(otp: String)
}

class OTPCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var otpSentNotificationLabel: UILabel!
    @IBOutlet weak var view6: UIView!
    @IBOutlet weak var view5: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var enterOTPLabel: UILabel!
    @IBOutlet weak var textField1: UITextField!
    @IBOutlet weak var textField2: UITextField!
    @IBOutlet weak var textField3: UITextField!
    @IBOutlet weak var textField4: UITextField!
    @IBOutlet weak var textField5: UITextField!
    @IBOutlet weak var textField6: UITextField!
    
    @IBOutlet weak var containerTextField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    weak var delegate: OTPCellProtocol? = nil
    var otpConfirmationMessage = kOtpSentNotification {
        didSet{
            otpSentNotificationLabel.text = otpConfirmationMessage
        }
    }
    
    var currentTheme = theme.white {
        didSet {
            if currentTheme == .white {
                errorLabel.textColor = Constants.App.themeColor.white
                enterOTPLabel.textColor = Constants.App.themeColor.white
                textField1.textColor = Constants.App.themeColor.white
                textField2.textColor = Constants.App.themeColor.white
                textField3.textColor = Constants.App.themeColor.white
                textField4.textColor = Constants.App.themeColor.white
                textField5.textColor = Constants.App.themeColor.white
                textField6.textColor = Constants.App.themeColor.white
                view1.backgroundColor = Constants.App.themeColor.white
                view2.backgroundColor = Constants.App.themeColor.white
                view3.backgroundColor = Constants.App.themeColor.white
                view4.backgroundColor = Constants.App.themeColor.white
                view5.backgroundColor = Constants.App.themeColor.white
                view6.backgroundColor = Constants.App.themeColor.white
                containerTextField.tintColor = Constants.App.themeColor.white
            } else {
                otpSentNotificationLabel.textColor = Constants.App.themeColor.grey
                errorLabel.textColor = Constants.App.themeColor.grey
                enterOTPLabel.textColor = Constants.App.themeColor.grey
                textField1.textColor = Constants.App.themeColor.grey
                textField2.textColor = Constants.App.themeColor.grey
                textField3.textColor = Constants.App.themeColor.grey
                textField4.textColor = Constants.App.themeColor.grey
                textField5.textColor = Constants.App.themeColor.grey
                textField6.textColor = Constants.App.themeColor.grey
                view1.backgroundColor = Constants.App.themeColor.grey
                view2.backgroundColor = Constants.App.themeColor.grey
                view3.backgroundColor = Constants.App.themeColor.grey
                view4.backgroundColor = Constants.App.themeColor.grey
                view5.backgroundColor = Constants.App.themeColor.grey
                view6.backgroundColor = Constants.App.themeColor.grey
                containerTextField.tintColor = Constants.App.themeColor.grey
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = UIColor.red.withAlphaComponent(0)

        containerTextField.delegate = self
    }
    
    func reset() {
        errorLabel.stopBlink()
        errorLabel.startBlink()
        textField1.text = ""
        textField2.text = ""
        textField3.text = ""
        textField4.text = ""
        textField5.text = ""
        textField6.text = ""
        containerTextField.text = ""
        containerTextField.becomeFirstResponder()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        containerTextField.tintColor = UIColor.clear
        textField1.text = ""
        textField2.text = ""
        textField3.text = ""
        textField4.text = ""
        textField5.text = ""
        textField6.text = ""
        errorLabel?.text = ""
        // On inputing value to textfield
        var currentText: String = textField.text! as String
        
        var isBackPressed = false
        if let char = string.cString(using: String.Encoding.utf8) {
            let isBackSpace = strcmp(char, "\\b")
            if (isBackSpace == -92) {
                isBackPressed = true
            }
        }
        
        if isBackPressed {
            currentText = String(currentText.dropLast())
        } else {
            currentText += string
            if currentText.count > 6 {
                return false
            }
        }
        
        for index in 0..<currentText.count {
            let pos = currentText.index(currentText.startIndex, offsetBy: index)
            switch index {
            case 0:
                textField1.text = String(currentText[pos]) 
            case 1:
                textField2.text = String(currentText[pos])
            case 2:
                textField3.text = String(currentText[pos]) 
            case 3:
                textField4.text = String(currentText[pos]) 
            case 4:
                textField5.text = String(currentText[pos]) 
            case 5:
                textField6.text = String(currentText[pos])
                if let delegate = self.delegate {
                    let finalOTP = String(format: "%@%@%@%@%@%@", textField1.text!, textField2.text!, textField3.text!, textField4.text!, textField5.text!, textField6.text!)
                    delegate.didFinishEnteringOTP(otp: finalOTP)
                }
            default:
                break
            }
        }
        
        return (currentText.count <= 6) ? true : false
    }
}

extension UILabel {
    func startBlink() {
              UIView.animate(withDuration: 0.8,//Time duration
                            delay:0.0,
                            options:[.allowUserInteraction, .curveEaseInOut, .autoreverse, .repeat],
                            animations: { self.alpha = 0 },
                            completion: nil)
    }
    
    func stopBlink() {
        layer.removeAllAnimations()
        alpha = 1
    }
}
