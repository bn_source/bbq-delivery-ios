//
//  PhoneNumberCell.swift
//  Barbeque Nation
//
//  Created by Rabindra L on 26/07/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

protocol PhoneNumberCellDelegate {
    
    func phoneNumberEntered(length: Int, text: String)
    
    func textFieldStartsEditing(_ textField: UITextField)
    
    func textFieldStopsEditing(_ textField: UITextField)
}

class PhoneNumberCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var isdCodeLabel: UILabel!
    @IBOutlet weak var errorMessage: UILabel!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var labelTopConst: NSLayoutConstraint!
    var delegate: PhoneNumberCellDelegate?
    @IBOutlet weak var countryCodeButton: UIButton!
    var validPhoneNumber:(length:Int, code:String) = (10,"+91")
    var currentTheme = theme.white {
        didSet{
            if currentTheme == .white {
                errorMessage.textColor = .red
                errorLabel.textColor = Constants.App.themeColor.white
                textField.textColor = Constants.App.themeColor.white
                isdCodeLabel.textColor = Constants.App.themeColor.white
                countryCodeButton.setTitleColor(Constants.App.themeColor.white, for: .normal)
                view1.backgroundColor = Constants.App.themeColor.white
                view2.backgroundColor = Constants.App.themeColor.white
            } else {
                errorMessage.textColor = .red
                errorLabel.textColor = Constants.App.themeColor.grey
                textField.textColor = Constants.App.themeColor.grey
                isdCodeLabel.textColor = Constants.App.themeColor.grey
                countryCodeButton.setTitleColor(Constants.App.themeColor.grey, for: .normal)
                view1.backgroundColor = Constants.App.themeColor.grey
                view2.backgroundColor = Constants.App.themeColor.grey
            }
            errorLabel.text = kValidPhoneNumberLabel
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = UIColor.red.withAlphaComponent(0)
        labelTopConst.constant = 4
        textField.delegate = self
        errorMessage.isHidden = true
        //errorMessage.startBlink()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK:- Public Methods
    func updatePhoneNumberCell(with countryCode: String) {
        self.countryCodeButton.setTitle(countryCode, for: .normal)
    }
    
    @IBAction func numberChanged(_ sender: Any) {
//        var string = textField.text ?? ""
//        string = string.replacingOccurrences(of: " ", with: "")
//        if string.count >= 10{
//            textField.text = String(string.suffix(10))
//            delegate?.phoneNumberEntered(length: 10, text: String(string.suffix(10)))
//        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {    //delegate method
        changeLabelPosition(position: -19.0)
        self.delegate?.textFieldStartsEditing(textField)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text?.count == 0 {
            changeLabelPosition(position: 4.0)
        }
        self.delegate?.textFieldStopsEditing(textField)
    }
    
    func changeLabelPosition(position: CGFloat) {
        labelTopConst.constant = position
        
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
            self.layoutIfNeeded()
        }, completion: { finished in
            
        })
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = textField.text! as NSString
        if Int(string) != nil || string == "" {
            let newString1 = string.replacingOccurrences(of: " ", with: "")
            if newString1.count >= 10{
                textField.text = String(newString1.suffix(10))
                delegate?.phoneNumberEntered(length: 10, text: String(newString1.suffix(10)))
                return false
            }
            
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString

            if isValidNumber(number: String(newString)) {
                //errorLabel.text = kValidPhoneNumberLabel
                validPhoneNumber.code = BBQUserDefaults.sharedInstance.phoneCode
                validPhoneNumber.length = newString.length
            } else {
                if newString.length > validPhoneNumber.length && validPhoneNumber.code == BBQUserDefaults.sharedInstance.phoneCode {
                    return false
                } else {
                    delegate?.phoneNumberEntered(length: newString.length, text: String(newString))
                    return true
                }
            }
            delegate?.phoneNumberEntered(length: newString.length, text: String(newString))
            return true
        } else { // allow only digita
            let newString = string.replacingOccurrences(of: " ", with: "")
            if newString.count >= 10{
                textField.text = String(newString.suffix(10))
                delegate?.phoneNumberEntered(length: 10, text: String(newString.suffix(10)))
            }else if string == " "{
                return true
            }
            return false
        }
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if let text = textField.text {
            if !isValidNumber(number: text) {
                
            }
        }
        return true
    }
    
    func isValidNumber(number: String) -> Bool {
        return BBQPhoneNumberValidation.isValidNumber(number: number, for: BBQUserDefaults.sharedInstance.phoneCode)
    }
}
