/*
 //  LoginBackground.swift
 
*  Created by Rabindra L on 23/07/19.
*  Copyright © BARBEQUE NATION HOSPITALITY LIMITED 2019. All rights reserved.
*  Last modified 03/09/19
 
 * CHANGE LOG
 * —------------------------------------------------------------------------------
 * ID          Bug ID      Author Name          Date                  Description
 * —------------------------------------------------------------------------------
 * 1            NA         Rabindra L           23/07/19        Initial Version
 * 2            1          Rabindra L           03/09/19        Review comments
 */

import UIKit

class LoginBackground: UIViewController {
    
    enum constantValues:Double {
        case animationDuration = 0.5,
        withDelay = 0.1,
        confirmationScreenDisplayWaitTime = 2.0,
        initialValue = 0.0,
        increamentOne = 1.0
    }
    
    @IBOutlet weak var bgTrailingConst: NSLayoutConstraint!
    @IBOutlet weak var bgTopConst: NSLayoutConstraint!
    @IBOutlet weak var bgLeadingConst: NSLayoutConstraint!
    @IBOutlet weak var bgBottomConst: NSLayoutConstraint!
    @IBOutlet var layoutView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        layoutView.backgroundColor = .clear
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if BBQUserDefaults.sharedInstance.accessToken == ""
        {
//            if BBQUserDefaults.sharedInstance.signupCount >= Constants.App.Validations.signupCount {
//                navigateToHomePage(navigationFrom: .Splash)
//            } else {
                var count = BBQUserDefaults.sharedInstance.signupCount
                count += Int(constantValues.increamentOne.rawValue)
                BBQUserDefaults.sharedInstance.signupCount = count
                BBQUserDefaults.sharedInstance.defaults.synchronize()
                bgTrailingConst.constant = CGFloat(constantValues.initialValue.rawValue)
                bgTopConst.constant = CGFloat(constantValues.initialValue.rawValue)
                bgLeadingConst.constant = CGFloat(constantValues.initialValue.rawValue)
                bgBottomConst.constant = CGFloat(constantValues.initialValue.rawValue)
                
                let viewController = UIStoryboard.loadLoginThroughPhoneController()
                viewController.delegate = self
             //   viewController.navigationFromScreen = .Splash
                viewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                self.present(viewController, animated: true, completion: nil)
                
                UIView.animate(withDuration: constantValues.animationDuration.rawValue,
                               delay: constantValues.withDelay.rawValue,
                               options: .curveEaseIn,
                               animations: {
                    self.view.layoutIfNeeded()
                }, completion: { finished in
                    
                })
//            }
        } else {
            navigateToHomePage(navigationFrom: .Splash)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.dismiss(animated: false) {
        }
    }
}

extension LoginBackground: LoginControllerDelegate {
    func gotoHomeScreen(isExistingUser: Bool, navigationFrom: LoginNavigationScreen) {
        if isExistingUser || BBQUserDefaults.sharedInstance.isGuestUser {
            navigateToHomePage(navigationFrom: navigationFrom)
        } else  {
            showRegistrationConfirmationPage(navigationFrom: navigationFrom)
        }
    }
    
    func navigateToHomePage(navigationFrom: LoginNavigationScreen) {
        //Create Login Controller instance only if it is coming from Splash screen
        if navigationFrom == .Splash {
            /*let homeViewController = UIStoryboard.loadHomeViewController()
            homeViewController.navigationFromScreen = navigationFrom
            self.navigationController?.pushViewController(homeViewController, animated: true)*/
            goToHomePage()
        }
    

    }
    
    func showRegistrationConfirmationPage(navigationFrom: LoginNavigationScreen) {
        let registrationConfirmationVC = UIStoryboard.loadRegistrationConfirmation()
        registrationConfirmationVC.modalPresentationStyle = .overFullScreen
        registrationConfirmationVC.titleText = kSignUpConfirmationTitleText
        registrationConfirmationVC.navigationText = kSignUpConfirmationNavigationText
        
        self.present(registrationConfirmationVC, animated: false) {
            sleep(UInt32(constantValues.confirmationScreenDisplayWaitTime.rawValue))
            
            self.dismiss(animated: false) {
                self.navigateToHomePage(navigationFrom: navigationFrom)
            }
        }
    }
    
}
