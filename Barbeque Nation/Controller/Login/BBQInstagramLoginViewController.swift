//
//  BBQInstagramLoginViewController.swift
//  Barbeque Nation
//
//  Created by Ajith CP on 12/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit
import WebKit
import FirebaseAnalytics

protocol BBQInstagramLoginControllerProtocol {
    // Delegate callback for recieving instagram user name. Use this from where username manipulates
    func didRecieveInstagram(userName: String, authToken: String)
}

class BBQInstagramLoginViewController: UIViewController, WKNavigationDelegate {
   
    // Webview for loading instagram signup/login page. It's reccommended by instagram API documents
    // See https://www.instagram.com/developer/authentication/ for details.
    @IBOutlet weak var instagramWebView: WKWebView!
    
    var instaLoginVCDelegate : BBQInstagramLoginControllerProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AnalyticsHelper.shared.screenName(name: "SocialLoginScreen", className: "BBQInstagramLoginViewController")
        Analytics.logEvent("SocialLogging", parameters: [
            "name": "SocialLogin" as NSObject,
            "full_text": "SocialLogin page" as NSObject
            ])
        
        self.instagramWebView.navigationDelegate = self
        self.loadInstagramSignup()
        
    }
    
    // MARK:- Lazy Inits
    
    lazy private var instaLoginViewModel : BBQInstgramLoginControllerViewModel = {
        let instaLoginVM = BBQInstgramLoginControllerViewModel()
        return instaLoginVM
    } ()
    
    // MARK:- Setup Methods
    
    func loadInstagramSignup() {
        
        BBQActivityIndicator.showIndicator(view: self.instagramWebView)
        
        let urlRequest = URLRequest.init(url: URL(string: self.instaLoginViewModel.instagramAuthenticationURL)!)
        self.instagramWebView.load(urlRequest)
    }
    
    // MARK:- IBActions
    
    @IBAction func closeInstgramLoginPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK:- WKWebView Delegate Methods
    
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation
        navigation: WKNavigation!) {
       
        BBQActivityIndicator.hideIndicator(from: self.instagramWebView)
        
        let isSuccess =
            self.instaLoginViewModel.isRecievedRequestForCallbackURLWithToken(request: URLRequest(url: webView.url!)).isSuccess
        let accessToken = self.instaLoginViewModel.isRecievedRequestForCallbackURLWithToken(request: URLRequest(url: webView.url!)).authToken
        // If Instagram successfully signup/signin passes user name to manipulate login page with
        // full name.
        if isSuccess {
            
            self.instaLoginViewModel.getInstagramUserInfo(with: accessToken) { (fullName) in
                self.instaLoginVCDelegate?.didRecieveInstagram(userName: fullName, authToken: accessToken)
                self.dismiss(animated: true, completion: nil)
            }
        }
        
        
    }
    

}
