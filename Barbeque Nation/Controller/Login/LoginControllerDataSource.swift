//
//  LoginControllerDataSource.swift
//  Barbeque Nation
//
//  Created by Rabindra L on 07/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

class LoginControllerDataSource: NSObject, UITableViewDelegate, UITableViewDataSource {

    var loginController:LoginController!
    weak var tableView: UITableView!
    
    func registerTableViewCells() {
        let loginHeaderCell = UINib(nibName:Constants.CellIdentifiers.loginHeaderCell, bundle:nil)
        self.tableView.register(loginHeaderCell, forCellReuseIdentifier: Constants.CellIdentifiers.loginHeaderCell)
        
        let phoneNumberCell = UINib(nibName:Constants.CellIdentifiers.phoneNumberCell, bundle:nil)
        self.tableView.register(phoneNumberCell, forCellReuseIdentifier: Constants.CellIdentifiers.phoneNumberCell)
        
        let socialCell = UINib(nibName:Constants.CellIdentifiers.socialLogoCell, bundle:nil)
        self.tableView.register(socialCell, forCellReuseIdentifier: Constants.CellIdentifiers.socialLogoCell)
        
        let nameCell = UINib(nibName:Constants.CellIdentifiers.nameCell, bundle:nil)
        self.tableView.register(nameCell, forCellReuseIdentifier: Constants.CellIdentifiers.nameCell)
        
        let referrCell = UINib(nibName:Constants.CellIdentifiers.referralCell, bundle:nil)
        self.tableView.register(referrCell, forCellReuseIdentifier: Constants.CellIdentifiers.referralCell)

        
        let backButtonCell = UINib(nibName:Constants.CellIdentifiers.backButtonCell, bundle:nil)
        self.tableView.register(backButtonCell, forCellReuseIdentifier: Constants.CellIdentifiers.backButtonCell)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height:CGFloat = 0.0
        
        switch loginController.currentScreen {
        case .onlyMobileNumber_screen:
            switch indexPath.row {
            case 0:
                height = Constants.App.LoginCellHeight.headerCellHeight
            case 1:
                height = Constants.App.LoginCellHeight.hidingCellHeight
            case 2:
                height = Constants.App.LoginCellHeight.phoneCellHeight
            case 3:
                height = Constants.App.LoginCellHeight.socialCellHeight
            default:
                break
            }
        case .mobile_NameField_screen:
            switch indexPath.row {
            case 0:
                height = Constants.App.LoginCellHeight.headerCellHeight
            case 1:
                height = Constants.App.LoginCellHeight.signUpCellHeight
            case 2:
                height = Constants.App.LoginCellHeight.phoneCellHeight
            case 3:
                height = Constants.App.LoginCellHeight.nameCellHeight
            case 4:
                height = Constants.App.LoginCellHeight.referralCellHeight
            case 5:
                height = Constants.App.LoginCellHeight.backButtonCellHeight
                
            default: print("add code for email field")
                
            }
        default: print("add code for email field")
            break
        }
        
        return height
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var number:Int
        
        switch loginController.currentScreen {
        case .onlyMobileNumber_screen: number = 4
        case .mobile_NameField_screen: number = 6
        default : number = 0
            print("add code for email field")
        }
        return number
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell1 = UITableViewCell()
        switch loginController.currentScreen {
        case .onlyMobileNumber_screen:
            switch indexPath.row {
            case 0:
                if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.loginHeaderCell) as? LoginHeaderCell {
                    cell.currentTheme = loginController.currentTheme
                    if loginController.signUpFailed {
                        cell.greetingLabel.text = kSignUpFailedMessage
                    } else {
                        cell.greetingLabel.text = kSignUpMessage
                    }
                    cell1 = cell
//                    return cell
                }
            case 1:
                if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.signUpCell) {
                    cell.backgroundColor = UIColor.red.withAlphaComponent(0)
                    
                    let label = cell.contentView.viewWithTag(10) as! UILabel
                    if loginController.currentTheme == .white {
                        label.textColor = Constants.App.themeColor.white
                    } else {
                        label.textColor = Constants.App.themeColor.grey
                    }
                    cell1 = cell
                }
            case 2:
                if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.phoneNumberCell) as? PhoneNumberCell {
                    cell.currentTheme = loginController.currentTheme
                    loginController.phoneNumberText = cell.textField.text ?? ""
                    cell.delegate = loginController
                    cell.countryCodeButton.addTarget(loginController, action:#selector(loginController.countryCodeTapped), for: .touchUpInside)
                    cell.textField.text = ""
                    cell1 = cell
                }
            case 3:
                if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.socialLogoCell) as? SocialLogoCell {
                    cell.currentTheme = loginController.currentTheme
                    cell.nextButton.addTarget(loginController, action:#selector(loginController.nextButtonTapped), for: .touchUpInside)
                    cell.skipButton.addTarget(loginController, action:#selector(loginController.skipButtonTapped), for: .touchUpInside)
                    cell.skipButton.isHidden = loginController.navigationFromScreen != .Splash
                    loginController.nextButton = cell.nextButton
                    cell1 = cell
                }
            default:
                break
            }
            
        case .mobile_NameField_screen:
            switch indexPath.row {
            case 0:
                if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.loginHeaderCell) as? LoginHeaderCell {
                    cell.currentTheme = loginController.currentTheme
                    cell.greetingLabel.text = kSignUpMessage
                    cell1 = cell
                }
            case 1:
                if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.signUpCell) {
                    cell.backgroundColor = UIColor.red.withAlphaComponent(0)
                    let label = cell.contentView.viewWithTag(10) as! UILabel
                    if loginController.currentTheme == .white {
                        label.textColor = Constants.App.themeColor.white
                    } else {
                        label.textColor = Constants.App.themeColor.grey
                    }
                    cell1 = cell
                }
            case 2:
                if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.phoneNumberCell) as? PhoneNumberCell {
                    cell.currentTheme = loginController.currentTheme
                    cell.delegate = loginController
                    cell1 = cell
                }
            case 3:
                if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.nameCell) as? NameCell {
                    cell.currentTheme = loginController.currentTheme
                    loginController.nameTextField = cell.textField
                    cell.delegate = loginController
                    cell.titleButton.addTarget(loginController, action:#selector(loginController.titleTapped), for: .touchUpInside)
                    cell.titleButton.setTitle(loginController.userTitle,for: .normal)
                    cell1 = cell
                }
            case 4:
                if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.referralCell) as? ReferralCodeCell {
                    cell.currentTheme = loginController.currentTheme
                    loginController.referCodeTextField = cell.referralTextField
//                    cell.delegate = (loginController as! ReferralCellDelegate)
                    cell1 = UITableViewCell()
                }
            case 5:
                if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.backButtonCell) as? BackButtonCell {
                    cell.currentTheme = loginController.currentTheme
                    cell.backButton.addTarget(loginController, action:#selector(loginController.backButtonTapped), for: .touchUpInside)
                    cell.sendOTPButton.addTarget(loginController, action:#selector(loginController.sendOTPTapped), for: .touchUpInside)
                    cell.style = .send
                    loginController.backButtonCell = cell
                    
                    cell1 = cell
                }
            default:print("add code for email field")
                break
            }
        default:print("add code for email field")

        }
        
//        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        return cell1
        
    }

}

extension UITableView {
    func reloadData(completion: @escaping ()->()) {
        UIView.animate(withDuration: 0, animations: { self.reloadData() })
        { _ in completion() }
    }
}
