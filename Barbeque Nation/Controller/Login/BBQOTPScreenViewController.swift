//
//  Created by Rabindra L on 09/09/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQOTPScreenViewController.swift
//
//  —-----------------------------------------------------------------------------------------
//  ID          Bug ID      Author Name          Date                  Description
//  —-----------------------------------------------------------------------------------------
//  1            NA         Ajith CP             17/09/19        Customization for reusability

import UIKit
import FirebaseAnalytics

@objc protocol BBQOTPScreenDelegate: AnyObject {
    func otpValidation(status: Bool, otp:String)
    func backButtonPressed()
   @objc optional func dismiss()
}

enum BBQOTPValidationType : Int {
    
    case mobileNumber = 0
    case profileEmailID
    case corporateEmailID
}

enum OtpType: Int {
    case normalLogin = 0, corporate, others
}

class BBQOTPScreenViewController: UIViewController {
    
    enum constant:CGFloat {
        case cornerRadius = 10.0
        case delayDuration = 0.3
    }
    
    @IBOutlet weak var subViewHeightConst: NSLayoutConstraint!
    @IBOutlet weak var topBlackView: UIView!
    @IBOutlet weak var tableView: UITableView!
    var currentTheme = theme.white
    var isExistingUser = false
    weak var delegate: BBQOTPScreenDelegate? = nil
    private var verifyOTPCounter = 0
    var backButtonCell:BackButtonCell!
    var resendOtp:UIButton!
    var resendOTPCounter = 0
    var phoneCode = ""
    var header = kCorporateOffers
    var logoImage = Constants.AssetName.corporateLogoBig
    var signUp = ""
    var otpMessage = kOtpSentNotification
    var otpType = OtpType.normalLogin
    var validationType : BBQOTPValidationType = BBQOTPValidationType.mobileNumber
    var validationPath = "" // Set email id or mobile number here.
    var shouldCloseTouchingOutside = false
    
    @IBOutlet weak var subView: UIView!
    var keyBoardHeight = 0
    private var keyboardVisible = false
    var screenHeight = 0
    var otpID = ""
    
    // TODO:- Create separate view model for OTP component.
    lazy var loginVerifyViewModel : LoginVerifyViewModel = {
        let viewModel = LoginVerifyViewModel()
        return viewModel
    }()
    
    lazy var bookingViewModel : CorporateBookingViewModel = {
        let viewModel = CorporateBookingViewModel()
        return viewModel
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AnalyticsHelper.shared.screenName(name: "OTPScreen", className: "BBQOTPScreenViewController")
//        Analytics.logEvent("Generating OTP", parameters: [
//            "name": "OTP" as NSObject,
//            "full_text": "OTP page" as NSObject
//            ])
        AnalyticsHelper.shared.triggerEvent(type: .B01)
        
        registerTableViewCells()
        
        let tap = UITapGestureRecognizer(target: self, action: nil)
        self.view.addGestureRecognizer(tap)
        
        self.subView.maskByRoundingCorners(cornerRadius: constant.cornerRadius.rawValue, maskedCorners: CornerMask.topCornersMask)
        topBlackView.roundCorners(cornerRadius: 3, borderColor: UIColor(red:54.00, green:64.0, blue:68.0, alpha:1.0), borderWidth: 0.1)
        
        
        
        if  currentTheme == .white {
            self.blurView()
        } else {
            subView.backgroundColor = Constants.App.themeColor.white
        }
        
        tableView.backgroundColor = UIColor.red.withAlphaComponent(0)
        subView.addSubview(tableView)
        setViewHeight()
        
        self.tableView.reloadData {
            let cell = self.tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as? OTPCell
            cell?.containerTextField.becomeFirstResponder()
        }
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        
        let keyboardTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        self.view.addGestureRecognizer(keyboardTap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.screenName(name: .Otp_Screen)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
        keyboardVisible = false
        setViewHeight()
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            self.keyBoardHeight = Int(keyboardRectangle.height)
        }
        
        keyboardVisible = true
        setViewHeight()
    }
    
    func setViewHeight() {
        
        subViewHeightConst.constant = CGFloat(screenHeight + (keyboardVisible ? keyBoardHeight : 0))
        
        UIView.animate(withDuration: TimeInterval(constant.delayDuration.rawValue), delay: 0, options: .curveEaseIn, animations: {
            self.view.layoutIfNeeded()
        }, completion: { finished in
            
        })
    }
    
    private func blurView() {
        subView.backgroundColor = UIColor.white.withAlphaComponent(0)
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = subView.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        subView.addSubview(blurEffectView)
    }
    
    func registerTableViewCells() {
        let otpCell = UINib(nibName:Constants.CellIdentifiers.otpCell, bundle:nil)
        self.tableView.register(otpCell, forCellReuseIdentifier: Constants.CellIdentifiers.otpCell)
        
        let loginHeaderCell = UINib(nibName:Constants.CellIdentifiers.loginHeaderCell, bundle:nil)
        self.tableView.register(loginHeaderCell, forCellReuseIdentifier: Constants.CellIdentifiers.loginHeaderCell)
        
        let backButtonCell = UINib(nibName:Constants.CellIdentifiers.backButtonCell, bundle:nil)
        self.tableView.register(backButtonCell, forCellReuseIdentifier: Constants.CellIdentifiers.backButtonCell)
        
        let mobileVerifiedCell = UINib(nibName:Constants.NibName.verifierHeaderCell, bundle:nil)
        self.tableView.register(mobileVerifiedCell, forCellReuseIdentifier: Constants.CellIdentifiers.verifierHeaderCellID)
    }
    
    @objc func backButtonTapped(sender: UIButton){
        if let delegate = self.delegate {
            self.verifyOTPCounter = 0
            self.resendOTPCounter = 0
            let cell = self.tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as? OTPCell
            cell?.reset()
            cell?.containerTextField.resignFirstResponder()
            self.dismiss(animated: true, completion: nil)
            delegate.backButtonPressed()
            AnalyticsHelper.shared.triggerEvent(type: .B04)
        }
    }
    
    @objc func sendOTPTapped(sender: UIButton) {
        resendOTPCounter += 1
        tableView.reloadData()
        if(resendOTPCounter <= Constants.App.Validations.maxResendOTP) {
            if resendOTPCounter == Constants.App.Validations.maxResendOTP {
                PopupHandler.showSingleButtonsPopup(title: kMaxAtmpReachTitle,
                                                    message: kMaxAtmpReachDesc,
                                                    image: UIImage(named: "icon_cross_black"),
                                                    on: self,
                                                    firstButtonTitle: kMaxOtpTryOkayButton,
                                                    firstAction: {
                                                        let cell = self.tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as? OTPCell
                                                        cell?.containerTextField.becomeFirstResponder()
                                                    })
                view.endEditing(true)
            }
            generateOTPWithOtpId(otpId: self.otpID)
        } else {
            self.showMaximumOTPTryAlert()
        }
    }
    
    private func generateOTPWithOtpId(otpId: String) {
        BBQActivityIndicator.showIndicator(view: self.view)
        // Switching resend OTP API calls in between mobile and email.
        switch self.validationType {
        case .corporateEmailID, .profileEmailID:
            self.resendEmailOTP(with: otpId)
        default:
            self.resendMobileOTP(with: otpId)
            
        }
    }
    
    // MARK:- Private Methods
    private func resendMobileOTP(with otpID: String) {
        AnalyticsHelper.shared.triggerEvent(type: .B03)
        loginVerifyViewModel.generateOTP(countryCode: self.phoneCode,
                                         mobileNumber: Int64(self.validationPath)!,
                                         otpId:otpID) { (isSuccess) in
            DispatchQueue.main.async {
                if isSuccess{
                    AnalyticsHelper.shared.triggerEvent(type: .B03A)
                }else{
                    AnalyticsHelper.shared.triggerEvent(type: .B03B)
                }
                BBQActivityIndicator.hideIndicator(from: self.view)
                self.tableView.reloadData {
                    if(self.resendOTPCounter < Constants.App.Validations.maxResendOTP) {
                        let cell = self.tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as? OTPCell
                        cell?.containerTextField.becomeFirstResponder()
                    }
                }
            }
        }
    }
    
    private func resendEmailOTP(with otpID: String) {
        AnalyticsHelper.shared.triggerEvent(type: .B03)
        self.bookingViewModel.verifyCoporateEmail(corporateEmailId: self.validationPath,
                                                  otpID: otpID) { (isOTPResent) in
            BBQActivityIndicator.hideIndicator(from: self.view)
            if isOTPResent{
                AnalyticsHelper.shared.triggerEvent(type: .B03A)
            }else{
                AnalyticsHelper.shared.triggerEvent(type: .B03B)
            }
            self.tableView.reloadData {
                if(self.resendOTPCounter < Constants.App.Validations.maxResendOTP) {
                    let cell = self.tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as? OTPCell
                    cell!.errorLabel.isHidden = true
                    cell!.otpSentNotificationLabel.isHidden = false
                    cell?.containerTextField.becomeFirstResponder()
                }
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if touch?.view == self.view {
            if shouldCloseTouchingOutside {
                self.dismiss(animated: true, completion: nil)
                if let delegate = self.delegate {
                    delegate.dismiss?()
                }
                view.endEditing(true)
            }
        }
    }
}

extension BBQOTPScreenViewController:UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
                   if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.loginHeaderCell) as? LoginHeaderCell {
                    cell.currentTheme = currentTheme
                    cell.imageName = logoImage
                    if otpType == .normalLogin {
//                        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.loginHeaderCell) as? LoginHeaderCell {
                            cell.currentTheme = currentTheme
                            cell.imageName = logoImage
                            if currentTheme == .white {
                                cell.bbqLogo.setImageColor(color: Constants.App.themeColor.white)
                            } else {
                                cell.bbqLogo.setImageColor(color: Constants.App.themeColor.grey)
                            }
                            
                            if isExistingUser {
                                cell.greetingLabel.text = kSignInMessage
                            } else {
                                cell.greetingLabel.text = kSignUpMessage
                            }
//                            return cell
//                        }
                    }else{
                        cell.greetingLabel.text = header
                        cell.imageName = logoImage


                    }
                    return cell


                   }
// else {
//                cell.greetingLabel.text = header
//
////               if let headerCell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.verifierHeaderCellID)
////                as? BBQMobileVerifierHeaderTableViewCell {
////
////                    headerCell.topDrawerView.isHidden = true
////
////                headerCell
////                return headerCell
//                }
//            }
        case 1:
            if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.signUpCell) {
                cell.backgroundColor = UIColor.red.withAlphaComponent(0)
                let label = cell.contentView.viewWithTag(10) as! UILabel
                if isExistingUser {
                    label.text = kSignInLabel
                } else {
                    label.text = kSignUpLabel
                }
                
                if currentTheme == .white {
                    label.textColor = Constants.App.themeColor.white
                } else {
                    label.textColor = Constants.App.themeColor.grey
                }
                
                return cell
            }
        case 2:
            if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.otpCell) as? OTPCell {
                cell.delegate = self
                cell.currentTheme = currentTheme
                cell.otpConfirmationMessage = otpMessage
                cell.reset()
                return cell
            }
        case 3:
            if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.backButtonCell) as? BackButtonCell {
                cell.currentTheme = currentTheme
                cell.backButton.addTarget(self, action:#selector(backButtonTapped), for: .touchUpInside)
                cell.sendOTPButton.addTarget(self, action:#selector(sendOTPTapped), for: .touchUpInside)
                cell.style = .resend
                resendOtp = cell.sendOTPButton
                if resendOTPCounter < Constants.App.Validations.maxOTPTrial {
                    cell.sendOTPButton.alpha = opacity.solid.rawValue
                } else {
                    cell.sendOTPButton.alpha = opacity.week.rawValue
                }
                backButtonCell = cell
                
                return cell
            }
        default:
            break
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var height:CGFloat = 0.0
        
        switch indexPath.row {
        case 0:
            height = Constants.App.LoginCellHeight.headerCellHeight
        case 1:
            if signUp.count == 0 {
                height = 0
            } else {
                height = Constants.App.LoginCellHeight.signUpCellHeight
            }
        case 2:
            height = Constants.App.LoginCellHeight.otpCellHeightCorporate
        case 3:
            height = Constants.App.LoginCellHeight.backButtonCellHeight
        default:
            break
        }
        
        return height
    }
}

extension BBQOTPScreenViewController: OTPCellProtocol {
    func didFinishEnteringOTP(otp: String) {
        verifyOTPCounter += 1
        AnalyticsHelper.shared.triggerEvent(type: .B02)
        if verifyOTPCounter <= Constants.App.Validations.maxOTPTrial {
            BBQActivityIndicator.showIndicator(view: self.view)
            loginVerifyViewModel.verifyOTP(otp: Int(otp) ?? 0, otpId: self.otpID) { (isSuccess) in
                BBQActivityIndicator.hideIndicator(from: self.view)
                
                self.view.endEditing(true)
                if isSuccess {
                    AnalyticsHelper.shared.triggerEvent(type: .B02A)
                    AnalyticsHelper.shared.triggerEvent(type: .Login_Home, properties: [AnalyticsHelper.MoEngageAttrKey.User_Creation : AnalyticsHelper.MoEngageAttrValue.Success.rawValue])
                    if let delegate = self.delegate {
                        self.verifyOTPCounter = 0
                        self.resendOTPCounter = 0
                        let cell = self.tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as? OTPCell
                        cell?.reset()
                        self.view.endEditing(true)
                        self.dismiss(animated: true, completion: nil)
                        delegate.otpValidation(status: true, otp: otp)
                    }
                } else {
                    AnalyticsHelper.shared.triggerEvent(type: .B02B)
                    if self.verifyOTPCounter >= Constants.App.Validations.maxOTPTrial {
                        if let delegate = self.delegate {
                            self.verifyOTPCounter = 0
                            self.resendOTPCounter = 0
                            let cell = self.tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as? OTPCell
                            cell!.errorLabel.isHidden = false
                            cell?.reset()
                            cell?.containerTextField.resignFirstResponder()
                            self.dismiss(animated: true, completion: nil)
                            delegate.otpValidation(status: false, otp: otp)
                        }
                        AnalyticsHelper.shared.triggerEvent(type: .Login_Home)
                    } else {
                        let cell = self.tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as? OTPCell
                        cell!.errorLabel.isHidden = false
                        cell?.reset()
                        cell?.enterOTPLabel.text = kReEnterOTPMessage
                        
                        cell!.otpSentNotificationLabel.isHidden = true
                        cell?.errorLabel.text = kOTPErrorMessage
                        cell?.errorLabel.textColor = .red
                        cell?.errorLabel.text = (cell?.errorLabel.text ?? "")
                        AnalyticsHelper.shared.triggerEvent(type: .Login_Home, properties: [AnalyticsHelper.MoEngageAttrKey.User_Creation : cell?.errorLabel.text ?? ""])
                    }
                    
                }
            } //verifyOTP
        }
    }
    
    // MARK: Private Method
    
    private func showMaximumOTPTryAlert() {
        PopupHandler.showSingleButtonsPopup(title: kMaxReqCrossedTitle,
                                            message: kMaxReqCrossedDesc,
                                            image: UIImage(named: "icon_cross_black"),
                                            on: self,
                                            firstButtonTitle: kMaxOtpTryOkayButton,
                                            firstAction: {
                                                let cell = self.tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as? OTPCell
                                                cell?.containerTextField.becomeFirstResponder()
        })
        view.endEditing(true)
    }
}
