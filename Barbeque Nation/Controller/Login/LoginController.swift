//
//  LoginController.swift
/*
 *  Created by Rabindra L on 25/07/19.
 *  Copyright © BARBEQUE NATION HOSPITALITY LIMITED 2019. All rights reserved.
 *  Last modified 03/09/19
 
 * CHANGE LOG
 * —------------------------------------------------------------------------------
 * ID          Bug ID      Author Name          Date                  Description
 * —------------------------------------------------------------------------------
 * 1            NA         Rabindra L           25/07/19        Initial Version
 * 2            1          Rabindra L           03/09/19        Review comments
 * 3            2          Rabindra L           06/09/19        Error messages

 */


import UIKit
import Foundation
import UserNotifications
import FirebaseAnalytics

enum screen: Int { 
    case onlyMobileNumber_screen = 1, mobile_NameField_screen , mobile_Email_screen
}

enum LoginNavigationScreen {
    case Splash, Voucher, Booking, Profile, Hamburger, DeliveryHome
}

enum signupProvider : String {
    case facebook, instagram, google, others
    
    func triggerPressedEvent() {
        if self == .facebook{
            AnalyticsHelper.shared.triggerEvent(type: .A03)
        }else if self == .google{
            AnalyticsHelper.shared.triggerEvent(type: .A04)
        }
    }
    
    func triggerSuccesEvent() {
        if self == .facebook{
            AnalyticsHelper.shared.triggerEvent(type: .A03A)
        }else if self == .google{
            AnalyticsHelper.shared.triggerEvent(type: .A04A)
        }
    }
    
    func triggerFailureEvent() {
        if self == .facebook{
            AnalyticsHelper.shared.triggerEvent(type: .A03B)
        }else if self == .google{
            AnalyticsHelper.shared.triggerEvent(type: .A04B)
        }
    }
}

protocol LoginControllerDelegate: AnyObject {
    func gotoHomeScreen(isExistingUser: Bool, navigationFrom: LoginNavigationScreen)
}

extension LoginControllerDelegate {
    func gotoHomeScreen(isExistingUser: Bool, navigationFrom: LoginNavigationScreen) {
        
    }
}

var namecellObject = NameCell()

enum opacity:CGFloat {
    case solid = 1.0
    case week = 0.6
}

enum theme:Int {
    case dark = 0, white
}

class LoginController: UIViewController {
    enum screenHeight: CGFloat {
        case screen_one_height = 300
        case screen_two_height = 420
        case screen_three_height = 401
        case screen_one_height_noKB = 302
        case screen_two_height_noKB = 525
    }
    
    enum constant:CGFloat {
        case cornerRadius = 15.0
        case titlePositionDown = 30.0
        case titlePositionUp = 5.0001
        case delayDuration = 0.3
    }
    
    @IBOutlet var subView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var subViewHeightConst: NSLayoutConstraint!
    
    weak var delegate: LoginControllerDelegate? = nil
    let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
    
    var nextButton:UIButton!
    var backButtonCell:BackButtonCell!
    var phoneNumberText = ""
    var nameTextField: UITextField!
    var referCodeTextField: UITextField!
    var referralCode: String? = nil
    var userName: String? = nil
    var keyboardHeight:CGFloat = 0
    var shouldCloseTouchingOutside = false
    var isNewRegistration = false
    var dataSource = LoginControllerDataSource()
    var resendOtp:UIButton!
    var verifyOTPCounter = 0
    private var userSelectedLocation:String = ""
    private var isReferralClicked:Bool = false
    var nameLength = 0, phoneNumberLength = 0,referralCodeLength = 0
    var userTitle = "" {
        didSet {
            if userTitle.count == 0 {
                let cell = tableView.cellForRow(at: IndexPath(row: 3, section: 0)) as? NameCell
                cell?.titleButton.setTitle(title,for: .normal)
                cell?.titleTopConst.constant = constant.titlePositionDown.rawValue
            }
        }
    }
    var phoneCode: String = "" {
        didSet {
            BBQUserDefaults.sharedInstance.phoneCode = phoneCode
        }
    }
    var isPickerView = false
    var signUpFailed = false
    var currentTheme = theme.white
    var fieldValidation = true {
        didSet {
            if currentScreen == .onlyMobileNumber_screen {
                if fieldValidation == true {
                    updatePhoneNumberCell(showError: false)
                }
            }
        }
    }
    
    static var socialProvider : signupProvider?
    var socialSignupToken : String = ""
    
    lazy var loginVerifyViewModel : LoginVerifyViewModel = {
        let viewModel = LoginVerifyViewModel()
        return viewModel
    }()
    
    lazy private var profileViewModel : BBQProfileViewModel = {
        let profileVM = BBQProfileViewModel()
        return profileVM
    } ()
    lazy private var loginControllerViewModel : LoginControllerViewModel = {
        let viewModel = LoginControllerViewModel()
        return viewModel
    }()
    lazy var notificationViewModel : BBQNotificationTokenViewModel = {
        let viewModel = BBQNotificationTokenViewModel()
        return viewModel
    }()
    
    var screenMaxHeight = false {
        didSet{
            if self.currentScreen == .onlyMobileNumber_screen {
                subViewHeightConst.constant = keyboardHeight +
                    screenHeight.screen_one_height.rawValue
            } else if self.currentScreen == .mobile_NameField_screen {
                subViewHeightConst.constant = keyboardHeight +
                    screenHeight.screen_two_height.rawValue
            }
            
            UIView.animate(withDuration: TimeInterval(constant.delayDuration.rawValue), delay: 0, options: .curveEaseIn, animations: {
                self.view.layoutIfNeeded()
            }, completion: { finished in
                
            })
        }
    }
    
    var currentScreen = screen.onlyMobileNumber_screen {
        didSet {
            if self.currentScreen == .onlyMobileNumber_screen {
                verifyOTPCounter = 0
                phoneNumberText = ""
                self.nameTextField?.text = ""
                phoneNumberLength = 0
                nameLength = 0
                userName = ""
                referralCode = ""
                userTitle = ""
                phoneCode = Constants.App.defaultValues.phoneNumberCode
                nextButton.isHidden = true
            } else if self.currentScreen == .mobile_NameField_screen {
                signUpFailed = false
                verifyOTPCounter = 0
            }
            
            screenChanged(screenValue: currentScreen)
        }
    }
    
    var navigationFromScreen: LoginNavigationScreen = .Splash
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AnalyticsHelper.shared.screenName(name: "LoginScreen", className: "LoginController")
//        Analytics.logEvent("Logging In", parameters: [
//            "name": "Login" as NSObject,
//            "full_text": "Login page" as NSObject
//            ])
        AnalyticsHelper.shared.triggerEvent(type: .A01)
        //Authorize
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (_, error) in
            guard error == nil else{
                print(error!.localizedDescription)
                return
            }
        }
      
        //GIDSignIn.sharedInstance()?.delegate = self
        dataSource.loginController = self
        dataSource.tableView = self.tableView
        dataSource.registerTableViewCells()
        self.tableView.dataSource = dataSource
        self.tableView.delegate = dataSource
        phoneCode = Constants.App.defaultValues.phoneNumberCode
        BBQUserDefaults.sharedInstance.phoneCode = phoneCode
        if  currentTheme == .white {
            self.view.backgroundColor = UIColor.red.withAlphaComponent(0)
            self.blurView()
        } else {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            subView.backgroundColor = Constants.App.themeColor.white
        }
        
        tableView.backgroundColor = UIColor.red.withAlphaComponent(0)
        subView.addSubview(tableView)
        
        self.tableView.maskByRoundingCorners(cornerRadius: constant.cornerRadius.rawValue, maskedCorners: CornerMask.topCornersMask)
        self.subView.maskByRoundingCorners(cornerRadius: constant.cornerRadius.rawValue, maskedCorners: CornerMask.topCornersMask)
        
        let tap = UITapGestureRecognizer(target: self, action: nil)
        self.view.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        
        let keyboardTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        subView.addGestureRecognizer(keyboardTap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setViewHeight()
        self.screenName(name: .Login_Screen)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
        
        screenMaxHeight = false
        setViewHeight()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if touch?.view == self.view {
            if shouldCloseTouchingOutside {
                view.endEditing(true)
                self.dismiss(animated: false, completion: nil)
                
            }
        }
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if isPickerView {
            return
        }
        
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            if self.currentScreen == .mobile_NameField_screen{
                keyboardHeight = keyboardRectangle.height + 78
                
            }else{
                keyboardHeight = keyboardRectangle.height + 35 // Adding skip button space also.
                
            }
        }
        
        screenMaxHeight = true
    }
    
    /* DELEGATE CALL */
    func screenChanged(screenValue: screen) {
        setViewHeight()
        self.tableView.reloadData {
            let phoneNumberCell = self.tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as? PhoneNumberCell
            let nameCell = self.tableView.cellForRow(at: IndexPath(row: 3, section: 0)) as?
            NameCell
            
            
            if screenValue == .mobile_NameField_screen {
                phoneNumberCell?.textField.becomeFirstResponder()
                if let name = self.userName, name.count > 0 { // Only if user name is fetched.
                    self.userName = name
                    self.nameLength = name.count
                    self.nameTextField?.text = name
                    nameCell?.changeLabelPosition(position: 5.0)
                } else {
                    self.nameTextField?.text = self.userName
                    self.nameLength = self.userName?.count ?? 0
                    nameCell?.changeLabelPosition(position: 30)
                }
                /*
                let referralCell = self.tableView.cellForRow(at: IndexPath(row: 4, section: 0)) as? ReferralCodeCell
                referralCell?.delegate = self
                if let code = self.referralCode,code.count>0{
                    
                }else{
                    referralCell?.referralTextField.text = ""
                    if self.currentTheme == .white {
                        let attributes = referralCell?.getTheAttributeWIthGivenColor(color: Constants.App.themeColor.white)
                        
                        referralCell!.referralTextField.attributedPlaceholder = NSAttributedString(string: kEnterInviteCode, attributes: attributes)
                    } else {
                        let attributes = referralCell?.getTheAttributeWIthGivenColor(color: Constants.App.themeColor.grey)
                        referralCell!.referralTextField.attributedPlaceholder = NSAttributedString(string: kEnterInviteCode, attributes: attributes)
                    }
                    referralCell?.referralTitleLabel.isHidden = true
                }*/
                
                
                
                if self.nameTextField.text!.count < Constants.App.Validations.nameMinLength || !self.isValidNumber(number: self.phoneNumberText) {
                    self.backButtonCell.sendOTPButton.alpha = opacity.week.rawValue
                } else {
                    self.backButtonCell.sendOTPButton.alpha = opacity.solid.rawValue
                }
            } else if screenValue == .onlyMobileNumber_screen {
                phoneNumberCell?.labelTopConst.constant = 4
            }
            
            phoneNumberCell?.countryCodeButton.setTitle(self.phoneCode, for: .normal)
        }
    }
    
    func setViewHeight() {
        
        switch self.currentScreen {
        case .onlyMobileNumber_screen:
            subViewHeightConst.constant = screenHeight.screen_one_height_noKB.rawValue
        case .mobile_NameField_screen:
            subViewHeightConst.constant = screenHeight.screen_two_height_noKB.rawValue
        //case to be handled for email
        default: print("case to be handled for email")
        }
        
        UIView.animate(withDuration: TimeInterval(constant.delayDuration.rawValue), delay: 0, options: .curveEaseIn, animations: {
            self.view.layoutIfNeeded()
        }, completion: { finished in
            
        })
    }
    
}

//MARK: Private Methods
extension LoginController {
    private func generateOTPWithOtpId(otpId: String) {
        BBQActivityIndicator.showIndicator(view: self.view)
        AnalyticsHelper.shared.triggerEvent(type: .login_otp_status)
        loginVerifyViewModel.generateOTP(countryCode: self.phoneCode, mobileNumber: Int64(self.phoneNumberText)!, otpId: otpId) { (isSuccess) in
            
            DispatchQueue.main.async {
                
                BBQActivityIndicator.hideIndicator(from: self.view)
                if isSuccess {
                    AnalyticsHelper.shared.triggerEvent(type: .login_otp_success_value)
                    self.goToOTPScreen()
                }else{
                    AnalyticsHelper.shared.triggerEvent(type: .login_otp_failure_value)
                }
            }
        }
    }
    
    private func gotoHomeScreen() {
        self.view.endEditing(true)
// If user login/signup from the reserve screen and no location is set by user before login,The outlet selected as guest user will be taken as the outlet for booking and same has to be updated as the last visited branch of the user
        if self.userSelectedLocation.count == 0{
            let bbqLastVisitedViewModel = BBQLastVisitedViewModel()
            bbqLastVisitedViewModel.getLastVisitedBranch(id: BBQLocationManager.shared.outlet_branch_id) {_ in
                //                print(self.bbqLastVisitedViewModel)
            }

        }
        if let delegate = self.delegate {
            self.dismiss(animated: false) {
                if let userData = self.loginVerifyViewModel.userVerificationData, userData.isExistingUser {
                    delegate.gotoHomeScreen(isExistingUser: true, navigationFrom: self.navigationFromScreen)
                } else {
                    delegate.gotoHomeScreen(isExistingUser: false, navigationFrom: self.navigationFromScreen)
                }
            }
        }
    }
    
    private func goToHomeFromSocialLogin(isNewUser: Bool) {
        self.dismiss(animated: false) {
            self.delegate?.gotoHomeScreen(isExistingUser: !isNewUser,
                                          navigationFrom: self.navigationFromScreen)
        }
    }
    
    private func blurView() {
        subView.backgroundColor = UIColor.white.withAlphaComponent(0)
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = subView.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        subView.addSubview(blurEffectView)
    }
    
    private func isValidNumber(number: String) -> Bool {
        return BBQPhoneNumberValidation.isValidNumber(number: number, for: phoneCode)
    }
    
    private func showSendOTPButton() {
        if currentScreen == .mobile_NameField_screen {
            let validNumber = isValidNumber(number: self.phoneNumberText)
            let validTitle = (userTitle.count > 0)
            let validName = (nameLength >= Constants.App.Validations.nameMinLength)
            if(validNumber) {
                updatePhoneNumberCell(showError: false)
            }
            if validTitle {
                updateTitleCell(showError: false)
            }
            if validName {
                updateNameCell(showError: false)
            }
            
            if !(validNumber && validTitle && validName) {
                backButtonCell.sendOTPButton.alpha = opacity.week.rawValue
                fieldValidation = false
            } else {
                backButtonCell.sendOTPButton.alpha = opacity.solid.rawValue
                fieldValidation = true
            }
        } else if currentScreen == .onlyMobileNumber_screen {
            if isValidNumber(number: self.phoneNumberText) {
                fieldValidation = true
            } else {
                fieldValidation = false
            }
        }
    }
}

//MARK: Button Tapped
extension LoginController {
    
    @objc func backButtonTapped(sender: UIButton){
        if self.currentScreen == .mobile_NameField_screen {
            self.currentScreen = screen.onlyMobileNumber_screen
        }
    }
    
    @objc func titleTapped(sender: UIButton) {
        let pickerViewController = UIStoryboard.loadPickerViewController()
        pickerViewController.delegate = self
        pickerViewController.type = .title
        pickerViewController.isComingFromProfile = false
        pickerViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(pickerViewController, animated: false, completion: nil)
        
        isPickerView = true
    }
    
    
    @objc func skipButtonTapped(sender: UIButton) {
        AnalyticsHelper.shared.triggerEvent(type: .A07)
        if let delegate = self.delegate {
            self.dismiss(animated: false) {
                delegate.gotoHomeScreen(isExistingUser: true, navigationFrom: self.navigationFromScreen)
            }
        }
    }
    
    @objc func countryCodeTapped(sender: UIButton){
        let pickerViewController = UIStoryboard.loadPickerViewController()
        pickerViewController.delegate = self
        pickerViewController.type = .countryCode
        pickerViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(pickerViewController, animated: false, completion: nil)
        isPickerView = true
    }
}

//MARK: Handling Name & Phone field
extension LoginController: PhoneNumberCellDelegate, NameCellDelegate,ReferralCellDelegate{
    func ReferralCodeEntered(length: Int, text: String) {
        if length >= Constants.App.Validations.nameMinLength {
            backButtonCell.sendOTPButton.alpha = opacity.solid.rawValue
        } else {
            self.backButtonCell.sendOTPButton.alpha = opacity.week.rawValue
        }
        self.referralCode = text
        referralCodeLength = length
    }
    
    func didReferralReturnKeyPressed(){
        view.endEditing(true)
        screenMaxHeight = false
        setViewHeight()
    }
    
    func didNameReturnKeyPressed() {
        view.endEditing(true)
        screenMaxHeight = false
        setViewHeight()
    }
    func nameEntered(length: Int, text: String) {
        if length >= Constants.App.Validations.nameMinLength {
            backButtonCell.sendOTPButton.alpha = opacity.solid.rawValue
        } else {
            self.backButtonCell.sendOTPButton.alpha = opacity.week.rawValue
        }
        
        self.userName = text
        nameLength = length
        showSendOTPButton()
    }
    
    func phoneNumberEntered(length: Int, text: String) {
        if currentScreen == .onlyMobileNumber_screen {
            if length >= Constants.App.Validations.nameMinLength {
                nextButton.isHidden = false
            } else {
                nextButton.isHidden = true
            }
        }
        
        phoneNumberText = text
        phoneNumberLength = length
        showSendOTPButton()
    }
    
    func textFieldStartsEditing(_ textField: UITextField) {
        // TODO: Make these optional
    }
    
    func textFieldStopsEditing(_ textField: UITextField) {
        // TODO: Make these optional
    }
}

//MARK: Handling OTP
extension LoginController {
    
    func loginUser(otp:String) {
        self.view.endEditing(true)
        
        BBQActivityIndicator.showIndicator(view: self.view)
        AnalyticsHelper.shared.triggerEvent(type: .login_status)
        self.loginControllerViewModel.login(countryCode: self.phoneCode, mobileNumber: Int64(self.phoneNumberText)!, otpId: self.loginVerifyViewModel.otpId, otp: Int(otp)!, completion: { (isLoginSuccess) in
            BBQActivityIndicator.hideIndicator(from: self.view)
            if isLoginSuccess {
                //if Login success update header with user previously selected location
                AnalyticsHelper.shared.triggerEvent(type: .login_success_value)
                if !BBQUserDefaults.sharedInstance.isGuestUser{
                    //                    if BBQUserDefaults.sharedInstance.branchIdValue == ""{
                    self.profileViewModel.getUserProfile { (isFetched) in
                        if isFetched {

                            //Delete token for guest user And send the token for  logged in user
                            if BBQUserDefaults.sharedInstance.accessToken != "" && BBQUserDefaults.sharedInstance.isGuestUserAPI == true {
                                
                                self.notificationViewModel.deleteNotificationTokenForGuestUser(token: BBQUserDefaults.sharedInstance.firebaseToken, deviceType: "IOS"){ (isSuccess) in
                                    print("Login: delete token for guest user",isSuccess)
                                    BBQUserDefaults.sharedInstance.isGuestUserAPI = false
                                    
                                    //In case postNotificationToken has return unsucessful , then BBQUserDefaults.sharedInstance.firebaseToken will be ""
                                    //restrict to hit api when its blank
                                    
                                    if  BBQUserDefaults.sharedInstance.firebaseLocalToken != "" {
                                        self.notificationViewModel.postNotificationToken(token: BBQUserDefaults.sharedInstance.firebaseLocalToken, deviceType: "IOS"){ (isSuccess) in
                                            BBQUserDefaults.sharedInstance.firebaseToken = BBQUserDefaults.sharedInstance.firebaseLocalToken
                                            print("Login: create token for logged in user",isSuccess)
                                        }
                                    }
                                }
                            }else if BBQUserDefaults.sharedInstance.accessToken != "" {
                                self.notificationViewModel.postNotificationToken(token: BBQUserDefaults.sharedInstance.firebaseLocalToken, deviceType: "IOS"){ (isSuccess) in
                                    BBQUserDefaults.sharedInstance.firebaseToken = BBQUserDefaults.sharedInstance.firebaseLocalToken
                                    print("Login: create token for logged in user",isSuccess)
                                }
                            }
                            //
                            if let userModel =  self.profileViewModel.profileData{
                                
                                if !BBQBookingUtilies.shared.isLocationAlreadySet() {
                                    // If location set already once, no need to fetch and show last visited branch.
                                    BBQUserDefaults.sharedInstance.CurrentSelectedLocation = (userModel.lastVisitedBranchName)!
                                    BBQUserDefaults.sharedInstance.branchIdValue = (userModel.lastVisitedBranch)!
                                }
                                if userModel.lastVisitedBranchName!.count>0{
                                    BBQUserDefaults.sharedInstance.CurrentSelectedLocation = (userModel.lastVisitedBranchName)!
                                    self.userSelectedLocation = BBQUserDefaults.sharedInstance.CurrentSelectedLocation

                                }else{
                                    self.userSelectedLocation = ""
                                }
                                BBQUserDefaults.sharedInstance.signUpReferralCode = (userModel.referralCode)
                                if let appDelegate = self.appDelegate {
                                    appDelegate.saveNotificationData(dataArray: [])
                                }
                                DispatchQueue.main.async {
                                    let userDict = ["userSelectedLocation": userModel.lastVisitedBranchName,
                                                    "selectedBrachID": userModel.lastVisitedBranch]
                                    NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.NotificationName.receiedUserProfileData), object: nil, userInfo: userDict as [AnyHashable : Any])
                                    self.gotoHomeScreen()
                                }
                                
                                
                            }
                        }else{
                        }
                    }
                    //                    }
                }
            } else {
                AnalyticsHelper.shared.triggerEvent(type: .login_failure_value)
                PopupHandler.showSingleButtonsPopup(title: kFailed,
                                                    message: kLoginFailed,
                                                    image: UIImage(named: "icon_cross_black"),
                                                    on: self,
                                                    firstButtonTitle: kOkButton,
                                                    firstAction: { })
            }
        })
    }
    
    func registerUser(otp:String) {
        BBQActivityIndicator.showIndicator(view: self.view)
        AnalyticsHelper.shared.triggerEvent(type: .registration_status)
        let formattedTitle = loginVerifyViewModel.formattedUserTitle(with: self.userTitle)
        AnalyticsHelper.shared.triggerEvent(type: .A05A)
        self.loginControllerViewModel.registerUser(countryCode: self.phoneCode,
                                                   mobileNumber: Int64(self.phoneNumberText)!,
                                                   title: formattedTitle,
                                                   name: self.userName ?? "",
                                                   otpId: self.loginVerifyViewModel.otpId,
                                                   otp: Int(otp)!, referralCode: self.referralCode ?? "", completion: { (isRegisterSuccess) in
                                                    
                                                    BBQActivityIndicator.hideIndicator(from: self.view)
                                                    if isRegisterSuccess {
                                                        AnalyticsHelper.shared.triggerEvent(type: .A05AA)
                                                        AnalyticsHelper.shared.triggerEvent(type: .registration_success_value)
                                                        self.profileViewModel.getUserProfile { (isFetched) in
                                                            if isFetched {
                                                                BBQActivityIndicator.hideIndicator(from: self.view)
                                                                if let userModel =  self.profileViewModel.profileData{
                                                                    
                                                                    if userModel.lastVisitedBranchName!.count>0{
                                                                        BBQUserDefaults.sharedInstance.CurrentSelectedLocation = (userModel.lastVisitedBranchName)!
                                                                        self.userSelectedLocation = BBQUserDefaults.sharedInstance.CurrentSelectedLocation

                                                                    }else{
                                                                        self.userSelectedLocation = ""

                                                                    }
                                                                    if let appDelegate = self.appDelegate {
                                                                        appDelegate.saveNotificationData(dataArray: [])
                                                                    }
                                                                    BBQUserDefaults.sharedInstance.branchIdValue = (userModel.lastVisitedBranch)!
                                                                    BBQUserDefaults.sharedInstance.signUpReferralCode = (userModel.referralCode)
                                                                    DispatchQueue.main.async {
                                                                        let userDict = ["userSelectedLocation": userModel.lastVisitedBranchName,
                                                                                        "selectedBrachID": userModel.lastVisitedBranch]
                                                                        NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.NotificationName.receiedUserProfileData), object: nil, userInfo: userDict as [AnyHashable : Any])
                                                                        self.gotoHomeScreen()
                                                                    }
                                                                    
                                                                    
                                                                }
                                                            }else{
                                                                BBQActivityIndicator.hideIndicator(from: self.view)
                                                            }
                                                        }
                                                    } else {
                                                        AnalyticsHelper.shared.triggerEvent(type: .A05AB)
                                                        AnalyticsHelper.shared.triggerEvent(type: .registration_failure_value)
                                                        PopupHandler.showSingleButtonsPopup(title: kFailed,
                                                                                            message: kRegistrationFailed,
                                                                                            image: UIImage(named: "icon_cross_black"),
                                                                                            on: self,
                                                                                            firstButtonTitle: kOkButton,
                                                                                            firstAction: { })
                                                    }
        })
    }
    
    func handleSocialMediaSignupWith(provider: signupProvider, otp: Int) {
        
        BBQActivityIndicator.showIndicator(view: self.view)
        
        
        if self.loginVerifyViewModel.otpId == "" {
            self.loginVerifyViewModel.otpId = "0" // Handling back button once after otpID is generated.
        }
        AnalyticsHelper.shared.triggerEvent(type: .social_login_status)
        provider.triggerPressedEvent()
        let formattedTitle = loginVerifyViewModel.formattedUserTitle(with: self.userTitle)
        self.loginControllerViewModel.socialLogin(token: self.socialSignupToken,
                                                  provider: provider.rawValue ,
                                                  countryCode:self.phoneCode,
                                                  mobileNumber: Int64(self.phoneNumberText) ?? 0,
                                                  otpId: self.loginVerifyViewModel.otpId ,
                                                  otp: otp,
                                                  title: formattedTitle,
                                                  name: self.userName ?? "",
                                                  completion: { (isNewUser, isLoginFailed) in
                                                    
                                                    print("isNewUser",isNewUser)
                                                    print("isLoginFailed",isLoginFailed)
                                                    if !isLoginFailed { // Incase of successful login
                                                        AnalyticsHelper.shared.triggerEvent(type: .social_login_success_value)
                                                        provider.triggerSuccesEvent()
                                                        self.profileViewModel.getUserProfile { (isFetched) in
                                                            if isFetched {
                                                                BBQActivityIndicator.hideIndicator(from: self.view)
                                                                if let userModel =  self.profileViewModel.profileData{
                                                                    if userModel.lastVisitedBranchName!.count>0{
                                                                        BBQUserDefaults.sharedInstance.CurrentSelectedLocation = (userModel.lastVisitedBranchName)!
                                                                    }
                                                                    BBQUserDefaults.sharedInstance.branchIdValue = (userModel.lastVisitedBranch)!
                                                                    BBQUserDefaults.sharedInstance.signUpReferralCode = (userModel.referralCode)
                                                                    if let mobileNumber = userModel.mobileNumber{
                                                                        BBQUserDefaults.sharedInstance.customerId = mobileNumber
                                                                    }
                                                                    DispatchQueue.main.async {
                                                                        let userDict = ["userSelectedLocation": userModel.lastVisitedBranchName,
                                                                                        "selectedBrachID": userModel.lastVisitedBranch]
                                                                        NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.NotificationName.receiedUserProfileData), object: nil, userInfo: userDict as [AnyHashable : Any])
                                                                        self.goToHomeFromSocialLogin(isNewUser: self.isNewRegistration)
                                                                    }
                                                                    
                                                                    
                                                                }
                                                            }else{
                                                                BBQActivityIndicator.hideIndicator(from: self.view)
                                                            }
                                                        }
                                                    }else{
                                                        provider.triggerFailureEvent()
                                                        AnalyticsHelper.shared.triggerEvent(type: .social_login_failure_value)
                                                        
                                                        //handle case when , isNewUser = false isLoginFailed = true
                                                        //Check if social token exist,
                                                        //fetch profile details from token
                                                        //update profile model with respective value
                                                        
                                                        BBQActivityIndicator.hideIndicator(from: self.view)
//                                                        guard let tokenExist = self.socialSignupToken as? String else {
//                                                            // Go to specified screen, take mobile number
//                                                        }
                                                        
                                                    }
                                                    
                                                    if isNewUser { // Incase of new user navigate to signup page
                                                        BBQActivityIndicator.hideIndicator(from: self.view)
                                                        self.isNewRegistration = true
                                                        self.currentScreen = .mobile_NameField_screen
                                                        let phoneNumberCell = self.tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as? PhoneNumberCell
                                                        phoneNumberCell?.textField.text = ""
                                                    }
                                                    
        })
        
    }
    
}

//MARK: Picker View
extension LoginController: PickerViewDelegate {
    
    func titleSelected(title: String) {
        let cell = tableView.cellForRow(at: IndexPath(row: 3, section: 0)) as? NameCell
        cell?.titleButton.setTitle(title,for: .normal)
        cell?.titleTopConst.constant = constant.titlePositionUp.rawValue
        userTitle = title
        showSendOTPButton()
        isPickerView = false
    }
    
    func countryCodeSelected(phoneCode: String) {
        let cell = tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as? PhoneNumberCell
        if self.phoneCode != phoneCode {
            self.phoneNumberText = ""
            phoneNumberLength = 0
            cell?.textField.text = ""
            nextButton.isHidden = true
        }
        self.phoneCode = phoneCode
        cell?.countryCodeButton.setTitle(self.phoneCode, for: .normal)
        isPickerView = false
        showSendOTPButton()
    }
    
    func pickerClosed() {
        isPickerView = false
    }
}

//MARK: Google Login
//extension LoginController: GIDSignInDelegate {
//
//    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
//////        return GIDSignIn.sharedInstance().handle(url as URL?,
////                                                 sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
////                                                 annotation: options[UIApplication.OpenURLOptionsKey.annotation])
//        return GIDSignIn.sharedInstance()?.handle(url as URL?) ?? false
//    }
//
//    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
//              withError error: Error!) {
//        if let error = error {
//            print("\(error.localizedDescription)")
//        } else {
//            self.loginControllerViewModel.googleUser = user
//            self.userName = user.profile.name
//            self.handleSocialMediaSignupWith(provider: .google, otp: 0)
//        }
//    }
//
//    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
//              withError error: Error!) {
//        // Perform any operations when the user disconnects from app here.
//        // ...
//        print(error.localizedDescription)
//    }
//}


//MARK: Instagram Login
extension LoginController : BBQInstagramLoginControllerProtocol {
    
    func didRecieveInstagram(userName: String, authToken: String) {
        self.socialSignupToken = authToken
        self.userName = userName
        self.handleSocialMediaSignupWith(provider: .instagram, otp: 0)
    }
    
}

//Field Validation
extension LoginController {
    //First Screen
    @objc func nextButtonTapped(sender: UIButton) {
        LoginController.socialProvider = .others // Using phone number for signup
        if fieldValidation == true {
            updatePhoneNumberCell(showError: false)
//            if self.phoneNumberText == "" {
                let phoneNumberCell = self.tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as? PhoneNumberCell
                self.phoneNumberText = phoneNumberCell?.textField.text ?? ""
//            }
            guard let phoneNumberText = Int64(self.phoneNumberText) else {
                return
            }
            BBQActivityIndicator.showIndicator(view: self.view)
            
            var countryCode = BBQUserDefaults.sharedInstance.phoneCode
            if countryCode == "" {
                countryCode = "+91" // Fix for clearing phone code scenarios.
            }
            AnalyticsHelper.shared.triggerEvent(type: .A02)
            loginVerifyViewModel.verifyUserPhoneNUmber(countryCode: countryCode,
                                                       mobileNumber: phoneNumberText,
                                                       otpGenerate: true) { (isSuccess) in
                if isSuccess {
                    BBQActivityIndicator.hideIndicator(from: self.view)
                    AnalyticsHelper.shared.triggerEvent(type: .A02A)
                    if let userData = self.loginVerifyViewModel.userVerificationData, !userData.isExistingUser {
                        AnalyticsHelper.shared.triggerEvent(type: .A05)
                        self.currentScreen = screen.mobile_NameField_screen
                    } else {
                        AnalyticsHelper.shared.triggerEvent(type: .A06)
                        self.goToOTPScreen()
                    }
                } else {
                    // Showing response error field from server, written in base manager class
                    self.view.endEditing(true) // Forcing keyboiard to dismiss to show error toast.
                    AnalyticsHelper.shared.triggerEvent(type: .A02B)
                    BBQActivityIndicator.hideIndicator(from: self.view)
                }
            }
        } else {
            updatePhoneNumberCell(showError: true)
        }
    }
    
    //second & third screen
    @objc func sendOTPTapped(sender: UIButton) {
        // TODO: Refactoring needed.
        showSendOTPButton()
        if fieldValidation {
            BBQActivityIndicator.showIndicator(view: self.view)
           
            var countryCode = BBQUserDefaults.sharedInstance.phoneCode
            if countryCode == "" {
                countryCode = "+91" // Fix for clearing phone code scenarios.
            }
            
            loginVerifyViewModel.verifyUserPhoneNUmber(countryCode: countryCode, mobileNumber: Int64(self.phoneNumberText)!, otpGenerate: true) { (isSuccess) in
                if isSuccess {
                    if let userData = self.loginVerifyViewModel.userVerificationData, !userData.isExistingUser {
                        BBQActivityIndicator.hideIndicator(from: self.view)
                        if let refSTring = self.referralCode, !self.referralCode!.isEmpty {
                            self.loginVerifyViewModel.verifyReferralCodeEntered(referralCode: refSTring){(isSuccessful) in
                                if isSuccessful{
                                    //If referral Code verification is successful call generate otp API
                                    self.loginVerifyViewModel.generateOTP(countryCode: BBQUserDefaults.sharedInstance.phoneCode,
                                                                          mobileNumber: Int64(self.phoneNumberText)!,
                                                                          otpId:"") { (isSuccess) in
                                                                            self.goToOTPScreen()
                                    }
                                }else{
                                    //If referral Code verification is failed show error
                                    DispatchQueue.main.async {
                                        self.showFailedReferralCodeVerificationOnUI()
                                    }
                                    
                                }
                            }
                        }else{
                            self.loginVerifyViewModel.generateOTP(countryCode: BBQUserDefaults.sharedInstance.phoneCode,
                                                                  mobileNumber: Int64(self.phoneNumberText)!,
                                                                  otpId:"") { (isSuccess) in
                                                                    self.goToOTPScreen()
                            }
                        }
                        
                        
                    } else {
                        self.goToOTPScreen()
                    }
                    
                } else {
                    // Showing response error field from server, written in base manager class
                    BBQActivityIndicator.hideIndicator(from: self.view)
                }
            }
        } else {
            checkAllFieldValidation()
        }
    }
    
    func showFailedReferralCodeVerificationOnUI(){
        if currentScreen == .mobile_NameField_screen {
            let cell = self.tableView.cellForRow(at: IndexPath(row: 4, section: 0)) as? ReferralCodeCell
            cell?.referralErrorLabel.stopBlink()
            cell?.referralErrorLabel.startBlink()
            cell?.referralErrorLabel.isHidden = false
        }
    }
    func checkAllFieldValidation() {
        //Phone number
        if isValidNumber(number: self.phoneNumberText) {
            updatePhoneNumberCell(showError: false)
        } else {
            updatePhoneNumberCell(showError: true)
        }
        
        //name
        if nameLength < Constants.App.Validations.nameMinLength {
            updateNameCell(showError: true)
        } else {
            updateNameCell(showError: false)
        }
        
        //title
        if userTitle.count <= 0 {
            updateTitleCell(showError: true)
        } else {
            updateTitleCell(showError: false)
        }
    }
    
    func updatePhoneNumberCell(showError:Bool) {
        let cell = self.tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as? PhoneNumberCell
        cell?.errorMessage.stopBlink()
        cell?.errorMessage.startBlink()
        cell?.errorMessage.isHidden = !showError
    }
    
    func updateNameCell(showError:Bool) {
        let cell = self.tableView.cellForRow(at: IndexPath(row: 3, section: 0)) as? NameCell
        cell?.nameErrorMessage.stopBlink()
        cell?.nameErrorMessage.startBlink()
        cell?.nameErrorMessage.isHidden = !showError
    }
    
    func updateTitleCell(showError:Bool) {
        let cell = self.tableView.cellForRow(at: IndexPath(row: 3, section: 0)) as? NameCell
        cell?.titleErrorMessage.stopBlink()
        cell?.titleErrorMessage.startBlink()
        cell?.titleErrorMessage.isHidden = !showError
    }
}

extension LoginController:BBQOTPScreenDelegate {
    func otpValidation(status: Bool, otp:String) {
        if status == true {
            if let userData = self.loginVerifyViewModel.userVerificationData, userData.isExistingUser {
                if let provider = LoginController.socialProvider, provider != .others  {
                    self.handleSocialMediaSignupWith(provider: provider, otp: Int(otp) ?? 0)
                } else {
                    self.loginUser(otp: otp)
                }
                
            } else {
                self.view.endEditing(true)
                if let provider = LoginController.socialProvider, provider != .others  {
                    self.handleSocialMediaSignupWith(provider: provider, otp: Int(otp) ?? 0)
                } else {
                    self.registerUser(otp: otp)
                }
            }
        } else {
            ToastHandler.showToastWithMessage(message: kMaxAtmpOtpReachDesc)
            self.subView.isHidden = false
            self.signUpFailed = true
            let lastScreen = screen.onlyMobileNumber_screen
            self.currentScreen = lastScreen
            setViewHeight()
            
        }
    }
    
    func backButtonPressed() {
        let screen = self.currentScreen
        self.view.endEditing(true)
        self.screenMaxHeight = false
        self.currentScreen = screen
        self.subView.isHidden = false
        
        
        //When back button is pressed from OTP screen, Fill the previously entered phone number
        self.phoneNumberEntered(length: 10, text: phoneNumberText)
        self.tableView.reloadData {
            // let phoneNumberCell = self.tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as? PhoneNumberCell
            
            let phoneNumberCell = self.tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as? PhoneNumberCell
            phoneNumberCell?.textField.text = self.phoneNumberText
            phoneNumberCell?.textField?.becomeFirstResponder()
            
            if self.isValidNumber(number: self.phoneNumberText) {
                self.fieldValidation = true
            } else {
                self.fieldValidation = false
            }
        }
        
    }
    
    @objc func dismiss() {
        self.dismiss(animated: false, completion: nil)
    }
    
    func goToOTPScreen() {
        BBQActivityIndicator.hideIndicator(from: self.view)
        signUpFailed = false
        self.subView.isHidden = true
        let otpViewController = UIStoryboard.loadOTPViewController()
        otpViewController.delegate = self
        otpViewController.screenHeight = Int(screenHeight.screen_three_height.rawValue)
        otpViewController.validationPath = self.phoneNumberText
        otpViewController.validationType = .mobileNumber
        
        let countryCode = BBQUserDefaults.sharedInstance.phoneCode
        if countryCode == "" {
            otpViewController.phoneCode  = "+91"
        }else{
            otpViewController.phoneCode = countryCode
        }
        
        otpViewController.otpID = loginVerifyViewModel.otpId
        otpViewController.header = ""
        otpViewController.otpMessage = ""
        otpViewController.signUp = kSignUp
        otpViewController.logoImage = Constants.AssetName.bbqLogo
        otpViewController.currentTheme = self.currentTheme
        otpViewController.otpType = .normalLogin
        otpViewController.shouldCloseTouchingOutside = self.shouldCloseTouchingOutside
        if let userData = self.loginVerifyViewModel.userVerificationData, userData.isExistingUser {
            otpViewController.isExistingUser = true
        } else {
            otpViewController.isExistingUser = false
        }
        otpViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(otpViewController, animated: true, completion: nil)
    }
}
