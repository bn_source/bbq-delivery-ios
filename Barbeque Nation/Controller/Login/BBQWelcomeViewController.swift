//
 //  Created by Sridhar on 16/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQWelcomeViewController.swift
 //

import UIKit

class BBQWelcomeViewController: UIViewController {

    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var unlimitedexperinceLabel: UILabel!
    @IBOutlet weak var welcomeCollectionView: UICollectionView!
    @IBOutlet weak var commentsLabel: UILabel!
    @IBOutlet weak var skipWelcomeButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        BBQUserDefaults.sharedInstance.welcomeScreen = "set"
    }
    

    @IBAction func skipwelcomePressed(_ sender: Any) {
        goToLoginPage()
    }
    
    func goToLoginPage() {
        let tabViewController = UIStoryboard.loadLoginBackground()
        self.navigationController?.pushViewController(tabViewController, animated: true)
    }
}

extension BBQWelcomeViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        /*if(collectionView.tag == Constants.TagValues.promotionCellTag) {
            return promotionsArray.count
        } else if(collectionView.tag == Constants.TagValues.voucherCellTag) {
            return vouchersArray.count
        }else{
            return upcomingReservationsArray.count
        }*/
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell  = UICollectionViewCell()
        
        let welcomeCell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CellIdentifiers.welcomeCell, for: indexPath) as! BBQWelcomeCollectionViewCell
            cell = welcomeCell
        /*if(collectionView.tag == Constants.TagValues.promotionCellTag){
            let promoCell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CellIdentifiers.promotionCell, for: indexPath) as! BBQPromotionCell
            if(promotionsArray.count>0){
                let  promodel:PromotionModel = promotionsArray[indexPath.row]
                promoCell.setPrmotionCellValues(promodel)
                
            }
            cell = promoCell
        }*/
        return cell
    }
    
    // MARK: - getWelcomeImagesorVideos
    func getWelcomeImagesorVideos(){
        /*BBQActivityIndicator.showIndicator(view: self.view)
        self.homeViewModel.getPromosVouchers(latitide: BBQLocationManager.shared.user_current_latitude, longitude: BBQLocationManager.shared.user_current_longitude, brach_ID: self.branchIDValue) {(isSuccess) in
            BBQActivityIndicator.hideIndicator(from: self.view)
            if isSuccess {
                self.promotionsArray = self.homeViewModel.getPromotionsDataData()
                self.promotionPageControl.isHidden = false
                self.promotionPageControl.numberOfPages = self.promotionsArray.count
                self.promoViewModel = PromosViewModel(promosDataArray: self.promotionsArray)
                self.vouchersArray = self.homeViewModel.getVouchersData()
                DispatchQueue.main.async {
                    self.promotionCollectionView .reloadData()
                    self.vouchersCollectionView.reloadData()
                }
            } else {
            }
        }
    */}
}

extension BBQWelcomeViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        /*if( collectionView.tag == Constants.TagValues.promotionCellTag) {
            //Create instance for the controller that will be shown
            let viewController = BottomSheetImagesViewController()
            
            //Move the item that is clicked to the top
            let item = (self.promoViewModel?.promosArray[indexPath.row])!
            self.promoViewModel?.promosArray.remove(at: indexPath.row)
            self.promoViewModel?.promosArray.insert(item, at: 0)
            
            //Assign View Model
            viewController.promosViewModel = self.promoViewModel
            
            //Present your controller over current controller
            let bottomSheetController = BottomSheetController()
            bottomSheetController.present(viewController, on: self)
        }*/
    }
}
