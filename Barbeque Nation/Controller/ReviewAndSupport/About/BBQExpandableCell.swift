//
 //  Created by Rabindra L on 12/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQExpandableCell.swift
 //

import UIKit

class BBQExpandableCell: UITableViewCell {

    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var iconArrow: UIImageView!
    
    @IBOutlet weak var iconLine: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
       // subView.layer.cornerRadius = 13
      //  subView.layer.shadowColor = UIColor.gray.cgColor
        subView.layer.shadowOpacity = 0.25
        subView.layer.shadowRadius = 10
        subView.layer.shadowOffset = CGSize(width: 5, height: 5)
        subView.setCornerRadius(12)
        subView.setShadow(color: UIColor.hexStringToUIColor(hex: "55606D1A") , opacity: 0.2, offSet: .zero, radius: 7, scale: true)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
