//
 //  Created by Sakir on 02/06/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified BBQAboutAppVC.swift
 //

import UIKit

class BBQAboutAppVC: BaseViewController {
    @IBOutlet weak var lblAppVersion: UILabel!
    @IBOutlet weak var lblBuild: UILabel!
    @IBOutlet weak var lblCopyrights: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblAppVersion.text = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
        lblBuild.text = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String
        lblCopyrights.text = String(format: "@2018 - %@ Barbeque Nation", Date().string(format: "YYYY"))
        
    }
    
    @IBAction func onClickBtnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
