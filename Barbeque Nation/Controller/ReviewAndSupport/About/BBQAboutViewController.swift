/*
 //  BBQAboutViewController.swift
 
 *  Created by Rabindra L on 12/09/19.
 *  Copyright © BARBEQUE NATION HOSPITALITY LIMITED 2019. All rights reserved.
 *  Last modified 12/09/19
 
 * CHANGE LOG
 * —------------------------------------------------------------------------------
 * ID          Bug ID      Author Name          Date                  Description
 * —------------------------------------------------------------------------------
 * 1            NA         Rabindra L           12/09/19        Initial Version
 */

import UIKit

class BBQAboutViewController: BBQBaseViewController {
    
    enum subString: String {
        case TERMS
        case PRIVACY
        case ABOUT
        case VERSION
        case FEED
    }

    @IBOutlet weak var tableView: UITableView!
    var selectedRow: Int?
    var aboutViewModel = BBQAboutViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AnalyticsHelper.shared.triggerEvent(type: .AU01)
        registerTableViewCells()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.screenName(name: .About_Us_Screen)
    }
        
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    func registerTableViewCells() {
        tableView.estimatedRowHeight = 74.0
        let expandableCell = UINib(nibName:Constants.CellIdentifiers.expandableCell, bundle:nil)
        self.tableView.register(expandableCell, forCellReuseIdentifier: Constants.CellIdentifiers.expandableCell)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        aboutViewModel.getAboutList() {_ in
            self.tableView.reloadData()
        }
        
    }
    
    func getImageFor(title: String) -> String {
        if title.lowercased().contains(subString.TERMS.rawValue.lowercased()) {
            return Constants.AssetName.termsCondition
        } else if title.lowercased().contains(subString.PRIVACY.rawValue.lowercased()) {
            return Constants.AssetName.privacyPolicy
        } else if title.lowercased().contains(subString.VERSION.rawValue.lowercased()) {
            return Constants.AssetName.appVersion
        }  else if title.lowercased().contains(subString.FEED.rawValue.lowercased()) {
            return Constants.AssetName.FeedDaileWager
        } else if title.lowercased().contains(subString.ABOUT.rawValue.lowercased()) {
            return Constants.AssetName.About
        }else {
            return Constants.AssetName.bbqPoints
        }
    }
}

extension BBQAboutViewController:UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let aboutModel = self.aboutViewModel.aboutDataModel {
            return aboutModel.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.expandableCell) as? BBQExpandableCell {
            
            if let aboutModel = self.aboutViewModel.aboutDataModel {
                cell.nameLabel.text = aboutModel[indexPath.row].field_title
                cell.iconImage.image = UIImage(named: getImageFor(title: aboutModel[indexPath.row].field_title ?? ""))
                
                if indexPath.row == selectedRow {
                    if let description = aboutModel[indexPath.row].field_description {
                        if aboutModel[indexPath.row].field_title?.lowercased().contains(subString.VERSION.rawValue.lowercased()) ?? true {
                            cell.detailsLabel.text = self.aboutViewModel.getAboutApp(copyRight: description)
                        } else {
                            cell.detailsLabel.text = description.htmlToString
                        }
                    } else {
                        cell.detailsLabel.text = ""
                    }
                    UIView.animate(withDuration: 0.2) {
                        cell.iconArrow.image = UIImage(named: "ExpandebleArrowUp")
                        cell.iconLine.isHidden = false
                    }
                completion: { (isCompleted) in
                    
                    cell.iconArrow.image = UIImage(named: "ExpandebleArrowUp")
                    cell.iconLine.isHidden = false
                }
                } else {
                    UIView.animate(withDuration: 0.2) {
                        cell.detailsLabel.text = ""
                        cell.iconArrow.image = UIImage(named: "ExpandebleArrowDown")
                        cell.iconLine.isHidden = true }
                completion: { (isCompleted) in
                    
                    cell.detailsLabel.text = ""
                    cell.iconArrow.image = UIImage(named: "ExpandebleArrowDown")
                    cell.iconLine.isHidden = true
                }
                    
                }
            }
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let oldIndex = selectedRow
        if selectedRow != indexPath.row {
            if let models = aboutViewModel.aboutDataModel, models.count > indexPath.row, (models[indexPath.row].field_title ?? "").lowercased().contains(subString.ABOUT.rawValue.lowercased()) {
                AnalyticsHelper.shared.triggerEvent(type: .AU02)
            }
            selectedRow = indexPath.row
            if let oldIndex = oldIndex{
                let oldIndexPath = IndexPath(row: oldIndex, section: 0)
                self.tableView.reloadRows(at: [oldIndexPath,indexPath], with: UITableView.RowAnimation.none)
            }else{
                self.tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.none)
            }
        } else {
            selectedRow = nil
            if let oldIndex = oldIndex{
                let oldIndexPath = IndexPath(row: oldIndex, section: 0)
                self.tableView.reloadRows(at: [oldIndexPath,indexPath], with: UITableView.RowAnimation.none)
            }
        }
    }
}
