//
 //  Created by Rabindra L on 14/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQRatingsViewController.swift
 //

import UIKit

protocol BBQRatingProtocol: AnyObject {
    func didRatingDonePressed()
    func didTapOutsideSheet()
}

class BBQRatingsViewController: UIViewController {

    @IBOutlet weak var viewHeightConst: NSLayoutConstraint!
    
    @IBOutlet weak var subView: UIView!
    
    @IBOutlet weak var topBlackView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var improveTitleLabel: UILabel!
    @IBOutlet weak var thankyouTitleLabel: UILabel!
    @IBOutlet weak var ratingDoneButton: UIButton!
    weak var ratingDelegate: BBQRatingProtocol? = nil
    var ratingBottomsheet : BottomSheetController? = nil

    
    var selectedRow:Int?
    var rating:Int?
    var bookingID:String?
    var ratingsViewModel = BBQFeedbackViewModel()
    
    var reasonList = [(qid:Int, question:String)]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()

        self.view.backgroundColor = UIColor.red.withAlphaComponent(0)
        self.subView.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        topBlackView.roundCorners(cornerRadius: 3, borderColor: UIColor(red:54.00, green:64.0, blue:68.0, alpha:1.0), borderWidth: 0.1)
        
        viewHeightConst.constant = 331
        registerCells()
        
        let tap = UITapGestureRecognizer(target: self, action: nil)
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        
        if self.reasonList.count>0{
             self.tableView.reloadData()
        }else{
            BBQActivityIndicator.showIndicator(view: self.view)
            ratingsViewModel.getBookingFeedbackQuestions() {_ in
                BBQActivityIndicator.hideIndicator(from: self.view)
                if let reasonList = self.ratingsViewModel.bookingFeedBackModel {
                    for reasons in reasonList {
                        self.reasonList.append((reasons.qid ?? 0, reasons.question ?? ""))
                    }
                    self.tableView.reloadData()
                }else{
                    
                }
            }
        }

    }
    
    // MARK: Setup Methods
    
    func setupUI() {
        self.improveTitleLabel.text = kRatingImproveDesc
        self.thankyouTitleLabel.text = kRatingThankYouTitle
        self.ratingDoneButton.roundCorners(cornerRadius: self.ratingDoneButton.frame.size.height/2, borderColor: .orange, borderWidth: 2.0)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if touch?.view == self.view {
            ratingBottomsheet!.dismissBottomSheet(on: self)

             self.ratingDelegate?.didTapOutsideSheet()
        }
    }
    
    private func registerCells() {
        let singleLabelCell = UINib(nibName:Constants.CellIdentifiers.singleLabelCell, bundle:nil)
        self.tableView.register(singleLabelCell, forCellReuseIdentifier: Constants.CellIdentifiers.singleLabelCell)

        let cancelButtonCell = UINib(nibName:Constants.CellIdentifiers.cancelButtonCell, bundle:nil)
        self.tableView.register(cancelButtonCell, forCellReuseIdentifier: Constants.CellIdentifiers.cancelButtonCell)
    }
 
    @IBAction func doneTapped(){
         submitRating()
    }
    func submitRating() {
        BBQActivityIndicator.showIndicator(view: self.view)
        ratingsViewModel.submitBookingFeedback(rating: (self.rating ?? 0), bookingID: (self.bookingID ?? ""), qid: (self.selectedRow ?? 0)) {(isSuccess) in
            BBQActivityIndicator.hideIndicator(from: self.view)
            if isSuccess{
                self.ratingDelegate?.didRatingDonePressed()
                //kFeedbackSuccessful
            ToastHandler.showToastWithMessage(message: kFeedbackSuccessful)
                if let presentingController = self.ratingBottomsheet{
                    presentingController.dismissBottomSheet(on: self)
                }
            }else{
                
            }
        }
    }
}

extension BBQRatingsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reasonList.count 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if let reasonsCell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.singleLabelCell) as? SingleLabelCell {
                reasonsCell.detailedLabel.text = ""
                reasonsCell.label.text = reasonList[indexPath.row].question

                reasonsCell.priceLabel.text = ""
                reasonsCell.checkBox.isHidden = true
                if let row = selectedRow {
                    if row == indexPath.row {
                        reasonsCell.checkBox.roundCorners(cornerRadius: 12, borderColor: #colorLiteral(red: 1, green: 0.3882352941, blue: 0.003921568627, alpha: 1), borderWidth: 0.1)
                        reasonsCell.checkBox.layer.borderWidth = 1
                        reasonsCell.checkBox.layer.borderColor = #colorLiteral(red: 1, green: 0.3882352941, blue: 0.003921568627, alpha: 1)
                        reasonsCell.checkBox.isHidden = false
                    }
                }
                
                return reasonsCell
            }
        let cell = tableView.dequeueReusableCell(withIdentifier: "reusableIdentifier", for: indexPath)
        cell.textLabel?.text = ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row < reasonList.count {
            selectedRow = indexPath.row
            viewHeightConst.constant = 331
            tableView.reloadData()
//            UIView.animate(withDuration: 0.5,
//                           delay: 0.1,
//                           options: .curveEaseIn,
//                           animations: {
//                            self.view.layoutIfNeeded()
//            }, completion: { finished in
//            })
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == reasonList.count {
            return UITableView.automaticDimension
        } else {
            return UITableView.automaticDimension
        }
    }
}
