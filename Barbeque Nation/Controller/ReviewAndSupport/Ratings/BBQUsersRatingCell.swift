//
 //  Created by Abhijit on 14/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQUsersRatingCell.swift
 //

import UIKit
protocol BBQUsersRatingProtocol: AnyObject {
    func ratingDismissed()
}

class BBQUsersRatingCell: UIViewController {

    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet var ratingStar: [UIButton]!
    
    @IBOutlet weak var improveTitleLabel: UILabel!
    @IBOutlet weak var experienceLabel: UILabel!
    weak var ratingcellDelegate :BBQUsersRatingProtocol? = nil
    var ratingsViewModel = BBQFeedbackViewModel()
    var selectedBookingID:String = ""
     var currentBottomSheet : BottomSheetController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
    }
    
    // MARK: Setup Methods
    
    func setupUI() {
        
        self.improveTitleLabel.text = kRatingImprove
        self.experienceLabel.text   = kRatingExxperience
        self.skipButton.isHidden = false
        self.view.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        let yourAttributes: [NSAttributedString.Key: Any] = [
            .foregroundColor: #colorLiteral(red: 0.2117647059, green: 0.2509803922, blue: 0.2666666667, alpha: 1),
            .underlineStyle: NSUnderlineStyle.single.rawValue]
        let attributeString = NSMutableAttributedString(string: kButtonSkip,
                                                        attributes: yourAttributes)
        self.skipButton.setAttributedTitle(attributeString, for: .normal)
    }
    
    // MARK: IBAction Methods
    
    @IBAction func skipButtonTapped(_ sender: Any) {
        currentBottomSheet!.dismissBottomSheet(on: self)
    }
    
    @IBAction func starPressed(_ sender: UIButton) {
        let selectedImage = UIImage(named: "icon_star_filled")
        let unselectedImage = UIImage(named: "icon_star_empty")
        
        for button in ratingStar {
            if button.tag <= sender.tag {
                button.setImage(selectedImage, for: .normal)
            } else {
                button.setImage(unselectedImage, for: .normal)
            }
        }
        
        sleep(UInt32(0.25))
        if sender.tag > 3 {
            self.submitRating(ratings:sender.tag, bookingID: self.selectedBookingID, qid:0)
            self.openAppStoreNavigationBottomSheet()
        } else {
            // Dismissing time slot first, then bottomsheet itself.
                self.openRatingReasonBottonSheet(rating: sender.tag,bookingId: self.selectedBookingID)
        }
          // If user clicks the star then hide the skip button.
        self.skipButton.isHidden = true
    }
    
    // MARK: Private Methods
    
    private func submitRating(ratings:Int, bookingID:String, qid:Int) {
        BBQActivityIndicator.showIndicator(view: self.view)
        ratingsViewModel.submitBookingFeedback(rating: ratings, bookingID: bookingID, qid: qid) {_ in
            BBQActivityIndicator.hideIndicator(from: self.view)
        }
    }
    
    private func openAppStoreNavigationBottomSheet() {
        let configuration = BottomSheetConfiguration(animationDuration: 0.4,
                                                     minimumTopSpacing: UIScreen.main.bounds.size.height - 200.0)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        let cancellationViewController = FeedBackViewController()
        cancellationViewController.feedbackDelegate = self
        cancellationViewController.feedbackBottomSheet = self.currentBottomSheet!
        cancellationViewController.view.maskByRoundingCorners(cornerRadius: 10.0,
                                                              maskedCorners: CornerMask.topCornersMask)
        bottomSheetController.present(cancellationViewController, on: self)
    }
    
    private func openRatingReasonBottonSheet(rating:Int,bookingId:String) {
        let reasonsVC = UIStoryboard.loadRatingsViewController()

        reasonsVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        reasonsVC.modalPresentationStyle = .overFullScreen
        reasonsVC.rating = rating
        reasonsVC.ratingDelegate = self
        reasonsVC.ratingBottomsheet = self.currentBottomSheet!
        reasonsVC.bookingID = bookingId
        let configuration = BottomSheetConfiguration(animationDuration: 0.4,
                                                     minimumTopSpacing: UIScreen.main.bounds.size.height - reasonsVC.view.frame.height)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        bottomSheetController.present(reasonsVC, on: self)
    }
}

extension BBQUsersRatingCell:BBQRatingProtocol{
    func didRatingDonePressed() {
//        currentBottomSheet!.dismissBottomSheet(on: self)
        
        if let presentingController = self.currentBottomSheet{
            presentingController.dismissBottomSheet(on: self)
        }
          self.dismiss(animated: false, completion: nil)
        
        self.ratingcellDelegate!.ratingDismissed()

//        if let presentingController = self.currentBottomSheet{
//            presentingController.dismissBottomSheet(on: self)
////            self.dismiss(animated: true, completion: nil)
//        }
    }
    func didTapOutsideSheet() {
        currentBottomSheet!.dismissBottomSheet(on: self)
    }
    
}
extension BBQUsersRatingCell:FeedbackProtocol{
    func didOkPressed() {
        if let presentingController = self.currentBottomSheet{
            presentingController.dismissBottomSheet(on: self)
        }
        self.dismiss(animated: false, completion: nil)
        self.ratingcellDelegate!.ratingDismissed()


    }
    
    func didNotNowPressed() {
        if let presentingController = self.currentBottomSheet{
            presentingController.dismissBottomSheet(on: self)
        }
//        currentBottomSheet!.dismissBottomSheet(on: self)
        self.dismiss(animated: false, completion: nil)
        self.ratingcellDelegate!.ratingDismissed()


    }
    
    
}
