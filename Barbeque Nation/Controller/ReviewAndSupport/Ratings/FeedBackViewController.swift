//
 //  Created by Abhijit on 14/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified FeedBackViewController.swift
 //

import UIKit
import StoreKit

protocol FeedbackProtocol: AnyObject {
    func didOkPressed()
    func didNotNowPressed()
}

class FeedBackViewController: UIViewController {

    @IBOutlet weak var okayButton: UIButton!
    @IBOutlet weak var notNowButton: UIButton!
    @IBOutlet weak var ratingQuestionLabel: UILabel!
    @IBOutlet weak var thankyouTitleLabel: UILabel!
    var feedbackBottomSheet = BottomSheetController()

    weak var feedbackDelegate: FeedbackProtocol? = nil

    // MARK: Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.setupUI()
    }
    
    // MARK: Setup Methods
    
    func setupUI() {
        
        self.ratingQuestionLabel.text = kRatingAppstore
        self.thankyouTitleLabel.text = kRatingThankYouTitle
        
        self.okayButton.setTitle(kButtonOkay, for: .normal)
        self.notNowButton.setTitle(kButtonNotNow, for: .normal)

        
        self.okayButton.roundCorners(cornerRadius: 12,
                                     borderColor: #colorLiteral(red: 0.2117647059, green: 0.2509803922, blue: 0.2666666667, alpha: 0.8),
                                     borderWidth: 1.0)
        self.notNowButton.roundCorners(cornerRadius: 12,
                                       borderColor: #colorLiteral(red: 0.2117647059, green: 0.2509803922, blue: 0.2666666667, alpha: 0.8), borderWidth: 1.0)
        
    }
    
    // MARK: IBAction Methods
    
    @IBAction func okayButton(_ sender: UIButton) {
        if let url = URL(string: "itms-apps://itunes.apple.com/app/id1080269411"),
            UIApplication.shared.canOpenURL(url)
        {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
            BBQUserDefaults.sharedInstance.isUserRatedBBQApp = true
        }
        self.dismiss(animated: true, completion: {
            self.feedbackDelegate?.didOkPressed()
        })
    }

    @IBAction func notNow(_ sender: UIButton) {
//        self.dismiss(animated: true, completion: {
            self.feedbackDelegate?.didNotNowPressed()
            feedbackBottomSheet.dismissBottomSheet(on: self)
    }
}
