//
 //  Created by Rabindra L on 13/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQHelpSupportFooterCell.swift
 //

import UIKit

protocol BBQHelpSupportFooterCellProtocol: AnyObject {
    func contactUsPressed()
}

class BBQHelpSupportFooterCell: UIView {
    
    @IBOutlet weak var contactUs: UIButton!
    weak var delegate: BBQHelpSupportFooterCellProtocol? = nil
    
    //MARK: Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contactUs.roundCorners(cornerRadius: contactUs.frame.size.height/2, borderColor: .clear, borderWidth: 0.0)
    }
    
    @IBAction func contactUsTapped() {
        if let delegate = self.delegate {
            delegate.contactUsPressed()
        }
    }
}
