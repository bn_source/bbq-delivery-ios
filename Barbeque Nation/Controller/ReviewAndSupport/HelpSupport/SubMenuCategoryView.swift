//
 //  Created by Chandan Singh on 05/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified SubMeuCategoryView.swift
 //

import UIKit

class SubMenuCategoryView: MenuCategoryView {
    
    override func addTag(text:String, tagIndex: Int) {
        //instantiate button
        //you can customize your button here! but make sure everything fit. Default row height is 30.
        let rounderRectButton = RoundedRectButton(withTitle: text)
        rounderRectButton.tag = tagIndex
        rounderRectButton.isSelected = false
        self.tags.append(rounderRectButton)
        
        //process actions
        rounderRectButton.addTarget(self, action: #selector(self.tappedOnButton(sender:)), for: .touchUpInside)
           
        //calculate frame
        rounderRectButton.frame = CGRect(x: rounderRectButton.frame.origin.x,y: rounderRectButton.frame.origin.y , width: rounderRectButton.frame.width + tagCombinedMargin, height: rowHeight - tagVerticalPadding)
        if self.tags.count == 0 {
            rounderRectButton.frame = CGRect(x: hashtagsOffset.left, y: hashtagsOffset.top, width: rounderRectButton.frame.width, height: rounderRectButton.frame.height)
            self.addSubview(rounderRectButton)
        } else {
            rounderRectButton.frame = self.generateFrameAtIndex(index: tags.count-1, rowNumber: &currentRow)
            self.addSubview(rounderRectButton)
        }
    }
    
    @objc override func tappedOnButton(sender: RoundedRectButton) {
        //Check whether the button is already selected. If yes, unselect it and return
        if sender.isSelected {
            sender.isSelected = false
            if let delegate = self.menuDelegate {
                delegate.didTapOnMenuCategoryAt(index: sender.tag)
            }
            return
        }
        
        //Mark the selected one
        let selectedRoundedRectButton = self.tags[sender.tag]
        selectedRoundedRectButton.isSelected = true
        
        //Unmark all other Buttons
        for index in 0..<self.tags.count {
            if index != sender.tag {
                let unselectButton = self.tags[index]
                unselectButton.isSelected = false
            }
        }
        
        if let delegate = self.menuDelegate {
            delegate.didTapOnMenuCategoryAt(index: sender.tag)
        }
    }
}
