/*
 //  BBQHelpSupportViewController.swift
 
 *  Created by Rabindra L on 12/09/19.
 *  Copyright © BARBEQUE NATION HOSPITALITY LIMITED 2019. All rights reserved.
 *  Last modified 12/09/19
 
 * CHANGE LOG
 * —------------------------------------------------------------------------------
 * ID          Bug ID      Author Name          Date                  Description
 * —------------------------------------------------------------------------------
 * 1            NA         Rabindra L           12/09/19        Initial Version
 * 2            1          Rabindra L           19/09/19        API Integration
 */

import UIKit
import MessageUI
import ShimmerSwift

class BBQHelpSupportViewController: BBQBaseViewController, MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var contactUsTopConst: NSLayoutConstraint!
    @IBOutlet weak var contactUsView: UIView!
    @IBOutlet  var tableViewTopConstWithStackTop: NSLayoutConstraint!
    @IBOutlet  var tableViewTopConstWithStackBottom: NSLayoutConstraint!
    @IBOutlet weak var bookingIDLAbel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblUpcomingCount: UILabel!

    @IBOutlet weak var contactUsBtn: UIButton!
    var upcomingReservationsArray : [UpcomingBookingModel] = []
    @IBOutlet weak var upcomingReservationCollectionView:UICollectionView!
    @IBOutlet weak var stackViewForUpcomingReservation: UIStackView!
    var upcomingReservationModel = UpcomingReserevationViewModel()

    private var isUserLoggedOut:Bool = false
    var buttonList = [String]()
    var footerView: BBQHelpSupportFooterCell?
    var lastSelectedRow: Int?
    var faqViewModel = BBQHelpSupportViewModel()
    enum bookingInfoView:CGFloat {
        case show = 15
        case hide = -70
    }
    private var lastSelectedButtonIndex: Int?
    
    lazy var homeViewModel : HomeViewModel = {
        let homeModel = HomeViewModel()
        return homeModel
    }()
    
    var faqList: [QA]? {
        didSet {
            self.tableView.reloadData()
        }
    }

    
    @IBOutlet weak var shimmerSupportView: UIView!
    @IBOutlet weak var shimmerView: UIView!
    var shimmerControl : ShimmeringView?
    var isShimmerRunning : Bool = false

    @IBOutlet weak var segmentControl:DGScrollableSegmentControl!
    
    @IBOutlet weak var mainview: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AnalyticsHelper.shared.triggerEvent(type: .HS01)
        self.shimmerControl = ShimmeringView(frame: shimmerSupportView.bounds)
        registerTableViewCells()
        
        showFAQList()
        
        contactUsView.isHidden = true
        contactUsView.layer.cornerRadius = 13
        contactUsView.layer.shadowColor = UIColor.gray.cgColor
        contactUsView.layer.shadowOpacity = 0.25
        contactUsView.layer.shadowRadius = 10
        contactUsView.layer.shadowOffset = CGSize(width: 5, height: 5)
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tapRecognizer.cancelsTouchesInView = false
        self.tableView.addGestureRecognizer(tapRecognizer)

        //  collectionView.tag = Constants.TagValues.voucherCellTag
          mainview.layer.masksToBounds = false
          mainview.layer.shadowColor = UIColor.hexStringToUIColor(hex: "#55606D1A").cgColor
          mainview.layer.shadowOffset = CGSize(width: 0, height: 3)
          mainview.layer.shadowRadius = 1
          mainview.layer.shadowOpacity = 0.2
          
          segmentControl.clipsToBounds = true
          segmentControl.layer.cornerRadius = 12
          segmentControl.layer.maskedCorners = [ .layerMinXMinYCorner, .layerMinXMaxYCorner]
          segmentControl.numbersOfDevider = 3.0
          segmentControl.isFirstItemAutoSelected = false
        
          if #available(iOS 13.0, *) {
              segmentControl.layer.cornerCurve = .continuous
          } else {
              // Fallback on earlier versions
          }
        getTheUpcomingReservation()

       // upcomingReservationCollectionView.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.screenName(name: .Help_Support_Screen)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @objc fileprivate func handleTap(_ sender: UITapGestureRecognizer) {
        contactUsView.isHidden = true
    }
    
    private func setupMenuHeader() {
        headerView.nameLabel.isHidden = true
    }
    
    func setupCoinCell(){
        
        //On click from Mybenifits more text on smile, opne row with smile club information
     
        var indexFound = -1
        //find out the section for smileClub question
        indexFound = self.faqViewModel.faqDataModel?.tags?.firstIndex(where: { question in
             question.tagName?.lowercased().contains("smile") == true
         }) ?? -1
        
        
//        var tempmodel   = faqList?[indexFound]
//
//        if tempmodel != nil {
//
//            //To expand details of found row
//            tempmodel?.isExpanded = true
//
//            faqList?.remove(at: indexFound)
//            faqList?.insert(tempmodel!, at: indexFound)
//
//        }
//
        if indexFound != -1 {
            
            //  item.setTitleColor(.white, for: .normal)
            faqViewModel.faqDataModel?.tags?[indexFound].isSelected =  true
            segmentControl.reload()
            segmentControl.scroll(toIndex: indexFound )
            didTapOnMenuCategoryAt(index: indexFound)
        }
        
        //Reset the accessibility once scrolled
        self.accessibilityLabel = ""
    }
    
    func showFAQList() {
        showShimmering()
        faqViewModel.getQuestionList() {_ in
            for button in self.faqViewModel.faqDataModel?.tags ?? [] {
                self.buttonList.append(button.tagName ?? "")
            }
            self.faqList = self.faqViewModel.faqDataModel?.qa
            self.segmentControl.delegate = self
            self.segmentControl.datasource = self
            self.stopShimmering()
            self.segmentControl.reload()
            
            if self.accessibilityLabel == "MyBenifits" {
                self.setupCoinCell()
            }
            
        }
    }
    
    
    func registerTableViewCells() {
        tableView.estimatedRowHeight = 74.0
        let expandableCell = UINib(nibName:Constants.CellIdentifiers.questionnarieCell, bundle:nil)
        let noResultCell = UINib(nibName:Constants.CellIdentifiers.noResult, bundle:nil)
        self.tableView.register(expandableCell, forCellReuseIdentifier: Constants.CellIdentifiers.questionnarieCell)
        self.tableView.register(noResultCell, forCellReuseIdentifier: Constants.CellIdentifiers.noResult)
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

    @objc func contactUsTapped() {
//        do {
//            let config = YMConfig(botId: "x1626071102338")
//            config.payload = ["name": "BBQ", "device-type": "mobile", "mobile": BBQUserDefaults.sharedInstance.customerId, "custname": BBQUserDefaults.sharedInstance.UserName]
//            YMChat.shared.config = config
//            YMChat.shared.startChatbot(on: self)
//        } catch {
//            print("Error occured while loading chatbot \(error)")
//        }
        AnalyticsHelper.shared.triggerEvent(type: .HS04)
        let globalPoint = self.tableView?.convert(footerView!.frame.origin, to: nil)
        contactUsTopConst.constant = -globalPoint!.y + 86
        tableView.addSubview(contactUsView)
        contactUsView.clipsToBounds = true
        contactUsView.isHidden = !contactUsView.isHidden
    }
    
    @IBAction func callNumber(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .HS04A)
        contactUsView.isHidden = true
        let url:NSURL = NSURL(string: Constants.values.customerCareNumber)!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url as URL)
        }
    }
    
    @IBAction func sendEmail(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .HS04B)
        contactUsView.isHidden = true
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([Constants.values.customerCareEmail])
            mail.setMessageBody("", isHTML: true)
            
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        contactUsView.isHidden = true
    }
}

extension BBQHelpSupportViewController: BBQHelpSupportFooterCellProtocol {
    
    @IBAction func  contactBtnPresses(_ sender :UIButton){
        
        contactUsPressed()
    }
    func contactUsPressed() {

        AnalyticsHelper.shared.triggerEvent(type: .HS04)
        let globalPoint = self.view?.convert(contactUsBtn!.frame.origin, to: nil)
        contactUsTopConst.constant = -globalPoint!.y  + ( contactUsBtn.frame.size.height + 50 )
        self.view.addSubview(contactUsView)
        contactUsView.clipsToBounds = true
        contactUsView.isHidden = !contactUsView.isHidden
    }
}


extension BBQHelpSupportViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if faqList?.count ?? 0 > 0 {
            return faqList!.count
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if faqList?.count ?? 0 > 0 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.questionnarieCell) as? BBQQuestionnarieCell {
                cell.detailsLabel.isHidden = true
                let isExpanded = faqList![indexPath.section].isExpanded
                cell.titleLabel.text = faqList![indexPath.section].Q
                
                if isExpanded {
                  //  cell.arrowImage.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                    cell.arrowImage.image = UIImage(named: "HelpAndSupportUpArrow")
                    cell.setTextAsAtrributedText(textValue: faqList?[indexPath.section].A)
                    cell.textView.isHidden = false
                    cell.subView.backgroundColor = UIColor.hexStringToUIColor(hex: "F9F9F9")
                 //   cell.detailsLabel.text = faqList?[indexPath.section].A
                } else {
                   // cell.arrowImage.transform = CGAffineTransform(rotationAngle: 0)
                    cell.arrowImage.image = UIImage(named: "HelpAndSupportDownArrow")
                    cell.subView.backgroundColor = UIColor.hexStringToUIColor(hex: "F5F5F5")
                    cell.detailsLabel.text = ""
                    cell.textView.isHidden = true
                }
                
                return cell
            }
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.noResult) as? NoResultTableViewCell {
                return cell
            }
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //Toggle Section
        
        if faqList?.count ?? 0 < indexPath.section{
            return
        }
        
        self.faqList![indexPath.section].isExpanded = !self.faqList![indexPath.section].isExpanded
        self.tableView.reloadSections(NSIndexSet(index: indexPath.section) as IndexSet, with: .none)
        
        if self.faqList![indexPath.section].isExpanded {
            AnalyticsHelper.shared.triggerEvent(type: .HS03)
            self.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
        }
        
        //Close last selected row
        if let lastSelectedRow = lastSelectedRow, self.lastSelectedRow != indexPath.section {
            self.faqList![lastSelectedRow].isExpanded = false
            self.tableView.reloadSections(NSIndexSet(index: lastSelectedRow) as IndexSet, with: .none)
        }
        
        self.lastSelectedRow = indexPath.section
    }

}
extension BBQHelpSupportViewController :DGScrollableSegmentControlDataSource,DGScrollableSegmentControlDelegate {
   
    func numbersOfItem( _ sender : DGScrollableSegmentControl) -> Int {
        return self.faqViewModel.faqDataModel?.tags?.count ?? 0
        
    }
    
    func didTapOnMenuCategoryAt(index tagIndex: Int) {
        AnalyticsHelper.shared.triggerEvent(type: .HS02)
        if tagIndex == self.lastSelectedButtonIndex {
            //Same button is selected, load all the data.
            self.lastSelectedButtonIndex = nil
            self.lastSelectedRow = nil
            self.faqList = self.faqViewModel.faqDataModel?.qa
            return
        }
        
        if let data = self.faqViewModel.faqDataModel, let tags = data.tags, tags.count > 0 {
            let tagIndexForServiceCall = tags[tagIndex].tagId! as NSString
            faqViewModel.getQuestionListById(id: [tagIndexForServiceCall.integerValue]) {_ in
                self.lastSelectedRow = nil
                self.faqList = self.faqViewModel.faqByIdDataModel?.qa
            }
        }
        self.lastSelectedButtonIndex = tagIndex
    }
    func itemfor(_ index: Int ,  sender : DGScrollableSegmentControl ) -> DGItem {
        
        let item = DGItem()
        
        item.setTitle(faqViewModel.faqDataModel?.tags?[index].tagName, for: .normal)
        //  item.setTitleColor(.white, for: .normal)
        item.isSelected = faqViewModel.faqDataModel?.tags?[index].isSelected ?? false
        item.tag = Int(faqViewModel.faqDataModel?.tags?[index].tagId ?? "0") ?? 0
        
        return item
        
        
    }
    func didSelect(_ item: DGItem, atIndex index: Int ,  sender : DGScrollableSegmentControl) {
        
        print("Selected Index \(index)")
        //self.segmentControl.reload()
        
        didTapOnMenuCategoryAt(index: item.tag)

        
    }
}

extension BBQHelpSupportViewController: UICollectionViewDataSource {
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
           
                if(self.upcomingReservationsArray.count>5){
                    return 5
                }else{
                    return self.upcomingReservationsArray.count
                }
            }
        
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
                let reservationCell:BBQUpcomingReservationCell = collectionView.dequeueReusableCell(withReuseIdentifier:Constants.CellIdentifiers.upcomingReservationCell, for: indexPath) as! BBQUpcomingReservationCell
                if (self.upcomingReservationsArray.count>0){
                    if(indexPath.row < 5){
                        let reveremodel:UpcomingBookingModel = upcomingReservationsArray[indexPath.row]
                        reservationCell.setUpcomingReservationCellvalues(reveremodel, delegate: upcomingReservationModel)
                    }
                }
                return reservationCell
            
        }
        
    }

extension BBQHelpSupportViewController:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 150)

    }
}

extension BBQHelpSupportViewController {
    
    
    // MARK: - getTheUpcomingReservation called to get the 5 upcoming reservation of the logged in user
    func getTheUpcomingReservation(){
        
        if BBQUserDefaults.sharedInstance.accessToken != "" {
            if !self.isUserLoggedOut{
              //  self.showShimmering()

            }
            
            //self.upcomingArray.removeAll()
            self.homeViewModel.getUpcomingReservations { (isSuccess) in
                if !self.isUserLoggedOut{
                   // self.stopShimmering()
                }

                if isSuccess {
                    self.upcomingReservationsArray = self.homeViewModel.getReservationsData()
                    DispatchQueue.main.async {
                        print(self.upcomingReservationsArray)
                        self.lblUpcomingCount.text = String(format: "%li of %li", 1, self.upcomingReservationsArray.count)

                        if self.upcomingReservationsArray.count > 5{
                            
                            self.lblUpcomingCount.text = String(format: "%li of %li", 1, 5)
                        }
                        
                        if(self.upcomingReservationsArray.count>0){
                            AnalyticsHelper.shared.triggerEvent(type: .H14)
                            self.upcomingReservationCollectionView.isHidden = false
                            self.stackViewForUpcomingReservation.isHidden = false
                            
                            self.tableViewTopConstWithStackTop.isActive = false
                            self.tableViewTopConstWithStackTop.priority = .defaultLow
                            
                            self.tableViewTopConstWithStackBottom.isActive = true
                            self.tableViewTopConstWithStackBottom.priority = .defaultHigh


                        }else{
                            self.upcomingReservationCollectionView.isHidden = true
                            self.stackViewForUpcomingReservation.isHidden = true
                            self.tableViewTopConstWithStackBottom.isActive = false
                            self.tableViewTopConstWithStackBottom.priority = .defaultLow
                            self.tableViewTopConstWithStackTop.isActive = true
                            self.tableViewTopConstWithStackTop.priority = .defaultHigh

                         

                            self.viewDidLayoutSubviews()
                        }
                        self.upcomingReservationCollectionView .reloadData()
                        
                       
                    }
                }else{
                    self.upcomingReservationCollectionView.isHidden = true
                    self.stackViewForUpcomingReservation.isHidden = true
                    self.tableViewTopConstWithStackBottom.isActive = false
                    self.tableViewTopConstWithStackBottom.priority = .defaultLow
                    self.tableViewTopConstWithStackTop.isActive = true
                    self.tableViewTopConstWithStackTop.priority = .defaultHigh
                    self.viewDidLayoutSubviews()
                    
                }
            }
        }else{
            // not a logged in user , hide upcoming reservation screen
            self.upcomingReservationCollectionView.isHidden = true
            self.stackViewForUpcomingReservation.isHidden = true
            self.viewDidLayoutSubviews()
        }
        
         self.view.layoutIfNeeded()
    }
}

extension BBQHelpSupportViewController{
    // MARK: Shimmer Methods
  
    private func showShimmering() {
        self.shimmerSupportView.isHidden = false
        
        if let shimmer = self.shimmerControl {
            self.shimmerSupportView.addSubview(shimmer)
            shimmer.contentView = shimmerView
            shimmer.shimmerAnimationOpacity = 0.2
            shimmer.shimmerSpeed = 500.00
            shimmer.isHidden = false
            shimmer.isShimmering = true
            self.isShimmerRunning = true
        }
    }
    
    private func stopShimmering() {
        self.shimmerSupportView.isHidden = true
        
        if let shimmer = self.shimmerControl {
            shimmer.isShimmering = false
            shimmer.isHidden = true
            self.isShimmerRunning = false
        }
    }
}
