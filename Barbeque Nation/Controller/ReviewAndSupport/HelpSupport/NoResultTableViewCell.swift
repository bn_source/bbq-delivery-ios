//
 //  Created by Abhijit on 04/11/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified NoResultTableViewCell.swift
 //

import UIKit

class NoResultTableViewCell: UITableViewCell {
    
    @IBOutlet weak var noResultLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.noResultLabel.text = "No results found"
    }
}
