/*
 //  BBQHelpSupportViewController.swift
 
 *  Created by Rabindra L on 12/09/19.
 *  Copyright © BARBEQUE NATION HOSPITALITY LIMITED 2019. All rights reserved.
 *  Last modified 12/09/19
 
 * CHANGE LOG
 * —------------------------------------------------------------------------------
 * ID          Bug ID      Author Name          Date                  Description
 * —------------------------------------------------------------------------------
 * 1            NA         Rabindra L           12/09/19        Initial Version
 * 2            1          Rabindra L           19/09/19        API Integration
 */

import UIKit
import MessageUI
//import YMChat

class BBQHelpSupportViewController: BBQBaseViewController, MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var contactUsTopConst: NSLayoutConstraint!
    @IBOutlet weak var contactUsView: UIView!
    @IBOutlet weak var tableViewTopConst: NSLayoutConstraint!
    @IBOutlet weak var topSeparatorTopConst: NSLayoutConstraint!
    @IBOutlet weak var bookingIDLAbel: UILabel!
    @IBOutlet weak var reservationView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var menuCategoryView: SubMenuCategoryView!
    
    var buttonList = [String]()
    var footerView: BBQHelpSupportFooterCell?
    var lastSelectedRow: Int?
    var faqViewModel = BBQHelpSupportViewModel()
    enum bookingInfoView:CGFloat {
        case show = 15
        case hide = -70
    }
    private var lastSelectedButtonIndex: Int?
    
    lazy var homeViewModel : HomeViewModel = {
        let homeModel = HomeViewModel()
        return homeModel
    }()
    
    var faqList: [QA]? {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AnalyticsHelper.shared.triggerEvent(type: .HS01)
        topSeparatorTopConst.constant = bookingInfoView.hide.rawValue
        registerTableViewCells()
        
        menuCategoryView.menuDelegate = self
        reservationView.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
        setupMenuHeader()
        showFAQList()
        showLatestBooking()
        
        contactUsView.isHidden = true
        contactUsView.layer.cornerRadius = 13
        contactUsView.layer.shadowColor = UIColor.gray.cgColor
        contactUsView.layer.shadowOpacity = 0.25
        contactUsView.layer.shadowRadius = 10
        contactUsView.layer.shadowOffset = CGSize(width: 5, height: 5)
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tapRecognizer.cancelsTouchesInView = false
        self.tableView.addGestureRecognizer(tapRecognizer)
        
        menuCategoryView.showsVerticalScrollIndicator = true
        menuCategoryView.showsHorizontalScrollIndicator = true

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.screenName(name: .Help_Support_Screen)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //Everytime when view appears, scrollindicator will flash to notift its scrollable
        self.menuCategoryView.flashScrollIndicators()
    }
    
    @objc fileprivate func handleTap(_ sender: UITapGestureRecognizer) {
        contactUsView.isHidden = true
    }
    
    private func setupMenuHeader() {
        headerView.nameLabel.isHidden = true
    }
    
    func showFAQList() {
        UIUtils.showLoader()
        faqViewModel.getQuestionList() {_ in
            for button in self.faqViewModel.faqDataModel?.tags ?? [] {
                self.buttonList.append(button.tagName ?? "")
            }
            self.setupMenuCategoryButtonsForMenu(index: 0)
            self.faqList = self.faqViewModel.faqDataModel?.qa
            UIUtils.hideLoader()
        }
    }
    
    func showLatestBooking() {
        if BBQUserDefaults.sharedInstance.accessToken != "" {
            self.homeViewModel.getUpcomingReservations { (isSuccess) in
                if self.homeViewModel.getReservationsData().count > 0 {
                    let data = self.homeViewModel.getReservationsData()
                    let booking = data[0]
                    self.bookingIDLAbel.text = "#\(booking.bookingId ?? "")"
                    self.topSeparatorTopConst.constant = bookingInfoView.show.rawValue
                }
            }
        }
    }
    
    func registerTableViewCells() {
        tableView.estimatedRowHeight = 74.0
        let expandableCell = UINib(nibName:Constants.CellIdentifiers.questionnarieCell, bundle:nil)
        let noResultCell = UINib(nibName:Constants.CellIdentifiers.noResult, bundle:nil)
        self.tableView.register(expandableCell, forCellReuseIdentifier: Constants.CellIdentifiers.questionnarieCell)
        self.tableView.register(noResultCell, forCellReuseIdentifier: Constants.CellIdentifiers.noResult)
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    private func setupMenuCategoryButtonsForMenu(index menuIndex: Int) {
        menuCategoryView.reset()
        
        for (index, text) in buttonList.enumerated() {
            menuCategoryView.addTag(text: text, tagIndex: index)
        }
    }
    
    @objc func contactUsTapped() {
//        do {
//            let config = YMConfig(botId: "x1234567890")
//            YMChat.shared.config = config
//            try YMChat.shared.startChatbot(on: self)
//        } catch {
//            print("Error occured while loading chatbot \(error)")
//        }
        AnalyticsHelper.shared.triggerEvent(type: .HS04)
        let globalPoint = self.tableView?.convert(footerView!.frame.origin, to: nil)
        contactUsTopConst.constant = -globalPoint!.y + 86
        tableView.addSubview(contactUsView)
        contactUsView.clipsToBounds = true
        contactUsView.isHidden = !contactUsView.isHidden
    }
    
    @IBAction func callNumber(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .HS04A)
        contactUsView.isHidden = true
        let url:NSURL = NSURL(string: Constants.values.customerCareNumber)!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url as URL)
        }
    }
    
    @IBAction func sendEmail(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .HS04B)
        contactUsView.isHidden = true
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([Constants.values.customerCareEmail])
            mail.setMessageBody("", isHTML: true)
            
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        contactUsView.isHidden = true
    }
}

extension BBQHelpSupportViewController: BBQHelpSupportFooterCellProtocol {
    func contactUsPressed() {
//        let config = YMConfig(botId: "x1609740331340")
//        config.enableHistory = true
//        config.statusBarColor = .theme
//        config.enableSpeech = true
//        config.payload = ["UserState": "Anonymous"]
//        YMChat.shared.config = config
//        YMChat.shared.enableLogging = true
//        YMChat.shared.startChatbot()
//        YMChat.shared.delegate = self
        AnalyticsHelper.shared.triggerEvent(type: .HS04)
        let globalPoint = self.tableView?.convert(footerView!.frame.origin, to: nil)
        contactUsTopConst.constant = -globalPoint!.y + 86
        tableView.addSubview(contactUsView)
        contactUsView.clipsToBounds = true
        contactUsView.isHidden = !contactUsView.isHidden
    }
}

//extension BBQHelpSupportViewController: YMChatDelegate{
//    func onEventFromBot(response: YMBotEventResponse) {
//
//    }
//
//}

extension BBQHelpSupportViewController: MenuCategoryViewProtocol {
    func didTapOnMenuCategoryAt(index tagIndex: Int) {
        AnalyticsHelper.shared.triggerEvent(type: .HS02)
        if tagIndex == self.lastSelectedButtonIndex {
            //Same button is selected, load all the data.
            self.lastSelectedButtonIndex = nil
            self.lastSelectedRow = nil
            self.faqList = self.faqViewModel.faqDataModel?.qa
            return
        }
        
        if let data = self.faqViewModel.faqDataModel, let tags = data.tags, tags.count > 0 {
            let tagIndexForServiceCall = tags[tagIndex].tagId! as NSString
            faqViewModel.getQuestionListById(id: [tagIndexForServiceCall.integerValue]) {_ in
                self.lastSelectedRow = nil
                self.faqList = self.faqViewModel.faqByIdDataModel?.qa
            }
        }
        self.lastSelectedButtonIndex = tagIndex
    }
}

extension BBQHelpSupportViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if faqList?.count ?? 0 > 0 {
            return faqList!.count
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if faqList?.count ?? 0 > 0 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.questionnarieCell) as? BBQQuestionnarieCell {
                
                let isExpanded = faqList![indexPath.section].isExpanded
                cell.titleLabel.text = faqList![indexPath.section].Q
                
                if isExpanded {
                    cell.arrowImage.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                    cell.setTextAsAtrributedText(textValue: faqList?[indexPath.section].A)
                    cell.textView.isHidden = false
                 //   cell.detailsLabel.text = faqList?[indexPath.section].A
                } else {
                    cell.arrowImage.transform = CGAffineTransform(rotationAngle: 0)
                    cell.detailsLabel.text = ""
                    cell.textView.isHidden = true
                }
                
                return cell
            }
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.noResult) as? NoResultTableViewCell {
                return cell
            }
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //Toggle Section
        
        if faqList?.count ?? 0 < indexPath.section{
            return
        }
        
        self.faqList![indexPath.section].isExpanded = !self.faqList![indexPath.section].isExpanded
        self.tableView.reloadSections(NSIndexSet(index: indexPath.section) as IndexSet, with: .none)
        
        if self.faqList![indexPath.section].isExpanded {
            AnalyticsHelper.shared.triggerEvent(type: .HS03)
            self.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
        }
        
        //Close last selected row
        if let lastSelectedRow = lastSelectedRow, self.lastSelectedRow != indexPath.section {
            self.faqList![lastSelectedRow].isExpanded = false
            self.tableView.reloadSections(NSIndexSet(index: lastSelectedRow) as IndexSet, with: .none)
        }
        
        self.lastSelectedRow = indexPath.section
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        var isLastSection = false
        if (section == (faqList?.count ?? 0) - 1) || self.faqList == nil {
            isLastSection = true
        }
        
        if isLastSection {
            return 129.0
        }
        return 1.0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        var isLastSection = false
        if (section == (faqList?.count ?? 0) - 1) || self.faqList == nil {
            isLastSection = true
        }
        
        if isLastSection {
            self.footerView = UINib(nibName: "BBQHelpSupportFooterCell", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? BBQHelpSupportFooterCell
            
            self.footerView?.delegate = self
            return self.footerView
        }
        
        return nil
    }
}
