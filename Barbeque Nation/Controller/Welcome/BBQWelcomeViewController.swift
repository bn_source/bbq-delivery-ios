/*
 *  Created by Nischitha on 16/09/19.
 *  Copyright © BARBEQUE NATION HOSPITALITY LIMITED 2019. All rights reserved.
 
 * CHANGE LOG
 * —------------------------------------------------------------------------------
 * ID          Bug ID      Author Name          Date                  Description
 * —------------------------------------------------------------------------------
 * 1            NA         Nischitha         16/09/19       Initial Version
 * 2            NA         Nischitha         1/10/19        Review comments
 */

import UIKit


struct WelcomeCollectionViewModel {
    var imageName: String?
    var titleText: String?
    var detailedText: String?
}

class BBQWelcomeViewController: UIViewController {
    
//    //MARK:- Properties
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var containerView: UIView!
    
    var welcomePageViewController: BBQWelcomePageViewController? {
        didSet {
            welcomePageViewController?.welcomeDelegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageControl.addTarget(self, action: #selector(didChangePageControlValue), for: .valueChanged)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let BBQWelcomePageViewController = segue.destination as? BBQWelcomePageViewController{
            self.welcomePageViewController = BBQWelcomePageViewController
        }
    }
        
    @IBAction func didTapNextButton(_ sender: Any) {
        //welcomePageViewController?.scrollToNextViewController()
        goToLoginPage()
    }
    
    /**
     Fired when the user taps on the pageControl to change its current page.
     */
    @objc func didChangePageControlValue() {
        welcomePageViewController?.scrollToViewController(index: pageControl.currentPage)
    }
    
    @IBAction func skipLogin(_ sender : UIButton){
        
        //when user pressed Guest button, Allow to go home page without login
        AnalyticsHelper.shared.triggerEvent(type: .A07)
        goToHomePage()
    }
}

extension BBQWelcomeViewController: WelcomePageViewControllerDelegate {
  
    
   
    
    func welcomePageViewController(welcomePageViewController: BBQWelcomePageViewController,
        didUpdatePageCount count: Int) {
        pageControl.numberOfPages = count
    }
    
    func welcomePageViewController(welcomePageViewController: BBQWelcomePageViewController,
        didUpdatePageIndex index: Int) {
        pageControl.currentPage = index
    }
    
}
