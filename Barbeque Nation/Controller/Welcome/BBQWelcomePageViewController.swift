//
 //  Created by Arpana on 15/02/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified BBQWelcomePageViewController.swift
 //

import UIKit

class BBQWelcomePageViewController: UIPageViewController {

        
        weak var welcomeDelegate: WelcomePageViewControllerDelegate?
        
        private(set) lazy var orderedViewControllers: [UIViewController] = {
            // The view controllers will be shown in this order
            return [self.welcomeFlowViewController("Second"),
                self.welcomeFlowViewController("First"),
                self.welcomeFlowViewController("Third")]
        }()
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            dataSource = self
            delegate = self
            
            if let initialViewController = orderedViewControllers.first {
                scrollToViewController(viewController: initialViewController)
            }
            
            welcomeDelegate?.welcomePageViewController(welcomePageViewController: self, didUpdatePageCount: orderedViewControllers.count)
        }
        
        /**
         Scrolls to the next view controller.
         */
        func scrollToNextViewController() {
            if let visibleViewController = viewControllers?.first,
                let nextViewController = pageViewController(self, viewControllerAfter: visibleViewController) {
                        scrollToViewController(viewController: nextViewController)
            }
        }
        
        /**
         Scrolls to the view controller at the given index. Automatically calculates
         the direction.
         
         - parameter newIndex: the new index to scroll to
         */
        func scrollToViewController(index newIndex: Int) {
            if let firstViewController = viewControllers?.first,
                let currentIndex = orderedViewControllers.firstIndex(of: firstViewController) {
                let direction: UIPageViewController.NavigationDirection = newIndex >= currentIndex ? .forward : .reverse
                    let nextViewController = orderedViewControllers[newIndex]
                    scrollToViewController(viewController: nextViewController, direction: direction)
            }
        }
        
        func welcomeFlowViewController(_ color: String) -> UIViewController {
            return UIStoryboard(name: "Login", bundle: nil) .
                instantiateViewController(withIdentifier: "\(color)ViewController")
        }
        
        /**
         Scrolls to the given 'viewController' page.
         
         - parameter viewController: the view controller to show.
         */
        private func scrollToViewController(viewController: UIViewController,
                                            direction: UIPageViewController.NavigationDirection = .forward) {
            
            DispatchQueue.main.async { [weak self] in
                
                //Fix for 'Duplicate states in queue' crash
                self?.setViewControllers([viewController],
                                   direction: direction,
                                   animated: false,
                                   completion: { (finished) -> Void in
                    // Setting the view controller programmatically does not fire
                    // any delegate methods, so we have to manually notify the
                    // 'tutorialDelegate' of the new index.
                    self?.notifyWelcomePageDelegateOfNewIndex()
                })
            }
            
        }
        
        /**
         Notifies '_tutorialDelegate' that the current page index was updated.
         */
        private func notifyWelcomePageDelegateOfNewIndex() {
            if let firstViewController = viewControllers?.first,
                let index = orderedViewControllers.firstIndex(of: firstViewController) {
                    welcomeDelegate?.welcomePageViewController(welcomePageViewController: self, didUpdatePageIndex: index)
            }
            
        }
        
    }

    // MARK: UIPageViewControllerDataSource

    extension BBQWelcomePageViewController: UIPageViewControllerDataSource {
        
        func pageViewController(_ pageViewController: UIPageViewController,
                                viewControllerBefore viewController: UIViewController) -> UIViewController? {
                guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else {
                    return nil
                }
                
                let previousIndex = viewControllerIndex - 1
                
                // User is on the first view controller and swiped left to loop to
                // the last view controller.
                guard previousIndex >= 0 else {
                    return orderedViewControllers.last
                }
                
                guard orderedViewControllers.count > previousIndex else {
                    return nil
                }
                
                return orderedViewControllers[previousIndex]
        }

        func pageViewController(_ pageViewController: UIPageViewController,
                                viewControllerAfter viewController: UIViewController) -> UIViewController? {
                guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else {
                    return nil
                }
                
                let nextIndex = viewControllerIndex + 1
                let orderedViewControllersCount = orderedViewControllers.count
                
                // User is on the last view controller and swiped right to loop to
                // the first view controller.
                guard orderedViewControllersCount != nextIndex else {
                    return orderedViewControllers.first
                }
                
                guard orderedViewControllersCount > nextIndex else {
                    return nil
                }
                
                return orderedViewControllers[nextIndex]
        }
        
    }

    extension BBQWelcomePageViewController: UIPageViewControllerDelegate {
        
        func pageViewController(_ pageViewController: UIPageViewController,
            didFinishAnimating finished: Bool,
            previousViewControllers: [UIViewController],
            transitionCompleted completed: Bool) {
            notifyWelcomePageDelegateOfNewIndex()
        }
        
    }

    protocol WelcomePageViewControllerDelegate: class {
        
        /**
         Called when the number of pages is updated.
         
         - parameter welcomePageViewController: the BBQWelcomeViewControllerinstance
         - parameter count: the total number of pages.
         */
        func welcomePageViewController(welcomePageViewController: BBQWelcomePageViewController,
            didUpdatePageCount count: Int)
        
        /**
         Called when the current index is updated.
         
         - parameter welcomePageViewController: the BBQWelcomeViewControllerinstance
         - parameter index: the index of the currently visible page.
         */
        func welcomePageViewController(welcomePageViewController: BBQWelcomePageViewController,
            didUpdatePageIndex index: Int)
        
    }
