/*
 *  Created by Nischitha on 17/09/19.
 *  Copyright © BARBEQUE NATION HOSPITALITY LIMITED 2019. All rights reserved.
 
 * CHANGE LOG
 * —------------------------------------------------------------------------------
 * ID          Bug ID      Author Name          Date                  Description
 * —------------------------------------------------------------------------------
 * 1            NA         Nischitha         17/09/19       Initial Version
 * 2            NA         Nischitha         1/10/19        Review comments
 */

import UIKit
import AVFoundation
import AVKit

class BBQWelcomeCollectionViewCell: UICollectionViewCell {
    
    //MARK:- IBoutlets
    @IBOutlet weak var welcomeImageView: UIImageView!
  //  @IBOutlet weak var welcomeView: UIView!
    //@IBOutlet weak var playerActivityIndicator: UIActivityIndicatorView!
    //MARK:- Properties
//    private var playerLayer: AVPlayerLayer? = nil
//    static var player :AVPlayer?
//    var isVideo:Bool = false
//
    //MARK:- UI methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
     //   welcomeView.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
        welcomeImageView.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
        
    }
//    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
//        if keyPath == "currentItem.loadedTimeRanges" {
//             playerActivityIndicator.stopAnimating()
//        }
//    }
//    override public func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
//        if keyPath == "timeControlStatus", let change = change, let newValue = change[NSKeyValueChangeKey.newKey] as? Int, let oldValue = change[NSKeyValueChangeKey.oldKey] as? Int {
//            let oldStatus = AVPlayer.TimeControlStatus(rawValue: oldValue)
//            let newStatus = AVPlayer.TimeControlStatus(rawValue: newValue)
//            if newStatus != oldStatus {
//                DispatchQueue.main.async {[weak self] in
//                    if newStatus == .playing || newStatus == .paused {
//                        self?.playerActivityIndicator.stopAnimating()
//                    } else {
//                        self?.playerActivityIndicator.startAnimating()
//                    }
//                }
//            }
//        }
//    }
//    func configureCellWithVideo(url: String) {
//        let url = URL(string: url)
//        if let videoUrl = url {
//            BBQWelcomeCollectionViewCell.player = AVPlayer(url: videoUrl)
//            if self.playerLayer != nil {
//                self.playerLayer?.removeFromSuperlayer()
//                self.playerLayer = nil
//            }
//            self.playerLayer = AVPlayerLayer(player: BBQWelcomeCollectionViewCell.player)
//            if let layer = self.playerLayer {
//                let bounds = UIScreen.main.bounds
//                let welcomeViewRect = self.welcomeView.frame
//                let rect = CGRect(x: 0, y: welcomeViewRect.origin.y, width: bounds.size.width, height: welcomeViewRect.size.height)
//                layer.frame =  rect//self.welcomeView.bounds
//                self.welcomeView.layer.addSublayer(layer)
//                layer.videoGravity = AVLayerVideoGravity.resizeAspectFill;
//                layer.masksToBounds = false
//                BBQWelcomeCollectionViewCell.player?.play()
//                playerActivityIndicator.startAnimating()
//
//                //Changes by Arpana, to fix firebase crash
//                //message was received but not handled. Key path: currentItem.loadedTimeRanges Observed object: <AVPlayer: 0x28071cdf0> Change: { kind = 1; new = ( "CMTimeRange: {{0/1 = 0.000}, {151680/90000 = 1.685}}" ); } Context: 0x0
//              //  BBQWelcomeCollectionViewCell.player?.addObserver(self, forKeyPath: "currentItem.loadedTimeRanges", options: .new, context: nil)
//
//
//                BBQWelcomeCollectionViewCell.player?.addObserver(self, forKeyPath: "timeControlStatus", options: [.old, .new], context: nil)
//
//            }
//        }
//    }
    func configureCellWithImage(url:String)  {
       // LazyImageLoad.setImageOnImageViewFromURL(imageView: welcomeImageView, url: url) { (image) in}
        self.welcomeImageView.image = UIImage(named: url)
    }
    
//    func pauseVideo(){
//        BBQWelcomeCollectionViewCell.player?.pause()
//    }
}
