
//  BBQMenuViewController.swift
/*
 *  Created by Abhijit Singh on 20/08/19.
 *  Copyright © BARBEQUE NATION HOSPITALITY LIMITED 2019. All rights reserved.
 *  Last modified 03/09/19
 
 * CHANGE LOG
 * —------------------------------------------------------------------------------
 * ID          Bug ID      Author Name          Date                  Description
 * —------------------------------------------------------------------------------
 *
 */

import UIKit
import AKSideMenu

protocol BBQMenuViewControllerProtocol: AnyObject {
    func postUserAccountProcessing()
    func showPromotionsAndOffers()
    func showBookingScreen()
}

protocol BBQHostMenuDelegate: AnyObject{
    func hideSideMenu()
}

class BBQMenuViewController: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var hamburgerRotatedButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var hamburgerTableView: UITableView!
    @IBOutlet weak var cancelImage: UIImageView!
    
    //MARK: Properties
    weak var delegate: BBQMenuViewControllerProtocol? = nil
    weak var sideMenuDelegate: BBQHostMenuDelegate? = nil
    weak var navigationVC: UINavigationController?
    
    lazy var notificationViewModel : BBQNotificationTokenViewModel = {
        let viewModel = BBQNotificationTokenViewModel()
        return viewModel
    }()
    
    lazy private var hamMenuViewModel: HamMenuViewModel = {
        let viewModel = HamMenuViewModel()
        return viewModel
    }()
    
    var outletInfo = BBQOutletInfoViewModel()
    
    let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.bookATableButton.setTitle(kHamMenuBookATableTitle, for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hamburgerTableView.reloadData()
//
//        menuContainerViewController.selectContentViewController(menuContainerViewController.contentViewControllers[0])
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//
//        menuContainerViewController.selectContentViewController(menuContainerViewController.contentViewControllers[0])
    }
    //MARK: Hamburger Dismiss
    @IBAction func hamburgerRotatedButton(_ sender: UIButton) {
//        self.view.removeFromSuperview()
    }
    
    @IBAction func cancelButton(_ sender: UIButton) {
//        self.view.removeFromSuperview()
        sideMenuDelegate?.hideSideMenu()
    }
    
    @IBAction func bookATablePressed(_ sender: UIButton) {
        if let delegate = self.delegate {
            delegate.showBookingScreen()
        }
//        self.view.removeFromSuperview()
    }
    
    deinit{
        print()
    }
    
    @IBAction func inviteFriendsPressed(_ sender: UIButton) {
        //let textToShare = "Hey! Want to relish the best BBQ's in town. Check out this latest app from Barbeque Nation & earn SMILES when you dine.To download click https://apps.apple.com/in/app/barbeque-nation/id1080269411"
        // BBQShareHandler.shareContent(contentToShare: ShareModel(data: textToShare, type: .Text, attributedText: nil), on: self, isBottomSheetPresenter: true)
        
        // Removed showing toast for facebook as per request.
        //        ToastHandler.showToastWithMessage(message: "Press hold to paste the contents on Facebook")
        _ = Timer.scheduledTimer(timeInterval: 0.0, target: self, selector: #selector(self.shareMethod), userInfo: nil, repeats: false)
    }
    
    @objc func shareMethod() {
        let  textToShare = Constants.App.LoyalitiPage.InviteFriends + Constants.App.LoyalitiPage.referAndEarnShortLink
        let pasteboard = UIPasteboard.general
        pasteboard.string = textToShare
        
        let shareModel = ShareModel(data: textToShare, type: .Text, attributedText: nil)
        BBQShareHandler.shareContent(contentToShare: shareModel, on: self, isBottomSheetPresenter: true)
    }
    
    //MARK: Helper Methods
    func showLoggedOutAlert() {
        //Chandan: If successful message is required after logout, uncomment below line.
        //ToastHandler.showToastWithMessage(message: kHamMenuLogoutSuccessTitle)
        goToLoginPage()
    }
    
    //MARK: Alert for permission of loggingout
    func showLogoutAlert() {
        PopupHandler.showTwoButtonsPopup(title: kLogoutConfirmationMessage,
                                         message: "",
                                         isCentered: true,
                                         on: self,
                                         firstButtonTitle: kNoButton,
                                         firstAction: { },
                                         secondButtonTitle: kYesButton) {
                                            self.logoutClearCache()
                                        }
    }
    
    private func gotoLoginHome(){
        func goToHome() {
            let currentNavigationStack = self.navigationVC?.viewControllers
            
            guard let navigationStack = currentNavigationStack, let lastController = navigationStack.last else {
                return
            }
            if !((lastController.isKind(of: BBQHomeViewController.self))) {
                self.navigationVC?.popViewController(animated: true)
            }
        }
    }
    
    func logoutClearCache() {
        BBQActivityIndicator.showIndicator(view: self.view)
        self.notificationViewModel.deleteNotificationToken(token: BBQUserDefaults.sharedInstance.firebaseToken, deviceType: "IOS"){ (isSuccess) in
            print("Logout: delete token for logged in user",isSuccess)
            self.hamMenuViewModel.logoutUser { [](message, result) in
                if result == true {
                    BBQActivityIndicator.hideIndicator(from: self.view)
                    self.notificationViewModel.postNotificationTokenForGuestUser(token: BBQUserDefaults.sharedInstance.firebaseToken, deviceType: "IOS"){ (isSuccess) in
                        print("Logout:crate token for guest user",isSuccess)
                        BBQUserDefaults.sharedInstance.isGuestUserAPI = true
                        BBQUserDefaults.sharedInstance.firebaseToken = ""
                    }
                    if let appDelegate = self.appDelegate {
                        appDelegate.saveNotificationData(dataArray: [])
                    }
                    //Clear all data
                    let tempBranchId = BBQUserDefaults.sharedInstance.branchIdValue
                    BBQUserDefaults.sharedInstance.clearAppDefaults()
                    BBQUserDefaults.sharedInstance.branchIdValue = tempBranchId
                    //Just update the cart badge count to 0 , when user logs out
                    UIUtils.updateCartTabBadgeCountNew(count: 0)
                    BBQLocationManager.shared.clearAllData()
                    if let delegate = self.delegate {
                        delegate.postUserAccountProcessing()
                    }
                    self.showLoggedOutAlert()
                    
                } else {
                    return
                }
                
            }
        }
    }
}

extension BBQMenuViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: CGRect(x: 20,
                                                  y: 0,
                                                  width: self.hamburgerTableView.frame.width,
                                                  height: 50))
        
        let label =  UILabel.init(frame: headerView.frame)
        label.text =  section == 0 ? "Main Menu" : "Other"
        label.textColor = UIColor.hexStringToUIColor(hex: "F04B24")
        label.font =  UIFont(name: Constants.App.BBQAppFont.ThemeRegular, size: 14.0)
        headerView.addSubview(label)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        hamMenuViewModel.countForSection(section)
    
        //return hamMenuViewModel.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MenuListCell
        cell.lbl.text = hamMenuViewModel.getMenuItemFor(indexPath: indexPath)
        cell.lbl.textColor = .black
        cell.lbl.alpha = 1.0
        cell.lblSubTitle.text = ""
        cell.imgView.image = UIImage.init(named: hamMenuViewModel.getMenuImageFor(indexPath: indexPath))
        cell.lbl.font =  UIFont(name: Constants.App.BBQAppFont.ThemeMedium, size: 16.0)

        let rowIndex = indexPath.section > 0 ? indexPath.row + HamburgerMenu.mainCaseCount  : indexPath.row
        
        if BBQUserDefaults.sharedInstance.isGuestUser, let menuName = HamburgerMenu(rawValue: rowIndex) {
            if (menuName == .myHappinessCard_Menu_Model || menuName == .myReservations_Menu_Model || menuName == .delivery_History_Model || menuName == .manage_address_Model || menuName == .feedback_Menu_Model || menuName == .profile_Menu_Model) {
                cell.lbl.textColor = .lightGray
                cell.lbl.alpha = 0.6
            }
        }
        if  !(BBQUserDefaults.sharedInstance.isGuestUser) && (HamburgerMenu(rawValue: rowIndex) == .inviteFriends) {
            cell.lbl.font =  UIFont(name: Constants.App.BBQAppFont.ThemeSemiBold, size: 16.0)
            cell.lblSubTitle.text = "Invite friends and get 100 Smile Coins."
        }
        return cell
    }
}

extension BBQMenuViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let rowIndex = indexPath.section > 0 ? indexPath.row + HamburgerMenu.mainCaseCount  : indexPath.row

        
        print("IndexPtah \(rowIndex)")
        switch HamburgerMenu(rawValue: rowIndex) {
        case .home_Menu_Model: //Home
            
            //If we are on the same navigation , just pop from the given screen

            if let tabBarController1 = navigationVC?.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
                tabBarController1.selectedIndex = 0
                
                if tabBarController1.selectedViewController != nil {
                    // Check whether the top-most view controller on the stack is an
                    
                    let currentNavigationStack = navigationVC?.viewControllers
                    guard let modifyingStack = currentNavigationStack else {
                        return
                    }
                    if let lastController = modifyingStack.last {
                        //Check if navigation controller stack includes this tabBarController1
                        //pop to the respected  tabbar
                        if ((lastController.isKind(of: BBQHomeViewController.self))) {
                            //not required to do any thing
                        }
                        else if let viewControllers = self.navigationVC?.viewControllers {
                            for vc in viewControllers {
                                if vc.isKind(of: DeliveryTabBarController.classForCoder()) {
                                    self.navigationVC?.popToViewController(vc, animated: true)
                                }
                            }
                        }
                        else {
                            let  homeViewController = UIStoryboard.loadHomeViewController()
                            self.navigationVC?.pushViewController(homeViewController, animated: false)
                        }
                    }
                    
                }
            }
        
        break
            
     /*   case .myTransactions_Menu_Model: //Transaction
            if !BBQUserDefaults.sharedInstance.isGuestUser {
                let bookingHistoryController = UIStoryboard.loadBookingHistoryController()
                bookingHistoryController.isComingFromNotification = false
                replaceControllerInCurrentStack(newVC: bookingHistoryController)
            } else {
                ToastHandler.showToastWithMessage(message: kHamMenuTransactionLoginMessage)
            }
            break*/
           
        case .myReservations_Menu_Model: //Reservation
            if !BBQUserDefaults.sharedInstance.isGuestUser {
                let bookingReservationController = UIStoryboard.loadReservationViewController()
                bookingReservationController.isComingFromNotification = false
                replaceControllerInCurrentStack(newVC: bookingReservationController)
            } else {
                ToastHandler.showToastWithMessage(message: kHamMenuTransactionLoginMessage)
            }
        break
        
        case .myHappinessCard_Menu_Model: //happiness card
            if !BBQUserDefaults.sharedInstance.isGuestUser {
                let bookingHappinesscardController = UIStoryboard.loadHappinesscardViewController()
                bookingHappinesscardController.isComingFromNotification = false
                replaceControllerInCurrentStack(newVC: bookingHappinesscardController)
            } else {
                ToastHandler.showToastWithMessage(message: kHamMenuTransactionLoginMessage)
            }
        break
        
        case .outletInfo_Menu_Model: //Outlet
            if BBQUserDefaults.sharedInstance.branchIdValue != "" {
                let controller = UIStoryboard.loadOutletInfoViewController()
                controller.view.layoutIfNeeded()
                self.navigationVC?.pushViewController(controller, animated: true)
            }
            else {
                ToastHandler.showToastWithMessage(message: kHamMenuOutletInfoMessage)
            }
            
        case .delivery_History_Model: //Delivery History
            if !BBQUserDefaults.sharedInstance.isGuestUser {
                let orderHisVC =  UIUtils.viewController(storyboardId: "DeliveryHome", vcId: "OrderHistoryViewController") as! OrderHistoryViewController
                orderHisVC.orderViewModel = OrderHistoryViewModel()
                self.navigationVC?.pushViewController(orderHisVC, animated: true)
            } else {
                ToastHandler.showToastWithMessage(message: kHamMenuTransactionLoginMessage)
            }
            break
        case .promotionsandoffers_menu_model: //Promotions&Offers
            if let delegate = self.delegate {
                delegate.showPromotionsAndOffers()
            }
            
        case .notifications_Menu_Model: //Notification
            let notificationController = UIStoryboard.loadNotificationViewController()
            replaceControllerInCurrentStack(newVC: notificationController)
            
        case .profile_Menu_Model: //Profile
            if !BBQUserDefaults.sharedInstance.isGuestUser {
                let controller = UIStoryboard.loadProfileViewController()
                replaceControllerInCurrentStack(newVC: controller)
            }else {
                ToastHandler.showToastWithMessage(message: kHamMenuLoginTitle)
            }
        case .HelpAndSupport_Menu_Model: //Help & Support
            let controller = UIStoryboard.loadHelpSupportViewController()
            replaceControllerInCurrentStack(newVC: controller)
            
        case .about_Menu_Model: //About
            let controller = UIStoryboard.loadAboutViewController()
            replaceControllerInCurrentStack(newVC: controller)
//          let  controller = UIStoryboard.loadFeedBackViewController()//loadAboutViewController()
//            replaceControllerInCurrentStack(newVC: controller)
            
        case .logout_Menu_Model: //Login/Logout
            if BBQUserDefaults.sharedInstance.isGuestUser {
//                let controller = UIStoryboard.loadLoginViewController()
//                controller.delegate = self
//                controller.shouldCloseTouchingOutside = true
//                controller.currentTheme = .dark
//                controller.navigationFromScreen = .Hamburger
//                controller.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//                self.present(controller, animated: true, completion: nil)
//                let tabViewController = UIStoryboard.loadLoginBackground()
//            //let tabViewController = UIStoryboard.loadLoginThroughPhoneController()
//            self.navigationVC?.pushViewController(tabViewController, animated: false)
                goToLoginPage()
                
            } else {
                showLogoutAlert()
            }
            
        case .manage_address_Model:
            if !BBQUserDefaults.sharedInstance.isGuestUser {
                let manageAddressViewController =  UIUtils.viewController(storyboardId: "DeliveryHome", vcId: "ManageAddressViewController") as! ManageAddressViewController
                self.navigationVC?.pushViewController(manageAddressViewController, animated: true)
            } else {
                ToastHandler.showToastWithMessage(message: kHamMenuTransactionLoginMessage)
            }
            break
            
        case .inviteFriends: //Invite Friend
           if !BBQUserDefaults.sharedInstance.isGuestUser {
              //  _ = Timer.scheduledTimer(timeInterval: 0.0, target: self, selector: #selector(self.shareMethod), userInfo: nil, repeats: false)
            self.navigationVC?.pushViewController(UIStoryboard.loadAppReferralFullScreen(), animated: true)
            }
        break
            
            
        case .feedback_Menu_Model: //Feedback screen
            if !BBQUserDefaults.sharedInstance.isGuestUser {
                let bookingHappinesscardController = UIStoryboard.loadFeedBackFromSideMenuViewController()
                replaceControllerInCurrentStack(newVC: bookingHappinesscardController)
            } else {
                ToastHandler.showToastWithMessage(message: kHamMenuLoginTitle)
            }
        break
            
            
        default:
            break
        }
        sideMenuDelegate?.hideSideMenu()
        //self.view.removeFromSuperview()
    }
}

extension BBQMenuViewController: LoginControllerDelegate {
  
    func gotoHomeScreen(isExistingUser: Bool, navigationFrom: LoginNavigationScreen) {
        if let delegate = self.delegate {
            delegate.postUserAccountProcessing()
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func replaceControllerInCurrentStack(newVC: UIViewController){
        navigationVC?.pushViewController(newVC, animated: true)
    }

    
}


