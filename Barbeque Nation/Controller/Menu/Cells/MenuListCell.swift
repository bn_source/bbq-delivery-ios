//
//  MenuListCell.swift
//  finalHamburger
//
//  Created by Abhijit on 05/08/19.
//  Copyright © 2019 Abhijit. All rights reserved.
//

import UIKit

class MenuListCell: UITableViewCell {
    
    
    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
