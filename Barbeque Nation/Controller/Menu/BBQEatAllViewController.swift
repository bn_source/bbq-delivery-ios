//
 //  Created by Arpana on 04/05/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified BBQEatAllViewController.swift
 //

import UIKit
import ShimmerSwift

enum  segmentTag : Int {
    
    case Menu = 890
    case SubMenu  = 876
}


class MenuModelDetails {
    
    var menuName: String?
    var menuDescription : String?
    var menuImageName: String?
    var menuType : BBQMenuType = .BBQVeg
    var tagImage: String?
    var price : String?
    var currency : String?
    var isBeverage: Bool = false
    var buffetData :[BuffetData]?

    init(menuName name: String, menuDescription desc: String, menuImage image: String?, menuType menuTypeL: BBQMenuType, tagImage tagImagePath: String?, priceOfItem: String?, currencyReceived: String?, BuffetDataL: [BuffetData]?){
        
        menuName =  name
        menuDescription = desc
        menuImageName = image?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
        menuType = menuTypeL
        tagImage = tagImagePath?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
        price = priceOfItem
        currency = currencyReceived
        buffetData = BuffetDataL
    }
}
class BBQEatAllViewController: UIViewController {

    @IBOutlet weak var segmentControl:DGScrollableSegmentControl!
    @IBOutlet weak var subMenusegmentControl:DGScrollableSegmentControl!

    @IBOutlet weak var mainview: UIView!
    @IBOutlet weak var lblBranchLbl: UILabel!
    @IBOutlet weak var tbleView:  UITableView!
    var MainMenuSelectedIndex = 0
    var menuType : BBQMenuType?
    var isBeverages : Bool = false
    var buffetTitle :  String?
    
    private var menuCategoryIndex = -1
    
    var buffetData: [BuffetData]? {
        didSet {
            
        }
    }
    
    private var menuIndex = 0 {
        didSet {
            if self.consolidateAllDataArray.count > 0 {
                self.consolidateAllDataArray.removeAll()
            }
            self.consolidateAllDataArray = self.getConsolidatedArray(forMenuIndex: self.menuIndex)
        }
    }
    
    private var consolidateAllDataArray = [MenuItems]()

    
    override func viewDidLoad() {
            super.viewDidLoad()
            AnalyticsHelper.shared.triggerEvent(type: .HS01)
          //  self.shimmerControl = ShimmeringView(frame: shimmerSupportView.bounds)
           // registerTableViewCells()
            
        segmentControl.datasource = self
        segmentControl.delegate = self
        subMenusegmentControl.delegate = self
        subMenusegmentControl.datasource = self
        segmentControl.tag = segmentTag.Menu.rawValue
        subMenusegmentControl.tag = segmentTag.SubMenu.rawValue
        
            //  collectionView.tag = Constants.TagValues.voucherCellTag
              mainview.layer.masksToBounds = false
              mainview.layer.shadowColor = UIColor.hexStringToUIColor(hex: "#55606D1A").cgColor
              mainview.layer.shadowOffset = CGSize(width: 0, height: 3)
              mainview.layer.shadowRadius = 1
              mainview.layer.shadowOpacity = 0.2
              
              segmentControl.clipsToBounds = true
              segmentControl.layer.cornerRadius = 12
              segmentControl.layer.maskedCorners = [ .layerMinXMinYCorner, .layerMinXMaxYCorner]
              segmentControl.numbersOfDevider = 3.0
              segmentControl.isFirstItemAutoSelected = true
            
        subMenusegmentControl.clipsToBounds = true
        subMenusegmentControl.layer.cornerRadius = 12
        subMenusegmentControl.layer.maskedCorners = [ .layerMinXMinYCorner, .layerMinXMaxYCorner]
        subMenusegmentControl.numbersOfDevider = 4.0
        subMenusegmentControl.isFirstItemAutoSelected = false
        tbleView.estimatedRowHeight = 100
        tbleView.rowHeight = UITableView.automaticDimension
        
        
        lblBranchLbl.text = BBQUserDefaults.sharedInstance.CurrentSelectedLocation
        
        
              if #available(iOS 13.0, *) {
                  segmentControl.layer.cornerCurve = .continuous
              } else {
                  // Fallback on earlier versions
              }
            self.segmentControl.reload()

           // upcomingReservationCollectionView.isHidden = true
        }
    
    
    private func getConsolidatedArray(forMenuIndex: Int) -> [MenuItems] {
        self.consolidateAllDataArray.removeAll()
        var consolidatedArray = [MenuItems]()
        if let data = self.buffetData, data.count > 0 {
            if let buffetItemsArray = data[forMenuIndex].buffetItems {
                for buffetItemsData in buffetItemsArray {
                    for buffetItems in buffetItemsData.buffetItems_menuItems ?? [] {
                        self.consolidateAllDataArray.append(buffetItems)
                    }
                }
            }
        }
        return self.consolidateAllDataArray
    }

    @IBAction func backBtnPressed(_ sender : UIButton){
        
        dismiss(animated: true, completion: nil)
    }

}

extension BBQEatAllViewController :DGScrollableSegmentControlDataSource,DGScrollableSegmentControlDelegate {

    func numberOfMenuItems() -> [String] {
        var menuArray = [String]()
        if let data = self.buffetData, data.count > 0 {
            for (_, item) in data.enumerated() {
                menuArray.append(item.buffetName ?? "")
            }
        }
        return menuArray
    }
    
    func numbersOfItem(_ sender : DGScrollableSegmentControl) -> Int {
        
        switch (sender.tag){
            
        case segmentTag.Menu.rawValue :
            return numberOfMenuItems().count
            
        case segmentTag.SubMenu.rawValue :
            return menuCategoryForMenuAt(index: menuIndex).count

        default: return 0
        }
        
        
    }
    
  
    
    func itemfor(_ index: Int,  sender : DGScrollableSegmentControl) -> DGItem {
        
        let item = DGItem()
    
        
        switch (sender.tag){
            
        case segmentTag.Menu.rawValue:
            
            item.setTitle(numberOfMenuItems()[index], for: .normal)
              item.setTitleColor( UIColor.hexStringToUIColor(hex: "#2B3849") , for: .normal)
           // item.isSelected = self.buffetData[index] ?? false
            item.tag = index
            return item
               
            
        case segmentTag.SubMenu.rawValue:
            
            item.setTitle(menuCategoryForMenuAt(index: menuIndex)[index], for: .normal)
            item.tag = menuIndex
              item.setTitleColor( UIColor.hexStringToUIColor(hex: "#2B3849") , for: .normal)
            return item

        default: print("Wrong index")
        }
        
        return item
        
    }
    func didSelect(_ item: DGItem, atIndex index: Int ,  sender : DGScrollableSegmentControl) {
        
        switch (sender.tag) {
            
        case segmentTag.Menu.rawValue:
            //Main Menu index no
            menuIndex = index
            
            //Reload menu list again by deselecting  submenu
            menuCategoryIndex = -1
            self.subMenusegmentControl.reload()
            tbleView.reloadData()

            break
        
        case segmentTag.SubMenu.rawValue:
            
            //Sub Main Menu index no
            menuCategoryIndex = index
            tbleView.reloadData()
            
        default: print("Wrong index")
        }
      
        

        
    }
}

extension BBQEatAllViewController {
    
    func menuCategoryForMenuAt(index menuIndex: Int) -> [String] {
        
        var categoryArray = [String]()
        if let data = self.buffetData, data.count > 0 {
            if let buffetItems = data[menuIndex].buffetItems {
                for buffetItem in buffetItems {
                    categoryArray.append(buffetItem.buffetItems_name ?? "")
                }
            }
        }
        return categoryArray
    }
    
    
    func numberOfImagesFor(menu: Int, menuCategory: Int) -> Int {
        if let data = self.buffetData, data.count > 0 {
            if menuCategory == -1 {
                //Show all the images
                var totalCount = 0
                if let buffetItemsArray = data[menu].buffetItems {
                    for buffetItemsData in buffetItemsArray {
                        totalCount += buffetItemsData.buffetItems_menuItems?.count ?? 0
                    }
                }
                return totalCount
            }
            return data[menu].buffetItems?[menuCategory].buffetItems_menuItems?.count ?? 0
        }
        return 0
    }
    
    
    func menuDataAt(row rowNumber: Int, forMenuIndex: Int, forMenuCategoryIndex: Int) -> MenuModelDetails {//(menuName: String, menuDescription: String, menuImage: String?, menuType: BBQMenuType, tagImagePath: String?, price: String?, currency: String?, buffetData: [BuffetData]?) {
        if let data = self.buffetData, data.count > 0 {
            
            var menuData: MenuItems?
            if menuCategoryIndex == -1 {
                //Show all the images
                menuData = self.consolidateAllDataArray[rowNumber]
            } else {
                menuData = data[forMenuIndex].buffetItems?[forMenuCategoryIndex].buffetItems_menuItems?[rowNumber]
            }
            
            var menuType = BBQMenuType.BBQOthers
            if menuData?.menuitems_Type == "NONVEG" {
                menuType = BBQMenuType.BBQNonVeg
            } else if menuData?.menuitems_Type == "VEG" {
                menuType = BBQMenuType.BBQVeg
            }
            
            let tagImagePath = menuData?.menuitems_Tags?.tagImage
            return MenuModelDetails.init(menuName: menuData?.menuitems_Name ?? "", menuDescription:menuData?.menuitems_Description ?? "", menuImage: menuData?.menuitems_Image ?? "", menuType: menuType, tagImage: tagImagePath, priceOfItem: menuData?.menuitems_DefaultPrice, currencyReceived: menuData?.menuitems_Currency ?? "INR", BuffetDataL: data)
        }
        return MenuModelDetails.init(menuName: "", menuDescription: "", menuImage: "", menuType: BBQMenuType.BBQOthers, tagImage: "", priceOfItem: "", currencyReceived: "", BuffetDataL: [BuffetData]())
        
        
    }
    
    
//    private func setupMenuCategoryButtonsForMenu(index menuIndex: Int) {
//
//        let demoCategory = self.menuCategoryForMenuAt(index: menuIndex)
//
//      //  menuCategoryView.reset()
//        for (index, text) in demoCategory.enumerated() {
//            menuCategoryView.addTag(text: text, tagIndex: index)
//        }
//
//
//        let totalRows = menuCategoryView.getRowNumber(index: demoCategory.count - 1)
//
//        UIView.animate(withDuration: 0.2, animations: {
//            let showCostLabel =  self.numberOfMenuItems()[menuIndex] == "Beverages"
//            self.setupVisibilityForCostLabel(doShow: showCostLabel)
//
//            if totalRows == 0 {
//                self.categoryMenuHeightConstarint.constant = 45
//            } else if totalRows == 1 {
//                self.categoryMenuHeightConstarint.constant = 90
//            } else {
//                self.categoryMenuHeightConstarint.constant = 125
//            }
//            self.view.layoutIfNeeded()
//        })
//
//
//
//    }
}

extension BBQEatAllViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.numberOfImagesFor(menu: menuIndex, menuCategory: menuCategoryIndex)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BBQAllYouEatTableViewCell", for: indexPath) as! BBQAllYouEatTableViewCell
            
        
        let menuData = self.menuDataAt(row: indexPath.row, forMenuIndex: menuIndex, forMenuCategoryIndex: menuCategoryIndex)
        
        self.buffetTitle = self.numberOfMenuItems()[menuIndex]
        self.isBeverages = buffetTitle == "Beverages"

        menuData.isBeverage = self.isBeverages
        cell.configureCell(model : menuData)
        
            return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let menuData = self.menuDataAt(row: indexPath.row, forMenuIndex: menuIndex, forMenuCategoryIndex: menuCategoryIndex)
        let outletMenuItemDetailsView = OutletMenuItemDetailsView().initWith(item: menuData)
        outletMenuItemDetailsView.addToSuperView(view: self)
    }

//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//
//            if let cell = tableView.dequeueReusableCell(withIdentifier: "BBQAllYouEatTableViewCell") as? BBQAllYouEatTableViewCell, let item = self.menuDataAt(row: indexPath.row, forMenuIndex: menuIndex, forMenuCategoryIndex: menuCategoryIndex) as? MenuModelDetails{
//
//                cell.itemNmLbl.text = "\(item.menuName ?? "")"
//                cell.itemDetailsLbl.text =  "\(String(describing: item.menuDescription))"
//                cell.layoutIfNeeded()
//                var height = "\(item.menuName)".height(withConstrainedWidth: cell.itemNmLbl.frame.width, font: cell.itemNmLbl.font)
//
//                height  +=  "\(item.menuDescription)".height(withConstrainedWidth: cell.itemDetailsLbl.frame.width, font: cell.itemDetailsLbl.font)
//
//                height  += 30
//
//                if height < 90 {
//                    return 90
//                }
//                return height
//
//            }
//
//            return 30
//        }

    
   
}
