//
//  LogoAnimationView.swift
//  GifLaunchAnimation
//
//  Created by Nischitha on 16/09/19.
//  Copyright © 2019 Nischitha. All rights reserved.
//

import UIKit
import SwiftyGif

class LogoAnimationView: UIView {
    
    let logoGifImageView: UIImageView = {
        guard let gifImage = try? UIImage(gifName: "Splash.gif") else {
            return UIImageView()
        }
        return UIImageView(gifImage: gifImage, loopCount: 1)
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        addSubview(logoGifImageView)
        logoGifImageView.translatesAutoresizingMaskIntoConstraints = false
        logoGifImageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        logoGifImageView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        logoGifImageView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        logoGifImageView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
    }

}
