//
 //  Created by Nischitha on 17/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified SplashScreenViewController.swift
 //

import UIKit
import SwiftyGif

class SplashScreenViewController: UIViewController {
    
    let logoAnimationView = LogoAnimationView()
    let welcomeViewModel = WelcomeViewModel()
    
    private var isInitialDataFetched = false
    
    // This dispatch group has aligned with calling two APIs we had in splash screen.
    // Extra task is also added for animating gif atleast once.
    private var dispatchGroup : DispatchGroup?
    
    // MARK: Lazy Inits
    
    lazy private var profileViewModel : BBQProfileViewModel = {
        let profileVM = BBQProfileViewModel()
        return profileVM
    } ()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        view.addSubview(logoAnimationView)
        logoAnimationView.pinEdgesToSuperView()
        logoAnimationView.logoGifImageView.delegate = self
        
        
        self.dispatchGroup = DispatchGroup()
        self.fetchInitialLoadData()
       
        self.dispatchGroup?.enter() // Adding 1st Task
        logoAnimationView.logoGifImageView.startAnimatingGif()

    }
    
    func goToWelcomePage() {
        let viewController = UIStoryboard.loadWelcomePage()
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToHomePage() {
        let tabViewController = UIStoryboard.loadHomeViewController()
        self.navigationController?.pushViewController(tabViewController, animated: true)
    }
    
    func goToLoginPage() {
        let tabViewController = UIStoryboard.loadLoginBackground()
        self.navigationController?.pushViewController(tabViewController, animated: true)
    }
    
    
    func navigateFromSplashScreen()  {
        self.logoAnimationView.isHidden = true
        self.goToWelcomePage()
    }
    
    // MARK: Data Fetch Methods
    private func fetchInitialLoadData() {
       // let dispatchGroup = DispatchGroup()

        // Profile information fetch in case of logged in user
        if !BBQUserDefaults.sharedInstance.isGuestUser {
            self.dispatchGroup?.enter() // Adding 2nd Task
            self.profileViewModel.getUserProfile { (isFetched) in
                if isFetched {
                     // Leaving 2nd Task
                    self.dispatchGroup?.leave()
                } // Add message here if not profile fetched successfully
            }
        }
        
        // Welcome page data fetch
        self.dispatchGroup?.enter() // Adding 3rd Task
        self.welcomeViewModel.getWelcomeInfo { (result) in
            // Leaving 3rd Task
            self.dispatchGroup?.leave() // TODO: Abhijith, move this inside result true once welcome API is ready
            if result == true {
            
            } else {
                print("no data")
            }
        }
        
        self.dispatchGroup?.notify(queue: DispatchQueue.main) {
            self.navigateFromSplashScreen()
        }
        
    }
}

extension SplashScreenViewController: SwiftyGifDelegate {
    
    // MARK:- SwiftyGifDelegate Methods
    
    func gifDidStop(sender: UIImageView) {
        self.dispatchGroup?.leave()
    }
    
}
