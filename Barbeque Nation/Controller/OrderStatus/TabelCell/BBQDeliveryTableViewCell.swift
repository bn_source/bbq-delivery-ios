//
 //  Created by Arpana Rani on 19/02/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQDeliveryTableViewCell.swift
 //

import UIKit

protocol  BBQDeliveryTableViewCellCallDelegate {
    func call()
}

class BBQDeliveryTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var BtnCall: UIButton!
    @IBOutlet weak var outerBView : UIView!
    var callDelegate : BBQDeliveryTableViewCellCallDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        outerBView.roundCorners(cornerRadius: 8.0, borderColor: UIColor(red: 0.753, green: 0.8, blue: 0.855, alpha: 1), borderWidth: 1.0)
    }

    func setData()  {
        
       // labelTitle.text = "adsac"
        
    }
    @IBAction func callBtnPressed (_ sender : Any){
        
        callDelegate?.call()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
