//
 //  Created by Arpana Rani on 19/02/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQImageTableViewCell.swift
 //

import UIKit

class BBQImageTableViewCell: UITableViewCell {

    var imageArray = [String]()
    @IBOutlet weak var collectionForrOrderDispatch: UICollectionView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionForrOrderDispatch.delegate = self
        collectionForrOrderDispatch.dataSource = self
       
        collectionForrOrderDispatch.registerCell(nibName: BBQPackingPhotoCollectionViewCell.getCellIdentifier())
        if let flowLayout = collectionForrOrderDispatch.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.scrollDirection = .horizontal
            flowLayout.estimatedItemSize = CGSize(width: self.frame.size.width / 3, height :180)
            
        }
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpImage(){
        
        collectionForrOrderDispatch.reloadData()
    }

}

extension BBQImageTableViewCell : UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout  {
    
    // MARK: CollectionView Methods

    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return 3 // imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BBQPackingPhotoCollectionViewCell.getCellIdentifier(), for: indexPath) as! BBQPackingPhotoCollectionViewCell
        return cell

    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//            return CGSize(width: collectionView.frame.width/3, height: 300)
//
//    }
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: self.frame.width/3, height: self.frame.height  )
    
        }
}
