//
 //  Created by Arpana Rani on 23/02/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQItemDetailUITableViewCell.swift
 //

import UIKit

class BBQItemDetailUITableViewCell: UITableViewCell {

    @IBOutlet weak var itemAmountLbl: UILabel!
    @IBOutlet weak var itemNmLbl: UILabel!
    @IBOutlet weak var itemTypeImgView: UIImageView!
    @IBOutlet weak var lblItemCount :UILabel!
    @IBOutlet weak var mainView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        lblItemCount.setCornerRadius(5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        mainView.setCornerRadius(10)
        mainView.setShadow(color: UIColor.hexStringToUIColor(hex: "55606D1A") , opacity: 0.2, offSet: .zero, radius: 7, scale: true)
        mainView.layer.masksToBounds = true
    }

    func setupItem(_ item: Lst_items) {
        itemAmountLbl.text = "\(getCurrency())\(item.total ?? "0")"
        itemNmLbl.text = "\(item.item_name ?? "")"
        itemTypeImgView.image = item.food_type == "0" ? #imageLiteral(resourceName: "Group 846") : #imageLiteral(resourceName: "Group 845")

        lblItemCount.text = "\(item.quantity ?? "0")"
    }
    
    func setupItem(_ item: Item) {
        itemAmountLbl.text = "\(getCurrency())\(item.total)"
        itemNmLbl.text = "\(item.name)   x \(item.quantity)"
        itemTypeImgView.image = item.foodType == FoodType.nonVeg ? #imageLiteral(resourceName: "Group 845") : #imageLiteral(resourceName: "Group 846")
    }
}
