//
 //  Created by Arpana Rani on 23/02/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQTaxTableViewCell.swift
 //

import UIKit

class BBQTaxTableViewCell: UITableViewCell {
    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnDetails: UIButton!
    @IBOutlet weak var lblValue: UILabel!
    
    var index: Int?
    var taxBreakUp: TaxBreakUp?
    var delegate: BBQTaxTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setViewBorder(index: Int, isLastCell: Bool){
        if index == 0{
            viewForContainer.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        }else if isLastCell{
            viewForContainer.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.bottomCornersMask)
        }
    }

    func setCellData(taxBreakUp: TaxBreakUp, index: Int, isLastCell: Bool) {
        self.taxBreakUp = taxBreakUp
        self.index = index
        setViewBorder(index: index, isLastCell: isLastCell)
        lblTitle.text = taxBreakUp.label
        lblValue?.text = "\(getCurrency())0"
        if taxBreakUp.label == "Total"{
            lblTitle.setTheme(size: 15.0, weight: .medium, color: .darkText)
            lblValue.setTheme(size: 15.0, weight: .medium, color: .darkText)
        }else{
            lblTitle.setTheme(size: 14.0, weight: .regular, color: .text)
            lblValue.setTheme(size: 14.0, weight: .regular, color: .text)
        }
        if let amount = Int( taxBreakUp.value), amount <= 0{
            lblValue?.textColor = .darkGreen
            if amount < 0{
                lblValue?.text = "-\(getCurrency())\(-amount)"
                btnDetails.setTitle("-\(getCurrency())\(-amount)", for: .normal)
            }else if !taxBreakUp.label.lowercased().contains("smile"), !taxBreakUp.label.lowercased().contains("promo"), !taxBreakUp.label.lowercased().contains("discount"),!taxBreakUp.label.lowercased().contains("offer"){
                lblValue?.text = "FREE"
                btnDetails.setTitle("FREE", for: .normal)
            }
        }else{
            lblValue?.textColor = .text
            lblValue?.text = "\(getCurrency())\( taxBreakUp.value )"
            btnDetails.setTitle("\(getCurrency())\( taxBreakUp.value )", for: .normal)
        }
        
        if taxBreakUp.details.count > 0{
            btnDetails?.isHidden = false
            lblValue?.isHidden = true
            let string =  NSAttributedString(string: getCurrency()+taxBreakUp.value, attributes: [.underlineStyle : NSUnderlineStyle.single.rawValue])
            self.btnDetails.setAttributedTitle(string, for: .normal)
        }else{
            btnDetails?.isHidden = true
            lblValue?.isHidden = false
        }
    }
    
    @IBAction func onClickBtnDetails(_ sender: Any) {
        if let index = index, let taxBreakUp = taxBreakUp{
            delegate?.didTapOnBillDetails(cell: self, index: index, taxBreakup: taxBreakUp)
        }
    }
    
}

protocol BBQTaxTableViewCellDelegate {
    func didTapOnBillDetails(cell: BBQTaxTableViewCell, index: Int, taxBreakup: TaxBreakUp)
}
