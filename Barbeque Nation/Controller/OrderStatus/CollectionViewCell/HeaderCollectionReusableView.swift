//
 //  Created by Arpana Rani on 22/02/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified HeaderCollectionReusableView.swift
 //

import UIKit

class HeaderCollectionReusableView: UICollectionReusableView {

    @IBOutlet weak var headerLabel: UILabel!
    
    static func getCellIdentifier() -> String {
        return "HeaderCollectionReusableView"
    }
    
    func updateView(titleHeader: String) {
        headerLabel.text = titleHeader
    }
}


