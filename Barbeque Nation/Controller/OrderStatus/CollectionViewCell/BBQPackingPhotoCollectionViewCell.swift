//
 //  Created by Arpana Rani on 22/02/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQPackingPhotoCollectionViewCell.swift
 //

import UIKit

class BBQPackingPhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var addBtn: UIButton!
    
    
    private var indexPath: IndexPath?
    
    static func getCellIdentifier() -> String {
        return "BBQPackingPhotoCollectionViewCell"
    }
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        itemImageView.image = nil
    }
    
    private func updateUI() {
        outerView.setCornerRadius(20)
      //  self.contentView.roundCorners(cornerRadius: 20, borderColor: UIColor(red: 0.753, green: 0.8, blue: 0.855, alpha: 1), borderWidth: 1.0)
       // outerView.setShadow()
        itemImageView.setCornerRadius(4)

       // itemImageView.setCornerRadius(itemImageView.frame.width / 1.4)
        
        
    }
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
       updateUI()
    }
    
}
