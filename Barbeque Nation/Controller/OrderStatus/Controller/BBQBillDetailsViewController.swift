//
 //  Created by Arpana Rani on 23/02/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQBillDetailsViewController.swift
 //

import UIKit
import AMPopTip

protocol BBQBillDetailsDelegate: AnyObject {
    func onClose()
}

class BBQBillDetailsViewController: UIViewController {
    
    @IBOutlet var containerView: UIView!

    @IBOutlet weak var tableView:  UITableView!
    var orderIdVal: String?
    var orderDetails: DeliveryStatusResponse?
    weak var delegate: BBQBillDetailsDelegate?
    var popTip = PopTip()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        orderDetails?.data?.addTotalInBreakUp()
        registerTableViewCell()
        tableView.reloadData()
        
        popTip.bubbleColor = .white
        popTip.cornerRadius = 5.0
        popTip.shouldShowMask = true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.containerView.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
    }

    func registerTableViewCell(){
        
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        tableView.sectionHeaderHeight = 70
        tableView.separatorStyle = .none
        tableView.register(UINib(nibName: "BBQItemDetailUITableViewCell", bundle: nil), forCellReuseIdentifier: "BBQItemDetailUITableViewCell")
        tableView.register(UINib(nibName: "BBQTaxTableViewCell", bundle: nil), forCellReuseIdentifier: "BBQTaxTableViewCell")
    }
    

}
extension BBQBillDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (orderDetails?.data?.lst_items?.count ?? 0) + (orderDetails?.data?.lst_bill_details?.count ?? 0) //(orderDetails?.data?.lst_bill_details?.count ?? 0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print(indexPath.row)
        let cellCount = orderDetails?.data?.lst_items?.count ?? 0
        if indexPath.row >= cellCount {
            let index = indexPath.row - cellCount
            if index <  (orderDetails?.data?.lst_bill_details?.count ?? 0), let data =  orderDetails?.data?.lst_bill_details?[index]{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "BBQTaxTableViewCell", for: indexPath) as? BBQTaxTableViewCell else { return UITableViewCell() }
                cell.setCellData(taxBreakUp: data, index: index, isLastCell: index + 1 == (orderDetails?.data?.lst_bill_details?.count ?? 0) ? true : false )
                cell.delegate = self
                return cell
            }
            
        } else if indexPath.row < cellCount {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "BBQItemDetailUITableViewCell", for: indexPath) as? BBQItemDetailUITableViewCell else { return UITableViewCell() }
            if let item = orderDetails?.data?.lst_items?[indexPath.row] {
                  cell.setupItem(item)
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row >= ((orderDetails?.data?.lst_items?.count ?? 0)) {
          //  return 30
            return  UITableView.automaticDimension

        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "BBQItemDetailUITableViewCell") as? BBQItemDetailUITableViewCell, let item = orderDetails?.data?.lst_items?[indexPath.row] {
                cell.itemAmountLbl.text = "\(getCurrency())\(item.amount ?? 0)"
                cell.itemNmLbl.text = "\(item.item_name ?? "")   x \(item.quantity ?? "1")"
                    cell.layoutIfNeeded()
                let height = "\(item.item_name ?? "")   x \(item.quantity ?? "1")".height(withConstrainedWidth: cell.itemNmLbl.frame.width, font: cell.itemNmLbl.font)
                    return (height + 70)
                    
                }
                
            }
            
            return 30
        }
    
    
    @IBAction func close(_ sender : Any){
        self.dismiss(animated: true, completion: nil)
    }
   
}

extension BBQBillDetailsViewController: BBQTaxTableViewCellDelegate{
    func didTapOnBillDetails(cell: BBQTaxTableViewCell, index: Int, taxBreakup: TaxBreakUp) {
        let popupVC = BillDetailsPopVC(nibName: "BillDetailsPopVC", bundle: nil)
        popupVC.modalPresentationStyle = UIModalPresentationStyle .popover
        popupVC.preferredContentSize = CGSize(width: 170, height: 130)
        popupVC.strTitle = taxBreakup.label
        popupVC.taxBreakUp = taxBreakup.details
        popupVC.view.frame.size.width = 200 > self.view.frame.size.width ? self.view.frame.size.width : 200
        popupVC.view.frame.size.height = CGFloat(60 + (taxBreakup.details.count * 26))
        var originFrame = cell.btnDetails.frame
        originFrame.size.width = 80
        originFrame.origin.x = cell.frame.width - 60
        let frame = cell.convert(originFrame, to: self.view)
        popTip.show(customView: popupVC.view, direction: .auto, in: self.view, from: frame)
        AnalyticsHelper.shared.triggerEvent(type: .C19)
    }
}
