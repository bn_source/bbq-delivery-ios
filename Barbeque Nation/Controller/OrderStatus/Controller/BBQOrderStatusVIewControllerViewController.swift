//
 //  Created by Arpana Rani on 19/02/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQOrderStatusVIewControllerViewController.swift
 //

import UIKit
import ShimmerSwift


class BBQOrderStatusVIewControllerViewController: BaseViewController {
    
    var viewModel: DeliveryStatusViewModel?
    var orderId: String?
    var orderTransactionType: Int = 1
    var deliveryStatusLis: DeliveryStatusResponse?
   // var orderDetails: Order?
    var timer = Timer()
   // var orderViewModel: OrderHistoryViewModel?

    @IBOutlet weak var labelForOrderReceived: UILabel!
    @IBOutlet weak var labelForOrderReceivedTime: UILabel!
    @IBOutlet weak var btnTitleForOrderReceived: UILabel!
    @IBOutlet weak var lineForOrderReceived: UIImageView!
    @IBOutlet weak var dashedViewForOrderReceived : CustomDashedView!
    @IBOutlet weak var titleHeaderLabelOrderReceived: UILabel!
    @IBOutlet weak var imageViewForOrderReceived: UIImageView!
    @IBOutlet weak var rateViewForTakeAway : UIView!
    @IBOutlet weak var rateStarViewForTakeAway : FloatRatingView!
    @IBOutlet weak var labelForOrderConfirmed: UILabel!
    @IBOutlet weak var labelForOrderConfirmedTitle : UILabel!// remove this later
    @IBOutlet weak var labelForOrderConfirmedTime: UILabel!
    @IBOutlet weak var btnTitleOrderConfirmed: UILabel!
    @IBOutlet weak var lineForOrderCofirmed: UIImageView!
    @IBOutlet weak var viewForOrderConfirmedBorder: UIView!
    @IBOutlet weak var dashedViewForOrderConfirmed : CustomDashedView!
    @IBOutlet weak var titleHeaderLabelOrderConfirmed: UILabel!
    @IBOutlet weak var imageViewForOrderConfirmed: UIImageView!
    @IBOutlet weak var viewForOTP: UIView!
    @IBOutlet weak var lblDeliveryOTP: UILabel!
    @IBOutlet weak var appReferalView: AppReferralView!
    @IBOutlet weak var appReferalViewForDelivery: AppReferralView!
    var bottomSheetController: BottomSheetController?

    @IBOutlet weak var lblTakeAwayStatusDescription: UILabel!
    @IBOutlet weak var labelForOrderPrepare: UILabel!
    @IBOutlet weak var labelForOrderPrepareTime: UILabel!
    @IBOutlet weak var btnTitleForOrderPrepare: UILabel!
    @IBOutlet weak var lineForOrderPrepare: UIImageView!
    @IBOutlet weak var chefProfileImage: UIImageView!
    @IBOutlet weak var dashedViewForOrderPrepare : CustomDashedView!
    @IBOutlet weak var titleHeaderLabelOrderPrepare: UILabel!
    @IBOutlet weak var imageViewForOrderPrepare: UIImageView!
    @IBOutlet weak var heighForTakewayConfirm: NSLayoutConstraint!
    

    @IBOutlet weak var btnTitleForOrderDispatch: UILabel!
    @IBOutlet weak var labelForOrderDispatchTime: UILabel!
    @IBOutlet weak var lineForOrderDispatch: UIImageView!
    @IBOutlet weak var dashedViewForOrderDispatch : CustomDashedView!
    @IBOutlet weak var titleHeaderLabelOrderDispatch: UILabel!
    @IBOutlet weak var imageViewForOrderDispatch: UIImageView!
    @IBOutlet weak var chefProfileImageWidthConstarint: NSLayoutConstraint!
    
    @IBOutlet weak var deliveryTrackConstaint: NSLayoutConstraint!
    @IBOutlet weak var driverDescriptionLabelDispatch : UILabel!
    @IBOutlet weak var btnTitleForOrderDeliver: UILabel!
    @IBOutlet weak var labelForOrderDeliverTime: UILabel!
    @IBOutlet weak var titleHeaderLabelOrderDelivered: UILabel!
    @IBOutlet weak var imageViewForOrderDelivered: UIImageView!

    @IBOutlet weak var driverImageView: UIImageView!
   @IBOutlet weak var lineForOrderDeliver: UIImageView!
    @IBOutlet weak var imgOutletLocationPin: UIImageView!
    //@IBOutlet weak var dashedViewForOrderDeliver : CustomDashedView!


    @IBOutlet weak var labelBillAmount: UILabel!
    @IBOutlet weak var labelSaveOnBillDetails: UILabel!
    @IBOutlet weak var viewForBillDetails: UIView!

    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var ratingView: FloatRatingView!

    @IBOutlet weak var viewForrOrderDispatchBorder: UIView!

    @IBOutlet weak var topOrderIdLbl: UILabel!
    @IBOutlet weak var topOrderDateLbl: UILabel!
    
    @IBOutlet weak var bottomTotalBillLbl: UILabel!
    @IBOutlet weak var bottomTotalBillSavingLbl: UILabel!

    @IBOutlet weak var outerViewForDispatchTracking: UIView!
   // @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var labelPickUpOTP: UILabel!

    
    //outlet for  takeaway
    @IBOutlet weak var contentViewForTakeAway: UIView!
    @IBOutlet weak var contentViewForDelivery: UIView!
    @IBOutlet weak var scrollView: UIScrollView!

    @IBOutlet weak var borderViewForLocationTakeAway: UIView!
    @IBOutlet weak var borderViewForTakeAway: UIView!

    @IBOutlet weak var takeAwayPickUpTime: UILabel!
    @IBOutlet weak var takeAwayPickUpAddress: UILabel!
    @IBOutlet weak var takeAwayPickupBranchName: UILabel!
    
    @IBOutlet weak var bottomConstraintReceived: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraintDelivery: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraintPrepared: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraintDispatch: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraintConfirmed: NSLayoutConstraint!
    @IBOutlet weak var shimmerContainer: UIView!
    
    @IBOutlet weak var bottomConstraintForDeliveryToRateView: NSLayoutConstraint!
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var btnReschedule: UIButton!
    
    var shimmerView : ShimmeringView?
    @IBOutlet weak var shimmerViewOrder: UIView!
    @IBOutlet weak var stackViewForCertificate: UIStackView!
    @IBOutlet weak var btnViewCerti: UIButton!
    @IBOutlet weak var lblViewCerti: UILabel!
    @IBOutlet weak var imgBrandLogo: UIImageView!
    @IBOutlet weak var btnViewDetails: UIButton!
    
    var refreshControl: UIRefreshControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        AnalyticsHelper.shared.triggerEvent(type: .OS01)

        self.hideNavigationBar(true, animated: true)
        viewForBillDetails.isHidden = true
        shimmerView = ShimmeringView(frame: contentViewForDelivery.bounds)

    }
    
    private func showShimmering() {
        self.shimmerView?.isHidden = false
        shimmerContainer.isHidden = false
        shimmerViewOrder.isHidden = false
        if let shimmer = self.shimmerView{
            self.shimmerContainer.addSubview(shimmer)
            shimmer.contentView = shimmerViewOrder
            shimmer.shimmerAnimationOpacity = 0.2
            shimmer.shimmerSpeed = 500.00
            shimmer.isShimmering = true
        }
    }
    
    private func stopShimmering() {

        if let shimmer = self.shimmerView {
            shimmer.isShimmering = false
            shimmer.isHidden = true
        }
        shimmerContainer.isHidden = true
        self.shimmerViewOrder.isHidden = true
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.screenName(name: .Order_Status_Screen)
        setUI()
        AnalyticsHelper.shared.triggerEvent(type: .OS04)
        refreshAPICall()
        self.showShimmering()

        //orderId = "9071772469"
       
        
//        orderViewModel?.getOrderDetails(orderId, completionHandler: { (orderDetailFetched) in
//
//            print("order detail \(orderDetailFetched)")
//            self.orderDetails = orderDetailFetched
//        })

        
      /*  guard let orderId = orderId else { return }
        viewModel?.getDeliveryStatus(orderId: orderId, completionHandler: {
            //update UI
            DispatchQueue.main.async { [weak self] in
                
                self?.deliveryStatusLis = self?.viewModel?.orderDeliveryStatus
                if self?.deliveryStatusLis?.data?.transaction_type == 2{
                    //delevery
                    self?.contentViewForDelivery.isHidden = true
                    self?.contentViewForTakeAway.isHidden = false
                    self?.scrollView.isScrollEnabled = false
                    self?.setUpUIForTakerAway()
                    self?.setDataForTakeAwayItem(self?.deliveryStatusLis)
                }else{
                    //takeaway screen
                    self?.scrollView.alwaysBounceVertical = true
                    self?.scrollView.bounces  = true
                    self?.refreshControl = UIRefreshControl()
                    self?.refreshControl.addTarget(self, action: #selector(self?.didPullToRefresh), for: .valueChanged)
                    self?.scrollView.insertSubview((self?.refreshControl)!, at: 0)

                   // self?.scrollView.addSubview((self?.refreshControl)!)
                    self?.contentViewForTakeAway.isHidden = true
                    self?.contentViewForDelivery.isHidden = false
                    self?.scrollView.isScrollEnabled = true
                    self?.fillUI(self?.deliveryStatusLis)

                }


            }
        })
        
        orderViewModel?.getOrderDetails(orderId, completionHandler: { (orderDetailFetched) in
            
            print("order detail \(orderDetailFetched)")
            self.orderDetails = orderDetailFetched
        })
      //  fillUI(deliveryStatusLis ?? nil)*/
       
        
    }
    
    private func updateThemeUI(){
        self.topOrderIdLbl.textColor = self.viewModel?.orderDeliveryStatus?.data?.textColor ?? UIColor.theme
        self.btnReschedule.setTitleColor(self.viewModel?.orderDeliveryStatus?.data?.textColor ?? UIColor.deliveryThemeTextColor, for: .normal)
        self.btnReschedule.backgroundColor = self.viewModel?.orderDeliveryStatus?.data?.backgroudColor ?? UIColor.deliveryThemeColor
        self.btnViewDetails.setTitleColor(self.viewModel?.orderDeliveryStatus?.data?.backgroudColor ?? UIColor.deliveryThemeColor, for: .normal)
    }
    
    private func refreshAPICall() {
        guard let orderId = orderId else { return }
        

        viewModel?.getDeliveryStatus(orderId: orderId,isRequiredToShowLoader: true, completionHandler: {
            //update UI
            DispatchQueue.main.async {
                //sleep(5)
                self.imgBrandLogo.setImagePNG(url: self.viewModel?.orderDeliveryStatus?.data?.brand_logo ?? "", placeholderImage: nil)
                self.imgOutletLocationPin.image = UIImage(named: "BBQ-location-pin")
                if (self.viewModel?.orderDeliveryStatus?.data?.brand_name.lowercased().contains("dum") ?? false){
                    self.imgOutletLocationPin.image = UIImage(named: "Dumsafar-location-pin")
                }
                self.updateThemeUI()
                self.stopShimmering()
                self.viewForBillDetails.isHidden = false
                self.deliveryStatusLis = self.viewModel?.orderDeliveryStatus
                if self.deliveryStatusLis?.data?.transaction_type == 2{
                    //takeAway
                    self.contentViewForDelivery.isHidden = true
                    self.contentViewForTakeAway.isHidden = false
                    self.scrollView.isScrollEnabled = false
                    self.setUpUIForTakerAway()
                    self.setDataForTakeAwayItem(self.deliveryStatusLis)
                }else{
                    //Delivery
                    self.contentViewForTakeAway.isHidden = true
                    self.contentViewForDelivery.isHidden = false
                    self.scrollView.isScrollEnabled = true
                    self.scrollView.alwaysBounceVertical = true
                    self.scrollView.bounces  = true
                    self.refreshControl = UIRefreshControl()
                    self.refreshControl.addTarget(self, action: #selector(self.didPullToRefresh), for: .valueChanged)
                    self.scrollView.insertSubview((self.refreshControl)!, at: 0)

                    self.scrollView.addSubview((self.refreshControl)!)
                    self.fillUI(self.deliveryStatusLis)
                  //  self.updateDriverStatus()


                }


            }
        })
    }
    
    func updateDriverStatus() {
        
        //check if status is not delivered
        if deliveryStatusLis?.data?.current_status  != "delivered" {
            timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(refreshApiData), userInfo: nil, repeats: true)
            //refreshApiData() every five seconds until the timer is terminated,
            
        }else {
             timer.invalidate()
        }
        
    }
    
    @objc func refreshApiData(){
        
       
        AnalyticsHelper.shared.triggerEvent(type: .OS02)

        guard let orderId = orderId else { return }
        viewModel?.getDeliveryStatus(orderId: orderId,isRequiredToShowLoader: false, completionHandler: {
            //update UI
            DispatchQueue.main.async { [weak self] in
              
                self?.deliveryStatusLis = self?.viewModel?.orderDeliveryStatus
                if self?.deliveryStatusLis?.data?.transaction_type == 2{
                    //delevery
                    self?.contentViewForDelivery.isHidden = true
                    self?.contentViewForTakeAway.isHidden = false
                    self?.scrollView.isScrollEnabled = false
                    self?.setUpUIForTakerAway()
                    self?.setDataForTakeAwayItem(self?.deliveryStatusLis)
                }else{
                    //takeaway screen
                    self?.scrollView.alwaysBounceVertical = true
                    self?.scrollView.bounces  = true
                    self?.contentViewForTakeAway.isHidden = true
                    self?.contentViewForDelivery.isHidden = false
                    self?.scrollView.isScrollEnabled = true
                    self?.fillUI(self?.deliveryStatusLis)
                    
                }
                
            }
        })
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer.invalidate()
    }
    @objc func didPullToRefresh() {
        if    refreshControl?.isRefreshing == true {
            
            guard let orderId = orderId else { return }
            viewModel?.getDeliveryStatus(orderId: orderId,isRequiredToShowLoader: false,  completionHandler: {
                //update UI
                DispatchQueue.main.async { [self] in
                    // print(self.viewModel)
                    // For End refrshing
                    refreshControl?.endRefreshing()
                    resetUI()
                    self.deliveryStatusLis = self.viewModel?.orderDeliveryStatus
                    if self.deliveryStatusLis?.data?.transaction_type == 2{
                        //delevery
                        self.contentViewForDelivery.isHidden = true
                        self.contentViewForTakeAway.isHidden = false
                        self.scrollView.isScrollEnabled = false
                        self.setUpUIForTakerAway()
                        self.setDataForTakeAwayItem(self.deliveryStatusLis)
                    }else{
                        //takeaway screen
                        self.scrollView.alwaysBounceVertical = true
                        self.scrollView.bounces  = true
                        self.contentViewForTakeAway.isHidden = true
                        self.contentViewForDelivery.isHidden = false
                        self.scrollView.isScrollEnabled = true
                        self.fillUI(self.deliveryStatusLis)
                        
                    }
                    
                }
            })
        }


     }
    func setUI()  {
        
        // Reset float rating view's background color
        ratingView.backgroundColor = UIColor.clear
        /** Note: With the exception of contentMode, type and delegate,
         all properties can be set directly in Interface Builder **/
        ratingView.delegate = self
        ratingView.contentMode = UIView.ContentMode.scaleAspectFit
        ratingView.type = .wholeRatings

        viewForOrderConfirmedBorder.roundCorners(cornerRadius: 8.0, borderColor: UIColor(red: 0.753, green: 0.8, blue: 0.855, alpha: 1), borderWidth: 1.0)
        chefProfileImage.setCornerRadius(4.0)
        
        outerViewForDispatchTracking.roundCorners(cornerRadius: 8.0, borderColor: UIColor(red: 0.753, green: 0.8, blue: 0.855, alpha: 1), borderWidth: 1.0)
        viewForOTP.roundCorners(cornerRadius: 8.0, borderColor: UIColor(red: 0.753, green: 0.8, blue: 0.855, alpha: 1), borderWidth: 1.0)
       // viewForrOrderDispatchBorder.roundCorners(cornerRadius: 8.0, borderColor: UIColor(red: 0.753, green: 0.8, blue: 0.855, alpha: 1), borderWidth: 1.0)
        btnReschedule.roundCorners(cornerRadius: 8.0, borderColor: .clear, borderWidth: 1.0)
        
        if isAppReferAndEarnActivated == true {
            appReferalView.isHidden = false
            appReferalView.setShadow(color: .lightGray, opacity: 0.2, offSet: .zero, radius: 10.0, scale: true)
            appReferalViewForDelivery.setShadow(color: .lightGray, opacity: 0.2, offSet: .zero, radius: 10.0, scale: true)
            
            appReferalView.delegetToReferal = self
            appReferalViewForDelivery.delegetToReferal = self
        }else{
            appReferalView.isHidden = true

        }
      
    }
    
    
    func setUpUIForTakerAway()  {
        borderViewForTakeAway.roundCorners(cornerRadius: 8.0, borderColor: UIColor(red: 0.753, green: 0.8, blue: 0.855, alpha: 1), borderWidth: 1.0)
        borderViewForLocationTakeAway.roundCorners(cornerRadius: 8.0, borderColor: UIColor(red: 0.753, green: 0.8, blue: 0.855, alpha: 1), borderWidth: 1.0)
        
        rateViewForTakeAway.roundCorners(cornerRadius: 8.0, borderColor: UIColor(red: 0.753, green: 0.8, blue: 0.855, alpha: 1), borderWidth: 1.0)
       
        // Reset float rating view's background color
        rateStarViewForTakeAway.backgroundColor = UIColor.clear
        /** Note: With the exception of contentMode, type and delegate,
         all properties can be set directly in Interface Builder **/
        rateStarViewForTakeAway.delegate = self
        rateStarViewForTakeAway.contentMode = UIView.ContentMode.scaleAspectFit
        rateStarViewForTakeAway.type = .wholeRatings
        rateViewForTakeAway.isHidden = true
        btnReschedule.isHidden = !(deliveryStatusLis?.data?.reschedule_order ?? false)

       if (deliveryStatusLis?.data?.current_status) == "delivered" {
        btnReschedule.isHidden = true
        rateViewForTakeAway.isHidden = false
        rateStarViewForTakeAway.delegate = self
       }
        
        if btnReschedule.isHidden{
            heighForTakewayConfirm.constant = 180
        }else{
            heighForTakewayConfirm.constant = 230
        }
    }
    //update description on order received status

        
    func setDataForTakeAwayItem(_ deliveryStatusLis: DeliveryStatusResponse?)  {
        
        takeAwayPickUpTime.text = deliveryStatusLis?.data?.delivery_time
        takeAwayPickUpAddress.text = deliveryStatusLis?.data?.branch_address
        takeAwayPickupBranchName.text  = deliveryStatusLis?.data?.branch_name
        labelPickUpOTP.text = deliveryStatusLis?.data?.pickup_otp
        
        //Set description dep[ending on title
        
        switch (deliveryStatusLis?.data?.current_status?.lowercased()) {
        
        case "received" :
            lblTakeAwayStatusDescription.text = "Your order has been scheduled. Reach the outlet on the scheduled date."
            break
            
            
        case "accepted":
            lblTakeAwayStatusDescription.text = "Your order has been scheduled. Reach the outlet on the scheduled date."
            
            break
        case "prepared":
            lblTakeAwayStatusDescription.text = "Our chef is preparing your food."
            break
            
        case "dispatched":

            lblTakeAwayStatusDescription.text = "Your order has been picked-up."
            break
            
            
        case "delivered" :
            lblTakeAwayStatusDescription.text = "Your order has been picked-up."

            break
            
        default:
            print("This condition should not occur")
        }
        setHeaderAndBottomLabelData()
        
    }
    func fillUI(_ deliveryStatusLis: DeliveryStatusResponse?) {
        
        
        bottomConstraintForDeliveryToRateView.constant = 0
        rateLabel.text = ""
        ratingView.isHidden = true
        
        let unselectedButtonColor = UIColor(red: 0.514, green: 0.569, blue: 0.655, alpha: 1) // gray
        let selectedButtonColor = deliveryStatusLis?.data?.backgroudColor ?? .deliveryThemeColor // blue
        let nowButtonHighlightColor = deliveryStatusLis?.data?.backgroudColor ?? .deliveryThemeColor
        let borderWidth :CGFloat = 1.0
        let doneImage = UIImage(named: "success_green")
        self.deliveryStatusLis = deliveryStatusLis
        
        resetUI()
        //Set time for all status
        labelForOrderReceivedTime.text =   changeFormat(dateTimeString: deliveryStatusLis?.data?.received?.time ?? "")
        labelForOrderPrepareTime.text =  changeFormat(dateTimeString:deliveryStatusLis?.data?.prepared?.time ?? "")
        labelForOrderConfirmedTime.text = changeFormat(dateTimeString:deliveryStatusLis?.data?.accepted?.time ?? "" )
        labelForOrderDispatchTime.text = changeFormat(dateTimeString:deliveryStatusLis?.data?.dispatched?.time ?? "" )
        labelForOrderDeliverTime.text = changeFormat(dateTimeString:deliveryStatusLis?.data?.delivered?.time ?? "" )
        
        titleHeaderLabelOrderReceived.text = deliveryStatusLis?.data?.received?.current_title ?? "Received"
        titleHeaderLabelOrderConfirmed.text = deliveryStatusLis?.data?.accepted?.current_title ?? "Accepted"
        titleHeaderLabelOrderPrepare.text = deliveryStatusLis?.data?.prepared?.current_title ?? "Prepared"
        titleHeaderLabelOrderDispatch.text = deliveryStatusLis?.data?.dispatched?.current_title ?? "Dispatched"
        titleHeaderLabelOrderDelivered.text = deliveryStatusLis?.data?.delivered?.current_title ?? "Delivered"
        //If status is received , set progressview progess as 0.0
      //  progressView.progress = 0.0
//(textfeildOrderNumber.text) {
        outerViewForDispatchTracking.isHidden = true
        bottomConstraintDispatch.constant = 0
        viewForOTP.isHidden = true
        
        switch (deliveryStatusLis?.data?.current_status) {
        
        case "received" :
            
            AnalyticsHelper.shared.triggerEvent(type: .OS05A)

            btnTitleForOrderReceived.layer.borderColor = nowButtonHighlightColor.cgColor
            btnTitleForOrderReceived.textColor = nowButtonHighlightColor
            btnTitleForOrderReceived.text = "Now"
          //  btnTitleForOrderReceived.font = UIFont.appThemeExtraBoldWith(size: 14)
            titleHeaderLabelOrderReceived.font = UIFont.appThemeBoldWith(size: 16)

            btnTitleForOrderReceived.layer.borderWidth = borderWidth + 1
            btnTitleForOrderReceived.layer.cornerRadius  = 2.0
            btnTitleForOrderReceived.backgroundColor = .clear
            labelForOrderReceived.text = deliveryStatusLis?.data?.received?.description
            lineForOrderReceived.isHidden = true
            bottomConstraintReceived.constant = -60
           
            //NEXT all label color of for NEXT
            btnTitleForOrderPrepare.layer.borderColor = unselectedButtonColor.cgColor
            btnTitleForOrderPrepare.textColor = unselectedButtonColor
            btnTitleForOrderPrepare.text = "NEXT"
            btnTitleForOrderPrepare.layer.borderWidth = borderWidth
            btnTitleForOrderPrepare.layer.cornerRadius  = 2.0
            btnTitleForOrderPrepare.backgroundColor = .clear
            lineForOrderPrepare.isHidden = true
            bottomConstraintPrepared.constant = -60

            btnTitleForOrderDispatch.layer.borderColor = unselectedButtonColor.cgColor
            btnTitleForOrderDispatch.backgroundColor = .clear
            btnTitleForOrderDispatch.textColor = unselectedButtonColor
            btnTitleForOrderDispatch.text = "NEXT"
            lineForOrderDispatch.isHidden = true
            btnTitleForOrderDispatch.layer.borderWidth = borderWidth
            btnTitleForOrderDispatch.layer.cornerRadius  = 2.0
            
            bottomConstraintDispatch.constant = -50

            btnTitleOrderConfirmed.backgroundColor = .clear
           // titleHeaderLabelOrderConfirmed.text = "Order Confirmation"

            btnTitleOrderConfirmed.layer.borderColor = unselectedButtonColor.cgColor
            btnTitleOrderConfirmed.textColor = unselectedButtonColor
            btnTitleOrderConfirmed.text = "NEXT"
            lineForOrderCofirmed.isHidden = true
            btnTitleOrderConfirmed.layer.cornerRadius  = 2.0
            btnTitleOrderConfirmed.layer.borderWidth = borderWidth
            btnTitleOrderConfirmed.backgroundColor = .clear
            viewForOrderConfirmedBorder.isHidden = true
            stackViewForCertificate.isHidden = true

            btnTitleForOrderDeliver.backgroundColor = .clear
            btnTitleForOrderDeliver.layer.borderWidth = borderWidth
            btnTitleForOrderDeliver.text = "NEXT"
            //  lineForOrderDeliver.isHidden = true
            btnTitleForOrderDeliver.layer.borderColor = unselectedButtonColor.cgColor
            btnTitleForOrderDeliver.textColor = unselectedButtonColor
            btnTitleForOrderDeliver.layer.cornerRadius  = 2.0
            bottomConstraintDelivery.constant = -60

            break
            
            
        case "accepted":
            AnalyticsHelper.shared.triggerEvent(type: .OS05B)

            btnTitleForOrderReceived.layer.borderColor = selectedButtonColor.cgColor
            btnTitleForOrderReceived.textColor = selectedButtonColor
            btnTitleForOrderReceived.text = "DONE"
            btnTitleForOrderReceived.layer.borderWidth = borderWidth
            btnTitleForOrderReceived.layer.cornerRadius  = 2.0
            btnTitleForOrderReceived.backgroundColor = .clear
            labelForOrderReceived.text = ""
            dashedViewForOrderReceived.isHidden = true
            lineForOrderReceived.isHidden = false
            bottomConstraintReceived.constant = -60
            btnTitleForOrderReceived.widthAnchor.constraint(equalToConstant:  btnTitleForOrderReceived.intrinsicContentSize.width + 2 * 8).isActive = true
            
            btnTitleForOrderReceived.isHidden = true
            imageViewForOrderReceived.image = doneImage
            
            btnTitleOrderConfirmed.layer.borderColor = nowButtonHighlightColor.cgColor
            btnTitleOrderConfirmed.textColor = nowButtonHighlightColor
            btnTitleOrderConfirmed.text = "Now"
            btnTitleOrderConfirmed.font = UIFont.appThemeExtraBoldWith(size: 14)
           // titleHeaderLabelOrderConfirmed.font = UIFont.appThemeBoldWith(size: 16)
            btnTitleOrderConfirmed.layer.borderWidth = borderWidth + 1
            btnTitleOrderConfirmed.layer.cornerRadius  = 2.0
            btnTitleOrderConfirmed.backgroundColor = .clear
            labelForOrderConfirmed.text = deliveryStatusLis?.data?.accepted?.description
            viewForOrderConfirmedBorder.isHidden = false
            stackViewForCertificate.isHidden = false
            if let vaccination_link = deliveryStatusLis?.data?.prepared?.vaccination_link, vaccination_link != ""{
                btnViewCerti.isHidden = false
                lblViewCerti.isHidden = true
            }else{
                btnViewCerti.isHidden = true
                lblViewCerti.isHidden = false
            }
            
            btnTitleOrderConfirmed.isHidden = true
            imageViewForOrderConfirmed.image = doneImage
            dashedViewForOrderConfirmed.isHidden = true
            lineForOrderCofirmed.isHidden = false


            bottomConstraintPrepared.constant = -50 //-30 changes now

//            btnTitleForOrderPrepare.layer.borderColor = unselectedButtonColor.cgColor
//            btnTitleForOrderPrepare.textColor = unselectedButtonColor
//            btnTitleForOrderPrepare.text = "NEXT"
//            btnTitleForOrderPrepare.layer.borderWidth = borderWidth
//            btnTitleForOrderPrepare.layer.cornerRadius  = 2.0
//            btnTitleForOrderPrepare.backgroundColor = .clear
//            lineForOrderPrepare.isHidden = true
            labelForOrderPrepare.text = deliveryStatusLis?.data?.prepared?.description

            btnTitleForOrderPrepare.textColor = nowButtonHighlightColor
            btnTitleForOrderPrepare.text = "NOW"
           // btnTitleForOrderPrepare.font = UIFont.appThemeExtraBoldWith(size: 14)
            titleHeaderLabelOrderPrepare.font = UIFont.appThemeBoldWith(size: 16)
            btnTitleForOrderPrepare.layer.borderWidth = borderWidth + 1
            btnTitleForOrderPrepare.layer.cornerRadius  = 2.0
            btnTitleForOrderPrepare.backgroundColor = .clear
            btnTitleForOrderPrepare.layer.borderColor = nowButtonHighlightColor.cgColor
           // btnTitleForOrderPrepare.text = deliveryStatusLis?.data?.accepted?.description
            
        
            
            
            btnTitleForOrderDispatch.layer.borderColor = unselectedButtonColor.cgColor
            btnTitleForOrderDispatch.backgroundColor = .clear
            btnTitleForOrderDispatch.textColor = unselectedButtonColor
            btnTitleForOrderDispatch.text = "NEXT"
            lineForOrderDispatch.isHidden = true
            btnTitleForOrderDispatch.layer.borderWidth = borderWidth
            btnTitleForOrderDispatch.layer.cornerRadius  = 2.0
            bottomConstraintDispatch.constant =  0 //-50// change for new flow

            btnTitleForOrderDeliver.backgroundColor = .clear
            btnTitleForOrderDeliver.layer.borderWidth = borderWidth
            btnTitleForOrderDeliver.text = "NEXT"
            // lineForOrderDeliver.isHidden = true
            btnTitleForOrderDeliver.layer.borderColor = unselectedButtonColor.cgColor
            btnTitleForOrderDeliver.textColor = unselectedButtonColor
            btnTitleForOrderDeliver.layer.cornerRadius  = 2.0
            bottomConstraintDelivery.constant = -60
            
            

            break
        case "prepared":
            
            AnalyticsHelper.shared.triggerEvent(type: .OS05C)

            btnTitleForOrderReceived.layer.borderColor = selectedButtonColor.cgColor
            btnTitleForOrderReceived.textColor = selectedButtonColor
            btnTitleForOrderReceived.text = "DONE"
            btnTitleForOrderReceived.layer.borderWidth = borderWidth
            btnTitleForOrderReceived.layer.cornerRadius  = 2.0
            btnTitleForOrderReceived.backgroundColor = .clear
            labelForOrderReceived.text = ""
            dashedViewForOrderReceived.isHidden = true
            lineForOrderReceived.isHidden = false
            bottomConstraintReceived.constant = -60
            btnTitleForOrderReceived.widthAnchor.constraint(equalToConstant:  btnTitleForOrderReceived.intrinsicContentSize.width + 2 * 8).isActive = true
            btnTitleForOrderReceived.isHidden = true
            imageViewForOrderReceived.image = doneImage
            
            
            btnTitleOrderConfirmed.layer.borderColor = selectedButtonColor.cgColor
            btnTitleOrderConfirmed.textColor = selectedButtonColor
            btnTitleOrderConfirmed.text = "DONE"
            btnTitleOrderConfirmed.layer.borderWidth = borderWidth
            btnTitleOrderConfirmed.layer.cornerRadius  = 2.0
            btnTitleOrderConfirmed.backgroundColor = .clear
            labelForOrderConfirmed.text = ""
            dashedViewForOrderConfirmed.isHidden = true
            lineForOrderCofirmed.isHidden = false
         //   bottomConstraintconfirmed.constant = 20
            viewForOrderConfirmedBorder.isHidden = false
            stackViewForCertificate.isHidden = false
            if let vaccination_link = deliveryStatusLis?.data?.prepared?.vaccination_link, vaccination_link != ""{
                btnViewCerti.isHidden = false
                lblViewCerti.isHidden = true
            }else{
                btnViewCerti.isHidden = true
                lblViewCerti.isHidden = false
            }
            
            
            btnTitleOrderConfirmed.isHidden = true
            imageViewForOrderConfirmed.image = doneImage
           // labelForOrderConfirmed.text = deliveryStatusLis?.data?.accepted?.description

            btnTitleForOrderPrepare.layer.borderColor = nowButtonHighlightColor.cgColor
            btnTitleForOrderPrepare.textColor = nowButtonHighlightColor
            btnTitleForOrderPrepare.text = "Now"
            btnTitleForOrderPrepare.font = UIFont.appThemeExtraBoldWith(size: 14)
           // titleHeaderLabelOrderPrepare.font = UIFont.appThemeBoldWith(size: 16)
            chefProfileImageWidthConstarint.constant = 56 //changes for new flow

            btnTitleForOrderPrepare.layer.borderWidth = borderWidth + 1
            btnTitleForOrderPrepare.layer.cornerRadius  = 2.0
            btnTitleForOrderPrepare.backgroundColor = .clear
            labelForOrderPrepare.text = deliveryStatusLis?.data?.prepared?.description
            bottomConstraintPrepared.constant = -60 //space between accept and prepare
            
            lineForOrderPrepare.isHidden = false
            dashedViewForOrderPrepare.isHidden = true
            btnTitleForOrderPrepare.isHidden = true
            imageViewForOrderPrepare.image = doneImage
            
            btnTitleForOrderDispatch.layer.borderColor = nowButtonHighlightColor.cgColor
            btnTitleForOrderDispatch.backgroundColor = .clear
            btnTitleForOrderDispatch.textColor = nowButtonHighlightColor
            btnTitleForOrderDispatch.text = "NOW"
           // btnTitleForOrderDispatch.font = UIFont.appThemeExtraBoldWith(size: 14)
            titleHeaderLabelOrderDispatch.font = UIFont.appThemeBoldWith(size: 16)
            lineForOrderDispatch.isHidden = true
            btnTitleForOrderDispatch.layer.borderWidth = borderWidth + 1
            btnTitleForOrderDispatch.layer.cornerRadius  = 2.0
            dashedViewForOrderDispatch.isHidden = false
            bottomConstraintDispatch.constant = 0

            
            btnTitleForOrderDeliver.backgroundColor = .clear
            btnTitleForOrderDeliver.layer.borderWidth = borderWidth
            btnTitleForOrderDeliver.text = "NEXT"
            //  lineForOrderDeliver.isHidden = true
            btnTitleForOrderDeliver.layer.borderColor = unselectedButtonColor.cgColor
            btnTitleForOrderDeliver.textColor = unselectedButtonColor
            btnTitleForOrderDeliver.layer.cornerRadius  = 2.0
            bottomConstraintDelivery.constant = -60

            break
            
        case "dispatched":
            
            AnalyticsHelper.shared.triggerEvent(type: .OS05D)

            btnTitleForOrderReceived.layer.borderColor = selectedButtonColor.cgColor
            btnTitleForOrderReceived.textColor = selectedButtonColor
            btnTitleForOrderReceived.text = "DONE"
            btnTitleForOrderReceived.layer.borderWidth = borderWidth
            btnTitleForOrderReceived.layer.cornerRadius  = 2.0
            btnTitleForOrderReceived.backgroundColor = .clear
            labelForOrderReceived.text = ""
            dashedViewForOrderReceived.isHidden = true
            lineForOrderReceived.isHidden = false
            bottomConstraintReceived.constant = -60
            btnTitleForOrderReceived.widthAnchor.constraint(equalToConstant:  btnTitleForOrderReceived.intrinsicContentSize.width + 2 * 8).isActive = true
            
            
            btnTitleForOrderReceived.isHidden = true
            imageViewForOrderReceived.image = doneImage
            
            btnTitleOrderConfirmed.layer.borderColor = selectedButtonColor.cgColor
            btnTitleOrderConfirmed.textColor = selectedButtonColor
            btnTitleOrderConfirmed.text = "DONE"
            btnTitleOrderConfirmed.layer.borderWidth = borderWidth
            btnTitleOrderConfirmed.layer.cornerRadius  = 2.0
            btnTitleOrderConfirmed.backgroundColor = .clear
            labelForOrderConfirmed.text = ""
            dashedViewForOrderConfirmed.isHidden = true
            lineForOrderCofirmed.isHidden = false
         //   bottomConstraintconfirmed.constant = 20

            viewForOrderConfirmedBorder.isHidden = true
            stackViewForCertificate.isHidden = true
            
            btnTitleOrderConfirmed.isHidden = true
            imageViewForOrderConfirmed.image = doneImage
            
            btnTitleForOrderPrepare.layer.borderColor = selectedButtonColor.cgColor
            btnTitleForOrderPrepare.textColor = selectedButtonColor
            btnTitleForOrderPrepare.text = "DONE"
            btnTitleForOrderPrepare.layer.borderWidth = borderWidth
            btnTitleForOrderPrepare.layer.cornerRadius  = 2.0
            btnTitleForOrderPrepare.backgroundColor = .clear
            labelForOrderPrepare.text = ""
            dashedViewForOrderPrepare.isHidden = true
            lineForOrderPrepare.isHidden = false
            bottomConstraintPrepared.constant = -60
           // titleHeaderLabelOrderPrepare.text = "Prepared"
            btnTitleForOrderPrepare.isHidden = true
            imageViewForOrderPrepare.image = doneImage

            btnTitleForOrderDispatch.layer.borderColor = nowButtonHighlightColor.cgColor
            btnTitleForOrderDispatch.textColor = nowButtonHighlightColor
            btnTitleForOrderDispatch.text = "Now"
          //  btnTitleForOrderDispatch.font = UIFont.appThemeExtraBoldWith(size: 14)
            titleHeaderLabelOrderDispatch.font = UIFont.appThemeBoldWith(size: 16)

            btnTitleForOrderDispatch.layer.borderWidth = borderWidth + 1
            btnTitleForOrderDispatch.layer.cornerRadius  = 2.0
            btnTitleForOrderDispatch.backgroundColor = .clear
            lineForOrderDispatch.isHidden = true
            outerViewForDispatchTracking.isHidden = false
            bottomConstraintDispatch.constant = -50
            driverDescriptionLabelDispatch.text = deliveryStatusLis?.data?.dispatched?.description
            if let otp = deliveryStatusLis?.data?.pickup_otp, otp.count > 0{
                viewForOTP.isHidden = false
                lblDeliveryOTP.text = otp
            }
            
            
            btnTitleForOrderDispatch.isHidden = true
            imageViewForOrderDispatch.image = doneImage
            
            
            bottomConstraintDelivery.constant = 0
            btnTitleForOrderDeliver.backgroundColor = .clear
            btnTitleForOrderDeliver.layer.borderWidth = borderWidth
            btnTitleForOrderDeliver.text = "NEXT"
            // lineForOrderDeliver.isHidden = true
            //btnTitleForOrderDeliver.font = UIFont.appThemeExtraBoldWith(size: 14)
            //titleHeaderLabelOrderDelivered.font = UIFont.appThemeBoldWith(size: 16)
            btnTitleForOrderDeliver.layer.borderColor = unselectedButtonColor.cgColor
            btnTitleForOrderDeliver.textColor = unselectedButtonColor
            btnTitleForOrderDeliver.layer.cornerRadius  = 2.0

                        
            //update progress view
            updateProgressView()
            break
            
            
        case "delivered" :
            
            AnalyticsHelper.shared.triggerEvent(type: .OS05E)

            //when status is delivered, give option to rate you food
            ratingView.isHidden = false
            bottomConstraintForDeliveryToRateView.constant = 40
            rateLabel.text = "RATE YOUR FOOD"

            btnTitleForOrderReceived.layer.borderColor = selectedButtonColor.cgColor
            btnTitleForOrderReceived.textColor = selectedButtonColor
            btnTitleForOrderReceived.text = "DONE"
            btnTitleForOrderReceived.layer.borderWidth = borderWidth
            btnTitleForOrderReceived.layer.cornerRadius  = 2.0
            btnTitleForOrderReceived.backgroundColor = .clear
            labelForOrderReceived.text = ""
            dashedViewForOrderReceived.isHidden = true
            lineForOrderReceived.isHidden = false
            bottomConstraintReceived.constant = -60
            btnTitleForOrderReceived.isHidden = true
            imageViewForOrderReceived.image = doneImage
            
            btnTitleOrderConfirmed.layer.borderColor = selectedButtonColor.cgColor
            btnTitleOrderConfirmed.textColor = selectedButtonColor
            btnTitleOrderConfirmed.text = "DONE"
            btnTitleOrderConfirmed.layer.borderWidth = borderWidth
            btnTitleOrderConfirmed.layer.cornerRadius  = 2.0
            btnTitleOrderConfirmed.backgroundColor = .clear
            labelForOrderConfirmed.text = ""
            dashedViewForOrderConfirmed.isHidden = true
            lineForOrderCofirmed.isHidden = false
            viewForOrderConfirmedBorder.isHidden = true
            stackViewForCertificate.isHidden = true
            labelForOrderConfirmed.text = ""
            btnTitleOrderConfirmed.isHidden = true
            imageViewForOrderConfirmed.image = doneImage

            btnTitleForOrderPrepare.layer.borderColor = selectedButtonColor.cgColor
            btnTitleForOrderPrepare.textColor = selectedButtonColor
            btnTitleForOrderPrepare.text = "DONE"
            btnTitleForOrderPrepare.layer.borderWidth = borderWidth
            btnTitleForOrderPrepare.layer.cornerRadius  = 2.0
            btnTitleForOrderPrepare.backgroundColor = .clear
            labelForOrderPrepare.text = ""
            dashedViewForOrderPrepare.isHidden = true
            lineForOrderPrepare.isHidden = false
            bottomConstraintPrepared.constant = -60
            
            
            btnTitleForOrderPrepare.isHidden = true
            imageViewForOrderPrepare.image = doneImage
            
            btnTitleForOrderDispatch.layer.borderColor = selectedButtonColor.cgColor
            btnTitleForOrderDispatch.textColor = selectedButtonColor
            btnTitleForOrderDispatch.text = "DONE"
            btnTitleForOrderDispatch.layer.borderWidth = borderWidth
            btnTitleForOrderDispatch.layer.cornerRadius  = 2.0
            btnTitleForOrderDispatch.backgroundColor = .clear
            dashedViewForOrderDispatch.isHidden = true
            lineForOrderDispatch.isHidden = false
            outerViewForDispatchTracking.isHidden = true
            bottomConstraintDispatch.constant = -50
            viewForOTP.isHidden = true
            
            
            btnTitleForOrderDispatch.isHidden = true
            imageViewForOrderDispatch.image = doneImage
            
            bottomConstraintDelivery.constant = -50
            btnTitleForOrderDeliver.layer.borderColor =  nowButtonHighlightColor.cgColor
            btnTitleForOrderDeliver.textColor = nowButtonHighlightColor
          //  titleHeaderLabelOrderDelivered.text = "Delivered"
            titleHeaderLabelOrderDelivered.font = UIFont.appThemeBoldWith(size: 16)
            btnTitleForOrderDeliver.text = "DONE"
            btnTitleForOrderDeliver.font = UIFont.appThemeExtraBoldWith(size: 14)
            btnTitleForOrderDeliver.layer.borderWidth = borderWidth + 1
            btnTitleForOrderDeliver.layer.cornerRadius  = 2.0
            btnTitleForOrderDeliver.backgroundColor = .clear
            lineForOrderDeliver.isHidden = false

            btnTitleForOrderDeliver.isHidden = true
            imageViewForOrderDelivered.image = doneImage
            updateProgressView()


            break
            
        default:
            print("This condition should not occur")
        }
        //set intrinsic width
        btnTitleForOrderDeliver.widthAnchor.constraint(equalToConstant:  btnTitleForOrderDeliver.intrinsicContentSize.width + 2 * 5).isActive = true
        
        btnTitleForOrderDeliver.heightAnchor.constraint(equalToConstant:  btnTitleForOrderDeliver.intrinsicContentSize.height + 2 * 3).isActive = true
        
        btnTitleForOrderReceived.widthAnchor.constraint(equalToConstant:  btnTitleForOrderReceived.intrinsicContentSize.width + 2 * 5).isActive = true
        btnTitleForOrderReceived.heightAnchor.constraint(equalToConstant:  btnTitleForOrderReceived.intrinsicContentSize.height + 2 * 3).isActive = true
        
        btnTitleOrderConfirmed.widthAnchor.constraint(equalToConstant:  btnTitleOrderConfirmed.intrinsicContentSize.width + 2 * 5).isActive = true
        btnTitleOrderConfirmed.heightAnchor.constraint(equalToConstant:  btnTitleOrderConfirmed.intrinsicContentSize.height + 2 * 3).isActive = true
        
        btnTitleForOrderDispatch.widthAnchor.constraint(equalToConstant:  btnTitleForOrderDispatch.intrinsicContentSize.width + 2 * 5).isActive = true
        btnTitleForOrderDispatch.heightAnchor.constraint(equalToConstant:  btnTitleForOrderDispatch.intrinsicContentSize.height + 2 * 3).isActive = true
        
        btnTitleForOrderPrepare.heightAnchor.constraint(equalToConstant:  btnTitleForOrderPrepare.intrinsicContentSize.height + 2 * 3).isActive = true
        btnTitleForOrderPrepare.widthAnchor.constraint(equalToConstant:  btnTitleForOrderPrepare.intrinsicContentSize.width + 2 * 5).isActive = true
        setHeaderAndBottomLabelData()
       
    }
    
    private func setHeaderAndBottomLabelData(){
        
        //Update the bill ui
        //if bill amounyt is 0 , dop not show the saving details
        if deliveryStatusLis?.data?.discount_text != "0" {
            let billtext = String(format: "You saved ₹%@ on this order. ", deliveryStatusLis?.data?.discount_text ?? 0)
            
            let range = (billtext as NSString?)?.range(of: billtext.numeralsOnlyFromString )
            
            if range != nil {
                let attributedString = NSMutableAttributedString(string:billtext )
                attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(red: 0.020, green: 0.651, blue: 0.376, alpha: 1), range: range!)
                attributedString.addAttribute(NSAttributedString.Key.font, value:UIFont.appThemeBoldWith(size: 16.0), range: range!)
                bottomTotalBillSavingLbl.attributedText = attributedString

            }
            else{
                self.bottomTotalBillSavingLbl.text = billtext
                
            }
        }
        //update the navigation head UI
        bottomTotalBillLbl.text =   "₹ \(String(format: "%02d",  deliveryStatusLis?.data?.bill_total ?? "0")) "

        let orderNumber = NSMutableAttributedString(string: "ORDER", attributes: [NSAttributedString.Key.font: UIFont.appThemeRegularWith(size: 16.0), NSAttributedString.Key.foregroundColor: UIColor.text])
        let orderNumberID =
        NSAttributedString(string: " #" + (String(deliveryStatusLis?.data?.order_id ?? 0)), attributes: [NSAttributedString.Key.font: UIFont.appThemeMediumWith(size: 16.0), NSAttributedString.Key.foregroundColor: deliveryStatusLis?.data?.backgroudColor ?? UIColor.deliveryThemeColor] )
      
        orderNumber.append(orderNumberID)
        topOrderIdLbl.attributedText = orderNumber
        topOrderDateLbl.text = self.deliveryStatusLis?.data?.order_time
        
    }
    
    private func setStatusView(label: UILabel, selectedButtonColor: UIColor, text: String, borderWidth: CGFloat = 1.0, statusLabel: UILabel, dashedView: CustomDashedView, imageView: UIImageView){
        label.layer.borderColor = selectedButtonColor.cgColor
        label.textColor = selectedButtonColor
        label.text = text
        label.layer.borderWidth = borderWidth
        label.layer.cornerRadius  = 2.0
        label.backgroundColor = .clear
        statusLabel.text = ""
        dashedView.isHidden = true
        imageView.isHidden = false
    }
    
    func resetUI()  {
        
        titleHeaderLabelOrderReceived.font = UIFont.appThemeSemiBoldWith(size: 16)
        titleHeaderLabelOrderConfirmed.font = UIFont.appThemeSemiBoldWith(size: 16)
        titleHeaderLabelOrderPrepare.font = UIFont.appThemeSemiBoldWith(size: 16)
        titleHeaderLabelOrderDispatch.font = UIFont.appThemeSemiBoldWith(size: 16)
        titleHeaderLabelOrderDelivered.font = UIFont.appThemeSemiBoldWith(size: 16)
        btnTitleForOrderReceived.font = UIFont.appThemeExtraBoldWith(size: 10)
        btnTitleOrderConfirmed.font = UIFont.appThemeExtraBoldWith(size: 10)
        btnTitleForOrderPrepare.font = UIFont.appThemeExtraBoldWith(size: 10)
        btnTitleForOrderDispatch.font = UIFont.appThemeExtraBoldWith(size: 10)
        btnTitleForOrderDeliver.font = UIFont.appThemeExtraBoldWith(size: 10)
        
        

    }
    @IBAction func callDeliveryBoyBtnPressed(_ sender : Any){
        
        AnalyticsHelper.shared.triggerEvent(type: .OS06)

        if deliveryStatusLis?.data?.dispatched?.rider_number != "" {
            //call the rider
            UIUtils.callNumber(phoneNumber: deliveryStatusLis?.data?.dispatched?.rider_number ?? "")
        }
    }
    
    
    @objc func updateProgressView() {
        
        //Get the current status, calculate the progress, update the progress view
        //In case of received, accepted, prepared we are not displaying progress bar
        //So check case from dispatched
        UIView.animate(withDuration: 1.0,
                       delay: 0.0,
                       options: [.curveLinear],
                       animations: { [weak self] in
                        let estimatedTime = (self?.deliveryStatusLis?.data?.eta_mins) ?? 40
                        let currentEstimatedTime = self?.deliveryStatusLis?.data?.current_eta ?? 0
                        
                        
                        var progressPercentage  = 1.0
                        if estimatedTime != 0 && currentEstimatedTime != 0 {
                            progressPercentage = Double(  currentEstimatedTime) / Double( estimatedTime )
                        }
                        
                        let xPoint = CGFloat(1.0 - progressPercentage) * (self?.progressView.frame.size.width  ?? 0.0)
                        
                        //In case of delivery done
                        if xPoint == 0 && self?.deliveryStatusLis?.data?.current_status == "delivered" {
                            self?.deliveryTrackConstaint.constant =  (self?.progressView.frame.size.width ?? 0) - 104
                        }else if xPoint <= 64{
                            self?.deliveryTrackConstaint.constant = 0
                        }else if self?.progressView.frame.size.width ?? 0.0 <= xPoint + 40{
                            self?.deliveryTrackConstaint.constant = (self?.progressView.frame.size.width ?? 0) - 104
                        }else{
                            self?.deliveryTrackConstaint.constant = xPoint - 64
                        }
                        self?.view.layoutIfNeeded()
                   
                        switch (self?.deliveryStatusLis?.data?.current_status)  {
                        
                        
                        
                        
                        case "delivered" :
                            
                            let progressPercentage =  1.0
                            print("progressPercent \(progressPercentage)")
                            
                            self?.progressView.progress = Float(progressPercentage)
                            self?.progressView.setProgress(self?.progressView.progress ?? 0, animated: true)
                            break
                            
                            
                        default:
                            self?.progressView.progress = Float(1.0 - progressPercentage)
                            self?.progressView.setProgress(self?.progressView.progress ?? 0, animated: true)        }
                        
                       }) { (isCompleted) in
            self.view.layoutIfNeeded()
        }
        
        
    }
    
    

    @IBAction func viewBillDetailsButtonPressed(_ sender : Any){
        

        
        AnalyticsHelper.shared.triggerEvent(type: .OS03)

        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: self.view.frame.size.height * 0.5)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        let directionBottomSheet = BBQBillDetailsViewController(nibName: "BBQBillDetailsViewController", bundle: nil)
        directionBottomSheet.orderDetails  = deliveryStatusLis ?? nil// self.orderDetails
        directionBottomSheet.orderIdVal = orderId
        directionBottomSheet.delegate = self
       // bottomSheetController.present(directionBottomSheet, on: self)
        
        directionBottomSheet.modalPresentationStyle = .overFullScreen
        self.present(directionBottomSheet, animated: true, completion: nil)
        
    }
    @IBAction func onClickBtnReschedule(_ sender: Any) {
        guard let orderId = orderId else {
            return
        }
        viewModel?.getPickSlotsAvail(orderId: orderId, completionHandler: {
            self.openTakeAwayPicker()
        })
    }
    @IBAction func onClickBtnViewCerti(_ sender: Any) {
        if let vaccination_link = deliveryStatusLis?.data?.prepared?.vaccination_link, vaccination_link != "vaccination_link", vaccination_link != ""{
            let webView = UIStoryboard.loadCommonWebView()
            webView.URLSring = vaccination_link
            webView.strTitle = "Vaccination Certificate"
            webView.orderID = "Vaccination Certificate"
            webView.shareOption = false
            self.present(webView, animated: true, completion: nil)
        }
    }
    
    private func openTakeAwayPicker() {
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: 80)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        let directionBottomSheet = BBQTakeAwayTimeVC(nibName: "BBQTakeAwayTimeVC", bundle: nil)
        directionBottomSheet.pickUpAvailabilityTime = viewModel?.pickSlots?.data ?? [PickUpDateTime]()
        directionBottomSheet.delegate = self
        directionBottomSheet.strTitle = "Reschedule Takeaway"
        directionBottomSheet.backgroundColor = self.viewModel?.orderDeliveryStatus?.data?.backgroudColor ?? UIColor.deliveryThemeColor
        directionBottomSheet.textColor = self.viewModel?.orderDeliveryStatus?.data?.textColor ?? UIColor.deliveryThemeTextColor
        bottomSheetController.present(directionBottomSheet, on: self)
    }
}
    
    
extension BBQOrderStatusVIewControllerViewController: FloatRatingViewDelegate {

    // MARK: FloatRatingViewDelegate
    
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double) {
        print( String(format: "%.2f", self.ratingView.rating))
        print( String(format: "%.2f", self.rateStarViewForTakeAway.rating))
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
        let  controller = UIStoryboard.loadFeedBackDetailsViewController()
        controller.userRatingValue =  orderTransactionType == 2 ? self.rateStarViewForTakeAway.rating
 : self.ratingView.rating

       // controller.userRatingValue = self.ratingView.rating
        controller.orderID = orderId
        controller.transactionType  = orderTransactionType
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    
}
extension BBQOrderStatusVIewControllerViewController {
    
    @IBAction func backButtonPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionOnCall() {
        
        AnalyticsHelper.shared.triggerEvent(type: .OS07)

//     guard let status = textfeildOrderNumber.text else { return }
//
//        fillUI(deliveryStatusLis)
//        return
        UIUtils.callNumber(phoneNumber: viewModel?.orderDeliveryStatus?.data?.branch_contact_no ?? "")
    }
}

extension BBQOrderStatusVIewControllerViewController {
    
    func convertStatusTimeStringToDate(DateString: String) -> Date? {
        
        //      "add_info": "2021/02/26 15:20:21",
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd  HH:mm:ss"
        print(formatter.date(from: DateString) ?? "Unknown date")
        return (formatter.date(from: DateString) ?? nil)
    }
    
    
    func changeFormat(dateTimeString: String?) -> String {
        
        if dateTimeString == nil || dateTimeString == "" {
            return ""
        }
        
        let dateFormatter = DateFormatter()

        // step 1
        dateFormatter.dateFormat = "yyyy/MM/dd  HH:mm:ss" // input format
        guard let date = dateFormatter.date(from: dateTimeString!) else { return "" }

        // step 2
        dateFormatter.dateFormat = "hh:mm a" // output format
        let string = dateFormatter.string(from: date)
        return string
    }
    
    func findDateDiff(time1Str: String, time2Str: String) -> String {
        let timeformatter = DateFormatter()
        timeformatter.dateFormat = "hh:mm a"

        guard let time1 = timeformatter.date(from: time1Str),
            let time2 = timeformatter.date(from: time2Str) else { return "" }

        //You can directly use from here if you have two dates

        let interval = time2.timeIntervalSince(time1)
        let hour = interval / 3600;
        let minute = interval.truncatingRemainder(dividingBy: 3600) / 60
        let intervalInt = Int(interval)
        print("\(intervalInt < 0 ? "-" : "+") \(Int(hour)) Hours \(Int(minute)) Minutes")
        return "\(intervalInt < 0 ? "-" : "+") \(Int(hour)) Hours \(Int(minute)) Minutes"

    }
}


extension BBQOrderStatusVIewControllerViewController : BBQBillDetailsDelegate {
    func onClose() {
    }
}
extension BBQOrderStatusVIewControllerViewController {
    
    @IBAction func branchLocationClicked() {
        
        if let latitude = deliveryStatusLis?.data?.branch_lat, let longitude = deliveryStatusLis?.data?.branch_long {

            let directionBottomSheet = Bundle.main.loadNibNamed("BookingRedirectionBottomSheet", owner: nil)!.first as! BookingRedirectionBottomSheet
            let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: self.view.frame.size.height * 0.2)
            let bottomSheetController = BottomSheetController(configuration: configuration)

            directionBottomSheet.latitude =  Double(latitude)
            directionBottomSheet.longitude =  Double(longitude)
            directionBottomSheet.bottomSheetType = .Direction
            directionBottomSheet.view.maskByRoundingCorners(cornerRadius: 10, maskedCorners: CornerMask.topCornersMask)
            if(UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
                directionBottomSheet.isGoogleMapsAvailable = true
            }
            bottomSheetController.present(directionBottomSheet, on: self)
        }
    }
    
    @IBAction func contactBranchClicked() {
        
            UIUtils.callNumber(phoneNumber: viewModel?.orderDeliveryStatus?.data?.branch_contact_no ?? "")
    }
}
extension BBQOrderStatusVIewControllerViewController: BBQTakeAwayTimeDelegate{
    func onSlotSelection(slot: String) {
        let confirmView = PickupRescheduleConfirmation().initWith(delegate: self)
        confirmView.slot = slot
        confirmView.lblOrderId.text = "#" + (orderId ?? "")
        confirmView.backColor = self.viewModel?.orderDeliveryStatus?.data?.backgroudColor ?? UIColor.deliveryThemeColor
        confirmView.textColor = self.viewModel?.orderDeliveryStatus?.data?.textColor ?? UIColor.deliveryThemeTextColor
        confirmView.styleUI()
        confirmView.addToSuperView(view: self)
    }
}
extension BBQOrderStatusVIewControllerViewController: PickupRescheduleConfirmationDelegate{
    func cancel(view: PickupRescheduleConfirmation) {
        
    }
    func confirm(view: PickupRescheduleConfirmation, slot: String) {
        guard let orderId = orderId else {
            return
        }
        viewModel?.updatePickSlot(orderId: orderId, slot: slot, completionHandler: { (isSuccess) in
            if isSuccess{
                let successView = PickupRescheduleSuccess().initWith()
                successView.addToSuperView(view: self)
            }
            self.refreshAPICall()
        })
    }
}

extension BBQOrderStatusVIewControllerViewController: ReferalDelegate{
    func shareReferalCode() {
        
        AnalyticsHelper.shared.triggerEvent(type: .RE06)

        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: 80)
        bottomSheetController = BottomSheetController(configuration: configuration)
        
       let vc = BBQShareReferalCodeViewController(nibName: "BBQShareReferalCodeViewController", bundle: nil)
        bottomSheetController?.present(vc, on: self, viewHeight: self.view.frame.size.height - (self.view.frame.size.height * (20/100)))

      
    }
}
