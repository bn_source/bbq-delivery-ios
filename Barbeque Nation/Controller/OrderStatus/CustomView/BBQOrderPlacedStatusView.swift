//
 //  Created by Arpana Rani on 03/03/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQOrderPlacedStatusView.swift
 //

import UIKit

protocol OrderStatusViwDelegate: AnyObject {
    func goToOrderDetails(orderID: String)
  
}
class BBQOrderPlacedStatusView: UIView {

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    var item : CartItems?
    @IBOutlet weak var orderReceivedLabel: UILabel!
    @IBOutlet weak var orderDescriptionLabel: UILabel!
    weak var delgateOrder: OrderStatusViwDelegate?
    
    func loadFromNib() -> BBQOrderPlacedStatusView? {
        if let views = Bundle.main.loadNibNamed("BBQOrderPlacedStatusView", owner: self, options: nil), let view = views[0] as? BBQOrderPlacedStatusView{
            view.frame = UIApplication.shared.windows[0].bounds
            return view
        }
        return nil
    }
    
    func setData() {
        orderReceivedLabel.text = item?.onGoingOrders[0].state
        orderDescriptionLabel.text = item?.onGoingOrders[0].stateInfo
        timeLabel.text =  String(format: "%d mins ", item?.onGoingOrders[0].current_eta ?? 0)

        
    }
    
    @IBAction func tappedOnView(_ sender : Any){
        delgateOrder?.goToOrderDetails(orderID: item?.onGoingOrders[0].orderId ?? "")
    }
    
    //MARK:- View Configuration
    func initWith(item: CartItems?) -> BBQOrderPlacedStatusView {
        let view = loadFromNib()!
        view.item = item
            return view
    }
    
    func addToSuperView(view: UIViewController?) {
        
        if let view = view{
            
         
           // self.axis = .vertical
            
            view.view.addSubview(self)
            self.translatesAutoresizingMaskIntoConstraints = false
            self.bottomAnchor.constraint(equalTo:  view.view.safeAreaLayoutGuide.bottomAnchor , constant: -5).isActive = true
            self.leadingAnchor.constraint(equalTo:  view.view.safeAreaLayoutGuide.leadingAnchor ,constant: 10).isActive = true
            self.trailingAnchor.constraint(equalTo:  view.view.safeAreaLayoutGuide.trailingAnchor, constant: -7).isActive = true
            self.heightAnchor.constraint(equalToConstant: 70.0).isActive = true
            self.setShadow()
            //self.setCornerRadius(10)
            self.layoutIfNeeded()
        }else{
            getMainView().addSubview(self)
        }
       
    }
    
    func removeToSuperView() {
        self.removeFromSuperview()

    }
}
