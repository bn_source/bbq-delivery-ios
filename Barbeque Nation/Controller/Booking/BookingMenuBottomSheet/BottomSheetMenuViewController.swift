//
//  BottomSheetMenuViewController.swift
//  BottomSheet
//
//  Created by Chandan Singh on 29/07/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

enum MenuCategory: String, CaseIterable {
    case Starters, Mains, Salads, Desserts, Beverages
    
    static var caseCount: Int { return self.allCases.count }
}

enum BBQMenuType : String {
    
    case BBQVeg = "BBQVeg"
    case BBQNonVeg = "BBQNonVeg"
    case BBQOthers = "Others"
    
}

class BottomSheetMenuViewController: UIViewController{

    @IBOutlet weak var handleView: UIView!
    @IBOutlet var containerView: UIView!
    @IBOutlet weak var menuOptionsContainerView: UIView!
    @IBOutlet weak var menuButtonScrollView: UIScrollView!
    @IBOutlet weak var menuCategoryView: MenuCategoryView!
    @IBOutlet weak var categoryMenuHeightConstarint: NSLayoutConstraint!
    @IBOutlet weak var menuImagesCollectionView: UICollectionView!
    
    @IBOutlet weak var categoryCostLabel: UILabel!
    @IBOutlet weak var costLabelHeightConstraint: NSLayoutConstraint!
    
    var previousSelectedMenuView: BottomSheetMenuButtonView? = nil
    
    var menuType : BBQMenuType?
    var isBeverages : Bool = false
    var buffetTitle :  String?
    
    fileprivate let imageCellId = "BottomSheetImageCollectionCell"
    fileprivate var columnLayout = BottomSheetImageFlowLayout(cellsPerRow: 2)
    
    private var menuIndex = 0 {
        didSet {
            if self.consolidateAllDataArray.count > 0 {
                self.consolidateAllDataArray.removeAll()
            }
            self.consolidateAllDataArray = self.getConsolidatedArray(forMenuIndex: self.menuIndex)
        }
    }
    
    private var menuCategoryIndex = -1
    private let menuCategoryViewMaxHeight = 96
    private var consolidateAllDataArray = [MenuItems]()
    lazy private var menuCategoryItems = [String]()
    
    var buffetData: [BuffetData]? {
        didSet {
            
        }
    }
    
    weak var dataSource: MenuBottomSheetDataSource? = nil
    weak var delegate: MenuBottomSheetDelegate? = nil
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        registerCollectionNibs()
        setupUI()
    }
    
    //MARK:- UI Methods
    private func registerCollectionNibs() {
        menuImagesCollectionView.register(UINib(nibName: imageCellId, bundle: nil), forCellWithReuseIdentifier: imageCellId)
    }
    
    private func setupUI() {
        //Top HandleView
        handleView.backgroundColor = UIColor.bottomSheetHandleColor
        handleView.roundCorners(cornerRadius: handleView.frame.size.height/2, borderColor: .clear, borderWidth: 0.0)
        
        //Make the container view rounded from top 2 corners
        containerView.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        
        //Flow Layout for CollectionView
        menuImagesCollectionView.collectionViewLayout = columnLayout
        
        //Setup UI for Menu buttons at the top
        self.menuIndex = 0
        setupMenuButtons()
        
        //Menu Category rounded rect buttons. Pass 0for initial setup.
        menuCategoryView.menuDelegate = self
        setupMenuCategoryButtonsForMenu(index: 0)
    }
    
    private func setupMenuButtons() {
        
        let numberOfMenuItems = self.numberOfMenuItems()
        
        var contentSize: CGFloat = 0.0
        
        for index in 0..<numberOfMenuItems.count {
            let menuButtonView = UINib(nibName: "BottomSheetMenuButtonView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! BottomSheetMenuButtonView
            
            menuButtonView.buttonTitle = numberOfMenuItems[index]
            
            let labelSize = menuButtonView.menuButton.titleLabel?.sizeThatFits(CGSize(width: menuButtonView.menuButton.frame.width, height: .greatestFiniteMagnitude)) ?? .zero
            
            if index == 0 {
                contentSize = 10.0
                menuButtonView.frame = CGRect(x: contentSize + CGFloat(20 * index), y: 0.0, width: labelSize.width + 20, height: menuButtonView.frame.size.height)
            } else {
                menuButtonView.frame = CGRect(x: contentSize + CGFloat(20 * index), y: 0.0, width: labelSize.width + 20, height: menuButtonView.frame.size.height)
            }
            
            menuButtonView.delegate = self
            
            menuButtonView.tag = index
            if index == 0 {
                menuButtonView.bottomLineView.isHidden = false
            }
            menuButtonScrollView.addSubview(menuButtonView)
            
            if index == 0 {
                previousSelectedMenuView = menuButtonView
                menuButtonView.menuButton.isSelected = true
                menuButtonView.menuButton.setButtonSelectionUI()
            }
            contentSize += labelSize.width
        }
        
        menuButtonScrollView.contentSize = CGSize(width: contentSize + CGFloat(20 * numberOfMenuItems.count), height: menuButtonScrollView.frame.size.height)
    }
    
    private func setupMenuCategoryButtonsForMenu(index menuIndex: Int) {

        let demoCategory = self.menuCategoryForMenuAt(index: menuIndex)

        menuCategoryView.reset()
        for (index, text) in demoCategory.enumerated() {
            menuCategoryView.addTag(text: text, tagIndex: index)
        }
        
        
        let totalRows = menuCategoryView.getRowNumber(index: demoCategory.count - 1)
        
        UIView.animate(withDuration: 0.2, animations: {
            let showCostLabel =  self.numberOfMenuItems()[menuIndex] == "Beverages"
            self.setupVisibilityForCostLabel(doShow: showCostLabel)
            
            if totalRows == 0 {
                self.categoryMenuHeightConstarint.constant = 45
            } else if totalRows == 1 {
                self.categoryMenuHeightConstarint.constant = 90
            } else {
                self.categoryMenuHeightConstarint.constant = 125
            }
            self.view.layoutIfNeeded()
        })
        
       
        
    }
}

extension BottomSheetMenuViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.numberOfImagesFor(menu: menuIndex, menuCategory: menuCategoryIndex)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let imagesCell : BottomSheetImageCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: imageCellId, for: indexPath) as! BottomSheetImageCollectionCell
        
        let menuData = self.menuDataAt(row: indexPath.row, forMenuIndex: menuIndex, forMenuCategoryIndex: menuCategoryIndex)
        
        self.buffetTitle = self.numberOfMenuItems()[menuIndex]
        self.isBeverages = buffetTitle == "Beverages"

        imagesCell.configureCell(menuName: menuData.menuName, menuDescription: menuData.menuDescription, menuImage: menuData.menuImage, menuType: menuData.menuType, tagImage: menuData.tagImagePath, price: menuData.price, currency: menuData.currency, isBeverage: isBeverages)
        
        imagesCell.menuOverlayType = .Collapsed
        imagesCell.configureOverlay()
        
        return imagesCell
    }
}

extension BottomSheetMenuViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! BottomSheetImageCollectionCell
        if cell.menuOverlayType == .Expanded {
            cell.menuOverlayType = .Collapsed
        } else {
            cell.menuOverlayType = .Expanded
        }
        cell.configureOverlay()
    }
}

extension BottomSheetMenuViewController: BottomSheetMenuButtonViewProtocol {
    func getPreviousView() -> BottomSheetMenuButtonView {
        return previousSelectedMenuView!
    }
    
    func didSelectedButtonAt(view prevView: BottomSheetMenuButtonView) {
        if prevView.tag == menuIndex {
            //Do not reload if same menu is selected
            return
        }
        if let buffetData = buffetData, buffetData.count > prevView.tag{
            if buffetData[prevView.tag].buffetName?.lowercased() == "veg"{
                AnalyticsHelper.shared.triggerEvent(type: .OI02B)
            }else if buffetData[prevView.tag].buffetName?.lowercased() == "nonveg"{
                AnalyticsHelper.shared.triggerEvent(type: .OI02C)
            }else if (buffetData[prevView.tag].buffetName?.lowercased() ?? "").contains("kid"){
                AnalyticsHelper.shared.triggerEvent(type: .OI02A)
            }else if buffetData[prevView.tag].buffetName?.lowercased() == "beverages"{
                AnalyticsHelper.shared.triggerEvent(type: .OI02D)
            }
        }
        previousSelectedMenuView = prevView
        menuButtonScrollView.scrollRectToVisible(prevView.frame, animated: true)
        
        //Perform Menu selection event
        menuIndex = prevView.tag
        menuCategoryIndex = -1
        
        self.menuImagesCollectionView.reloadData {
            if self.menuImagesCollectionView.numberOfItems(inSection: 0) > 0 {
                self.menuImagesCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .top, animated: true)
            }
        }
        
        setupMenuCategoryButtonsForMenu(index: menuIndex)
    }
    
    private func setupVisibilityForCostLabel(doShow: Bool) {
            if doShow {
                self.costLabelHeightConstraint.constant = 21
            } else {
                self.costLabelHeightConstraint.constant = 0
            }
    }
}

extension BottomSheetMenuViewController: MenuCategoryViewProtocol {
    func didTapOnMenuCategoryAt(index tagIndex: Int) {
        if tagIndex == menuCategoryIndex {
            //Do not reload if same category is selected
            return
        }
        
        menuCategoryIndex = tagIndex
        self.menuImagesCollectionView.reloadData {
            if self.menuImagesCollectionView.numberOfItems(inSection: 0) > 0 {
                
                self.menuImagesCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .top, animated: true)
            }
        }
    }
}

//MARK:- Data Handler
extension BottomSheetMenuViewController {
    func numberOfMenuItems() -> [String] {
        var menuArray = [String]()
        if let data = self.buffetData, data.count > 0 {
            for (_, item) in data.enumerated() {
                menuArray.append(item.buffetName ?? "")
            }
        }
        return menuArray
    }
    
    func menuCategoryForMenuAt(index menuIndex: Int) -> [String] {
        var categoryArray = [String]()
        if let data = self.buffetData, data.count > 0 {
            if let buffetItems = data[menuIndex].buffetItems {
                for buffetItem in buffetItems {
                    categoryArray.append(buffetItem.buffetItems_name ?? "")
                }
            }
        }
        return categoryArray
    }
    
    func numberOfImagesFor(menu: Int, menuCategory: Int) -> Int {
        if let data = self.buffetData, data.count > 0 {
            if menuCategory == -1 {
                //Show all the images
                var totalCount = 0
                if let buffetItemsArray = data[menu].buffetItems {
                    for buffetItemsData in buffetItemsArray {
                        totalCount += buffetItemsData.buffetItems_menuItems?.count ?? 0
                    }
                }
                return totalCount
            }
            return data[menu].buffetItems?[menuCategory].buffetItems_menuItems?.count ?? 0
        }
        return 0
    }
    
    func menuDataAt(row rowNumber: Int, forMenuIndex: Int, forMenuCategoryIndex: Int) -> (menuName: String, menuDescription: String, menuImage: String?, menuType: BBQMenuType, tagImagePath: String?, price: String?, currency: String?, buffetData: [BuffetData]?) {
        if let data = self.buffetData, data.count > 0 {
            
            var menuData: MenuItems?
            if menuCategoryIndex == -1 {
                //Show all the images
                menuData = self.consolidateAllDataArray[rowNumber]
            } else {
                menuData = data[forMenuIndex].buffetItems?[forMenuCategoryIndex].buffetItems_menuItems?[rowNumber]
            }
            
            var menuType = BBQMenuType.BBQOthers
            if menuData?.menuitems_Type == "NONVEG" {
                menuType = BBQMenuType.BBQNonVeg
            } else if menuData?.menuitems_Type == "VEG" {
                menuType = BBQMenuType.BBQVeg
            }
            
            let tagImagePath = menuData?.menuitems_Tags?.tagImage
            return (menuName: menuData?.menuitems_Name ?? "", menuDescription:menuData?.menuitems_Description ?? "", menuImage: menuData?.menuitems_Image ?? "", menuType: menuType, tagImagePath: tagImagePath, price: menuData?.menuitems_DefaultPrice, currency: menuData?.menuitems_Currency ?? "INR", buffetData: data)
        }
        return (menuName: "", menuDescription: "", menuImage: "", menuType: BBQMenuType.BBQOthers, tagImagePath: "", price: "", currency: "", buffetData: [BuffetData]())
    }
    
    private func getConsolidatedArray(forMenuIndex: Int) -> [MenuItems] {
        //var consolidatedArray = [MenuItems]()
        if let data = self.buffetData, data.count > 0 {
            if let buffetItemsArray = data[forMenuIndex].buffetItems {
                for buffetItemsData in buffetItemsArray {
                    for buffetItems in buffetItemsData.buffetItems_menuItems ?? [] {
                        self.consolidateAllDataArray.append(buffetItems)
                    }
                }
            }
        }
        return self.consolidateAllDataArray
    }
}

extension UICollectionView {
    func reloadData(_ completion: @escaping () -> Void) {
        reloadData()
        DispatchQueue.main.async { completion() }
    }
}
