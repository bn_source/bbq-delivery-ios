//
 //  Created by Chandan Singh on 19/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQGradientView.swift
 //

import UIKit

class BBQGradientView: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.createGradientLayer()
    }
    
    func createGradientLayer() {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = CGRect(x: 0, y: 0, width: 200, height: 200)
        gradientLayer.colors = [UIColor.menuOverlayLeftColor.cgColor, UIColor.menuOverlayRightColor.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradientLayer.locations = [0, 1]
        self.layer.addSublayer(gradientLayer)
    }
}
