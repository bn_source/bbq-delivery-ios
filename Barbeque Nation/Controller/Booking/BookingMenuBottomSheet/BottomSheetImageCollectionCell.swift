//
//  BottomSheetImageCollectionCell.swift
//  BottomSheet
//
//  Created by Chandan Singh on 29/07/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

enum MenuOverlayType {
    case Expanded, Collapsed
}

class BottomSheetImageCollectionCell: UICollectionViewCell {

    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var vegImageView: UIImageView!
    @IBOutlet weak var spicinessImageView: UIImageView!
    
    @IBOutlet weak var priceSeperator: UIView!
    
    @IBOutlet weak var gradientView: BBQGradientView!
    @IBOutlet weak var detailsContainerView: BBQMenuContainerView!
   
    @IBOutlet weak var detailContainerTopConstarint: NSLayoutConstraint!
    @IBOutlet weak var gradientViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var priceLabel: UILabel!
    
    var menuOverlayType: MenuOverlayType = .Collapsed
    var itemPrice : String?
    var isBeverage: Bool?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.addRoundedCorner(cornerRadius: 10.0)
    }
    
    // TODO: Move necessary methods to view model. Pass config class as constructor parameter instead of so many individual parsm.
    func configureCell(menuName name: String, menuDescription desc: String, menuImage image: String?, menuType: BBQMenuType, tagImage tagImagePath: String?, price: String?, currency: String?, isBeverage: Bool?) {
        
        self.isBeverage = isBeverage
        
        if let imageStr = image, imageStr.count > 0 {
            LazyImageLoad.setImageOnImageViewFromURL(imageView: self.cellImageView, url: imageStr.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)) { (returnImage) in
                if returnImage == nil {
                    self.cellImageView.image = nil
                }
            }
        } else {
            self.cellImageView.image = nil
        }
        
        if menuType == .BBQVeg {
            vegImageView.image = UIImage(named: "icon_veg")
        } else if menuType == .BBQNonVeg {
            vegImageView.image = UIImage(named: "icon_non_veg")
        } else {
            vegImageView.image = UIImage()
        }
        
        if let tag = tagImagePath {
            LazyImageLoad.setImageOnImageViewFromURL(imageView: spicinessImageView, url: tag) { (returnTagImage) in
                if returnTagImage == nil {
                    self.spicinessImageView.image = nil
                }
            }
        } else {
            self.spicinessImageView.image = nil
        }
        
        self.detailsContainerView.menuNameLabel.text = name
        self.detailsContainerView.detailsMenuNameLabel.text = name
        
        if let priceValue = price, let currencyValue = currency, self.isBeverage ?? false {
            let currSymbol = currencyValue == "INR" ? "₹" : ""
            let formattedPrice = String(format: "%.0f", Double(priceValue) ?? 0.0)
            self.itemPrice = "\(currSymbol)\(formattedPrice.toCurrencyFormat())"
            priceSeperator.isHidden = false
        } else {
            self.itemPrice = ""
            priceSeperator.isHidden = true
        }
        
        self.detailsContainerView.detailsdescriptionTextView.text = desc.htmlToString
        
        self.handleHiddings()
    }
    
    private func handleHiddings() {
        if self.menuOverlayType == .Collapsed {
            self.detailsContainerView.menuNameLabel.isHidden = false
            self.detailsContainerView.detailsMenuNameLabel.isHidden = true
            self.detailsContainerView.detailsdescriptionTextView.isHidden = true

            
            self.priceLabel.text = self.itemPrice
            priceSeperator.isHidden = !(self.isBeverage ?? false)
            
        } else {
            self.detailsContainerView.menuNameLabel.isHidden = true
            self.detailsContainerView.detailsMenuNameLabel.isHidden = false
            self.detailsContainerView.detailsdescriptionTextView.isHidden = false
            
            self.priceLabel.text = ""
            priceSeperator.isHidden = true
            
        }
        self.layoutIfNeeded()
    }
    
    func configureOverlay() {
        
        
        // TODO: Change to constraints, animation making some unusual behaviour here.
        let lineCount = self.detailsContainerView.menuNameLabel.calculateMaxLines()
        var topConstant =  self.bounds.size.height * 0.6 - 5
        if lineCount == 1 {
            topConstant = self.bounds.size.height * 0.8 - 5
        }
        else if lineCount == 2 {
            topConstant = self.bounds.size.height * 0.7 - 5
        }
        
        
        if self.menuOverlayType == .Expanded {
            UIView.animate(withDuration: 0.3) {
                self.detailContainerTopConstarint.constant = 0
                self.gradientViewTopConstraint.constant = 0
                self.layoutIfNeeded()
            }
            self.handleHiddings()
        } else {
            UIView.animate(withDuration: 0.3) {
                self.detailContainerTopConstarint.constant = topConstant
                self.gradientViewTopConstraint.constant = topConstant
                self.layoutIfNeeded()
            }
            self.handleHiddings()
            self.detailsContainerView.detailsdescriptionTextView.setContentOffset(CGPoint.zero, animated: false)
        }
    }
}

extension UICollectionViewCell {
    func addRoundedCorner(cornerRadius radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
}
