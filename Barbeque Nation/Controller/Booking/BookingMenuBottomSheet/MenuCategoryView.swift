//
//  MenuCategoryView.swift
//  BottomSheet
//
//  Created by Chandan Singh on 30/07/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

protocol MenuCategoryViewProtocol: AnyObject {
    func didTapOnMenuCategoryAt(index tagIndex: Int)
}

class MenuCategoryView: UIScrollView {
    
    var numberOfRows = 0
    var currentRow = 0
    var tags = [RoundedRectButton]()
    
    var hashtagsOffset: UIEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 0, right: 0)
    var rowHeight: CGFloat = 40 //height of rows
    var tagHorizontalPadding: CGFloat = 10.0 // padding between tags horizontally
    var tagVerticalPadding: CGFloat = 10.0 // padding between tags vertically
    var tagCombinedMargin: CGFloat = 10.0 // margin of left and right combined, text in tags are by default centered.
    
    weak var menuDelegate: MenuCategoryViewProtocol? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func configureView() {
        numberOfRows = Int(frame.height / rowHeight)
        self.showsVerticalScrollIndicator = false
        self.isScrollEnabled = true
    }
    
    func addTag(text:String, tagIndex: Int) {
        //instantiate button
        //you can customize your button here! but make sure everything fit. Default row height is 30.
        let rounderRectButton = RoundedRectButton(withTitle: text)
        rounderRectButton.tag = tagIndex
        
        rounderRectButton.isSelected = false
        //If first button needs to selected by default, uncomment below lines and comment above line.
        /*if tagIndex == 0 {
            rounderRectButton.isSelected = true
        } else {
            rounderRectButton.isSelected = false
        }*/
        
        self.tags.append(rounderRectButton)
        
        //process actions
        rounderRectButton.addTarget(self, action: #selector(self.tappedOnButton(sender:)), for: .touchUpInside)
        
        //calculate frame
        rounderRectButton.frame = CGRect(x: rounderRectButton.frame.origin.x,y: rounderRectButton.frame.origin.y , width: rounderRectButton.frame.width + tagCombinedMargin, height: rowHeight - tagVerticalPadding)
        if self.tags.count == 0 {
            rounderRectButton.frame = CGRect(x: hashtagsOffset.left, y: hashtagsOffset.top, width: rounderRectButton.frame.width, height: rounderRectButton.frame.height)
            self.addSubview(rounderRectButton)
        } else {
            rounderRectButton.frame = self.generateFrameAtIndex(index: tags.count-1, rowNumber: &currentRow)
            self.addSubview(rounderRectButton)
        }
    }
    
    @objc func tappedOnButton(sender: RoundedRectButton) {
        //Mark the selected one
        let selectedRoundedRectButton = self.tags[sender.tag]
        selectedRoundedRectButton.isSelected = true
        
        //Unmark all other Buttons
        for index in 0..<self.tags.count {
            if index != sender.tag {
                let unselectButton = self.tags[index]
                unselectButton.isSelected = false
            }
        }
        if let delegate = self.menuDelegate {
            delegate.didTapOnMenuCategoryAt(index: sender.tag)
        }
    }
    
    private func isOutofBounds(newPoint:CGPoint, labelFrame:CGRect) {
        let bottomYLimit = newPoint.y + labelFrame.height
        if bottomYLimit > self.contentSize.height {
            self.contentSize = CGSize(width: self.contentSize.width, height: self.contentSize.height + rowHeight - tagVerticalPadding)
        } else if bottomYLimit < self.contentSize.height {
            self.contentSize = CGSize(width: self.contentSize.width, height: rowHeight - tagVerticalPadding)
        }
    }
    
    func getNextPosition() -> CGPoint {
        return getPositionForIndex(index: tags.count-1, rowNumber: self.currentRow)
    }
    
    func getPositionForIndex(index:Int,rowNumber:Int) -> CGPoint {
        if index == 0 {
            return CGPoint(x: hashtagsOffset.left, y: hashtagsOffset.top)
        }
        let y = CGFloat(rowNumber) * self.rowHeight + hashtagsOffset.top
        let lastTagFrame = tags[index-1].frame
        let x = lastTagFrame.origin.x + lastTagFrame.width + tagHorizontalPadding
        return CGPoint(x: x, y: y)
    }
    
    func reset() {
        for tag in tags {
            tag.removeFromSuperview()
        }
        tags = []
        currentRow = 0
        numberOfRows = 0
    }
    
    func removeTagWithName(name: String) {
        for (index, tag) in tags.enumerated()
        {
            if tag.titleLabel?.text! == name
            {
                removeTagWithIndex(index: index)
            }
        }
    }
    
    func removeTagWithIndex(index:Int) {
        if index > tags.count - 1
        {
            print("ERROR: Tag Index \(index) Out of Bounds")
            return
        }
        tags[index].removeFromSuperview()
        tags.remove(at: index)
        layoutTagsFromIndex(index: index)
    }
    
    func getRowNumber(index:Int) -> Int {
        return Int((tags[index].frame.origin.y - hashtagsOffset.top)/rowHeight)
    }
    
    private func layoutTagsFromIndex(index:Int,animated:Bool = true) {
        if tags.count == 0
        {
            return
        }
        let animation:()->() =
        {
            var rowNumber = self.getRowNumber(index: index)
            for i in index...self.tags.count - 1
            {
                self.tags[i].frame = self.generateFrameAtIndex(index: i, rowNumber: &rowNumber)
            }
        }
        UIView.animate(withDuration: 0.3, animations: animation)
    }
    
    func generateFrameAtIndex(index:Int, rowNumber: inout Int) -> CGRect {
        var newPoint = self.getPositionForIndex(index: index, rowNumber: rowNumber)
        if (newPoint.x + self.tags[index].frame.width + 40) >= self.frame.width
        {
            rowNumber+=1
            newPoint = CGPoint(x: self.hashtagsOffset.left, y: CGFloat(rowNumber) * rowHeight + self.hashtagsOffset.top)
        }
        self.isOutofBounds(newPoint: newPoint,labelFrame: self.tags[index].frame)
        return CGRect(x: newPoint.x, y: newPoint.y, width: self.tags[index].frame.width + 40, height: self.tags[index].frame.height)
    }
    
    func removeMultipleTagsWithIndices(indexSet:Set<Int>) {
        var sortedArray = Array(indexSet)
        sortedArray.sort()
        for index in sortedArray
        {
            if index > tags.count - 1
            {
                print("ERROR: Tag Index \(index) Out of Bounds")
                continue
            }
            tags[index].removeFromSuperview()
            tags.remove(at: index)
        }
        layoutTagsFromIndex(index: sortedArray.first!)
    }
}
