//
//  BottomSheetMenuButtonView.swift
//  BottomSheet
//
//  Created by Chandan Singh on 30/07/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

protocol BottomSheetMenuButtonViewProtocol: AnyObject {
    func getPreviousView() -> BottomSheetMenuButtonView
    func didSelectedButtonAt(view prevView: BottomSheetMenuButtonView)
}

class BottomSheetMenuButtonView: UIView {
    
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var bottomLineView: UIView!
    @IBOutlet weak var bottomLineWidthConstraint: NSLayoutConstraint!
    
    weak var delegate: BottomSheetMenuButtonViewProtocol? = nil
    
    var buttonTitle: String? {
        didSet {
            self.menuButton.setTitle(buttonTitle, for: .normal)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupUI()
    }
    
    //MARK:- MenuButton UI
    func setupUI() {
        menuButton.setButtonSelectionUI()
        menuButton.backgroundColor = .clear
        menuButton.titleLabel?.adjustsFontSizeToFitWidth = true
        menuButton.titleLabel?.lineBreakMode = .byClipping
        bottomLineView.backgroundColor = UIColor.bottomSheetHandleColor
        bottomLineView.roundCorners(cornerRadius: bottomLineView.frame.size.height/2, borderColor: .clear, borderWidth: 0.0)
    }
    
    @IBAction func menuButtonPressed(_ sender: UIButton) {
        sender.isSelected = true
        sender.setButtonSelectionUI()
        self.bottomLineView.isHidden = false
        
        guard let delegate = self.delegate else {
            return
        }
        let prevView = delegate.getPreviousView()
        
        if prevView.tag != self.tag {
            prevView.menuButton.isSelected = false
            prevView.menuButton.setButtonSelectionUI()
            prevView.bottomLineView.isHidden = true
        }
        
        delegate.didSelectedButtonAt(view: self)
    }
}

extension UIButton {
    func setButtonSelectionUI() {
        if self.isSelected {
            UIView.animate(withDuration: 3.0) {
                self.titleLabel?.font = UIFont.appThemeBoldWith(size: 18.0)
                self.setTitleColor(.menuButtonSelectedColor, for: .normal)
            }
        } else {
            UIView.animate(withDuration: 3.0) {
                self.titleLabel?.font = UIFont.appThemeBoldWith(size: 18.0)
                self.setTitleColor(.menuButtonUnselectedColor, for: .normal)
            }
        }
    }
}
