//
//  ManuBottomSheetProtocols.swift
//  BottomSheet
//
//  Created by Chandan Singh on 29/07/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

protocol MenuBottomSheetDataSource: AnyObject {
    //For Menu Buttons
    func numberOfMenuItems() -> [String]
    
    //For Menu Category
    func menuCategoryForMenuAt(index menuIndex: Int) -> [String]
    
    //For Bottom Collection Layout
    func numberOfImagesFor(menu: Int, menuCategory: Int) -> Int
    func menuDataAt(row rowNumber: Int, forMenuIndex: Int, forMenuCategoryIndex: Int) -> (menuName: String, menuDescription: String, menuImage: String?, isVeg: Bool, tagImagePath: String?, price: String?, currency: String?)
}

protocol MenuBottomSheetDelegate: AnyObject {
    
}
