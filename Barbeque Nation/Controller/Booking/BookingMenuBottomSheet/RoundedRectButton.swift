//
//  RoundedRectButton.swift
//  BottomSheet
//
//  Created by Chandan Singh on 29/07/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

class RoundedRectButton: UIButton {
    
    override var frame: CGRect {
        didSet {
            self.roundCorners(cornerRadius: self.frame.size.height / 2, borderColor: UIColor.menuOrangeColor, borderWidth: 1.0)
        }
    }
    
    override var isSelected: Bool {
        didSet {
            //Based on the selection, change backgroung and text colors
            self.backgroundColor = isSelected ? UIColor.menuOrangeColor : UIColor.menuCategoryButtonUnselectedBg
            if isSelected {
                self.setTitleColor(.menuCategoryButtonUnselectedBg, for: .normal)
            } else {
                self.setTitleColor(.menuOrangeColor, for: .normal)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //custom logic goes here
    }
    
    required init(withTitle title: String) {
        super.init(frame: .zero)
        
        self.setTitle(title, for: .normal)
        self.titleLabel?.font = UIFont.appThemeMediumWith(size: 14.0)
        
        //Increase the button width based on the text length
        self.sizeToFit()
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
