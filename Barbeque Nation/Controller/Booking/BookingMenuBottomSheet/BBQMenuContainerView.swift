//
 //  Created by Chandan Singh on 19/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQMenuContainerView.swift
 //

import UIKit

class BBQMenuContainerView: UIView {
    
    @IBOutlet weak var menuNameLabel: UILabel!
    @IBOutlet var detailsMenuNameLabel: UILabel!
    @IBOutlet weak var detailsdescriptionTextView: UITextView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
