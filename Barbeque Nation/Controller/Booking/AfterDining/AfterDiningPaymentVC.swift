//
 //  Created by Sakir Sherasiya on 21/12/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified AfterDiningPaymentVC.swift
 //

import UIKit
import AMPopTip
import WebKit
import Razorpay


class AfterDiningPaymentVC: BaseViewController {
    
    
    @IBOutlet weak var lblReservationSlot: UILabel!
    @IBOutlet weak var lblOutletName: UILabel!
    @IBOutlet weak var lblReservationID: UILabel!
    @IBOutlet weak var btnPayNow: UIButton!
    @IBOutlet weak var tblCalculation: UITableView!
    @IBOutlet weak var tblInvoiceBreakUp: UITableView!
    @IBOutlet weak var heightForTblCalculation: NSLayoutConstraint!
    @IBOutlet weak var heightForTblInvoiceBreakUp: NSLayoutConstraint!
    @IBOutlet weak var stackViewForTable: UIStackView!
    @IBOutlet weak var btnViewBreakup: UIButton!
    @IBOutlet weak var btnPaymentDetails: UIButton!
    
    var arrBreakUpData = [AfterDiningData]()
    var arrCalculationsCells = [AfterDiningPaymentCellType]()

    var bookingID: String = ""
    var model = AfterDiningVCViewModel()
    var isPointsResetWarned = false
    var vouchersTerms: [String]?
    var delegate: BBQAfterDiningDelegate?
    var isAnimating = false
    var popTip = PopTip()
    var textIndex = 0
    
    var paymentFetchMethods: [AnyHashable:Any]?
    var razorpay: RazorpayCheckout!
    
    lazy private var paymentControllerViewModel : MakePaymentViewModel = {
        let paymentVM = MakePaymentViewModel()
        return paymentVM
    } ()
    
    
    internal var pointsValueModel: BBQPointsValueModel? = nil
    lazy var cartViewModel: CartViewModel = {
        let viewModel = CartViewModel()
        return viewModel
    }()

    lazy var buffetsViewModel: BBQBookingBuffetsViewModel = {
        let buffetModel = BBQBookingBuffetsViewModel()
        return buffetModel
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registreCell()
        setTheme()
        initiateThePaymentData()
        model.fetchAfterDiningPaymentData(for: bookingID, isNeedToRefresh: true) { isFetched in
            self.refreshData()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tblCalculation.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tblCalculation.removeObserver(self, forKeyPath: "contentSize")
    }
    
    func setTheme() {
//        viewForTblBreakUp.setShadow(color: .lightGray, opacity: 0.2, offSet: .zero, radius: 10.0, scale: true)
        stackViewForTable.setShadow(color: .lightGray, opacity: 0.2, offSet: .zero, radius: 10.0, scale: true)
        tblCalculation.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0)
        tblInvoiceBreakUp.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0)
        btnPaymentDetails.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0)
        btnPaymentDetails.titleLabel?.numberOfLines = 0

        popTip.bubbleColor = .white
        popTip.cornerRadius = 5.0
        popTip.shouldShowMask = true
    }
    
    func registreCell(){
        tblInvoiceBreakUp.register(UINib(nibName: "AfterDiningPaymentCell", bundle: nil), forCellReuseIdentifier: "AfterDiningPaymentCell")
        tblInvoiceBreakUp.register(UINib(nibName: "ReservationDetailsCell", bundle: nil), forCellReuseIdentifier: "ReservationDetailsCell")
        tblCalculation.register(UINib(nibName: "AfterDiningPaymentCell", bundle: nil), forCellReuseIdentifier: "AfterDiningPaymentCell")
        
        
        tblCalculation.register(UINib(nibName: "BBQAfterBookingPointsTableViewCell",
                                      bundle: nil),
                                forCellReuseIdentifier: "BBQAfterBookingPointsTableViewCell")
        
        
        tblCalculation.register(UINib(nibName: "BBQCorpOffersTableViewCell",
                                      bundle: nil),
                                forCellReuseIdentifier: "BBQCorpOffersTableViewCellID")
        
        tblCalculation.register(UINib(nibName: "BBQAfterBookingCouponsTableViewCell",
                                      bundle: nil),
                                forCellReuseIdentifier: "BBQAfterBookingCouponsTableViewCell")
        
        tblCalculation.register(UINib(nibName: "BBQAfterBookingApplyVouchersTableViewCell",
                                      bundle: nil),
                                forCellReuseIdentifier: "BBQAfterBookingApplyVouchersTableViewCell")
    }
    
    func refreshData() {
        let data = model.getArrayBreakUpData()
        model.countTheSubtotalAmount()
        self.arrBreakUpData = data.invoiceBreakup
        self.arrCalculationsCells = data.calculationCells

        lblOutletName.text = model.dataModel?.branchName
        lblReservationID.text = model.dataModel?.bookingID
        lblReservationSlot.text = String(format: "%@%.02f", getCurrency(), model.netPayableAmount)
        
        if model.countGSTBaseAmount == 0 || (model.dataModel?.netPayable ?? 0) == 0{
            btnPayNow.isHidden = true
        }else{
            btnPayNow.isHidden = false
        }
        
        tblInvoiceBreakUp.reloadData{
            self.heightForTblInvoiceBreakUp.constant = self.tblInvoiceBreakUp.contentSize.height
        }
        self.textIndex = 0
        tblCalculation.reloadData{
            self.heightForTblCalculation.constant = self.tblCalculation.contentSize.height
        }
    }

    
    internal func getPointsFromServer() {
        let priceInDouble = self.model.getSmilesDataCount()
        let price = String(format: "%.02f", priceInDouble)
        UIUtils.showLoader()
        self.cartViewModel.getLoyaltyPointsValue(amount: price, currency: "INR", completion: { (isSuccess) in
                UIUtils.hideLoader()
                if isSuccess {
                        self.pointsValueModel = self.cartViewModel.getPointValueModel
                        self.model.appliedSmiles = Int((self.pointsValueModel?.redeemAmount ?? 0).rounded(.up))
                    self.model.countTheSubtotalAmount()
                    self.lblReservationSlot.text = String(format: "%@%.02f", getCurrency(), self.model.netPayableAmount)
                    self.textIndex = 0
                        self.tblCalculation.reloadData {
                                self.heightForTblCalculation.constant = self.tblCalculation.contentSize.height
                        }
                }
        })
    }
    
    @IBAction func onClickBtnPayNow(_ sender: Any) {
        //updateFinalPaymentDetails(paymentOrderId: "order_KxMjNdset2wJgj", paymentId: "pay_KxMjc8O6Vlzk7b", paymentGatewayName: "RAZOR_PAY", paymentSignature: "1f324362319aee64d136b985e3cb6f048157fe3c2e47351e27640d90cc0d4033")
        self.proceedToPaymentPressed(paymentAmount:model.netPayableAmount, paymentRemarks: self.bookingID)
    }
    @IBAction func onClickBtnPaymentDetails(_ sender: Any) {
        onClickBtnBreakUp(sender)
    }
    @IBAction func onClickBtnBreakUp(_ sender: Any) {
        if !isAnimating{
            isAnimating = true
            btnViewBreakup.isSelected = !btnViewBreakup.isSelected
            
            UIView.animate(withDuration: 0.2, delay: 0, options: .transitionCrossDissolve) {
                self.tblInvoiceBreakUp.isHidden = !self.btnViewBreakup.isSelected
                self.tblInvoiceBreakUp.reloadData()
                
//                self.btnPaymentDetails.isHidden = !self.btnViewBreakup.isSelected
            } completion: { isCompleted in
                self.tblInvoiceBreakUp.isHidden = !self.btnViewBreakup.isSelected
                self.tblInvoiceBreakUp.reloadData{
                    self.heightForTblInvoiceBreakUp.constant = self.tblInvoiceBreakUp.contentSize.height
                }
//                self.btnPaymentDetails.isHidden = !self.btnViewBreakup.isSelected
                self.isAnimating = false
            }

        }
    }
    
    @IBAction func onClickBtnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AfterDiningPaymentVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblInvoiceBreakUp{
            return model.dataModel?.bookingDetails?.count ?? 0
        }else{
            return arrCalculationsCells.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tblCalculation == tableView, arrCalculationsCells.count > indexPath.row{
            var afterDiningData: AfterDiningData?
            if arrCalculationsCells[indexPath.row] == .subTotal{
                afterDiningData = AfterDiningData(title: "Sub Total", value: String(format: "%@%.02f", getCurrency(), model.bookingSubTotal), breakup: [])
            }else if arrCalculationsCells[indexPath.row] == .smiles{
                return loadPointsDetailsCell(for: indexPath.row)
            }else if arrCalculationsCells[indexPath.row] == .coupons{
                return loadBookingCouponsCell(for: indexPath.row)
            }else if arrCalculationsCells[indexPath.row] == .taxes{
                let taxes = model.recountTheTax(isCountApplied: false)
                afterDiningData = AfterDiningData(title: "Taxes", value: String(format: "%@%.02f", getCurrency(), model.bookingDTaxAmount), breakup: taxes)
            }else if arrCalculationsCells[indexPath.row] == .vouchers{
                return loadVouchersDetailsCell(for: indexPath.row)
            }else if arrCalculationsCells[indexPath.row] == .netPayable{
                afterDiningData = AfterDiningData(title: "Payable Amount", value: String(format: "%@%.02f", getCurrency(), model.netPayableAmount), breakup: [])
            }else if arrCalculationsCells[indexPath.row] == .text{
                if arrBreakUpData.count > textIndex{
                    afterDiningData = arrBreakUpData[textIndex]
                    textIndex += 1
                }
            }
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: "AfterDiningPaymentCell", for: indexPath) as? AfterDiningPaymentCell, let afterDiningData = afterDiningData{
                cell.setCellData(afterdiningData: afterDiningData, delegate: self, isLastCell: arrCalculationsCells.count - 1 == indexPath.row)
                return cell
            }
        }else if let cell = tableView.dequeueReusableCell(withIdentifier: "ReservationDetailsCell", for: indexPath) as? ReservationDetailsCell, (model.dataModel?.bookingDetails?.count ?? 0) > indexPath.row, let model = model.dataModel?.bookingDetails?[indexPath.row]{
            cell.setCellData(model: model)
            if indexPath.row == (self.model.dataModel?.bookingDetails?.count ?? 0) - 1{
                cell.lblLightLine.isHidden = true
            }else{
                cell.lblLightLine.isHidden = false
            }
            return cell
        }
        return UITableViewCell()
    }
    
    private func loadPointsDetailsCell(for index: Int) -> BBQAfterBookingPointsTableViewCell {
        
        let pointsCell =
            self.tblCalculation.dequeueReusableCell(withIdentifier: "BBQAfterBookingPointsTableViewCell")
                as! BBQAfterBookingPointsTableViewCell
        pointsCell.pointCellDelegate = self
        pointsValueModel?.redeemAmount = Double(model.appliedSmiles)
        pointsCell.loadAppliedPointsCell(pointsModel:pointsValueModel ?? BBQPointsValueModel(),
                                             didAppliedAlready: false,
                                         didAddNewPoints: model.appliedSmiles > 0,
                                         isPointEnabled: (self.model.appliedServerSmiles == 0 && (self.model.netPayableAmount > 0 || model.appliedSmiles > 0)))
        return pointsCell
    }
//
//    private func loadCorporateOffersCell(for index: Int) -> BBQCorpOffersTableViewCell {
//
//        let corpCell =
//            self.tblCalculation.dequeueReusableCell(withIdentifier: "BBQCorpOffersTableViewCellID")
//                as! BBQCorpOffersTableViewCell
//        corpCell.contentView.layer.opacity = 1.0
//        corpCell.isUserInteractionEnabled = !didAddCoupons
//
//        if let appliedOffers = self.appliedCorpOffers {
//            corpCell.loadAppliedCorpOffers(for: self.model.dataModel?.numberOfPacks ?? 0,
//                                           offerData: appliedOffers[0],
//                                           isCorpOffersApplied: self.didAddCorpOffers,
//                                           isCardEnabled: afterDiningViewModel.isCorpOffersEnabled(isCouponsAdded: didAddCoupons))
//        }
//        return corpCell
//    }
    
    private func loadBookingCouponsCell(for index: Int) -> BBQAfterBookingCouponsTableViewCell {
        
        let couponCell =
            self.tblCalculation.dequeueReusableCell(withIdentifier: "BBQAfterBookingCouponsTableViewCell")
                as! BBQAfterBookingCouponsTableViewCell
        couponCell.couponDelegate = self
        couponCell.loadCouponsData(with: self.model.arrAppliedCoupons,
                                   didAdd: self.model.arrAppliedCoupons.count > 0,
                                   isCardEnabled: (self.model.arrServerAppliedCoupons.count == 0 && (self.model.netPayableAmount > 0 || self.model.arrAppliedCoupons.count > 0)),
                                   bookingType: .AfterDining)
        return couponCell
    }
    
    
    private func loadVouchersDetailsCell(for index: Int) -> BBQAfterBookingApplyVouchersTableViewCell {
        let voucherCell =
            self.tblCalculation.dequeueReusableCell(withIdentifier: "BBQAfterBookingApplyVouchersTableViewCell")
                as! BBQAfterBookingApplyVouchersTableViewCell
        voucherCell.voucherDelegate = self
        voucherCell.loadVouchersData(with: self.model.arrAppliedVoucher,
                                     didAdd: self.model.arrAppliedVoucher.count > 0,
                                     isCardEnabled: self.model.netPayableAmount > 0 || self.model.arrAppliedVoucher.count > 0)
        
        
        return voucherCell
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize"{
            self.heightForTblCalculation.constant = tblCalculation.contentSize.height
        }
    }
}

extension AfterDiningPaymentVC: AfterDiningPaymentCellDelegate{
    func didTapOnDetails(cell: AfterDiningPaymentCell, afterDiningData: AfterDiningData) {
        if afterDiningData.breakup.count > 0{
            let popupVC = BillDetailsPopVC(nibName: "BillDetailsPopVC", bundle: nil)
            popupVC.modalPresentationStyle = UIModalPresentationStyle .popover
            popupVC.preferredContentSize = CGSize(width: 170, height: 130)
            popupVC.strTitle = afterDiningData.title
            var data = [TaxBreakUp]()
            for dataObje in afterDiningData.breakup{
                data.append(TaxBreakUp(label: dataObje.title, value: dataObje.value))
            }
            popupVC.taxBreakUp = data
            if afterDiningData.title.lowercased().contains("tax"){
                popupVC.view.frame.size.width = 200 > self.view.frame.size.width ? self.view.frame.size.width : 200
            }else{
                popupVC.view.frame.size.width = 200 < self.view.frame.size.width/1.5 ? self.view.frame.size.width/1.5 : 200
            }
            popupVC.view.frame.size.height = CGFloat(60 + (data.count * 26))
            //            var originFrame = cell.valueLabel.frame
            //            originFrame.size.width = 40
            //            originFrame.origin.x = originFrame.origin.x + 35
            var frame = cell.convert(cell.lblValue.frame, to: self.view)
            frame.origin.x += cell.lblValue.frame.width/2.0
            popTip.show(customView: popupVC.view, direction: .auto, in: self.view, from: frame)
        }
    }
}

extension AfterDiningPaymentVC: BBQAfterBookingPointsTableViewCellDelegate, BBQAfterBookingCouponsTableViewCellDelegate, BBQAfterBookingApplyVouchersTableViewCellDelegate{
    
    private func showSmilesResetWarning(completion: @escaping (_ doReset: Bool)->()) {
        if !self.isPointsResetWarned && self.model.appliedSmiles > 0 {
            PopupHandler.showTwoButtonsPopup(title: "Applied Smiles will be removed",
                                             message: "All applied Smiles will be removed if you apply any coupon/happiness card/ corporate offers on top of Smiles. Please re -apply Smiles again.",
                                             on: self, firstButtonTitle: "Cancel",
                                             firstAction: {
                                                completion(false)
                                                
            }, secondButtonTitle: "OK", secondAction: {
                completion(true)
            })
            self.isPointsResetWarned = true
        } else {
            completion(true)
        }
        
    }
    
    private func resetPoints() {
        if model.appliedSmiles > 0 {
            model.appliedSmiles = 0
            model.arrAppliedVoucher.removeAll()
            self.model.countTheSubtotalAmount()
        }
    }
    
    func didPressedVoucherAddRemoveButton(isAdd: Bool) {
        if isAdd{
            AnalyticsHelper.shared.triggerEvent(type: .AP04)
            self.showSmilesResetWarning { (doReset) in
                if doReset {
                    self.showAllOffersController(type: .VouchersForYou)
                }
            }
        }else{
            self.model.arrAppliedVoucher = [VoucherCouponsData]()
            self.model.countTheSubtotalAmount()
            self.lblReservationSlot.text = String(format: "%@%.02f", getCurrency(), self.model.netPayableAmount)
            self.textIndex = 0
            self.tblCalculation.reloadData{
                self.heightForTblCalculation.constant = self.tblCalculation.contentSize.height
            }
        }
    }
    
    func didPressedVouchersTermsAndConditions() {
        let termsVC = UIStoryboard.loadTermsAndConditionsScreen()
        termsVC.view.layoutIfNeeded()
        termsVC.modalPresentationStyle = .overFullScreen
        if let vouchersTermsData = self.vouchersTerms {
            termsVC.loadTermsControllerWith(termsData: vouchersTermsData)
            self.present(termsVC, animated: true, completion: nil)
        }
    }
    
    func didPressedAddRemoveCouponButton(isAdd: Bool) {
        if isAdd{
            AnalyticsHelper.shared.triggerEvent(type: .AP04)
            self.showSmilesResetWarning { (doReset) in
                if doReset {
                    self.resetPoints()
                    self.showAllOffersController(type: .CouponsForYou)
                    self.lblReservationSlot.text = String(format: "%@%.02f", getCurrency(), self.model.netPayableAmount)
                    self.textIndex = 0
                    self.tblCalculation.reloadData{
                        self.heightForTblCalculation.constant = self.tblCalculation.contentSize.height
                    }
                }
            }
        }else{
            self.model.arrAppliedCoupons = [VoucherCouponsData]()
            self.model.countTheSubtotalAmount()
            self.lblReservationSlot.text = String(format: "%@%.02f", getCurrency(), self.model.netPayableAmount)
            self.textIndex = 0
            self.tblCalculation.reloadData{
                self.heightForTblCalculation.constant = self.tblCalculation.contentSize.height
            }
        }
    }
    
    func didPressedCouponsTermsAndConditions() {
        
    }
    
    func didPressedPointsAddRemoveButton(isAdd: Bool) {
        if isAdd{
            getPointsFromServer()
//            model.appliedSmiles = Int(self.pointsValueModel?.redeemAmount ?? Double(BBQUserDefaults.sharedInstance.UserLoyaltyPoints))
//            model.countTheSubtotalAmount()
//            self.tblCalculation.reloadData{
//                self.heightForTblCalculation.constant = self.tblCalculation.contentSize.height
//            }
        }else{
            model.appliedSmiles = 0
            model.countTheSubtotalAmount()
            self.lblReservationSlot.text = String(format: "%@%.02f", getCurrency(), self.model.netPayableAmount)
            self.textIndex = 0
            self.tblCalculation.reloadData{
                self.heightForTblCalculation.constant = self.tblCalculation.contentSize.height
            }
        }
    }
    
    
    internal func showAllOffersController(type: ScreenType) {
        let viewController = BBQAllOffersViewController()
        viewController.screenType = type
        viewController.isAfterDiningScreen = false
        viewController.doManualCoupon = true
        viewController.doManualCoupon = true
        
        if type == .VouchersForYou || type == .CouponsForYou {
            
            let totalPacks = self.model.dataModel?.numberOfPacks ?? 0
            let branchID = self.model.dataModel?.branchId ?? "0"
            let slotID = self.model.dataModel?.slotId ?? "0"
            let currency = self.model.dataModel?.currency ?? ""
            let finalAmount = self.model.dataModel?.billTotal ?? 0.00
            let resDate = self.model.dataModel?.reservationDate ?? ""
            let resTime = self.model.dataModel?.reservationTime ?? ""
            
            self.buffetsViewModel.saveBookingDataForCorporateOffers(reservationDate: resDate + " " + resTime,
                                                                    packs: totalPacks,
                                                                    branchId: branchID,
                                                                    totalAmount: finalAmount,
                                                                    slotId: Int(slotID) ?? 0,
                                                                    currency: currency)
            viewController.buffetsViewModel = self.buffetsViewModel
        }
        
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: UIScreen.main.bounds.size.height - viewController.view.frame.size.height)
        let bottomSheet = BottomSheetController(configuration: configuration)
        viewController.delegate = self
        viewController.bottomSheet = bottomSheet
//        bottomSheet.present(viewController, on: self)
        viewController.modalPresentationStyle = .fullScreen
        self.present(viewController, animated: true, completion: nil)
    }
    
}

extension AfterDiningPaymentVC: BBQAllOffersProtocol{
    func buyNowButtonClicked(bottomSheet: BottomSheetController?) {
        
    }
    func userVoucherSelectedWith(voucherList:[VoucherCouponsData], type: ScreenType,termsAndConditions:[String]){
        UIUtils.hideLoader()
        if type == .CouponsForYou {
            self.model.arrAppliedCoupons = voucherList
            self.resetPoints()
            self.model.countTheSubtotalAmount()
            self.lblReservationSlot.text = String(format: "%@%.02f", getCurrency(), self.model.netPayableAmount)
            self.textIndex = 0
            self.tblCalculation.reloadData()
        }
        if type == .VouchersForYou {
            self.model.arrAppliedVoucher = voucherList
            self.vouchersTerms = termsAndConditions
            self.model.countTheSubtotalAmount()
            self.lblReservationSlot.text = String(format: "%@%.02f", getCurrency(), self.model.netPayableAmount)
            self.textIndex = 0
            self.tblCalculation.reloadData()
        }
    }
    func selectedCorporateOffer(corporateOffer:CorporateOffer){
        
    }
}

enum AfterDiningPaymentCellType{
    case smiles
    case coupons
    case vouchers
    case taxes
    case vat
    case cess
    case netPayable
    case subTotal
    case text
}


extension AfterDiningPaymentVC {
    
    func proceedToPaymentPressed(paymentAmount: Double, paymentRemarks: String) {
        
        // Calling booking details API once again to confirm net payable has not changed.
        AnalyticsHelper.shared.triggerEvent(type: .AP08)
        UIUtils.showLoader()
        self.model.fetchAfterDiningPaymentData(for: self.bookingID , isNeedToRefresh: false) { (isFetched) in
            UIUtils.hideLoader()
            if isFetched {
                // If order is not modified after last reciept go for payment.
                 if paymentAmount <= 0.00 {
                     AnalyticsHelper.shared.triggerEvent(type: .AP08A)
                     self.updateFinalPaymentDetails()
                     return
                 }
                 AnalyticsHelper.shared.triggerEvent(type: .AP09)
//                 let rpController = BBQRazorpayContainerController()
//                 rpController.modalPresentationStyle = .overCurrentContext
//                 self.present(rpController, animated: true, completion: nil)
//                 rpController.payDelegate = self
//                 rpController.createOrderForPayment(with: paymentAmount,
//                                                    currency:"INR", title: "Final Payment",
//                                                    and: paymentRemarks,
//                                                    bookingID: self.bookingID,
//                                                    loyalty_points: Int(self.model.appliedSmiles))
                // MARK: - Custom Payment UI Code added v5.3.5
                self.showPaymentForm()
            }
        }
    }
    
    func updateFinalPaymentDetails(paymentOrderId: String? = nil,
                                   paymentId: String? = nil,
                                   paymentGatewayName: String? = nil,
                                   paymentSignature: String? = nil) {
        
        //        let corpOfferData = CorporateOffer(voucherCode: appliedCorpOffers?[0].barcode ?? "",
        //                                               voucherType: appliedCorpOffers?[0].voucherType ?? "",
        //                                               voucherAmount: appliedCorpOffers?[0].denomination ?? 0)
        
        // Only new vouchers can be added in after dining payment. Passing other 2 nil here.
        AnalyticsHelper.shared.triggerEvent(type: .AP10)
        let voucherDetails =
        self.buffetsViewModel.createConsolidatedVoucherDetails(appliedCorpOffers: nil,
                                                               appliedCoupons: self.model.arrAppliedCoupons,
                                                               appliedVouchers: self.model.arrAppliedVoucher,
                                                               arrAddedAmounts: [],
                                                               preferredBranch: nil)
        
        let updatePayModel = FinalPaymentUpdateBody(bookingId: self.bookingID ,
                                                    branch_id: model.dataModel?.branchId, paymentGateway: paymentGatewayName,
                                                    paymentID: paymentId,
                                                    orderID: paymentOrderId,
                                                    paymentSignature: paymentSignature,
                                                    loyaltyPoints: self.model.appliedSmiles > 0 ? self.model.appliedSmiles : nil,
                                                    loyaltyAmount: self.model.appliedSmiles > 0 ? Double(self.model.appliedSmiles) : nil,
                                                    voucherDetails: voucherDetails.voucherDetails, currency: model.dataModel?.currency, reservation_date: model.dataModel?.reservationDate, reservation_time: model.dataModel?.reservationTime, country_code: "+91", mobile_number: Int64(SharedProfileInfo.shared.profileData?.mobileNumber ?? BBQUserDefaults.sharedInstance.customerId))
        UIUtils.showLoader()
        self.model.updateBooking(finalPaymentData: updatePayModel) { (isUpdated) in
            UIUtils.hideLoader()
            if isUpdated {
                AnalyticsHelper.shared.triggerEvent(type: .AP10A)
                self.showPaymentSuccessScreen()
            }else{
                AnalyticsHelper.shared.triggerEvent(type: .AP10B)
            }
            
            
        }
    }
    
    func showPaymentSuccessScreen() {
        let registrationConfirmationVC = UIStoryboard.loadRegistrationConfirmation()
        registrationConfirmationVC.modalPresentationStyle = .overFullScreen
        registrationConfirmationVC.titleText = kPaymentSuccessText
        registrationConfirmationVC.navigationText = kMyTransactionPaymentRedirectionMessage
        
        //let keyWindow = UIApplication.shared.keyWindow?.rootViewController?.presentedViewController
        self.present(registrationConfirmationVC, animated: false) {
            sleep(UInt32(2.0))
            
            self.dismiss(animated: true) {
                
            }
            self.delegate?.onPaymentSuceess()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    // MARK: - BBQRazorpayControllerProtocol Methods
    internal func showPaymentForm() {
        
        let paymenVc = UIStoryboard.loadPayment()
        let paymentData = getWebViewPaymentId(paymentVC: paymenVc, order_id: nil)
        paymenVc.viewModel = CustomPaymentViewModel.init(razorPay: self.razorpay,razorpayOptions: paymentData.options ?? [:], delegate: paymenVc,webView: paymentData.webView, response: paymentFetchMethods)
        paymenVc.delegate = self
        self.navigationController?.pushViewController(paymenVc, animated: true)
    }
    private func initiateThePaymentData() {
        if razorpay == nil{
            let webView = WKWebView.init(frame: self.view.frame)
            let razorPayAppKey = Bundle.main.object(forInfoDictionaryKey: Constants.Payment.razorpayPublicKey)
            self.razorpay = Razorpay.initWithKey(razorPayAppKey as! String, andDelegate: self, withPaymentWebView: webView)
        }
        
        self.razorpay.getPaymentMethods(withOptions: nil) { (response) in
            self.paymentFetchMethods = response
        } andFailureCallback: { (error) in
            print("Error fetching payment methods:\(error)")
        }
    }
    private func getWebViewPaymentId(paymentVC: BBQCustomPaymentViewController, order_id: String?) -> (webView: WKWebView, options: [String: Any]?){
        var netpayable = Double(String(format: "%.2f", model.netPayableAmount)) ?? 0.0
        let amount = Int(round(netpayable * 100))
        let options: [String:Any] = [
            "amount": amount,
            "currency": "INR",
            "description": "Advance amount to be paid",
            "order_id": order_id as Any,
            //            "name": paymentModel.name ?? "",
            "email" : SharedProfileInfo.shared.profileData?.email  ?? BBQUserDefaults.sharedInstance.customPaymentEmail,
            "contact": SharedProfileInfo.shared.profileData?.mobileNumber as Any,
            //Comment me for release
            //            "contact": 9844019983
            
        ]
        
        let webView = WKWebView.init(frame: self.view.frame)
        let razorPayAppKey = Bundle.main.object(forInfoDictionaryKey: Constants.Payment.razorpayPublicKey)
        self.razorpay = Razorpay.initWithKey(razorPayAppKey as! String, andDelegate: paymentVC, withPaymentWebView: webView)
        return (webView, options)
    }
    func onRazorpayPaymentError(_ code: Int32, description str: String) {
        AnalyticsHelper.shared.triggerEvent(type: .AP09B)
        switch code {
        case 0,1,3:
            PopupHandler.showSingleButtonsPopup(title: kFailedTitle,
                                                message: str,
                                                on: self, firstButtonTitle: kOkButtonTitle) { }
        case 2:
            self.showTransactionCancelled()
            
        default:
            print("");
        }
    }
    
    func showTransactionCancelled() {
        PopupHandler.showSingleButtonsPopup(title: kMyCartTransactionCancelTitle,
                                            message: kMyCartTransactionCancelMessage,
                                            on: self,
                                            firstButtonTitle: kButtonOkay) { }
    }
    
    func onRazorpayPaymentSuccess(_ paymentData: BBQPaymentDataModel) {
        //let razorpayModel = paymentData
        AnalyticsHelper.shared.triggerEvent(type: .AP09A)
        let paymentGateway = Constants.BBQBookingStatus.razorPayment
        
        UIUtils.showLoader()
        self.updateFinalPaymentDetails(paymentOrderId: paymentData.razorpayOrderID ?? "",
                                       paymentId: paymentData.paymentID ?? "",
                                       paymentGatewayName: Constants.BBQBookingStatus.razorPayment,
                                       paymentSignature: paymentData.razorpaySignature ?? "")
        
        
    }
    
}
extension AfterDiningPaymentVC: BBQCustomPaymentViewControllerDelegate, RazorpayPaymentCompletionProtocol{
        func onPaymentSuccess(_ payment_id: String, andData response: [AnyHashable : Any]) {
            self.navigationController?.popToViewController(self, animated: true)
            
            AnalyticsHelper.shared.triggerEvent(type: .AP09A)
            let paymentGateway = Constants.BBQBookingStatus.razorPayment
            
            UIUtils.showLoader()
            self.updateFinalPaymentDetails(paymentOrderId: response["razorpay_order_id"] as? String ?? "",
                                           paymentId: response["razorpay_payment_id"] as? String ?? "",
                                           paymentGatewayName: Constants.BBQBookingStatus.razorPayment,
                                           paymentSignature: response["razorpay_signature"] as? String ?? "")
        }
        
        func onPaymentError(_ code: Int32, description str: String, andData response: [AnyHashable : Any]) {
            self.navigationController?.popToViewController(self, animated: true)
            AnalyticsHelper.shared.triggerEvent(type: .AP09B)
            switch code {
            case 0,1,3:
                PopupHandler.showSingleButtonsPopup(title: kFailedTitle,
                                                    message: str,
                                                    on: self, firstButtonTitle: kOkButtonTitle) { }
            case 2:
                self.showTransactionCancelled()
                
            default:
                print("");
            }
        }
        
        func createRazorPayPaymentId(paymentVC: BBQCustomPaymentViewController, completion: @escaping (WKWebView, [String : Any]?, String?, Razorpay?, String?) -> Void) {
            guard let booking = model.dataModel  else {
                ToastHandler.showToastWithMessage(message: "Please try again!")
                return
            }
            
            AnalyticsHelper.shared.triggerEvent(type: .booking_payment_status)
            
            self.paymentControllerViewModel.createPaymentOrder(with: String(format: "%.02f", model.netPayableAmount), currency: booking.currency ?? "", bookingID: booking.bookingID ?? "", loyalty_points: model.appliedSmiles > 0 ? model.appliedSmiles : nil) { (orderID) in
                // Procceed only for valid order id
                guard orderID.count > 0 else {
                    self.navigationController?.popToViewController(self, animated: true)
                    Timer.scheduledTimer(withTimeInterval: 0.75, repeats: false) { timer in
                        self.onPaymentError(3, description: kRazorpayCrateOrderFailedDescription, andData: [:])
                        timer.invalidate()
                    }
                    return
                }
                
                let data = self.getWebViewPaymentId(paymentVC: paymentVC, order_id: orderID)
                completion(data.webView, data.options, orderID, self.razorpay, "")
                
            }
        }
    }

protocol BBQAfterDiningDelegate{
    func onPaymentSuceess()
}
