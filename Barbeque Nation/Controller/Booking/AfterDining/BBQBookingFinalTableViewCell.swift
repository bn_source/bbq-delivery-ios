//
 //  Created by Ajith CP on 14/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQBookingFinalTableViewCell.swift
 //

import UIKit

protocol BBQBookingFinalTableViewCellProtocol: AnyObject {
    func didPressReserveButton(isPayNow: Bool)
    func didAdvacnceRequired()
    func didPressOpenBill(with recieptURL: String)
}

extension BBQBookingFinalTableViewCellProtocol {
    // Not everywhere needed.
    func didPressOpenBill(with recieptURL: String) { }
}

class BBQBookingFinalTableViewCell: UITableViewCell {
    
    @IBOutlet weak var finalAmountTitleLabel: UILabel!
    @IBOutlet weak var finalAmountLabel: UILabel!
    @IBOutlet weak var paymentInfoLabel: UILabel!
    
    @IBOutlet weak var paymentButton: UIButton!
    @IBOutlet weak var recieptButton: UIButton!
    @IBOutlet weak var btnPayFull: UIButton!
    @IBOutlet weak var btnPayLater: UIButton!
    @IBOutlet weak var viewForAdvance: UIView!
    @IBOutlet weak var stackViewForPayment: UIStackView!
    
    @IBOutlet weak var payButtonHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var recieptButtonHeightConstraint: NSLayoutConstraint!

    weak var delegate: BBQBookingFinalTableViewCellProtocol? = nil
    
    private var bookingScreenType: BookingScreenType = .Booking
    private var recieptURLString : String = ""

    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        self.paymentInfoLabel.text = String(format: "%@ %@\n%@", kMyCartPriceTermsText, "INR", "Advance Booking Fee may be charged for large group bookings")
    }

    // MARK: Public Methods
    func loadFinalPaymentContent(with viewModel: SlotsViewModel, screenType: BookingScreenType) {
        
        self.setupButtonTitlefor(screenType: screenType)
        self.setupRecieptLinkButton(screenType: screenType)
        let finalAmount = viewModel.finalAmountForPayment < 0 ? 0.00 : viewModel.finalAmountForPayment
        self.finalAmountLabel.text = getCurrency() + String(format: "%.0f", round(finalAmount)).toCurrencyFormat()
    }
    
    func loadFinalPaymentAfterDining(with viewModel:BBQAfterDiningControllerViewModel,
                                     screenType: BookingScreenType) {
       
        self.recieptURLString = viewModel.paymentRecieptURLString
        
        self.setupButtonTitlefor(screenType: screenType)
        self.setupRecieptLinkButton(screenType: screenType)
        self.setupPayButtonVisibility(screenType: screenType,
                                      isHidden: !viewModel.isPaymentEnabled())
        
        let finalAmount = viewModel.bookingPayTotal < 0 ? 0.00 : viewModel.bookingPayTotal
        self.finalAmountTitleLabel.text = "Net Payable"
        self.finalAmountLabel.text = String(format: "%@%.0f",getCurrency(), round(finalAmount))
        if viewModel.diningDataModel?.isBookingSettled() ?? false{
            self.finalAmountLabel.text = "\(getCurrency())0"
        }
        
    }
    
    // TODO: Move to view model
    func setupButtonTitlefor(screenType: BookingScreenType) {
        var title = "Reserve Table"
        if screenType == .Reschedule {
            title = "Modify"
        }
        if screenType == .AfterDining {
            title = kPaymentButtonTitle
        }
        self.paymentButton.setTitle(title, for: .normal)
    }
    
    func setupRecieptLinkButton(screenType: BookingScreenType)  {
        if screenType == .AfterDining &&  self.recieptURLString != "" {
            self.recieptButtonHeightConstraint.constant = 30.00
            self.recieptButton.isHidden = false
        } else {
            self.recieptButtonHeightConstraint.constant = 0.00
            self.recieptButton.isHidden = true
        }
    }
    
    func setupPayButtonVisibility(screenType: BookingScreenType, isHidden: Bool) {
        if screenType == .AfterDining && isHidden {
            self.paymentButton.isHidden = true
            self.payButtonHeightConstraint.constant = 0
        } else {
            self.paymentButton.isHidden = false
            self.payButtonHeightConstraint.constant = 48
        }
    }
    
    // MARK: IBAction Methods
    
    @IBAction func reserveTablePressed(_ sender: UIButton) {
        if let delegate = self.delegate {
            delegate.didPressReserveButton(isPayNow: false)
        }
    }
    
    @IBAction func openBillButtonPressed(_ sender: UIButton) {
        if self.recieptURLString != "" {
            self.delegate?.didPressOpenBill(with: self.recieptURLString)
        } else {
            ToastHandler.showToastWithMessage(message: "No reciept URL to show")
        }
    }
    @IBAction func onClickBtnInfo(_ sender: Any) {
        
    }
    @IBAction func onClickBtnPayAdvance(_ sender: Any) {
        
    }
    @IBAction func onClickBtnPayLater(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didPressReserveButton(isPayNow: false)
        }
    }
    @IBAction func onClickBtnPayNow(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didPressReserveButton(isPayNow: true)
        }
    }
    

}
