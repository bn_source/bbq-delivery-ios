//
 //  Created by Sakir Sherasiya on 21/12/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified AfterDiningPaymentCell.swift
 //

import UIKit

class AfterDiningPaymentCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblValue: UIButton!
    @IBOutlet weak var lblLineSeprator: UILabel!
    @IBOutlet weak var imgSmiles: UIImageView!
    
    var afterdiningData: AfterDiningData?
    var delegate: AfterDiningPaymentCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func setCellData(afterdiningData: AfterDiningData, delegate: AfterDiningPaymentCellDelegate?, isLastCell: Bool)  {
        self.afterdiningData = afterdiningData
        self.delegate = delegate
        lblTitle.text = afterdiningData.title
        lblTitle.isHidden = false
        imgSmiles.isHidden = true
        if afterdiningData.title == "Smiles"{
            lblTitle.isHidden = true
            imgSmiles.isHidden = false
        }
        if afterdiningData.title == "Payable Amount"{
            lblTitle.font = UIFont.appThemeMediumWith(size: 14.0)
            lblValue.titleLabel?.font = UIFont.appThemeMediumWith(size: 14.0)
        }else{
            lblTitle.font = UIFont.appThemeRegularWith(size: 13.0)
            lblValue.titleLabel?.font = UIFont.appThemeRegularWith(size: 13.0)
        }
        if afterdiningData.value.contains("-"){
            lblValue.setTitleColor(.darkGreen, for: .normal)
        }else if afterdiningData.breakup.count > 0{
            lblValue.setTitleColor(.theme, for: .normal)
        }else{
            lblValue.setTitleColor(.text, for: .normal)
            if afterdiningData.title == "Payable Amount"{
                lblValue.setTitleColor(.darkText, for: .normal)
            }
        }
        if afterdiningData.breakup.count > 0{
            let string =  NSAttributedString(string: afterdiningData.value, attributes: [.underlineStyle : NSUnderlineStyle.single.rawValue])
            self.lblValue.setAttributedTitle(string, for: .normal)
        }else{
            let string =  NSAttributedString(string: afterdiningData.value, attributes: [:])
            self.lblValue.setAttributedTitle(string, for: .normal)
        }
        lblLineSeprator.isHidden = isLastCell
    }
    
    @IBAction func onClickBtnValue(_ sender: Any) {
        if let afterdiningData = afterdiningData{
            delegate?.didTapOnDetails(cell: self, afterDiningData: afterdiningData)
        }
    }
}

protocol AfterDiningPaymentCellDelegate{
    func didTapOnDetails(cell: AfterDiningPaymentCell, afterDiningData: AfterDiningData)
}

struct AfterDiningData {
    struct BreakUp{
        var title: String
        var value: String
        
        init(title: String, value: String) {
            self.title = title
            self.value = value
        }
    }
    
    var title: String
    var value: String
    var breakup: [BreakUp]
    
    init(title: String, value: String, breakup: [BreakUp]) {
        self.title = title
        self.value = value
        self.breakup = breakup
    }
}

