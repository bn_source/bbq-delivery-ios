//
 //  Created by Rabindra L on 26/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQAfterDiningViewController.swift
 //

import UIKit

class BBQAfterDiningViewController: UIViewController {

    // MARK: Outlets
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var viewForBackground: UIView!
    
    @IBOutlet weak var bookingID: UILabel!
    @IBOutlet weak var locationName: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var payAmountTitleLabel: UILabel!
    
    @IBOutlet weak var btnPayNow: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    private var amountDetailsHeight : CGFloat?

    internal var isTaxSplitRefreshNeeded : Bool = true
    internal var isCardsResetWarned : Bool = false
    internal var isPointsResetWarned : Bool = false
    
    private var didAddPoints     : Bool = false
    private var didAddCoupons    : Bool = false
    private var didAddVouchers   : Bool = false
    private var didAddCorpOffers : Bool = false
    
    private var didAppliedPointsAlready : Bool =  false
    
    internal var appliedCouponAmount  : Double = 0.00
    internal var appliedVoucherAmount : Double = 0.00
    internal var appliedPointsAmount : Double = 0.00
    internal var appliedPoints : Int? = nil
    
    internal var pointsValueModel: BBQPointsValueModel? = nil
    internal var appliedCoupons    : [VoucherCouponsData]?
    internal var initialAppliedVouchers   : [VoucherCouponsData]?
    internal var appliedVouchers   : [VoucherCouponsData]?
    internal var appliedCorpOffers : [VoucherCouponsData]?
    internal var vouchersTerms : [String]?


    var bookingIDString: String? = nil
    var diningBottomSheet : BottomSheetController?
    var corpBottomSheet : BottomSheetController?
    var delegate: BBQAfterDiningDelegate?

    typealias AddedCompletionBlock = (_ isLoaded: Bool) -> Void
    var completionBlock: AddedCompletionBlock? = nil

    // MARK: Lazy Initializer
    
    lazy private var afterDiningViewModel : BBQAfterDiningControllerViewModel = {
        let afterDiningVM = BBQAfterDiningControllerViewModel()
        return afterDiningVM
    } ()
    
    lazy var buffetsViewModel: BBQBookingBuffetsViewModel = {
        let buffetModel = BBQBookingBuffetsViewModel()
        return buffetModel
    }()

    lazy var slotsViewModel: SlotsViewModel = {
        let slotModel = SlotsViewModel()
        return slotModel
    }()
    
    lazy var cartViewModel: CartViewModel = {
        let viewModel = CartViewModel()
        return viewModel
    }()
    
    // MARK: Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AnalyticsHelper.shared.triggerEvent(type: .AP01)
        self.setupDiningComponent()
        self.fetchAfterDiningPaymentData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.screenName(name: .After_Dining_Payment_Screen)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        BBQApplyVouchersTableViewCell.initialAppliedVouchers = nil
    }
    
    // MARK: Setup Methods
    
    func setupDiningComponent() {
        
        self.setupDiningTableView()
        
        self.payAmountTitleLabel.text = kDiningPayAmount
        self.appliedVouchers = [VoucherCouponsData]()
        
        self.tableView.contentInset = UIEdgeInsets(top: 24, left: 0, bottom: 0, right: 0)
    }
    
    func setupDiningTableView() {
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.estimatedRowHeight = 300
        self.tableView.rowHeight = UITableView.automaticDimension
        
        self.tableView.register(UINib(nibName: "BBQBookingAmountDetailsTableViewCell",
                                             bundle: nil),
                                       forCellReuseIdentifier: "BBQBookingAmountDetailsTableViewCellID")
        
        self.tableView.register(UINib(nibName: "BBQBookingPointsTableViewCell",
                                      bundle: nil),
                                forCellReuseIdentifier: "BBQBookingPointsTableViewCellID")
        
        self.tableView.register(UINib(nibName: "BBQBookingFinalTableViewCell",
                                      bundle: nil),
                                forCellReuseIdentifier: "BBQBookingFinalTableViewCellID")
        
        self.tableView.register(UINib(nibName: "BBQCorpOffersTableViewCell",
                                      bundle: nil),
                                forCellReuseIdentifier: "BBQCorpOffersTableViewCellID")
        
        self.tableView.register(UINib(nibName: "BBQCouponsTableViewCell",
                                      bundle: nil),
                                forCellReuseIdentifier: "BBQCouponsTableViewCellID")
        
        self.tableView.register(UINib(nibName: "BBQApplyVouchersTableViewCell",
                                      bundle: nil),
                                forCellReuseIdentifier: "BBQApplyVouchersTableViewCellID")
        

    }
    
    
    // MARK: API Methods
    
    func fetchAfterDiningPaymentData()  {

        BBQActivityIndicator.showIndicator(view: self.subView)
        self.afterDiningViewModel.fetchAfterDiningPaymentData(for: self.bookingIDString ?? "") { (isFetched) in
            
            BBQActivityIndicator.hideIndicator(from: self.subView)
            
            if let amount = self.afterDiningViewModel.diningDataModel?.payments?.amountPaid, amount > 0{
                self.fetchAfterDiningPaymentDataDetails(billTotal: self.afterDiningViewModel.getTotalWithTax())
            }else{
                self.refreshData()
            }
        }
    }
    
    func fetchAfterDiningPaymentDataDetails(billTotal: Double?)  {

        BBQActivityIndicator.showIndicator(view: self.subView)
        self.afterDiningViewModel.afterDiningPaymentDataDetails(for: self.bookingIDString ?? "") { (isFetched) in
            BBQActivityIndicator.hideIndicator(from: self.subView)
            self.afterDiningViewModel.diningDataModel?.billWithTax = billTotal
            self.refreshData()
        }
    }
    
    private func refreshData(){
        if let pointsModel = self.afterDiningViewModel.pointsValueModel {
            self.pointsValueModel = pointsModel
            self.didAppliedPointsAlready = true
        }
        if let couponsApplied = self.afterDiningViewModel.appliedCouponList, couponsApplied.count > 0 {
            self.appliedCoupons = couponsApplied
            self.didAddCoupons = true
        }
        if let corpOffersApplied = self.afterDiningViewModel.appliedCorpOffersList, corpOffersApplied.count > 0 {
            self.appliedCorpOffers = corpOffersApplied
            self.didAddCoupons = true
        }
        if let vouchersApplied = self.afterDiningViewModel.appliedVoucherList, vouchersApplied.count > 0 {
            self.initialAppliedVouchers = vouchersApplied
            BBQApplyVouchersTableViewCell.initialAppliedVouchers = vouchersApplied
        }
        
        self.prepareBuffetData()
        self.loadDataAfterDataFetch()
        
        if self.afterDiningViewModel.diningDataModel?.isBookingSettled() ?? false{
            self.btnPayNow.isHidden = true
        }else{
            self.btnPayNow.isHidden = false
        }
        
        self.tableView.reloadData()
    }
    
    internal func getPointsFromServer() {
        let priceInDouble = self.afterDiningViewModel.diningDataModel?.netPayable ?? 0.00
        let price = String(format: "%.02f", priceInDouble)
        BBQActivityIndicator.showIndicator(view: self.view)
        self.cartViewModel.getLoyaltyPointsValue(amount: price,
                                                 currency: "INR",
                                                 completion: { (isSuccess) in
                                                    BBQActivityIndicator.hideIndicator(from: self.view)
                                                    if isSuccess {
                                                        self.didAddPoints = true
                                                        self.pointsValueModel = self.cartViewModel.getPointValueModel
                                                        self.appliedPointsAmount = self.pointsValueModel?.redeemAmount ?? 0
                                                        self.appliedPoints = self.pointsValueModel?.redeemablePoints
                                                        self.afterDiningViewModel.adjustTotalAmountForPayment(with: Double(self.pointsValueModel?.redeemAmount ?? 0),
                                                                                                              isAddition: true,
                                                                                                              offerType: .Smiles)
                                                        self.tableView.reloadData {
                                                            self.adjustBottomSheetComponent()
                                                        }
                                                    }
        })
    }
    
    func updateFinalPaymentDetails(paymentOrderId: String? = nil,
                                   paymentId: String? = nil,
                                   paymentGatewayName: String? = nil,
                                   paymentSignature: String? = nil) {
        
        //        let corpOfferData = CorporateOffer(voucherCode: appliedCorpOffers?[0].barcode ?? "",
        //                                               voucherType: appliedCorpOffers?[0].voucherType ?? "",
        //                                               voucherAmount: appliedCorpOffers?[0].denomination ?? 0)
        
        // Only new vouchers can be added in after dining payment. Passing other 2 nil here.
        AnalyticsHelper.shared.triggerEvent(type: .AP10)
        let voucherDetails =
            self.buffetsViewModel.createConsolidatedVoucherDetails(appliedCorpOffers: nil,
                                                                   appliedCoupons: self.appliedCoupons,
                                                                   appliedVouchers: self.appliedVouchers,
                                                                   arrAddedAmounts: self.afterDiningViewModel.getAddedAmounts(),
                                                                   preferredBranch: nil)
        
        let updatePayModel = FinalPaymentUpdateBody(bookingId: self.bookingIDString ?? "",
                                                    branch_id: afterDiningViewModel.diningDataModel?.branchId, paymentGateway: paymentGatewayName,
                                                    paymentID: paymentId,
                                                    orderID: paymentOrderId,
                                                    paymentSignature: paymentSignature,
                                                    loyaltyPoints: self.appliedPoints,
                                                    loyaltyAmount: self.appliedPointsAmount > 0.00 ? self.appliedPointsAmount : nil,
                                                    voucherDetails: voucherDetails.voucherDetails, currency: afterDiningViewModel.diningDataModel?.currency, reservation_date: afterDiningViewModel.diningDataModel?.reservationDate, reservation_time: afterDiningViewModel.diningDataModel?.reservationTime, country_code: "+91", mobile_number: Int64(SharedProfileInfo.shared.profileData?.mobileNumber ?? BBQUserDefaults.sharedInstance.customerId))
        BBQActivityIndicator.showIndicator(view: self.view)
        self.afterDiningViewModel.updateBooking(finalPaymentData: updatePayModel) { (isUpdated) in
            BBQActivityIndicator.hideIndicator(from: self.view)
            if isUpdated {
                AnalyticsHelper.shared.triggerEvent(type: .AP10A)
                self.showPaymentSuccessScreen()
            }else{
                AnalyticsHelper.shared.triggerEvent(type: .AP10B)
            }
            
            
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if touch?.view == self.view {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    // MARK: Private Methods
    
    private func expandCollapseBottomSheet(newHeight: CGFloat, isExpand: Bool) {
        if let bottomSheet = self.diningBottomSheet {
            let adjustHeight =  newHeight - self.view.frame.height
            bottomSheet.expandOrCollapse(viewController: self, withHeight: adjustHeight, isExpand: isExpand)
        }
    }
    
    private func loadDataAfterDataFetch() {
        self.isTaxSplitRefreshNeeded = true
        self.dateLabel.text = self.afterDiningViewModel.bookingSlotInformation
        self.locationName.text = self.afterDiningViewModel.diningDataModel?.branchName ?? ""
        self.bookingID.text = "#\(self.afterDiningViewModel.diningDataModel?.bookingID ?? "")"
    }
    
    private func prepareBuffetData() {
        if let resDate = self.afterDiningViewModel.diningDataModel?.reservationDate,
           let resTime = self.afterDiningViewModel.diningDataModel?.reservationTime,
            let numberOfPacks = self.afterDiningViewModel.totalNumberOfPacks,
            let branchID = self.afterDiningViewModel.diningDataModel?.branchId,
            let totalAmount = self.afterDiningViewModel.diningDataModel?.billTotal,
            let slotID = self.afterDiningViewModel.diningDataModel?.slotId {
            self.buffetsViewModel.saveBookingDataForCorporateOffers(reservationDate: resDate + " " + resTime,
                                                                    packs: numberOfPacks,
                                                                    branchId: branchID,
                                                                    totalAmount: totalAmount,
                                                                    slotId: Int(slotID) ?? 0, currency: "")
        }
    }
    @IBAction func onClickBtnTestPayment(_ sender: Any) {
        self.updateFinalPaymentDetails()
    }
    
    @IBAction func onlickBtnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
  
    @IBAction func onlickBtnPayment(_ sender: Any) {
        self.proceedToPaymentPressed(paymentAmount: round(self.afterDiningViewModel.bookingPayTotal), paymentRemarks: "")
    }
    
}

extension BBQAfterDiningViewController: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: UITableViewDelegate & Datasource methods

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
            return self.amountDetailsHeight ?? 44.0
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.appliedCorpOffers == nil {
            return 5
        }
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
            
        case 0:
            return self.loadBookingAmountDetailsCell(for: indexPath.row)
            
        case 1:
            return self.loadPointsDetailsCell(for: indexPath.row)
            
        case 2:
             return self.loadBookingCouponsCell(for: indexPath.row)
        
        case 3:
            return self.loadVouchersDetailsCell(for: indexPath.row)
            
        case 4:
            if self.appliedCorpOffers == nil {
                return self.loadFinalPaymentCell(for: indexPath.row)
            }
            return self.loadCorporateOffersCell(for: indexPath.row)
            
        default:
            return self.loadFinalPaymentCell(for: indexPath.row)
        }
    }
    
    // MARK: Private Methods
    
    private func loadBookingAmountDetailsCell(for index: Int) -> BBQBookingAmountDetailsTableViewCell {

        let amountSplitCell =
            self.tableView.dequeueReusableCell(withIdentifier: "BBQBookingAmountDetailsTableViewCellID")
                as! BBQBookingAmountDetailsTableViewCell
        amountSplitCell.amountCellDelegate = self
        
        if self.isTaxSplitRefreshNeeded {
            if let _ = self.afterDiningViewModel.taxDataList {
                amountSplitCell.loadAfterDiningAmountDetails(with: self.afterDiningViewModel)
            }
            self.isTaxSplitRefreshNeeded = false
        }
        return amountSplitCell
    }
    
    private func loadPointsDetailsCell(for index: Int) -> BBQBookingPointsTableViewCell {
        
        let pointsCell =
            self.tableView.dequeueReusableCell(withIdentifier: "BBQBookingPointsTableViewCellID")
                as! BBQBookingPointsTableViewCell
        pointsCell.pointCellDelegate = self
        pointsCell.loadAppliedPointsCell(pointsModel:pointsValueModel ?? BBQPointsValueModel(),
                                             didAppliedAlready: didAppliedPointsAlready,
                                             didAddNewPoints: didAddPoints,
                                             isPointEnabled: self.afterDiningViewModel.isPointsEnabled())
        return pointsCell
    }
    
    private func loadCorporateOffersCell(for index: Int) -> BBQCorpOffersTableViewCell {
        
        let corpCell =
            self.tableView.dequeueReusableCell(withIdentifier: "BBQCorpOffersTableViewCellID")
                as! BBQCorpOffersTableViewCell
        corpCell.contentView.layer.opacity = didAddCoupons ? 0.4 : 1.0
        corpCell.isUserInteractionEnabled = !didAddCoupons
        
        if let appliedOffers = self.appliedCorpOffers {
            corpCell.loadAppliedCorpOffers(for: self.afterDiningViewModel.diningDataModel?.numberOfPacks ?? 0,
                                           offerData: appliedOffers[0],
                                           isCorpOffersApplied: self.didAddCorpOffers,
                                           isCardEnabled: afterDiningViewModel.isCorpOffersEnabled(isCouponsAdded: didAddCoupons))
        }
        return corpCell
    }
    
    private func loadBookingCouponsCell(for index: Int) -> BBQCouponsTableViewCell {
        
        let couponCell =
            self.tableView.dequeueReusableCell(withIdentifier: "BBQCouponsTableViewCellID")
                as! BBQCouponsTableViewCell
        couponCell.couponDelegate = self
        couponCell.loadCouponsData(with: self.appliedCoupons ?? [VoucherCouponsData](),
                                   didAdd: self.didAddCoupons,
                                   isCardEnabled: afterDiningViewModel.isCouponsEnabled(isCorpOffersAdded: didAddCorpOffers),
                                   bookingType: .AfterDining)
        return couponCell
    }
    
    
    private func loadVouchersDetailsCell(for index: Int) -> BBQApplyVouchersTableViewCell {
        let voucherCell =
            self.tableView.dequeueReusableCell(withIdentifier: "BBQApplyVouchersTableViewCellID")
                as! BBQApplyVouchersTableViewCell
        voucherCell.voucherDelegate = self
        voucherCell.loadVouchersData(with: self.appliedVouchers ?? [VoucherCouponsData](),
                                     didAdd: self.didAddVouchers,
                                     isCardEnabled: afterDiningViewModel.isHappinessCardsEnabled())
        
        return voucherCell
    }

    private func loadFinalPaymentCell(for index: Int) -> BBQBookingFinalTableViewCell {
        let finalCell =
            self.tableView.dequeueReusableCell(withIdentifier: "BBQBookingFinalTableViewCellID")
                as! BBQBookingFinalTableViewCell
        finalCell.loadFinalPaymentAfterDining(with: self.afterDiningViewModel,
                                              screenType: .AfterDining)
//        if !(afterDiningViewModel.diningDataModel?.showPayment ?? false), afterDiningViewModel.diningDataModel?.netPayable ?? 0 > 0{
//            //finalCell.setupPayButtonVisibility(screenType: .AfterDining, isHidden: true)
//        }
        finalCell.setupPayButtonVisibility(screenType: .AfterDining, isHidden: true)
        finalCell.delegate = self

        return finalCell
    }

}

extension BBQAfterDiningViewController : BBQBookingAmountDetailsTableViewCellDelegate,
BBQBookingPointsTableViewCellDelegate,
BBQCouponsTableViewCellDelegate, BBQApplyVouchersTableViewCellDelegate,
                                         BBQBookingFinalTableViewCellProtocol {
    func didClickOnDetails(cell: BBQAmountEntryTableViewCell) {
        
    }
    
    
    func didPressReserveButton(isPayNow: Bool) {
        self.proceedToPaymentPressed(paymentAmount: round(self.afterDiningViewModel.bookingPayTotal),
                                     paymentRemarks: "")
    }
    
    func didAdvacnceRequired() {
        
    }
    
    func didPressOpenBill(with recieptURL: String) {
        if recieptURL != ""{
            let webView = UIStoryboard.loadCommonWebView()
            webView.URLSring = recieptURL
            webView.strTitle = "Invoice"
            webView.orderID = self.afterDiningViewModel.diningDataModel?.bookingID ?? "Invoice"
            self.present(webView, animated: true, completion: nil)
        }
    }
    
    
    func didPressedVoucherAddRemoveButton(isAdd: Bool) {
        if isAdd {
            AnalyticsHelper.shared.triggerEvent(type: .AP06)
            self.showSmilesResetWarning { (doReset) in
                if doReset {
                    self.showAllOffersController(type: .VouchersForYou)
                }
            }
        } else {
            AnalyticsHelper.shared.triggerEvent(type: .AP07)
            self.didAddVouchers = false
            self.appliedVouchers?.removeAll()
            self.afterDiningViewModel.adjustTotalAmountForPayment(with: self.appliedVoucherAmount,
                                                            isAddition: false,
                                                            offerType: .Vouchers)
            let smilesIndex = IndexPath(row: 1, section: 0)
            let couponIndex = IndexPath(row: 2, section: 0)
            let voucherIndex = IndexPath(row: 3, section: 0)
            let paymentIndex = self.appliedCorpOffers == nil ? IndexPath(row: 4, section: 0) :
                IndexPath(row: 5, section: 0)
            self.tableView.reloadRows(at: [smilesIndex, couponIndex, voucherIndex, paymentIndex], with: .none)
            self.adjustBottomSheetComponent()
        }
    }
    
    func didPressedVouchersTermsAndConditions() {
        let termsVC = UIStoryboard.loadTermsAndConditionsScreen()
        termsVC.view.layoutIfNeeded()
        termsVC.modalPresentationStyle = .overFullScreen
        if let vouchersTermsData = self.vouchersTerms {
            termsVC.loadTermsControllerWith(termsData: vouchersTermsData)
            self.present(termsVC, animated: true, completion: nil)
        }
    }
    
    func didPressedAddRemoveCouponButton(isAdd: Bool) {
        if isAdd {
            AnalyticsHelper.shared.triggerEvent(type: .AP04)
            self.showSmilesResetWarning { (doReset) in
                if doReset {
                    self.showAllOffersController(type: .CouponsForYou)
                }
            }
        } else {
            AnalyticsHelper.shared.triggerEvent(type: .AP05)
            self.didAddCoupons = false
            self.afterDiningViewModel.adjustTotalAmountForPayment(with: self.appliedCouponAmount,
                                                            isAddition: false,
                                                            offerType: .Coupons)
            let smilesIndex = IndexPath(row: 1, section: 0)
            let couponsIndex = IndexPath(row: 2, section: 0)
            let voucherIndex = IndexPath(row: 3, section: 0)
            let paymentIndex = self.appliedCorpOffers == nil ? IndexPath(row: 4, section: 0) : IndexPath(row: 5, section: 0)
            let corpOffersIndex = IndexPath(row: 4, section: 0)
            let cardArray = self.appliedCorpOffers == nil ? [smilesIndex, couponsIndex,voucherIndex, paymentIndex] :
                [couponsIndex, paymentIndex, corpOffersIndex]
                self.tableView.reloadRows(at: cardArray, with: .none)
            self.adjustBottomSheetComponent()
        }
    }
    
    func didPressedCouponsTermsAndConditions() {
        
    }
    
    func didPressedTermsAndConditions() {
        
    }
    
    
    func didPressedPointsAddRemoveButton(isAdd: Bool) {
        if isAdd {
            AnalyticsHelper.shared.triggerEvent(type: .AP02)
            self.getPointsFromServer()
        } else {
            AnalyticsHelper.shared.triggerEvent(type: .AP03)
            self.didAddPoints = false
            self.afterDiningViewModel.adjustTotalAmountForPayment(with: Double(self.pointsValueModel?.redeemAmount ?? 0),
                                                            isAddition: false,
                                                            offerType: .Smiles)
            let smilesIndex = IndexPath(row: 1, section: 0)
            let couponIndex = IndexPath(row: 2, section: 0)
            let voucherIndex = IndexPath(row: 3, section: 0)
            let paymentIndex = self.appliedCorpOffers == nil ? IndexPath(row: 4, section: 0) : IndexPath(row: 5, section: 0)
            self.tableView.reloadRows(at: [smilesIndex, couponIndex, voucherIndex, paymentIndex], with: .none)
            self.adjustBottomSheetComponent()
        }
    }

    func contentDidChange(cell: BBQBookingAmountDetailsTableViewCell, contentHeight: CGFloat) {
        self.amountDetailsHeight = contentHeight
        self.adjustBottomSheetComponent()
    }
    
    // MARK: Private Methods
    
    func adjustBottomSheetComponent() {
        
        self.tableView.beginUpdates()
        self.tableView.endUpdates()
        
        let modifiedHeight = self.tableView.contentSize.height + 180
        self.expandCollapseBottomSheet(newHeight: CGFloat(modifiedHeight), isExpand: true)
    }
    
    func resetCards() {
        
        self.didAddCoupons = false
        self.didAddVouchers = false
        self.didAddCorpOffers = false
        self.didAddPoints = false
        
        self.appliedPoints = 0
        self.appliedPointsAmount = 0.00
        self.pointsValueModel = nil
        
        
        self.appliedCoupons?.removeAll()
        self.appliedVouchers?.removeAll()
        self.appliedCorpOffers?.removeAll()
    }
    
    private func showSmilesResetWarning(completion: @escaping (_ doReset: Bool)->()) {
        if !self.isPointsResetWarned && self.didAddPoints {
            PopupHandler.showTwoButtonsPopup(title: "Applied Smiles will be removed",
                                             message: "All applied Smiles will be removed if you apply any coupon/happiness card/ corporate offers on top of Smiles. Please re -apply Smiles again.",
                                             on: self, firstButtonTitle: "Cancel",
                                             firstAction: {
                                                completion(false)
                                                
            }, secondButtonTitle: "OK", secondAction: {
                completion(true)
            })
            self.isPointsResetWarned = true
        } else {
            completion(true)
        }
        
    }
    
    private func resetPoints() {
        if didAddPoints {
            self.didAddPoints = false
            self.afterDiningViewModel.adjustTotalAmountForPayment(with: self.appliedPointsAmount,
                                                            isAddition: false,
                                                            offerType: .Smiles)
        }
    }
    
}

extension BBQAfterDiningViewController { // Navigations to different controllers.
    
    internal func showAllOffersController(type: ScreenType) {
        let viewController = BBQAllOffersViewController()
        viewController.screenType = type
        viewController.isAfterDiningScreen = false
        viewController.doManualCoupon = true
        viewController.doManualCoupon = true
        
        if type == .VouchersForYou || type == .CouponsForYou {
            
            let totalPacks = self.afterDiningViewModel.diningDataModel?.numberOfPacks ?? 0
            let branchID = self.afterDiningViewModel.diningDataModel?.branchId ?? "0"
            let slotID = self.afterDiningViewModel.diningDataModel?.slotId ?? "0"
            let currency = self.afterDiningViewModel.diningDataModel?.currency ?? ""
            let finalAmount = self.afterDiningViewModel.diningDataModel?.billTotal ?? 0.00
            let resDate = self.afterDiningViewModel.diningDataModel?.reservationDate ?? ""
            let resTime = self.afterDiningViewModel.diningDataModel?.reservationTime ?? ""
            
            self.buffetsViewModel.saveBookingDataForCorporateOffers(reservationDate: resDate + " " + resTime,
                                                                    packs: totalPacks,
                                                                    branchId: branchID,
                                                                    totalAmount: finalAmount,
                                                                    slotId: Int(slotID) ?? 0,
                                                                    currency: currency)
            viewController .buffetsViewModel = self.buffetsViewModel
        }
        
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: UIScreen.main.bounds.size.height - viewController.view.frame.size.height)
        let bottomSheet = BottomSheetController(configuration: configuration)
        viewController.delegate = self
        viewController.bottomSheet = bottomSheet
//        bottomSheet.present(viewController, on: self)
        viewController.modalPresentationStyle = .fullScreen
        self.present(viewController, animated: true, completion: nil)
    }
    
}

extension BBQAfterDiningViewController : BBQAllOffersProtocol {
   
    // MARK: BBQAllOffersProtocol Methods
    
    func buyNowButtonClicked(bottomSheet: BottomSheetController?) {
        
    }
    
    func userVoucherSelectedWith(voucherList:[VoucherCouponsData], type: ScreenType, termsAndConditions:[String]) {
        if type == .CouponsForYou {
            self.didAddCoupons = true
            self.appliedCoupons = voucherList
            let couponData = voucherList[0] // Only one coupon can apply.
            var currentAmount = self.afterDiningViewModel.bookingSubTotal
            currentAmount = currentAmount < 0.00 ? 0.00 : currentAmount
            let adjustedDenomination = self.slotsViewModel.consumedCouponAmount(denomination: couponData.denomination ?? 0.00,
                                                                                payAmount: currentAmount)
            self.resetPoints()
            self.appliedCouponAmount = adjustedDenomination
            self.afterDiningViewModel.adjustTotalAmountForPayment(with: adjustedDenomination,
                                                            isAddition: true,
                                                            offerType: .Coupons)
            self.isTaxSplitRefreshNeeded = true
            self.tableView.reloadData()
        }
        if type == .VouchersForYou {
            self.didAddVouchers = true
            self.appliedVouchers = voucherList
            self.vouchersTerms = termsAndConditions
            
            let totalVoucherAmount = self.slotsViewModel.totalVoucherAmount(for: self.appliedVouchers!)
            self.resetPoints()
            self.appliedVoucherAmount = totalVoucherAmount.amount + countVoucherPaxValue(pax: totalVoucherAmount.packs)
            self.afterDiningViewModel.adjustTotalAmountForPayment(with: self.appliedVoucherAmount,
                                                            isAddition: true,
                                                            offerType: .Vouchers)
            self.tableView.reloadData()
        }
    }
    
    private func countVoucherPaxValue(pax: Int) -> Double{
        var totalAmount = 0.0
        var arrAddedAmount = self.afterDiningViewModel.getAddedAmounts()
        for _ in 0..<pax{
            var index = 0
            for i in 0..<arrAddedAmount.count {
                if arrAddedAmount[i] > arrAddedAmount[index]{
                    index = i
                }
            }
            if arrAddedAmount.count > 0{
                totalAmount += arrAddedAmount[index]
                arrAddedAmount.remove(at: index)
            }else{
                break
            }
        }
        return totalAmount
    }
    
}

extension BBQAfterDiningViewController : BBQRazorpayControllerProtocol {
    
    func proceedToPaymentPressed(paymentAmount: Double, paymentRemarks: String) {
        
        // Calling booking details API once again to confirm net payable has not changed.
        AnalyticsHelper.shared.triggerEvent(type: .AP08)
        BBQActivityIndicator.showIndicator(view: self.view)
        self.afterDiningViewModel.fetchAfterDiningPaymentData(for: self.bookingIDString ?? "") { (isFetched) in
            BBQActivityIndicator.hideIndicator(from: self.view)
            if isFetched {
                if self.afterDiningViewModel.isOrderModified {
                    PopupHandler.showSingleButtonsPopup(title: "Change in order items",
                                                        message: "Your order has changed recently, Please re-apply smiles, coupons, happiness card once again if any",
                                                        on: self,
                                                        firstButtonTitle: "Okay",
                                                        firstAction: {
                                                             self.resetCards()
                                                             self.isTaxSplitRefreshNeeded = true
                                                             self.tableView.reloadData()
                    })
                } else {
                   // If order is not modified after last reciept go for payment.
                    if paymentAmount <= 0.00 {
                        AnalyticsHelper.shared.triggerEvent(type: .AP08A)
                        self.updateFinalPaymentDetails()
                        return
                    }
                    AnalyticsHelper.shared.triggerEvent(type: .AP09)
                    let rpController = BBQRazorpayContainerController()
                    rpController.modalPresentationStyle = .overCurrentContext
                    self.present(rpController, animated: true, completion: nil)
                    rpController.payDelegate = self
                    rpController.createOrderForPayment(with: paymentAmount,
                                                       currency:"INR", title: "Final Payment",
                                                       and: paymentRemarks,
                                                       bookingID: self.bookingIDString,
                                                       loyalty_points: Int(self.appliedPointsAmount))
                }
            }
        }
    }
   
    func onRazorpayPaymentError(_ code: Int32, description str: String) {
        AnalyticsHelper.shared.triggerEvent(type: .AP09B)
        self.dismiss(animated: true) {
            switch code {
            case 0,1,3:
                PopupHandler.showSingleButtonsPopup(title: kFailedTitle,
                                                    message: str,
                                                    on: self, firstButtonTitle: kOkButtonTitle) { }
            case 2:
                self.showTransactionCancelled()
                
            default:
                self.diningBottomSheet?.dismissBottomSheet(on: self)
            }
        }
    }
    
    func onRazorpayPaymentSuccess(_ paymentData: BBQPaymentDataModel) {
        self.dismiss(animated: true) {
            AnalyticsHelper.shared.triggerEvent(type: .AP09A)
            self.updateFinalPaymentDetails(paymentOrderId: paymentData.razorpayOrderID ?? "",
                                           paymentId: paymentData.paymentID ?? "",
                                           paymentGatewayName: Constants.BBQBookingStatus.razorPayment,
                                           paymentSignature: paymentData.razorpaySignature ?? "")
        }
    }
    
    @objc func showTransactionCancelled() {
        let keyWindow = UIApplication.shared.keyWindow?.rootViewController?.presentedViewController
        
        guard let window = keyWindow else {
            return
        }
        
        PopupHandler.showSingleButtonsPopup(title: kMyCartTransactionCancelTitle,
                                            message: kMyCartTransactionCancelMessage,
                                            on: self,
                                            firstButtonTitle: kButtonOkay) { }
    }
    
    func showPaymentSuccessScreen() {
        let payConfirmVC = UIStoryboard.loadRegistrationConfirmation()
        payConfirmVC.modalPresentationStyle = .overFullScreen
        payConfirmVC.titleText = kPaymentSuccessText
        payConfirmVC.navigationText = kMyTransactionPaymentRedirectionMessage
        
        self.present(payConfirmVC, animated: false) {
            sleep(UInt32(2.0))
            
            self.dismiss(animated: true) {
                self.navigationController?.popViewController(animated: true)
                self.delegate?.onPaymentSuceess()
            }
        }
    }

}
