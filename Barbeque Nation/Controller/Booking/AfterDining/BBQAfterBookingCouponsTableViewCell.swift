//
 //  Created by Ajith CP on 15/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQAfterBookingCouponsTableViewCell.swift
 //

import UIKit
import FirebaseAnalytics

protocol BBQAfterBookingCouponsTableViewCellDelegate : AnyObject {
    
    func didPressedAddRemoveCouponButton(isAdd: Bool)
    
    func didPressedCouponsTermsAndConditions()
}

class BBQAfterBookingCouponsTableViewCell: UITableViewCell {
    
    weak var couponDelegate : BBQAfterBookingCouponsTableViewCellDelegate?
    var isCouponsAdded : Bool?
    
    @IBOutlet weak var termsConditionHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var appliedTickImageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var couponAmountHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var appliedTickImageView: UIImageView!
    @IBOutlet weak var couponLogoImgView: UIImageView!
    
    @IBOutlet weak var termsConditionsButton: UIButton!
    @IBOutlet weak var addRemoveButton: UIButton!

    @IBOutlet weak var couponTitleLabel: UILabel!
    @IBOutlet weak var couponVaalueLabel: UILabel!
    @IBOutlet weak var lblCouponAmount: UILabel!
    @IBOutlet weak var viewForContainer: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        AnalyticsHelper.shared.screenName(name: "CouponsScreen", className: "BBQAfterBookingCouponsTableViewCell")
        Analytics.logEvent("Coupons", parameters: [
            "name": "Coupons" as NSObject,
            "full_text": "Coupons page" as NSObject
            ])
        
        viewForContainer.dropShadow()
        self.setupCouponCell()
    }
    
    // MARK: Setup Methods
    
    func setupCouponCell() {
        self.isCouponsAdded = false
    }
    
    // MARK: Public Methods
    func loadCouponsData(with applyCoupons: [VoucherCouponsData],
                         didAdd: Bool,
                         isCardEnabled: Bool,
                         bookingType: BookingScreenType? = .Booking) {
        
        self.contentView.layer.opacity = isCardEnabled ?  1.0 : 0.4
        self.isUserInteractionEnabled = isCardEnabled
        
        self.isCouponsAdded = didAdd

        if didAdd{
            self.addRemoveButton.applyAddRemoveButtonTheme(title: kCouponsVouchersValueRemoveTitle, color: .theme)
        }else{
            self.addRemoveButton.applyAddRemoveButtonTheme(title: kCouponsVouchersValueAddTitle, color: .darkGreen)
        }
        
        
        if didAdd, let couponTitle = applyCoupons[0].title  {
            self.couponVaalueLabel.text = "\(couponTitle) applied"
            lblCouponAmount.text = "-\(getCurrency())\(applyCoupons[0].denomination ?? 0)"
            lblCouponAmount.textColor = .darkGreen
            lblCouponAmount.isHidden = false
        } else {
            self.couponVaalueLabel.text = ""
            lblCouponAmount.isHidden = true
        }
        
        self.hideShowCouponsValueLabel(isHidden: didAdd)
        
        if bookingType == .AfterDining && didAdd {
            self.addRemoveButton.isHidden = true
            self.termsConditionsButton.isHidden = true
            self.termsConditionHeightConstraint.constant = 0
            if isCardEnabled{
                self.addRemoveButton.isHidden = false
            }
        }
        
        self.layoutIfNeeded()
    }


    // MARK: IBAction Methods
    
    @IBAction func addRemoveButtonPressed(_ sender: Any) {
        self.isCouponsAdded = !self.isCouponsAdded!
        self.couponDelegate?.didPressedAddRemoveCouponButton(isAdd: self.isCouponsAdded!)
        self.isCouponsAdded = !self.isCouponsAdded!
    }
    
    @IBAction func termsNConditionsButtonPressed(_ sender: UIButton) {
        self.couponDelegate?.didPressedCouponsTermsAndConditions()
    }
    
    // MARK: Private Methods
    
    private func hideShowCouponsValueLabel(isHidden: Bool) {
        if isHidden {
            self.couponVaalueLabel.isHidden = false
            self.couponAmountHeightConstraint.constant = 20
            
            self.appliedTickImageView.isHidden = false
            self.appliedTickImageHeightConstraint.constant = 16
            
            self.termsConditionsButton.isHidden = false
            self.termsConditionHeightConstraint.constant = 26
            
            //self.contentView.backgroundColor = UIColor.hexStringToUIColor(hex: "F7F7F7")
            
        } else {
            self.couponVaalueLabel.isHidden = true
            self.couponAmountHeightConstraint.constant = 0
            
            self.appliedTickImageView.isHidden = true
            self.appliedTickImageHeightConstraint.constant = 0
            
            self.termsConditionsButton.isHidden = true
            self.termsConditionHeightConstraint.constant = 0
            
            //self.contentView.backgroundColor = .clear
        }
    }
}
