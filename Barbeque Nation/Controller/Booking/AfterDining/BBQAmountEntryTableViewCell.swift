//
 //  Created by Ajith CP on 14/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQAmountEntryTableViewCell.swift
 //

import UIKit

@objc protocol BBQAmountEntryTableViewCellDelegate: AnyObject {
    
    @objc func contentDidChange(cell: BBQAmountEntryTableViewCell)
    @objc func didClickOnDetails(cell: BBQAmountEntryTableViewCell)
    @objc func contentDidChange(cell: BBQMyCartGSTCell)
    @objc func didClickOnDetails(cell: BBQMyCartGSTCell)

}

class BBQAmountEntryTableViewCell: UITableViewCell {
    
    weak var entryDelegate : BBQAmountEntryTableViewCellDelegate?

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    
    // MARK: Public Methods
    
    func loadAmountEntries(with dataModel: AmountRowData) {
        self.titleLabel.textColor = .text
        self.valueLabel.textColor = .text
        
        self.titleLabel.text = dataModel.amountTitle
        self.valueLabel.text = dataModel.amountValue
        if dataModel.amountTitle == kDiningTaxServiceCharge{
            let string =  NSAttributedString(string: dataModel.amountValue ?? "0", attributes: [.underlineStyle : NSUnderlineStyle.single.rawValue])
            self.valueLabel.attributedText = string
            self.valueLabel.textColor = .theme
        }
    }
    @IBAction func onClickBtnValue(_ sender: Any) {
        if titleLabel.text == kDiningTaxServiceCharge{
            entryDelegate?.didClickOnDetails(cell: self)
        }
    }
    
}
