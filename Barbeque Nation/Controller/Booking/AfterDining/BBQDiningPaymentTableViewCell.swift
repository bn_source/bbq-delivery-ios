//
 //  Created by Ajith CP on 12/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQDiningPaymentTableViewCell.swift
 //

import UIKit

class BBQDiningPaymentTableViewCell: UITableViewCell {

    @IBOutlet weak var currencyDescLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.currencyDescLabel.text = kDiningCurrencyDesc
    }

    // MARK: IBAction Methods
    
    @IBAction func paymentButtonPressed(_ sender: UIButton) {
        
    }
}
