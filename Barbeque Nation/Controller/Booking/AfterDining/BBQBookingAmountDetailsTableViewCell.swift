//
 //  Created by Ajith CP on 14/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQBookingAmountDetailsTableViewCell.swift
 //

import UIKit

enum PaymentStatus : String {
    case AfterDining = "AfterDining"
    case Booking = "Booking"
}

@objc protocol BBQBookingAmountDetailsTableViewCellDelegate: AnyObject {
    @objc func contentDidChange(cell: BBQBookingAmountDetailsTableViewCell, contentHeight: CGFloat)
    @objc func contentDidChange(cell: BBQMyCartGSTCell, contentHeight: CGFloat)
    @objc func didClickOnDetails(cell: BBQAmountEntryTableViewCell)
}

class BBQBookingAmountDetailsTableViewCell: UITableViewCell {
   
    @IBOutlet weak var amountSplitTableView: UITableView!
    
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewForContainer: UIView!
    
    weak var amountCellDelegate: BBQBookingAmountDetailsTableViewCellDelegate?
    
    private var taxList : [Taxes]?
    private var slotsViewModel  : SlotsViewModel?
    private var diningViewModel : BBQAfterDiningControllerViewModel?
    
    private var bookingStatus : PaymentStatus?
    private var paidAmount: Double?
    
    var subTotalAmount : Double = 0.0 {
        didSet {
            self.slotsViewModel?.bookingAmountSubTotal = subTotalAmount
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupAmountSplitTable()
        viewForContainer.dropShadow()
    }
    
    // MARK: Setup Methods
    
    func setupAmountSplitTable() {
        self.amountSplitTableView.delegate = self
        self.amountSplitTableView.dataSource = self
        
        self.amountSplitTableView.estimatedRowHeight = 100
        self.amountSplitTableView.rowHeight = UITableView.automaticDimension
        
        self.amountSplitTableView.register(UINib(nibName: "BBQAmountEntryTableViewCell",
                                                 bundle: nil),
                                           forCellReuseIdentifier: "BBQAmountEntryTableViewCellID")
    }

    // MARK: Public Methods
    
    func loadBookingAmountDetails(with viewModel: SlotsViewModel) {
        self.bookingStatus = .Booking
        self.slotsViewModel = viewModel
        self.taxList = viewModel.getTaxesForPrefferedBranch()?.taxes
        self.amountSplitTableView.reloadData {
            self.amountCellDelegate?.contentDidChange(cell: self,
                                                      contentHeight: self.amountSplitTableView.contentSize.height)
        }
    }
    
    func loadAfterDiningAmountDetails(with viewModel: BBQAfterDiningControllerViewModel) {
        self.bookingStatus = .AfterDining
        self.diningViewModel = viewModel
        self.taxList = viewModel.taxDataList
        paidAmount = nil
        self.paidAmount = viewModel.diningDataModel?.payments?.amountPaid
        self.amountSplitTableView.reloadData {
            self.amountCellDelegate?.contentDidChange(cell: self,
                                                      contentHeight: self.amountSplitTableView.contentSize.height)
        }
    }
    
    
}


extension BBQBookingAmountDetailsTableViewCell : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if bookingStatus == .AfterDining {
//            return 1 // Always showing bill total row only in case of after dining.
            if paidAmount != nil, paidAmount != 0, let taxSplitList = self.taxList{
                return taxSplitList.count + 2
            }else if let taxSplitList = self.taxList{
                return taxSplitList.count + 1
            }
            return 1
        }
        return self.slotsViewModel?.getBookingAmountRowData().count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let entryCell = tableView.dequeueReusableCell(withIdentifier: "BBQAmountEntryTableViewCellID",
                                                      for: indexPath) as! BBQAmountEntryTableViewCell
  
        if bookingStatus == .AfterDining, indexPath.row > 0 {
            if let taxList = taxList, taxList.count >= indexPath.row{
                let rowData = AmountRowData(amountTitle: taxList[indexPath.row-1].tax, amountValue: "\(getCurrency())\(taxList[indexPath.row-1].taxAmount ?? 0)")
                entryCell.loadAmountEntries(with: rowData)
                return entryCell
            }
            let rowData = AmountRowData(amountTitle: "Paid amount", amountValue: "-\(getCurrency())\(paidAmount ?? 0)")
            entryCell.loadAmountEntries(with: rowData)
            entryCell.valueLabel.textColor = .darkGreen
            return entryCell
        }
        var rowData = self.slotsViewModel?.getBookingAmountRowData()[indexPath.row] // For booking
        if let afterDiningData = self.diningViewModel?.getPaymentAfterDiningAmountRowData() {
            rowData = afterDiningData[indexPath.row] // For after dining
        }
        if bookingStatus == .AfterDining {
            rowData?.amountValue = self.diningViewModel?.getEstimatedTotal()
        }
        entryCell.loadAmountEntries(with: rowData ?? AmountRowData())
        entryCell.entryDelegate = self
        return entryCell
    }
    
    
}
extension BBQBookingAmountDetailsTableViewCell: BBQAmountEntryTableViewCellDelegate{
    func contentDidChange(cell: BBQAmountEntryTableViewCell) {
        
    }
    
    func didClickOnDetails(cell: BBQAmountEntryTableViewCell) {
        amountCellDelegate?.didClickOnDetails(cell: cell)
    }
}
