//
 //  Created by Ajith CP on 14/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQBookingAmountDetailsTableViewCell.swift
 //

import UIKit


@objc protocol BBQBookingPaymentAmountDetailsTableViewCellDelegate: AnyObject {
    @objc func contentDidChange(cell: BBQBookingPaymentAmountDetailsTableViewCell, contentHeight: CGFloat)
    @objc func contentDidChange(cell: BBQMyCartGSTCell, contentHeight: CGFloat)
    @objc func didClickOnDetails(cell: BBQAmountEntryTableViewCell)
}

class BBQBookingPaymentAmountDetailsTableViewCell: UITableViewCell {
   
    @IBOutlet weak var amountSplitTableView: UITableView!
    
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewForContainer: UIView!
    
    weak var amountCellDelegate: BBQBookingPaymentAmountDetailsTableViewCellDelegate?
    
    private var taxList : [Taxes]?
    private var diningViewModel : BBQAfterDiningControllerViewModel?
    
    private var paidAmount: Double?
    private var isLastCell = false
    private var offerDiscount: Double = 0.0
    
    var subTotalAmount : Double = 0.0 {
        didSet {
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupAmountSplitTable()
        viewForContainer.dropShadow()
    }
    
    // MARK: Setup Methods
    
    func setupAmountSplitTable() {
        self.amountSplitTableView.delegate = self
        self.amountSplitTableView.dataSource = self
        
        self.amountSplitTableView.estimatedRowHeight = 100
        self.amountSplitTableView.rowHeight = UITableView.automaticDimension
        
        self.amountSplitTableView.register(UINib(nibName: "BBQAmountEntryTableViewCell",
                                                 bundle: nil),
                                           forCellReuseIdentifier: "BBQAmountEntryTableViewCellID")
    }

    // MARK: Public Methods
    
    
    func loadAfterDiningAmountDetails(with viewModel: BBQAfterDiningControllerViewModel, isLastCell: Bool) {
        self.diningViewModel = viewModel
        self.isLastCell = isLastCell
        self.taxList = viewModel.taxDataList
        paidAmount = nil
        self.paidAmount = viewModel.diningDataModel?.payments?.amountPaid
        self.offerDiscount = viewModel.diningDataModel?.getOfferDiscountDetails() ?? 0.0
        self.amountSplitTableView.reloadData {
            self.amountCellDelegate?.contentDidChange(cell: self,
                                                      contentHeight: self.amountSplitTableView.contentSize.height)
        }
    }
    
    
}


extension BBQBookingPaymentAmountDetailsTableViewCell : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
//            return 1 // Always showing bill total row only in case of after dining.
        if isLastCell{
            if paidAmount != nil, paidAmount != 0, let taxSplitList = self.taxList, offerDiscount > 0{
                return taxSplitList.count + 3
            }else if paidAmount != nil, paidAmount != 0, let taxSplitList = self.taxList{
                return taxSplitList.count + 2
            }else if let taxSplitList = self.taxList, offerDiscount > 0{
                return taxSplitList.count + 2
            }else if let taxSplitList = self.taxList{
                return taxSplitList.count + 1
            }
        }
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let entryCell = tableView.dequeueReusableCell(withIdentifier: "BBQAmountEntryTableViewCellID",
                                                      for: indexPath) as! BBQAmountEntryTableViewCell
        entryCell.isFontSizeRequired = false
        if indexPath.row > 0 {
            let count = offerDiscount > 0 ? 1 : 0
            if indexPath.row == 1, offerDiscount > 0{
                let rowData = AmountRowData(amountTitle: "Offer discount", amountValue: "-\(getCurrency())\(offerDiscount)")
                entryCell.loadAmountEntries(with: rowData)
                entryCell.valueLabel.textColor = .darkGreen
                return entryCell
            }
            if let taxList = taxList, taxList.count >= indexPath.row - count{
                let rowData = AmountRowData(amountTitle: taxList[indexPath.row-count-1].tax, amountValue: "\(getCurrency())\(taxList[indexPath.row-count-1].taxAmount ?? 0)")
                entryCell.loadAmountEntries(with: rowData)
                return entryCell
            }
            let rowData = AmountRowData(amountTitle: "Paid amount", amountValue: "-\(getCurrency())\(paidAmount ?? 0)")
            entryCell.loadAmountEntries(with: rowData)
            entryCell.valueLabel.textColor = .darkGreen
            return entryCell
        }
        
        var rowData = self.diningViewModel?.getPaymentAfterDiningAmountRowData()[indexPath.row]
        rowData?.amountValue = self.diningViewModel?.getEstimatedTotal()
        if isLastCell{
            let subTotal = self.diningViewModel?.getTotalPayableAmountWithoutTax()
            rowData?.amountTitle = "Sub Total"
            rowData?.amountValue = String(format: "%@%.2f", getCurrency(), subTotal?.total ?? 0)
            self.taxList = subTotal?.tax
        }
        entryCell.isFontSizeRequired = true
        entryCell.loadAmountEntries(with: rowData ?? AmountRowData())
        entryCell.entryDelegate = self
        return entryCell
    }
    
    
}
extension BBQBookingPaymentAmountDetailsTableViewCell: BBQAmountEntryTableViewCellDelegate{
    func contentDidChange(cell: BBQAmountEntryTableViewCell) {
        
    }
    
    func didClickOnDetails(cell: BBQAmountEntryTableViewCell) {
        amountCellDelegate?.didClickOnDetails(cell: cell)
    }
}
