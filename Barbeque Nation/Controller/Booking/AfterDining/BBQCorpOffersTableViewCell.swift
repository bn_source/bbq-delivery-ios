//
 //  Created by Ajith CP on 14/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQCorpOffersTableViewCell.swift
 //

import UIKit
import FirebaseAnalytics

protocol BBQCorpOffersTableViewCellDelegate : AnyObject {
    
    func didPressedCorpOffersAddRemoveButton(isAdded: Bool)
    
    func didPressedTermsAndConditions()
}

class BBQCorpOffersTableViewCell: UITableViewCell {
    
    weak var corpDelegate : BBQCorpOffersTableViewCellDelegate?

    @IBOutlet weak var appliedOffersLabel: UILabel!
    @IBOutlet weak var corpOffersTitleLabel: UILabel!
    
    @IBOutlet weak var emailVerifiedImgView: UIImageView!
    
    @IBOutlet weak var coluringView: UIView!
    
    @IBOutlet weak var addRemoveButton: UIButton!
    @IBOutlet weak var termsNConditionButton: UIButton!
    
    @IBOutlet weak var verifiedImageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var appliedOffersHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var termsNConditionsHeightConstraint: NSLayoutConstraint!
    
    var isCorpOffersAdded : Bool?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        AnalyticsHelper.shared.screenName(name: "CorporateOffersScreen")
        Analytics.logEvent("CorporateOffers", parameters: [
            "name": "CorporateOffers" as NSObject,
            "full_text": "CorporateOffers page" as NSObject
            ])
        coluringView.dropShadow()
        self.setupCell()
    }
    
    // MARK: Setup Methods
    
    func setupCell() {
        // Initial settings.
        self.isCorpOffersAdded = false
        if isCorpOffersAdded!{
            self.addRemoveButton.applyAddRemoveButtonTheme(title: kCouponsVouchersValueRemoveTitle, color: .theme)
        }else{
            self.addRemoveButton.applyAddRemoveButtonTheme(title: kCouponsVouchersValueAddTitle, color: .darkGreen)
        }
        self.hideShowCorporateValues(isHidden: !self.isCorpOffersAdded!)

    }
    
    // MARK: Public Methods
    
    func loadCorporateCellWith(for buffetItems: Int, offerData: CorporateOffer,
                               isCorpOffersApplied: Bool,
                               isCardEnabled: Bool) {
        
        self.contentView.layer.opacity = isCardEnabled ?  1.0 : 0.4
        self.isUserInteractionEnabled = isCardEnabled
        
        let offerText = "\(offerData.corporateName) of \(offerData.corporateDenomination) off on minimum of \(buffetItems) pax applied"
        
        self.appliedOffersLabel.text = offerText
        
        self.isCorpOffersAdded = isCorpOffersApplied
        
        
        if isCorpOffersAdded!{
            self.addRemoveButton.applyAddRemoveButtonTheme(title: kCouponsVouchersValueRemoveTitle, color: .theme)
        }else{
            self.addRemoveButton.applyAddRemoveButtonTheme(title: kCouponsVouchersValueAddTitle, color: .darkGreen)
        }
        
        self.hideShowCorporateValues(isHidden: !self.isCorpOffersAdded!)
        self.layoutIfNeeded()
        
    }
    
    // For after dining data update
    
    func loadAppliedCorpOffers(for buffetItems: Int,
                               offerData: VoucherCouponsData,
                               isCorpOffersApplied: Bool,
                               isCardEnabled: Bool) {
        self.contentView.layer.opacity = isCardEnabled ?  1.0 : 0.4
        self.isUserInteractionEnabled = isCardEnabled
        
        let offerText = "\(String(describing: offerData.title)) of \(String(describing: offerData.denomination)) off on minimum of \(buffetItems) pax applied"
        
        self.appliedOffersLabel.text = offerText
        
        self.isCorpOffersAdded = isCorpOffersApplied
        
        if isCorpOffersAdded!{
            self.addRemoveButton.applyAddRemoveButtonTheme(title: kCouponsVouchersValueRemoveTitle, color: .theme)
        }else{
            self.addRemoveButton.applyAddRemoveButtonTheme(title: kCouponsVouchersValueAddTitle, color: .darkGreen)
        }
        self.hideShowCorporateValues(isHidden: !self.isCorpOffersAdded!)
        
        
        
        if isCorpOffersApplied {
            self.addRemoveButton.isHidden = true
            self.termsNConditionButton.isHidden = true
            self.termsNConditionsHeightConstraint.constant = 0
            
        }
        
        self.layoutIfNeeded()
    }

    // MARK: IBAction Methods
    
    @IBAction func offersAddRemoveButtonPressed(_ sender: UIButton) {
        
        self.isCorpOffersAdded = !self.isCorpOffersAdded!
        self.corpDelegate?.didPressedCorpOffersAddRemoveButton(isAdded: self.isCorpOffersAdded!)
        self.isCorpOffersAdded = !self.isCorpOffersAdded!
    }
    
    @IBAction func termsAndConditionButtonPressed(_ sender: UIButton) {
        self.corpDelegate?.didPressedTermsAndConditions()
    }
    
    // MARK: Private Methods
    
    private func hideShowCorporateValues(isHidden: Bool) {
        if isHidden {
            
            self.termsNConditionButton.isHidden = true
            self.termsNConditionsHeightConstraint.constant = 0
            
            self.appliedOffersLabel.isHidden = true
            self.appliedOffersHeightConstraint.constant = 0
            
            self.emailVerifiedImgView.isHidden = true
            self.verifiedImageHeightConstraint.constant = 0
            
            //self.coluringView.backgroundColor = UIColor.clear
            
        } else {
            self.termsNConditionButton.isHidden = false
            self.termsNConditionsHeightConstraint.constant = 26
            
            self.appliedOffersLabel.isHidden = false
            self.appliedOffersHeightConstraint.constant = 17
            
            self.emailVerifiedImgView.isHidden = false
            self.verifiedImageHeightConstraint.constant = 16
            
          //  self.coluringView.backgroundColor = UIColor.hexStringToUIColor(hex: "F7F7F7")
            
        }
    }
}
