//
 //  Created by Ajith CP on 15/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQAfterBookingApplyVouchersTableViewCell.swift
 //

import UIKit
import FirebaseAnalytics

protocol BBQAfterBookingApplyVouchersTableViewCellDelegate: AnyObject {
    
    func didPressedVoucherAddRemoveButton(isAdd: Bool)
    
    func didPressedVouchersTermsAndConditions()
}

class BBQAfterBookingApplyVouchersTableViewCell: UITableViewCell {
    
    weak var voucherDelegate : BBQAfterBookingApplyVouchersTableViewCellDelegate?

    @IBOutlet weak var appliedTickImageView: UIImageView!
    @IBOutlet weak var voucherLogoImgView: UIImageView!
    
    @IBOutlet weak var vouchersTitleLabel: UILabel!
    @IBOutlet weak var voucherInfoLabel: UILabel!
    
    @IBOutlet weak var addRemoveButton: UIButton!
    @IBOutlet weak var termConditionButton: UIButton!
    
    @IBOutlet weak var applidTickImageHeighConstraint: NSLayoutConstraint!
    @IBOutlet weak var vouchersInfoLabelHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var termsConditionHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblAppliedAmount: UILabel!
    @IBOutlet weak var viewForContainer: UIView!
    
    static var initialAppliedVouchers : [VoucherCouponsData]?
    private var newVouchersApplied : [VoucherCouponsData]?
    
    internal var isVouchersAdded : Bool?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        AnalyticsHelper.shared.screenName(name: "VouchersApplyScreen")
        Analytics.logEvent("ApplyingVouchers", parameters: [
            "name": "ApplyVouchers" as NSObject,
            "full_text": "ApplyVouchers page" as NSObject
            ])

        viewForContainer.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.bottomCornersMask)
        viewForContainer.dropShadow()
        self.isVouchersAdded = false
    }

    // MARK: IBAction Methods
    
    @IBAction func voucherAddRemoveButtonPressed(_ sender: Any) {
        self.isVouchersAdded  = newVouchersApplied?.count ?? 0 > 0 ? false : true
        
        if let _ = BBQAfterBookingApplyVouchersTableViewCell.initialAppliedVouchers {
            self.isVouchersAdded = self.newVouchersApplied?.count ?? 0 > 0 ? false : true
            
        }
        self.voucherDelegate?.didPressedVoucherAddRemoveButton(isAdd: self.isVouchersAdded!)
    }
    
    @IBAction func termsNConditionsButtonPressed(_ sender: UIButton) {
        self.voucherDelegate?.didPressedVouchersTermsAndConditions()
    }
    
    
    // MARK: Public Methods
    
    func loadVouchersData(with applyVouchers: [VoucherCouponsData], didAdd: Bool, isCardEnabled: Bool) {
        
        self.contentView.layer.opacity = isCardEnabled ?  1.0 : 0.4
        self.isUserInteractionEnabled = isCardEnabled
        
        self.isVouchersAdded = didAdd
        self.newVouchersApplied = applyVouchers
        
        if didAdd{
            self.addRemoveButton.applyAddRemoveButtonTheme(title: kCouponsVouchersValueRemoveTitle, color: .theme)
        }else{
            self.addRemoveButton.applyAddRemoveButtonTheme(title: kCouponsVouchersValueAddTitle, color: .darkGreen)
        }
        
        var voucherTotalCount = 0
        
        if let initialCount = BBQAfterBookingApplyVouchersTableViewCell.initialAppliedVouchers?.count, initialCount > 0 { // Only for after dining.
            voucherTotalCount = initialCount
            self.isVouchersAdded = true
        }
        if didAdd, applyVouchers.count != 0  {
            voucherTotalCount += applyVouchers.count
        }
        var amount: Double = 0
//        var pax: Int = 0
        for voucher in applyVouchers {
//            if voucher.pax_applicable == 0{
                amount += voucher.denomination ?? 0.0
//            }else{
//                pax += voucher.pax_applicable
//            }
        }
        lblAppliedAmount.isHidden = true
//
//        if pax > 1{
//            lblAppliedAmount.textColor = .darkGreen
//            lblAppliedAmount.isHidden = false
//            lblAppliedAmount.text = String(format: "-%li Persons", pax)
//            if pax == 1{
//                lblAppliedAmount.text = "-1 Person"
//            }
//        }
        
        if amount > 0{
            var paxText = ""
//            if pax > 0{
//                paxText = lblAppliedAmount.text ?? "" + "\n"
//            }
            
            lblAppliedAmount.text = "\(paxText)-\(getCurrency())\(amount)"
            lblAppliedAmount.textColor = .darkGreen
            lblAppliedAmount.isHidden = false
        }
        
        let countInfoText = voucherTotalCount > 1 ? "happiness cards" : "happiness card"
        self.voucherInfoLabel.text = "\(voucherTotalCount) \(countInfoText) applied"
        
        self.hideShowVouchersValueLabel(isHidden: self.isVouchersAdded ?? false)
        self.layoutIfNeeded()
    }
    
    // MARK: Private Methods
    
    private func hideShowVouchersValueLabel(isHidden: Bool) {
        if isHidden {
            self.voucherInfoLabel.isHidden = false
            self.vouchersInfoLabelHeightConstraint.constant = 20
            
            self.termConditionButton.isHidden = false
            self.termsConditionHeightConstraint.constant = 26
            
            self.appliedTickImageView.isHidden = false
            self.applidTickImageHeighConstraint.constant = 16
            
            //self.contentView.backgroundColor = UIColor.hexStringToUIColor(hex: "F7F7F7")
            
        } else {
            self.voucherInfoLabel.isHidden = true
            self.vouchersInfoLabelHeightConstraint.constant = 0
            
            self.termConditionButton.isHidden = true
            self.termsConditionHeightConstraint.constant = 0
            
            self.appliedTickImageView.isHidden = true
            self.applidTickImageHeighConstraint.constant = 0
            
           // self.contentView.backgroundColor = .clear
        }
    }
    
}
