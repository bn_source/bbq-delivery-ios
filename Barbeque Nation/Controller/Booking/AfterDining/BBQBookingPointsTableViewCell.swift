//
 //  Created by Ajith CP on 14/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQBookingPointsTableViewCell.swift
 //

import UIKit

protocol BBQBookingPointsTableViewCellDelegate : AnyObject {
    
    func didPressedPointsAddRemoveButton(isAdd: Bool)
    
}

class BBQBookingPointsTableViewCell: UITableViewCell {
    
    weak var pointCellDelegate : BBQBookingPointsTableViewCellDelegate?
    private var isPointsAdded : Bool?

    @IBOutlet weak var pointsHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var pointsTitleLabel: UILabel!
    @IBOutlet weak var pointsValueLabel: UILabel!
    @IBOutlet weak var viewForContainer: UIView!
    
    @IBOutlet weak var addRemoveButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupCell()
        self.viewForContainer.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        self.viewForContainer.dropShadow()
    }
    
    // MARK: Setup Methods
    
    private func setupCell() {
        self.isPointsAdded = false
    }
    

    // MARK: IBAction Methods
    
    @IBAction func addRemoveButtonPressed(_ sender: UIButton) {
        self.isPointsAdded = !self.isPointsAdded!
        self.pointCellDelegate?.didPressedPointsAddRemoveButton(isAdd:  self.isPointsAdded!)
    }
    
    // MARK: Public Methods
    
    func loadLoyaltyPointCell(isAdded: Bool, pointModel: BBQPointsValueModel, isPointEnabled: Bool) {
        self.isPointsAdded = isAdded
        
        self.contentView.layer.opacity = isPointEnabled ?  1.0 : 0.4
        self.isUserInteractionEnabled = isPointEnabled
        
        
    
        if isAdded, let redAmount = pointModel.redeemAmount, let availablePoints = pointModel.availablePoints  {
            self.pointsValueLabel.text = "-\(getCurrency())\(redAmount)"
            self.pointsValueLabel.textColor = .darkGreen
            self.pointsTitleLabel.text = "[\(availablePoints) \(kPoints)]" //"\(kPoints) [\(availablePoints) \(kPoints)]"
        } else {
            let holdingPoints = BBQUserDefaults.sharedInstance.UserLoyaltyPoints
            self.pointsTitleLabel.text = "[\(holdingPoints) \(kPoints)]"//"\(kPoints) [\(holdingPoints) \(kPoints)]"
            if holdingPoints <= 0 && !BBQUserDefaults.sharedInstance.isGuestUser {
                self.isUserInteractionEnabled = false
                self.contentView.layer.opacity = 0.4 // Disabling points row if 0 points are available.
            }
        }
       
        if isAdded{
            self.addRemoveButton.applyAddRemoveButtonTheme(title: kMyCartRemoveCouponsText, color: .theme)
        }else{
            self.addRemoveButton.applyAddRemoveButtonTheme(title: kMyCartAddCouponsText, color: .darkGreen)
        }
        
        self.hideShowPointsValueLabel(isHidden: isAdded)
        self.layoutIfNeeded()

    }
    
    // Update method for after dining page
    
    func loadAppliedPointsCell(pointsModel: BBQPointsValueModel, didAppliedAlready: Bool,
                               didAddNewPoints: Bool, isPointEnabled: Bool) {
        
        self.isPointsAdded = didAddNewPoints
        
        self.contentView.layer.opacity = isPointEnabled ?  1.0 : 0.4
        self.isUserInteractionEnabled = isPointEnabled
        
        if (didAddNewPoints || didAppliedAlready), let redAmount = pointsModel.redeemAmount,
            let redPoints = pointsModel.redeemablePoints {
            self.pointsValueLabel.text = "-\(getCurrency())\(redAmount)"
            self.pointsValueLabel.textColor = .darkGreen
            self.pointsTitleLabel.text = "[\(redPoints) \(kPoints)]"
        } else {
            let holdingPoints = BBQUserDefaults.sharedInstance.UserLoyaltyPoints
            self.pointsTitleLabel.text = "[\(holdingPoints) \(kPoints)]"
        }
        
        let holdingPoints = BBQUserDefaults.sharedInstance.UserLoyaltyPoints
        if holdingPoints <= 0 || didAppliedAlready {
            self.isUserInteractionEnabled = false
            self.contentView.layer.opacity = 0.4
        }
        
        if didAppliedAlready || didAddNewPoints{
            self.addRemoveButton.applyAddRemoveButtonTheme(title: kMyCartRemoveCouponsText, color: .theme)
        }else{
            self.addRemoveButton.applyAddRemoveButtonTheme(title: kMyCartAddCouponsText, color: .darkGreen)
        }
        
        if didAppliedAlready {
           self.hideShowPointsValueLabel(isHidden: didAppliedAlready)
        } else {
            self.hideShowPointsValueLabel(isHidden: didAddNewPoints)
        }
        
        self.layoutIfNeeded()
        
    }
    
    // MARK: Private Methods
    
    private func hideShowPointsValueLabel(isHidden: Bool) {
        if isHidden {
            self.pointsValueLabel.isHidden = false
            self.pointsHeightConstraint.constant = 20
            //self.contentView.backgroundColor = UIColor.hexStringToUIColor(hex: "F7F7F7")
        } else {
            self.pointsValueLabel.isHidden = true
            self.pointsHeightConstraint.constant = 0
           // self.contentView.backgroundColor = .clear
        }
    }
}

