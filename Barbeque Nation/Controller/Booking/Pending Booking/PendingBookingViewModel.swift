//
 //  Created by Mahmadsakir on 20/10/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified PendingBookingModal.swift
 //

import Foundation
import UIKit
import ObjectMapper
import Alamofire

class PendingBookingViewModel{
    var bookingData: [String: Any] = [String: Any]()
    var pendingBookingData: PendingBookingData?
    var createBookingDataModel: CreateBookingDataModel?
    var createBookingBody: CreateBookingBody!
    
    init(bookingData: [String: Any]) {
        self.bookingData = bookingData
    }
    
    // MARK: - update Payment API
    
    /*
     {
         "razor_pay_order_id": "order_ICMB4rfkihpHYS",
         "res_adv_id": "RES_Adv_6",
         "razor_pay_payment_id":"pay_ICMBKQYgEmr1kQ",
         "booking_id":"",
         "razor_Pay_payment_status":"success",
         "customer_id":"8873391851",
         "payment_gateway":"Razor_Pay",
         "razorpay_signature":"43hjhkt5k2hk24j6lk26k6kl42j57klj6kl25jkl2"
     }
     */
    
    func checkPaymentStatus(paymentData: [String: Any]?, completion: @escaping (Bool, ReservationAdvPayment?)-> ()) {
        var params = [String: Any]()
        params["razor_pay_order_id"] = pendingBookingData?.data?.razor_pay_order_id
        params["razor_pay_payment_id"] = paymentData?["razorpay_payment_id"]
        params["razorpay_signature"] = paymentData?["razorpay_signature"]
        pendingBookingData?.data?.payment_id = paymentData?["razorpay_payment_id"] as? String ?? ""
        pendingBookingData?.data?.payment_signature = paymentData?["razorpay_signature"] as? String ?? ""
        updateAdvPaymentAPI(params: params, completion: completion)
    }
    
    func paymentUnknown(completion: @escaping (Bool, ReservationAdvPayment?)-> ()) {
        var params = [String: Any]()
        params["razor_pay_order_id"] = pendingBookingData?.data?.razor_pay_order_id
        updateAdvPaymentAPI(params: params, completion: completion)
    }
    
    func paymentUnknownFailed(completion: @escaping (Bool, ReservationAdvPayment?)-> ()) {
        var params = [String: Any]()
        params["razor_pay_order_id"] = pendingBookingData?.data?.razor_pay_order_id
        params["booking_id"] = "failed"
        updateAdvPaymentAPI(params: params, completion: completion)
    }
    
    func bookingCreated(booking_id: String, completion: @escaping (Bool, ReservationAdvPayment?)-> ()) {
        var params = [String: Any]()
        params["booking_id"] = booking_id
        params["razor_pay_order_id"] = pendingBookingData?.data?.razor_pay_order_id
        updateAdvPaymentAPI(params: params, completion: completion)
    }
    
    func bookingCreationFailed(completion: @escaping (Bool, ReservationAdvPayment?)-> ()) {
        var params = [String: Any]()
        params["booking_id"] = "failed"
        params["razor_pay_order_id"] = pendingBookingData?.data?.razor_pay_order_id
        params["razor_pay_payment_id"] = pendingBookingData?.data?.payment_id
        updateAdvPaymentAPI(params: params, completion: completion)
    }
    
    private func updateAdvPaymentAPI(params: [String: Any], completion: @escaping (Bool, ReservationAdvPayment?)-> ()){
        UIUtils.showLoader()
        BBQBookingManager.updateReservationAdvPayment(params: params) { model, result in
            if result, model?.message_type?.lowercased() == "success"{
                completion(true, model)
            }
            completion(false, model)
            UIUtils.hideLoader()
        }
    }
}
extension PendingBookingViewModel{
    class PendingBookingData: Mappable{
        var message: String?
        var message_type: String?
        var data: PendingBooking?
        
        required init?(map: Map) {}
        
        func mapping(map: Map) {
            message <- map["message"]
            message_type <- map["message_type"]
            data <- map["data"]
        }
        
        class PendingBooking: Mappable{
            var advance_type: BookingAdvanceType = .NA
            var advance_amount:Double = 0
            var id: Int64 = 0
            var res_adv_id = ""
            var razor_pay_order_id = ""
            var payment_id: String = ""
            var payment_gateway = "RAZOR_PAY"
            var payment_signature = ""
            
            
            enum BookingAdvanceType: String{
                case Full = "Full"
                case Partial = "Partial"
                case NA = "NA"
            }
            
            required init?(map: Map) {}
            
            func mapping(map: Map) {
                id <- map["id"]
                res_adv_id <- map["res_adv_id"]
                razor_pay_order_id <- map["razor_pay_order_id"]
                payment_id <- map["payment_id"]
                advance_type <- map["advance_type"]
                advance_amount <- map["advance_amount"]
            }
        }
    }
}

extension PendingBookingViewModel{
    class ReservationAdvPayment: Mappable{
        var message: String?
        var message_type: String?
        var data: PaymentData?
        
        required init?(map: Map) {}
        
        func mapping(map: Map) {
            message <- map["message"]
            message_type <- map["message_type"]
            data <- map["data"]
        }
        
        class PaymentData: Mappable{
            required init?(map: Map) {}
            
            var razor_pay_order_id = ""
            var razor_pay_payment_id = ""
            var booking_id = ""
            var razor_pay_payment_status = ""
            var payment_gateway = ""
            var payment_refund_id = ""
            var payment_refund_amount:Double = 0
            
            func mapping(map: Map) {
                razor_pay_order_id <- map["razor_pay_order_id"]
                booking_id <- map["booking_id"]
                razor_pay_payment_id <- map["razor_pay_payment_id"]
                razor_pay_payment_status <- map["razor_pay_payment_status"]
                payment_gateway <- map["payment_gateway"]
                payment_refund_id <- map["payment_refund_id"]
                payment_refund_amount <- map["payment_refund_amount"]
            }
        }
    }
}
