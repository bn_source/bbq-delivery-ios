//
 //  Created by Mahmadsakir on 18/10/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified PendingBookingVC.swift
 //

import UIKit
import WebKit
import Razorpay
//import Razorpay

class PendingBookingVC: UIViewController {
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var btnPayNow: UIButton!
    @IBOutlet weak var btnTest: UIButton!
    @IBOutlet weak var viewForPaymentLoading: UIView!
    @IBOutlet weak var imgPaymentLoading: UIImageView!
    
    var viewModel: PendingBookingViewModel!
//    var razorpay: RazorpayCheckout!
    var buffetsViewModel: BBQBookingBuffetsViewModel?
    var selectedDateType: BBQBookingDateType = .Today
    var selectedDateString : String?
    var selectedTimeString : String?
    var selectedformattedDate: String = ""
    var superVC: UIViewController?
    var no_of_pax: Int = 0
    var payable_amt: Double = 0
    
    var paymentFetchMethods: [AnyHashable:Any]?
    var razorpay: RazorpayCheckout!
    var razorpay_id: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        AnalyticsHelper.shared.triggerEvent(type: .AD01)
        //razorpay = RazorpayCheckout.initWithKey("rzp_live_woEpCznOt0mINU", andDelegateWithData: self)
        
        lblAmount.text = String(format: "%@%.02f", getCurrency(), viewModel.pendingBookingData?.data?.advance_amount ?? 0)
        //BBQ_logo_welcome
        //icon_error_theme
        initiateThePaymentData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        imgPaymentLoading.stopAnimatingGif()
    }
    
    @IBAction func onClickBtnBack(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .AD02)
        let alertVC = UIAlertController(title: nil, message: "Are you sure you don’t want to complete your reservation? You will be taken back to the home page, where you can do the reservation again.", preferredStyle: .actionSheet)
        
        alertVC.addAction(UIAlertAction(title: "Continue With Reservation", style: .cancel, handler: {action in
            AnalyticsHelper.shared.triggerEvent(type: .AD02B)
        }))
        alertVC.addAction(UIAlertAction(title: "Cancel Reservation", style: .default, handler: { action in
            AnalyticsHelper.shared.triggerEvent(type: .AD02A)
            if let superVC = self.superVC {
                self.navigationController?.popToViewController(superVC, animated: true)
            }else{
                self.navigationController?.popViewController(animated: true)
            }
        }))
        self.present(alertVC, animated: true, completion: nil)
    }
    
    @IBAction func onClickBtnPayNow(_ sender: Any) {
        showPaymentForm()
    }
    @IBAction func onClickBtnTest(_ sender: Any) {
//        if btnTest.isSelected{
//            self.navigationController?.popViewController(animated: true)
//            return
//        }
        
    }
    
    func createTableReservation(){
        AnalyticsHelper.shared.triggerEvent(type: .AD04)
        self.buffetsViewModel?.createBooking(createBookingData: viewModel.createBookingBody) { (isSuccess, message) in
            UIUtils.showLoader()
            if isSuccess {
                AnalyticsHelper.shared.triggerEvent(type: .AD04A)
                AnalyticsHelper.shared.triggerEvent(type: .R14A)
                AnalyticsHelper.shared.triggerEvent(type: .Reservation_Status, properties: [.Reservation_booked_Status : AnalyticsHelper.MoEngageAttrValue.Success.rawValue, .Location: BBQUserDefaults.sharedInstance.CurrentSelectedLocation, .Booking_id: self.buffetsViewModel?.bookingId ?? ""])
                AnalyticsHelper.shared.triggerEvent(type: .AD05)
                self.imgLogo.image = UIImage(named: "icon_success_green")
                self.lblHeading.text = "Table Reserved"
                self.lblMessage.text = ""
                self.buffetsViewModel?.updateBooking(bookingId: self.buffetsViewModel?.bookingId ?? "", paymentOrderId: self.viewModel.pendingBookingData?.data?.razor_pay_order_id ?? "", paymentId: self.viewModel.pendingBookingData?.data?.payment_id ?? "", paymentGatewayName: self.viewModel.pendingBookingData?.data?.payment_gateway ?? "", paymentSignature: self.viewModel.pendingBookingData?.data?.payment_signature ?? "", completion: { isSuccess in
                    self.viewModel.bookingCreated(booking_id: self.buffetsViewModel?.bookingId ?? "Unknown") { isSuccess, response in
                        if isSuccess{
                            AnalyticsHelper.shared.triggerEvent(type: .AD05A)
                        }else{
                            AnalyticsHelper.shared.triggerEvent(type: .AD05B)
                        }
                        self.launchConfirmBookingScreen()
                        UIUtils.hideLoader()
                    }
                })
            }else{
                AnalyticsHelper.shared.triggerEvent(type: .AD04B)
                AnalyticsHelper.shared.triggerEvent(type: .R14B)
                AnalyticsHelper.shared.triggerEvent(type: .Reservation_Status, properties: [.Reservation_booked_Status : AnalyticsHelper.MoEngageAttrValue.Failed.rawValue, .Location: BBQUserDefaults.sharedInstance.CurrentSelectedLocation, .Error: self.viewModel.createBookingDataModel?.fieldErrors?.first?.message ?? ""])
                self.lblHeading.text = "Reservation failed"
                self.lblMessage.text = ""
                self.imgLogo.image = UIImage(named: "icon_error_theme")
                self.viewModel.bookingCreationFailed { isSuccess, response in
                    if isSuccess, let response = response{
                        self.launchPaymentFailedScreen(data: response)
                    }
                    UIUtils.hideLoader()
                }
            }
        }
    }
    
    
    
    @objc internal func launchConfirmBookingScreen() {
        let storyBoard : UIStoryboard = UIStoryboard(name: Constants.OrderConfirm.orderConfirmStoryBoard, bundle:nil)
        let orderConfirmVC = storyBoard.instantiateViewController(withIdentifier: "BBQBookingConfirmationVC") as! BBQBookingConfirmationVC
        orderConfirmVC.bookingID = self.buffetsViewModel?.bookingId ?? ""
        orderConfirmVC.superVC = superVC
        self.navigationController?.pushViewController(orderConfirmVC, animated: true)
    }
    
    
    internal func launchPaymentFailedScreen(data: PendingBookingViewModel.ReservationAdvPayment){
        let refundBookingPaymentVC = UIStoryboard.refundBookingPaymentVC()
        refundBookingPaymentVC.refund_id = data.data?.payment_refund_id
        refundBookingPaymentVC.refund_amount = String(format: "%.02f", data.data?.payment_refund_amount ?? 0)
        refundBookingPaymentVC.selectedTimeString = selectedTimeString
        refundBookingPaymentVC.delegate = self
        refundBookingPaymentVC.superVC = superVC
        self.navigationController?.pushViewController(refundBookingPaymentVC, animated: true)
    }
    
    internal func getReservationDateForSelectedDate(type: BBQBookingDateType) -> String {
        var reservationDate = ""
        switch type {
        case .Today:
            reservationDate = BBQBookingUtilies.shared.getTodayDateForAPI()
        case .Tomorrow:
            reservationDate = BBQBookingUtilies.shared.getTomorrowDateForAPI()
        default: //Pick a date Pressed
            reservationDate = self.selectedformattedDate
        }
        return reservationDate
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PendingBookingVC: RefundBookingPaymentDelegate{
    func dismissRefund(viewController: RefundBookingPaymentVC) {
        if let superVC = superVC{
            self.navigationController?.popToViewController(superVC, animated: true)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
}


extension PendingBookingVC: BBQCustomPaymentViewControllerDelegate, RazorpayPaymentCompletionProtocol{
    // MARK: - BBQRazorpayControllerProtocol Methods
    internal func showPaymentForm() {
        AnalyticsHelper.shared.triggerEvent(type: .AD03)
        let paymenVc = UIStoryboard.loadPayment()
        let paymentData = getWebViewPaymentId(paymentVC: paymenVc, order_id: nil)
        paymenVc.viewModel = CustomPaymentViewModel.init(razorPay: self.razorpay,razorpayOptions: paymentData.options ?? [:], delegate: paymenVc,webView: paymentData.webView, response: paymentFetchMethods)
        paymenVc.delegate = self
        self.navigationController?.pushViewController(paymenVc, animated: true)
    }
    
    private func initiateThePaymentData() {
        if razorpay == nil{
            let webView = WKWebView.init(frame: self.view.frame)
            let razorPayAppKey = Bundle.main.object(forInfoDictionaryKey: Constants.Payment.razorpayPublicKey)
            self.razorpay = Razorpay.initWithKey(razorPayAppKey as! String, andDelegate: self, withPaymentWebView: webView)
        }
        
        self.razorpay.getPaymentMethods(withOptions: nil) { (response) in
            self.paymentFetchMethods = response
        } andFailureCallback: { (error) in
            print("Error fetching payment methods:\(error)")
        }
    }
    private func getWebViewPaymentId(paymentVC: BBQCustomPaymentViewController, order_id: String?) -> (webView: WKWebView, options: [String: Any]?){
        let amount = Int(round((viewModel.pendingBookingData?.data?.advance_amount ?? 0) * 100))
        let options: [String:Any] = [
            "amount": amount,
            "currency": "INR",
            "description": "Final Payment",
            "order_id": order_id as Any,
            //            "name": paymentModel.name ?? "",
            "email" : SharedProfileInfo.shared.profileData?.email  ?? BBQUserDefaults.sharedInstance.customPaymentEmail,
            "contact": SharedProfileInfo.shared.profileData?.mobileNumber as Any,
            //Comment me for release
            //            "contact": 9844019983
            
        ]
        
        let webView = WKWebView.init(frame: self.view.frame)
        let razorPayAppKey = Bundle.main.object(forInfoDictionaryKey: Constants.Payment.razorpayPublicKey)
        self.razorpay = Razorpay.initWithKey(razorPayAppKey as! String, andDelegate: paymentVC, withPaymentWebView: webView)
        return (webView, options)
    }
    
    func onPaymentSuccess(_ payment_id: String, andData response: [AnyHashable : Any]) {
        self.navigationController?.popToViewController(self, animated: true)
        print("payment success")
        AnalyticsHelper.shared.triggerEvent(type: .AD03A)
        imgPaymentLoading.loadGif(name: "loader_food")
        viewForPaymentLoading.isHidden = false
        imgLogo.image = UIImage(named: "icon_success_green")
        lblHeading.text = "Payment Successful"
        lblMessage.text = "Table Reservation is in-progress..."
        if let response = response as? [String: Any]{
            UIUtils.showLoader()
            viewModel.checkPaymentStatus(paymentData: response) { isSuccess, responseData in
                if isSuccess{
                    self.createTableReservation()
                }
            }
        }
    }
    
    func onPaymentError(_ code: Int32, description str: String, andData response: [AnyHashable : Any]) {
        self.navigationController?.popToViewController(self, animated: true)
        print("payment error: \(code) \(str)")
        imgLogo.image = UIImage(named: "icon_error_theme")
        lblHeading.text = "Payment Unsuccessful,\nPlease try Again!"
        btnPayNow.setTitle("Retry", for: .normal)
        AnalyticsHelper.shared.triggerEvent(type: .AD03B)
    }
    
    func createRazorPayPaymentId(paymentVC: BBQCustomPaymentViewController, completion: @escaping (WKWebView, [String : Any]?, String?, Razorpay?, String?) -> Void) {
        if let razorpay_id = razorpay_id{
            let data = self.getWebViewPaymentId(paymentVC: paymentVC, order_id: razorpay_id)
            completion(data.webView, data.options, razorpay_id, self.razorpay, "")
            self.razorpay_id = nil
        }
        self.buffetsViewModel?.checkReservationRule(createBookingData: viewModel.createBookingBody, no_of_pax: no_of_pax, payable_amt: payable_amt) { isSuccess, response in
            BBQActivityIndicator.hideIndicator(from: self.view)
            if isSuccess, let response = response{
                if let advance_amount = response.data?.advance_amount, advance_amount > 0{
                    self.viewModel.pendingBookingData = response
                    let data = self.getWebViewPaymentId(paymentVC: paymentVC, order_id: response.data?.razor_pay_order_id ?? "")
                    completion(data.webView, data.options, response.data?.razor_pay_order_id ?? "", self.razorpay, "")
                }else{
                    self.navigationController?.popToViewController(self, animated: true)
                    self.createTableReservation()
                }
            }
        }
    }
}
