//
 //  Created by Sakir on 18/04/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified ReservationMoreInfoVC.swift
 //

import UIKit

class ReservationMoreInfoVC: UIViewController {
    //MARK: - Properties and variables
    var delegate: ReservationMoreInfoDelegate?
    
    
    //MARK: - View Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        view.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
    }

    //MARK: - Button Action Methods
    @IBAction func onClickBtnCabBooking(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate?.clickedCabBooking(viewController: self)
        }
        
    }
    
    @IBAction func onClickBtnDirections(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate?.clickedDirections(viewController: self)
        }
    }
    
    @IBAction func onClickBtnCallOutlet(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate?.clickedCallOutlet(viewController: self)
        }
    }
    
    @IBAction func onClickBtnShare(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate?.clickedShare(viewController: self)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

protocol ReservationMoreInfoDelegate{
    func clickedCabBooking(viewController: ReservationMoreInfoVC)
    func clickedDirections(viewController: ReservationMoreInfoVC)
    func clickedCallOutlet(viewController: ReservationMoreInfoVC)
    func clickedShare(viewController: ReservationMoreInfoVC)
}
