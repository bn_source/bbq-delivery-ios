//
//  Created by Mahmadsakir on 22/02/22
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
//  All rights reserved.
//  Last modified ReservationDetailsVC.swift
//

import UIKit
import CoreMedia
import PopMenu
import AMPopTip
import WebKit
import Razorpay



protocol BookingCancellationlDelegate: AnyObject {
    func bookingCancelled(bookingId: String)
 }


class ReservationDetailsVC: BaseViewController {
    //MARK: - Outlet Collections
    @IBOutlet weak var lblOutletName: UILabel!
    @IBOutlet weak var lblReservationId: UILabel!
    @IBOutlet weak var lblReservationDate: UILabel!
    @IBOutlet weak var lblReservationTime: UILabel!
    @IBOutlet weak var tblPackDetails: UITableView!
    @IBOutlet weak var stackViewForCoupons: UIStackView!
    @IBOutlet weak var lblCouponsTitle: UILabel!
    @IBOutlet weak var btnCouponValue: UIButton!
    @IBOutlet weak var stackViewForSmiles: UIStackView!
    @IBOutlet weak var lblSmilePoints: UILabel!
    @IBOutlet weak var stackViewForSubTotal: UIStackView!
    @IBOutlet weak var lblForSubTotal: UILabel!
    @IBOutlet weak var btnTaxes: UIButton!
    @IBOutlet weak var stackViewForHappinessCard: UIStackView!
    @IBOutlet weak var btnHappinessCard: UIButton!
    @IBOutlet weak var stackViewForAdvancePayment: UIStackView!
    @IBOutlet weak var lblAdvancePayment: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var heightForTableView: NSLayoutConstraint!
    @IBOutlet weak var viewForCalculation: UIView!
    @IBOutlet weak var viewForPackDetails: UIView!
    @IBOutlet weak var viewForReservationDate: UIView!
    @IBOutlet weak var viewForOutletInfo: UIView!
    @IBOutlet weak var btnPayNow: UIButton!
    @IBOutlet weak var stackViewForCancelBooking: UIStackView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var viewForStatus: UIView!
    @IBOutlet weak var stackViewForTaxes: UIStackView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var btnViewBill: UIButton!
    @IBOutlet weak var lblAdultCount: UILabel!
    @IBOutlet weak var lblChildCount: UILabel!
    @IBOutlet weak var stackViewForMainTax: UIStackView!
    @IBOutlet weak var stackVIewForBreakUp: UIStackView!
    
    var cancelDelagate : BookingCancellationlDelegate?
    //MARK: - Properties
    var bookingID = ""
    var bottomSheetController: BottomSheetController?
    var isAdvanceAmountPaid = false
    var textToShare:String?
    var dateTimePlace:String?
    var popTip = PopTip()
    
    var paymentFetchMethods: [AnyHashable:Any]?
    var razorpay: RazorpayCheckout!
    
    lazy var bookingConfirmationViewModel: BookingConfirmationViewModel = {
        let viewModel = BookingConfirmationViewModel(bookingID: bookingID)
        return viewModel
    }()
    
    lazy var cartViewModel : CartViewModel = {
        let viewModel = CartViewModel()
        return viewModel
    }()
    
    lazy private var afterDiningViewModel : BBQAfterDiningControllerViewModel = {
        let afterDiningVM = BBQAfterDiningControllerViewModel()
        return afterDiningVM
    } ()
    
    lazy private var paymentControllerViewModel : MakePaymentViewModel = {
        let paymentVM = MakePaymentViewModel()
        return paymentVM
    } ()
    
    //MARK: - View Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        AnalyticsHelper.shared.triggerEvent(type: .RD01)
        setViewData()
        viewForStatus.isHidden = true
        initiateThePaymentData()
        // Do any additional setup after loading the view.
    }
    
    
    func setViewData(){
        tblPackDetails.register(UINib(nibName: "ReservationDetailsCell", bundle: nil), forCellReuseIdentifier: "ReservationDetailsCell")
        viewForPackDetails.setShadow(color: .lightGray, opacity: 0.2, offSet: .zero, radius: 10.0, scale: true)
        viewForCalculation.setShadow(color: .lightGray, opacity: 0.2, offSet: .zero, radius: 10.0, scale: true)
        viewForReservationDate.setShadow(color: .lightGray, opacity: 0.2, offSet: .zero, radius: 10.0, scale: true)
        viewForOutletInfo.setShadow(color: .lightGray, opacity: 0.2, offSet: .zero, radius: 10.0, scale: true)
        viewForStatus.setCornerRadius(10.0)
        tblPackDetails.setCornerRadius(10.0)
        popTip.bubbleColor = .white
        popTip.cornerRadius = 5.0
        popTip.shouldShowMask = true
        btnViewBill.roundCorners(cornerRadius: 5.0, borderColor: .theme, borderWidth: 1.0)
        getBookingDetails()
        
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: self.view.frame.size.height - 278.0)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        self.bottomSheetController = bottomSheetController
    }
    
    //MARK: - Popup Menu view
    func showInfoPopupView() {
        
    }
    
    //MARK: - Button Action
    @IBAction func onClickBtnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickBtnCoupons(_ sender: Any) {
        
    }
    @IBAction func onClickBtnTaxes(_ sender: Any) {
        if let model = bookingConfirmationViewModel.model{
            let popupVC = BillDetailsPopVC(nibName: "BillDetailsPopVC", bundle: nil)
            popupVC.modalPresentationStyle = UIModalPresentationStyle .popover
            popupVC.preferredContentSize = CGSize(width: 170, height: 130)
            popupVC.strTitle = kDiningTaxServiceCharge
            popupVC.taxBreakUp = model.getTaxes()
            popupVC.view.frame.size.width = 200 > self.view.frame.size.width ? self.view.frame.size.width : 200
            popupVC.view.frame.size.height = CGFloat(60 + (model.getTaxes().count * 26))
            //            var originFrame = cell.valueLabel.frame
            //            originFrame.size.width = 40
            //            originFrame.origin.x = originFrame.origin.x + 35
            var frame = stackViewForTaxes.convert(btnTaxes.frame, to: self.view)
            frame.origin.x += 102
            popTip.show(customView: popupVC.view, direction: .auto, in: self.view, from: frame)
        }
    }
    @IBAction func onClickBtnHappinessCard(_ sender: Any) {
        
    }
    @IBAction func onClickBtnPayNow(_ sender: Any) {
        if let bookingId = bookingConfirmationViewModel.model?.bookingID{
            if bookingConfirmationViewModel.model?.bookingStatus == "PRE_RECEIPT"{
                let afterDiningPaymentVC = AfterDiningPaymentVC(nibName: "AfterDiningPaymentVC", bundle: nil)
                afterDiningPaymentVC.bookingID = bookingId
                afterDiningPaymentVC.delegate = self
                self.navigationController?.pushViewController(afterDiningPaymentVC, animated: true)
//                proceedToPaymentPressed(bookingID: bookingId,
//                                        paymentAmount: bookingConfirmationViewModel.model?.billTotal ?? 0,
//                                        paymentRemarks: "Bill settlment", controller: nil)
            }else{
                proceedToPaymentPressed(bookingID: bookingId,
                                        paymentAmount: bookingConfirmationViewModel.model?.billTotal ?? 0,
                                        paymentRemarks: "Advance amount to be paid", controller: nil)
            }
            
        }
    }
    @IBAction func onClickBtnReschedule(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .RD03)
        rescheduleTapped(sender)
    }
    @IBAction func onClickBtnCancel(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .RD04)
        bookingHistoryButtonsPressed(type: .Cancel)
    }
    @IBAction func onClickBtnInfo(_ sender: UIButton) {
        AnalyticsHelper.shared.triggerEvent(type: .RD02)
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: self.view.frame.size.height - 100)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        let reservationMoreInfoVC = ReservationMoreInfoVC(nibName: "ReservationMoreInfoVC", bundle: nil)
        reservationMoreInfoVC.delegate = self
        bottomSheetController.present(reservationMoreInfoVC, on: self, viewHeight: 380)
    }
    @IBAction func onClickBtnViewBill(_ sender: Any) {
        if let invoiceLink = bookingConfirmationViewModel.model?.billLink{
            let webView = UIStoryboard.loadCommonWebView()
            webView.URLSring = invoiceLink
            webView.strTitle = "Invoice"
            webView.orderID = bookingConfirmationViewModel.model?.bookingID ?? "Invoice"
            self.present(webView, animated: true, completion: nil)
        }
    }
    
    //MARK: - Web Services
    func getBookingDetails(){
        
        bookingConfirmationViewModel.getBookingData { model, error in
            if let model = model {
                self.viewForStatus.isHidden = false
                let status = model.getStatus()
                self.viewForStatus.backgroundColor = status.1.withAlphaComponent(0.15)
                self.lblStatus.textColor = status.1
                self.lblStatus.text = status.0
                
                
                self.btnPayNow.isHidden = !(model.showPayment ?? true)
                self.stackViewForCancelBooking.isHidden = status.2 == 0 ? false : true
                if status.2 == 3 || status.2 == 4{
                    self.btnPayNow.isHidden = true
                }
                
                self.lblOutletName.text = model.branchName
                self.lblReservationId.text = String(format: "#%@", model.bookingID ?? "")
                self.lblReservationDate.text = model.reservationDate
                self.lblReservationTime.text = model.reservationTime
                self.tblPackDetails.reloadData {
                    self.heightForTableView.constant = self.tblPackDetails.contentSize.height
                }
                
                self.stackViewForSmiles.isHidden = (model.loyaltyPoints ?? 0) <= 0
                self.lblSmilePoints.text = String(format: "-%@%li", getCurrency(), model.loyaltyPoints ?? 0)
                
                
                self.stackViewForCoupons.isHidden = (self.bookingConfirmationViewModel.model?.getDiscountsAmount() ?? 0) <= 0
                self.btnCouponValue.setTitle(String(format: "-%@%.0f", getCurrency(), model.getDiscountsAmount() ), for: .normal)
                
                self.stackViewForHappinessCard.isHidden = self.bookingConfirmationViewModel.vouchers.count <= 0
                
                self.btnHappinessCard.setTitle(String(format: "-%@%.02f", getCurrency(), self.bookingConfirmationViewModel.voucher_amount), for: .normal)
                self.lblForSubTotal.text = String(format: "%@%.02f", getCurrency(), model.billWithoutTax ?? 0)
                
                let string =  NSAttributedString(string: String(format: "%@%.02f", getCurrency(), self.bookingConfirmationViewModel.tax_amount), attributes: [.underlineStyle : NSUnderlineStyle.single.rawValue])
                self.btnTaxes.setAttributedTitle(string, for: .normal)
                
                self.lblTotalAmount.text = self.bookingConfirmationViewModel.getTotalPayableAmount()//String(format: "%@%.02f", getCurrency(), self.bookingConfirmationViewModel.sub_total + self.bookingConfirmationViewModel.tax_amount - self.bookingConfirmationViewModel.voucher_amount - (model.payments?.amountPaid ?? 0.0))
                let paidAmount = model.getPaymentAmountInDetailScreen()
                self.stackViewForAdvancePayment.isHidden = paidAmount <= 0
                self.lblAdvancePayment.text = String(format: "-%@%.02f", getCurrency(), paidAmount)
                self.lblChildCount.text = "\(model.childPax ?? 0)"
                self.lblAdultCount.text = "\(model.adultPax ?? 0)"
                
                //self.stackVIewForBreakUp.isHidden = !(model.getTaxes().count > 0)
                if let link = model.billLink, link.count > 1{
                    self.btnViewBill.isHidden = false
                }else{
                    self.btnViewBill.isHidden = true
                }
                if self.bookingConfirmationViewModel.isBookingSettled(){
                    self.viewForStatus.backgroundColor = UIColor(red: 5.0/255.0, green: 166.0/255.0, blue: 96.0/255.0, alpha: 1.0).withAlphaComponent(0.15)
                    self.lblStatus.textColor = UIColor(red: 5.0/255.0, green: 166.0/255.0, blue: 96.0/255.0, alpha: 1.0)
                    self.lblStatus.text = "Paid"
                    self.lblTotalAmount.text = "\(getCurrency())0"
                }
                
                if (model.bookingStatus ?? "") == "PRE_RECEIPT"{
                    self.btnPayNow.setTitle("Proceed", for: .normal)
                }else{
                    self.btnPayNow.setTitle("Pay Now", for: .normal)
                }
            }
        }
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension ReservationDetailsVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bookingConfirmationViewModel.model?.bookingDetails?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReservationDetailsCell", for: indexPath) as! ReservationDetailsCell
        if (bookingConfirmationViewModel.model?.bookingDetails?.count ?? 0) > indexPath.row, let model = bookingConfirmationViewModel.model?.bookingDetails?[indexPath.row]{
            cell.setCellData(model: model)
            if indexPath.row == (bookingConfirmationViewModel.model?.bookingDetails?.count ?? 0) - 1{
                cell.lblLightLine.isHidden = true
            }else{
                cell.lblLightLine.isHidden = false
            }
        }
        return cell
    }
}


extension ReservationDetailsVC: BBQRazorpayControllerProtocol, BBQAfterDiningDelegate{
    func onPaymentSuceess() {
        getBookingDetails()
    }
    
    //MARK: Payment Methods
    func proceedToPaymentPressed(bookingID: String, paymentAmount: Double,
                                 paymentRemarks: String, controller: UIViewController?) {
        guard let booking = bookingConfirmationViewModel.model  else {
            ToastHandler.showToastWithMessage(message: "Please try again!")
            return
        }
        if booking.bookingStatus == "PRE_RECEIPT"{
            AnalyticsHelper.shared.triggerEvent(type: .RD02E)
            showAfterDiningScreen(bookingId: bookingID)
        }else{
            AnalyticsHelper.shared.triggerEvent(type: .RD02D)
            AnalyticsHelper.shared.triggerEvent(type: .RD05)
            // MARK: - Custom Payment UI Code added v5.3.5
            showPaymentForm()
        }
    }
    
    private func showAfterDiningScreen(bookingId:String) {
        let diningVC = UIStoryboard.loadAfterDining()
        diningVC.bookingIDString = bookingId
        diningVC.delegate = self
//        diningVC.view.layoutIfNeeded()
//        let proposedHeight =  UIScreen.main.bounds.height -  (500)
//        let configuration = BottomSheetConfiguration(animationDuration: 0.4,
//                                                     minimumTopSpacing: CGFloat(proposedHeight))
//        let bottomSheetController = BottomSheetController(configuration: configuration)
//        diningVC.diningBottomSheet = bottomSheetController
        self.navigationController?.pushViewController(diningVC, animated: true)
        //        bottomSheetController.present(diningVC, on: self)
    }
    
    func cancelPressed() {
        
    }
    
    // MARK: - BBQRazorpayControllerProtocol Methods
    internal func showPaymentForm() {
        
        let paymenVc = UIStoryboard.loadPayment()
        let paymentData = getWebViewPaymentId(paymentVC: paymenVc, order_id: nil)
        paymenVc.viewModel = CustomPaymentViewModel.init(razorPay: self.razorpay,razorpayOptions: paymentData.options ?? [:], delegate: paymenVc,webView: paymentData.webView, response: paymentFetchMethods)
        paymenVc.delegate = self
        self.navigationController?.pushViewController(paymenVc, animated: true)
    }
    private func initiateThePaymentData() {
        if razorpay == nil{
            let webView = WKWebView.init(frame: self.view.frame)
            let razorPayAppKey = Bundle.main.object(forInfoDictionaryKey: Constants.Payment.razorpayPublicKey)
            self.razorpay = Razorpay.initWithKey(razorPayAppKey as! String, andDelegate: self, withPaymentWebView: webView)
        }
        
        self.razorpay.getPaymentMethods(withOptions: nil) { (response) in
            self.paymentFetchMethods = response
        } andFailureCallback: { (error) in
            print("Error fetching payment methods:\(error)")
        }
    }
    private func getWebViewPaymentId(paymentVC: BBQCustomPaymentViewController, order_id: String?) -> (webView: WKWebView, options: [String: Any]?){
        let amount = Int(round((bookingConfirmationViewModel.model?.billTotal ?? 0) * 100.0))
        let options: [String:Any] = [
            "amount": amount,
            "currency": "INR",
            "description": "Advance amount to be paid",
            "order_id": order_id as Any,
            //            "name": paymentModel.name ?? "",
            "email" : SharedProfileInfo.shared.profileData?.email  ?? BBQUserDefaults.sharedInstance.customPaymentEmail,
            "contact": SharedProfileInfo.shared.profileData?.mobileNumber as Any,
            //Comment me for release
            //            "contact": 9844019983
            
        ]
        
        let webView = WKWebView.init(frame: self.view.frame)
        let razorPayAppKey = Bundle.main.object(forInfoDictionaryKey: Constants.Payment.razorpayPublicKey)
        self.razorpay = Razorpay.initWithKey(razorPayAppKey as! String, andDelegate: paymentVC, withPaymentWebView: webView)
        return (webView, options)
    }
    func onRazorpayPaymentError(_ code: Int32, description str: String) {
        AnalyticsHelper.shared.triggerEvent(type: .RD05B)
        switch code {
        case 0,1,3:
            PopupHandler.showSingleButtonsPopup(title: kFailedTitle,
                                                message: str,
                                                on: self, firstButtonTitle: kOkButtonTitle) { }
        case 2:
            self.showTransactionCancelled()
            
        default:
            print("");
        }
    }
    
    func showTransactionCancelled() {
        PopupHandler.showSingleButtonsPopup(title: kMyCartTransactionCancelTitle,
                                            message: kMyCartTransactionCancelMessage,
                                            on: self,
                                            firstButtonTitle: kButtonOkay) { }
    }
    
    func onRazorpayPaymentSuccess(_ paymentData: BBQPaymentDataModel) {
        //let razorpayModel = paymentData
        AnalyticsHelper.shared.triggerEvent(type: .RD05A)
        AnalyticsHelper.shared.triggerEvent(type: .RD06)
        let paymentGateway = Constants.BBQBookingStatus.razorPayment
        
        BBQActivityIndicator.showIndicator(view: self.view)
        self.bookingConfirmationViewModel.updateBooking(bookingId: bookingID, paymentOrderId: paymentData.razorpayOrderID ?? "", paymentId: paymentData.paymentID ?? "", paymentGatewayName: paymentGateway, paymentSignature: paymentData.razorpaySignature ?? "") { (isSuccess) in
            BBQActivityIndicator.hideIndicator(from: self.view)
            
            if isSuccess {
                AnalyticsHelper.shared.triggerEvent(type: .RD06A)
                self.showPaymentSuccessScreen()
                self.getBookingDetails()
                
            }else{
                AnalyticsHelper.shared.triggerEvent(type: .RD06B)
            }
        }
        
        
    }
    func showPaymentSuccessScreen() {
        let registrationConfirmationVC = UIStoryboard.loadRegistrationConfirmation()
        registrationConfirmationVC.modalPresentationStyle = .overFullScreen
        registrationConfirmationVC.titleText = kPaymentSuccessText
        registrationConfirmationVC.navigationText = kMyTransactionPaymentRedirectionMessage
        
        //let keyWindow = UIApplication.shared.keyWindow?.rootViewController?.presentedViewController
        self.present(registrationConfirmationVC, animated: false) {
            sleep(UInt32(2.0))
            
            self.dismiss(animated: true) {
                
            }
        }
    }
    
    
    
    
    // func onClickBtnPayNow(indexPath: IndexPath, booking: BookingHistory) {
    //     if let bookingId = booking.bookingId{
    //         if booking.bookingStatus == "PRE_RECEIPT"{
    //             proceedToPaymentPressed(bookingID: bookingId,
    //                                     paymentAmount: booking.billTotal ?? 0,
    //                                                      paymentRemarks: "Bill settlment", controller: nil)
    //         }else{
    //             proceedToPaymentPressed(bookingID: bookingId,
    //                                     paymentAmount: booking.billTotal ?? 0,
    //                                                      paymentRemarks: "Advance amount to be paid", controller: nil)
    //         }
    //
    //     }
    // }
    
    // func onClickBtnReschedule(indexPath: IndexPath, booking: BookingHistory) {
    //     bookingHistoryButtonsPressed(type: .Reschedule)
    // }
    //
    //     func onClickBtnCancel(indexPath: IndexPath, booking: BookingHistory) {
    //         bookingHistoryButtonsPressed(type: .Cancel)
    //     }
    //
    // func onClickBtnShare(indexPath: IndexPath, booking: BookingHistory) {
    //     let directionBottomSheet = Bundle.main.loadNibNamed("BookingMoreBottomSheet", owner: nil)!.first as! BookingMoreBottomSheet
    //     directionBottomSheet.delegate = self
    //     directionBottomSheet.view.maskByRoundingCorners(cornerRadius: 10, maskedCorners: CornerMask.topCornersMask)
    //     bottomSheetController?.present(directionBottomSheet, on: self, viewHeight: 268)
    ////        bookingHistoryButtonsPressed(type: .Share)
    // }
}




extension ReservationDetailsVC: BookingDetailsCellProtocol
{
    
    func dateTimeConverter(dateString: String?, timeString: String?)-> (String, String){
        guard let dateString = dateString else { return ("", timeString ?? "") }
        guard let originalTimeString = timeString else { return (dateString, "") }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: dateString)
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "EEEE, dd MMM,"
        let convertedDate = dateformatter.string(from: date!)
        //converting time to 12 hr format
        let time = BBQBookingUtilies.shared.getDisplayTimeIn12HourFormat(originalTimeString)
        //let convertedDateAndTime = convertedDate + " " + time
        return (convertedDate, time)
    }
    
    @objc func shareMethod() {
        guard let booking = bookingConfirmationViewModel.model  else {
            ToastHandler.showToastWithMessage(message: "Please try again!")
            return
        }
        
        let bookingId = booking.bookingID ?? ""
        let place = booking.branchName ?? ""
        guard let dateString = booking.reservationDate else { return }
        guard let originalTimeString = booking.reservationTime else { return }
        //converting time to 12 hr format
        let time = BBQBookingUtilies.shared.getDisplayTimeIn12HourFormat(originalTimeString)
        let urlstring  = booking.outletUrl ?? ""
        
        let textToShare = "\(BBQUserDefaults.sharedInstance.UserName) invites you to be part of his BBQ experience @BBQ Nation, \(place) on \( dateString )) at \(time) & ID \( bookingId ).Location"
        let textShareModel = ShareModel(data: textToShare, type: .Text, attributedText: nil)
        let urlToShare = "\(urlstring)"
        let urlShareModel = ShareModel(data: urlToShare, type: .Url, attributedText: nil)
        
        let pasteboard = UIPasteboard.general
        pasteboard.string = textToShare
        
        BBQShareHandler.shareContent(contentToShare: textShareModel, urlShareModel, on: self, isBottomSheetPresenter: false)
    }
    
    private func showReschedulePopup() {
        PopupHandler.showTwoButtonsPopup(title: kRescheduleResetAllTitle,
                                         message: kRescheduleResetAllDesc,
                                         on: self,
                                         firstButtonTitle: kNoButton,
                                         firstAction: { },
                                         secondButtonTitle: kYesButton) {
            self.resetAllForReschedule()
        }
    }
    
    private func resetAllForReschedule() {
        BBQActivityIndicator.showIndicator(view: self.view)
        self.bookingConfirmationViewModel.rescheduleResetAll(bookingId: bookingID) { (isFetched) in
            BBQActivityIndicator.hideIndicator(from: self.view)
            if isFetched {
                NotificationCenter.default.post(name: Notification.Name(Constants.NotificationName.upadateHomeHeader), object: nil)
                self.initiateReschedule()
            }
        }
    }
    
    private func initiateReschedule() {
        let bookingId = self.bookingID
        BBQActivityIndicator.showIndicator(view: self.view)
        self.afterDiningViewModel.fetchAfterDiningPaymentData(for: bookingId) { (isFetched) in
            BBQActivityIndicator.hideIndicator(from: self.view)
            if isFetched {
                self.showRescheduleScreen()
            }
        }
    }
    
    private func showRescheduleScreen() {
        let bookingBSHeight = CGFloat(470.00) // As per UX
        let minimumTop = UIScreen.main.bounds.height - bookingBSHeight
        let configuration = BottomSheetConfiguration(animationDuration: 0.4,
                                                     minimumTopSpacing: minimumTop)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        let bookingVC = BBQRescheduleViewController()
        bookingVC.bookingBottomSheet = bottomSheetController
        bookingVC.afterDiningViewModel = self.afterDiningViewModel
        bookingVC.showCalendarInBooking = false
        bookingVC.superVC = self
        self.navigationController?.pushViewController(bookingVC, animated: true)
        //            bottomSheetController.present(bookingVC, on: self)
    }
}

extension ReservationDetailsVC: BBQCancellationControllerDelegate {
    func showCancellationBottomSheet(bookingDate: String, bookingTime: String) {
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: self.view.frame.size.height - 100)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        self.bottomSheetController = bottomSheetController
        let cancellationBS = BBQCancellationBottomSheet()
        cancellationBS.bookingInfo = dateTimePlace ?? ""
        cancellationBS.bookingID = self.bookingID
        cancellationBS.outletLocation = bookingConfirmationViewModel.model?.branchName ?? ""
        cancellationBS.bookingDate = bookingDate
        cancellationBS.bookingTime = bookingTime
        cancellationBS.bookingAmount = String(format: "%@%.02f", getCurrency(), self.bookingConfirmationViewModel.sub_total + self.bookingConfirmationViewModel.tax_amount)
        cancellationBS.cancellationDelegate = self
        cancellationBS.view.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        cancellationBS.presentingBS = self.bottomSheetController!
        cancellationBS.superVC = self
        bottomSheetController.present(cancellationBS, on: self, viewHeight: 410)
        
        cancellationBS.loadContentView(for: self.isAdvanceAmountPaid)
    }
    
    @objc func rescheduleTapped(_ sender: Any) {
        self.showReschedulePopup()
    }
    
    func cancellationDone(status: Bool) {
        let cancellationConfirmationVC = UIStoryboard.loadCancellationDone()
        cancellationConfirmationVC.modalPresentationStyle = .overFullScreen
        self.present(cancellationConfirmationVC, animated: false) {
            sleep(3)
            cancellationConfirmationVC.dismiss(animated: true, completion: {
                //self.refreshForCancel()
                self.getBookingDetails()
                self.getUpdatedPoints()
                if self.cancelDelagate != nil {
                    self.cancelDelagate?.bookingCancelled(bookingId: self.bookingID)
                }
                //Firing notification to update homescreen
                NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.NotificationName.bookingCancelled),
                                                object: nil)
                
            })
        }
    }
}




extension ReservationDetailsVC: ReservationMoreInfoDelegate{
    func clickedCabBooking(viewController: ReservationMoreInfoVC) {
        AnalyticsHelper.shared.triggerEvent(type: .RD02B)
        bookingHistoryButtonsPressed(type: .Cab)
    }
    
    func clickedDirections(viewController: ReservationMoreInfoVC) {
        AnalyticsHelper.shared.triggerEvent(type: .RD02A)
        bookingHistoryButtonsPressed(type: .Direction)
    }
    
    func clickedCallOutlet(viewController: ReservationMoreInfoVC) {
        AnalyticsHelper.shared.triggerEvent(type: .RD02C)
        bookingHistoryButtonsPressed(type: .Phone)
    }
    
    func clickedShare(viewController: ReservationMoreInfoVC) {
        AnalyticsHelper.shared.triggerEvent(type: .RD02F)
        bookingHistoryButtonsPressed(type: .Share)
    }
    
    
    func bookingHistoryButtonsPressed(type: ReservationButtonType) {
        guard let booking = bookingConfirmationViewModel.model  else {
            ToastHandler.showToastWithMessage(message: "Please try again!")
            return
        }
        let latitude = booking.latitude
        let longitude = booking.longitude
        
        switch type {
            
        case .Cab:
            AnalyticsHelper.shared.triggerEvent(type: .RD02B)
            
            let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: self.view.frame.size.height - 278.0)
            let bottomSheetController = BottomSheetController(configuration: configuration)
            self.bottomSheetController = bottomSheetController
            let directionBottomSheet = Bundle.main.loadNibNamed("BookingRedirectionBottomSheet", owner: nil)!.first as! BookingRedirectionBottomSheet
            directionBottomSheet.latitude =  NSNumber(value: latitude!) as? Double
            directionBottomSheet.longitude =  NSNumber(value: longitude!) as? Double
            directionBottomSheet.bottomSheetType = .Cab
            directionBottomSheet.view.maskByRoundingCorners(cornerRadius: 10, maskedCorners: CornerMask.topCornersMask)
            if UIApplication.shared.canOpenURL(URL(fileURLWithPath: "uber://")) {
                directionBottomSheet.isUberAvailable = true
            }
            if UIApplication.shared.canOpenURL(URL(fileURLWithPath: "olacabs://")) {
                directionBottomSheet.isOlaAvailable = true
            }
            self.bottomSheetController!.present(directionBottomSheet, on: self)
            
        case .Direction:
            AnalyticsHelper.shared.triggerEvent(type: .RD02A)
            
            let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: self.view.frame.size.height - 278.0)
            let bottomSheetController = BottomSheetController(configuration: configuration)
            self.bottomSheetController = bottomSheetController
            let directionBottomSheet = Bundle.main.loadNibNamed("BookingRedirectionBottomSheet", owner: nil)!.first as! BookingRedirectionBottomSheet
            
            directionBottomSheet.latitude =  NSNumber(value: latitude!) as? Double
            directionBottomSheet.longitude =  NSNumber(value: longitude!) as? Double
            directionBottomSheet.bottomSheetType = .Direction
            directionBottomSheet.view.maskByRoundingCorners(cornerRadius: 10, maskedCorners: CornerMask.topCornersMask)
            if(UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
                directionBottomSheet.isGoogleMapsAvailable = true
            }
            self.bottomSheetController!.present(directionBottomSheet, on: self)
            
        case .Phone:
            AnalyticsHelper.shared.triggerEvent(type: .RD02C)
            bookingConfirmationViewModel.callNumber()
        case .Share:
            AnalyticsHelper.shared.triggerEvent(type: .RD02F)
            // ToastHandler.showToastWithMessage(message: "Press hold to paste the contents on Facebook")
            self.shareMethod()
            //_ = Timer.scheduledTimer(timeInterval: 0.0, target: self, selector: #selector(self.shareMethod), userInfo: nil, repeats: false)
            
        case .Reschedule:
            AnalyticsHelper.shared.triggerEvent(type: .RD03)
            print("reshedule pressed")
            self.showReschedulePopup()
            
        case .Cancel:
            AnalyticsHelper.shared.triggerEvent(type: .RD04)
            guard let booking = bookingConfirmationViewModel.model  else {
                ToastHandler.showToastWithMessage(message: "Please try again!")
                return
            }
            let branchName = booking.branchName
            self.isAdvanceAmountPaid =
            booking.advanceAmount ?? 0.00 > 0.00
            
            let date =  dateTimeConverter(dateString: booking.reservationDate, timeString: booking.reservationTime)
            dateTimePlace = date.0 + " " + date.1 + ", @" + (branchName ?? "")
            print("cancel presssed")
            showCancellationBottomSheet(bookingDate: date.0, bookingTime: date.1)
        }
    }
}

extension ReservationDetailsVC{
    private func getUpdatedPoints() {
        if BBQUserDefaults.sharedInstance.accessToken != "" {
            self.cartViewModel.getLoyaltyPointsCount { (isSuccess) in
                if isSuccess {
                    if let remainingPointsModel = self.cartViewModel.getPointCountModel, let remainingPoints = remainingPointsModel.remainingPoints {
                        BBQUserDefaults.sharedInstance.UserLoyaltyPoints = remainingPoints
                    }
                }
            }
        }
    }
    
}

extension ReservationDetailsVC: BBQCustomPaymentViewControllerDelegate, RazorpayPaymentCompletionProtocol{
    func onPaymentSuccess(_ payment_id: String, andData response: [AnyHashable : Any]) {
        self.navigationController?.popToViewController(self, animated: true)
        
        AnalyticsHelper.shared.triggerEvent(type: .RD05A)
        AnalyticsHelper.shared.triggerEvent(type: .RD06)
        let paymentGateway = Constants.BBQBookingStatus.razorPayment
        
        BBQActivityIndicator.showIndicator(view: self.view)
        self.bookingConfirmationViewModel.updateBooking(bookingId: bookingID, paymentOrderId: response["razorpay_order_id"] as? String ?? "", paymentId: response["razorpay_payment_id"] as? String ?? "", paymentGatewayName: paymentGateway, paymentSignature: response["razorpay_signature"] as? String ?? "") { (isSuccess) in
            BBQActivityIndicator.hideIndicator(from: self.view)
            
            if isSuccess {
                //  self.launchConfirmBookingScreen()
                AnalyticsHelper.shared.triggerEvent(type: .RD06A)
                self.showPaymentSuccessScreen()
                self.getBookingDetails()
                
            }else{
                AnalyticsHelper.shared.triggerEvent(type: .RD06B)
            }
        }
    }
    
    func onPaymentError(_ code: Int32, description str: String, andData response: [AnyHashable : Any]) {
        self.navigationController?.popToViewController(self, animated: true)
        AnalyticsHelper.shared.triggerEvent(type: .RD05B)
        switch code {
        case 0,1,3:
            PopupHandler.showSingleButtonsPopup(title: kFailedTitle,
                                                message: str,
                                                on: self, firstButtonTitle: kOkButtonTitle) { }
        case 2:
            self.showTransactionCancelled()
            
        default:
            print("");
        }
    }
    
    func createRazorPayPaymentId(paymentVC: BBQCustomPaymentViewController, completion: @escaping (WKWebView, [String : Any]?, String?, Razorpay?, String?) -> Void) {
        guard let booking = bookingConfirmationViewModel.model  else {
            ToastHandler.showToastWithMessage(message: "Please try again!")
            return
        }
        
        AnalyticsHelper.shared.triggerEvent(type: .booking_payment_status)
        
        self.paymentControllerViewModel.createPaymentOrder(with: String(format: "%.02f",bookingConfirmationViewModel.model?.billTotal ?? 0), currency: booking.currency ?? "", bookingID: booking.bookingID ?? "", loyalty_points: nil) { (orderID) in
            // Procceed only for valid order id
            guard orderID.count > 0 else {
                self.navigationController?.popToViewController(self, animated: true)
                Timer.scheduledTimer(withTimeInterval: 0.75, repeats: false) { timer in
                    self.onPaymentError(3, description: kRazorpayCrateOrderFailedDescription, andData: [:])
                    timer.invalidate()
                }
                return
            }
            
            let data = self.getWebViewPaymentId(paymentVC: paymentVC, order_id: orderID)
            completion(data.webView, data.options, orderID, self.razorpay, "")
            
        }
    }
}
