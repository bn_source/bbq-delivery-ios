//
 //  Created by Mahmadsakir on 23/02/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified ReservationDetailsCell.swift
 //

import UIKit

class ReservationDetailsCell: UITableViewCell {
    @IBOutlet weak var imgPackType: UIImageView!
    @IBOutlet weak var lblPackType: UILabel!
    @IBOutlet weak var lblPackQty: UILabel!
    @IBOutlet weak var lblLightLine: UILabel!
    @IBOutlet weak var lblPackAmount: UILabel!
    @IBOutlet weak var lblItemAmount: UILabel!
    
    var model: RescheduleDetails?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblPackQty.setCornerRadius(5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func setCellData(model: RescheduleDetails) {
        self.model = model
        
        lblPackType.text = model.buffetName
        lblPackQty.text = String(format: "x %li", model.packs ?? 0)
        lblPackAmount.text = String(format: "%@%.0f", getCurrency(), model.totalPrice ?? 0)
        lblItemAmount.text = String(format: "%@%.0f", getCurrency(), model.price ?? 0)
        if model.buffetType == "VEG"{
            if (model.buffetName?.lowercased() ?? "").contains("veg"){
                imgPackType.image = UIImage(named: "vegIcon")
            }else{
                imgPackType.image = UIImage(named: "drink_icon")
            }
        }else if model.buffetType == "KIDS"{
            imgPackType.image = UIImage(named: "kids_buffet_icon")
        }else{
            if (model.buffetName?.lowercased() ?? "").contains("veg"){
                imgPackType.image = UIImage(named: "nonvegIcon")
            }else{
                imgPackType.image = UIImage(named: "drink_icon")
            }
        }
    }
    
}
