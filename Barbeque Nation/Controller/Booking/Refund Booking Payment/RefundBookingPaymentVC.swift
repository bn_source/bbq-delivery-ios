//
 //  Created by Mahmadsakir on 18/10/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified PendingBookingVC.swift
 //

import UIKit

class RefundBookingPaymentVC: UIViewController {
    @IBOutlet weak var btnPayNow: UIButton!
    @IBOutlet weak var lblRestaurantName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblRefundAmount: UILabel!
    @IBOutlet weak var lblPaymentId: UILabel!
    
    var selectedTimeString : String?
    var refund_amount: String?
    var refund_id: String?
    var restaurant_id: String?
    var delegate: RefundBookingPaymentDelegate?
    var superVC: UIViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblRefundAmount.text = String(format: "%@%@", getCurrency(), refund_amount ?? "0")
        lblPaymentId.text = refund_id
        lblRestaurantName.text = BBQUserDefaults.sharedInstance.CurrentSelectedLocation
        lblTime.text = selectedTimeString
    }
    
    @IBAction func onClickBtnBack(_ sender: Any) {
        if let superVC = superVC {
            self.navigationController?.popToViewController(superVC, animated: true)
        }else{
            self.navigateToHomePageForceFull()
        }
    }
    
    @IBAction func onClickBtnOkay(_ sender: Any) {
        if let superVC = superVC {
            self.navigationController?.popToViewController(superVC, animated: true)
        }else{
            self.navigateToHomePageForceFull()
        }
    }
}


protocol RefundBookingPaymentDelegate{
    func dismissRefund(viewController: RefundBookingPaymentVC)
}
