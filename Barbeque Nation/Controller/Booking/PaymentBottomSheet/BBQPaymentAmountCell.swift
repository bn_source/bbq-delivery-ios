//
//  BBQPaymentAmountCell.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 11/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

class BBQPaymentAmountCell: UITableViewCell {

    @IBOutlet weak var amountTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        amountTextField.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

extension BBQPaymentAmountCell: UITextFieldDelegate {
    
}
