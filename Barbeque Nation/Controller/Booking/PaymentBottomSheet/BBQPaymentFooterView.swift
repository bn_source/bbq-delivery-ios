//
//  BBQPaymentFooterView.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 11/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

protocol BBQPaymentFooterViewDelegate {
    func animateScreenForTextField(isUp: Bool, textFieldText: String)
    func didTapOnPaymentButton()
    func didTapOnCancelButton()
}

class BBQPaymentFooterView: UIView {
    
    //MARK:- IBOutlets
    @IBOutlet weak var remarksTextField: UITextField!
    @IBOutlet weak var paymentButton: UIButton!
    
    let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

    
    //MARK:- Properties
    var delegate: BBQPaymentFooterViewDelegate?
    
    //MARK:- Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
    }
    
    //MARK:- UI Methods
    private func configureUI() {
        remarksTextField.delegate = self
        paymentButton.backgroundColor = .paymentButtonOrangeColor
        paymentButton.setTitle(kPaymentButtonTitle, for: .normal)
        paymentButton.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
    }

    @IBAction func paymentButtonPressed(_ sender: UIButton) {
        if let delegate = self.delegate {
            delegate.didTapOnPaymentButton()
        }
    }
    
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        if let delegate = self.delegate {
            delegate.didTapOnCancelButton()
        }
    }
}

extension BBQPaymentFooterView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        if let delegate = self.delegate {
            delegate.animateScreenForTextField(isUp: true, textFieldText: textField.text ?? "")
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        if let delegate = self.delegate {
            delegate.animateScreenForTextField(isUp: false, textFieldText: textField.text ?? "")
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //No special character allowed
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: "")
        
        //Max 256 characters
        let maxLength = 256
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        //Return true only if text is not a special character and max length in not crossed
        return (string == filtered) && (newString.length <= maxLength)
    }
}
