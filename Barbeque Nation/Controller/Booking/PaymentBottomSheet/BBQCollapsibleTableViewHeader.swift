//
//  CollapsibleTableViewHeader.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 11/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

protocol CollapsibleTableViewHeaderDelegate {
    func toggleSection(_ header: BBQCollapsibleTableViewHeader, section: Int)
}

class BBQCollapsibleTableViewHeader: UITableViewHeaderFooterView {

    //MARK:- IBOutlets
    @IBOutlet weak var headerTitleLabel: UILabel!
    
    //MARK:- Properties
    var section: Int = 0
    var delegate: CollapsibleTableViewHeaderDelegate?
    
    //MARK:- Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //MARK:- UI Methods
    func configureHeaderWith(text titleText: String) {
        self.headerTitleLabel.text = titleText
        
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(BBQCollapsibleTableViewHeader.tapHeader(_:))))
    }
    
    // Trigger toggle section when tapping on the header
    @objc func tapHeader(_ gestureRecognizer: UITapGestureRecognizer) {
        guard let cell = gestureRecognizer.view as? BBQCollapsibleTableViewHeader else {
            return
        }
        
        delegate?.toggleSection(self, section: cell.section)
    }
}
