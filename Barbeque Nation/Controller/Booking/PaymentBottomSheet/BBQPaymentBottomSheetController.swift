//
//  BBQPaymentBottomSheetController.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 10/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit
import FirebaseAnalytics

typealias PaymentDetails = (advanceAmount: Double, totalBill: Double, bookingID: String)

protocol BBQPaymentBottomSheetProtocol: AnyObject {
    func proceedToPaymentPressed(bookingID: String, paymentAmount: Double,
                                 paymentRemarks: String, controller: UIViewController?)
    func cancelPressed()
}

class BBQPaymentBottomSheetController: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var advancePayTitleLabel: UILabel!
    @IBOutlet weak var paymentTableView: UITableView!
    
    //MARK:- Properties
    fileprivate let headerCellId = Constants.CellIdentifiers.paymentBottomSheetHeaderCell
    fileprivate let footerCellId = Constants.CellIdentifiers.paymentBottomSheetFooterCell
    fileprivate let paymentCellId = Constants.CellIdentifiers.paymentAmountCell
    
    fileprivate var sections = [Section]()
    fileprivate var previousSection: Int? = -1
    fileprivate var activeSection: Int = -1
    fileprivate var footerView: BBQPaymentFooterView? = nil
    
    fileprivate var textFieldText = ""
    fileprivate var isKeyboardShown = false
    
    var bottomSheetController: BottomSheetController? = nil
    var buffetsViewModel: BBQBookingBuffetsViewModel? = nil
    
    lazy var makePaymentViewModel : MakePaymentViewModel = {
        let viewModel = MakePaymentViewModel()
        return viewModel
    }()
    
    weak var delegate: BBQPaymentBottomSheetProtocol? = nil
    
    var paymentDetails: PaymentDetails?
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AnalyticsHelper.shared.screenName(name: "BookingPaymentScreen", className: "BBQPaymentBottomSheetController")
        Analytics.logEvent("BookingPayment", parameters: [
            "name": "BookingPayment" as NSObject,
            "full_text": "BookingPayment page" as NSObject
            ])
        
        // Do any additional setup after loading the view.
        sections = makePaymentViewModel.sectionsData
        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.initialOpenSection()
    }
    
    //MARK:- UI Methods
    private func setupUI() {
        paymentTableView.register(UINib(nibName: paymentCellId, bundle: nil), forCellReuseIdentifier: paymentCellId)
        self.view.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        self.advancePayTitleLabel.text = kAdvancePayHeaderTitle
    }
    
    private func initialOpenSection() {
        activeSection = 0
        toggleFor(section: 0)
    }
    
    private func toggleFor(section: Int) {
        let collapsed = !sections[section].collapsed
        
        // Toggle
        sections[section].collapsed = collapsed
        if previousSection != -1, previousSection != section {
            let prevCollapse = sections[previousSection!].collapsed
            if prevCollapse {
                self.setTitleFor(section: section)
                paymentTableView.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
                self.expandBottomSheet(isExpanding: !collapsed)
            } else {
                sections[previousSection!].collapsed = true
                self.setTitleFor(section: section)
                self.setTitleFor(section: previousSection!)
                paymentTableView.reloadSections(IndexSet(arrayLiteral: section, previousSection!), with: .automatic)
            }
        } else {
            self.setTitleFor(section: section)
            paymentTableView.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
            self.expandBottomSheet(isExpanding: !collapsed)
        }
        
        previousSection = section
    }
    
    private func setTitleFor(section: Int) {
        if section == 0 {
            sections[section].title = sections[section].collapsed ? kMinimumPaySectionUnselectedTitle : kMinimumPaySectionTitle
        } else {
            sections[section].title = sections[section].collapsed ? kFullPaySectionUnselectedTitle : kFullPaySectionTitle
        }
    }
    
    private func expandBottomSheet(isExpanding: Bool) {
        self.bottomSheetController?.expandOrCollapse(viewController: self, withHeight: 36.0, isExpand: isExpanding)
    }
    
    private func activateKeyboardFor(section: Int) {
        if previousSection != -1, previousSection != section {
            let previousCell = paymentTableView.cellForRow(at: IndexPath(row: 0, section: previousSection!)) as? BBQPaymentAmountCell
            previousCell?.amountTextField.resignFirstResponder()
        }
        
        let currentCell = paymentTableView.cellForRow(at: IndexPath(row: 0, section: section)) as? BBQPaymentAmountCell
        currentCell?.amountTextField.becomeFirstResponder()
    }
    
    @objc func doneButtonAction() {
        self.view.endEditing(true)
    }
    
    private func validateTextFieldsText() -> String {
        if activeSection != -1 {
            let cell = paymentTableView.cellForRow(at: IndexPath(row: 0, section: activeSection)) as? BBQPaymentAmountCell
            let enteredAmount = cell?.amountTextField.text
            if let amount = enteredAmount {
                return amount
            } else {
                return ""
            }
        }
        return ""
    }
}

extension BBQPaymentBottomSheetController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return makePaymentViewModel.numberOfPaymentType
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].collapsed ? 0 : sections[section].rowItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: paymentCellId, for: indexPath) as? BBQPaymentAmountCell else {
            return UITableViewCell()
        }
        //populated the amount from response data
        if indexPath.section == 0 {
            let advanceAmount: Double = self.paymentDetails?.advanceAmount ?? 0.0
            cell.amountTextField.text = String(format: "%.2f", advanceAmount)
            cell.amountTextField.isUserInteractionEnabled = false
        } else if indexPath.section == 1 {
            
            cell.amountTextField.isUserInteractionEnabled = false
            let fullAmount:Double = self.paymentDetails?.totalBill ?? 0.0
            cell.amountTextField.text = String(format: "%.2f", fullAmount)
            cell.amountTextField.isUserInteractionEnabled = false
            
        } else if indexPath.section == 2 {
            // Add other amount if needed.
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 36.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UINib(nibName: headerCellId, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! BBQCollapsibleTableViewHeader
        
        header.configureHeaderWith(text: sections[section].title)
        header.section = section
        header.delegate = self
        
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == sections.count - 1 {
            return 94.0
            //return 152.0
        }
        return 1.0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == sections.count - 1 {
            footerView = UINib(nibName: footerCellId, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? BBQPaymentFooterView
            footerView?.remarksTextField.text = self.textFieldText
            footerView?.delegate = self
            return footerView
        }
        return nil
    }
}

extension BBQPaymentBottomSheetController: CollapsibleTableViewHeaderDelegate {
    func toggleSection(_ header: BBQCollapsibleTableViewHeader, section: Int) {
        activeSection = section
        toggleFor(section: section)
        activateKeyboardFor(section: section)
    }
}

extension BBQPaymentBottomSheetController: BBQPaymentFooterViewDelegate {
    func didTapOnPaymentButton() {
        //self.removeKeyboard()
        //Check whether any section is selected, if not show error and return.
        var isSectionSelected = false
        for section in self.sections {
            if section.collapsed == false {
                isSectionSelected = true
                break
            }
        }
        
        if !isSectionSelected {
            ToastHandler.showToastWithMessage(message: "Please choose any option to continue")
            return
        }
        
        let amountEntered = validateTextFieldsText()
        if amountEntered != "", let delegate = self.delegate {
            let remarks = footerView?.remarksTextField.text
            
            let payAmount = (amountEntered as NSString).doubleValue
            delegate.proceedToPaymentPressed(bookingID: paymentDetails?.bookingID ?? "",
                                             paymentAmount: payAmount,
                                             paymentRemarks: remarks ?? "", controller: self)
        }
    }
    
    func animateScreenForTextField(isUp: Bool, textFieldText: String) {
        self.textFieldText = textFieldText
        footerView?.remarksTextField.text = textFieldText
        
        let movementDistance:CGFloat = -275
        let movementDuration: Double = 0.3
        var movement:CGFloat = 0
        
        if isUp {
            self.isKeyboardShown = true
            movement = movementDistance
        } else {
            self.isKeyboardShown = false
            movement = -movementDistance
        }
        
        UIView.beginAnimations(Constants.NotificationName.animateTextField, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
    
    func removeKeyboard() {
        if self.isKeyboardShown {
            self.view.endEditing(true)
            self.isKeyboardShown = false
        }
    }
    
    func didTapOnCancelButton() {
        //self.removeKeyboard()
        PopupHandler.showTwoButtonsPopup(title: kPaymentCancelTitleText,
                                         message: kPaymentCancelDescriptionText,
                                         image: UIImage(named: "ICON-BBQ_Cancel"),
                                         titleImageFrame: CGRect(x: 0, y: -3, width: 45, height: 29),
                                         on: self,
                                         firstButtonTitle: kPaymentCancelYesText, firstAction: {
                                            self.bottomSheetController?.dismissBottomSheet(on: self, completion: { (isDismissed) in
                                                if let delegate = self.delegate {
                                                    delegate.cancelPressed()
                                                }
                                            })
                                        },
                                         secondButtonTitle: kPaymentCancelNoText) { }
    }
}
