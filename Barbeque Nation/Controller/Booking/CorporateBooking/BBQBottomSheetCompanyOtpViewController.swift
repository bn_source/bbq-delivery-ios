//
 //  Created by Maya R on 26/08/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQBottomSheetCompanyOtpViewController.swift
 //

import UIKit
enum Direction1  {case left,right}

class BBQBottomSheetCompanyOtpViewController: UIViewController {
    // MARK: - IBOutlets

    @IBOutlet var containerView: UIView!
    @IBOutlet weak var resendOtpButton:UIButton!
    @IBOutlet  var otpTextfieldCollection: [UITextField]!
    @IBOutlet  var otpBackButton: UIButton!
    var textFieldsIndexes:[UITextField:Int] = [:]

    // MARK: - Life Cycle Methods

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(Constants.NotificationName.otpVeifiedNotification) , object: nil)
    }

    // MARK: - Setup Methods

    func setupUI(){
        self.view.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)

        let yourAttributes: [NSAttributedString.Key: Any] = [
            .foregroundColor: UIColor.black,
            .underlineStyle: NSUnderlineStyle.single.rawValue]
        let attributeString = NSMutableAttributedString(string: kResendOtp,
                                                        attributes: yourAttributes)
        resendOtpButton.setAttributedTitle(attributeString, for: .normal)
        
        for index in 0 ..< otpTextfieldCollection.count {
            textFieldsIndexes[otpTextfieldCollection[index]] = index
        }
        for textfileds in otpTextfieldCollection {
            textfileds.addLine(position: .LINE_POSITION_BOTTOM, color: .darkGray, width: 0.5)
        }
        otpTextfieldCollection[0].becomeFirstResponder()

    }
    
    // MARK: - setNextResponder
    func setNextResponder(_ index:Int?, direction:Direction1) {
        guard let index = index else { return }
        var otpText :String = ""

        if direction == .left {
            index == 0 ?
                (_ = otpTextfieldCollection.first?.resignFirstResponder()) :
                (_ = otpTextfieldCollection[(index - 1)].becomeFirstResponder())
        } else {
            index == otpTextfieldCollection.count - 1 ?
                (_ = otpTextfieldCollection.last?.resignFirstResponder()) :
                (_ = otpTextfieldCollection[(index + 1)].becomeFirstResponder())
        }
        if (index >= 5) {
            for textfileds in otpTextfieldCollection {
                otpText.append(textfileds.text!)
            }
            if(otpText.count == 6){
                perform(#selector(self.goToCorporateBooking), with: nil, afterDelay: 1)
            }else{
                let alert = UIAlertController(title: "", message: "Please enter the otp", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true)

            }
        }
    }
    
   
    // MARK: - goToCorporateBooking
   @objc  func goToCorporateBooking(){
        self.dismiss(animated: false , completion: nil)
        
        NotificationCenter.default.post(name: Notification.Name(Constants.NotificationName.otpVeifiedNotification), object:nil)

    }
    
    // MARK: - animateOtpTextField
    func animateOtpTextField(textField: UITextField, up: Bool){
        let movementDistance:CGFloat = -230
        let movementDuration: Double = 0.3
        
        var movement:CGFloat = 0
        if up{
            movement = movementDistance
        }
        else{
            movement = -movementDistance
        }
        UIView.beginAnimations(Constants.NotificationName.animateTextField, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }

    // MARK: - otpBackClicked
    @IBAction func otpBackClicked(_ sender:UIButton){
        self.dismiss(animated: false, completion: nil)
    }
}

// MARK: - UITextFieldDelegate
extension BBQBottomSheetCompanyOtpViewController:UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        self.animateOtpTextField(textField: textField, up:true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        self.animateOtpTextField(textField: textField, up:false)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if range.length == 0 {
            textField.text = string
            setNextResponder(textFieldsIndexes[textField], direction: .right)
            return true
        } else if range.length == 1 {
            textField.text = ""
            setNextResponder(textFieldsIndexes[textField], direction: .left)
            return false
        }
        
        return false
    }
}
