//
//  Created by Sridhar on 26/08/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQBottomSheetCorporateViewController.swift
//
//  CHANGE LOG
//
//  —-----------------------------------------------------------------------------------------
//  ID          Bug ID      Author Name          Date                  Description
//  —-----------------------------------------------------------------------------------------
//  1            NA         Ajith CP             17/09/19        Customization for reusability



import UIKit

enum BBQEmailType : Int {
    case corporate = 0
    case personal
}


protocol BBQBottomSheetCorporateViewControllerDelegate {
   
    func didFinishedEmailValidation(emailId: String,
                                    otpID: Int,
                                    verifierOTP: Int)
    
    func didFinishSelectingCorporateOffer(corporateOffer:CorporateOffer)
    
}

class BBQBottomSheetCorporateViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet var containerView: UIView!
    
    @IBOutlet weak var verifedImageView:UIImageView!
    @IBOutlet weak var titleImageView: UIImageView!
    
    @IBOutlet weak var companyEmailTextField:UITextField!
    
    @IBOutlet weak var comapnyBackButton:UIButton!
    @IBOutlet weak var sendOtpButton:UIButton!
    @IBOutlet weak var doneButton:UIButton!
   
    @IBOutlet weak var companyEmailLabel:UILabel!
    @IBOutlet weak var companyErrorLabel:UILabel!
    @IBOutlet weak var titlelLabel: UILabel!
    
    var validatorDelegate : BBQBottomSheetCorporateViewControllerDelegate?
    var emailType : BBQEmailType?
    
    var componentTitle : String?
    var componentTitleLogo : UIImage?
    
    var isOtpVerified:Bool = false
    var corporateEmail:String = ""
    
    private var otpIDEmail : Int?
    private var verifierOTPEmail : Int?
    
    var currentBottomSheet : BottomSheetController?
    
    lazy var coprporateViewModel : CorporateBookingViewModel = {
        let corpModel = CorporateBookingViewModel()
        return corpModel
    }()
    
    lazy private var profileViewModel : BBQProfileViewModel = {
        let profileVM = BBQProfileViewModel()
        return profileVM
    } ()
    
    var buffetsViewModel: BBQBookingBuffetsViewModel?
    
    // MARK: - Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.otpVerified),
            name: Notification.Name(Constants.NotificationName.otpVeifiedNotification),
            object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(Constants.NotificationName.otpVeifiedNotification) , object: nil)
    }
    
    // MARK: - Setup Methods
    
    func setupView(){
        sendOtpButton.setTitle(kSendOtp, for: .normal)
        companyEmailTextField.returnKeyType = .done
        self.doneButton.isHidden = true
        self.sendOtpButton.isHidden = false
        //self.view .bringSubviewToFront(self.otpView)
        self.companyErrorLabel.isHidden = true
        self.companyEmailLabel.isHidden = true
        self.companyEmailLabel.text = (self.emailType == BBQEmailType.personal) ?
            kPersonalEmail : kCompanyEmail
        self.companyEmailTextField.addLine(position: .LINE_POSITION_BOTTOM, color: .darkGray, width: 0.5)
        verifedImageView.isHidden = true
        
        self.titlelLabel.text = self.componentTitle
        self.titleImageView.image = self.componentTitleLogo
        self.companyEmailTextField.placeholder = (self.emailType == BBQEmailType.personal) ?
                kPersonalEmail : kCompanyEmail
        self.comapnyBackButton.isHidden = (self.emailType == BBQEmailType.personal)
    }
    
    
    // MARK: - otpVerified notification
    @objc private func otpVerified(notification: NSNotification){
        //do stuff using the userInfo property of the notification object
        sendOtpButton.isHidden = true
        doneButton.isHidden = false
        companyEmailTextField.text = corporateEmail
        // Making email field non-editable once after verified.
        self.companyEmailTextField.isEnabled = false
        verifedImageView.isHidden =  false
    }
    
    // MARK: - animateEmailTextField action
    func animateEmailTextField(textField: UITextField, up: Bool){
//        let movementDistance:CGFloat = -220
//        let movementDuration: Double = 0.3
//        var movement:CGFloat = 0
//        if up{
//            movement = movementDistance
//        }else{
//            movement = -movementDistance
//        }
//        UIView.beginAnimations(Constants.NotificationName.animateTextField, context: nil)
//        UIView.setAnimationBeginsFromCurrentState(true)
//        UIView.setAnimationDuration(movementDuration)
//        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
//        UIView.commitAnimations()
    }
    
    // MARK: - isValidEmail
    func isValidEmail(emailStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
    
    // MARK: - sendOtpClicked action
    @IBAction func sendOtpClicked(_ sender:UIButton) {
        let isEmailValid = isValidEmail(emailStr: self.companyEmailTextField.text!)
        if(isEmailValid) {
            corporateEmail = companyEmailTextField.text!
            BBQActivityIndicator.showIndicator(view: self.view)

            if self.emailType == BBQEmailType.personal {
                self.profileViewModel.generateEmailOTP(emailID: corporateEmail) { (didOTPSent) in
                    if(didOTPSent){
                        let otpModel = self.profileViewModel.getOTPModel
                        self.otpIDEmail = Int(otpModel?.otpId ?? "0")
                        BBQActivityIndicator.hideIndicator(from: self.view)
                        self.goToOTPScreenWithOtpID(otpModel!.otpId ?? "0")
                    } else {
                        print("Sending otp to email has failed")
                    }
                }
            } else  {
                coprporateViewModel.verifyCoporateEmail(corporateEmailId: corporateEmail, otpID: "") { (isSuccess) in
                    BBQActivityIndicator.hideIndicator(from: self.view)

                    if(isSuccess){
                        let corporateModel = self.coprporateViewModel.getCorporateOtp
                        self.otpIDEmail = Int(corporateModel?.corporateOtpId ?? "0")
                        self.goToOTPScreenWithOtpID(corporateModel!.corporateOtpId)
                    } else {
//                        Invalid corporate email id
                       self.companyErrorLabel.isHidden = false
                        self.companyErrorLabel.text = kInvalidCorporateEmailID
                        self.companyErrorLabel.startBlink()

                    }
                }
            }
            
        } else {
            companyEmailTextField.resignFirstResponder()
            companyErrorLabel.isHidden = false
            companyErrorLabel.text = kIncorrectEmail
            companyErrorLabel.startBlink()
            sendOtpButton.isEnabled = false
        }
        
    }
    // MARK: - goToOTPScreenWithOtpID action
    func goToOTPScreenWithOtpID(_ otp_id:String) {
        let otpViewController = UIStoryboard.loadOTPReceiverViewController()
        otpViewController.delegate = self
//        otpViewController.currentTheme = .dark
//        otpViewController.screenHeight = 400
        otpViewController.otpID = otp_id
        otpViewController.otpType = .corporate
//        otpViewController.shouldCloseTouchingOutside = true
        otpViewController.validationPath = self.companyEmailTextField.text ?? ""
        otpViewController.validationType = .corporateEmailID

         if self.emailType == BBQEmailType.corporate{
            otpViewController.header = kCorporateOffers
            otpViewController.logoImage = Constants.AssetName.corporateIcon

         }else{
            otpViewController.header = kUpdateEmailTitle
            otpViewController.logoImage = Constants.AssetName.bbqAddEmailTitle

        }
        otpViewController.otpMessage =  (self.emailType == BBQEmailType.personal) ?
        kEmailSentNotification : kOtpSentNotification
        otpViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        let configuration = BottomSheetConfiguration(animationDuration: 0.4,
                                                     minimumTopSpacing: 0.0)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        bottomSheetController.present(otpViewController, on: self)
        
    }
    
    // MARK: - corpBackClicked action
    @IBAction func corpBackClicked(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - corpDoneClicked action
    @IBAction func corpDoneClicked(_ sender:UIButton) {
        
        if self.emailType == BBQEmailType.personal { // In case of update email
             self.validatorDelegate?.didFinishedEmailValidation(emailId: self.corporateEmail,
                                                                otpID: self.otpIDEmail!,
                                                                verifierOTP: self.verifierOTPEmail!)
        } else {
            
            let viewController = BBQAllOffersViewController()
            viewController.screenType = .CorporateOffers
            viewController.buffetsViewModel = self.buffetsViewModel
            viewController.corporateEmailID = corporateEmail
            //let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: self.view.frame.size.height )
           // let bottomSheetController = BottomSheetController(configuration: configuration)
            viewController.delegate = self
//            bottomSheetController.present(viewController, on: self)
            viewController.modalPresentationStyle = .fullScreen
            self.present(viewController, animated: true, completion: nil)
        }
       
    }
    // MARK :- showErrorAlertWithMessage
    func showErrorAlertWithMessage(message:String) {
        ToastHandler.showToastWithMessageAndAction(message: message,
                                                   actionText: kOkButton) {
                                                    
        }
    }
}

// MARK: - UITextFieldDelegate methods

extension BBQBottomSheetCorporateViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        companyEmailTextField.resignFirstResponder()
        companyEmailLabel.isHidden = false
        companyErrorLabel.isHidden = true
        companyErrorLabel.stopBlink()
        verifedImageView.isHidden = true

        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField){
        companyErrorLabel.isHidden = true
        verifedImageView.isHidden = true
        companyEmailLabel.isHidden = false
        sendOtpButton.isEnabled = true
        if textField.text == ""{
            textField.placeholder = ""
        }
        self.animateEmailTextField(textField: textField, up:true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        self.companyEmailLabel.isHidden = false
        companyErrorLabel.isHidden = true
        verifedImageView.isHidden = !self.isOtpVerified

        self.animateEmailTextField(textField: textField, up:false)
    }
}

extension BBQBottomSheetCorporateViewController: BBQAllOffersProtocol {
    func userVoucherSelectedWith(voucherList: [VoucherCouponsData], type: ScreenType, termsAndConditions: [String]) {
        
    }
    
    func navigateToLoginScreen() {
        
    }
   
    func addButtonClickedAt(index: Int) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func buyNowButtonClicked(bottomSheet: BottomSheetController?) { }
    
    func selectedCorporateOffer(corporateOffer:CorporateOffer){
        //update booking with user selected corporate offer
        print(corporateOffer)
        self.validatorDelegate?.didFinishSelectingCorporateOffer(corporateOffer: corporateOffer)
        if let presentingController = self.currentBottomSheet{
            presentingController.dismissBottomSheet(on: self)
        }

    }
   
}

extension BBQBottomSheetCorporateViewController:BBQOTPScreenDelegate{
   
    func otpValidation(status: Bool, otp: String) {
        if status == true {
            self.isOtpVerified = true
            self.verifierOTPEmail = Int(otp)
            sendOtpButton.isHidden = true
            doneButton.isHidden = false
            companyEmailTextField.text = corporateEmail
            verifedImageView.isHidden =  false
            verifedImageView.image = UIImage(named: Constants.AssetName.corporateIconTick)
            self.companyEmailLabel.text = (self.emailType == BBQEmailType.personal) ?
                kAddEmailString : kCompanyEmail
            companyErrorLabel.isHidden = true
            // Making email field non-editable once after verified
            self.companyEmailTextField.isEnabled = false
        }else{
            //If otp verification failed
            self.isOtpVerified = false
            sendOtpButton.isHidden = false
            doneButton.isHidden = true
            verifedImageView.isHidden =  false
            verifedImageView.image = UIImage(named: Constants.AssetName.corporateIconWrong)
            companyErrorLabel.isHidden = false
            if self.emailType == BBQEmailType.corporate {
                companyEmailTextField.text = ""
                companyErrorLabel.text = kEmailNotVerified
            }
            self.companyEmailTextField.isEnabled = true
        
            if (self.emailType == BBQEmailType.personal) {
                // For personal email we are only showing custom toast as similar to update mobile.
                verifedImageView.isHidden  =  true
                companyErrorLabel.isHidden = true
                self.view.endEditing(true)
                ToastHandler.showToastWithMessage(message: kMaxAtmpOtpReachDesc)
            }
        }
    }
    
    func backButtonPressed() {
        
    }
    
    
}
