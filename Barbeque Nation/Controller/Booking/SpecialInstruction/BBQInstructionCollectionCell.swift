//
 //  Created by Maya R on 29/08/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified InstructionCollectionCell.swift
 //

import UIKit

protocol BBQInstructionProtocol: AnyObject {
    func didOccasionSelected(currentIndex:NSIndexPath)
    
}
class BBQInstructionCollectionCell: UICollectionViewCell {
    @IBOutlet weak var occasionButton:UIButton!
    weak var celebrationDelegate: BBQInstructionProtocol? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    
//        let blackColor = UIColor(red:0.21, green:0.25, blue:0.27, alpha:0.6)// 1.0 alpha
//        let semiBlack = blackColor.withAlphaComponent(0.6) // 0.6 alpha
//        self.occasionButton.roundCorners(cornerRadius: 18.0, borderColor: semiBlack, borderWidth: 1.0)
    }
    
    @IBAction func occassionTapped(_ sender :UIButton){
        guard let delegate = self.celebrationDelegate else {
            return
        }
        let occasionIndex =  IndexPath(row: sender.tag, section: 0) as NSIndexPath
        delegate.didOccasionSelected(currentIndex: occasionIndex)

    }
}
