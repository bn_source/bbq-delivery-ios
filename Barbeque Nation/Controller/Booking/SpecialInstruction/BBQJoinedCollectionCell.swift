//
//  Created by Maya R on 18/09/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQJoinedCollectionCell.swift
//

import UIKit

protocol BBQJoinCellProtocol: AnyObject {
    func didJoinedSelected(currentIndex:NSIndexPath)

}
class BBQJoinedCollectionCell: UICollectionViewCell {
    @IBOutlet weak var joinButton:UIButton!
    weak var joinedDelegate: BBQJoinCellProtocol? = nil

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let blackColor = UIColor(red:0.21, green:0.25, blue:0.27, alpha:0.6)// 1.0 alpha
        let semiBlack = blackColor.withAlphaComponent(0.6) // 0.6 alpha
        self.joinButton.roundCorners(cornerRadius: 18.0, borderColor: semiBlack, borderWidth: 1.0)
    }
    
    @IBAction func joinedTapped(_ sender:UIButton){
        guard let delegate = self.joinedDelegate else {
            return
        }
        let occasionIndex =  IndexPath(row: sender.tag, section: 1) as NSIndexPath
        delegate.didJoinedSelected(currentIndex: occasionIndex)
    }
    
    
}
