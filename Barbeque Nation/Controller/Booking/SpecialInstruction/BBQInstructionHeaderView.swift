//
 //  Created by Maya R on 29/08/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQINstructionHeaderView.swift
 //

import UIKit

class BBQInstructionHeaderView: UICollectionReusableView {
    @IBOutlet weak var headerTitleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
