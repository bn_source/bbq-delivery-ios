//
//  Created by Maya R on 29/08/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQSpecialInstructionViewController.swift
// * CHANGE LOG
//* —------------------------------------------------------------------------------
//    * ID          Bug ID      Author Name          Date                  Description
//  * —------------------------------------------------------------------------------
//    * 1            NA         Maya Ranjith         29/08/19             Initial Version
//    * 2            NA         Maya Ranjith         21/09/19             Review Comments



import UIKit
protocol SpecialInsructionProtocol {
    /// Returns on clicking done button
    func specialInstructionsAdded(celebrationName:String)
}

class BBQSpecialInstructionViewController: UIViewController {
    // MARK: IBOutlets
    @IBOutlet weak var specialInstructionLabel: UILabel!
    @IBOutlet weak var contentView:UIView!
    @IBOutlet weak var instructionCollectionView:UICollectionView!
    @IBOutlet weak var textViewSpecialRequest: UITextView!
    
    var instructionDelegate : SpecialInsructionProtocol?
    private var celebrationsArray : [CelebrationModel] = []
    private var joinArray : [JoinedByModel] = []
    private var selectedIndexPath1: NSIndexPath?
    private var selectedIndexPath2: NSIndexPath?
    private var selectedOccasionValue:String = ""
    private var selectedOccasions:[String] = []
    private var selectedJoinedIds:[String] = []
    
    
    lazy var instructionViewModel : BBQSpecialInstructionViewModel = {
        let instructModel = BBQSpecialInstructionViewModel()
        return instructModel
    }()
    
    var selectedBookingID :String = "" {
        didSet{
            print("")
        }
    }
    // MARK: Lifecycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        specialInstructionLabel.text = kSpecialInstructionHeaderTitle
        setviewInsructionView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getCelebrationAndJoinedBy()
    }
    
    // MARK: setup methods
    func setviewInsructionView(){
        self.view.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        
        self.instructionCollectionView.collectionViewLayout = TagFlowLayout()

        instructionCollectionView.registerHeaderView(nibName: "BBQInstructionHeaderView", kind: UICollectionView.elementKindSectionHeader)
        
        instructionCollectionView.register(UINib(nibName: Constants.NibName.specialInstructionNibName, bundle: nil), forCellWithReuseIdentifier: Constants.CellIdentifiers.specialInstructionCell)
        
        instructionCollectionView.register(UINib(nibName: Constants.NibName.joinedCellNibName, bundle: nil), forCellWithReuseIdentifier: Constants.CellIdentifiers.joinedInstructionCell)
        
        instructionCollectionView.register(UINib(nibName: Constants.NibName.specialInstructionNibName, bundle: nil), forCellWithReuseIdentifier: Constants.CellIdentifiers.specialInstructionCell)
        
        instructionCollectionView.register(UINib(nibName: Constants.NibName.instructionHeaderNibName, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier:Constants.CellIdentifiers.specialInstructionHeader)
        
        self.instructionCollectionView.allowsMultipleSelection = true
        
    }
    
    // MARK: getCelebrationAndJoinedBy from API
    func getCelebrationAndJoinedBy(){
        BBQActivityIndicator.showIndicator(view: self.view)
        self.instructionViewModel.getSpecialInstructionValues { (isSuccess) in
            BBQActivityIndicator.hideIndicator(from: self.view)
            if(isSuccess){
                
                self.celebrationsArray = self.instructionViewModel.getCelebrations()
                
                self.joinArray = self.instructionViewModel.getJoinedBy()
                
                self.celebrationsArray = self.celebrationsArray.sorted(by: { (Obj1, Obj2) -> Bool in
                       let Obj1_Name = Obj1.occasionId ?? ""
                       let Obj2_Name = Obj2.occasionId ?? ""
                       return (Obj1_Name.localizedCaseInsensitiveCompare(Obj2_Name) == .orderedAscending)
                    })
                
                self.joinArray = self.joinArray.sorted(by: { (Obj1, Obj2) -> Bool in
                       let Obj1_Name = Obj1.relationshipId ?? ""
                       let Obj2_Name = Obj2.relationshipId ?? ""
                       return (Obj1_Name.localizedCaseInsensitiveCompare(Obj2_Name) == .orderedAscending)
                    })
                
                self.instructionCollectionView.reloadData();
            }
            
            
        }
    }
    
    // MARK: - animateEmailTextField action
    func animateInstructionTextField(textField: UITextField, up: Bool){
        let movementDistance:CGFloat = -230
        let movementDuration: Double = 0.3
        var movement:CGFloat = 0
        if up{
            movement = movementDistance
        }else{
            movement = -movementDistance
        }
        UIView.beginAnimations(Constants.NotificationName.animateTextField, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
    
    // MARK: - instructionDoneClicked action
    @IBAction func onClickBtnBack(_ sender: Any) {
        self.dismiss(animated: true)
    }
    @IBAction func instructionDoneClicked(){
        BBQActivityIndicator.showIndicator(view: self.view)
        if let path  = selectedIndexPath1{
            let celebObj = self.celebrationsArray[path.row]
            self.selectedOccasionValue = celebObj.occasionName!
            if(self.selectedOccasions.count>0){
                self.selectedOccasions.removeAll()
            }
            self.selectedOccasions.append(celebObj.occasionId!)
        }
        if  let path1  = selectedIndexPath2 {
            let joinObj = self.joinArray[path1.row]
            if(self.selectedJoinedIds.count>0){
                self.selectedJoinedIds.removeAll()
            }
            self.selectedJoinedIds.append(joinObj.relationshipId!)
        }
        if((self.textViewSpecialRequest.text!.count>0)||(self.selectedJoinedIds.count>0)||(self.selectedOccasions.count>0)){
            self.instructionViewModel.updateSpecialInstructions(bokingId: self.selectedBookingID,
                                                                celebrationIds: self.selectedOccasions,
                                                                joinedIds: self.selectedJoinedIds,
                                                                instructiontext: self.textViewSpecialRequest.text!)
            { (isSuccess) in
                BBQActivityIndicator.hideIndicator(from: self.view)
                if(isSuccess){
                    self.dismiss(animated: true, completion: nil)
                    let dataDict = ["celebrationName": self.selectedOccasionValue]
                    
                    NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.NotificationName.specialInstructionSelected), object: nil, userInfo: dataDict)
                    
                }else{
                    // BBQSpecialInstructionViewController.swift line 143 Firebase crash

                    self.showErrorAlertWithMessage(message: BBQBookingManager.networkResponse?.message ?? kFailedNetworkMessage)
                }
            }
        }else{
            //No spacial request from user text
            let dataDict = ["celebrationName": self.selectedOccasionValue]
            NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.NotificationName.specialInstructionNotSet), object: nil, userInfo: dataDict)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    // MARK: - showErrorAlertWithMessage
    
    func showErrorAlertWithMessage(message:String) {
        ToastHandler.showToastWithMessageAndAction(message: message,
                                                   actionText: kOkButton, action: {
                                                    
        })
    }
    
    
}
extension BBQSpecialInstructionViewController:UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int{
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(section == 0){
            return celebrationsArray.count
        }else{
            return joinArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = collectionView.defaultCell(indexPath: indexPath)
        if(indexPath.section == 0){
            let celebCell = (collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CellIdentifiers.specialInstructionCell, for: indexPath) as? BBQInstructionCollectionCell)!
            
            if(celebrationsArray.count>0){
                let celebObj = celebrationsArray[indexPath.row]
                celebCell.occasionButton.setTitle(celebObj.occasionName, for: .normal)
            }
            celebCell.occasionButton.tag = indexPath.row
            celebCell.celebrationDelegate = self
            configureCelebCell(cell: celebCell, forRowAtIndexPath: indexPath as NSIndexPath)
            
            cell = celebCell
        }else{
            let joinCell = (collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CellIdentifiers.joinedInstructionCell, for: indexPath) as? BBQJoinedCollectionCell)!
            if(joinArray.count>0){
                let joinObj = joinArray[indexPath.row]
                joinCell.joinButton.setTitle(joinObj.relationshipName, for: .normal)
            }
            joinCell.joinButton.tag = indexPath.row
            joinCell.joinedDelegate = self

            self.configureJoinCellCell(cell: joinCell, forRowAtIndexPath: indexPath as NSIndexPath)
            cell = joinCell
        }
        return cell
        
    }
    
    func configureCelebCell(cell: BBQInstructionCollectionCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if selectedIndexPath1 == indexPath {
            // selected
            cell.occasionButton.backgroundColor = UIColor.lightThemeBackground
            cell.occasionButton.setTitleColor(.theme, for: .normal)
            cell.occasionButton.roundCorners(cornerRadius: 5.0, borderColor: .theme, borderWidth: 0.0)
            cell.isSelected = true
        }
        else {
            // not selected
            cell.occasionButton.roundCorners(cornerRadius: 5.0, borderColor: .text, borderWidth: 1.0)
            cell.occasionButton.backgroundColor = UIColor.white
            cell.occasionButton.setTitleColor(UIColor.text, for: .normal)
            cell.isSelected = false
        }
    }
    
    func configureJoinCellCell(cell: BBQJoinedCollectionCell, forRowAtIndexPath indexPath:  NSIndexPath){
        
        
        if selectedIndexPath2 == indexPath {
            // selected
            cell.joinButton.backgroundColor = UIColor.lightThemeBackground
            cell.joinButton.setTitleColor(.theme, for: .normal)
            cell.joinButton.roundCorners(cornerRadius: 5.0, borderColor: .theme, borderWidth: 0.0)
        }
        else {
            // not selected
            cell.joinButton.roundCorners(cornerRadius: 5.0, borderColor: .text, borderWidth: 1.0)
            cell.joinButton.backgroundColor = UIColor.white
            cell.joinButton.setTitleColor(UIColor.text, for: .normal)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        return CGSize(width: collectionView.frame.width, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        var headerView = UICollectionReusableView()
        switch kind{
        case UICollectionView.elementKindSectionHeader:
            let instHeaderView:BBQInstructionHeaderView = (collectionView.dequeueReusableSupplementaryView(
                ofKind: kind,
                withReuseIdentifier:Constants.CellIdentifiers.specialInstructionHeader,
                for: indexPath) as? BBQInstructionHeaderView)!
            if(indexPath.section == 0 ){
                instHeaderView.headerTitleLabel.text = kCelebratingTitle
            }else{
                instHeaderView.headerTitleLabel.text = kJoinedByTitle
            }
            headerView = instHeaderView
            return headerView
        default:
            assert(false, "")
            
        }
        return headerView
    }
    
}
extension BBQSpecialInstructionViewController:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var  width = 0
        if(indexPath.section == 0){
            let celebObj = self.celebrationsArray[indexPath.row]
            
            width = Int(celebObj.occasionName!.width(withConstrainedHeight: 33,
                                                     font: UIFont.appThemeRegularWith(size: 16.0)))
        }else{
            let joinObj = self.joinArray[indexPath.row]
            
            width = Int(joinObj.relationshipName!.width(withConstrainedHeight: 33,
                                                        font: UIFont.appThemeRegularWith(size:16.0)))
        }
        let height = 36 //ratio
        return CGSize(width: width+27, height: height)
    }
    
}

// MARK: - UICollectionViewDelegate methods

//extension BBQSpecialInstructionViewController:UICollectionViewDelegate{
//    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        
//        if(indexPath.section == 0){
//            if selectedIndexPath1 == indexPath as NSIndexPath {
//                // selected same cell -> deselect all
//                selectedIndexPath1 = nil
//            }else {
//                // select different cell
//                let oldSelectedIndexPath = selectedIndexPath1
//                selectedIndexPath1 = indexPath as NSIndexPath
//                
//                // refresh old cell to clear old selection indicators
//                var _: BBQInstructionCollectionCell?
//                if let previousSelectedIndexPath = oldSelectedIndexPath {
//                    if let previousSelectedCell = collectionView.cellForItem(at: previousSelectedIndexPath as IndexPath) {
//                        configureCelebCell(cell: previousSelectedCell as! BBQInstructionCollectionCell, forRowAtIndexPath: previousSelectedIndexPath)
//                    }
//                }
//            }
//            
//            let selectedCell = collectionView.cellForItem(at: indexPath)!
//            configureCelebCell(cell: selectedCell as! BBQInstructionCollectionCell, forRowAtIndexPath: indexPath as NSIndexPath)
//            
//        }else{
//            if selectedIndexPath2 == indexPath as NSIndexPath {
//                // selected same cell -> deselect all
//                selectedIndexPath2 = nil
//            }else {
//                // select different cell
//                let oldSelectedIndexPath = selectedIndexPath2
//                selectedIndexPath2 = indexPath as NSIndexPath
//                
//                // refresh old cell to clear old selection indicators
//                var _: BBQJoinedCollectionCell?
//                if let previousSelectedIndexPath = oldSelectedIndexPath {
//                    if let previousSelectedCell = collectionView.cellForItem(at: previousSelectedIndexPath as IndexPath) {
//                        configureJoinCellCell(cell: previousSelectedCell as! BBQJoinedCollectionCell, forRowAtIndexPath: previousSelectedIndexPath)
//                    }
//                }
//            }
//            let selectedCell = collectionView.cellForItem(at: indexPath)!
//            configureJoinCellCell(cell: selectedCell as! BBQJoinedCollectionCell, forRowAtIndexPath: indexPath as NSIndexPath)
//            
//        }
//        
//        
//    }
//    
//    
//}
// MARK: - UITextFieldDelegate methods
extension BBQSpecialInstructionViewController:UITextViewDelegate{
//    textview
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
//        specialInstrnTextField.resignFirstResponder()
//        return true
//    }
//    func textFieldDidBeginEditing(_ textField: UITextField){
//
//        self.animateInstructionTextField(textField: textField, up:true)
//    }
//
//    func textFieldDidEndEditing(_ textField: UITextField){
//
//        self.animateInstructionTextField(textField: textField, up:false)
//    }
//
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        return textField.text!.count + (string.count - range.length) <= 160
//
//    }
//
}

extension BBQSpecialInstructionViewController:BBQInstructionProtocol{
    func didOccasionSelected(currentIndex: NSIndexPath) {
        if currentIndex.section == 0{
            if selectedIndexPath1 == currentIndex as NSIndexPath {
                // selected same cell -> deselect all
                selectedIndexPath1 = nil
            }else {
                // select different cell
                let oldSelectedIndexPath = selectedIndexPath1
                selectedIndexPath1 = currentIndex as NSIndexPath
                
                // refresh old cell to clear old selection indicators
                var _: BBQInstructionCollectionCell?
                if let previousSelectedIndexPath = oldSelectedIndexPath {
                    if let previousSelectedCell = instructionCollectionView.cellForItem(at: previousSelectedIndexPath as IndexPath) {
                        configureCelebCell(cell: previousSelectedCell as! BBQInstructionCollectionCell, forRowAtIndexPath: previousSelectedIndexPath)
                    }
                }
            }
            
            let selectedCell = instructionCollectionView.cellForItem(at: currentIndex as IndexPath)!
            configureCelebCell(cell: selectedCell as! BBQInstructionCollectionCell, forRowAtIndexPath: currentIndex as NSIndexPath)
            
        }
    }
}
extension BBQSpecialInstructionViewController:BBQJoinCellProtocol{
    func didJoinedSelected(currentIndex: NSIndexPath) {
        if currentIndex.section == 1{
            if selectedIndexPath2 == currentIndex as NSIndexPath {
                // selected same cell -> deselect all
                selectedIndexPath2 = nil
            }else {
                // select different cell
                let oldSelectedIndexPath = selectedIndexPath2
                selectedIndexPath2 = currentIndex as NSIndexPath
                
                // refresh old cell to clear old selection indicators
                if let previousSelectedIndexPath = oldSelectedIndexPath {
                    if let previousSelectedCell = instructionCollectionView.cellForItem(at: previousSelectedIndexPath as IndexPath) {
                        configureJoinCellCell(cell: previousSelectedCell as! BBQJoinedCollectionCell, forRowAtIndexPath: previousSelectedIndexPath)
                    }
                }
            }
            let selectedCell = instructionCollectionView.cellForItem(at: currentIndex as IndexPath)!
            configureJoinCellCell(cell: selectedCell as! BBQJoinedCollectionCell, forRowAtIndexPath: currentIndex as NSIndexPath)
            
        }
    }
}
    

    
    
