//
//  BottomSheetCalendarViewController.swift
//  BBQApp
//
//  Created by Sridhar on 31/07/19.
//  Copyright © 2019 Sridhar. All rights reserved.
//

import UIKit
import FSCalendar

protocol BottomSheetCalendarProtocol: AnyObject {
    func updateDateFromCalendar(originalDate: String, formattedDate: String)
}

class BottomSheetCalendarViewController: UIViewController, FSCalendarDelegate, FSCalendarDataSource {
    
    @IBOutlet weak var calendarView: UIView!
    @IBOutlet weak var selectedDateLabel: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    
    private weak var calendar: FSCalendar!
    private var selectedDate: String = ""
    private var selectedformattedDate: String = ""
    
    weak var delegate: BottomSheetCalendarProtocol? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        self.calendarView.layoutIfNeeded()
        let calendar = FSCalendar(frame: CGRect(x: 0, y: 0,
                                                width: calendarView.frame.width,
                                                height: calendarView.frame.height))
        calendar.appearance.selectionColor = UIColor.theme.withAlphaComponent(0.1)
        calendar.appearance.titleSelectionColor = UIColor.theme
        calendar.appearance.headerTitleColor = UIColor.text
        calendar.appearance.weekdayTextColor = UIColor.text
        calendar.appearance.borderRadius = 5.0
        calendar.appearance.todayColor = UIColor.clear
        calendar.appearance.titleTodayColor = UIColor.black
        calendar.dataSource = self
        calendar.delegate = self
        calendar.select(calendar.today)
        calendar.backgroundColor = UIColor.white
        calendar.pinEdgesToSuperView()
        self.calendarView.addSubview(calendar)
        self.calendar = calendar
        
        //Date formatter to show date like 01 April
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = Constants.BBQBookingStatus.calendarDateTimeFormat
        let currentselectedDate = dateformatter.string(from: calendar.today!)
        
        self.selectedDateLabel.text = currentselectedDate
        self.doneButton.roundCorners(cornerRadius: 5, borderColor: .clear, borderWidth: 0)
        self.doneButton.backgroundColor = .theme
    }
    
    func maximumDate(for calendar: FSCalendar) -> Date {
        var confirmDate:Date?
        confirmDate = Date()
        let nextMonth = confirmDate?.nextMonths
        confirmDate = nextMonth
        return confirmDate!
    }
    
    func minimumDate(for calendar: FSCalendar) -> Date {
        return calendar.today!
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if monthPosition == .previous || monthPosition == .next {
            calendar.setCurrentPage(date, animated: true)
        }
        
        //Date formatter to show date like 01 April
        let dateformatterToDisplay = DateFormatter()
        
        dateformatterToDisplay.dateFormat = Constants.BBQBookingStatus.calendarDateTimeFormat
        let currentselectedDate = dateformatterToDisplay.string(from: calendar.selectedDate!)
        
        self.selectedDateLabel.text = currentselectedDate
        
        dateformatterToDisplay.dateFormat = Constants.BBQBookingStatus.todaytomorrowDateTimeFormat
        let storedselectedDateForAPI = dateformatterToDisplay.string(from: calendar.selectedDate!)
        self.selectedformattedDate = storedselectedDateForAPI
        
        dateformatterToDisplay.dateFormat = Constants.BBQBookingStatus.todaytomorrowDisplayTimeFormat
        let storedselectedDate = dateformatterToDisplay.string(from: calendar.selectedDate!)
        self.selectedDate = storedselectedDate
    }
    
    func setTodayDateForDisplay() {
        //Date formatter to show date like 01 April
        let dateformatterToDisplay = DateFormatter()
        
        dateformatterToDisplay.dateFormat = Constants.BBQBookingStatus.calendarDateTimeFormat
        let currentselectedDate = dateformatterToDisplay.string(from: calendar.today!)
        
        self.selectedDateLabel.text = currentselectedDate
        
        dateformatterToDisplay.dateFormat = Constants.BBQBookingStatus.todaytomorrowDateTimeFormat
        let storedselectedDateForAPI = dateformatterToDisplay.string(from: calendar.today!)
        self.selectedformattedDate = storedselectedDateForAPI
        
        dateformatterToDisplay.dateFormat = Constants.BBQBookingStatus.todaytomorrowDisplayTimeFormat
        let storedselectedDate = dateformatterToDisplay.string(from: calendar.today!)
        
        self.selectedDate = storedselectedDate
    }

    @IBAction func donePressed(_ sender: Any) {
        
        if self.selectedDate == "" {
            self.setTodayDateForDisplay()
        }
        
        if let delegate = self.delegate {
            delegate.updateDateFromCalendar(originalDate: self.selectedDate, formattedDate: self.selectedformattedDate)
        }
    
        self.dismiss(animated: true, completion: nil)
    }
}
