//
 //  Created by Ajith CP on 15/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQBookingNavigationTableViewCell.swift
 //

import UIKit

protocol BBQBookingNavigationTableViewCellDelegate : AnyObject {
    
    func didPressedNavigationButton(isNavigateBack: Bool)
}

class BBQBookingNavigationTableViewCell: UITableViewCell {
    
    weak var navigationDelegate : BBQBookingNavigationTableViewCellDelegate?
   
    @IBOutlet weak var navigateButton: UIButton!
    
    private var isTimeSlotReady : Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    // MARK: Public Methods
    
    func loadNavigationItem(isTimeSlotReady: Bool) {
        self.isTimeSlotReady = isTimeSlotReady
        //let itemImageName = isTimeSlotReady ? "icon_previous_orange" : "icon_next_orange"
        let itemImageName = isTimeSlotReady ? "Back" : "Next"
        // TODO: Move to VM
        self.navigateButton.setTitle(itemImageName, for: .normal)
    }
    
    // MARK: IBAction Methods
    
    @IBAction func navigateButtonPressed(_ sender: Any) {
        self.isTimeSlotReady = !self.isTimeSlotReady
        self.navigationDelegate?.didPressedNavigationButton(isNavigateBack: self.isTimeSlotReady)
    }
    
}
