//
//  Created by Ajith CP on 15/10/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQBuffetSummaryTableViewCell.swift
//

import UIKit

protocol BBQBuffetSummaryTableViewCellDelegate : AnyObject {
    
    func didChangeBuffetSummaryContent(with contentSize: CGSize)
}

class BBQBuffetSummaryTableViewCell: UITableViewCell {
    
    weak var buffetDelegate : BBQBuffetSummaryTableViewCellDelegate?
    weak var buffetItemDelegate : BBQBuffetEntryTableViewCellDelegate?
    
    private var buffetSummaryVM : BBQBookingBuffetsViewModel?
    private var matchedBuffetIds: [Json]? = nil
    var arrAddedItems = [Int]()
    
    @IBOutlet weak var buffetTableview: UITableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupBuffetTable()
        
    }
    
    // MARK: Setup Methods
    
    private func setupBuffetTable() {
        
        self.buffetTableview.delegate = self
        self.buffetTableview.dataSource = self
        
        self.buffetTableview.estimatedRowHeight = 70
        self.buffetTableview.rowHeight = UITableView.automaticDimension
        
        self.buffetTableview.register(UINib(nibName: "BBQBuffetEntryTableViewCell",
                                            bundle: nil),
                                      forCellReuseIdentifier: "BBQBuffetEntryTableViewCellID")
    }
    
    // MARK: Public Methods
    
    func loadBuffettDetails(with viewModel: BBQBookingBuffetsViewModel, matchedBuffetsData: [Json]? = nil) {
        self.matchedBuffetIds = matchedBuffetsData
        self.buffetSummaryVM = viewModel
        if let buffetVM = self.buffetSummaryVM, let buffetItems = buffetVM.buffetData, arrAddedItems.count <= buffetItems.count {
            for _ in arrAddedItems.count..<buffetItems.count{
                arrAddedItems.append(0)
            }
        }
        self.buffetTableview.reloadData {
            self.layoutIfNeeded()
            self.buffetDelegate?.didChangeBuffetSummaryContent(with: self.buffetTableview.contentSize)
        }
    }
    
}

extension BBQBuffetSummaryTableViewCell : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let buffetVM = self.buffetSummaryVM, let buffetItems = buffetVM.buffetData {
            return buffetItems.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let buffetItemCell =
            self.buffetTableview.dequeueReusableCell(withIdentifier: "BBQBuffetEntryTableViewCellID")
                as! BBQBuffetEntryTableViewCell
        buffetItemCell.buffetItemDelegate = self.buffetItemDelegate
        buffetItemCell.updateDelegate = self
        buffetItemCell.tag = indexPath.row
        buffetItemCell.index = indexPath.row
        buffetItemCell.itemQuantity = arrAddedItems[indexPath.row]
        if let buffetVM = self.buffetSummaryVM, let buffetItems = buffetVM.buffetData {
            //let packCount = self.getMatchedDataPacks(buffetData: buffetItems[indexPath.row])
            buffetItemCell.loadBuffetEntryDetails(with: buffetItems[indexPath.row], menuId: buffetVM.menuId ?? "")
            buffetItemCell.itemQuantity = arrAddedItems[indexPath.row]
        }
        
        return buffetItemCell
    }
    
    /*private func getMatchedDataPacks(buffetData: BuffetData) -> Int {
        var count = 0
        if let currentBuffetId = buffetData.buffetId, let matchedBuffet = self.matchedBuffetIds {
            for (index, buffetsData) in matchedBuffet.enumerated() {
                if let value = buffetsData[currentBuffetId] {
                    count = value as! Int
                    self.matchedBuffetIds?.remove(at: index)
                    break
                }
            }
        }
        return count
    }*/
}
extension BBQBuffetSummaryTableViewCell: BBQBuffetEntryTableViewCellUpdateDelegate{
    func didUpdateBookingDetailsBody(bookingDetailsBody: Int?, index: Int?) {
        if let index = index, let bookingDetailsBody = bookingDetailsBody, arrAddedItems.count > index {
            arrAddedItems[index] = bookingDetailsBody
        }
    }
}
