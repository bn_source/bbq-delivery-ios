//
 //  Created by Ajith CP on 15/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQBuffetEntryTableViewCell.swift
 //

import UIKit

protocol BBQBuffetEntryTableViewCellDelegate : AnyObject {
   
    func didAddQuantity(in entryCell: BBQBuffetEntryTableViewCell, itemTotal: Double, bookingDetailsBodyData: BookingDetailsBody?)
    
    func didRemoveQuantity(in entryCell: BBQBuffetEntryTableViewCell, itemTotal: Double, bookingDetailsBodyData: BookingDetailsBody?)
    
    func didPressedMenuButton(in entryCell: BBQBuffetEntryTableViewCell)
}

protocol BBQBuffetEntryTableViewCellUpdateDelegate: AnyObject {
    func didUpdateBookingDetailsBody(bookingDetailsBody: Int?, index: Int?)
}

class BBQBuffetEntryTableViewCell: UITableViewCell {
    
    weak var buffetItemDelegate : BBQBuffetEntryTableViewCellDelegate?
    weak var updateDelegate: BBQBuffetEntryTableViewCellUpdateDelegate?
    var buffetDetails:Buffetdata?
    @IBOutlet weak var foodTypeImgView: UIImageView!
    
    @IBOutlet weak var foodTypeLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var itemAmountLabel: UILabel!
    @IBOutlet weak var totalAmountLabel: UILabel!
    
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var menuButton: UIButton!

    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var viewForQty: UIView!
    
    private var itemPrice : Double = 0.00
    private var itemCurrency : String = "₹"
    
    private var bookingDetailsBodyData: BookingDetailsBody?
    private var menuId: String = ""
    private var buffetId: String = ""
    private var buffetType :  String = "'"
    private var customerType: String = ""
    var index: Int?
    var itemQuantity: Int = 0 {
        didSet {
            self.quantityLabel.text = "\(itemQuantity)"
            
            self.totalItemAmount  = itemPrice * Double(itemQuantity)
            self.totalAmountLabel.text = "\(self.itemCurrency) \(String(format: "%.2f",self.totalItemAmount))"
        }
    }
    
    private var totalItemAmount : Double = 0.0 {
        didSet {

        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupBuffetItemCell()
    }
    
    // MARK: Setup Methods
    
    private func setupBuffetItemCell() {
        self.itemQuantity = 0
        self.quantityLabel.text = "\(itemQuantity)"
        
        _ = UITapGestureRecognizer(target: self, action: #selector(self.didPressMenuLabel))
        viewForQty.layer.cornerRadius = 5
        viewForQty.layer.shadowColor = #colorLiteral(red: 0.5141925812, green: 0.5142051578, blue: 0.5141984224, alpha: 1)
        viewForQty.layer.shadowOffset = CGSize(width: 0, height: 3)
        viewForQty.layer.shadowOpacity = 0.4
        viewForQty.layer.shadowRadius = 4
//        self.foodTypeLabel.addGestureRecognizer(tapGesture)
    }
    
    @objc private func didPressMenuLabel() {
        self.buffetItemDelegate?.didPressedMenuButton(in: self)
    }
    
    private func constructBookingDetailPayload(itemQuantity: Int) {
        let packs = String(format: "%d", itemQuantity)
        self.bookingDetailsBodyData = BookingDetailsBody(menuId: self.menuId, buffetId: self.buffetId, packs: packs , buffetType: self.buffetType, customerType:self.customerType)
        updateDelegate?.didUpdateBookingDetailsBody(bookingDetailsBody: itemQuantity, index: index)
    }
    
    func resetBuffetQuantity(isAdd: Bool) {
        if isAdd {
            self.itemQuantity -= 1
        } else {
            self.itemQuantity += 1
        }
    }
    
    /*private func initializeQuatityForReschedule(packsCount: Int) {
        for _ in 0..<packsCount {
            self.addButtonPressed(self.plusButton)
        }
    }*/
    
    // MARK: Public Methods
    
    func loadBuffetEntryDetails(with buffetItem: Buffetdata, menuId: String) {
        print(buffetItem)
        self.menuId = menuId
        self.buffetId = "\(buffetItem.buffetId)" ?? ""
        self.itemPrice = buffetItem.buffetPrice ?? 0
        self.buffetType = buffetItem.buffetType ?? ""
        self.customerType = buffetItem.customerType ?? ""
        if let currency = buffetItem.buffetCurrency?.currencySymbol() {
            self.itemCurrency = currency
        }
        self.foodTypeLabel.text = buffetItem.buffetType
        if buffetItem.customerType == "Child"{
            self.foodTypeLabel.text = "Kid (6-9 years)"
        }
        self.itemAmountLabel.text = "\(String(format: "%.2f", buffetItem.buffetPrice ?? 0))"
        
        let isVegetarian = buffetItem.buffetType == "VEG"
        if isVegetarian {
            let _ = UIColor.hexStringToUIColor(hex: "68C700")
            self.foodTypeImgView.image = UIImage.init(named: "vegIcon")
        } else {
            self.foodTypeImgView.image = UIImage.init(named: "nonvegIcon")
        }
        
        if buffetItem.customerType == "Child"{
            self.foodTypeImgView.image = UIImage.init(named: "kids_buffet_icon")
        }
        
        /*if packsCount > 0, itemQuantity == 0 {
            self.initializeQuatityForReschedule(packsCount: packsCount)
        }*/
    }

    // MARK: IBActions Methods

    @IBAction func minusButtonPressed(_ sender: UIButton) {
        if self.itemQuantity != 0 {
            self.itemQuantity -= 1
            self.constructBookingDetailPayload(itemQuantity: self.itemQuantity)
            self.buffetItemDelegate?.didRemoveQuantity(in: self, itemTotal: self.itemPrice, bookingDetailsBodyData: self.bookingDetailsBodyData)
            return
        }
    }
    
    @IBAction func addButtonPressed(_ sender: UIButton) {
        self.itemQuantity += 1
        self.constructBookingDetailPayload(itemQuantity: self.itemQuantity)
        self.buffetItemDelegate?.didAddQuantity(in: self, itemTotal: self.itemPrice, bookingDetailsBodyData: self.bookingDetailsBodyData)
    }
    
    @IBAction func menuButtonPressed(_ sender: UIButton) {
        self.buffetItemDelegate?.didPressedMenuButton(in: self)
    }
}
