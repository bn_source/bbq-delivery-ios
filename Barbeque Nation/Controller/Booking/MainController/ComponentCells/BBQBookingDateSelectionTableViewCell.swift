//
 //  Created by Ajith CP on 15/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQBookingDateSelectionTableViewCell.swift
 //

import UIKit

enum BBQBookingDateType : String {
    case Today = "Today"
    case Tomorrow = "Tomorrow"
    case Custom = "Pick a date"
    
    func triggerBookingEvent() {
        switch self {
        case .Today:
            AnalyticsHelper.shared.triggerEvent(type: .R03A)
            break
        case .Tomorrow:
            AnalyticsHelper.shared.triggerEvent(type: .R03B)
            break
        case .Custom:
            AnalyticsHelper.shared.triggerEvent(type: .R03C)
            break
        }
    }
}

protocol BBQBookingDateSelectionTableViewCellDelegate : AnyObject {
   
    func didSelectBokkingDateType(with type: BBQBookingDateType)
    func didSelectBokkingTimeSlot(with time: String, slotId: Int)
}

class BBQBookingDateSelectionTableViewCell: UITableViewCell {
    @IBOutlet weak var tomorrowButton: UIButton!
    @IBOutlet weak var todayButton: UIButton!
    @IBOutlet weak var customDateButton: UIButton!
    @IBOutlet weak var timeSlotTableView: UITableView!
    @IBOutlet weak var lblToday: UILabel!
    @IBOutlet weak var lblTodayDate: UILabel!
    @IBOutlet weak var lblTomorrow: UILabel!
    @IBOutlet weak var lblTomorrowDate: UILabel!
    @IBOutlet weak var lblPickDate: UILabel!
    @IBOutlet weak var lblPickDateValue: UILabel!
    @IBOutlet weak var imgCalendar: UIImageView!
    
    weak var dateSelctionDelegate : BBQBookingDateSelectionTableViewCellDelegate?
    
    private var dateType : BBQBookingDateType?
    private var isRefreshRequired : Bool = true
    private var timeSelected : String?
    
    var slotsViewModel: SlotsViewModel? = nil {
        didSet {
            timeSlotTableView.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.setupTimeSlotTableView()
        self.setupTextForCustomDateButton()
    }

    // MARK: Setup Methods
    
    private func setupTimeSlotTableView() {
        self.timeSlotTableView.delegate = self
        self.timeSlotTableView.dataSource = self
        
        self.timeSlotTableView.estimatedRowHeight = 300
        self.timeSlotTableView.rowHeight = UITableView.automaticDimension
        self.timeSlotTableView.allowsSelection = true
        self.timeSlotTableView.allowsSelectionDuringEditing = true
        
        self.timeSlotTableView.register(UINib(nibName: "BBQBookingTimeSlotTableViewCell",
                                             bundle: nil),
                                       forCellReuseIdentifier: "BBQBookingTimeSlotTableViewCellID")
        
        lblTodayDate.text = Date().string(format: "dd")
        lblTomorrowDate.text = Date().dayAfter.string(format: "dd")
    }
    
    private func setupTextForCustomDateButton(isSelected: Bool = false) {
//        var textFont = UIFont.appThemeSemiBoldWith(size: 14.0)
//        var textColor = UIColor.black
//        if isSelected {
//            textFont = UIFont.appThemeBoldWith(size: 14.0)
//            textColor = UIColor.theme
//        }
//        let attribute = [NSAttributedString.Key.font: textFont,
//                         NSAttributedString.Key.foregroundColor: textColor]
//        let fullString = NSMutableAttributedString(string: "Pick a date ", attributes: attribute)
//
//        // create our NSTextAttachment
//        if isSelected {
//            imgCalendar.image = UIImage(named: "icon_calendar_theme_tick")
//        } else {
//            imgCalendar.image = UIImage(named: "icon_calendar_big")
//        }
//
//
//        // wrap the attachment in its own attributed string so we can append it
//        let imageString = NSMutableAttributedString(attachment: imageAttachment)
//
//        // add the NSTextAttachment wrapper to our full string, then add some more text.
//        fullString.append(imageString)
//        customDateButton.setAttributedTitle(fullString, for: .normal)
    }
    
    private func setupTimeSlotCell() {
        var selectedButton = self.todayButton
        if self.dateType == BBQBookingDateType.Tomorrow {
            selectedButton = self.tomorrowButton
        }
        if self.dateType == BBQBookingDateType.Custom {
            selectedButton = self.customDateButton
        }
        self.colorDateSelectionButtons(for: selectedButton ?? self.todayButton)
    }
    
    // MARK: Public Methods
    
    func loadTimeSlotController(with dateType: BBQBookingDateType,
                                slotsModel: SlotsViewModel,
                                timeString: String,
                                delegate: BBQBookingDateSelectionTableViewCellDelegate,
                                isPrefillRequired: Bool = false) {
        self.dateSelctionDelegate = delegate
        self.dateType = dateType
        self.timeSelected = timeString
        
        if isPrefillRequired {
            //Used for resschedule. If user selects the same date, pre-populate the slots selected.
            self.isRefreshRequired = true
            self.timeSlotTableView.reloadData { }
        }
        
        if dateType == .Custom {
//            self.setupTextForCustomDateButton(isSelected: true)
            self.setupTextForCustomDateButton()
        }
        self.slotsViewModel = slotsModel
        self.setupTimeSlotCell()
    }

    // MARK: IBAction Methods
    
    @IBAction func todayButtonPressed(_ sender: UIButton) {
        self.colorDateSelectionButtons(for: sender)
        self.setupTextForCustomDateButton()
        self.slotsViewModel?.dateType = .Today
        self.dateSelctionDelegate?.didSelectBokkingDateType(with: .Today)
    }
    
    @IBAction func tomorrowButtonPressed(_ sender: UIButton) {
        self.colorDateSelectionButtons(for: sender)
        self.setupTextForCustomDateButton()
        self.slotsViewModel?.dateType = .Tomorrow
        self.dateSelctionDelegate?.didSelectBokkingDateType(with: .Tomorrow)
    }
    
    @IBAction func customDateButtonPrssed(_ sender: UIButton) {
       // self.colorDateSelectionButtons(for: sender)
        self.slotsViewModel?.dateType = .Custom
        self.dateSelctionDelegate?.didSelectBokkingDateType(with: .Custom)
    }
    
    // MARK: Private Methods
    
    private func colorDateSelectionButtons(for button: UIButton) {
        
//        self.todayButton.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
//        self.tomorrowButton.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
//        self.customDateButton.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
//        self.todayButton.setTitleColor(UIColor.black, for: .normal)
//        self.tomorrowButton.setTitleColor(UIColor.black, for: .normal)
//        self.customDateButton.setTitleColor(UIColor.black, for: .normal)
//
//        button.backgroundColor = UIColor.menuOrangeColor.withAlphaComponent(0.1)
//        button.setTitleColor(.menuOrangeColor, for: .normal)
//        button.titleLabel?.font = UIFont.appThemeBoldWith(size: 14.0)
        
        setUnselectedButton(label1: lblToday, label2: lblTodayDate, button: todayButton)
        setUnselectedButton(label1: lblTomorrow, label2: lblTomorrowDate, button: tomorrowButton)
        setUnselectedButton(label1: lblPickDate, label2: lblPickDateValue, button: customDateButton)
        imgCalendar.image = UIImage(named: "icon_calendar_black")
        
        if button == todayButton{
            setSelectedButton(label1: lblToday, label2: lblTodayDate, button: button)
        }else if button == tomorrowButton{
            setSelectedButton(label1: lblTomorrow, label2: lblTomorrowDate, button: button)
        }else if button == customDateButton{
            setSelectedButton(label1: lblPickDate, label2: lblPickDateValue, button: button)
            imgCalendar.image = UIImage(named: "icon_calendar_theme_tick")
        }
    }
    
    private func setSelectedButton(label1: UILabel, label2: UILabel, button: UIButton) {
        button.backgroundColor = UIColor.menuOrangeColor.withAlphaComponent(0.1)
        label1.textColor = .menuOrangeColor
        label2.textColor = .menuOrangeColor
        button.roundCorners(cornerRadius: 5.0, borderColor: .menuOrangeColor, borderWidth: 1.0)
    }
    
    private func setUnselectedButton(label1: UILabel, label2: UILabel, button: UIButton) {
        button.backgroundColor = UIColor.text.withAlphaComponent(0.1)
        label1.textColor = .text
        label2.textColor = .text
        button.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 1.0)
    }
    
    private func refreshTimeSlotTable() {
        self.isRefreshRequired = true
        BBQBookingTimeSlotTableViewCell.selectedIndex = nil
        self.timeSlotTableView.reloadData { }
    }
  
}

extension BBQBookingDateSelectionTableViewCell : UITableViewDelegate, UITableViewDataSource,
BBQBookingTimeSlotTableViewCellDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let slotsVM = self.slotsViewModel {
            return slotsVM.availableSlotsData.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let timeslotCell =
            self.timeSlotTableView.dequeueReusableCell(withIdentifier: "BBQBookingTimeSlotTableViewCellID")
                as! BBQBookingTimeSlotTableViewCell
        
        if let slotsVM = self.slotsViewModel {
            if slotsVM.availableSlotsData.count <= indexPath.row{
                return UITableViewCell()
            }
            
            guard  let index = slotsVM.availableSlotsData[indexPath.row].slotId else{
                return UITableViewCell()
            }
            
            timeslotCell.timeSlotButton.tag = index
            timeslotCell.timeslotDelegate = self
            timeslotCell.loadBookingTimeSlotCell(indexPath: indexPath.row,
                                                 isRefreshRequired: self.isRefreshRequired,
                                                 timeSelected: self.timeSelected ?? "",
                                                 slotAvailable: slotsVM.availableSlotsData[indexPath.row])
        }
        
        return timeslotCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }

    
    func timeSlotSelected(for index: Int, dateString: String) {
        self.isRefreshRequired = true
//        self.timeSlotTableView.reloadData {
            self.dateSelctionDelegate?.didSelectBokkingTimeSlot(with: dateString, slotId: index)
       // }
    }
    
    func defaultSlotSelected(for index: Int, dateString: String) {
        self.dateSelctionDelegate?.didSelectBokkingTimeSlot(with: dateString, slotId: index)
    }
    
}
