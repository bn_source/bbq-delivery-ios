//
 //  Created by Ajith CP on 23/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQTermsNConditionsTableViewCell.swift
 //

import UIKit

class BBQTermsNConditionsTableViewCell: UITableViewCell {

    @IBOutlet weak var termsTextLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: Public Methods
    
    func loadTermsData(termsString: String, for index: Int) {
        self.termsTextLabel.text = "\(index + 1). \(termsString)"
    }
    
}
