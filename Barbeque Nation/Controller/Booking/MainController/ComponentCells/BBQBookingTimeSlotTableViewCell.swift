//
 //  Created by Ajith CP on 16/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQBookingTimeSlotTableViewCell.swift
 //

import UIKit

protocol BBQBookingTimeSlotTableViewCellDelegate : AnyObject {
  
    func timeSlotSelected(for index: Int, dateString: String)
}

class BBQBookingTimeSlotTableViewCell: UITableViewCell {
    
    weak var timeslotDelegate : BBQBookingTimeSlotTableViewCellDelegate?
   
    @IBOutlet weak var timeSlotButton: UIButton!
    
    static var selectedIndex : Int?
    static var selectedSlotIndex : Int?
    var isSlotAvailable = true
    
    // MARK: Life Cycle Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    // MARK: IBAction Methods
    
    @IBAction func timeSlotButtonPressed(_ sender: UIButton) {
        if !isSlotAvailable{
            ToastHandler.showToastWithMessage(message: "No booking available for this slot!!")
            return
        }
        
        BBQBookingTimeSlotTableViewCell.selectedIndex = sender.tag
        self.timeslotDelegate?.timeSlotSelected(for: sender.tag,
                                                dateString: sender.titleLabel?.text ?? "")
    }
    
    // MARK: Public Methods
    
    func loadBookingTimeSlotCell(indexPath: Int,
                                 isRefreshRequired: Bool,
                                 timeSelected: String,
                                 slotAvailable: SlotsAvailable) {
        
        let slotTime = slotAvailable.slotStartTime ?? ""
        
       // #if DEBUG
        print("SELECTED TIME: XXXXX", timeSelected)
        
        
       // #endif
        
        let formattedTime = BBQBookingUtilies.shared.getDisplayTimeIn12HourFormat(slotTime)
        self.timeSlotButton.setTitle(formattedTime, for: .normal)
        
        self.isUserInteractionEnabled = true
        isSlotAvailable = true
        let timeString = self.timeSlotButton.titleLabel?.text
        timeSlotButton.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 1.0)
        if timeString == timeSelected && isRefreshRequired {
            
            BBQBookingTimeSlotTableViewCell.selectedSlotIndex = indexPath
            
            self.timeSlotButton.backgroundColor = .menuOrangeColor.withAlphaComponent(0.1)
            self.timeSlotButton.setTitleColor(.menuOrangeColor, for: .normal)
            self.timeSlotButton.titleLabel?.font = UIFont.appThemeRegularWith(size: 14.0)
            timeSlotButton.roundCorners(cornerRadius: 5.0, borderColor: .menuOrangeColor, borderWidth: 1.0)
        } else {
            self.timeSlotButton.backgroundColor = .lightGray.withAlphaComponent(0.1)
            self.timeSlotButton.setTitleColor(.darkGray, for: .normal)
            self.timeSlotButton.titleLabel?.font = UIFont.appThemeRegularWith(size: 14.0)
        }
        
        if slotAvailable.availableCapacity ?? 0 <= 0{
            // Disabling slots those are not available for booking
            self.timeSlotButton.backgroundColor = .lightGray.withAlphaComponent(0.1)
            self.timeSlotButton.setTitleColor(.lightGray, for: .normal)
            self.timeSlotButton.titleLabel?.font = UIFont.appThemeRegularWith(size: 14.0)
            isSlotAvailable = false
        }
        
    }
    
}
