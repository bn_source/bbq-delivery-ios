//
 //  Created by Ajith CP on 23/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQTermsAndConditionsViewController.swift
 //

import UIKit

class BBQTermsAndConditionsViewController: UIViewController {
    
    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var moreButton: UIButton!
    
    @IBOutlet weak var termsTableView: UITableView!
    
    @IBOutlet weak var contentViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentHeightConstraint: NSLayoutConstraint!
    
    private var corporateTermsList : [String]?
    private var showMoreItems : Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupTermsComponent()
        
        self.setupTermsTableView()
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.adjustComponentConstraints(isExpand: false)
    }
    
    // MARK: Setup Methods
    
    private func setupTermsComponent() {
        self.overlayView.blurView(with: 0.7)
        self.contentView.roundCorners(cornerRadius: 15.0, borderColor: .clear, borderWidth: 0.0)
    }
    
    private func setupTermsTableView() {
        self.termsTableView.rowHeight = UITableView.automaticDimension
        self.termsTableView.register(UINib(nibName: "BBQTermsNConditionsTableViewCell",
                                             bundle: nil),
                                       forCellReuseIdentifier: "BBQTermsNConditionsTableViewCellID")
    }
    
    // MARK: IBAction Methods
    
    @IBAction func closeButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func moreButtonPressed(_ sender: Any) {
        self.showMoreItems = true
        self.moreButton.isHidden = true
        let _ = self.moreButton.heightAnchor.constraint(equalToConstant: 0)
        self.termsTableView.reloadData {
            self.adjustComponentConstraints(isExpand: self.showMoreItems)
        }
    }
    
    // MARK: Private Methods
    
    private func adjustComponentConstraints(isExpand: Bool) {
        UIView.animate(withDuration: 0.3, animations: {
            self.contentHeightConstraint.constant = self.termsTableView.contentSize.height + 112
            self.contentHeightConstraint.isActive = true
            self.contentHeightConstraint.priority = UILayoutPriority.required
            self.updateViewConstraints()
        })
    }
    
    // MARK: Public Methods
    
    func loadTermsControllerWith(termsData: [String]) {
        self.corporateTermsList = termsData
        self.termsTableView.reloadData {
            self.adjustComponentConstraints(isExpand: self.showMoreItems)
        }
    }
    

}

extension BBQTermsAndConditionsViewController : UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if let termsList = self.corporateTermsList, termsList.count > 0 {
            if showMoreItems {
                return termsList.count
            }
            return termsList.count < 4 ? termsList.count : 4
        }
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let termsCell = self.termsTableView.dequeueReusableCell(withIdentifier: "BBQTermsNConditionsTableViewCellID") as! BBQTermsNConditionsTableViewCell
        if let termsData = self.corporateTermsList {
             termsCell.loadTermsData(termsString: termsData[indexPath.row], for: indexPath.row)
        }
        return termsCell
    }
    

}
