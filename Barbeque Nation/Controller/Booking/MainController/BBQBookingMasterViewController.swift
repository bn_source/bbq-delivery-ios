//
//  Created by Ajith CP on 15/10/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQBookingMasterViewController.swift
//

import UIKit
import AMPopTip

enum BookingScreenType {
    case Booking, Reschedule, AfterDining
}

enum BBQBookingStatus: String {
    case Pending = "PENDING"
    case Confirmed = "CONFIRMED"
}

protocol BBQBookingMasterViewControllerProtocol: AnyObject {
    func didSelectLocationOutletButton(bottomSheet: BottomSheetController?, selectedDateType: BBQBookingDateType, selectedformattedDate: String)
    func didSelectCancelPayButton(bottomSheet: BottomSheetController?)
    func didCanceledDueToDineInUnavailable(bottomSheet: BottomSheetController?)
}

class BBQBookingMasterViewController: BaseViewController {
    
    // MARK: IBOutlets
    @IBOutlet weak var bookingTableView: UITableView!
    @IBOutlet weak var bookingTimeLabel: UILabel!
    @IBOutlet weak var bookingDateLabel: UILabel!
    //@IBOutlet weak var titleContanierView: UIView!
    @IBOutlet weak var locationValueButton: UIButton!
    @IBOutlet weak var lblOutletName: UILabel!
    @IBOutlet weak var stackViewForOutlet: UIStackView!
    @IBOutlet weak var btnViewMenu: UIButton!
    
    weak var delegate: BBQBookingMasterViewControllerProtocol? = nil
    
    // MARK: Properties
    var bookingBottomSheet: BottomSheetController?
    var corpBottomSheet : BottomSheetController?
    var selectedDateType: BBQBookingDateType = .Today
    var bookingScreenType: BookingScreenType = .Booking
    var superVC: UIViewController?
    
    internal var buffetDetailsHeight : CGFloat?
    internal var amountSplitComponentHeight : CGFloat?
    
    internal var totalBuffetQuantity : Int = 0
    
    internal var timeDateSlected : Bool = false
    internal var isTimeSlotReady : Bool = false
    internal var isBuffetAdded   : Bool = false
    internal var didAddPoints    : Bool = false
    internal var didAddCoupons   : Bool = false
    internal var didAddVouchers   : Bool = false
    internal var didAppliedCorpOffers    : Bool = false
    internal var isBuffetsRefreshNeeded  : Bool = true
    internal var isTaxSplitRefreshNeeded : Bool = true
    internal var isCardsResetWarned : Bool = false
    internal var isPointsResetWarned : Bool = false
    internal var isHappinessPacksWarned: Bool = false
    internal var isSmilesInProgress: Bool = false
    
    
    var showCalendarInBooking : Bool = true // Used if custom date is selected in home screen and navigated to this screen.
    var selectedformattedDate: String = ""
    var advanceRequired = false
    var branchAdvanceRequired = false
    var buffetImageMenu = [String]()
    
    internal var selectedDateString : String?
    internal var selectedTimeString : String?
    internal var selectedSlotId : Int = 0
    internal var selectedBuffetData: BuffetData?
    
    internal var pointsValueModel: BBQPointsValueModel? = nil
    internal var appliedCorpOffers : CorporateOffer?
    internal var appliedCoupons  : [VoucherCouponsData]?
    internal var appliedVouchers : [VoucherCouponsData]?
    
    internal var vouchersTerms : [String]?
    internal var couponsTerms  : [String]?
    
    internal var appliedCouponAmount  : Double = 0.00
    internal var appliedVoucherAmount : Double = 0.00
    internal var appliedVoucherPacks: Int = 0
    internal var appliedPointsAmount : Double = 0.00
    internal var arrAddedAmounts = [Double]()
    private var popTip = PopTip()
    
    internal var masterSubTotal : Double = 0.0 {
        didSet {
            self.slotsViewModel.bookingAmountSubTotal = self.masterSubTotal
            if masterSubTotal == 0 {
                self.slotsViewModel.finalAmountForPayment = 0.0
            }
        }
    }
    
    // MARK: ViewModels Lazy Initializers
    
    lazy var slotsViewModel: SlotsViewModel = {
        let slotModel = SlotsViewModel()
        return slotModel
    }()
    
    lazy var buffetsViewModel: BBQBookingBuffetsViewModel = {
        let buffetModel = BBQBookingBuffetsViewModel()
        return buffetModel
    }()
    
    lazy var cartViewModel: CartViewModel = {
        let viewModel = CartViewModel()
        return viewModel
    }()
    
    lazy var bbqLastVisitedViewModel: BBQLastVisitedViewModel = {
        let viewModel = BBQLastVisitedViewModel()
        return viewModel
    }()
    
    // MARK: Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AnalyticsHelper.shared.triggerEvent(type: .R01)
        self.setupBookingTableView()
        self.setupBookingMasterComponent()
        self.updateLocationOutlet()
        self.getOutletInformation()
        bookingTableView.tableHeaderView = stackViewForOutlet
        
        popTip.bubbleColor = .white
        popTip.cornerRadius = 5.0
        popTip.shouldShowMask = true
        //btnViewMenu.roundCorners(cornerRadius: 5.0, borderColor: .theme, borderWidth: 1.0)
        //Get Slots initially when user comes to this screen.
        self.getSlotsFor(dateType: self.selectedDateType)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.screenName(name: .Reservation_Screen)
        
    }
                         
        
    
    // MARK: Setup Methods
    
    internal func setupBookingMasterComponent() {
        
        //self.view.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        
        if self.selectedDateType == .Custom {
            self.bookingDateLabel.text = self.selectedDateString ??
                BBQBookingUtilies.shared.formattedBookingDate(from: self.selectedformattedDate, dateType: self.selectedDateType)
        } else {
            self.bookingDateLabel.text = self.selectedDateString ??
                BBQBookingUtilies.shared.formattedBookingDate(dateType: self.selectedDateType)
        }
        
        self.bookingTimeLabel.text = self.selectedTimeString ?? "select time"
    }
    
    internal func updateLocationOutlet() {
        self.lblOutletName.text = BBQUserDefaults.sharedInstance.CurrentSelectedLocation
        self.locationValueButton.titleLabel?.numberOfLines = 1
        self.locationValueButton.titleLabel?.adjustsFontSizeToFitWidth = true
        self.locationValueButton.titleLabel?.lineBreakMode = .byClipping;
        self.getOutletInformation()
    }
    
    //Check if dining is available or not for selected outlet
    private func checkForBrnachDineIn(){
        if let branch = findBranchBy(id: BBQUserDefaults.sharedInstance.branchIdValue), branch.only_delivery{
            if let delegate = self.delegate {
                delegate.didCanceledDueToDineInUnavailable(bottomSheet: self.bookingBottomSheet ?? nil)
            }
        }else{
            getOutletInformation()
        }
    }
    
    private func getOutletInformation() {
        BBQOutletInfoManager.outletInfo(branchId: BBQUserDefaults.sharedInstance.branchIdValue) { (model, result) in
            if result != nil {
                self.buffetImageMenu = model?.branchMenu ?? []
                self.branchAdvanceRequired = model?.advance_payment == "1" ? true : false
                self.btnViewMenu.isHidden = self.buffetImageMenu.count > 0 ? false : true
            }
        }
    }
    
    internal func setupBookingTableView() {
        
        self.bookingTableView.delegate = self
        self.bookingTableView.dataSource = self
        
        self.bookingTableView.estimatedRowHeight = 300
        self.bookingTableView.rowHeight = UITableView.automaticDimension
        
        // TODO: Move to diff place
        self.bookingTableView.register(UINib(nibName: "BBQBookingDateSelectionTableViewCell",
                                             bundle: nil),
                                       forCellReuseIdentifier: "BBQBookingDateSelectionTableViewCellID")
        
        self.bookingTableView.register(UINib(nibName: "BBQBookingNavigationTableViewCell",
                                             bundle: nil),
                                       forCellReuseIdentifier: "BBQBookingNavigationTableViewCellID")
        
        self.bookingTableView.register(UINib(nibName: "BBQBuffetSummaryTableViewCell",
                                             bundle: nil),
                                       forCellReuseIdentifier: "BBQBuffetSummaryTableViewCellID")
        
        self.bookingTableView.register(UINib(nibName: "BBQBookingAmountDetailsTableViewCell",
                                             bundle: nil),
                                       forCellReuseIdentifier: "BBQBookingAmountDetailsTableViewCellID")
        
        self.bookingTableView.register(UINib(nibName: "BBQBookingPointsTableViewCell",
                                             bundle: nil),
                                       forCellReuseIdentifier: "BBQBookingPointsTableViewCellID")
        
        self.bookingTableView.register(UINib(nibName: "BBQBookingFinalTableViewCell",
                                             bundle: nil),
                                       forCellReuseIdentifier: "BBQBookingFinalTableViewCellID")
        
        self.bookingTableView.register(UINib(nibName: "BBQCorpOffersTableViewCell",
                                             bundle: nil),
                                       forCellReuseIdentifier: "BBQCorpOffersTableViewCellID")
        
        self.bookingTableView.register(UINib(nibName: "BBQCouponsTableViewCell",
                                             bundle: nil),
                                       forCellReuseIdentifier: "BBQCouponsTableViewCellID")
        
        self.bookingTableView.register(UINib(nibName: "BBQApplyVouchersTableViewCell",
                                             bundle: nil),
                                       forCellReuseIdentifier: "BBQApplyVouchersTableViewCellID")
        
    }
    
    // MARK: Public Methods
    
    // MARK: Data Fetch Methods
    
    internal func fetchBuffetDetails() {
        //Call menu buffet API
        if self.isTimeSlotReady {
            let branchId = BBQUserDefaults.sharedInstance.branchIdValue
            let slotTime = self.slotsViewModel.getSlotTime(from: BBQBookingTimeSlotTableViewCell.selectedSlotIndex ?? 0)
            let isEarlyBird = self.slotsViewModel.getSlotInfoFor(slotID: self.selectedSlotId)?.isEarlyBird
            let isLateBird  = self.slotsViewModel.getSlotInfoFor(slotID: self.selectedSlotId)?.isLateBird
            let reservationDate = BBQBookingUtilies.shared.formattedReservationDate(from: self.selectedformattedDate,
                                                                                    dateType: self.selectedDateType)
            BBQActivityIndicator.showIndicator(view: self.view)
            AnalyticsHelper.shared.triggerEvent(type: .R04)
            AnalyticsHelper.shared.triggerEvent(type: .Reservation, properties: [.Location : BBQUserDefaults.sharedInstance.CurrentSelectedLocation, .Calendar : reservationDate, .Time: slotTime])
            self.buffetsViewModel.getMenuBranchBuffet(branchId: branchId, reservationDate: reservationDate,
                                                      reservationTime: slotTime,
                                                      slotId: self.selectedSlotId,
                                                      isEarlyBird: isEarlyBird ?? false,
                                                      isLateBird: isLateBird ?? false) { (isSuccess) in
                BBQActivityIndicator.hideIndicator(from: self.view)
                if isSuccess {
                    self.bookingTableView.reloadData {
                        self.adjustBottomSheetComponent()
                    }
                } else {
                    self.isTimeSlotReady = false
                    self.bookingTableView.reloadData {
                        ToastHandler.showToastWithMessage(message: "No booking available for this slot!!")
                    }
                }
            }
        }
    }
    
    internal func getSlotsDataFromServer(with type: BBQBookingDateType) {
        let reservationDate = self.getReservationDateForSelectedDate(type: type)
        let dateType = reservationDate.isToday() ? .Today : type
        let branchId = BBQUserDefaults.sharedInstance.branchIdValue
        
        BBQActivityIndicator.showIndicator(view: self.view)

        slotsViewModel.getBookingSlotsWithBranch(selectedDateType: dateType,
                                                 reservationDate: reservationDate,
                                                 branchId: branchId,
                                                 dinnerType: kDefaultLunch) { (isSuccess) in
            self.makeDefaultSelection(with: self.slotsViewModel)
            if isSuccess {
                BBQActivityIndicator.hideIndicator(from: self.view)
                
                self.bookingTableView.reloadData()
            } else {
                ToastHandler.showToastWithMessage(message: "Slots not available for selected date")
                BBQActivityIndicator.hideIndicator(from: self.view)
                
            }
        }
    }
    
    internal func getPointsFromServer() {
        AnalyticsHelper.shared.triggerEvent(type: .R05)
        let priceInDouble = self.slotsViewModel.getSmilesDataCount()
        
        let price = String(format: "%.02f", priceInDouble) // Points applying on estimated total, so rounding off
        UIUtils.showLoader()
        self.cartViewModel.getLoyaltyPointsValue(amount: price,
                                                 currency: "INR",
                                                 completion: { (isSuccess) in
                                                    UIUtils.hideLoader()
                                                    if isSuccess {
                                                        AnalyticsHelper.shared.triggerEvent(type: .R05A)
                                                        self.didAddPoints = true
                                                        self.pointsValueModel = self.cartViewModel.getPointValueModel
                                                        self.appliedPointsAmount = self.pointsValueModel?.redeemAmount ?? 0
                                                        self.slotsViewModel.adjustTotalAmountForPayment(with:self.appliedPointsAmount,
                                                                                                        isAddition: true,
                                                                                                        offerType: .Smiles)
                                                        self.isTaxSplitRefreshNeeded = true
                                                        self.bookingTableView.reloadData {
                                                            self.adjustBottomSheetComponent()
                                                        }
                                                    }else{
                                                        AnalyticsHelper.shared.triggerEvent(type: .R05B)
                                                    }
            self.isSmilesInProgress = false
        })
    }
    
    @IBAction func locationOutletClickButton(_ sender: Any) {
        if isTimeSlotReady{
            AnalyticsHelper.shared.triggerEvent(type: .R13)
        }else{
            AnalyticsHelper.shared.triggerEvent(type: .R02)
        }
        self.getOutletForBooking()
    }
    
    func getOutletForBooking() {
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: UIScreen.main.bounds.size.height - 250.0)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        let viewController = BBQLocationViewController()
        viewController.delegate = self
        viewController.isNavigatingToBooking = true
        viewController.location = BBQLocationManager.shared.currentLocationManager.location
        viewController.setupPresentingController(controller: bottomSheetController)
        bottomSheetController.present(viewController, on: self, viewHeight: UIScreen.main.bounds.height - 165)
    }
    
    // MARK: internal Methods
    @IBAction func onClickBtnBack(_ sender: Any) {
        if self.isTimeSlotReady{
            self.isTimeSlotReady = false
            if let cell = bookingTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? BBQBuffetSummaryTableViewCell{
                cell.arrAddedItems.removeAll()
            }
            self.resetBuffet()
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func onClickBtnViewMenu(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .R04A)
        if  buffetImageMenu.count > 0, let branchMenuVC = UIStoryboard.loadOutletBranchMenuViewController() {
            branchMenuVC.menuImages = buffetImageMenu
            self.present(branchMenuVC, animated: true)
        }else{
            ToastHandler.showToastWithMessage(message: kNoMenuAvailableText)
        }
    }
    internal func getSlotsFor(dateType: BBQBookingDateType) {
        self.didSelectBokkingDateType(with: dateType)
    }
    
    internal func expandCollapseBottomSheet(newHeight: CGFloat, isExpand: Bool) {
//        if let bottomSheet = self.bookingBottomSheet {
//            let adjustHeight =  newHeight - self.view.frame.height
//            bottomSheet.expandOrCollapse(viewController: self, withHeight: adjustHeight, isExpand: isExpand)
//        }
    }
    
    internal func makeDefaultSelection(with viewModel: SlotsViewModel) {
        if viewModel.availableSlotsData.count > 0 {
            
            if let slotID = viewModel.getFirstAvailableSlot().slotID,
                let index = viewModel.getFirstAvailableSlot().index {
                let defaultTime = viewModel.availableSlotsData[index].slotStartTime ?? ""
                let formattedTime = BBQBookingUtilies.shared.getDisplayTimeIn12HourFormat(defaultTime)
                BBQBookingTimeSlotTableViewCell.selectedIndex = slotID
                self.didSelectBokkingTimeSlot(with: formattedTime, slotId:slotID)
            }
        }
    }
}

extension BBQBookingMasterViewController: BBQLocationViewControllerProtocol {
    func navigateToBookingScreen() {
        if self.isTimeSlotReady {
            //Booking second/third screen
            if let delegate = self.delegate {
                delegate.didSelectLocationOutletButton(bottomSheet: self.bookingBottomSheet, selectedDateType: self.selectedDateType, selectedformattedDate: self.selectedformattedDate)
                self.getOutletInformation()
            }
        } else {
            self.updateLocationOutlet()
            checkForBrnachDineIn()
            self.getSlotsDataFromServer(with: self.selectedDateType)

        }
    }
}

extension BBQBookingMasterViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isTimeSlotReady && self.timeDateSlected && self.isBuffetAdded {
            return 7
        }
        if self.timeDateSlected {
            return 2
        }
        return  1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            if self.isTimeSlotReady {
                if !isBuffetAdded{
                    return tableView.frame.size.height - 320
                }
                return self.buffetDetailsHeight ?? 44
            }else{
                return tableView.frame.size.height - 320
            }
        }
        if indexPath.row == 4 && self.isTimeSlotReady {
            return self.amountSplitComponentHeight ?? UITableView.automaticDimension
        }

        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            return self.isTimeSlotReady ?
                self.loadBuffetSummary(for: indexPath.row) :
                self.loadDateSelectionCell(for: indexPath.row)
            
        case 1:
            if self.isBuffetAdded && self.isTimeSlotReady {
                return self.loadPointsDetailsCell(for: indexPath.row)
            }
            return self.loadNavigationCell(for: indexPath.row)
            
        case 2:
            return self.loadBookingCouponsCell(for: indexPath.row)
        case 3:
            return self.loadCorporateOffersCell(for: indexPath.row)
        case 4:
            return self.loadBookingAmountDetailsCell(for: indexPath.row)
        case 5:
            return self.loadVouchersDetailsCell(for: indexPath.row)
            
        case 6:
            return self.loadFinalPaymentCell(for: indexPath.row)
            
        default:
            return self.loadNavigationCell(for: indexPath.row)
        }
    }
    
    // MARK: Constructor Methods
    // TODO: Move to diff place
    @objc
    internal func loadDateSelectionCell(for index: Int) -> BBQBookingDateSelectionTableViewCell {
        let dateSelctionCell =
            self.bookingTableView.dequeueReusableCell(withIdentifier: "BBQBookingDateSelectionTableViewCellID")
                as! BBQBookingDateSelectionTableViewCell
        dateSelctionCell.loadTimeSlotController(with: self.selectedDateType,
                                                slotsModel: self.slotsViewModel,
                                                timeString: self.selectedTimeString ?? "",
                                                delegate: self)
        return dateSelctionCell
    }
    
    internal func loadNavigationCell(for index: Int) -> BBQBookingNavigationTableViewCell {
        let navigationCell =
            self.bookingTableView.dequeueReusableCell(withIdentifier: "BBQBookingNavigationTableViewCellID")
                as! BBQBookingNavigationTableViewCell
        navigationCell.navigationDelegate = self
        navigationCell.loadNavigationItem(isTimeSlotReady: self.isTimeSlotReady)
        return navigationCell
    }
    
    //Since this method defined in extension, to override it we have to mark it as @objc
    @objc
    internal func loadBuffetSummary(for index: Int) -> BBQBuffetSummaryTableViewCell {
        let buffetCell =
            self.bookingTableView.dequeueReusableCell(withIdentifier: "BBQBuffetSummaryTableViewCellID")
                as! BBQBuffetSummaryTableViewCell
        buffetCell.buffetDelegate = self
        buffetCell.buffetItemDelegate = self
        if self.isBuffetsRefreshNeeded {
            buffetCell.loadBuffettDetails(with: self.buffetsViewModel)
            self.isBuffetsRefreshNeeded = false
        }
        return buffetCell
    }
    
    internal func loadBookingAmountDetailsCell(for index: Int) -> BBQBookingAmountDetailsTableViewCell {
        
        let amountSplitCell =
            self.bookingTableView.dequeueReusableCell(withIdentifier: "BBQBookingAmountDetailsTableViewCellID")
                as! BBQBookingAmountDetailsTableViewCell
        amountSplitCell.amountCellDelegate = self
        if self.isTaxSplitRefreshNeeded {
            amountSplitCell.loadBookingAmountDetails(with: self.slotsViewModel)
            self.isTaxSplitRefreshNeeded = false
        }
        return amountSplitCell
    }
    
    internal func loadPointsDetailsCell(for index: Int) -> BBQBookingPointsTableViewCell {
        
        let pointsCell =
            self.bookingTableView.dequeueReusableCell(withIdentifier: "BBQBookingPointsTableViewCellID")
                as! BBQBookingPointsTableViewCell
        pointsCell.pointCellDelegate = self
        pointsCell.loadLoyaltyPointCell(isAdded: self.didAddPoints,
                                        pointModel: self.pointsValueModel ?? BBQPointsValueModel(), isPointEnabled: slotsViewModel.isPointsEnabled())
        return pointsCell
    }
    
    internal func loadCorporateOffersCell(for index: Int) -> BBQCorpOffersTableViewCell {
        
        let corpCell =
            self.bookingTableView.dequeueReusableCell(withIdentifier: "BBQCorpOffersTableViewCellID")
                as! BBQCorpOffersTableViewCell
        corpCell.corpDelegate = self
        corpCell.contentView.layer.opacity = slotsViewModel.isCorpOffersEnabled(isCouponsAdded: didAddCoupons) ? 1.0 : 0.4
        corpCell.isUserInteractionEnabled = slotsViewModel.isCorpOffersEnabled(isCouponsAdded: didAddCoupons)
        corpCell.setupCell()
        if let appliedOffers = self.appliedCorpOffers {
            corpCell.loadCorporateCellWith(for: self.totalBuffetQuantity - appliedVoucherPacks,
                                           offerData: appliedOffers,
                                           isCorpOffersApplied: self.didAppliedCorpOffers,
                                           isCardEnabled: slotsViewModel.isCorpOffersEnabled(isCouponsAdded: didAddCoupons))
        }
        return corpCell
    }
    
    internal func loadBookingCouponsCell(for index: Int) -> BBQCouponsTableViewCell {
        
        let couponCell =
            self.bookingTableView.dequeueReusableCell(withIdentifier: "BBQCouponsTableViewCellID")
                as! BBQCouponsTableViewCell
        couponCell.couponDelegate = self
        couponCell.loadCouponsData(with: self.appliedCoupons ?? [VoucherCouponsData](),
                                   didAdd: self.didAddCoupons,
                                   isCardEnabled: slotsViewModel.isCouponsEnabled(isCorpOffersAdded: didAppliedCorpOffers))
        return couponCell
    }
    
    
    internal func loadVouchersDetailsCell(for index: Int) -> BBQApplyVouchersTableViewCell {
        let voucherCell =
            self.bookingTableView.dequeueReusableCell(withIdentifier: "BBQApplyVouchersTableViewCellID")
                as! BBQApplyVouchersTableViewCell
        voucherCell.voucherDelegate = self
        voucherCell.loadVouchersData(with: self.appliedVouchers ?? [VoucherCouponsData](),
                                   didAdd: self.didAddVouchers,
                                   isCardEnabled: slotsViewModel.isHappinessCardsEnabled())
        
        return voucherCell
    }
    
    internal func loadFinalPaymentCell(for index: Int) -> BBQBookingFinalTableViewCell {
        let finalCell =
            self.bookingTableView.dequeueReusableCell(withIdentifier: "BBQBookingFinalTableViewCellID")
                as! BBQBookingFinalTableViewCell
        finalCell.loadFinalPaymentContent(with: self.slotsViewModel, screenType: self.bookingScreenType)
        finalCell.delegate = self
        
        return finalCell
    }
}

extension BBQBookingMasterViewController : BBQBookingDateSelectionTableViewCellDelegate,
    BBQBookingNavigationTableViewCellDelegate, BBQBookingAmountDetailsTableViewCellDelegate,
BBQBookingPointsTableViewCellDelegate, BBQCorpOffersTableViewCellDelegate, BBQCouponsTableViewCellDelegate, BBQBookingFinalTableViewCellProtocol,BBQApplyVouchersTableViewCellDelegate {
    
    func didClickOnDetails(cell: BBQAmountEntryTableViewCell) {
        let popupVC = BillDetailsPopVC(nibName: "BillDetailsPopVC", bundle: nil)
        popupVC.modalPresentationStyle = UIModalPresentationStyle .popover
        popupVC.preferredContentSize = CGSize(width: 170, height: 130)
        popupVC.strTitle = kDiningTaxServiceCharge
        popupVC.taxBreakUp = self.slotsViewModel.getTaxBreakUpModel()
        popupVC.view.frame.size.width = 200 > self.view.frame.size.width ? self.view.frame.size.width : 200
        popupVC.view.frame.size.height = CGFloat(60 + (self.slotsViewModel.getTaxBreakUpModel().count * 28))
        var originFrame = cell.valueLabel.frame
        originFrame.size.width = 40
        originFrame.origin.x = originFrame.origin.x + 35
        let frame = cell.convert(originFrame, to: self.view)
        popTip.show(customView: popupVC.view, direction: .auto, in: self.view, from: frame)
    }
    
    func didPressedVoucherAddRemoveButton(isAdd: Bool) {
        if isAdd {
            AnalyticsHelper.shared.triggerEvent(type: .R09)
            self.showSmilesCouponResetWarning { (doReset) in
                if doReset {
                    self.showAllOffersController(type: .VouchersForYou)
                }
            }
        } else if self.didAddVouchers {
            AnalyticsHelper.shared.triggerEvent(type: .R10)
            self.didAddVouchers = false
            self.isTaxSplitRefreshNeeded = true
            self.appliedVouchers?.removeAll()
            self.appliedVoucherPacks = 0
            countVoucherPack()
            self.slotsViewModel.adjustTotalAmountForPayment(with: self.appliedVoucherAmount,
                                                            isAddition: false,
                                                            offerType: .Vouchers)
            self.isTaxSplitRefreshNeeded = true
            self.bookingTableView.reloadData()
            AnalyticsHelper.shared.triggerEvent(type: .R10A)
            self.adjustBottomSheetComponent()
        }

    }
    
    func didPressedVouchersTermsAndConditions() {
        
        let termsVC = UIStoryboard.loadTermsAndConditionsScreen()
        termsVC.view.layoutIfNeeded()
        termsVC.modalPresentationStyle = .overFullScreen
        if let vouchersTermsData = self.vouchersTerms {
            termsVC.loadTermsControllerWith(termsData: vouchersTermsData)
            self.present(termsVC, animated: true, completion: nil)
        }
        
    }
    
    
    func didPressedAddRemoveCouponButton(isAdd: Bool) {
        if isAdd {
            AnalyticsHelper.shared.triggerEvent(type: .R07)
            self.showSmilesResetWarning { (doReset) in
                if doReset {
                    self.showAllOffersController(type: .CouponsForYou)
                }
            }
        } else if self.didAddCoupons {
            AnalyticsHelper.shared.triggerEvent(type: .R08)
            self.didAddCoupons = false
            self.isTaxSplitRefreshNeeded = true
            self.appliedCoupons?.removeAll()
            self.slotsViewModel.adjustTotalAmountForPayment(with: self.appliedCouponAmount,
                                                            isAddition: false,
                                                            offerType: .Coupons)
            self.isTaxSplitRefreshNeeded = true
            //self.bookingTableView.reloadRows(at: slotsViewModel.cardArrayToRefresh(), with: .fade)
            self.bookingTableView.reloadData()
            AnalyticsHelper.shared.triggerEvent(type: .R08A)
            self.adjustBottomSheetComponent()
        }
    }
    
    func didPressedCouponsTermsAndConditions() {
        let termsVC = UIStoryboard.loadTermsAndConditionsScreen()
        termsVC.view.layoutIfNeeded()
        termsVC.modalPresentationStyle = .overFullScreen
        if let couponsTermsData = self.couponsTerms {
            termsVC.loadTermsControllerWith(termsData: couponsTermsData)
            self.present(termsVC, animated: true, completion: nil)
        }
    }
    
    func didPressedCorpOffersAddRemoveButton(isAdded: Bool) {
        if isAdded {
            AnalyticsHelper.shared.triggerEvent(type: .R11)
            self.showSmilesResetWarning { (doReset) in
                if doReset {
                    let finalAmount = self.slotsViewModel.finalAmountForPayment < 0 ? 0.00 : self.slotsViewModel.finalAmountForPayment
                    self.buffetsViewModel.saveBookingDataForCorporateOffers(reservationDate: self.getReservationDateForSelectedDate(type: self.selectedDateType) + " " + (self.selectedTimeString ?? ""),
                                                                            packs: self.totalBuffetQuantity - self.appliedVoucherPacks,
                                                                            branchId: BBQUserDefaults.sharedInstance.branchIdValue,
                                                                            totalAmount: finalAmount,
                                                                            slotId: self.selectedSlotId, currency: "")
                    
                    //To check for user has logged in or not
                    if BBQUserDefaults.sharedInstance.isGuestUser {
                        self.navigateToLogin()
                    } else {
                        self.showCorporateOffersController()
                    }
                }
            }
            
        } else if self.didAppliedCorpOffers {
            AnalyticsHelper.shared.triggerEvent(type: .R12)
            self.didAppliedCorpOffers = false
            self.isTaxSplitRefreshNeeded = true
            self.slotsViewModel.adjustTotalAmountForPayment(with: self.appliedCorpOffers?.corporateDenomination ?? 0.00,
                                                            isAddition: false,
                                                            offerType: .CorporateOffers)
            self.appliedCorpOffers = nil
            AnalyticsHelper.shared.triggerEvent(type: .R12A)
            self.adjustBottomSheetComponent()
        }
    }
    
    func didPressedTermsAndConditions() {
        let termsVC = UIStoryboard.loadTermsAndConditionsScreen()
        termsVC.view.layoutIfNeeded()
        termsVC.modalPresentationStyle = .overFullScreen
        if let offerData = self.appliedCorpOffers {
            termsVC.loadTermsControllerWith(termsData: offerData.corporateTerms)
        }
        self.present(termsVC, animated: true, completion: nil)
    }
    
    func didPressedPointsAddRemoveButton(isAdd: Bool) {
        
        if BBQUserDefaults.sharedInstance.isGuestUser {
            self.navigateToLogin()
            return
        }
        if !isSmilesInProgress{
            isSmilesInProgress = true
            if isAdd, !self.didAddPoints {
                 self.getPointsFromServer()
            } else if self.didAddPoints {
                AnalyticsHelper.shared.triggerEvent(type: .R06)
                self.didAddPoints = false
                self.isTaxSplitRefreshNeeded = true
                self.slotsViewModel.adjustTotalAmountForPayment(with: Double(self.pointsValueModel?.redeemAmount ?? 0),
                                                                isAddition: false,
                                                                offerType: .Smiles)
                AnalyticsHelper.shared.triggerEvent(type: .R06A)
                self.adjustBottomSheetComponent()
                self.isSmilesInProgress = false
            }
        }
        
    }
    
    func contentDidChange(cell: BBQBookingAmountDetailsTableViewCell, contentHeight: CGFloat) {
        self.amountSplitComponentHeight = contentHeight
        self.adjustBottomSheetComponent()
    }
    
    func didSelectBokkingDateType(with type: BBQBookingDateType) {
        
        self.timeDateSlected = false
        
        if type == .Custom, self.showCalendarInBooking {
            // Updating things only after date selection, retruns after showing calendar.
            self.showCalendarBottomSheet()
            return
        }
        self.showCalendarInBooking = true
        
        self.selectedDateType = type
        if type == .Custom {
            self.selectedDateString = BBQBookingUtilies.shared.formattedBookingDate(from: self.selectedformattedDate, dateType: type)
        } else {
            self.selectedDateString = BBQBookingUtilies.shared.formattedBookingDate(from: nil, dateType: type)
        }
        
        self.setupBookingMasterComponent()
        self.isTimeSlotReady = false
        //In case of reschedule, make timeDateSlected as true. to display arrow even if user has not changed date and time
        //set priviously booked time  to  self.bookingTimeLabel by default, change its text color to black
        if self.bookingScreenType == .Reschedule {
            self.timeDateSlected = true
            self.bookingTimeLabel.textColor = .black
            
        }else {
            
            self.bookingTimeLabel.text = "select time" // Always reset after date type selection.
            self.bookingTimeLabel.textColor = .lightGray
           }
        
        // Make time slot API call here.
        self.getSlotsDataFromServer(with: type)
    }
    
    func didSelectBokkingTimeSlot(with time: String, slotId: Int) {
        self.selectedSlotId = slotId
        self.timeDateSlected = true
        self.selectedTimeString = time
        self.bookingTimeLabel.textColor = .black
        self.setupBookingMasterComponent()
        
        self.bookingTableView.reloadData {
            // Bring expand/collapse if needed
        }
    }
    
    func didPressedNavigationButton(isNavigateBack: Bool) {
        self.isTimeSlotReady = isNavigateBack
        if self.isTimeSlotReady {
            self.isBuffetsRefreshNeeded = true
            AnalyticsHelper.shared.triggerEvent(type: .R03)
            selectedDateType.triggerBookingEvent()
            self.fetchBuffetDetails() // Only when going forward with booking
        } else {
            self.bookingTableView.reloadData { // Navigate back
//                let modifiedHeight = self.view.frame.height - 465 // fixed height for time slot selection: as per UX0
//                self.bookingBottomSheet?.expandOrCollapse(viewController: self,
//                                                          withHeight: modifiedHeight,
//                                                          isExpand: false)
                
            }
        }
    }
    
    @objc
    func didPressReserveButton(isPayNow: Bool) {
        advanceRequired = false
        if BBQUserDefaults.sharedInstance.isGuestUser {
            self.navigateToLogin()
        } else {
            //Create Booking
            self.createBooking(isPayNow: isPayNow)
        }
    }
    
    func didAdvacnceRequired() {
        advanceRequired = true
    }
    
    // MARK: internal Methods
    
    internal func adjustBottomSheetComponent() {
        self.bookingTableView.reloadData()
    }
    
    internal func getReservationDateForSelectedDate(type: BBQBookingDateType) -> String {
        var reservationDate = ""
        switch type {
        case .Today:
            reservationDate = BBQBookingUtilies.shared.getTodayDateForAPI()
        case .Tomorrow:
            reservationDate = BBQBookingUtilies.shared.getTomorrowDateForAPI()
        default: //Pick a date Pressed
            reservationDate = self.selectedformattedDate
        }
        return reservationDate
    }
    
    internal func navigateToLogin() {
//        let viewController = UIStoryboard.loadLoginViewController()
//        viewController.navigationFromScreen = .Booking
//        viewController.shouldCloseTouchingOutside = true
//        viewController.currentTheme = .dark
//        viewController.view.backgroundColor = UIColor.clear
//        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: UIScreen.main.bounds.size.height - viewController.view.frame.size.height)
//        let bottomSheet = BottomSheetController(configuration: configuration)
//        viewController.delegate = self
//        bottomSheet.present(viewController, on: self)
        
        let tabViewController = UIStoryboard.loadLoginThroughPhoneController()
        tabViewController.delegate = self
        tabViewController.navigationFromScreen = .Booking
    self.navigationController?.pushViewController(tabViewController, animated: false)

    }
    
    internal func createBooking(isPayNow: Bool) {
        AnalyticsHelper.shared.triggerEvent(type: .R14)
        AnalyticsHelper.shared.triggerEvent(type: .Reservation_Checkout, properties: [.Location : BBQUserDefaults.sharedInstance.CurrentSelectedLocation])
        let reservationDate = self.getReservationDateForSelectedDate(type: self.selectedDateType)
        let branchId = BBQUserDefaults.sharedInstance.branchIdValue
        let slotId = String(format: "%d", self.selectedSlotId)
        let slotTime = self.slotsViewModel.getSlotTime(from: BBQBookingTimeSlotTableViewCell.selectedSlotIndex ?? 0)
        var bbqPoints = 0
        var bbqPointsAmount = "0"
        if self.didAddPoints, self.pointsValueModel?.redeemAmount != 0.0 {
            bbqPoints = self.pointsValueModel?.redeemablePoints ?? 0
            bbqPointsAmount = String(format: "%.2lf", self.pointsValueModel?.redeemAmount ?? 0.0)
        }
        let voucherDetails = self.buffetsViewModel.createConsolidatedVoucherDetails(appliedCorpOffers: self.appliedCorpOffers, appliedCoupons: self.appliedCoupons, appliedVouchers: self.appliedVouchers, arrAddedAmounts: arrAddedAmounts, preferredBranch: slotsViewModel.prefferedBranchData)
        let createBookingData = CreateBookingBody(bookingId: nil, branchId: branchId, slotId: slotId, reservationDate: reservationDate, reservationTime: slotTime, loyaltyPoints: bbqPoints, loyaltyAmount: bbqPointsAmount, bookingDetails: self.buffetsViewModel.getBookingDetailsBodyData!, voucherDetails: voucherDetails.voucherDetails, voucher_meal_discount: voucherDetails.voucher_meal_discount)
        
        BBQActivityIndicator.showIndicator(view: self.view)
        AnalyticsHelper.shared.triggerEvent(type: .booking_status)
        
        //Check if booking data containes only kid, Do not allow to book,
        //It mist include adult item too
        
        //A variable to validate booking
        var isBookingValid = true
        //update every object  status type and image
        createBookingData.bookingDetails.forEach({ (model) in

            if model.customerType == "Adult"{
                
                //applicable for reservation
                isBookingValid = false
            }
          
        })
        if isBookingValid {
            //show message that any adult buffet should be booked
            UIUtils.showToast(message: "Only Kids Buffet can not be booked")
            BBQActivityIndicator.hideIndicator(from: self.view)
            AnalyticsHelper.shared.triggerEvent(type: .R14B)
            AnalyticsHelper.shared.triggerEvent(type: .Reservation_Status, properties: [.Reservation_booked_Status : AnalyticsHelper.MoEngageAttrValue.Failed.rawValue, .Location: BBQUserDefaults.sharedInstance.CurrentSelectedLocation, .Error: "Only Kids Buffet can not be booked"])
            return
        }
        else if branchAdvanceRequired{
            let availableCapacity = self.slotsViewModel.getAvailableCapacity(from: BBQBookingTimeSlotTableViewCell.selectedSlotIndex ?? 0)
            if availableCapacity >= totalBuffetQuantity{
                checkForReservationRule(createBookingData: createBookingData, isPayNow: isPayNow)
            }else{
                UIUtils.showToast(message: String(format: "Max available capacity is %li", availableCapacity))
                BBQActivityIndicator.hideIndicator(from: self.view)
            }
        }
        else  {
            self.proccessWithBookings(createBookingData: createBookingData, isPayNow: isPayNow)
        }
    }
    
    private func proccessWithBookings(createBookingData: CreateBookingBody, isPayNow: Bool){
        self.buffetsViewModel.createBooking(createBookingData: createBookingData) { (isSuccess, message) in
            BBQActivityIndicator.hideIndicator(from: self.view)
            if isSuccess {
                AnalyticsHelper.shared.triggerEvent(type: .booking_success_value)
                AnalyticsHelper.shared.triggerEvent(type: .R14A)
                AnalyticsHelper.shared.triggerEvent(type: .Reservation_Status, properties: [.Reservation_booked_Status : AnalyticsHelper.MoEngageAttrValue.Success.rawValue, .Location: BBQUserDefaults.sharedInstance.CurrentSelectedLocation, .Booking_id: self.buffetsViewModel.bookingId ?? ""])
                AnalyticsHelper.shared.triggerEvent(type: .R16)
                if let bookingStatus = self.buffetsViewModel.bookingStatus {
                    if bookingStatus == BBQBookingStatus.Pending.rawValue {
                        self.launchAdvancePayment() // Delegate
                    } else if bookingStatus == BBQBookingStatus.Confirmed.rawValue {
                        if isPayNow{
                            self.proceedToPaymentPressed(bookingID: self.buffetsViewModel.getAmountTobePaid().bookingID, paymentAmount: self.buffetsViewModel.getAmountTobePaid().totalBill, paymentRemarks: "Advance payment of estimated amount", controller: self)
                        }else{
                            self.launchConfirmBookingScreen()
                        }
                    }
                }
            }else{
                self.showErrorMessage(message: message)
                AnalyticsHelper.shared.triggerEvent(type: .booking_failure_value)
                AnalyticsHelper.shared.triggerEvent(type: .R14B)
                AnalyticsHelper.shared.triggerEvent(type: .Reservation_Status, properties: [.Reservation_booked_Status : AnalyticsHelper.MoEngageAttrValue.Failed.rawValue, .Location: BBQUserDefaults.sharedInstance.CurrentSelectedLocation, .Error: message as Any])
            }
        }
    }
    
    private func checkForReservationRule(createBookingData: CreateBookingBody, isPayNow: Bool){
        self.buffetsViewModel.checkReservationRule(createBookingData: createBookingData, no_of_pax: Int(totalBuffetQuantity), payable_amt: slotsViewModel.finalAmountForPayment) { isSuccess, response in
            BBQActivityIndicator.hideIndicator(from: self.view)
            if isSuccess, let response = response{
                if let advance_amount = response.data?.advance_amount, advance_amount > 0{
                    AnalyticsHelper.shared.triggerEvent(type: .R15)
                    let pendingBookingVC = UIStoryboard.pendingBookingVC()
                    pendingBookingVC.viewModel = PendingBookingViewModel(bookingData: self.buffetsViewModel.createJsonForCreateBooking(createBookingData: createBookingData) ?? Json())
                    pendingBookingVC.viewModel.pendingBookingData = response
                    pendingBookingVC.viewModel.createBookingBody = createBookingData
                    pendingBookingVC.buffetsViewModel = self.buffetsViewModel
                    pendingBookingVC.selectedDateType = self.selectedDateType
                    pendingBookingVC.selectedDateString = self.selectedDateString
                    pendingBookingVC.selectedTimeString = self.selectedTimeString
                    pendingBookingVC.selectedformattedDate = self.selectedformattedDate
                    pendingBookingVC.no_of_pax = Int(self.totalBuffetQuantity)
                    pendingBookingVC.payable_amt = self.slotsViewModel.finalAmountForPayment
                    if let navVc = self.navigationController {
                        self.dismissViewControllers()
                        pendingBookingVC.superVC = self.superVC
                        navVc.pushViewController(pendingBookingVC, animated: true)
                    }
                    
                }else{
                    self.proccessWithBookings(createBookingData: createBookingData, isPayNow: isPayNow)
                }
            }else{
                
            }
        }
    }
    
    private func showErrorMessage(message: String?){
        let configuration = BottomSheetConfiguration(animationDuration: 0.4,
                                                     minimumTopSpacing: 200, touchDismiss: true)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        let directionBottomSheet = Bundle.main.loadNibNamed("BookingFailedBottomSheet", owner: nil)!.first as! BookingFailedBottomSheet
        directionBottomSheet.message = message
        directionBottomSheet.view.maskByRoundingCorners(cornerRadius: 10, maskedCorners: CornerMask.topCornersMask)
        bottomSheetController.present(directionBottomSheet, on: self, viewHeight: 350)
    }
}

// MARK:- After booking Handling
extension BBQBookingMasterViewController {
   
    
    
    internal func launchAdvancePayment() {
        let minTop = UIScreen.main.bounds.height -  292 // 292 Initial height
        let configuration = BottomSheetConfiguration(animationDuration: 0.4,
                                                     minimumTopSpacing: minTop, touchDismiss: false)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        let viewController = BBQPaymentBottomSheetController()
        viewController.paymentDetails = self.buffetsViewModel.getAmountTobePaid()
        viewController.bottomSheetController = bottomSheetController
        viewController.buffetsViewModel = self.buffetsViewModel
        viewController.delegate = self
        bottomSheetController.present(viewController, on: self)
    }
    
    @objc internal func launchConfirmBookingScreen() {
        let storyBoard : UIStoryboard = UIStoryboard(name: Constants.OrderConfirm.orderConfirmStoryBoard, bundle:nil)
        let orderConfirmVC = storyBoard.instantiateViewController(withIdentifier: "BBQBookingConfirmationVC") as! BBQBookingConfirmationVC
        orderConfirmVC.bookingID = self.buffetsViewModel.bookingId ?? ""
        orderConfirmVC.superVC = superVC
        self.navigationController?.pushViewController(orderConfirmVC, animated: true)
    }
    
    //Confirmation Screen Done Button Press Event
    
    
    func dismissViewControllers() {

        guard let vc = self.presentedViewController else { return }
        vc.dismiss(animated: true, completion: nil)
    }
   
}

extension BBQBookingMasterViewController : BBQBuffetSummaryTableViewCellDelegate,
BBQBuffetEntryTableViewCellDelegate, BBQPaymentBottomSheetProtocol, BBQRazorpayControllerProtocol {
    
    // MARK: BBQRazorpayControllerProtocol Methods
    
    func onRazorpayPaymentError(_ code: Int32, description str: String) {
        if !advanceRequired{
            showTransactionRetry()
            return
        }
        switch code {
        case 0,1,3:
            PopupHandler.showSingleButtonsPopup(title: kFailedTitle,
                                                message: str,
                                                on: self, firstButtonTitle: kOkButtonTitle) { }
        case 2:
            self.showTransactionCancelled()
            
        default:
            self.bookingBottomSheet?.dismissBottomSheet(on: self)
        }
    }
    
    func onRazorpayPaymentSuccess(_ paymentData: BBQPaymentDataModel) {
        self.bookingBottomSheet?.dismissBottomSheet(on: self)
        
        let paymentGateway = Constants.BBQBookingStatus.razorPayment
        if let createBookingModel = self.buffetsViewModel.createBookingModel, let bookingId = createBookingModel.bookingId {
            BBQActivityIndicator.showIndicator(view: self.view)
            self.buffetsViewModel.updateBooking(bookingId: bookingId, paymentOrderId: paymentData.razorpayOrderID ?? "", paymentId: paymentData.paymentID ?? "", paymentGatewayName: paymentGateway, paymentSignature: paymentData.razorpaySignature ?? "") { (isSuccess) in
                BBQActivityIndicator.hideIndicator(from: self.view)
                
                if isSuccess {
                    self.showPaymentSuccessScreen()
                }
            }
        }
    }
    
    func showTransactionCancelled() {
        let keyWindow = UIApplication.shared.keyWindow?.rootViewController?.presentedViewController
        
        guard let window = keyWindow else {
            return
        }
        
        PopupHandler.showSingleButtonsPopup(title: kMyCartTransactionCancelTitle,
                                            message: kMyCartTransactionCancelMessage,
                                            on: self.presentedViewController ?? window,
                                            firstButtonTitle: kButtonOkay) {
            if !self.advanceRequired{
                self.dismiss(animated: true) {
                    self.launchConfirmBookingScreen()
                }
            }
        }
    }
    
    func showTransactionRetry() {
       
        
        PopupHandler.showTwoButtonsPopup(title: kMyCartTransactionCancelTitle, message: kMyCartTransactionCancelMessage, on: self, firstButtonTitle: "Retry Payment", firstAction: {
            self.proceedToPaymentPressed(bookingID: self.buffetsViewModel.getAmountTobePaid().bookingID, paymentAmount: self.buffetsViewModel.getAmountTobePaid().totalBill, paymentRemarks: "Advance payment of estimated amount", controller: self)
        }, secondButtonTitle: "Pay Later") {
            self.launchConfirmBookingScreen()
        }
    }
    
    @objc func showPaymentSuccessScreen() {
        let registrationConfirmationVC = UIStoryboard.loadRegistrationConfirmation()
        registrationConfirmationVC.modalPresentationStyle = .overFullScreen
        registrationConfirmationVC.titleText = kPaymentSuccessText
        registrationConfirmationVC.navigationText = kMyTransactionPaymentRedirectionMessage
        
        //let keyWindow = UIApplication.shared.keyWindow?.rootViewController?.presentedViewController
        self.present(registrationConfirmationVC, animated: false) {
            sleep(UInt32(2.0))
            
            self.dismiss(animated: true) {
                self.launchConfirmBookingScreen()
            }
        }
    }
    
    // MARK:  BBQPaymentBottomSheetProtocol Methods
    func proceedToPaymentPressed(bookingID: String, paymentAmount: Double,
                                 paymentRemarks: String, controller: UIViewController?) {
        let rpController = BBQRazorpayContainerController()
        rpController.modalPresentationStyle = .overCurrentContext
        

        if let presentingController = controller {
            presentingController.present(rpController, animated: true, completion: nil)
        } else {
            self.present(rpController, animated: true, completion: nil)
        }
        
        rpController.payDelegate = self
        rpController.createOrderForPayment(with: paymentAmount,
                                           currency:"INR", title: "", and: paymentRemarks, bookingID: bookingID, loyalty_points: Int(self.appliedPointsAmount))
    }
    
    func cancelPressed() {
        if let delegate = self.delegate {
            delegate.didSelectCancelPayButton(bottomSheet: self.bookingBottomSheet ?? nil)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func didChangeBuffetSummaryContent(with contentSize: CGSize) {
        
        self.buffetDetailsHeight = contentSize.height + 50 // Padding
        self.adjustBottomSheetComponent()
    }
    
    
    func didAddQuantity(in entryCell: BBQBuffetEntryTableViewCell, itemTotal: Double,
                        bookingDetailsBodyData: BookingDetailsBody?) {
        
        
        self.masterSubTotal =  self.masterSubTotal + itemTotal
        arrAddedAmounts.append(itemTotal)
        self.resetCardsApplication { (doReset) in
            if doReset || !self.isCardsResetWarned {
                self.totalBuffetQuantity +=  1
                self.prepareBookingAfterBuffetChange(bookingBodyData: bookingDetailsBodyData)
            } else {
                entryCell.resetBuffetQuantity(isAdd: true)
            }
        }
    }
    
    func didRemoveQuantity(in entryCell: BBQBuffetEntryTableViewCell, itemTotal: Double,
                           bookingDetailsBodyData: BookingDetailsBody?) {
        self.masterSubTotal = self.masterSubTotal - itemTotal
        for i in 0..<arrAddedAmounts.count{
            if arrAddedAmounts[i] == itemTotal{
                arrAddedAmounts.remove(at: i)
                break
            }
        }
        self.resetCardsApplication { (doReset) in
            
            if doReset {
                self.totalBuffetQuantity -=  1
                if self.totalBuffetQuantity == 0 {
                    self.amountSplitComponentHeight = nil
                    self.masterSubTotal = 0.0
                }
                self.prepareBookingAfterBuffetChange(bookingBodyData: bookingDetailsBodyData)
            } else {
                entryCell.resetBuffetQuantity(isAdd: false)
            }
        }
    }
    
    private func prepareBookingAfterBuffetChange(bookingBodyData: BookingDetailsBody?)  {
        self.isBuffetAdded = self.totalBuffetQuantity > 0 ? true : false
        self.isBuffetsRefreshNeeded = true
        self.isTaxSplitRefreshNeeded = true
        
        self.buffetsViewModel.constructBookingDetailsPayload(bookingDetailsBodyData: bookingBodyData)
        
        if self.didAddPoints {
            self.getPointsFromServer()
        } else { self.bookingTableView.reloadData() }
    }
    
    func didPressedMenuButton(in entryCell: BBQBuffetEntryTableViewCell) {
        if let buffetData = self.buffetsViewModel.buffetData {
//            self.selectedBuffetData = buffetData[entryCell.tag]
        }
        
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: 150.0)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        let viewController = BottomSheetMenuViewController()
        if let branchBuffetData = self.selectedBuffetData {
            viewController.buffetData = [branchBuffetData]
        }
        
        //viewController.dataSource = self
        bottomSheetController.present(viewController, on: self)
    }
    
    internal func resetCardsApplication(completion: @escaping (_ doReset: Bool)->()) {
        if !self.isCardsResetWarned && (didAppliedCorpOffers || didAddCoupons || didAddVouchers) {
            PopupHandler.showTwoButtonsPopup(title: "All applied items/coupons will be removed",
                                             message: "All the Items/Coupons except Smiles will be removed from your order. Please apply those again.",
                                             on: self, firstButtonTitle: "Ok",
                                             firstAction: {
                                                self.resetCards()
                                                completion(true)

            }, secondButtonTitle: "Cancel", secondAction: {
                completion(false)
            })
            self.isCardsResetWarned = true
        } else {
            self.resetCards()
            completion(true)
        }
    }
    
    func resetCards() {
        self.didAddCoupons = false
        self.didAddVouchers = false
        self.didAppliedCorpOffers = false
        self.appliedVouchers?.removeAll()
        self.appliedCoupons?.removeAll()
        self.appliedVoucherAmount = 0
        self.appliedCouponAmount = 0
        self.appliedVoucherPacks = 0
        countVoucherPack()
        self.isTaxSplitRefreshNeeded = true
        self.slotsViewModel.resetBookingCardsApplication()
        self.bookingTableView.reloadData()
    }
    
    func resetBuffet() {
        self.didAddCoupons = false
        self.didAddVouchers = false
        self.didAppliedCorpOffers = false
        self.appliedVouchers?.removeAll()
        self.appliedCoupons?.removeAll()
        self.appliedVoucherAmount = 0
        self.appliedCouponAmount = 0
        self.appliedVoucherPacks = 0
        self.isTaxSplitRefreshNeeded = true
        self.slotsViewModel.resetBookingCardsApplication()
        self.isBuffetsRefreshNeeded = true
        self.isBuffetAdded = false
        self.appliedVoucherPacks = 0
        self.arrAddedAmounts.removeAll()
        self.totalBuffetQuantity = 0
        self.amountSplitComponentHeight = nil
        self.masterSubTotal = 0.0
        self.isBuffetsRefreshNeeded = true
        resetPoints()
        
        
        self.bookingTableView.reloadData()
    }
    
    private func showSmilesResetWarning(completion: @escaping (_ doReset: Bool)->()) {
        if !self.isPointsResetWarned && self.didAddPoints {
            PopupHandler.showTwoButtonsPopup(title: "Applied Smiles will be removed",
                                             message: "All applied Smiles will be removed if you apply any coupon/happiness card/ corporate offers on top of Smiles. Please re -apply Smiles again.",
                                             on: self, firstButtonTitle: "Cancel",
                                             firstAction: {
                                                completion(false)
                                                
            }, secondButtonTitle: "OK", secondAction: {
                completion(true)
            })
            self.isPointsResetWarned = true
        } else {
            completion(true)
        }
        
    }
    
    private func showSmilesCouponResetWarning(completion: @escaping (_ doReset: Bool)->()) {
        if !self.isHappinessPacksWarned && (self.didAddPoints || self.didAddCoupons) {
            PopupHandler.showTwoButtonsPopup(title: "Applied Smiles/Coupons will be removed",
                                             message: "All applied Smiles will be removed if you apply any happiness card offers on top of Smiles/Coupons. Please re -apply Smiles again.",
                                             on: self, firstButtonTitle: "Cancel",
                                             firstAction: {
                                                completion(false)
                                                
            }, secondButtonTitle: "OK", secondAction: {
                completion(true)
            })
            self.isHappinessPacksWarned = true
        } else {
            completion(true)
        }
        
    }
    
    private func resetPoints() {
        if didAddPoints {
            self.didAddPoints = false
            self.isTaxSplitRefreshNeeded = true
            self.slotsViewModel.adjustTotalAmountForPayment(with: self.appliedPointsAmount,
                                                            isAddition: false,
                                                            offerType: .Smiles)
        }
    }
}

extension BBQBookingMasterViewController: BottomSheetCalendarProtocol  {
    
    // Copied logics from old class BottomSheetTimeSlotViewController
    
    func updateDateFromCalendar(originalDate: String, formattedDate: String) {
        
        self.selectedDateString = originalDate
        self.selectedformattedDate = formattedDate
        self.setupBookingMasterComponent()
        
        // Updating UI after date selection
        
        self.bookingTimeLabel.text = "select time" // Always reset after date type selection.
        self.bookingTimeLabel.textColor = .lightGray
        
        self.selectedDateType = .Custom
        BBQBookingTimeSlotTableViewCell.selectedIndex = nil
        self.getSlotsDataFromServer(with: .Custom) // Updating time slots for selected date.
    }
    
    func showCorporateOffersController() {
        let corpVC = BBQBottomSheetCorporateViewController()
        corpVC.componentTitle = kCorporateOffers
        corpVC.componentTitleLogo = UIImage(named:Constants.AssetName.corporateIcon)
        corpVC.buffetsViewModel = self.buffetsViewModel
        corpVC.emailType = BBQEmailType.corporate
        corpVC.validatorDelegate = self
        corpVC.modalPresentationStyle = .fullScreen
        let configuration = BottomSheetConfiguration(animationDuration: 0.4,
                                                     minimumTopSpacing: UIScreen.main.bounds.size.height - corpVC.view.frame.size.height)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        self.corpBottomSheet = bottomSheetController
        self.present(corpVC, animated: true, completion: nil)
        //bottomSheetController.present(corpVC, on: self)
    }
    
    internal func showCalendarBottomSheet() {
        let viewController = BottomSheetCalendarViewController()
        viewController.delegate = self
        let configuration = BottomSheetConfiguration(animationDuration: 0.4,
                                                     minimumTopSpacing: UIScreen.main.bounds.height - viewController.view.frame.size.height)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        
        bottomSheetController.present(viewController, on: self)
    }
    
    internal func showAllOffersController(type: ScreenType) {
        if BBQUserDefaults.sharedInstance.isGuestUser {
            self.navigateToLogin()
        } else {
            //call vouchers and coupons
            let viewController = BBQAllOffersViewController()
            viewController.screenType = type
            if type == .VouchersForYou||type == .CouponsForYou{
                let finalAmount = self.slotsViewModel.finalAmountForPayment < 0 ? 0.00 : self.slotsViewModel.finalAmountForPayment
                var currencyValue = ""
                if let buffetData = self.buffetsViewModel.buffetData {
                    currencyValue = buffetData[0].buffetCurrency!
                }
                self.buffetsViewModel.saveBookingDataForCorporateOffers(reservationDate: self.getReservationDateForSelectedDate(type: self.selectedDateType) + " " + (self.selectedTimeString ?? ""),
                                                                        packs: self.totalBuffetQuantity - appliedVoucherPacks,
                                                                        branchId: BBQUserDefaults.sharedInstance.branchIdValue,
                                                                        totalAmount: finalAmount,
                                                                        slotId: self.selectedSlotId, currency: currencyValue)
                viewController .buffetsViewModel = self.buffetsViewModel
            }
            if type == .CouponsForYou {
                // Manual coupon code entry only for copons.
                viewController.doManualCoupon = true
            }
            let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: UIScreen.main.bounds.size.height - viewController.view.frame.size.height)
            let bottomSheet = BottomSheetController(configuration: configuration)
            viewController.delegate = self

            viewController.bottomSheet = bottomSheet
//            bottomSheet.present(viewController, on: self)
            viewController.modalPresentationStyle = .fullScreen
            self.present(viewController, animated: true, completion: nil)
        }
    }
    
}

extension BBQBookingMasterViewController: LoginControllerDelegate {
    func gotoHomeScreen(isExistingUser: Bool, navigationFrom: LoginNavigationScreen) {
        //Update user selected outlet
        if BBQUserDefaults.sharedInstance.accessToken != "" {
            self.bbqLastVisitedViewModel.getLastVisitedBranch(id: BBQUserDefaults.sharedInstance.branchIdValue) {_ in
                NotificationCenter.default.post(name: Notification.Name(Constants.NotificationName.upadateHomeHeader), object: nil)
            }
        }
        
        if !isExistingUser {
            showRegistrationConfirmationPage(navigationFrom: navigationFrom)
        }
        
        self.bookingTableView.reloadData() // Refresh data after login, mainly user default updates.
    }
    
    func showRegistrationConfirmationPage(navigationFrom: LoginNavigationScreen) {
        let registrationConfirmationVC = UIStoryboard.loadRegistrationConfirmation()
        registrationConfirmationVC.modalPresentationStyle = .overFullScreen
        registrationConfirmationVC.titleText = kSignUpConfirmationTitleText
        registrationConfirmationVC.navigationText = kSignUpConfirmationNavigationText
        
        self.present(registrationConfirmationVC, animated: false) {
            sleep(UInt32(2.0))
            self.dismiss(animated: false) {
                
            }
        }
    }
}
extension BBQBookingMasterViewController: BBQAllOffersProtocol {
   
    func selectedCorporateOffer(corporateOffer: CorporateOffer) {
    }
    
    func buyNowButtonClicked(bottomSheet: BottomSheetController?) { }
    
    func addButtonClickedAt(index: Int) {
    }
    
    func test(voucherList:[VoucherCouponsData], type: ScreenType,termsAndConditions:[String]){
        if type == .CouponsForYou {
            self.didAddCoupons = true
            self.appliedCoupons = voucherList
            let couponData = voucherList[0] // Only one coupon can apply.
            var currentAmount = self.slotsViewModel.finalAmountForPayment - self.slotsViewModel.totalTaxAmount
            currentAmount = currentAmount < 0.00 ? 0.00 : currentAmount
            let adjustedDenomination = self.slotsViewModel.consumedCouponAmount(denomination: couponData.denomination ?? 0.00,
                                                                                payAmount: currentAmount)
            self.resetPoints()
            self.appliedCouponAmount = adjustedDenomination
            self.slotsViewModel.adjustTotalAmountForPayment(with: adjustedDenomination,
                                                            isAddition: true,
                                                            offerType: .Coupons)
            self.isTaxSplitRefreshNeeded = true
            self.bookingTableView.reloadData()
        }
        if type == .VouchersForYou {
            self.didAddVouchers = true
            self.appliedVouchers = voucherList
            let totalVoucherAmount = self.slotsViewModel.totalVoucherAmount(for: self.appliedVouchers!)
            self.resetPoints()
            self.appliedVoucherAmount = totalVoucherAmount.amount
            self.slotsViewModel.adjustTotalAmountForPayment(with: totalVoucherAmount.amount,
                                                            isAddition: true,
                                                            offerType: .Vouchers)
            self.bookingTableView.reloadData()
        }
    }
    
    func userVoucherSelectedWith(voucherList:[VoucherCouponsData], type: ScreenType, termsAndConditions:[String]) {
        if type == .CouponsForYou {
            self.didAddCoupons = true
            self.appliedCoupons = voucherList
            self.couponsTerms = termsAndConditions
            let couponData = voucherList[0] // Only one coupon can apply.
            var currentAmount = self.masterSubTotal
            currentAmount = currentAmount < 0.00 ? 0.00 : currentAmount
            let adjustedDenomination = self.slotsViewModel.consumedCouponAmount(denomination: couponData.denomination ?? 0.00,
                                                                                payAmount: currentAmount)
            self.resetPoints()
            self.appliedCouponAmount = adjustedDenomination
            self.slotsViewModel.adjustTotalAmountForPayment(with: adjustedDenomination,
                                                            isAddition: true,
                                                            offerType: .Coupons)
            self.isTaxSplitRefreshNeeded = true
            AnalyticsHelper.shared.triggerEvent(type: .R07A)
            self.bookingTableView.reloadData()
        }
        if type == .VouchersForYou {
            self.resetPoints()
            self.resetCards()
            
            self.didAddVouchers = true
            self.appliedVouchers = voucherList
            self.vouchersTerms = termsAndConditions
            
            let totalVoucherAmount = self.slotsViewModel.totalVoucherAmount(for: self.appliedVouchers!)
            self.appliedVoucherAmount = totalVoucherAmount.amount
            self.appliedVoucherPacks = totalVoucherAmount.packs
            countVoucherPack()
            self.slotsViewModel.adjustTotalAmountForPayment(with: totalVoucherAmount.amount,
                                                            isAddition: true,
                                                            offerType: .Vouchers)
            AnalyticsHelper.shared.triggerEvent(type: .R09A)
            self.isTaxSplitRefreshNeeded = true
            self.bookingTableView.reloadData()
        }
    }
    
    
    func countVoucherPack(){
        var TempSubTotal: Double = 0.0
        for amount in arrAddedAmounts {
            TempSubTotal += amount
        }
        var voucherAmount: Double = 0.0
        var voucherAddedCopy = arrAddedAmounts
        for _ in 0..<appliedVoucherPacks{
            if voucherAddedCopy.count <= 0 {
                break
            }
            var index = 0
            for i in 0..<voucherAddedCopy.count{
                if voucherAddedCopy[index] < voucherAddedCopy[i]{
                    index = i
                }
            }
            voucherAmount += voucherAddedCopy[index]
            voucherAddedCopy.remove(at: index)
        }
        TempSubTotal -= voucherAmount
        self.masterSubTotal = TempSubTotal
    }
    // TODO: Move to View Model

    
}
extension BBQBookingMasterViewController:BBQBottomSheetCorporateViewControllerDelegate {
    
    func didFinishedEmailValidation(emailId: String, otpID: Int, verifierOTP: Int) {
        
    }

    func didFinishSelectingCorporateOffer(corporateOffer: CorporateOffer) {
        self.corpBottomSheet?.dismissBottomSheet(on: self,
                                                 completion: { (isDismissed) in
                                                    self.didAppliedCorpOffers = true
                                                    self.resetPoints()
                                                    self.slotsViewModel.adjustTotalAmountForPayment(with: corporateOffer.corporateDenomination,
                                                                                                    isAddition: true,offerType: .CorporateOffers)
                                                    self.appliedCorpOffers =  corporateOffer
                                                    self.isTaxSplitRefreshNeeded = true
                                                    AnalyticsHelper.shared.triggerEvent(type: .R11A)
                                                    self.adjustBottomSheetComponent()
        })
    }
}
