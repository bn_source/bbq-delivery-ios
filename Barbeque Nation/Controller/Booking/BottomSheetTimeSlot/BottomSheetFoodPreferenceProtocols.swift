//
//  BottomSheetFoodPreferenceProtocols.swift
//  BBQApp
//
//  Created by Sridhar on 01/08/19.
//  Copyright © 2019 Sridhar. All rights reserved.
//

import UIKit

protocol BottomSheetTimeSlotDataSource: class {
    func numberOfItems() -> Int
    func textForPriceAt(row rowIndex: Int) -> String
    func textForFoodAt(row rowIndex: Int) -> String
    func imageForBackgroundAt( row rowIndex: Int) -> UIImage
}

extension BottomSheetTimeSlotDataSource {
    
}

protocol BottomSheetTimeSlotDelegate: class {
    func didSelectedRowAt(_ index: Int)
}
