//
//  BottomSheetTimeSlotTableViewCell.swift
//  BBQApp
//
//  Created by Sridhar on 01/08/19.
//  Copyright © 2019 Sridhar. All rights reserved.
//

import UIKit

protocol BottomSheetBookingFoodTypeTableViewCellProtocol: class {
    func addButtonPressed(forRow index: Int, forAmount amountvalue: Int, forPeople numberOfpeople: Int)
    func minusButtonPressed(forRow index: Int, forAmount amountValue: Int, forPeople numberOfpeople: Int)
    func showMenuPressed(forRow index: Int)
    func getunitPrice(forRow index:Int)->Int
    func getfoodtypeColor(forRow index:Int)->UIColor
}

class BottomSheetTimeSlotTableViewCell: UITableViewCell {

    @IBOutlet weak var foodtypeImage: UIImageView!
    
    @IBOutlet weak var foodtypeLabel: UILabel!
    
    @IBOutlet weak var menuImage: UIImageView!
    
    @IBOutlet weak var menuButton: UIButton!
    
    @IBOutlet weak var minusButton: UIButton!
    
    @IBOutlet weak var numberofPersonLabel: UILabel!
    
    @IBOutlet weak var addButton: UIButton!
    
    @IBOutlet weak var unitPriceLabel: UILabel!
    
    @IBOutlet weak var subtotalPriceLabel: UILabel!
    
    weak var delegate: BottomSheetBookingFoodTypeTableViewCellProtocol? = nil
    
    var numberofPeopleForFood : Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func resetValues()
    {
        numberofPeopleForFood = 0
    }
    
    @IBAction func minusPressed(_ sender: UIButton) {
        if numberofPeopleForFood == 0 {
            return
        }
        numberofPeopleForFood -= 1
        self.numberofPersonLabel.text = String(numberofPeopleForFood)
        let valueRow = sender.tag
        let colorValue = delegate?.getfoodtypeColor(forRow: valueRow)
        self.numberofPersonLabel.textColor = colorValue
        let unitPrice = delegate?.getunitPrice(forRow: valueRow)
        let unitPriceTotal = unitPrice! * numberofPeopleForFood
        let rupee = "\u{20B9} "
        self.subtotalPriceLabel.text = rupee + String(unitPriceTotal)
        delegate?.minusButtonPressed(forRow: valueRow, forAmount: unitPriceTotal, forPeople: numberofPeopleForFood)
    }
    @IBAction func addPressed(_ sender: UIButton) {
        numberofPeopleForFood += 1
        self.numberofPersonLabel.text = String(numberofPeopleForFood)
        let valueRow = sender.tag
        let colorValue = delegate?.getfoodtypeColor(forRow: valueRow)
        self.numberofPersonLabel.textColor = colorValue
        let unitPrice = delegate?.getunitPrice(forRow: valueRow)
        let unitPriceTotal = unitPrice! * numberofPeopleForFood
        let rupee = "\u{20B9} "
        self.subtotalPriceLabel.text = rupee + String(unitPriceTotal)
        delegate?.addButtonPressed(forRow: valueRow, forAmount: unitPriceTotal, forPeople: numberofPeopleForFood)
    }
    
    @IBAction func menuPressed(_ sender: UIButton) {
        let valueRow = sender.tag
        delegate?.showMenuPressed(forRow: valueRow)
    }
}
