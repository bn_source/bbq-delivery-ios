//
 //  Created by Sridhar on 21/08/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BottomSheetBookingFoodTypeProtocols.swift
 //

import UIKit

protocol BottomSheetBookingFoodTypeDataSource: class {
    func numberOfItems() -> Int
    //func textForFoodAt(row rowIndex: Int) -> String
    //func textForPriceAt(row rowIndex: Int) -> String
    //func imageForBackgroundAt( row rowIndex: Int) -> UIImage
    func textColorForFoodAt(row rowIndex: Int) -> UIColor
    func imageForFoodTypeAt( row rowIndex: Int) -> UIImage
}

extension BottomSheetBookingFoodTypeDataSource {
    
}

protocol BottomSheetBookingFoodTypeDelegate: class {
    func didSelectedRowAt(_ index: Int)
    func didSelectedAddButtonRowAt(_ index:Int)
    func didSelectedMinusButtonRowAt(_ index:Int)
}

protocol BottomSheetDateTimeDataSource: class {
    func numberOfSlotItems() -> Int
    func textForDateTimeAt(row rowIndex: Int) -> String
}

protocol BottomSheetDateTimeDelegate: class {
    func didSelectedRowAt(_ index: Int)
}

