//
 //  Created by Sridhar on 06/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified TaxDetailsView.swift
 //

import UIKit
import Foundation
class TaxDetailsView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBOutlet weak var taxnameLabel: UILabel!
    @IBOutlet weak var taxvalueLabel: UILabel!
}
