//
//  BottomSheetTimeSlotViewController.swift
//  BBQApp
//
//  Created by Sridhar on 01/08/19.
//  Copyright © 2019 Sridhar. All rights reserved.
//

import UIKit
import Razorpay
enum FoodTypeBottomSheetType {
    case Food,Coupon,TimeSlots
}
class BottomSheetTimeSlotViewController: UIViewController,BottomSheetBookingFoodTypeTableViewCellProtocol,BottomSheetBookingDateTimeTableViewCellProtocol {
    //Outlet View
    @IBOutlet weak var outletView: UIView!
    //Outlet Reserve label Eg: "Reserve a table"
    @IBOutlet weak var reserveLabel: UILabel!
    //Outlet label Eg: "Indiranager"
    @IBOutlet weak var outletLabel: UILabel!
    //Outlet Image
    @IBOutlet weak var outletImage: UIImageView!
    //Date label to show selected date from Calendar
    @IBOutlet weak var dateLabel: UILabel?
    //Time label to show selected Time slot from tableview of timeslots
    @IBOutlet weak var timeLabel: UILabel!
    //Change DateTime : hence goto Previous view
    @IBOutlet weak var changeDateTimeButton: UIButton!
    //Outlet change Button
    @IBOutlet weak var changeoutletButton: UIButton!
    //Date and Time UIView
    @IBOutlet weak var datetimeView: UIView!
    //Today Button : upon selection to show Today date in dateLabel
    @IBOutlet weak var todayButton: UIButton!
    //Tomorrow Button : upon selection to show Tomorrow date in dateLabel
    @IBOutlet weak var tomorrowButton: UIButton!
    //Pick a Date Button : upon selection to show FSCalendar
    @IBOutlet weak var pickadateButton: UIButton!
    //Pick a Date image
    @IBOutlet weak var pickadateImage: UIImageView!
    //Time Slots TableView to show slots Eg: 12:00pm,12:30pm etc
    @IBOutlet weak var timeslotTableView: UITableView!
    //Next Button : Upon selection to show foodtype options
    @IBOutlet weak var nextButton: UIButton!
    //Array data for time slot Eg: 12:00pm,12:30pm etc
    var timeslotpickerData: [String] = [String]()
    //TimeSlot view consists of Today,Tomorrow,PickaDate,Next button and TimeSlot Tableview
    @IBOutlet weak var timeslotView: UIView!
    //Food Type view consists of Previous button and FoodType Tableview
    @IBOutlet weak var foodtypeView: UIView!
    //FoodType TableView to show foodtype Eg: Veg/NonVeg/Kids etc
    @IBOutlet weak var foodtypeTableView: UITableView!
    //Previous Button : Upon selection to show timeslotView
    @IBOutlet weak var previousButton: UIButton!
    //ContainerView to hold all subviews related to Booking
    @IBOutlet var containerView: UIView!
    //Cell identifier for FoodType TableViewCell
    fileprivate let cellId = Constants.CellIdentifiers.foodTypeCell
    //Cell identifier for DateTime TableViewCell
    fileprivate let timecellId = Constants.CellIdentifiers.timeSlotCell
    //BottomSheet type for loading either Food,TimeSlot,Coupons
    var bottomSheetType: FoodTypeBottomSheetType = .TimeSlots
    //DataSource for FoodType
    weak var dataSourceFood: BottomSheetBookingFoodTypeDataSource? = nil
    //Deleagte for FoodType
    weak var delegateFood: BottomSheetBookingFoodTypeDelegate? = nil
    //DataSource for TimeSlots
    weak var dataSourceTimeSlot: BottomSheetDateTimeDataSource? = nil
    //Deleagte for TimeSlots
    weak var delegateTimeSlot: BottomSheetDateTimeDelegate? = nil
    
    //ReserveBooking view consists of comuptation of amount for selected food type and corresponding people along with GST & Service Charge computation also consist of BBQ points to redeem, Coupons/Vouchers and Corporate Offers to apply and Reserve Table button
    @IBOutlet weak var reservebookingView: UIView!
    //SubTotal Label
    @IBOutlet weak var subtotalLabel: UILabel!
    //subtotalValueLabel to show amount of number of people added
    @IBOutlet weak var subtotalValueLabel: UILabel!
    //Tax details View
    @IBOutlet weak var gstView: UIView!
    //Tax details TableView
    //@IBOutlet weak var gstTableView: UITableView!
    //gst Label
    @IBOutlet weak var gstLabel: UILabel!
    //gstValueLabel to show amount of GST for subTotal amount
    @IBOutlet weak var gstValueLabel: UILabel!
    //service charge Label
    @IBOutlet weak var servicechargeLabel: UILabel!
    //servicechargeValueLabel to show amount of Service Charge applicable
    @IBOutlet weak var servicechargeValueLabel: UILabel!
    //Grey color line separator
    @IBOutlet weak var separatorView: UIView!
    //BBQ Points UIView
    @IBOutlet weak var bbqpointsView: UIView!
    //BBQ points image
    @IBOutlet weak var bbqnpointsImage: UIImageView!
    //BBQ Points Label
    @IBOutlet weak var bbqpointsLabel: UILabel!
    //BBQ points to button
    @IBOutlet weak var bbqnpointsButton: UIButton!
    //BBQ points value to be redeem
    @IBOutlet weak var bbqnpointsValueLabel: UILabel!
    //Coupons UIView
    @IBOutlet weak var couponsView: UIView!
    //Coupons/Vouchers image
    @IBOutlet weak var couponsImage: UIImageView!
    //Coupons Label
    @IBOutlet weak var couponsLabel: UILabel!
    //Coupons Button : upon selection add the Coupons
    @IBOutlet weak var couponsButton: UIButton!
    //Coupons value labelled as "add" or "remove"
    @IBOutlet weak var couponsValueLabel: UILabel!
    //Coupons disclaimer or information label
    @IBOutlet weak var couponsInfoLabel: UILabel!
    //Coupons selected tick mark image
    @IBOutlet weak var couponstickImage: UIImageView!
    //Coupons term & condition button
    @IBOutlet weak var couponstermButton: UIButton!
    //Vouchers UIView
    @IBOutlet weak var vouchersView: UIView!
    //Vouchers image
    @IBOutlet weak var vouchersImage: UIImageView!
    //Vouchers Label
    @IBOutlet weak var vouchersLabel: UILabel!
    //Vouchers Button : upon selection add the Vouchers
    @IBOutlet weak var voucherButton: UIButton!
    //Vouchers value labelled as "add" or "remove"
    @IBOutlet weak var voucherValueLabel: UILabel!
    //Vouchers disclaimer or information label
    @IBOutlet weak var vouchersInfoLabel: UILabel!
    //Vouchers selected tick mark image
    @IBOutlet weak var voucherstickImage: UIImageView!
    //Vouchers term & condition button
    @IBOutlet weak var voucherstermButton: UIButton!
    //Label to Show Estimated total value based on calculations
    @IBOutlet weak var estimatedtotalValueLabel: UILabel!
    //Reserve a table Button : upon selection API call to book a table
    @IBOutlet weak var reservetableButton: UIButton!
    //PresentingContoller of type BottomSheetController tpye
    private var presentingController =  BottomSheetController()
    //Bool value to check bottomsheet is expanded or collapse state
    var isExpand = false
    //Booking ScrollView view containing of all UI components
    @IBOutlet weak var bookingScrollView: UIScrollView!
    //Booking UIView view containing of all UI components
    @IBOutlet weak var bookingMainView: UIView!
    //Corporate Image
    @IBOutlet weak var corporateImage: UIImageView!
    //Corporate Label
    @IBOutlet weak var corporateLabel: UILabel!
    //Corporate Button : upon selection load BottomSheet of Corporate Offers
    @IBOutlet weak var corporateButton: UIButton!
    //Corporate value labelled as "add" or "remove"
    @IBOutlet weak var corporateValueLabel: UILabel!
    //Corporate disclaimer or information label
    @IBOutlet weak var corporateinfoLabel: UILabel!
    //Corporate selected tick mark image
    @IBOutlet weak var corporatetickImage: UIImageView!
    //Corporate term & condition button
    @IBOutlet weak var corporatetermsButton: UIButton!
    //BookingViewModel variable
    var bookingViewModel: BookingViewModel?
    //Corporate View consist of Image, Corporate Offers button add/remove label
    @IBOutlet weak var corporateView: UIView!
    //Estimate View consist of Estimated amount
    @IBOutlet weak var estimateView: UIView!
    //Estimate condition information label
    @IBOutlet weak var estimateconditionLabel: UILabel!
    //Variable declaration
    var slotsAvailableArray:[SlotsAvailable] = []
    var preferredBranchDetails: [PreferredBranch] = []
    var nearbyBranchDetails: [NearByBranches] = []
    var amenitiesArray:[Amenities] = []
    var buffetsArray = [Buffets]()
    var buffetdataArray = [BuffetData]()
    var buffetItemsArray = [BuffetItems]()
    var selectedSlotId : Int?
    var selectedPacks : Int?
    var selectedReservationTime : String?
    var packDetails = [[String:Any]]()
    var bookingDetails = [[String:Int]]()
    var details = [String: Int]()
    var taxdetailsArray:[Taxes] = []
    var csgstAmount : Int = 0
    var gstDetailsView: TaxDetailsView!
    
    var selectedMenuIndex: Int = 0
    
    @IBOutlet weak var reserveLabelLeadingSpace: NSLayoutConstraint!
    
    @IBOutlet weak var vouchersViewHeight: NSLayoutConstraint!
    @IBOutlet weak var couponsViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var estimateViewTop: NSLayoutConstraint!
    @IBOutlet weak var corporateViewTop: NSLayoutConstraint!
    @IBOutlet weak var corporateViewHeight: NSLayoutConstraint!
    @IBOutlet weak var vouchersViewTop: NSLayoutConstraint!
    @IBOutlet weak var foodtypeTableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var couponsviewTop: NSLayoutConstraint!
    @IBOutlet weak var gstViewHeight: NSLayoutConstraint!
    @IBOutlet weak var bbqpointsViewHeight: NSLayoutConstraint!
    @IBOutlet weak var gstViewBottom: NSLayoutConstraint!
    
    @IBOutlet weak var bbqpointsButtonTop: NSLayoutConstraint!
    // MARK: - Delegate/Protocol for changed date from Calendar
    func updateDateFromCalendar(_ model: BookingViewModel) {
        self.dateLabel?.text = model.pickaDate_Value
    self.loadSlotsForSelectedDate((bookingViewModel?.getSelectedDateForAPI())!)
    }
    // MARK: - Delegate/Protocol for updating the Unit Price ,Color  & Menu selected
    func getunitPrice(forRow index: Int)-> Int {
        return (Int(self.buffetdataArray[index].buffetPrice!))
    }
    func getfoodtypeColor(forRow index:Int)->UIColor {
         let colorValue = BBQBookingUtilies.shared.getDisplayColorForFoodType(self.buffetdataArray[index].buffetType!)
        return colorValue!
    }
    func showMenuPressed(forRow index: Int) {
        //Show Menu for food eg Veg/NonVeg/Kids
        self.selectedMenuIndex = index
        self.buffetItemsArray = self.buffetdataArray[index].buffetItems!
        showMenuBottomSheet()
    }
    //Stubs for TableViewCell delegate methods of TimeSlot TableViewCell
    func addButtonPressed(forRow index: Int, forAmount amountvalue: Int, forPeople numberOfpeople: Int) {
        //Loads the Reservation View after user select number of people in either Veg/NonVeg/Kids type
        setupUIForReserving()
        packDetails[index].updateValue(numberOfpeople, forKey: Constants.BBQBookingStatus.packsCount)
        setupBookingPacks(forRow: index)
        bookingViewModel?.setupBookedPacksData(forRow: index, forPerson: numberOfpeople)
        var updatedAmountValue: Int? = 0
        var eachTotal: Double? = 0.0
        for j in 0...self.buffetdataArray.count-1
        {
            let unitvalue : Double = self.buffetdataArray[j].buffetPrice!
            let numberofperson:Int = packDetails[j][Constants.BBQBookingStatus.packsCount] as! Int //"packs"
            eachTotal = unitvalue * Double(numberofperson)
            let eachtotal = NSNumber(value: eachTotal!).intValue
            updatedAmountValue = eachtotal+updatedAmountValue!
            eachTotal = 0.0
        }
        self.subtotalValueLabel.text = String(updatedAmountValue!)
        
        var cgstValue:Double? = 0.0
        var sgstValue:Double? = 0.0
        //var cgstName:String? = ""
        //var sgstName:String? = ""
        //for n in 0...self.taxdetailsArray.count-1
        //{
        //}
        cgstValue = self.taxdetailsArray[0].taxPercentage
        sgstValue = self.taxdetailsArray[1].taxPercentage
        let gstValue = Int(cgstValue! + sgstValue!)
        //let gstValue = Int ((bookingViewModel?.getGSTData())!)
        let gstAmount = updatedAmountValue! * gstValue / 100
        csgstAmount  = gstAmount/2
        let servicechargeValue = Int ((bookingViewModel?.getServiceChargeData())!)
        let servicechargeAmount = updatedAmountValue! * servicechargeValue / 100
        //self.gstValueLabel.text = String (gstAmount)
        self.servicechargeValueLabel.text = String (servicechargeAmount)
        let bbqpoint = Int (self.bbqnpointsValueLabel.text!)
        self.estimatedtotalValueLabel.text = String (updatedAmountValue! + gstAmount + servicechargeAmount + bbqpoint!)
        selectedPacks = numberOfpeople
        bookingViewModel?.currency_Type = self.buffetdataArray[index].buffetCurrency!
        
        setupGSTUI()
        
    }
    //Subtracts the number of people for specific food Type
    func minusButtonPressed(forRow index: Int, forAmount amountValue: Int, forPeople numberOfpeople: Int) {
        packDetails[index].updateValue(numberOfpeople, forKey: Constants.BBQBookingStatus.packsCount)
        setupUpdateBookingPacks(forRow: index)
        bookingViewModel?.setupUpdateBookedPacksData(forRow: index, forPerson: numberOfpeople)
        var updatedAmountValue: Int? = 0
        var eachTotal: Double? = 0.0
        for j in 0...self.buffetdataArray.count-1
        {
            let unitvalue : Double = self.buffetdataArray[j].buffetPrice!
            let numberofperson:Int = packDetails[j][Constants.BBQBookingStatus.packsCount] as! Int //"packs"
            eachTotal = unitvalue * Double(numberofperson)
            let eachtotal = NSNumber(value: eachTotal!).intValue
            updatedAmountValue = eachtotal+updatedAmountValue!
            eachTotal = 0.0
        }
        self.subtotalValueLabel.text = String(updatedAmountValue!)
        let gstValue = Int ((bookingViewModel?.getGSTData())!)
        let gstAmount = updatedAmountValue! * gstValue / 100
        csgstAmount  = gstAmount/2
        let servicechargeValue = Int ((bookingViewModel?.getServiceChargeData())!)
        let servicechargeAmount = updatedAmountValue! * servicechargeValue / 100
        //self.gstValueLabel.text = String (gstAmount)
        self.servicechargeValueLabel.text = String (servicechargeAmount)
        self.estimatedtotalValueLabel.text = String (updatedAmountValue! + gstAmount + servicechargeAmount)
        let bbqpoint = Int (self.bbqnpointsValueLabel.text!)
        self.estimatedtotalValueLabel.text = String (updatedAmountValue! + gstAmount + servicechargeAmount + bbqpoint!)
        selectedPacks = numberOfpeople
        setupGSTUI()
    }
    // MARK: - Life Cycle Methods
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        
        // Do any additional setup after loading the view.
        self.foodtypeView.isHidden = true
        self.nextButton.isHidden = true
        self.reservebookingView.isHidden = true
        self.taxdetailsArray = (self.bookingViewModel?.getTaxesData())!
        let heightCount = self.taxdetailsArray.count
        let heightValue = CGFloat (heightCount*20)
        self.gstViewHeight.constant = heightValue
        self.bbqpointsView.backgroundColor = .clear
        bbqpointsViewHeight.constant = 25
        self.couponsView.backgroundColor = .clear
        //self.couponsView.frame = CGRect(x: self.couponsView.frame.origin.x, y: self.couponsView.frame.origin.y, width: self.couponsView.frame.width, height: 52)
        couponsViewHeight.constant = 52
        self.vouchersView.backgroundColor = .clear
        //self.vouchersView.frame = CGRect(x: self.vouchersView.frame.origin.x, y: self.vouchersView.frame.origin.y, width: self.vouchersView.frame.width, height: 52)
        vouchersViewHeight.constant = 52
        vouchersViewTop.constant = vouchersViewTop.constant - 10
        self.corporateView.backgroundColor = .clear
        //self.corporateView.frame = CGRect(x: self.corporateView.frame.origin.x, y: self.corporateView.frame.origin.y, width: self.corporateView.frame.width, height: 52)
        corporateViewHeight.constant = 52
        self.reservetableButton.roundCorners(cornerRadius: 15, borderColor: Constants.App.themeColor.orange, borderWidth: 1)
        self.outletLabel.text = bookingViewModel?.branch_name
        self.outletLabel.sizeToFit()
        self.dateLabel?.text = bookingViewModel?.pickaDate_Value
        self.bookingViewModel?.bookedDetailsData.removeAll()
        self.bookingViewModel?.detailsData.removeAll()
        let valuetoCorrect = self.reserveLabel.intrinsicContentSize.width+self.outletLabel.intrinsicContentSize.width+self.outletImage.intrinsicContentSize.width+10
        let actualValue = 375.0 - valuetoCorrect
        if actualValue > 0
        {
            reserveLabelLeadingSpace.constant = CGFloat(actualValue*0.5)
        }
        else {
            reserveLabelLeadingSpace.constant = CGFloat(49)
        }
        //self.view.layoutIfNeeded()
        
        setupInitialUI()
        setupSlotUI()
        setupUI()
        //self.bookingScrollView.frame = CGRect(x: self.bookingScrollView.frame.origin.x, y: self.bookingScrollView.frame.origin.y, width: self.bookingScrollView.frame.width, height: self.timeslotView.frame.height)
        //self.bookingScrollView.contentSize = CGSize(width: self.bookingScrollView.frame.width, height: self.timeslotView.frame.height)presentingController.expandOrCollapse(viewController: self, withHeight: reservebookingView.frame.size.height-149, isExpand: true)
        
        //self.gsttableviewHeight.constant = heightValue
        //setupGSTUI()
        
    }
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        self.foodtypeTableViewHeight?.constant = self.foodtypeTableView.contentSize.height
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
    // MARK: - UI initialisation
    //todo the setting of Today/Tomorrow/Selecta Date from HomePage
    private func setupInitialUI() {
        if bookingViewModel?.todayPressedFlag == true
        {
            self.dateLabel?.text = BBQBookingUtilies.shared.getTodayDate()
            self.todayButton.backgroundColor = Constants.App.themeColor.orange
            self.todayButton.titleLabel?.textColor = UIColor.white
            self.todayButton.roundCorners(cornerRadius: 15, borderColor: Constants.App.themeColor.borderBlack, borderWidth: 1)
            self.todayButton.titleLabel?.font = UIFont.appNunitoSansBoldWith(size: 14.0)
            self.todayButton.isSelected = true
        }
        else if bookingViewModel?.tomorrowPressedFlag == true
        {
            self.dateLabel?.text = BBQBookingUtilies.shared.getTomorrowDate()
            self.tomorrowButton.isSelected = true
            self.tomorrowButton.backgroundColor = Constants.App.themeColor.orange
            self.tomorrowButton.titleLabel?.textColor = UIColor.white
            self.tomorrowButton.roundCorners(cornerRadius: 15, borderColor: Constants.App.themeColor.borderBlack, borderWidth: 1)
            self.tomorrowButton.titleLabel?.font = UIFont.appNunitoSansBoldWith(size: 14.0)
        }
        else if bookingViewModel?.selectadatePressedFlag == true
        {
            self.pickadateButton.isSelected = true
            self.pickadateButton.backgroundColor = Constants.App.themeColor.orange
            self.pickadateButton.titleLabel?.textColor = UIColor.white
            self.pickadateButton.roundCorners(cornerRadius: 15, borderColor: Constants.App.themeColor.borderBlack, borderWidth: 1)
            self.pickadateButton.titleLabel?.font = UIFont.appNunitoSansBoldWith(size: 14.0)
            self.pickadateImage.image = UIImage(named: "icon_calendar_white")
        }
    }
    //Register TimeSlot Tableview
    private func setupSlotUI() {
        self.timeslotTableView.register(UINib(nibName: timecellId, bundle: nil), forCellReuseIdentifier: timecellId)
        self.timeslotTableView.delegate = self
    }
    //Register FoodType Tableview
    private func setupUI() {
        self.foodtypeTableView.register(UINib(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
    }
    //Register GST Tableview
    private func setupGSTUI() {
        for addedviews in self.gstView.subviews {
            addedviews.removeFromSuperview()
        }
        
        var height = 0.0
        for taxdetailsItem in self.taxdetailsArray
        {
            gstDetailsView = Bundle.main.loadNibNamed("TaxDetailsView", owner: self, options: nil)?.first as? TaxDetailsView
            self.gstView.addSubview(gstDetailsView!)
            gstDetailsView?.translatesAutoresizingMaskIntoConstraints = false
            gstDetailsView?.taxnameLabel?.text = taxdetailsItem.tax
            gstDetailsView?.taxvalueLabel?.text = String(csgstAmount)
            
            gstDetailsView?.leadingAnchor.constraint(equalTo: self.gstView.leadingAnchor, constant: 0.0).isActive = true
            gstDetailsView?.trailingAnchor.constraint(equalTo: (self.gstView.trailingAnchor), constant: 0.0).isActive = true
            gstDetailsView?.topAnchor.constraint(equalTo: self.gstView.topAnchor, constant: CGFloat(height)).isActive = true
            
            height = height+20.0
        }
        //height = height
        self.gstViewHeight.constant = CGFloat(height)
        self.gstViewBottom.constant = 0
        //self.gstView.backgroundColor = .red
    }
    // MARK: - Change outlet Pressed
    @IBAction func changeoutletPressed(_ sender: Any) {
        presentingController.dismissBottomSheet(on: self)
        self.dismiss(animated: false) {
            self.callOutletChange()
        }
    }
    // Change outlet Notification
    func callOutletChange()
    {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "changeOuletCall"), object: nil)
    }
    // MARK: - Change DateTime Pressed
    @IBAction func changeDateTimePressed(_ sender: Any) {
        self.timeslotView.isHidden = false
        self.foodtypeView.isHidden = true
        if isExpand
        {
            self.reservebookingView.isHidden = true
            presentingController.expandOrCollapse(viewController: self, withHeight: self.view.frame.height - 420, isExpand: false)
            changeDateTimeButton.isUserInteractionEnabled = false
            isExpand = false
            bookingScrollView.contentSize = CGSize(width: 0, height: 0)
            self.bookingViewModel?.bookedDetailsData.removeAll()
            self.bookingViewModel?.detailsData.removeAll()
        }
        //changeDateTimeButton.isUserInteractionEnabled = false
        timeLabel.frame = CGRect(x: 253, y: 15, width: 97, height: 27)
        bottomSheetType = .TimeSlots
        self.resetSlotUI()
    }
    // MARK: - Next Pressed & Previous Pressed
    //Next Pressed to load FoodType View
    @IBAction func nextPressed(_ sender: Any) {
        self.timeslotView.isHidden = true
        self.foodtypeView.isHidden = false
        changeDateTimeButton.isUserInteractionEnabled = true
        bottomSheetType = .Food
        timeLabel.frame = CGRect(x: 253, y: 15, width: 97, height: 27)
        setupUI()
        foodtypeTableView .reloadData()
        updateSelectedPacksData()
        //setupGSTUI()
        
    }
    //Create a array of Dictioanry to hold number of packs
    func updateSelectedPacksData()
    {
        guard self.buffetdataArray.count != 0 else {
            return
        }
        for j in 0...self.buffetdataArray.count-1
        {
            let details: [String: Any] = ["buffet_type": self.buffetdataArray[j].buffetType!, Constants.BBQBookingStatus.packsCount:0]
            packDetails.append(details)
        }
    }
    //Previous Pressed to load TimeSlot View
    @IBAction func previousPressed(_ sender: Any) {
        self.timeslotView.isHidden = false
        self.foodtypeView.isHidden = true
        timeLabel.frame = CGRect(x: 253, y: 15, width: 97, height: 27)
        bottomSheetType = .TimeSlots
        self.resetSlotUI()
    }
    // MARK: - Reserve View UI setup
    //Reserve View UI setup
    func setupUIForReserving() {
        reservebookingView.isHidden = false
        previousButton.isHidden = true
        //changeDateTimeButton.isUserInteractionEnabled = false
        self.estimateconditionLabel.text = kEstimateConditonTitle + bookingViewModel!.currency_Type
        if !isExpand {
            isExpand = true
            foodtypeTableView.frame = CGRect(x: foodtypeTableView.frame.origin.x, y: foodtypeTableView.frame.origin.y, width: foodtypeTableView.frame.width, height: foodtypeTableView.frame.height + 10)
            //bookingScrollView.frame = CGRect(x: bookingScrollView.frame.origin.x, y: bookingScrollView.frame.origin.y, width: bookingScrollView.frame.width, height: reservebookingView.frame.height+bookingScrollView.frame.height)
            //bookingScrollView.contentSize = CGSize(width: bookingScrollView.frame.width, height: bookingScrollView.frame.height-10)
            bookingScrollView.contentSize = CGSize(width: bookingScrollView.frame.width, height: reservebookingView.frame.origin.y+reservebookingView.frame.height)
            //if let presentingController = self.presentingController {
            //presentingController.expandOrCollapse(viewController: self, withHeight: reservebookingView.frame.size.height, isExpand: true)
            presentingController.expandOrCollapse(viewController: self, withHeight: reservebookingView.frame.size.height-149, isExpand: true)
            //}
            //bottomSheetController.expandOrCollapse(viewController: self, withHeight: 250, isExpand: true)
        }
    }
    // MARK: - BBQ points pressed
    //BBQ points Pressed
    @IBAction func bbqnpointsPressed(_ sender: Any) {
        if bbqnpointsButton.titleLabel?.text == kCouponsVouchersValueAddTitle {
            bbqnpointsButton.setTitle(kCouponsVouchersValueRemoveTitle, for: .normal)
            self.bbqpointsView.backgroundColor = Constants.App.themeColor.corporateBackgroundColor
            self.bbqnpointsValueLabel.isHidden = false
            self.bbqnpointsValueLabel.text = "-250"
            self.bbqpointsViewHeight.constant = 45
            self.bbqpointsButtonTop.constant = 21
        }
        else {
            bbqnpointsButton.setTitle(kCouponsVouchersValueAddTitle, for: .normal)
            self.bbqpointsView.backgroundColor = .clear
            self.bbqnpointsValueLabel.text = "0"
            self.bbqnpointsValueLabel.isHidden = true
            self.bbqpointsViewHeight.constant = 25
            self.bbqpointsButtonTop.constant = 0
        }
    }
    // MARK: - Coupons pressed and terms
    //Coupons Pressed
    @IBAction func couponsPressed(_ sender: Any) {
        
        /*let viewController = BBQAllOffersViewController()
        viewController.screenType = .CouponsForYou
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: self.view.frame.size.height - viewController.view.frame.size.height)
        let bottomSheet = BottomSheetController(configuration: configuration)
        viewController.delegate = self as? BBQAllOffersProtocol
        viewController.bottomSheet = bottomSheet
        bottomSheet.present(viewController, on: self)*/
        //Commented above code is just for testing as below code need to be added based on particluar Coupons added by user
        if couponsButton.titleLabel?.text == kCouponsVouchersValueAddTitle
        {
            couponsButton.setTitle(kCouponsVouchersValueRemoveTitle, for: .normal)
            self.couponsView.backgroundColor = Constants.App.themeColor.corporateBackgroundColor
            //self.corporateView.frame = CGRect(x: self.corporateView.frame.origin.x, y: self.corporateView.frame.origin.y, width: self.corporateView.frame.width, height: 89)
            self.couponsViewHeight.constant = 89
            //self.vouchersViewTop.constant = self.vouchersViewTop.constant + 57
        /*    self.corporateViewTop.constant = self.corporateViewTop.constant + 57
            self.estimateViewTop.constant = self.estimateViewTop.constant + 57
 */
            //self.vouchersView.frame = CGRect(x: self.vouchersView.frame.origin.x, y: self.vouchersView.frame.origin.y + 57, width: self.vouchersView.frame.width, height: self.vouchersView.frame.height)
            /*self.corporateView.frame = CGRect(x: self.corporateView.frame.origin.x, y: self.corporateView.frame.origin.y + 57, width: self.corporateView.frame.width, height: self.corporateView.frame.height)
            self.estimateView.frame = CGRect(x: self.estimateView.frame.origin.x, y: self.estimateView.frame.origin.y + 57, width: self.estimateView.frame.width, height: self.estimateView.frame.height)
            self.estimateconditionLabel.frame = CGRect(x: self.estimateconditionLabel.frame.origin.x, y: self.estimateconditionLabel.frame.origin.y + 57, width: self.estimateconditionLabel.frame.width, height: self.estimateconditionLabel.frame.height)
            self.reservetableButton.frame = CGRect(x: self.reservetableButton.frame.origin.x, y: self.reservetableButton.frame.origin.y + 57, width: self.reservetableButton.frame.width, height: self.reservetableButton.frame.height)*/
            
            self.couponsInfoLabel.isHidden = false
            self.couponstickImage.isHidden = false
            self.couponstermButton.isHidden = false
        }
        else
        {
            couponsButton.setTitle(kCouponsVouchersValueAddTitle, for: .normal)
            self.couponsView.backgroundColor = .clear
            //self.corporateView.frame = CGRect(x: self.corporateView.frame.origin.x, y: self.corporateView.frame.origin.y, width: self.corporateView.frame.width, height: self.corporateView.frame.height-37)
            self.couponsViewHeight.constant = 52
           // self.vouchersViewTop.constant = self.vouchersViewTop.constant - 57
/*            self.corporateViewTop.constant = self.corporateViewTop.constant - 57
            self.estimateViewTop.constant = self.estimateViewTop.constant - 57
*/            //self.vouchersView.frame = CGRect(x: self.vouchersView.frame.origin.x, y: self.vouchersView.frame.origin.y - 57, width: self.vouchersView.frame.width, height: self.vouchersView.frame.height)
           /* self.corporateView.frame = CGRect(x: self.corporateView.frame.origin.x, y: self.corporateView.frame.origin.y - 57, width: self.corporateView.frame.width, height: self.corporateView.frame.height)
            self.estimateView.frame = CGRect(x: self.estimateView.frame.origin.x, y: self.estimateView.frame.origin.y - 57, width: self.estimateView.frame.width, height: self.estimateView.frame.height)
            self.estimateconditionLabel.frame = CGRect(x: self.estimateconditionLabel.frame.origin.x, y: self.estimateconditionLabel.frame.origin.y - 57, width: self.estimateconditionLabel.frame.width, height: self.estimateconditionLabel.frame.height)
            self.reservetableButton.frame = CGRect(x: self.reservetableButton.frame.origin.x, y: self.reservetableButton.frame.origin.y - 57, width: self.reservetableButton.frame.width, height: self.reservetableButton.frame.height)*/
            //presentingController.expandOrCollapse(viewController: self, withHeight: reservebookingView.frame.size.height-484, isExpand: true)
            
            self.couponsInfoLabel.isHidden = true
            self.couponstickImage.isHidden = true
            self.couponstermButton.isHidden = true
        }
    }
    
    @IBAction func couponstermsPressed(_ sender: Any) {
    }
    // MARK: - Vouchers pressed and terms
    //Vouchers Pressed
    @IBAction func vouchersPressed(_ sender: Any) {

        
        let viewController = BBQAllOffersViewController()
        viewController.screenType = .VouchersForYou
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: self.view.frame.size.height - viewController.view.frame.size.height)
        let bottomSheet = BottomSheetController(configuration: configuration)
        viewController.delegate = self
        print(self.bookingViewModel!.bookedDetailsData)
        var coversCount:Int = 0
        for packsData in self.bookingViewModel!.bookedDetailsData{
            let packs = Int(packsData["packs"]!)
            coversCount = coversCount + packs!
        }
        viewController.coversValue = coversCount
        viewController.bottomSheet = bottomSheet
        bottomSheet.present(viewController, on: self)
        //Commented above code is just for testing as below code need to be added based on particluar Vouchers added by user
     /*   if voucherValueLabel.text == kCouponsVouchersValueAddTitle
||||||| .r21810
        if voucherValueLabel.text == kCouponsVouchersValueAddTitle
=======
        if voucherButton.titleLabel?.text == kCouponsVouchersValueAddTitle
>>>>>>> .r21829
        {
            voucherButton.setTitle(kCouponsVouchersValueRemoveTitle, for: .normal)
            self.vouchersView.backgroundColor = Constants.App.themeColor.corporateBackgroundColor
            //self.corporateView.frame = CGRect(x: self.corporateView.frame.origin.x, y: self.corporateView.frame.origin.y, width: self.corporateView.frame.width, height: 89)
            self.vouchersViewHeight.constant = 89
 /*           self.corporateViewTop.constant = self.corporateViewTop.constant + 57
            self.estimateViewTop.constant = self.estimateViewTop.constant + 57
   */         //self.vouchersViewTop.constant = self.vouchersViewTop.constant + 57
            //self.vouchersView.frame = CGRect(x: self.vouchersView.frame.origin.x, y: self.vouchersView.frame.origin.y + 57, width: self.vouchersView.frame.width, height: self.vouchersView.frame.height)
          /*  self.corporateView.frame = CGRect(x: self.corporateView.frame.origin.x, y: self.corporateView.frame.origin.y + 57, width: self.corporateView.frame.width, height: self.corporateView.frame.height)
            self.estimateView.frame = CGRect(x: self.estimateView.frame.origin.x, y: self.estimateView.frame.origin.y + 57, width: self.estimateView.frame.width, height: self.estimateView.frame.height)
            self.estimateconditionLabel.frame = CGRect(x: self.estimateconditionLabel.frame.origin.x, y: self.estimateconditionLabel.frame.origin.y + 57, width: self.estimateconditionLabel.frame.width, height: self.estimateconditionLabel.frame.height)
            self.reservetableButton.frame = CGRect(x: self.reservetableButton.frame.origin.x, y: self.reservetableButton.frame.origin.y + 57, width: self.reservetableButton.frame.width, height: self.reservetableButton.frame.height)*/
            
            self.vouchersInfoLabel.isHidden = false
            self.voucherstickImage.isHidden = false
            self.voucherstermButton.isHidden = false
        }
        else
        {
            voucherButton.setTitle(kCouponsVouchersValueAddTitle, for: .normal)
            self.vouchersView.backgroundColor = .clear
            //self.corporateView.frame = CGRect(x: self.corporateView.frame.origin.x, y: self.corporateView.frame.origin.y, width: self.corporateView.frame.width, height: self.corporateView.frame.height-37)
            self.vouchersViewHeight.constant = 52
 /*           self.corporateViewTop.constant = self.corporateViewTop.constant - 57
            self.estimateViewTop.constant = self.estimateViewTop.constant - 57
  */          //self.vouchersViewTop.constant = self.vouchersViewTop.constant - 57
            //self.vouchersView.frame = CGRect(x: self.vouchersView.frame.origin.x, y: self.vouchersView.frame.origin.y - 57, width: self.vouchersView.frame.width, height: self.vouchersView.frame.height)
         /*   self.corporateView.frame = CGRect(x: self.corporateView.frame.origin.x, y: self.corporateView.frame.origin.y - 57, width: self.corporateView.frame.width, height: self.corporateView.frame.height)
            self.estimateView.frame = CGRect(x: self.estimateView.frame.origin.x, y: self.estimateView.frame.origin.y - 57, width: self.estimateView.frame.width, height: self.estimateView.frame.height)
            self.estimateconditionLabel.frame = CGRect(x: self.estimateconditionLabel.frame.origin.x, y: self.estimateconditionLabel.frame.origin.y - 57, width: self.estimateconditionLabel.frame.width, height: self.estimateconditionLabel.frame.height)
            self.reservetableButton.frame = CGRect(x: self.reservetableButton.frame.origin.x, y: self.reservetableButton.frame.origin.y - 57, width: self.reservetableButton.frame.width, height: self.reservetableButton.frame.height)*/
            //presentingController.expandOrCollapse(viewController: self, withHeight: reservebookingView.frame.size.height-484, isExpand: true)
            
            self.vouchersInfoLabel.isHidden = true
            self.voucherstickImage.isHidden = true
            self.voucherstermButton.isHidden = true
        }
        */
        
    }
    
    @IBAction func voucherstermsPressed(_ sender: Any) {
    }
    // MARK: - Corporate terms and Offers
    @IBAction func corporatetermsPressed(_ sender: Any) {
    }
    
    //Corporate Offers pressed
    @IBAction func corporatePressed(_ sender: Any) {
        
        if corporateButton.titleLabel?.text == kCouponsVouchersValueAddTitle
        {
            corporateButton.setTitle(kCouponsVouchersValueRemoveTitle, for: .normal)
            self.corporateView.backgroundColor = Constants.App.themeColor.corporateBackgroundColor
            //self.corporateView.frame = CGRect(x: self.corporateView.frame.origin.x, y: self.corporateView.frame.origin.y, width: self.corporateView.frame.width, height: 89)
            self.corporateViewHeight.constant = 89
            //self.estimateView.frame = CGRect(x: self.estimateView.frame.origin.x, y: self.estimateView.frame.origin.y + 57, width: self.estimateView.frame.width, height: self.estimateView.frame.height)
            self.estimateViewTop.constant = self.estimateViewTop.constant + 57
            //self.estimateconditionLabel.frame = CGRect(x: self.estimateconditionLabel.frame.origin.x, y: self.estimateconditionLabel.frame.origin.y + 57, width: self.estimateconditionLabel.frame.width, height: self.estimateconditionLabel.frame.height)
            //self.reservetableButton.frame = CGRect(x: self.reservetableButton.frame.origin.x, y: self.reservetableButton.frame.origin.y + 57, width: self.reservetableButton.frame.width, height: self.reservetableButton.frame.height)
   //         presentingController.expandOrCollapse(viewController: self, withHeight: reservebookingView.frame.size.height-354, isExpand: true)
            // TODO:- Maya, make constants for below strings
            let viewController = BBQBottomSheetCorporateViewController()
            viewController.componentTitle = kCorporateOffers
            viewController.componentTitleLogo = UIImage(named:Constants.AssetName.corporateIcon)
            let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: UIScreen.main.bounds.size.height - viewController.view.frame.size.height)
            let bottomSheetController = BottomSheetController(configuration: configuration)

            //Create instance for the controller that will be shown
            
            //Present your controller over current controller
            bottomSheetController.present(viewController, on: self)
     
            self.corporateinfoLabel.isHidden = false
            self.corporatetickImage.isHidden = false
            self.corporatetermsButton.isHidden = false
        }
        else
        {
            corporateButton.setTitle(kCouponsVouchersValueAddTitle, for: .normal)
            self.corporateView.backgroundColor = .clear
            self.corporateViewHeight.constant = self.corporateViewHeight.constant - 37
            //self.corporateView.frame = CGRect(x: self.corporateView.frame.origin.x, y: self.corporateView.frame.origin.y, width: self.corporateView.frame.width, height: self.corporateView.frame.height-37)
            //self.estimateView.frame = CGRect(x: self.estimateView.frame.origin.x, y: self.estimateView.frame.origin.y - 57, width: self.estimateView.frame.width, height: self.estimateView.frame.height)
            self.estimateViewTop.constant = self.estimateViewTop.constant - 57
            //self.estimateconditionLabel.frame = CGRect(x: self.estimateconditionLabel.frame.origin.x, y: self.estimateconditionLabel.frame.origin.y - 57, width: self.estimateconditionLabel.frame.width, height: self.estimateconditionLabel.frame.height)
            //self.reservetableButton.frame = CGRect(x: self.reservetableButton.frame.origin.x, y: self.reservetableButton.frame.origin.y - 57, width: self.reservetableButton.frame.width, height: self.reservetableButton.frame.height)
  //          presentingController.expandOrCollapse(viewController: self, withHeight: reservebookingView.frame.size.height-484, isExpand: true)
            
            self.corporateinfoLabel.isHidden = true
            self.corporatetickImage.isHidden = true
            self.corporatetermsButton.isHidden = true
        }
    }
    
    // MARK: - Reserve Table Pressed
    //Reserve Table pressed : API call to book a table
    @IBAction func reservetablePressed(_ sender: Any) {
        let tokenValue = EnviornmentConfiguration().environment.authorization
        let inValidToken = "Bearer "
        if (tokenValue == inValidToken)
        {
            callLogin()
        }else {
            self.callCreateBooking()
        }
    }
    //Call Login BottomSheet and OTP API
    func callLogin()
    {
        let viewController = UIStoryboard.loadLoginViewController()
        viewController.delegate = self as? LoginControllerDelegate
        viewController.subView?.backgroundColor = .black
        viewController.navigationFromScreen = .Booking
        viewController.shouldCloseTouchingOutside = true
        viewController.currentTheme = .dark
        viewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(viewController, animated: true, completion: nil)
    }
    //Call create booking API
    func callCreateBooking()
    {
        BBQActivityIndicator.showIndicator(view: self.view)
        
        //let slotId = selectedSlotId!
        let branchId = bookingViewModel?.branch_idStringForAPI
        let reservationTime = selectedReservationTime!
        var reservationDate:String = ""
        bookingViewModel?.bbqPointsValue = 0
        if bookingViewModel?.todayPressedFlag == true {
            reservationDate = BBQBookingUtilies.shared.getTodayDateForAPI()
        }else if bookingViewModel?.tomorrowPressedFlag == true {
            reservationDate = BBQBookingUtilies.shared.getTomorrowDateForAPI()
        }else if bookingViewModel?.selectadatePressedFlag == true {
            reservationDate = (bookingViewModel?.getSelectedDateForAPI())!
        }
        bookingViewModel?.slotConfirmation_Id = String (selectedSlotId!)
        let slotId = bookingViewModel?.slotConfirmation_Id
        let filterDetails = bookingDetails.filter { !$0.values.contains(0)}
        bookingDetails = filterDetails
        let filters = bookingViewModel?.bookedDetailsData.filter {!$0.values.contains("0")}
        bookingViewModel?.bookedDetailsData = filters!
        bookingViewModel?.createBooking(branchId: branchId!, slotId: slotId!, reservationDate: reservationDate, reservationTime: reservationTime, bookingDetails: bookingDetails)
        {
            (isSuccess) in
            BBQActivityIndicator.hideIndicator(from: self.view)
            if isSuccess {
                let bookingstatus = self.bookingViewModel?.booking_status
                let statusPending = Constants.BBQBookingStatus.pendingBooking
                let statusValid = Constants.BBQBookingStatus.confirmedBooking
                if (bookingstatus == statusPending)
                {
                    let advancebill:Double = self.bookingViewModel!.advanceAmount
                    let advanceamount = NSNumber(value: advancebill).stringValue
                    //rpController.createOrderForPayment(with: advanceamount , currency: "INR")
                    self.callAdvancePay(advanceamount)
                }
                else if (bookingstatus == statusValid)
                {
                    //Upon BookingStatus == BOOKING_CONFIRMED
                    //Navigate to Confirmation Page
                    self.callConfirmBooking()
                }
            } else {
            }
        }
    }
    
    func setupBookingPacks(forRow indexValue:Int)
    {
        if bookingDetails.count > indexValue
        {
            if bookingDetails[indexValue].keys.contains("buffet_id")
            {
                let existId = bookingDetails[indexValue]["buffet_id"]
                let existStringId = String(existId!)
                if self.buffetdataArray[indexValue].buffetId == existStringId
                {
                    bookingDetails[indexValue][Constants.BBQBookingStatus.packsCount] = (packDetails[indexValue][Constants.BBQBookingStatus.packsCount] as! Int) //"packs"
                    return
                }
            }
        }
        details = ["menu_id":Int(bookingViewModel!.menu_id) ?? 0, "buffet_id": Int(self.buffetdataArray[indexValue].buffetId!) ?? 0, Constants.BBQBookingStatus.packsCount: packDetails[indexValue][Constants.BBQBookingStatus.packsCount] as! Int]
        bookingDetails.append(details)
    }
    
    func setupUpdateBookingPacks(forRow indexValue:Int)
    {
        for details in bookingDetails
        {
            let buffetIdValue = details["buffet_id"]
            let actualbuffetId = Int (self.buffetdataArray[indexValue].buffetId!)
            if buffetIdValue == actualbuffetId
            {
                bookingDetails[indexValue][Constants.BBQBookingStatus.packsCount] = (packDetails[indexValue][Constants.BBQBookingStatus.packsCount] as! Int)
                return
            }
        }
    }
    //Call Confirm Booking view contolller only if BookingStatus is BOOKING_CONFIRMED
    func callConfirmBooking() {
        let storyBoard : UIStoryboard = UIStoryboard(name: Constants.OrderConfirm.orderConfirmStoryBoard,
                                                     bundle:nil)
        let orderConfirmVC =
            storyBoard.instantiateViewController(withIdentifier: Constants.OrderConfirm.orderConfirmPageNibID) as! BBQBookingConfirmationController
        orderConfirmVC.delegate = self
        //orderConfirmVC.databookingModel = bookingViewModel
        self.present(orderConfirmVC, animated:true, completion:nil)
    }
    func calldismissBooking() {
        presentingController.dismissBottomSheet(on: self)
        self.dismiss(animated: true, completion: nil)
    }
    //Call Advance Pay only if BookingStatus is PENDING
    func callAdvancePay(_ advanceAmount: String) {
        //Get instance of BottomSheet handler
//        let bottomSheetController = BottomSheetController()
//        //Create instance for the controller that will be shown
//        let viewController = BBQPaymentBottomSheetController()
//        viewController.bookingViewModel = bookingViewModel
//        viewController.delegate = self
//        //viewController.bookingViewController = self
//        //Present your controller over current controller
//        bottomSheetController.present(viewController, on: self)
    }
    // MARK: - Change date for slots like Today/Tomorrow/Select different Date
    //Today Pressed: show Today button with round selection and set the Date to Current date
    @IBAction func todayPressed(_ sender: UIButton) {
        if self.todayButton.isSelected == false
        {
            self.todayButton.backgroundColor = Constants.App.themeColor.orange//UIColor(red:1.00, green:0.39, blue:0.00, alpha:1.0)
            self.todayButton.titleLabel?.textColor = UIColor.white
            self.todayButton.roundCorners(cornerRadius: 15, borderColor: Constants.App.themeColor.borderBlack, borderWidth: 1)
            self.todayButton.titleLabel?.font = UIFont.appNunitoSansBoldWith(size: 14.0)
            self.todayButton.isSelected = true
            bookingViewModel?.todayPressedFlag = true
            bookingViewModel?.tomorrowPressedFlag = false
            bookingViewModel?.selectadatePressedFlag = false
            if self.tomorrowButton.isSelected == true
            {
                self.tomorrowButton.isSelected = false
                self.tomorrowButton.backgroundColor = .clear
                self.tomorrowButton.titleLabel?.textColor = Constants.App.themeColor.grey
                self.tomorrowButton.roundCorners(cornerRadius: 0, borderColor: .clear, borderWidth: 1)
                self.tomorrowButton.titleLabel?.font = UIFont.appNunitoSansSemiBoldWith(size: 14.0)
            }
            if self.pickadateButton.isSelected == true
            {
                self.pickadateButton.isSelected = false
                self.pickadateButton.backgroundColor = .clear
                self.pickadateButton.titleLabel?.textColor = Constants.App.themeColor.grey
                self.pickadateButton.roundCorners(cornerRadius: 0, borderColor: .clear, borderWidth: 1)
                self.pickadateButton.titleLabel?.font = UIFont.appNunitoSansSemiBoldWith(size: 14.0)
                self.pickadateImage.image = UIImage(named: "BBQCalendarIcon")
            }
        }
        self.dateLabel?.text = BBQBookingUtilies.shared.getTodayDate()
        self.loadSlotsForSelectedDate((BBQBookingUtilies.shared.getTodayDateForAPI()))
    }
    //Tomorrow Pressed: show Tomorrow button with round selection and set the Date to Tomorrow's date
    @IBAction func tomorrowPressed(_ sender: UIButton) {
        if self.tomorrowButton.isSelected == false
        {
            self.tomorrowButton.isSelected = true
            self.tomorrowButton.backgroundColor = Constants.App.themeColor.orange
            self.tomorrowButton.titleLabel?.textColor = UIColor.white
            self.tomorrowButton.roundCorners(cornerRadius: 15, borderColor: Constants.App.themeColor.borderBlack, borderWidth: 1)
            self.tomorrowButton.titleLabel?.font = UIFont.appNunitoSansBoldWith(size: 14.0)
            bookingViewModel?.todayPressedFlag = false
            bookingViewModel?.tomorrowPressedFlag = true
            bookingViewModel?.selectadatePressedFlag = false
            if self.todayButton.isSelected == true
            {
                self.todayButton.isSelected = false
                self.todayButton.backgroundColor = .clear
                self.todayButton.titleLabel?.textColor = Constants.App.themeColor.grey
                self.todayButton.roundCorners(cornerRadius: 0, borderColor: .clear, borderWidth: 1)
                self.todayButton.titleLabel?.font = UIFont.appNunitoSansSemiBoldWith(size: 14.0)
            }
            if self.pickadateButton.isSelected == true
            {
                self.pickadateButton.isSelected = false
                self.pickadateButton.backgroundColor = .clear
                self.pickadateButton.titleLabel?.textColor = Constants.App.themeColor.grey
                self.pickadateButton.roundCorners(cornerRadius: 0, borderColor: .clear, borderWidth: 1)
                self.pickadateButton.titleLabel?.font = UIFont.appNunitoSansSemiBoldWith(size: 14.0)
                self.pickadateImage.image = UIImage(named: "BBQCalendarIcon")
            }
        }
        self.dateLabel?.text = BBQBookingUtilies.shared.getTomorrowDate()
        self.loadSlotsForSelectedDate((BBQBookingUtilies.shared.getTomorrowDateForAPI()))
    }
    //Pick a Date Pressed: show PickaDate button with round selection and show FSCalendar
    @IBAction func pickadatePressed(_ sender: UIButton) {
        if self.pickadateButton.isSelected == false
        {
            self.pickadateButton.isSelected = true
            self.pickadateButton.backgroundColor = Constants.App.themeColor.orange
            self.pickadateButton.titleLabel?.textColor = UIColor.white
            self.pickadateButton.roundCorners(cornerRadius: 15, borderColor: Constants.App.themeColor.borderBlack, borderWidth: 1)
            self.pickadateButton.titleLabel?.font = UIFont.appNunitoSansBoldWith(size: 14.0)
            self.pickadateImage.image = UIImage(named: "icon_calendar_white")
            bookingViewModel?.todayPressedFlag = false
            bookingViewModel?.tomorrowPressedFlag = false
            bookingViewModel?.selectadatePressedFlag = true
            if self.todayButton.isSelected == true
            {
                self.todayButton.isSelected = false
                self.todayButton.backgroundColor = .clear
                self.todayButton.titleLabel?.textColor = Constants.App.themeColor.grey
                self.todayButton.roundCorners(cornerRadius: 0, borderColor: .clear, borderWidth: 1)
                self.todayButton.titleLabel?.font = UIFont.appNunitoSansSemiBoldWith(size: 14.0)
            }
            if self.tomorrowButton.isSelected == true
            {
                self.tomorrowButton.isSelected = false
                self.tomorrowButton.backgroundColor = .clear
                self.tomorrowButton.titleLabel?.textColor = Constants.App.themeColor.grey
                self.tomorrowButton.roundCorners(cornerRadius: 0, borderColor: .clear, borderWidth: 1)
                self.tomorrowButton.titleLabel?.font = UIFont.appNunitoSansSemiBoldWith(size: 14.0)
            }
        }
        bookingViewModel?.selectadateFromHomeorBookingFlag = false
        showCalendarBottomSheet()
    }
    //Load slots for selected Date from Today/Tomorrow/Pick a Date from calendar
    func loadSlotsForSelectedDate(_ selectedDate: String) {
        self.resetSlotUI()
        let selectDate = selectedDate
        let branchId = BBQLocationManager.shared.outlet_branch_id
        let dinnerType = "Lunch"
        bookingViewModel?.getbookingslotswithBranch(reservationDate: selectDate, branchId: branchId, dinnerType: dinnerType) { (isSuccess) in
            BBQActivityIndicator.hideIndicator(from: self.view)
            if isSuccess {
                self.slotsAvailableArray = (self.bookingViewModel?.getSlotsAvailableData())!
                print(self.slotsAvailableArray)
                NotificationCenter.default.post(name: Notification.Name(rawValue: "changeDateCall"), object: BookingViewModel.self)
                self.timeslotTableView.reloadData()
            } else {
            }
        }
    }
    //Reset Slot UI
    func resetSlotUI()
    {
        if let index = self.timeslotTableView.indexPathForSelectedRow {
            self.timeslotTableView.deselectRow(at: index, animated: true)
            let cell = self.timeslotTableView.cellForRow(at: index) as? BottomSheetDateTimeTableViewCell
            cell!.contentView.backgroundColor = .clear
            cell!.contentView.roundCorners(cornerRadius: 0, borderColor: .clear, borderWidth: 0)
            cell?.slotLabel.textColor = Constants.App.themeColor.grey
            cell?.slotLabel.font = UIFont.appNunitoSansSemiBoldWith(size: 14.0)
            timeLabel.frame = CGRect(x: 253, y: timeLabel.frame.origin.y, width: 97, height: timeLabel.frame.height)
            timeLabel.font = UIFont.appNunitoSansLightWith(size: 20.0)
            timeLabel.text = Constants.BBQBookingStatus.selectTime
            timeLabel.textColor = Constants.App.themeColor.selectTimeColor
            self.nextButton.isHidden = true
        }
    }
    // MARK: - Calendar BottomSheet
    private func showCalendarBottomSheet() {
        //Get instance of BottomSheet handler
        let bottomSheetController = BottomSheetController()
        //Create instance for the controller that will be shown
        let viewController = BottomSheetCalendarViewController()
        //viewController.bookingViewModel = bookingViewModel
       // viewController.bookingViewController = self
        //Present your controller over current controller
        bottomSheetController.present(viewController, on: self)
    }
    // MARK: - Show Menu BottomSheet
    private func showMenuBottomSheet() {
        //Get instance of BottomSheet handler
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: 150.0)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        //Create instance for the controller that will be shown
        let viewController = BottomSheetMenuViewController()
        //viewController.dataSource = self
        //Present your controller over current controller
        bottomSheetController.present(viewController, on: self)
    }
}
// MARK: - MenuBottomSheet
//MenuBottomSheet TableView Cell DataSource
/*extension BottomSheetTimeSlotViewController: MenuBottomSheetDataSource {
    func numberOfMenuItems() -> (menuName: [String], isVeg: [Bool]) {
        var menuArray = [String]()
        var isVegArray = [Bool]()
        menuArray.append(self.buffetdataArray[self.selectedMenuIndex].buffetName ?? "")
        
        if self.buffetdataArray[self.selectedMenuIndex].buffetType == "NONVEG" {
            isVegArray.append(false)
        } else {
            isVegArray.append(true)
        }
        
        return (menuName: menuArray, isVeg: isVegArray)
    }
    
    func menuCategoryForMenuAt(index menuIndex: Int) -> [String] {
        var categoryArray = [String]()
        let buffetData = self.buffetdataArray[self.selectedMenuIndex]
        if let buffetItems = buffetData.buffetItems {
            for buffetItem in buffetItems {
                categoryArray.append(buffetItem.buffetItems_name ?? "")
            }
        }
        
        return categoryArray
    }
    
    func numberOfImagesFor(menu: Int, menuCategory: Int) -> Int {
        return buffetdataArray[self.selectedMenuIndex].buffetItems?[menuCategory].buffetItems_menuItems?.count ?? 0
    }
    
    func menuDataAt(row rowNumber: Int, forMenuIndex: Int, forMenuCategoryIndex: Int) -> (menuName: String, menuDescription: String, menuImage: String?, isVeg: Bool, tagImagePath: String?) {
        let menuData = buffetdataArray[self.selectedMenuIndex].buffetItems?[forMenuCategoryIndex].buffetItems_menuItems?[rowNumber]
        var menuIsVeg = true
        if menuData?.menuitems_Type == "NONVEG" {
            menuIsVeg = false
        }
        let tagImagePath = menuData?.menuitems_Tags?.tagImage
        return (menuName: menuData?.menuitems_Name ?? "",menuDescription:menuData?.menuitems_Description ?? "", menuImage: menuData?.menuitems_Image ?? "", isVeg: menuIsVeg, tagImagePath: tagImagePath)
    }
}*/

extension BottomSheetTimeSlotViewController: BBQAllOffersProtocol {
   
    func selectedCorporateOffer(corporateOffer: CorporateOffer) {
        
    }
    
    func buyNowButtonClicked(bottomSheet: BottomSheetController?) { }
    
    func addButtonClickedAt(index: Int) {
        
    }
    func userVoucherSelectedWith(voucherList:[VoucherCouponsData]){
        //update booking with user selected vouchers
        self.bookingViewModel?.addVoucherDetailsToBookingData(vouchers: voucherList)
    }

}

// MARK: - FoodType TableView Cell and TimeSlots TableView Cell DataSource
//FoodType TableView Cell and TimeSlots TableView Cell DataSource
extension BottomSheetTimeSlotViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == timeslotTableView
        {
            guard let dataSource = self.dataSourceTimeSlot else {
                return 0
            }
            return dataSource.numberOfSlotItems()
        }else if tableView == foodtypeTableView
        {
            guard self.dataSourceFood != nil else {
                return 0
            }
            return self.buffetdataArray.count
        }/*else if tableView == gstTableView
        {
            guard self.taxdetailsArray.count != 0 else {
                return 0
            }
            return self.taxdetailsArray.count
        }*/
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == timeslotTableView
        {
            return 1
        }else if tableView == foodtypeTableView
        {
            return 1
        }/*else if tableView == gstTableView
        {
            return 1
        }*/
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == foodtypeTableView  {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? BottomSheetTimeSlotTableViewCell else {
                return UITableViewCell()
            }
            cell.minusButton.tag = indexPath.section
            cell.addButton.tag = indexPath.section
            cell.delegate = self as BottomSheetBookingFoodTypeTableViewCellProtocol
            guard self.dataSourceFood != nil else {
                return UITableViewCell()
            }
            cell.foodtypeLabel.text = self.buffetdataArray[indexPath.section].buffetName
            cell.foodtypeLabel.sizeToFit()
            let unitPrice:Double = self.buffetdataArray[indexPath.section].buffetPrice!
            let unitPriceString = NSNumber(value: unitPrice).stringValue
            cell.unitPriceLabel.text = unitPriceString
            let rupee = Constants.CurrencySymbol.rupeeSymbol
            cell.subtotalPriceLabel.text = rupee + unitPriceString
            let imageValue = BBQBookingUtilies.shared.getDisplayImageForFoodType( self.buffetdataArray[indexPath.section].buffetType!)
            let colorValue = BBQBookingUtilies.shared.getDisplayColorForFoodType(self.buffetdataArray[indexPath.section].buffetType!)
            cell.foodtypeImage.image = UIImage(named: imageValue)
            cell.foodtypeLabel.textColor = colorValue
            cell.selectionStyle = .none
            bookingViewModel!.currency_Type = self.buffetdataArray[indexPath.section].buffetCurrency!
            cell.menuButton.tag = indexPath.section
            cell.numberofPersonLabel.text = "0"
            cell.resetValues()
            cell.numberofPersonLabel.textColor = Constants.App.themeColor.grey
            return cell
        }
        else if tableView == timeslotTableView {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: timecellId, for: indexPath) as? BottomSheetDateTimeTableViewCell else {
                return UITableViewCell()
            }
            guard let dataSourceTimeSlot = self.dataSourceTimeSlot else {
                return UITableViewCell()
            }
            let dateFromCall = dataSourceTimeSlot.textForDateTimeAt(row: indexPath.section)
            let dateToDisplay = BBQBookingUtilies.shared.getDisplayTimeIn12HourFormat(dateFromCall)
            cell.slotLabel.text = dateToDisplay
            cell.selectionStyle = .none
            return cell
        } /*else if tableView == gstTableView {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: gstcellId, for: indexPath) as? GSTTableViewCell
                else {
                    return UITableViewCell()
            }
            cell.taxnameLabel.text = self.taxdetailsArray[indexPath.section].tax
            cell.taxValueLabel.text = String(self.csgstAmount)
        }*/
        return UITableViewCell()
    }
}
// MARK: - TimeSlots TableView Cell Delegate
//TimeSlots TableView Cell Delegate
extension BottomSheetTimeSlotViewController: UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == timeslotTableView {
            timeLabel.frame = CGRect(x: 253, y: timeLabel.frame.origin.y, width: 97, height: timeLabel.frame.height)
            timeLabel.font = UIFont.appNunitoSansSemiBoldWith(size: 20.0)
            timeLabel.textColor = Constants.App.themeColor.grey
            self.nextButton.isHidden = false
            
            let cell = tableView.cellForRow(at: indexPath) as? BottomSheetDateTimeTableViewCell
            cell!.contentView.backgroundColor = Constants.App.themeColor.orange
            cell!.contentView.roundCorners(cornerRadius: 15, borderColor:Constants.App.themeColor.orange, borderWidth: 1)
            cell?.slotLabel.textColor = .white
            cell?.slotLabel.font = UIFont.appNunitoSansBoldWith(size: 14.0)
            timeLabel.text = cell?.slotLabel.text
            self.slotsAvailableArray = (self.bookingViewModel?.getSlotsAvailableData())!
            BBQActivityIndicator.showIndicator(view: self.view)
            let selectedDate = BBQBookingUtilies.shared.getTodayDateForAPI()
            let branchId = bookingViewModel?.branch_idStringForAPI
            selectedSlotId = self.slotsAvailableArray[indexPath.section].slotId!
            selectedReservationTime = self.slotsAvailableArray[indexPath.section].slotStartTime!
            bookingViewModel?.slotConfirmation_Time = timeLabel.text!
            bookingViewModel?.slotReservationConfirmation_Time = selectedReservationTime!
            bookingViewModel?.getmenubranchBuffet(branchId: branchId!, reservationDate: selectedDate, slotId: selectedSlotId!)
            { (isSuccess) in
                BBQActivityIndicator.hideIndicator(from: self.view)
                if isSuccess {
                    print("Success")
                    self.buffetdataArray = (self.bookingViewModel?.getbuffetdataInfo())!
                    self.buffetItemsArray = (self.bookingViewModel?.getbuffetitemsData())!
                } else {
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if tableView == timeslotTableView {
            let cell = tableView.cellForRow(at: indexPath) as? BottomSheetDateTimeTableViewCell
            cell!.contentView.backgroundColor = .clear
            cell!.contentView.roundCorners(cornerRadius: 0, borderColor: .clear, borderWidth: 0)
            cell?.slotLabel.textColor = Constants.App.themeColor.grey
            cell?.slotLabel.font = UIFont.appNunitoSansSemiBoldWith(size: 14.0)
        }
    }
}
// MARK: - Razor pay and Booking Confirmation
extension BottomSheetTimeSlotViewController: BBQRazorpayControllerProtocol,
BBQBookingConfirmationControllerProtocol,BBQPaymentBottomSheetProtocol {
    func proceedToPaymentPressed(paymentAmount: Double, paymentRemarks: String) {
        let rpController = BBQRazorpayContainerController()
        self.present(rpController, animated: true, completion: nil)
        rpController.payDelegate = self
        //let amount = paymentAmount
//        rpController.createOrderForPayment(with:String(amount), currency:bookingViewModel!.currency_Type, title: "Test Title", and: paymentRemarks)
    }
    
   // MARK:- BBQBookingConfirmationControllerProtocol Methods
    
    func didPressedDoneButton() {
        // Dismissing time slot first, then bottomsheet itself.
        self.dismiss(animated: true, completion: {
            self.presentingController.dismissBottomSheet(on: self) { (isDismissed) in
                NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.NotificationName.bookingConfirmed),
                                                object: nil)
            }
        })
    }
       
    
    // MARK:- BBQRazorpayControllerProtocol Methods
    func onRazorpayPaymentError(_ code: Int32, description str: String) {
        switch code {
        case 0,1,3:
            PopupHandler.showSingleButtonsPopup(title: kFailedTitle,
                                                message: str,
                                                on: self, firstButtonTitle: kOkButtonTitle) { }
        default:
             self.presentingController.dismissBottomSheet(on: self)
        }
    }
    
    func onRazorpayPaymentSuccess(_ paymentData: BBQPaymentDataModel) {
        let razorpayModel = paymentData
        let paymentGateway = Constants.BBQBookingStatus.razorPayment
        BBQActivityIndicator.showIndicator(view: self.view)
        bookingViewModel?.updateBooking(bookingId: bookingViewModel!.booking_id, paymentOrderId: razorpayModel.razorpayOrderID!, paymentId: razorpayModel.paymentID!, paymentGatewayName: paymentGateway, paymentSignature: razorpayModel.razorpaySignature!)
        { (isSuccess) in
            BBQActivityIndicator.hideIndicator(from: self.view)
            if isSuccess {
                print("success call confirmation after razor pay")
                let storyBoard : UIStoryboard = UIStoryboard(name: Constants.OrderConfirm.orderConfirmStoryBoard,
                                                             bundle:nil)
                let orderConfirmVC =
                    storyBoard.instantiateViewController(withIdentifier: Constants.OrderConfirm.orderConfirmPageNibID) as! BBQBookingConfirmationController
                orderConfirmVC.delegate = self
                //orderConfirmVC.databookingModel = self.bookingViewModel
                self.present(orderConfirmVC, animated:true, completion:nil)
            } else {
                
            }
        }
        
    }
}
