//
 //  Created by Sridhar on 23/08/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BottomSheetDateTimeTableViewCell.swift
 //

import UIKit

protocol BottomSheetBookingDateTimeTableViewCellProtocol: class {
}

class BottomSheetDateTimeTableViewCell: UITableViewCell {

    @IBOutlet weak var slotLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
