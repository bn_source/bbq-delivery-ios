/*
 *  Created by Nischitha on 22/08/19.
 *  Copyright © BARBEQUE NATION HOSPITALITY LIMITED 2019. All rights reserved.
 
 * CHANGE LOG
 * —------------------------------------------------------------------------------
 * ID          Bug ID      Author Name          Date                  Description
 * —------------------------------------------------------------------------------
 * 1            NA         Nischitha         22/08/19       Initial Version
 */

import UIKit
import MapKit

class BookingMoreBottomSheet: UIViewController{
    
    var delegate: BookingMoreBottomSheetDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func onClickBtnCab(_ sender: Any) {
        delegate?.onClickBtnCab(view: self)
    }
    @IBAction func onClickBtnDirection(_ sender: Any) {
        delegate?.onClickBtnDirection(view: self)
    }
    @IBAction func onClickBtnCall(_ sender: Any) {
        delegate?.onClickBtnCall(view: self)
    }
    @IBAction func onClickBtnShare(_ sender: Any) {
        delegate?.onClickBtnShare(view: self)
    }
}

protocol BookingMoreBottomSheetDelegate {
    func onClickBtnCab(view: BookingMoreBottomSheet)
    func onClickBtnDirection(view: BookingMoreBottomSheet)
    func onClickBtnCall(view: BookingMoreBottomSheet)
    func onClickBtnShare(view: BookingMoreBottomSheet)
}
