/*
 *  Created by Nischitha on 05/09/19.
 *  Copyright © BARBEQUE NATION HOSPITALITY LIMITED 2019. All rights reserved.
 
 * CHANGE LOG
 * —------------------------------------------------------------------------------
 * ID          Bug ID      Author Name          Date                  Description
 * —------------------------------------------------------------------------------
 * 1            NA         Nischitha         05/09/19       Initial Version
 */

import UIKit

class BBQBookingBuffetCell: UIView {
    @IBOutlet weak var bookingBuffetImage: UIImageView!
    @IBOutlet weak var bookingBuffetNameLabel: UILabel!
}
