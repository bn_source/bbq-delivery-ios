/*
 *  Created by Nischitha on 22/08/19.
 *  Copyright © BARBEQUE NATION HOSPITALITY LIMITED 2019. All rights reserved.
 
 * CHANGE LOG
 * —------------------------------------------------------------------------------
 * ID          Bug ID      Author Name          Date                  Description
 * —------------------------------------------------------------------------------
 * 1            NA         Nischitha         22/08/19       Initial Version
 */

import UIKit
import MapKit

class BookingFailedBottomSheet: UIViewController{
    
    var message: String?
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var viewForCall: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewForCall.roundCorners(cornerRadius: 5.0, borderColor: .theme, borderWidth: 1.0)
        if let message = message{
            lblMessage.text = String(format: "Table reservation failed due to \n%@", message)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let message = message{
            lblMessage.text = String(format: "Table reservation failed due to \n%@", message)
        }
    }
    
    @IBAction func onClickBtnCall(_ sender: Any) {
        let url:NSURL = NSURL(string: Constants.values.customerCareNumber)!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url as URL)
        }
    }
    
}

