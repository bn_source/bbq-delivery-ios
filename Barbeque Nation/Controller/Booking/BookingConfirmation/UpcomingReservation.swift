/*
 *  Created by Nischitha on 12/08/19.
 *  Copyright © BARBEQUE NATION HOSPITALITY LIMITED 2019. All rights reserved.
 
 * CHANGE LOG
 * —------------------------------------------------------------------------------
 * ID          Bug ID      Author Name          Date                  Description
 * —------------------------------------------------------------------------------
 * 1            NA         Nischitha         12/08/19      Initial Version
 */

import UIKit

class UpcomingReservation: UIView {

    //MARK:- IBOutlet
    @IBOutlet weak var bookingIdLabel: UILabel?
    @IBOutlet weak var reservationDateAndTimeLabel: UILabel!
    @IBOutlet weak var placeLabel: UILabel!
    @IBOutlet weak var upcomingReservationTextLabel: UILabel!
    @IBOutlet weak var upcomingReservationBookingImage: UIImageView!
    @IBOutlet weak var upcomingReservationdownArrowImage: UIImageView!
    @IBOutlet weak var upcomingReservationBackgroundImage: UIImageView!
    @IBOutlet weak var eventTypeTextLabel: UILabel!
    
    //MARK:- UI Methods
    func setUpUpcomingReservationFeilds()  {
        upcomingReservationTextLabel.text = kUpcomingReservationTitle
        eventTypeTextLabel.text = kUpcomingReservationEventType
    }
}
