/*
 *  Created by Nischitha on 22/08/19.
 *  Copyright © BARBEQUE NATION HOSPITALITY LIMITED 2019. All rights reserved.
 
 * CHANGE LOG
 * —------------------------------------------------------------------------------
 * ID          Bug ID      Author Name          Date                  Description
 * —------------------------------------------------------------------------------
 * 1            NA         Nischitha         22/08/19      Initial Version
 */

import UIKit

class BookingRedirectCell: UITableViewCell {

    //MARK:- IBOutlet
    @IBOutlet weak var redirectImage: UIImageView!
    @IBOutlet weak var disableBtnView: UIView!
    @IBOutlet weak var redirectLabel: UILabel!
    
     //MARK:- Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    } 
    
}
