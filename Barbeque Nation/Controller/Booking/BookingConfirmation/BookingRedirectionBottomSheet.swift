/*
 *  Created by Nischitha on 22/08/19.
 *  Copyright © BARBEQUE NATION HOSPITALITY LIMITED 2019. All rights reserved.
 
 * CHANGE LOG
 * —------------------------------------------------------------------------------
 * ID          Bug ID      Author Name          Date                  Description
 * —------------------------------------------------------------------------------
 * 1            NA         Nischitha         22/08/19       Initial Version
 */

import UIKit
import MapKit

class BookingRedirectionBottomSheet: UIViewController{
    
    //MARK:- IBOutlet
    @IBOutlet weak var cabIndicatorLabel: UILabel!
    @IBOutlet weak var cabIndicatorImage: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    //MARK:- Properties
    //let bookingViewModel = BookingConfirmationViewModel(bookingID: "")
    var bottomSheetType: ReservationButtonType?
    var isUberAvailable = false
    var isOlaAvailable = false
    var isGoogleMapsAvailable = false
    var latitude: Double?
    var longitude: Double?
    var phoneNumber :String?
    
    
}
extension BookingRedirectionBottomSheet: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        tableView.register(UINib(nibName: "BookingRedirectCell", bundle: nil), forCellReuseIdentifier: "redirectCell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "redirectCell") as! BookingRedirectCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.disableBtnView.alpha = 0.75
        cell.isUserInteractionEnabled = false
        
        switch bottomSheetType {
            
        case .Cab?:
            if indexPath.row == 0 {
                cell.redirectImage.image = UIImage(named: "uber_logo")
                cell.redirectLabel.text = kUberTitle
                if isUberAvailable {
                    cabIndicatorLabel.text = kCabIndicatorTitle
                    cabIndicatorImage.image = UIImage(named: "icon_cab_black")
                    descriptionLabel.text = kCabIndicatorDescriptionTitle
                    cell.isUserInteractionEnabled = true
                    cell.disableBtnView.alpha = 0
                }
            }
            else if indexPath.row == 1 {
                cell.redirectImage.image = UIImage(named: "ola_logo")
                cell.redirectLabel.text = kOlaTitle
                if isOlaAvailable {
                    cabIndicatorLabel.text = kCabIndicatorTitle
                    cabIndicatorImage.image = UIImage(named: "icon_cab_black")
                    descriptionLabel.text = kCabIndicatorDescriptionTitle
                    cell.isUserInteractionEnabled = true
                    cell.disableBtnView.alpha = 0
                }
            }
            return cell
            
        case .Direction?:
            
            cabIndicatorLabel.text = kMapIndicatorTitle
            cabIndicatorImage.image = UIImage(named: "icon_navigation_black")
            descriptionLabel.text = kMapIndicatorDescriptionTitle
            
            
            if indexPath.row == 0 {
                
                cell.redirectLabel.text = kGoogleMapTitle
                cell.redirectImage.image = UIImage(named: "google_map_logo")
                if isGoogleMapsAvailable {
                    
                    cell.isUserInteractionEnabled = true
                    cell.disableBtnView.alpha = 0
                }
                
            }else if indexPath.row == 1 {
                
                cell.redirectLabel.text = kAppleMapTitle
                cell.redirectImage.image = UIImage(named: "apple_map_logo")
                cell.isUserInteractionEnabled = true
                cell.disableBtnView.alpha = 0
            }
            return cell
            
        default:
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch bottomSheetType {
        case .Cab?:
            openCab(index: indexPath.row)
            
        case .Direction?:
            openMap(index: indexPath.row)
            
        default:
            print("")
        }
    }
    
    
    func openCab(index: Int) {
//        let lat = String(latitude)
//        let long = String(longitude)
        switch index {
           
        case 0:
            if let latitude = latitude, let longitude = longitude {
            
                let uberLinkString = "uber://?action=setPickup&dropoff[latitude]=\(latitude)&dropoff[longitude]=\(longitude)&product_id=a1111c8c-c720-46c3-8534-2fcdd730040d&link_text=View%20team%20roster&partner_deeplink=partner%3A%2F%2Fteam%2F9383"
                
                if let url = URL(string: uberLinkString){
                UIApplication.shared.open(url)
                }
                
            }else {
                UIApplication.shared.open(URL(string: "uber://?action=setPickup&dropoff[latitude]= 0.0&dropoff[longitude]=0.0&dropoff[nickname]=Coit%20Tower&dropoff[formatted_address]=1%20Telegraph%20Hill%20Blvd%2C%20San%20Francisco%2C%20CA%2094133&product_id=a1111c8c-c720-46c3-8534-2fcdd730040d&link_text=View%20team%20roster&partner_deeplink=partner%3A%2F%2Fteam%2F9383")!)
            }
            
        case 1:
           if let latitude = latitude, let longitude = longitude {
            let olaLinkString = "olacabs://app/launch?/assets/ola-universal-link.html?&category=compact&utm_source=xapp_token&landing_page=bk&drop_lat=\(latitude)&&drop_lng=\(longitude)&affiliate_uid=12345"
            
            if let url = URL(string: olaLinkString){
                UIApplication.shared.open(url)
            }
            
           }else {
            UIApplication.shared.open(URL(string:"olacabs://app/launch/?lat=12.935&lng=77.614&category=compact&utm_source=12343&drop_lat=0.0&drop_lng= 0.0&dsw=yes")!)
            }
            
        default:
            print("")
        }
        
    }
    
    func openMap(index: Int) {
//        let lat = String(latitude)
//        let long = String(longitude)
        switch index {
        case 0:
            //Fixed by Sakir Sherasiya
            //BookingRedirectionBottomSheet.swift line 166
            //BookingRedirectionBottomSheet.openMap(index:)
            if let latitude = latitude, let longitude = longitude { UIApplication.shared.open(URL(string:"comgooglemaps://?saddr=&daddr=\(latitude),\(longitude)&directionsmode=driving")!)
            }else {
                UIApplication.shared.open(URL(string:"comgooglemaps://?saddr=&daddr=0.0,0.0&directionsmode=driving")!)
            }
            
        case 1:
            let destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: latitude ?? 0.0, longitude:  longitude ?? 0.0)))
            MKMapItem.openMaps(with: [destination], launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
            
        default:
            print("")
        }
    }
}

enum ReservationButtonType {
    case Share, Cancel, Reschedule, Cab, Direction, Phone
}

enum ConfirmationForType {
    case Booking, Reschedule
}
