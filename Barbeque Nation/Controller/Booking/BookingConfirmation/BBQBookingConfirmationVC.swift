/*
 *  Created by Nischitha on 01/08/19.
 *  Copyright © BARBEQUE NATION HOSPITALITY LIMITED 2019. All rights reserved.
 
 * CHANGE LOG
 * —------------------------------------------------------------------------------
 * ID          Bug ID      Author Name          Date                  Description
 * —------------------------------------------------------------------------------
 * 1            NA         Nischitha         01/08/19       Initial Version
 */

import UIKit
import FirebaseAnalytics




class BBQBookingConfirmationVC: UIViewController {
    
    //MARK:- IBOutlet
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblBookingId: UILabel!
    @IBOutlet weak var lblOutletName: UILabel!
    @IBOutlet weak var lblContactNumber: UILabel!
    @IBOutlet weak var viewForOutletInfo: UIView!
    @IBOutlet weak var viewForMenu: UIView!
    @IBOutlet weak var viewForAmbience: UIView!
    @IBOutlet weak var viewForOffers: UIView!
    @IBOutlet weak var viewForReservationDetails: UIView!
    @IBOutlet weak var stackViewForOutletinfo: UIStackView!
    @IBOutlet weak var viewForSpecialRequest: UIView!
    var isPostBookingReservationRequired = true
    var superVC: UIViewController?
    var selectedBranchId: String?

    var menuBranchBuffetData :MenuBranchBuffetDataModel?

    //MARK:- Properties
    @IBOutlet weak var headerViewForGradientColor: UIView!
    @IBOutlet weak var appReferalView: AppReferralView!

    var numberOfPacks: Int = 0
    var bookingID = ""
    lazy var bookingConfirmationViewModel: BookingConfirmationViewModel = {
        let viewModel = BookingConfirmationViewModel(bookingID: bookingID)
        return viewModel
    }()
    
    lazy private var afterDiningViewModel : BBQAfterDiningControllerViewModel = {
        let afterDiningVM = BBQAfterDiningControllerViewModel()
        return afterDiningVM
    } ()
    
    lazy private var outletInfoViewModel : BBQOutletInfoViewModel = {
        let outletVM = BBQOutletInfoViewModel()
        return outletVM
    } ()
    
    var cancellationBS : BBQCancellationBottomSheet?
    var bottomSheetController: BottomSheetController?
    var confirmationForType: ConfirmationForType = .Booking
    
    var slotConfirmationDate: String? = ""
    var slotConfirmationTime: String? = ""
    var eventType:String?
    var cancelDateAndTime:String?
    var specialInstructionsUpdated = false
    var isAdvancedAmountPaid = false
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        AnalyticsHelper.shared.triggerEvent(type: .RC01)
        AnalyticsHelper.shared.screenName(name: "BookingConfirmation", className: "BBQBookingConfirmationVC")
        Analytics.logEvent("Booking", parameters: [
            "name": "Booking" as NSObject,
            "full_text": "BookingConfirmation page" as NSObject
            ])
        
        setUp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(specialInstructionUpdated(_:)), name: Notification.Name(rawValue:Constants.NotificationName.specialInstructionSelected), object:nil)
        NotificationCenter.default.addObserver(self, selector: #selector(specialInstructionNotSet(_:)), name: Notification.Name(rawValue:Constants.NotificationName.specialInstructionNotSet), object:nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue:Constants.NotificationName.specialInstructionSelected), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue:Constants.NotificationName.specialInstructionNotSet), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @objc func specialInstructionUpdated(_ notification: Notification) {
        if let dictionary = notification.userInfo as NSDictionary? {
            eventType = dictionary["celebrationName"] as? String
            specialInstructionsUpdated = true
               self.setUp()
            
        }
    }
    
    @objc func specialInstructionNotSet(_ notification: Notification){
        if let dictionary = notification.userInfo as NSDictionary? {
            eventType = dictionary["celebrationName"] as? String
            specialInstructionsUpdated = false
            self.setUp()
            
        }
    }
    
    //MARK:- UI Methodsapple-reference-documentation://hsc3QTHbnP
    func setUp() {

        lblTitle.font = UIFont(name : Constants.App.BBQAppFont.ThemeMedium, size: 18.0)
        
        if self.confirmationForType == .Booking {
            lblTitle.text = String(format: "Your Grill Experience is Booked for %li Pax!", numberOfPacks)
        } else {
            lblTitle.text = String(format: "Your Grill Experience is Reschedule for %li Pax!", numberOfPacks)
        }
        
        lblBookingId.text = String(format: "Booking Id: %@", bookingID)
        
        bookingConfirmationViewModel.getBookingData { model, error in
            if let model = model {
                self.lblOutletName.text = model.branchName
                self.lblContactNumber.text = model.branchContactNumber
                if self.confirmationForType == .Booking {
                    self.lblTitle.text = String(format: "Your Grill Experience is\nBooked for %li Pax!", model.numberOfPacks ?? 0)
                } else {
                    self.lblTitle.text = String(format: "Your Grill Experience is\nReschedule for %li Pax!", model.numberOfPacks ?? 0)
                }
                self.getOutletInformation(branchId: model.branchId ?? "")
            }
        }
        
        viewForMenu.setShadow(color: .lightGray, opacity: 0.2, offSet: .zero, radius: 10.0, scale: true)
        viewForAmbience.setShadow(color: .lightGray, opacity: 0.2, offSet: .zero, radius: 10.0, scale: true)
        viewForOffers.setShadow(color: .lightGray, opacity: 0.2, offSet: .zero, radius: 10.0, scale: true)
        viewForOutletInfo.setShadow(color: .lightGray, opacity: 0.2, offSet: .zero, radius: 10.0, scale: true)
        viewForReservationDetails.setShadow(color: .lightGray, opacity: 0.2, offSet: .zero, radius: 10.0, scale: true)
        stackViewForOutletinfo.roundCorners(cornerRadius: 10, borderColor: .clear, borderWidth: 1)
        
        if isAppReferAndEarnActivated == true {
            appReferalView.isHidden = false
            appReferalView.delegetToReferal = self
            appReferalView.setShadow(color: .lightGray, opacity: 0.2, offSet: .zero, radius: 10.0, scale: true)
            // appReferalView.setBackGroundColor(color: UIColor.white)
            appReferalView.layer.cornerRadius = 10.0
        }else{
            appReferalView.isHidden = true

        }
//        requestsOrSpecialInstructionsView.roundCorners(cornerRadius: 10, borderColor: .clear, borderWidth: 0.0)
//        if let doneButton = self.doneButton {
//            doneButton.roundCorners(cornerRadius: 26, borderColor: .clear, borderWidth: 0.0)
//        }
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.specialInstructionPressed))
//        requestsOrSpecialInstructionsView.addGestureRecognizer(tapGesture)
        
    }
    

    
    func showMenuScreen() {
        AnalyticsHelper.shared.triggerEvent(type: .OI02)
        let eatAllViewController = UIUtils.viewController(storyboardId: "OutletInformation", vcId: "BBQEatAllViewController") as! BBQEatAllViewController
        
        //viewController.dataSource = self
        if let branchBuffetData = self.menuBranchBuffetData, let buffet = branchBuffetData.buffets,
            let _ = buffet.buffetData {
            eatAllViewController.buffetData = outletInfoViewModel.totalBuffetData()
        }
        
        //Present your controller over current controller
        self.present(eatAllViewController, animated: true, completion: nil)
    }
    private func getOutletInformation(branchId: String) {
        // Copied logics from view did load
        // TODO: Refactoring needed

//        BBQActivityIndicator.showIndicator( view: self.outletScrollView, addBackgroud: true)
        outletInfoViewModel.getOutletInfo(branchId: branchId) {(result) in
//            BBQActivityIndicator.hideIndicator(from: self.outletScrollView)
            if result == true {
//                self.setUpOutletInfoUIFeilds()
//               self.setUpDynamicUIFeilds()
//                self.promotionData = self.outletInfoViewModel.outletInfoData?.promotion
//                if let data = self.promotionData {
//                    self.promotionPageControl.numberOfPages = data.count
//                }
                if self.outletInfoViewModel.outletInfoData?.branchMenu?.count ?? 0 == 0{
                    self.menuBranchBuffetServiceCall()
                }
                
//                self.promotionCollectionView.reloadData()
//                self.promotionCollectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .right, animated: true)
            }
        }
    }
    
    func getCurrentDate() -> String{
      let todaysDate = NSDate()
      let dateFormatter = DateFormatter()
      dateFormatter.dateFormat = "yyyy-MM-dd"
      let DateInFormat = dateFormatter.string(from: todaysDate as Date)
       return DateInFormat
    }
    
    //Mark:- MenuBranchBuffet ServiceCall
    func  menuBranchBuffetServiceCall(){
        let currentDate = getCurrentDate()
        outletInfoViewModel.getmenubranchBuffet(branchId: BBQUserDefaults.sharedInstance.branchIdValue, reservationDate: currentDate, slotId: 0) { (result) in
            if result == true{
                if let data = self.outletInfoViewModel.menuBranchBuffetData{
                    self.menuBranchBuffetData = data

                }
            }
        }
    }
    //MARK: - Button Actions
    @IBAction func onClickBtnBack(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .RC09)
        if let superVC = superVC {
            self.navigationController?.popToViewController(superVC, animated: true)
        }else{
            self.openReservationController()
        }
       
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { timer in
            let dataDict = ["booking_id": self.bookingID,
                            "reservation_date": self.afterDiningViewModel.diningDataModel?.reservationDate,
                            "branch_name": self.afterDiningViewModel.diningDataModel?.branchName,
                            "occasion_description":"",
                            "type": self.confirmationForType == .Booking ? "booking" : "reschedule"]
            
            if self.isPostBookingReservationRequired == true {
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.NotificationName.bookingConfirmed),
                                                object: nil, userInfo: dataDict as [String: Any])
            }
        }
    }
    
    @IBAction func onClickBtnMenu(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .RC04)
        if outletInfoViewModel.outletInfoData?.branchMenu?.count ?? 0 > 0, let branchMenuVC = UIStoryboard.loadOutletBranchMenuViewController() {
            branchMenuVC.menuImages = outletInfoViewModel.outletInfoData?.branchMenu ?? [String]()
            self.present(branchMenuVC, animated: true)
        }else if self.menuBranchBuffetData?.buffets?.buffetData?.count ?? 0 > 0{
             self.showMenuScreen()
        }else{
            ToastHandler.showToastWithMessage(message: kNoMenuAvailableText)
        }
    }
    
    @IBAction func onClickBtnCall(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .RC03)
        callNumber(phone: bookingConfirmationViewModel.model?.branchContactNumber ?? "")
    }
    
    @IBAction func onClickBtnAmbience(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .RC05)
        if outletInfoViewModel.outletInfoData?.branchGallery?.count ?? 0 > 0 {
            if let bbqAmbienceVC = UIStoryboard.loadOutletAmbienceViewController(){
                bbqAmbienceVC.ambienceImages = outletInfoViewModel.outletInfoData?.branchGallery ?? [String]()
                self.present(bbqAmbienceVC, animated: true)
            }
        }else{
            ToastHandler.showToastWithMessage(message: kNoAmbienceAvailableText)
        }
    }
    
    @IBAction func onClickBtnFacilities(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .RC02)
        if outletInfoViewModel.outletInfoData?.outletAmenities?.count ?? 0 > 0{
            let configuration = BottomSheetConfiguration(animationDuration: 0.4,
                                                         minimumTopSpacing: self.view.frame.size.height - 120)
            let bottomSheetController = BottomSheetController(configuration: configuration)

            guard let aminitiesController = UIStoryboard.loadOutletAmenitiesViewController() else {
                ToastHandler.showToastWithMessage(message: kNoAminitiesAvailableText)
                return
            }
            aminitiesController.outletInfoViewModel = outletInfoViewModel
            //aminitiesController?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            aminitiesController.setupPresentingController(controller: bottomSheetController)
            bottomSheetController.present(aminitiesController, on: self)
            
        } else {
            ToastHandler.showToastWithMessage(message: kNoAminitiesAvailableText)
        }
    }
    
    @IBAction func onClickBtnOffers(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .RC06)
        BBQActivityIndicator.showIndicator(view: self.view)
        let homeModel = HomeViewModel()
        homeModel.getPromosVouchers(latitide: 0.0, longitude: 0.0, brach_ID: BBQUserDefaults.sharedInstance.branchIdValue) { (isSuccess) in
            
            BBQActivityIndicator.hideIndicator(from: self.view)
            
            let promosArray = homeModel.getPromotionsDataData()
            let promoViewModel = PromosViewModel(promosDataArray: promosArray)
            
            let viewController = PromotionOfffersVC()
            viewController.promoDelegate = self as? BottomSheetImagesViewControllerProtocol
            viewController.promoScreenType = .Hamburger
            viewController.promosData = promoViewModel.promosArray
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    @IBAction func onClickBtnReservationDetails(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .RC08)
        let reservationDetailsVC = ReservationDetailsVC()
        reservationDetailsVC.cancelDelagate = self
        reservationDetailsVC.bookingID = bookingID
        self.navigationController?.pushViewController(reservationDetailsVC, animated: true)
    }
    
    @IBAction func onClickBtnSpecialInstruction(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .RC07)
        if !specialInstructionsUpdated{
            let viewController = BBQSpecialInstructionViewController()
            if let model = self.bookingConfirmationViewModel.model,
                let bookingId = model.bookingID {
                viewController.selectedBookingID = bookingId
            }

            let configuration = BottomSheetConfiguration(animationDuration: 0.4,
                                                         minimumTopSpacing: UIScreen.main.bounds.size.height - viewController.view.frame.size.height)
            let bottomSheetController = BottomSheetController(configuration: configuration)
            //        self.corpBottomSheet = bottomSheetController
            bottomSheetController.present(viewController, on: self)
        }else{
            PopupHandler.showSingleButtonsPopup(title: kSpecialInstructionsPopUpTitle,
                                                message: kSpecialInstructionsPopUpMessage,
                                                on: self, firstButtonTitle: kOkButtonTitle) { }
        }
    }
    

    
    private func showRescheduleScreen() {
        let bookingBSHeight = CGFloat(470.00) // As per UX
        let minimumTop = UIScreen.main.bounds.height - bookingBSHeight
        let configuration = BottomSheetConfiguration(animationDuration: 0.4,
                                                     minimumTopSpacing: minimumTop)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        let bookingVC = BBQRescheduleViewController()
        bookingVC.bookingBottomSheet = bottomSheetController
        bookingVC.afterDiningViewModel = self.afterDiningViewModel
        bookingVC.showCalendarInBooking = false
        bookingVC.superVC = superVC
        self.navigationController?.pushViewController(bookingVC, animated: true)
//        bottomSheetController.present(bookingVC, on: self)
    }
    
    @IBAction func changeOutletButtonPressed(_ sender: UIButton) {
        AnalyticsHelper.shared.triggerEvent(type: .OI03)
        let configuration = BottomSheetConfiguration(animationDuration: 0.4,
                                                     minimumTopSpacing: self.view.frame.size.height - 250.0)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        let viewController = BBQLocationViewController()
        viewController.location = BBQLocationManager.shared.currentLocationManager.location
        viewController.setupPresentingController(controller: bottomSheetController)
        bottomSheetController.present(viewController, on: self, viewHeight: UIScreen.main.bounds.height - 165)
    }
    
    @objc func locationDataUpdated(_ notification: Notification) {
        AnalyticsHelper.shared.triggerEvent(type: .OI03A)
        selectedBranchId = nil
        getOutletInformation(branchId: "")
        self.view.layoutIfNeeded()
    }
    
    
    @IBAction func onClickBtnNavigation(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .OI06)
         let bottomSheetController = BottomSheetController()
        let latitude = outletInfoViewModel.outletInfoData?.latitude
        let longitude = outletInfoViewModel.outletInfoData?.longitude
        
        let directionBottomSheet = Bundle.main.loadNibNamed("BookingRedirectionBottomSheet", owner: nil)!.first as! BookingRedirectionBottomSheet
        if let latitude = latitude, let longitude = longitude {
            directionBottomSheet.latitude = latitude
            directionBottomSheet.longitude = longitude
        }
        directionBottomSheet.bottomSheetType = .Direction
        directionBottomSheet.view.maskByRoundingCorners(cornerRadius: 10, maskedCorners: CornerMask.topCornersMask)
        if(UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            directionBottomSheet.isGoogleMapsAvailable = true
        }
        bottomSheetController.present(directionBottomSheet, on: self)
    }
    
    func callNumber(phone : String)
    {
        let phoneUrl = URL(string: "telprompt://\(phone.replacingOccurrences(of: " ", with: ""))")
        let phoneFallbackUrl = URL(string: "tel://\(phone.replacingOccurrences(of: " ", with: ""))")
        if(phoneUrl != nil && UIApplication.shared.canOpenURL(phoneUrl!)) {
            UIApplication.shared.open(phoneUrl!) { (success) in
                if(!success) {
                    // Show an error message: Failed opening the url
                }
            }
        } else if(phoneFallbackUrl != nil && UIApplication.shared.canOpenURL(phoneFallbackUrl!)) {
            UIApplication.shared.open(phoneFallbackUrl!) { (success) in
                if(!success) {
                    // Show an error message: Failed opening the url
                }
            }
        } else {
            // Show an error message: Your device can not do phone calls.
        }
    }
}




extension BBQBookingConfirmationVC:  ReferalDelegate {
    func shareReferalCode() {
        AnalyticsHelper.shared.triggerEvent(type: .RE11)

        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: 80)
        bottomSheetController = BottomSheetController(configuration: configuration)
        
        let vc = BBQShareReferalCodeViewController(nibName: "BBQShareReferalCodeViewController", bundle: nil)
        bottomSheetController?.present(vc, on: self, viewHeight: self.view.frame.size.height - (self.view.frame.size.height * (20/100)))
    }
}
extension BBQBookingConfirmationVC :BookingCancellationlDelegate {
    
    func bookingCancelled(bookingId: String){
        
        if  bookingId == self.bookingID {
            
            //its the same booking which is calleded just after reservation
            //on back button pressed , do not post booking notification
            isPostBookingReservationRequired = false
        }
    }
}
