/*
 *  Created by Nischitha on 12/08/19.
 *  Copyright © BARBEQUE NATION HOSPITALITY LIMITED 2019. All rights reserved.
 
 * CHANGE LOG
 * —------------------------------------------------------------------------------
 * ID          Bug ID      Author Name          Date                  Description
 * —------------------------------------------------------------------------------
 * 1            NA         Nischitha         12/08/19       Initial Version
 */

import UIKit

protocol TableConfirmationContainerViewProtocol: AnyObject {
    func reservationButtonPressedWith(type: ReservationButtonType)
}

class TableConfirmationContainerView: UIView {
    
    //MARK:- IBOutlet
    @IBOutlet weak var bbqnPointsRedeemedImage: UIImageView!
    @IBOutlet weak var bbqPointsRedeemedValueLabel: UILabel!
    @IBOutlet weak var bbqnPointsRedeemedTextlabel: UILabel!
    @IBOutlet weak var estimatedTotalTextLabel: UILabel!
    @IBOutlet weak var estimatedTotalValueLabel: UILabel!
    @IBOutlet weak var advanceAmountTextLabel: UILabel!
    @IBOutlet weak var advanceAmountPaidValueLabel: UILabel! 
    @IBOutlet weak var bookingDetailsContainerView: UIView!
    @IBOutlet weak var resheduleButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    //MARK:- Properties
    weak var delegate: TableConfirmationContainerViewProtocol? = nil
    
    //MARK:- UI Methods
    func setUpTableConfirmationFeilds()
    {
        bbqnPointsRedeemedTextlabel.text = kBBQNPointsRedeemed
        estimatedTotalTextLabel.text = kEstimatedTotal
        advanceAmountTextLabel.text = kAdvanceAmountPaid
    }
    
    //MARK:- Actions
    @IBAction func shareButtonTapped(_ sender: UIButton) {
        triggerButtonEvent(type: .Share)
    }
    
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        triggerButtonEvent(type: .Cancel)
    }
    
    @IBAction func rescheduleButtonTapped(_ sender: UIButton) {
        triggerButtonEvent(type: .Reschedule)
    }
    
   
    @IBAction func cabButtonTapped(_ sender: UIButton) {
         triggerButtonEvent(type: .Cab)
    }
    
    @IBAction func directionButtonTapped(_ sender: UIButton){
         triggerButtonEvent(type: .Direction)
    }
    
    @IBAction func callButtonTapped(_ sender: UIButton) {
         triggerButtonEvent(type: .Phone)
    }
    
    
    //triggerButtonEvent upon button action will set the particular button pressed
    private func triggerButtonEvent(type: ReservationButtonType) {
        if let delegate = self.delegate {
            delegate.reservationButtonPressedWith(type: type)
        }
    }
}
