//
 //  Created by Chandan Singh on 21/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQRescheduleViewController.swift
 //

import UIKit

class BBQRescheduleViewController: BBQBookingMasterViewController {
    
    //MARK:- Properties
    private var bookingId: String?
    private var matchedBuffetCount = 0
    private var matchedBuffetIds = [Json]()
    private var isSlotsPrefill: Bool = false
    
    //MARK:- Outlet collections
    @IBOutlet weak var lblReservationId: UILabel!
    
    var afterDiningViewModel: BBQAfterDiningControllerViewModel? {
        didSet {
            if let id = self.afterDiningViewModel?.diningDataModel?.bookingID {
                self.bookingId = id
                self.setupInitialProperties()
            }
        }
    }

    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.bookingScreenType = .Reschedule
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //MARK:- Overriden Methods - Customize default behaviour
    override func updateLocationOutlet() {
        self.lblOutletName.text = self.afterDiningViewModel?.diningDataModel?.branchName
        self.lblReservationId.text = String(format: "#%@", self.bookingId ?? "")
    }
    
    override func getSlotsDataFromServer(with type: BBQBookingDateType) {
        let reservationDate = self.getReservationDateForSelectedDate(type: type)
        self.selectedformattedDate = reservationDate
        let dateType = reservationDate.isToday() ? .Today : type
        let branchId = self.afterDiningViewModel?.diningDataModel?.branchId
        BBQActivityIndicator.showIndicator(view: self.view)
        slotsViewModel.getBookingSlotsWithBranch(selectedDateType: dateType,
                                                 reservationDate: reservationDate,
                                                 branchId: branchId ?? "",
                                                 dinnerType: kDefaultLunch) { (isSuccess) in
            BBQActivityIndicator.hideIndicator(from: self.view)
            if isSuccess {
                //self.mapSlotsPreSelection
                self.bookingTableView.reloadData()
            } else {
                ToastHandler.showToastWithMessage(message: "Slots not available for selected date")
            }
        }
    }
    
    /*override func fetchBuffetDetails() {
        if self.isTimeSlotReady {
            let branchId = self.afterDiningViewModel?.diningDataModel?.branchId
            let reservationDate = self.getReservationDateForSelectedDate(type: self.selectedDateType)
            BBQActivityIndicator.showIndicator(view: self.view)
            self.buffetsViewModel.getMenuBranchBuffet(branchId: branchId ?? "", reservationDate: reservationDate, slotId: self.selectedSlotId) { (isSuccess) in
                BBQActivityIndicator.hideIndicator(from: self.view)
                if isSuccess {
                    self.isBuffetAdded = true
                    self.isBuffetsRefreshNeeded = true
                    self.mapBuffetIdForPacks()

                    self.bookingTableView.reloadData { }
                } else {
                    
                }
            }
        }
    }
    
    override func loadBuffetSummary(for index: Int) -> BBQBuffetSummaryTableViewCell {
        let buffetCell =
            self.bookingTableView.dequeueReusableCell(withIdentifier: "BBQBuffetSummaryTableViewCellID")
                as! BBQBuffetSummaryTableViewCell
        buffetCell.buffetDelegate = super.self()
        buffetCell.buffetItemDelegate = super.self()
        if self.isBuffetsRefreshNeeded {
            buffetCell.loadBuffettDetails(with: self.buffetsViewModel, matchedBuffetsData: self.matchedBuffetIds)
            self.isBuffetsRefreshNeeded = false
            if self.matchedBuffetIds.count > 0 {
                self.matchedBuffetIds.remove(at: 0)
            }
        }
        return buffetCell
    }*/
    
    override func loadDateSelectionCell(for index: Int) -> BBQBookingDateSelectionTableViewCell {
        let dateSelctionCell =
            self.bookingTableView.dequeueReusableCell(withIdentifier: "BBQBookingDateSelectionTableViewCellID")
                as! BBQBookingDateSelectionTableViewCell
        dateSelctionCell.loadTimeSlotController(with: self.selectedDateType,
                                                slotsModel: self.slotsViewModel,
                                                timeString: self.selectedTimeString ?? "",
                                                delegate: self, isPrefillRequired: self.isSlotsPrefill)
        return dateSelctionCell
    }
    
    override func didPressReserveButton(isPayNow: Bool) {
        if BBQUserDefaults.sharedInstance.isGuestUser {
            self.navigateToLogin()
        } else {
            //Reschedule Booking
            self.rescheduleBooking()
        }
    }
    
    private func rescheduleBooking() {
        let reservationDate = self.getReservationDateForSelectedDate(type: self.selectedDateType)
        let branchId = self.afterDiningViewModel?.diningDataModel?.branchId ?? ""
        let slotId = String(format: "%d", self.selectedSlotId)
        let slotTime = self.slotsViewModel.getSlotTime(from: BBQBookingTimeSlotTableViewCell.selectedSlotIndex ?? 0)
        var bbqPoints = 0
        var bbqPointsAmount = "0"
        if self.didAddPoints, self.pointsValueModel?.redeemAmount != 0.0 {
            bbqPoints = self.pointsValueModel?.redeemablePoints ?? 0
            bbqPointsAmount = String(format: "%.2lf", self.pointsValueModel?.redeemAmount ?? 0.0)
        }
        
        let voucherDetails = self.buffetsViewModel.createConsolidatedVoucherDetails(appliedCorpOffers: self.appliedCorpOffers, appliedCoupons: self.appliedCoupons, appliedVouchers: self.appliedVouchers, arrAddedAmounts: self.arrAddedAmounts, preferredBranch: slotsViewModel.prefferedBranchData)
        let rescheduleBookingData = CreateBookingBody(bookingId: self.bookingId ?? "", branchId: branchId, slotId: slotId, reservationDate: reservationDate, reservationTime: slotTime, loyaltyPoints: bbqPoints, loyaltyAmount: bbqPointsAmount, bookingDetails: self.buffetsViewModel.getBookingDetailsBodyData!, voucherDetails: voucherDetails.voucherDetails)
        AnalyticsHelper.shared.triggerEvent(type: .RD03A)
        BBQActivityIndicator.showIndicator(view: self.view)
        self.buffetsViewModel.rescheduleBooking(rescheduleBookingData: rescheduleBookingData) { (isSuccess) in
            BBQActivityIndicator.hideIndicator(from: self.view)
            if isSuccess {
                AnalyticsHelper.shared.triggerEvent(type: .RD03B)
                if let bookingStatus = self.buffetsViewModel.bookingStatus {
                    if bookingStatus == BBQBookingStatus.Pending.rawValue {
                        self.launchAdvancePayment()
                    } else if bookingStatus == BBQBookingStatus.Confirmed.rawValue {
                        self.launchConfirmBookingScreen()
                    }
                }
            }else{
                AnalyticsHelper.shared.triggerEvent(type: .RD03C)
            }
        }
    }
}

//MARK:- Server Data Mapping
extension BBQRescheduleViewController {
    private func setupInitialProperties() {
        if let viewModel = self.afterDiningViewModel, let model = viewModel.diningDataModel, let date = model.reservationDate {
            self.timeDateSlected = true
            self.selectedformattedDate = date
            self.selectedSlotId = Int(model.slotId ?? "0") ?? 0
            self.selectedTimeString = BBQBookingUtilies.shared.getDisplayTimeIn12HourFormat(model.reservationTime ?? "")
            self.selectedDateType = BBQBookingUtilies.shared.getDateType(date: date)
            self.selectedDateString = BBQBookingUtilies.shared.formattedBookingDate(from: date, dateType: self.selectedDateType)
        }
    }
    
    /*private func mapSlotsPreSelection() {
        //Check whether selected date is same as reschedule date. If it is same, make the pre-selection.
        if self.selectedformattedDate == self.afterDiningViewModel?.diningDataModel?.reservationDate, let availableSlotsData = self.slotsViewModel.availableSlotsData {
            let selectedSlotId = Int(self.afterDiningViewModel?.diningDataModel?.slotId ?? "0") ?? 0
            var isSlotMatched = false
            for slotsData in availableSlotsData {
                if slotsData.slotId == selectedSlotId {
                    isSlotMatched = true
                    break
                }
            }
            if isSlotMatched {
                self.isSlotsPrefill = true
                let defaultSelectedTime = BBQBookingUtilies.shared.getDisplayTimeIn12HourFormat(self.afterDiningViewModel?.diningDataModel?.reservationTime ?? "")
                
                BBQBookingTimeSlotTableViewCell.selectedIndex = selectedSlotId
                self.didSelectBokkingTimeSlot(with: defaultSelectedTime, slotId: selectedSlotId)
            } else {
                self.isSlotsPrefill = false
                self.bookingTableView.reloadData()
            }
        } else {
            //If date is not same, only reload the table. Expected behavior will be inherited.
            self.isSlotsPrefill = true
            self.makeDefaultSelection(with: self.slotsViewModel)
            self.bookingTableView.reloadData()
        }
    }*/
    
    /*private func mapBuffetIdForPacks() {
        
        if let viewModel = self.afterDiningViewModel, let afterDiningModel = viewModel.diningDataModel, let rescheduleBuffetDetails = afterDiningModel.bookingDetails {
            if let currentBuffetData = self.buffetsViewModel.buffetData {
                //Iterate through currently fetched buffetData, compare with booking details data and, determine which one to pre-populate.
                for buffetData in currentBuffetData {
                    for detailsBuffetData in rescheduleBuffetDetails {
                        if let currentBuffetId = buffetData.buffetId, let detailsBuffetId = detailsBuffetData.buffetId, Int(currentBuffetId) == detailsBuffetId {
                            //Buffet Id matched. Do something.
                            self.matchedBuffetIds.append([currentBuffetId: detailsBuffetData.packs as Any])
                        }
                    }
                }
                self.matchedBuffetCount = self.matchedBuffetIds.count
            }
        }
    }*/
}
