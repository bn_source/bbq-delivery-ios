//
 //  Created by Nischitha on 06/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQBookingHistoryViewController.swift
 //

import UIKit

class BBQBookingHistoryViewController: UIViewController {

     //MARK:- IBOutlet
    @IBOutlet weak var myOrderTableView: UITableView!
    @IBOutlet weak var headerView: BBQHomeHeaderView!
    @IBOutlet weak var voucherButton: UIButton!
    @IBOutlet weak var reservationButton: UIButton!
    
    //MARK:- Properties
    var vouchorHistoryViewModel = BBQVoucherHistoryViewModel()
    var contentData : [Content] = []
    var voucherHistoryData : [VoucherHistoryModel] = []
    var isVouchorButtonClicked = false
    var isReservationButtonClicked = true
    var noOfBuffetLabels = 0
    var packs: [Int] = []
    var selectedSection :Int?
    var bookingConfirmationViewController = BBQBookingConfirmationController()
    var upcomingReservationData : [UpcomingReservationDetails] = []
    private let detailScreenNib = Constants.CellIdentifiers.voucherDetailsControllerNib

    lazy var bookingHistoryViewModel : BookingHistoryViewModel = {
        let bookingHistoryViewModel = BookingHistoryViewModel()
        return bookingHistoryViewModel
    }()
    
    var heightOfCell = 240.0
    var bottomSheetController: BottomSheetController?
    var bookingID :String?
    var voucherId : Int?
    
    //MARK:- Button Actions
    
    @IBAction func reservationButtonTapped(_ sender: UIButton) {
      
        isReservationButtonClicked = true
        isVouchorButtonClicked = false
        reservationButton.backgroundColor = .orange
        reservationButton.setTitleColor(.white, for: .normal)
        voucherButton.backgroundColor = .white
        voucherButton.setTitleColor(.orange, for: .normal)
        myOrderTableView.reloadData()

    }
    
    @IBAction func voucherbuttonTapped(_ sender: UIButton) {
        
        isVouchorButtonClicked = true
        isReservationButtonClicked = false
        voucherButton.backgroundColor = .orange
        voucherButton.setTitleColor(.white, for: .normal)
        reservationButton.backgroundColor = .white
        reservationButton.setTitleColor(.orange, for: .normal)
         BBQActivityIndicator.showIndicator(view: self.view)
        //myOrderTableView.reloadData()
        
        vouchorHistoryViewModel.getVoucherHistoryDetails { (result) in
            if result == true{
                print(self.vouchorHistoryViewModel.vouchorHistoryModel)
                 BBQActivityIndicator.hideIndicator(from: self.view)
                self.myOrderTableView.reloadData()
                
                
            }
            else{
                print("no data")
            }
        }

    }
    //MARK:- Life Cycle
    override func viewDidLoad() {
       
        super.viewDidLoad()
        buttonSetUp()
       // bookingDetailsCell?.delegate = self
        guard let headerObj = self.headerView else {
            return
        }
        headerObj.homeHeaderDelegate = self

        bottomSheetController = BottomSheetController()
        BBQActivityIndicator.showIndicator(view: self.view)
        myOrderTableView.isHidden = true
        self.bookingHistoryViewModel.getBookingHistory { (isSuccess) in
            if isSuccess {
                print("success")
                BBQActivityIndicator.hideIndicator(from: self.view)
                self.myOrderTableView.isHidden = false
                
                if let data = self.bookingHistoryViewModel.getupcomingReservationDetailsData {
                    self.upcomingReservationData = data
                }
                print(self.upcomingReservationData)
                for (index,item)  in self.upcomingReservationData.enumerated()
                {
                    self.upcomingReservationData[index].cellExpand = false
                    if  self.upcomingReservationData[index].bookingId == self.bookingID
                    {
                        self.upcomingReservationData[index].opened = true
                        self.upcomingReservationData[index].cellExpand = true
                    }
                    else
                    {
                        self.upcomingReservationData[index].opened = false
                        
                        self.upcomingReservationData[index].cellExpand = false
                    }
                }
                self.myOrderTableView.reloadData()
            }
            else {
                BBQActivityIndicator.hideIndicator(from: self.view)
            }
        }
    }
    func buttonSetUp()
    {
        reservationButton.roundCorners(cornerRadius: 15.0, borderColor:.orange, borderWidth: 1.0)
        voucherButton.roundCorners(cornerRadius: 15.0, borderColor:.orange, borderWidth: 1.0)
    }
    
    @IBAction func historyBackButtonClicked(_sender:UIButton){
        let array = self.navigationController?.viewControllers
        self.navigationController?.popToViewController(array![1], animated: true)
    }
}

extension BBQBookingHistoryViewController: UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        if isReservationButtonClicked == true
        {
             return self.upcomingReservationData.count + 2
        }
        else
        {
            return self.vouchorHistoryViewModel.vouchorHistoryModel?.contents?.count ?? 0
        }
        
       
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isReservationButtonClicked == true && section < upcomingReservationData.count{
            if upcomingReservationData[section].opened == true
            {
                //extra added code
                var noOfBuffetLabels = 0
                for bookingDetails in upcomingReservationData[section].bookingDetails! {
                    noOfBuffetLabels = noOfBuffetLabels + bookingDetails.buffetDetails!.count
                }
                //            return 2 + noOfBuffetLabels
                return 2 + 2
                
            }
            else{
                return 1
            }
        }
        else{
            return 1
        }
       
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isReservationButtonClicked == true && indexPath.section < upcomingReservationData.count
        {
            if indexPath.row == 0
            {
                //extra added code
                noOfBuffetLabels = 0
                for bookingDetails in upcomingReservationData[indexPath.section].bookingDetails!
                {
                    packs.append(bookingDetails.packs ?? 0)
                    noOfBuffetLabels = noOfBuffetLabels + bookingDetails.buffetDetails!.count
                    noOfBuffetLabels = 2
                }
                
                
                tableView.register(UINib(nibName: "UpcomingReservationCell", bundle: nil), forCellReuseIdentifier: "UpcomingReservationCell")
                guard let upcomingReservationCell = tableView.dequeueReusableCell(withIdentifier: "UpcomingReservationCell") as? UpcomingReservationCell else {
                    return UITableViewCell() }
                upcomingReservationCell.bookingIdLabel.text = "  #" + upcomingReservationData[indexPath.section].bookingId!
                upcomingReservationCell.branchLabel.text = " @" + upcomingReservationData[indexPath.section].branchName!
                upcomingReservationCell.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
                return upcomingReservationCell
            }
            else if indexPath.row > 0 && indexPath.row <= noOfBuffetLabels {
                let buffetItemCell = tableView.dequeueReusableCell(withIdentifier: "buffetItemCell", for: indexPath) as! BuffetItemCell
                buffetItemCell.buffetNameLabel.text = "Non Vegetarian x3"
                buffetItemCell.buffetImage.image = UIImage(named: "icon_non_veg")
                return buffetItemCell
            }
            else if indexPath.row == noOfBuffetLabels + 1
            {
                
                tableView.register(UINib(nibName: "BookingDetailsCell", bundle: nil), forCellReuseIdentifier: "BookingDetailsCell")
                guard let bookingDetailsCell = tableView.dequeueReusableCell(withIdentifier: "BookingDetailsCell") as? BookingDetailsCell else {
                    return UITableViewCell() }
                
                bookingDetailsCell.delegate = self
                if upcomingReservationData[indexPath.section].cellExpand != true{
                    var height = 10.0
                    upcomingReservationData[indexPath.section].cellExpand = true
                    for bookingDetails in upcomingReservationData[indexPath.section].bookingDetails!
                    {
                        let packs:Int = bookingDetails.packs ?? 0
                        
                        for buffetItem in bookingDetails.buffetDetails!
                        {
                            let buffetCell = Bundle.main.loadNibNamed("BBQBookingBuffetCell", owner: self, options: nil)?.first as? BBQBookingBuffetCell
                            bookingDetailsCell.addSubview(buffetCell!)
                            buffetCell?.translatesAutoresizingMaskIntoConstraints = false
                            let nonVegBuffet = " x" + NSNumber(value: packs).stringValue
                            var data = buffetItem.name ?? ""
                            data = data + nonVegBuffet
                            buffetCell?.bookingBuffetNameLabel.text = data
                            buffetCell?.bookingBuffetImage.image = UIImage(named: "icon_non_veg")
                            buffetCell?.leadingAnchor.constraint(equalTo: bookingDetailsCell.leadingAnchor, constant: 0.0).isActive = true
                            buffetCell?.trailingAnchor.constraint(equalTo: (bookingDetailsCell.trailingAnchor), constant: 0.0).isActive = true
                            buffetCell?.topAnchor.constraint(equalTo: bookingDetailsCell.topAnchor, constant: CGFloat(height)).isActive = true
                            height = height + 25
                        }
                        
                    }
                }
                
                let bbqPoints:Int64 = upcomingReservationData[indexPath.section ].bbqPoints!
                bookingDetailsCell.bbqnPointsValueLabel.text =  NSNumber(value: bbqPoints).stringValue
                
                let estimatedAmount:Double = upcomingReservationData[indexPath.section].billTotal!
                bookingDetailsCell.estimatedTotalValueLabel.text =  NSNumber(value: estimatedAmount).stringValue
                return bookingDetailsCell
            }
            else
            {
                return UITableViewCell()
            }
        }else if isVouchorButtonClicked == true
        {
            tableView.register(UINib(nibName: "BBQAllOffersCell", bundle: nil), forCellReuseIdentifier: "CorporateOffersCell") as? BBQAllOffersCell
            guard let voucherCell = tableView.dequeueReusableCell(withIdentifier: "CorporateOffersCell") as? BBQAllOffersCell else {
                return UITableViewCell() }
            
            voucherCell.corporateOfferName.text = self.vouchorHistoryViewModel.vouchorHistoryModel?.contents?[indexPath.section].voucherName
            voucherCell.offerAddButton.isHidden = true
            let voucherPrice = self.vouchorHistoryViewModel.vouchorHistoryModel?.contents?[indexPath.section].price ?? 0.0
            voucherCell.priceLabel.text = "₹" + NSNumber(value: voucherPrice).stringValue
           let voucherImage = self.vouchorHistoryViewModel.vouchorHistoryModel?.contents?[indexPath.section].voucherBanner
            LazyImageLoad.setImageOnImageViewFromURL(imageView: voucherCell.corporateOffersImageview, url: voucherImage) { (image) in
                
            }
           // voucherCell.corporateOffersImageview.image = UIImage(n
            
            return voucherCell
            
        }
        else{
            let cell = UITableViewCell()
            cell.isUserInteractionEnabled = false
            return cell
        }
       
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isReservationButtonClicked == true
        {
            tableView.scrollToRow(at: indexPath, at: .top, animated: true)
            selectedSection = indexPath.section
            let cell = tableView.cellForRow(at: indexPath) as? UpcomingReservationCell
            let bookingDetailsCellPath = IndexPath(row: 1, section: indexPath.section)
            let bookingDetailsCell = tableView.cellForRow(at: bookingDetailsCellPath) as? BookingDetailsCell
            
            if upcomingReservationData[indexPath.section].opened == true
            {
                upcomingReservationData[indexPath.section].opened = false
                cell?.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
                cell?.arrowImage.image = UIImage(named: "icon_arrow")
                let sections = IndexSet.init(integer: indexPath.section)
                tableView.reloadSections(sections, with: .none)
            }
            else
            {
                
                var sections: IndexSet
                for (index,item) in self.upcomingReservationData.enumerated()
                {
                    if upcomingReservationData[index].opened == true {
                        let openedCellIndexPath = IndexPath(row: 0, section: index)
                        let openedcell = tableView.cellForRow(at: openedCellIndexPath)
                        sections = IndexSet.init(integer: index)
                        upcomingReservationData[index].opened = false
                        
                        tableView.reloadSections(sections, with: .none)
                    }
                }
                upcomingReservationData[indexPath.section].opened = true
                sections = IndexSet.init(integer: indexPath.section)
                cell?.arrowImage.image = UIImage(named: "icon_down_arrow")
                cell?.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { // Change `2.0` to the desired number of seconds.
                    // Code you want to be delayed'
                    tableView.reloadSections(sections, with: .none)
                    
                }
            }
        }else if isVouchorButtonClicked == true
        {
            voucherId = self.vouchorHistoryViewModel.vouchorHistoryModel?.contents?[indexPath.section].voucherId
            let detailsViewController = BBQVoucherDetailsController(nibName: detailScreenNib, bundle: nil)
            detailsViewController.voucherId = voucherId ?? 0
            self.present(detailsViewController, animated: true, completion: nil)
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0
        {
            return 131
        }
        else if indexPath.row > 0 && indexPath.row <= noOfBuffetLabels {
            return 30
        }
        else if indexPath.row == noOfBuffetLabels + 1
        {

           return CGFloat(heightOfCell)
           return 230
        }
        else{
            return 300
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5
    }
    
}
extension BBQBookingHistoryViewController: BookingDetailsCellProtocol
{
    func bookingHistoryButtonsPressed(type: ReservationButtonType) {
        
        let latitude = self.upcomingReservationData[selectedSection ?? Int(0.0)].latitude
        let longitude = self.upcomingReservationData[selectedSection ?? Int(0.0)].longitude
        
        switch type {
           
           
        case .Cab:
            
            let directionBottomSheet = Bundle.main.loadNibNamed("BookingRedirectionBottomSheet", owner: nil)!.first as! BookingRedirectionBottomSheet
            directionBottomSheet.latitude =  NSNumber(value: latitude!) as? Double
            directionBottomSheet.longitude =  NSNumber(value: longitude!) as? Double
            directionBottomSheet.bottomSheetType = .Cab
            directionBottomSheet.view.maskByRoundingCorners(cornerRadius: 10, maskedCorners: CornerMask.topCornersMask)
            if UIApplication.shared.canOpenURL(URL(fileURLWithPath: "uber://")) {
                directionBottomSheet.isUberAvailable = true
            }
            else if UIApplication.shared.canOpenURL(URL(fileURLWithPath: "olacabs://")) {
                directionBottomSheet.isOlaAvailable = true
            }
            bottomSheetController!.present(directionBottomSheet, on: self)
        
        case .Direction:
            
            let directionBottomSheet = Bundle.main.loadNibNamed("BookingRedirectionBottomSheet", owner: nil)!.first as! BookingRedirectionBottomSheet
            
            directionBottomSheet.latitude =  NSNumber(value: latitude!) as? Double
            directionBottomSheet.longitude =  NSNumber(value: longitude!) as? Double
            directionBottomSheet.bottomSheetType = .Direction
            directionBottomSheet.view.maskByRoundingCorners(cornerRadius: 10, maskedCorners: CornerMask.topCornersMask)
            
            
            if(UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
                directionBottomSheet.isGoogleMapsAvailable = true
            }
            bottomSheetController!.present(directionBottomSheet, on: self)
            
        case .Phone:
            let phoneNumber = "8073398588"
            bookingConfirmationViewController.callNumber(phone: phoneNumber)
        case .Share:
            print("share pressed")
        case .Reschedule:
            print("reshedule pressed")
        case .Cancel:
            print("cancel presssed")
        default:
            break
        }
    }

}
extension BBQBookingHistoryViewController:BBQHomeHeaderViewProtocol{
    func hamburgerClicked(actionSender: UIButton) {
        let menuViewController = UIStoryboard.loadMenuViewController()
        self.addChild(menuViewController)
        self.view.addSubview(menuViewController.view)
        menuViewController.didMove(toParent: self)

    }
    
    func notificationClicked(actionSender: UIButton) {
        
    }
    
    func mapClicked(actionSender: UIButton) {

    }
    
    func cartButtonClicked() {
        
    }
    
    func pointsClicked(actionSender: UIButton) {
        let  loyalityPointsViewController = UIStoryboard.loadLoyalityPointsViewController()
        self.navigationController?.pushViewController(loyalityPointsViewController, animated: true)
    }
}
