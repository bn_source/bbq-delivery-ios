//
 //  Created by Nischitha on 11/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BuffetItemCell.swift
 //

import UIKit

class BuffetItemCell: UITableViewCell {

    @IBOutlet weak var buffetImage: UIImageView!
    @IBOutlet weak var buffetNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
