//
 //  Created by Nischitha on 06/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BookingDetailsCell.swift
 //

import UIKit

protocol BookingDetailsCellProtocol: class {
    func bookingHistoryButtonsPressed(type: ReservationButtonType)
}
class BookingDetailsCell: UITableViewCell {

    //MARK:- IBOutlet
    @IBOutlet weak var bookingDetailsContainerView: UIView!
    @IBOutlet weak var bbqPointsRedeemedImage: UIImageView!
    @IBOutlet weak var estimatedTotalTextLabel: UILabel!
    @IBOutlet weak var bbqnPointsValueLabel: UILabel!
    @IBOutlet weak var bbqnpointsRedeemedTextLabel: UILabel!
    @IBOutlet weak var estimatedTotalValueLabel: UILabel!
    @IBOutlet weak var advanceAmountPaidTextLabel: UILabel!
    @IBOutlet weak var advanceAmountValueLabel: UILabel!
    
    //MARK:- Properties
    weak var delegate: BookingDetailsCellProtocol? 
    //MARK:- Button Actions
    @IBAction func cabButton(_ sender: UIButton) {
        triggerButtonEvent(type: .Cab)
    }
    
    @IBAction func mapButton(_ sender: UIButton) {
         triggerButtonEvent(type: .Direction)
    }
    
    @IBAction func phoneButton(_ sender: UIButton) {
         triggerButtonEvent(type: .Phone)
    }
    
    @IBAction func shareButton(_ sender: UIButton) {
         triggerButtonEvent(type: .Share)
    }
    
    @IBAction func cancelButton(_ sender: UIButton) {
         triggerButtonEvent(type: .Cancel)
    }
    
    @IBAction func rescheduleButton(_ sender: UIButton) {
         triggerButtonEvent(type: .Reschedule)
    }
    
    
     //MARK:- Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //triggerButtonEvent upon button action will set the particular button pressed
    private func triggerButtonEvent(type: ReservationButtonType) {
       
        self.delegate?.bookingHistoryButtonsPressed(type: type)
            
        
    }
}
