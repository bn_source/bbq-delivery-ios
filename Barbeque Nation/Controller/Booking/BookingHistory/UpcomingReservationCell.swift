//
 //  Created by Nischitha on 06/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified UpcomingReservationCell.swift
 //

import UIKit

class UpcomingReservationCell: UITableViewCell {
    
    //MARK:- IBOutlet
    @IBOutlet weak var upcomingReservationImage: UIImageView!
    @IBOutlet weak var bookingIdLabel: UILabel!
    @IBOutlet weak var upcomingRservationTextLabel: UILabel!
    @IBOutlet weak var dateAndTimeLabel: UILabel!
    @IBOutlet weak var eventTypeLabel: UILabel!
    @IBOutlet weak var branchLabel: UILabel!
    @IBOutlet weak var arrowImage: UIImageView!
    
    //MARK:- Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
