//
//  Created by Bhamidipati Kishore on 11/11/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQReferAndEarnController.swift
//

import UIKit

enum BBQReferType : String {
    case ReferCode = "BBQReferCodeScreen"
    case ReferInfo = "BBQReferCodeInfoScreen"
}

class BBQReferAndEarnController: UIViewController {
    
    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var referAndEarnTableView: UITableView!
    @IBOutlet weak var referralCodeLabel: UILabel!
    @IBOutlet weak var termsNConditionsLabel: UILabel!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var referFriendsButton:UIButton!
    
    @IBOutlet weak var contentViewHeightConstraint:NSLayoutConstraint!
    @IBOutlet weak var referCodeHeaderHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var referCodeCopyViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var termsTitleHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var referCodeLabelHeightConstriant: NSLayoutConstraint!
    
    private var showMoreItems : Bool = false
    private var referType : BBQReferType?
    
    var mobileTermsAndConditionsModel : [BBQMobileTermsAndConditionsDataModel]? = nil
    
    lazy var mobileTermsAndConditionsViewModel : BBQMobileTermsAndConditionViewModel = {
        let viewModel = BBQMobileTermsAndConditionViewModel()
        return viewModel
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupTermsTableView()
        loadMobileTermsAndConditionsData()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissView))
        self.overlayView.addGestureRecognizer(tap)
    }
    
    public func loadContollerWith(referType: BBQReferType) {
       
        self.referType = referType
        
        if referType == .ReferCode {
            self.referFriendsButton.setTitle("Refer Friends", for: .normal)
            self.referCodeHeaderHeightConstraint.constant = 55
            self.termsTitleHeightConstraint.constant = 0
            self.referCodeCopyViewHeightConstraint.constant = 50
            self.referCodeLabelHeightConstriant.constant = 50
        } else {
            self.referFriendsButton.setTitle(kButtonOkay, for: .normal)
            self.referCodeHeaderHeightConstraint.constant = 0
            self.termsTitleHeightConstraint.constant = 30
            self.referCodeCopyViewHeightConstraint.constant = 0
            self.referCodeLabelHeightConstriant.constant = 0
        }
        self.view.layoutIfNeeded()
    }
    
    @objc func dismissView() {
        self.dismiss(animated: true, completion: nil)        
    }
    
    func setUpHeadingLabel() {
        let normalText = kInviteAFriend
        let normalAttrs = [NSAttributedString.Key.font : UIFont(name: Constants.App.BBQAppFont.NutinoSansRegular, size: 20.0)!]
        let normalString = NSMutableAttributedString(string:normalText, attributes:normalAttrs)
        self.headingLabel.attributedText = normalString
    }
    
    func setUpTitleString() {
        let style = NSMutableParagraphStyle()
        style.alignment = NSTextAlignment.center
        let regularAttribute = [
            NSAttributedString.Key.font: UIFont(name: Constants.App.BBQAppFont.NutinoSansRegular, size: 17.0)!
        ]
        let boldAttribute = [
            NSAttributedString.Key.font: UIFont(name: Constants.App.BBQAppFont.NutinoSansBold, size: 17.0)!
        ]
        let regularText = NSAttributedString(string: kReferralCode, attributes: regularAttribute)
        let boldText = NSAttributedString(string: BBQUserDefaults.sharedInstance.signUpReferralCode, attributes: boldAttribute)
        let newString = NSMutableAttributedString()
        newString.append(regularText)
        newString.append(boldText)
        newString.addAttributes([ NSAttributedString.Key.paragraphStyle: style ],
                               range: NSMakeRange(0, regularText.length + boldText.length))        
        self.referralCodeLabel.attributedText = newString
    }
    
    private func setupUI() {
        self.overlayView.backgroundColor = UIColor.hexStringToUIColor(hex: Constants.App.LoyalitiPage.greyColor).withAlphaComponent(0.6)
        self.contentView.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
        self.referFriendsButton.layer.cornerRadius = 24.0
        setUpTitleString()
        self.termsNConditionsLabel.text = kTermsNConditions
        setUpHeadingLabel()
    }
    
    private func setupTermsTableView() {
        self.referAndEarnTableView.register(UINib(nibName: Constants.CellIdentifiers.BBQTermsNConditionsTableViewCell,
                                                  bundle: nil),
                                            forCellReuseIdentifier: Constants.CellIdentifiers.BBQTermsNConditionsTableViewCellID)
    }
    
    func loadMobileTermsAndConditionsData() {
        BBQActivityIndicator.showIndicator(view: self.view)    
        self.mobileTermsAndConditionsViewModel.getMobileTermsAndConditions { (isSuccess) in
            if isSuccess {
                BBQActivityIndicator.hideIndicator(from: self.view)
                self.mobileTermsAndConditionsModel = self.mobileTermsAndConditionsViewModel.getMobileTermsAndConditionsModel
                self.referAndEarnTableView.reloadData()
                self.referAndEarnTableView.layoutIfNeeded()
                let tableRect = self.referAndEarnTableView.frame
                if tableRect.size.height > self.referAndEarnTableView.contentSize.height {
                    self.contentViewHeightConstraint.constant = self.contentView.frame.size.height - (tableRect.size.height-self.referAndEarnTableView.contentSize.height)
                }
            } else {
                BBQActivityIndicator.hideIndicator(from: self.view)
            }
        }
    }
    
    
    @IBAction func referFriendsTapped(_ sender: Any) {
        if self.referType == BBQReferType.ReferInfo {
            self.dismissView()
            return
        }
        
        if !BBQUserDefaults.sharedInstance.signUpReferralCode.isEmpty{
            _ = Timer.scheduledTimer(timeInterval: 0.8, target: self, selector: #selector(self.shareMethod), userInfo: nil, repeats: false)
        }
    }
    
    @objc func shareMethod() {
        let  textToShare = Constants.App.LoyalitiPage.referAndEarn+BBQUserDefaults.sharedInstance.signUpReferralCode+Constants.App.LoyalitiPage.details+Constants.App.LoyalitiPage.referAndEarnShortLink
        let pasteboard = UIPasteboard.general
        pasteboard.string = textToShare
        let attributedString = NSMutableAttributedString(string: textToShare)
        let str = NSString(string: textToShare)
        let theRange = str.range(of: Constants.App.LoyalitiPage.referAndEarnShortLink)
        attributedString.addAttribute(.link, value: Constants.App.LoyalitiPage.referAndEarnUrl, range: theRange)
        BBQShareHandler.shareContent(contentToShare: ShareModel(data: textToShare, type: .Text, attributedText: nil), on: self, isBottomSheetPresenter: true)
    }
    
}

extension BBQReferAndEarnController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = self.mobileTermsAndConditionsModel?.count else {
            return 0
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let referAndEarnCell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.BBQTermsNConditionsTableViewCellID) as? BBQTermsNConditionsTableViewCell{
            if let model = self.mobileTermsAndConditionsModel {
                if model.count > 0 {
                    if let terms = model[0].terms_and_condition {
                        referAndEarnCell.termsTextLabel.text = terms.htmlToString
                    }
                }
            }
            return referAndEarnCell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.cellIdentifer, for: indexPath)
        return cell
    }
}
