//
 //  Created by Mahmadsakir on 16/12/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified BBQRedeemSmilesCollectionViewCell.swift
 //

import UIKit

class BBQRedeemSmilesCollectionViewCell: UICollectionViewCell {
    //MARK:- View Properties
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var lblRedeemMessage: UILabel!
    @IBOutlet weak var btnRedeem: UIButton!
    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var widthOfCell: NSLayoutConstraint!
    
    //MARK:- Class Properties
    var delegate: BBQRedeemSmileCellDelegate?
    var type: CellType!
    enum CellType {
        case redeem
        case reservation
        case order
    }
    
    //MARK:- View Methods
    func setTheme() {
        lblRedeemMessage.setTheme(size: 10, weight: .semibold, color: .white)
        btnRedeem.roundCorners(cornerRadius: btnRedeem.frame.size.height/2.0, borderColor: .clear, borderWidth: 0.0)
        btnRedeem.titleLabel?.setTheme(size: 10, weight: .semibold, color: .white)
        viewForContainer.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
        viewForContainer.dropShadow()
    }
    
    func setCellData(type: CellType, delegate: BBQRedeemSmileCellDelegate?, width: CGFloat) {
        setTheme()
        self.type = type
        self.delegate = delegate
        self.widthOfCell.constant = width
        lblRedeemMessage.text = ""
        switch type {
        case .order:
            imgBackground.image = UIImage(named: "BIB_CART")
            //lblRedeemMessage.text = "Redeem Smiles against Online food orders"
            break
        case .redeem:
            imgBackground.image = UIImage(named: "COUPON_CART")
            //lblRedeemMessage.text = "Convert Smiles in to Coupon"
            break
        case .reservation:
            imgBackground.image = UIImage(named: "RESERVATION_CART")
            //lblRedeemMessage.text = "Redeem Smiles On Reservation"
            break
        }
    }
    
    @IBAction func onClickBtnRedeem(_ sender: Any) {
        delegate?.didSelectRedeem(cellType: type)
    }
}
protocol BBQRedeemSmileCellDelegate {
    func didSelectRedeem(cellType: BBQRedeemSmilesCollectionViewCell.CellType)
}
