//
 //  Created by Mahmadsakir on 16/12/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified BBQSmileHistoryTableViewCell.swift
 //

import UIKit

class BBQSmileHistoryTableViewCell: UITableViewCell {
    
    //MARK:- View Properties
    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var lblSmileHistory: UILabel!
    @IBOutlet weak var lblSmileUsed: UILabel!
    @IBOutlet weak var lblSmilePoints: UILabel!
    @IBOutlet weak var lblDateAndTime : UILabel!
    @IBOutlet weak var lblTime : UILabel!
    //MARK:- Class Properties
    var loyaltyTransactions: LoyaltyTransactions!
    
    //MARK:- View Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        viewForContainer.setCornerRadius(12)
        viewForContainer.setShadow(color: UIColor.hexStringToUIColor(hex: "55606D1A") , opacity: 0.2, offSet: .zero, radius: 7, scale: true)
        
    }

    func setTheme() {
        lblSmileHistory.setTheme(size: 16.0, weight: .regular, color: .theme)
        lblSmileUsed.setTheme(size: 12.0, weight: .regular, color: .text)
        lblSmilePoints.setTheme(size: 14, weight: .bold, color: .darkGreen)
    }
    
    func setCellData(loyaltyTransactions: LoyaltyTransactions) {
        //setTheme()
        lblSmilePoints.setCircleView()
        self.loyaltyTransactions = loyaltyTransactions
        lblSmileHistory.text = ""
        if let points = loyaltyTransactions.points{
            if loyaltyTransactions.transaction_type == Constants.App.LoyalitiPage.credit{
                lblSmileHistory.text = String(format: "+%li Smiles Coins Earned", points)
               // lblSmilePoints.text = String(format: "+%li", points)
            }else{
                lblSmileHistory.text = String(format: "-%li Smiles Coins Redeemed", points)
              //  lblSmilePoints.text = String(format: "-%li", points)
            }
            var strText = ""
            var strTime = ""
            if let interval = loyaltyTransactions.created_on{
                let date = Date(timeIntervalSince1970: interval)
                strText = String(format: "%@", date.string(format: "dd , MMM EEEE"))
                strTime = String(format: "%@", date.string(format: "hh:mm a"))

            }
            lblDateAndTime.text = strText
            lblTime.text = strTime
            if let eventType = loyaltyTransactions.eventType, eventType.contains("DELIVERY_REDEMPTION"){
                lblSmileUsed.text = "Delivery/TakeAway Order"
            }else if let eventType = loyaltyTransactions.eventType, eventType.contains("DELIVERY_COMPLETED"){
                lblSmileUsed.text = "Delivery/TakeAway Order" //"Delivery/Takeaway order dated"
            } else{
                lblSmileUsed.text = loyaltyTransactions.description?.components(separatedBy: " on ").first ?? ""
            }
            if loyaltyTransactions.transaction_type == Constants.App.LoyalitiPage.credit, let expireydate = loyaltyTransactions.expiryDate{
                let expiredDate = Date(timeIntervalSince1970: TimeInterval(expireydate))
                if expiredDate > Date(){
                    lblSmileUsed.text = (lblSmileUsed.text ?? "") + "\nSmiles Coins will expire on " + expiredDate.string(format: "dd/MM/yyyy")
                }
            }
            
        }
        
    }
    

}
