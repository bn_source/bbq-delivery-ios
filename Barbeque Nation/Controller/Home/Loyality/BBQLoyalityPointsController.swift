//
//  Created by Bhamidipati Kishore on 16/09/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQLoyalityPointsController.swift
//

import UIKit

class BBQLoyalityPointsController: BBQBaseViewController {
    
    @IBOutlet weak var pointsTableView:UITableView!
    @IBOutlet weak var referAndEarnButton:UIButton!
    
    @IBOutlet weak var expiredSmilesLabel: UILabel!
    @IBOutlet weak var pointsSpentValue: UILabel!
    @IBOutlet weak var availablePointsValue: UILabel!
    
    
    //MARK: Loyality Properties
    var loyalityModel : BBQLoyalityPointsDataModel? = nil
    
    lazy var loyalityViewModel : BBQLoyalityPointsViewModel = {
        let viewModel = BBQLoyalityPointsViewModel()
        return viewModel
    }()
    var totalItems = 0
    var pageNumberValue = 0
    var pageSizeValue = 20
    var sortValue = Constants.App.LoyalitiPage.sortValue
    var loyaltyTransactionsArray = [LoyaltyTransactions]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //
        let pintsCell = UINib(nibName:Constants.NibName.pointsCellNibName, bundle:nil)
        self.pointsTableView.register(pintsCell, forCellReuseIdentifier: Constants.CellIdentifiers.pointsCell)
        //
        configureHeaderView()
        
        //
//        pointsEarnedTextLabel.text = kPointsEarnedText
//        pointsSpentTextLabel.text = kPointsSpentText
//        availablePointsTextLabel.text = kAvailablePointsText
        
//        pointsEarnedView.roundCorners(cornerRadius: pointsEarnedView.frame.size.width/2.0,
//                                      borderColor: UIColor.hexStringToUIColor(hex: Constants.App.LoyalitiPage.orangeColor),
//                                      borderWidth: Constants.App.LoyalitiPage.borderWidth)
//
//        pointsSpentView.roundCorners(cornerRadius: pointsSpentView.frame.size.width/2.0,
//                                     borderColor: UIColor.hexStringToUIColor(hex: Constants.App.LoyalitiPage.greyColor),
//                                     borderWidth: Constants.App.LoyalitiPage.borderWidth)
//
//        availablePointsView.roundCorners(cornerRadius: availablePointsView.frame.size.width/2.0,
//                                         borderColor: UIColor.hexStringToUIColor(hex: Constants.App.LoyalitiPage.greenColor),
//                                         borderWidth: Constants.App.LoyalitiPage.borderWidth)
//
//        //
//        self.referAndEarnButton.setTitle(KReferAndEarnText, for: .normal)
//        self.referAndEarnButton.layer.cornerRadius = Constants.App.LoyalitiPage.referAndEarnRadius
//        self.referAndEarnButton.backgroundColor = UIColor.hexStringToUIColor(hex: Constants.App.LoyalitiPage.orangeColor)
//        let bounds = UIScreen.main.bounds
//        let rect = CGRect(x: 0, y: 0, width: bounds.size.width, height: bounds.size.height)
//        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: Constants.App.LoyalitiPage.greyBackgroundViewRadius,height: Constants.App.LoyalitiPage.greyBackgroundViewRadius))
//        let maskLayer = CAShapeLayer()
//        maskLayer.path = path.cgPath
//        self.greyBackgroundView.layer.mask = maskLayer
//        self.greyBackgroundView.backgroundColor = UIColor.hexStringToUIColor(hex: Constants.App.LoyalitiPage.greyColor)
        //Call the API
        if BBQUserDefaults.sharedInstance.accessToken != "" {
            loadTransactions()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func loadTransactions() {
        BBQActivityIndicator.showIndicator(view: self.view)
        self.loyalityViewModel.getLoyalityPoints(pageNumber: pageNumberValue, pageSize: pageSizeValue, sort: sortValue) { (isSuccess) in
            if isSuccess {
                BBQActivityIndicator.hideIndicator(from: self.view)
                self.loyalityModel = self.loyalityViewModel.getLoyalityModel
                if let loyaltyTransactions = self.loyalityModel?.loyaltyTransactions {
                    self.loyaltyTransactionsArray.append(contentsOf: loyaltyTransactions)
                }
                if let totalElements = self.loyalityModel?.totalElements {
                    self.totalItems = totalElements
                }
//                if let pointsEarned = self.loyalityModel?.pointsEarned{
//                    self.pointsEarnedValue.text = String(pointsEarned)
//                }
                if let pointsSpent = self.loyalityModel?.pointsSpent{
                    self.pointsSpentValue.text = String(pointsSpent)
                }
                if let availablePoints = self.loyalityModel?.availablePoints{
                    self.availablePointsValue.text = String(availablePoints)
                }
                
                self.expiredSmilesLabel.text = self.loyalityViewModel.getExpiredPointsString()
                
                self.pointsTableView.reloadData()
            }else{
                BBQActivityIndicator.hideIndicator(from: self.view)
            }
        }
    }
    
    private func configureHeaderView() {
        guard let headerObj = self.headerView else {
            return
        }
        headerObj.pointsButton.isHidden = true
        headerObj.goldMemberButton.isHidden = false
        headerObj.goldMemberButton.addTarget(self,
                            action:#selector(self.handleSmilesTermsNConditions),
                            for: .touchUpInside)
        
//        let calculateNameLabelWidth = headerObj.nameLabel.intrinsicContentSize.width
//        headerObj.goldMemberLeadingContraint.constant = headerObj.nameLabel.frame.origin.x
//        + calculateNameLabelWidth + Constants.App.LoyalitiPage.goldMemberLeadingConstant
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func referAndEarnClicked(_ sender: Any) {
        UIView.animate(withDuration: 1, animations: {
            let  referController = UIStoryboard.laodReferAndEarnScreen()
            referController.view.layoutIfNeeded()
            referController.modalPresentationStyle = .overCurrentContext
            referController.modalTransitionStyle = .crossDissolve
            referController.loadContollerWith(referType: .ReferCode)
            self.present(referController, animated: false, completion: nil)
        }, completion: nil)
    }
    
    @objc func handleSmilesTermsNConditions() {
        UIView.animate(withDuration: 1, animations: {
            let  referController = UIStoryboard.laodReferAndEarnScreen()
            referController.view.layoutIfNeeded()
            referController.modalPresentationStyle = .overCurrentContext
            referController.modalTransitionStyle = .crossDissolve
            referController.loadContollerWith(referType: .ReferInfo)
            self.present(referController, animated: false, completion: nil)
        }, completion: nil)
    }
}

extension BBQLoyalityPointsController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.loyaltyTransactionsArray.count > 0 {
            return self.loyaltyTransactionsArray.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.pointsCell) as? BBQPointsCell{
            cell.pointsView.setCornerRadius(30)
            if loyaltyTransactionsArray.count > 0 {
                let loyaltyTransaction = loyaltyTransactionsArray[indexPath.row]
                if let transactionType = loyaltyTransaction.transaction_type, let points = loyaltyTransaction.points, let descriptionText = loyaltyTransaction.description {
                    if transactionType == Constants.App.LoyalitiPage.credit {
                        cell.pointsEarnedOrSpentLabel.text = "Earned for your Takeaway/Delivery Order"
                        cell.pointsEarnedOrSpentValueLabel.text = "+\(points)"
//                            "Earned for your Takeaway/Delivery Order"
//                        Constants.App.LoyalitiPage.plus + String(points)
                    } else if loyaltyTransaction.transaction_type == Constants.App.LoyalitiPage.debit {
                        cell.pointsEarnedOrSpentLabel.text = "Redeemed with your Takeaway/Delivery Order"
//                            kPointsSpentText
                        cell.pointsEarnedOrSpentValueLabel.text = "-\(points)"
//                            "Redeemed with your Takeaway/Delivery Order"
//                            Constants.App.LoyalitiPage.minus + String(points)
                    }
                    cell.expiryDateLabel.text =
                        loyalityViewModel.expiryDateString(for: loyaltyTransaction.expiryDate ?? 0,
                                                           transactionType:loyaltyTransaction.transaction_type ?? "")
                    cell.orderTransIDLabel.text = loyalityViewModel.transactionIDDescription(for: indexPath.row)
                    cell.dateLabel.text = descriptionText
                }
            }
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.cellIdentifer, for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row + 1 == loyaltyTransactionsArray.count {
            if loyaltyTransactionsArray.count < self.totalItems {
                self.pageNumberValue = self.pageNumberValue + 1
                loadTransactions()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
