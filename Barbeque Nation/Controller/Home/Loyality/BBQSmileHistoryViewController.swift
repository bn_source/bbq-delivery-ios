//
 //  Created by Mahmadsakir on 15/12/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified BBQSmileHistoryViewController.swift
 //

import UIKit

class BBQSmileHistoryViewController: BBQBaseViewController {
    //MARK:- View properties
    @IBOutlet weak var viewForCardView: UIView!
    @IBOutlet weak var lblAvailableSmileTitle: UILabel!
    @IBOutlet weak var lblAvailableSmile: UILabel!
    @IBOutlet weak var lblNameTitle: UILabel!
    @IBOutlet weak var lblPhoneNumberSmile: UILabel!
    
    @IBOutlet weak var lblRedeemedSmileTitle: UILabel!
    @IBOutlet weak var lblExpiredSmileTitle: UILabel!
    @IBOutlet weak var lblExpiredSmile: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblUpcomingExpiry: UILabel!
    @IBOutlet weak var imgCardView: UIImageView!
    @IBOutlet weak var lblConvertSmilesTitle: UILabel!
    @IBOutlet weak var collectionViewConvertSmiles: UICollectionView!
    @IBOutlet weak var tblTransactionHistory: UITableView!
    @IBOutlet weak var heightsOfTableView: NSLayoutConstraint!
    @IBOutlet weak var lblTotalSmileEarnedTitle: UILabel!
    @IBOutlet weak var lblTotalSmileEarned: UILabel!
    
    @IBOutlet weak var TotalSmileCountVIewCard: UIView!
    @IBOutlet weak var segmentControl: UISegmentedControl!

    @IBOutlet weak var ViewForTableView: UIView!
    @IBOutlet weak var shareSmileViewCard: UIView!
    @IBOutlet weak var lblNumberOfSmilesToShare: UILabel!
    @IBOutlet weak var shareSmileBtn: UIButton!
    @IBOutlet weak var heightForTextView: NSLayoutConstraint!
    @IBOutlet weak var textViewMessage: UITextView!
    
    @IBOutlet weak var lblAvailableSmileCount: UILabel!
    @IBOutlet weak var lblReddemedSmileCount: UILabel!
    @IBOutlet weak var lblExpiryInfo: UILabel!
    @IBOutlet weak var lblExpiredSmileCount: UILabel!
    @IBOutlet weak var appReferalView: AppReferralView!

    @IBOutlet weak var appReferalOuterView: UIView!
    //MARK:- Class properties
    lazy var loyalityViewModel : BBQLoyalityPointsViewModel = {
        let viewModel = BBQLoyalityPointsViewModel()
        return viewModel
    }()
    var totalItems = 0
    var pageNumberValue = 0
    var pageSizeValue = 20
    var sortValue = Constants.App.LoyalitiPage.sortValue
    var loyaltyTransactionsArray = [LoyaltyTransactions]()
    var loyalityModel : BBQLoyalityPointsDataModel? = nil
    var bottomSheetController: BottomSheetController?
    var isNeedToRefresh: Bool = false
    var isSmileRefreshing = false
    var isPullToRefreshClicked = false
    
    
    //MARK:- ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureHeaderView()
        setTheme()
        loadTransactions()
        
        appReferalView.delegetToReferal = self
        appReferalView.setShadow(color: .lightGray, opacity: 0.2, offSet: .zero, radius: 10.0, scale: true)
       // appReferalView.setBackGroundColor(color: UIColor.white)
        appReferalView.layer.cornerRadius = 10.0
     //   setUI()
    }
    
    
    func setUI(){
        
       // imgCardView.setCornerRadius(8)
        segmentControl.backgroundColor = UIColor.hexStringToUIColor(hex: "FEF1E4")
        segmentControl.tintColor =  UIColor.hexStringToUIColor(hex: "FEF1E4")
        // segmentedControl.backgroundColor = background
        
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.appThemeRegularWith(size: 12)], for: .normal)
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.appThemeSemiBoldWith(size: 12)], for: .selected)
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.hexStringToUIColor(hex: "F04B24")], for: .normal)
        
        fixBackgroundSegmentControl(segmentControl)
   
        segmentControl.addTarget(self, action: #selector(valueChangedOnDeliveryAndTakeawaySegmentControl(_:)), for: .valueChanged)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        heightForTextView.constant = textViewMessage.contentSize.height
    }
    
    func fixBackgroundSegmentControl( _ segmentControl: UISegmentedControl){
        if #available(iOS 13.0, *) {
            //just to be sure it is full loaded
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                for i in 0...(segmentControl.numberOfSegments-1)  {
                    let backgroundSegmentView = segmentControl.subviews[i]
                    //it is not enogh changing the background color. It has some kind of shadow layer
                    backgroundSegmentView.isHidden = true
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addObserver()
        if isNeedToRefresh{
            loadTransactions()
            isNeedToRefresh = false
        }
        
        if isAppReferAndEarnActivated == true {
            appReferalOuterView.isHidden = false
            if BBQUserDefaults.sharedInstance.isGuestUser == true {
                appReferalOuterView.isHidden = true
            }
        }
        else{
            appReferalOuterView.isHidden = true
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObserver()
    }
    
    private func addObserver(){
        tblTransactionHistory.addObserver(self, forKeyPath: "contentSize", options: .prior, context: nil)
    }
    
    private func removeObserver(){
        addObserver()
        tblTransactionHistory.removeObserver(self, forKeyPath: "contentSize")
    }
    
    
    private func setTheme(){
//        lblAvailableSmileTitle.setTheme(size: 10.0, weight: .regular, color: .white)
//        lblAvailableSmile.setTheme(size: 24.0, weight: .semibold, color: .white)
//        lblNameTitle.setTheme(size: 14.0, weight: .semibold, color: .white)
//        lblPhoneNumberSmile.setTheme(size: 10.0, weight: .medium, color: .white)
      //  shareSmileBtn.setTitle("Share Smiles", for: .normal)
      //  shareSmileBtn.titleLabel?.font = UIFont(name: Constants.App.BBQAppFont.ThemeMedium, size: 11.0)
      //  lblNumberOfSmilesToShare.setTheme(size: 24, weight: .medium)
      //  shareSmileBtn.setCornerRadius(4)
       // imgCardView.dropShadow()
        
        //lblConvertSmilesTitle.setTheme(size: 16.0, weight: .semibold, color: .theme)
        
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: 80)
        bottomSheetController = BottomSheetController(configuration: configuration)
        
        let mutableString = NSMutableAttributedString(attributedString: textViewMessage.attributedText)
        _ = mutableString.setAsLink(textToFind: "More details…", linkURL: "https://www.barbequenation.com/smiles")
        textViewMessage.attributedText = mutableString
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(BBQSmileHistoryViewController.tapFunction(_ :)))
        textViewMessage.addGestureRecognizer(tap)

        textViewMessage.delegate = self
        textViewMessage.dataDetectorTypes = .link
        self.textViewMessage.linkTextAttributes = [
            .foregroundColor: UIColor.blue,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        
        heightForTextView.constant = textViewMessage.contentSize.height
    }

    @objc func tapFunction (_ gesture: UITapGestureRecognizer) {

//                guard let text = self.textViewMessage.text else { return }
//                let privacyPolicyRange = (text as NSString).range(of: "More details…")
//                if gesture.didTapAttributedTextInLabel(txtView: self.textViewMessage, inRange: privacyPolicyRange) {
//                            if let url = URL(string: "https://www.barbequenation.com/smiles"),
//                                UIApplication.shared.canOpenURL(url) {
//                                if #available(iOS 10.0, *) {
//                                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
//                                } else {
//                                    UIApplication.shared.openURL(url)
//                                }
//                            }
//                }
        
        
        let controller = UIStoryboard.loadHelpSupportViewController()
        controller.accessibilityLabel = "MyBenifits"
        self.navigationController?.pushViewController(controller, animated: false)
    }
    private func configureHeaderView() {
        guard let headerObj = self.headerView else {
            return
        }
        headerObj.pointsButton.isHidden = true
        headerObj.goldMemberButton.isHidden = true
    }
    
    //MARK:- Web Service
    func loadTransactions() {
        BBQActivityIndicator.showIndicator(view: self.view)
        self.loyalityViewModel.getLoyalityPoints(pageNumber: pageNumberValue, pageSize: pageSizeValue, sort: sortValue) { (isSuccess) in
            if isSuccess {
                BBQActivityIndicator.hideIndicator(from: self.view)
                self.lblNameTitle.text = BBQUserDefaults.sharedInstance.UserName
                self.lblPhoneNumberSmile.text = BBQUserDefaults.sharedInstance.customerId
                if self.pageNumberValue == 0{
                    self.loyaltyTransactionsArray.removeAll()
                }
                self.loyalityModel = self.loyalityViewModel.getLoyalityModel
                if let loyaltyTransactions = self.loyalityModel?.loyaltyTransactions {
                    self.loyaltyTransactionsArray.append(contentsOf: loyaltyTransactions)
                }

//                if let availablePoints = self.loyalityModel?.availablePoints{
//                    self.lblAvailableSmile.text = String(format: "%d Smiles", availablePoints)
//                    self.lblNumberOfSmilesToShare.text =  String(format: "%d Smiles", availablePoints)
//                }
                self.tblTransactionHistory.isHidden = false
                self.tblTransactionHistory.reloadData()
                self.setDataAsPerSelectedTab()
            }else{
                BBQActivityIndicator.hideIndicator(from: self.view)
            }
            self.isSmileRefreshing = false
        }
    }
    
    
    
    func setDataAsPerSelectedTab() {
        
        if let availablePoints = self.loyalityModel?.availablePoints{
            self.lblAvailableSmile.text = String(format: "%d", availablePoints)
          //  self.lblNumberOfSmilesToShare.text =  String(format: "%d Smiles", availablePoints)
        }
        
        if self.loyalityModel != nil {
            
          
            if let pointsSpent = self.loyalityModel?.pointsSpent{
                self.lblReddemedSmileCount.text = String(pointsSpent)
            }
            
            if let totalearnedPoints = self.loyalityModel?.pointsEarned{
                self.lblAvailableSmileCount.text = String(totalearnedPoints)
            }
            
            let expiredSmiles  = (self.loyalityModel?.pointsEarned ?? 0) - (self.loyalityModel?.pointsSpent ?? 0) - (self.loyalityModel?.availablePoints ?? 0)
            
            self.lblExpiredSmileCount.text = String(expiredSmiles)
            
            
        }
    }
    
    //MARK:- Button Action Method
    @IBAction func onClickBtnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickInfoBtn(_ sender: Any) {
                
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: 100)
        bottomSheetController = BottomSheetController(configuration: configuration)
        
        let directionBottomSheet = BBQSmileyDetailedViewController(nibName: "BBQSmileyDetailedViewController", bundle: nil)
        directionBottomSheet.loyalityModel = self.loyalityModel

        bottomSheetController?.present(directionBottomSheet, on: self)
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize" {
            heightsOfTableView.constant = tblTransactionHistory.contentSize.height
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension BBQSmileHistoryViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BBQRedeemSmilesCollectionViewCell", for: indexPath) as! BBQRedeemSmilesCollectionViewCell
        if indexPath.row == 0{
            cell.setCellData(type: .reservation, delegate: self, width: (collectionView.frame.size.width/3.0) - 5)
        }else if indexPath.row == 1{
            cell.setCellData(type: .order, delegate: self, width: (collectionView.frame.size.width/3.0) - 5)
        }else{
            cell.setCellData(type: .redeem, delegate: self, width: (collectionView.frame.size.width/3.0) - 5)
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.size.width/3.0) - 5, height: 150)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 7.5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 7.5
    }
}

extension BBQSmileHistoryViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return loyaltyTransactionsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BBQSmileHistoryTableViewCell", for: indexPath) as! BBQSmileHistoryTableViewCell
        cell.setCellData(loyaltyTransactions: loyaltyTransactionsArray[indexPath.row])
        return cell
    }
        
        
        func scrollViewDidScroll(_ scrollView: UIScrollView) {
            isPullToRefreshClicked = false
            
            if scrollView.contentSize.height <= scrollView.contentOffset.y + view.frame.size.height + 100{
                if loyaltyTransactionsArray.count < (self.loyalityViewModel.getLoyalityModel?.totalElements ?? 0) - 1 && !isSmileRefreshing{
                    isSmileRefreshing = true
                    pageNumberValue = pageNumberValue + 1
                    self.loadTransactions()
                }
            }
        }
    
    
    
}
extension BBQSmileHistoryViewController: BBQRedeemSmileCellDelegate{
    func didSelectRedeem(cellType: BBQRedeemSmilesCollectionViewCell.CellType) {
        switch cellType {
        case .order:
            self.navigationController?.popViewController(animated: true)
            if let tabBarController = navigationController?.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
                tabBarController.selectedIndex = 2
            }
            break
        case .reservation:
            isNeedToRefresh = true
            //self.navigationController?.popViewController(animated: true)
            NotificationCenter.default.post(name: Notification.Name(Constants.NotificationName.showBookingScreen),
                                            object: nil)
            break
        case .redeem:
            if let availablePoints = self.loyalityModel?.availablePoints, availablePoints > 0{
                let directionBottomSheet = BBQRedeemSmilesViewController(nibName: "BBQRedeemSmilesViewController", bundle: nil)
                directionBottomSheet.loyalityModel = loyalityModel
                directionBottomSheet.delegate = self
                bottomSheetController?.present(directionBottomSheet, on: self)
            }
            break
        }
    }
}
extension BBQSmileHistoryViewController: BBQRedeemSmilesViewDelegate{
    func onSuccess(view: BBQRedeemSmilesViewController, loyalityCoupon: LoyalityCoupon.Datum.Voucher) {
        isNeedToRefresh = true
        let successView = BBQCouponRedeemSucessfullyView(nibName: "BBQCouponRedeemSucessfullyView", bundle: nil)
        successView.modalPresentationStyle = .overFullScreen
        successView.delegate = self
        successView.voucher = loyalityCoupon
        self.present(successView, animated: true, completion: nil)
    }
    
    func cancel(view: BBQRedeemSmilesViewController) {
        
    }
}
extension BBQSmileHistoryViewController: CouponRedeemSucessfullyViewDelegate{
    func didSelectSmileHistory(view: BBQCouponRedeemSucessfullyView) {
        
    }
    
    func didSelectCouponHistory(view: BBQCouponRedeemSucessfullyView) {
        isNeedToRefresh = true
        let  bookingHistoryController = UIStoryboard.loadHappinesscardViewController()
        bookingHistoryController.isComingFromNotification = true
//        bookingHistoryController.isVouchorButtonClicked = true
        self.navigationController?.pushViewController(bookingHistoryController, animated: true)
    }
    
    
}

extension BBQSmileHistoryViewController {
    
    
    @objc func valueChangedOnDeliveryAndTakeawaySegmentControl(_ uiSegmentControl: UISegmentedControl){
        
        switch (uiSegmentControl.selectedSegmentIndex){
        case 0:
            //smiles
            print("Valid 0 selected segment")
            heightsOfTableView.constant = 0
            tblTransactionHistory.isHidden = true
            shareSmileViewCard.isHidden = false
            TotalSmileCountVIewCard.isHidden = false
            ViewForTableView.isHidden = true

            
        case 1:
            //recent activities
            print("Valid 0  selected segment")
            tblTransactionHistory.reloadData()
            tblTransactionHistory.isHidden = false
            shareSmileViewCard.isHidden = true
            TotalSmileCountVIewCard.isHidden = true
            ViewForTableView.isHidden = false

        default:
             print("Invalide selected segment")
        }
        
    }

    @IBAction func sahreSmile(_ Sender : UIButton){
        
        if let availablePoints = self.loyalityModel?.availablePoints, availablePoints > 0{
            let directionBottomSheet = BBQRedeemSmilesViewController(nibName: "BBQRedeemSmilesViewController", bundle: nil)
            directionBottomSheet.loyalityModel = loyalityModel
            directionBottomSheet.delegate = self
            bottomSheetController?.present(directionBottomSheet, on: self)
        }
    }
}
    
   
extension BBQSmileHistoryViewController: UITextViewDelegate{
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        UIApplication.shared.open(URL)
        return false
    }
    func textView(_ textView: UITextView, shouldInteractWith textAttachment: NSTextAttachment, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        return true
    }
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        return true
    }
}
extension BBQSmileHistoryViewController:  ReferalDelegate {
    func shareReferalCode() {
        
        AnalyticsHelper.shared.triggerEvent(type: .RE07)

        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: 80)
        bottomSheetController = BottomSheetController(configuration: configuration)
        
        let vc = BBQShareReferalCodeViewController(nibName: "BBQShareReferalCodeViewController", bundle: nil)
        bottomSheetController?.present(vc, on: self, viewHeight: self.view.frame.size.height - (self.view.frame.size.height * (20/100)))
    }
}
