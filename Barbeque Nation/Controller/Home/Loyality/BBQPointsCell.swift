//
//  Created by Bhamidipati Kishore on 16/09/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQPointsCell.swift
//

import UIKit

class BBQPointsCell: UITableViewCell {
    
    @IBOutlet weak var pointsView: UIView!
    @IBOutlet weak var backGroundView:UIView!
    @IBOutlet weak var spaceView:UIView!
    
    @IBOutlet weak var pointsEarnedOrSpentLabel:UILabel!
    @IBOutlet weak var dateLabel:UILabel!
    @IBOutlet weak var pointsEarnedOrSpentValueLabel:UILabel!
    @IBOutlet weak var expiryDateLabel: UILabel!
    @IBOutlet weak var orderTransIDLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backGroundView.layer.cornerRadius = Constants.App.NotificationPage.backGroundViewCornerRadius
        //self.backGroundView.dropShadow()
        backGroundView.layer.cornerRadius = 13
        backGroundView.layer.shadowColor = UIColor.gray.cgColor
        backGroundView.layer.shadowOpacity = 0.25
        backGroundView.layer.shadowRadius = 10
        backGroundView.layer.shadowOffset = CGSize(width: 5, height: 5)
    }
}
