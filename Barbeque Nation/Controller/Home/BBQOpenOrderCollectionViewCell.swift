//
 //  Created by Arpana Rani on 09/03/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQOpenOrderCollectionViewCell.swift
 //

import UIKit


protocol OpenOrderCellDelegate: AnyObject {
    func clickedOnRateOrder(selectedIndex: Int )
}
class BBQOpenOrderCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    var item : CartItems?
    @IBOutlet weak var orderReceivedLabel: UILabel!
    @IBOutlet weak var orderDescriptionLabel: UILabel!
    @IBOutlet weak var ETALabel: UILabel!
    @IBOutlet weak var rateButton: UIButton!
    weak var openCellDelegate: OpenOrderCellDelegate?
   // @IBOutlet weak var rateView: FloatRatingView!
    @IBOutlet weak var lblOTPTitle: UILabel!
    @IBOutlet weak var lblOTPValue: UILabel!
    @IBOutlet weak var viewForScheduleOrder: UIView!
    @IBOutlet weak var lblScheduleOrder: UILabel!
    @IBOutlet weak var imgBrandLogo: UIImageView!
    //MARK: Life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
      //  rateView.delegate = self
      //  rateView.rating = 0.0
        self.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
        self.setShadow()
        viewForScheduleOrder.setCornerRadius(5.0)
        imgBrandLogo.image = nil
    }
    
    func setData(orderDetails: Order) {
        imgBrandLogo.setImagePNG(url: orderDetails.brand_logo, placeholderImage: nil)
         orderReceivedLabel.text = orderDetails.state
        orderDescriptionLabel.text =   orderDetails.stateInfo
        rateButton.isHidden = true
      //  rateView.isHidden = true
        timeLabel.isHidden = true
        ETALabel.isHidden = true
     //   rateView.rating = 0.0

        //check if status is delivered , show rate button
        if orderDetails.state == "Delivered" {
            timeLabel.isHidden = true
            ETALabel.isHidden = true
            rateButton.isHidden = false
         //   rateView.isHidden = false

           // rateButton.underline()

        }else{
            rateButton.isHidden = true
        //    rateView.isHidden = true

            
            if orderDetails.current_eta != 0 {
                // hide ETA in case of 0
                timeLabel.isHidden = false
                ETALabel.isHidden = false
                
            }
            timeLabel.text =  String(format: "%d mins ", orderDetails.current_eta ?? 0)
        }
        if orderDetails.state.lowercased() == "dispatched", let otp = orderDetails.otp, otp.count > 0{
            lblOTPTitle.isHidden = false
            lblOTPValue.isHidden = false
            lblOTPValue.text = otp
        }else{
            lblOTPTitle.isHidden = true
            lblOTPValue.isHidden = true
        }
        self.viewForScheduleOrder.isHidden = true
        if orderDetails.is_schedule_delivery{
            self.viewForScheduleOrder.isHidden = false
            self.lblOTPTitle.isHidden = true
            self.ETALabel.isHidden = true
            self.timeLabel.isHidden = true
            self.orderDescriptionLabel.isHidden = true
            self.lblOTPValue.isHidden = true
            //btnRateYourOrder.isHidden = true
            lblScheduleOrder.attributedText = orderDetails.schedule_delivery_time.getScheduleDeluveryString(isAfterPlaced: true, isNextLine: false, interval: orderDetails.lst_delivery_schedule_interval, format: "MM/dd/yyyy hh:mm:ss a", fontSize: 14).0
            orderReceivedLabel.text = "Your order is confirmed"
            
            lblScheduleOrder.textColor = .white
            lblScheduleOrder.alpha = 0.6
        }
        
        
    }
    
    @IBAction func rateBuutonClicked(_sender: UIButton){
        
       openCellDelegate?.clickedOnRateOrder( selectedIndex: rateButton.tag )
    }
}
extension UIButton {
    func underline() {
        guard let text = self.titleLabel?.text else { return }
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.underlineColor, value: self.titleColor(for: .normal)!, range: NSRange(location: 0, length: text.count))
        self.setAttributedTitle(attributedString, for: .normal)
    }
}
//extension BBQOpenOrderCollectionViewCell :  FloatRatingViewDelegate {
//    
//    // MARK: FloatRatingViewDelegate
//    
//     
//    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
//        openCellDelegate?.clickedOnRateOrder( selectedIndex: rateButton.tag , rateCount: rateView.rating)
//
//    }
//    
//    
//    
//}


