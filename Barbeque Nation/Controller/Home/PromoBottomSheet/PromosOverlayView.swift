//
//  PromosOverlayView.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 10/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

class PromosOverlayView: UIView {

    @IBOutlet weak var promoTitleLabel: UILabel!
    @IBOutlet weak var promoPromoTitleTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var promoDescTextView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
