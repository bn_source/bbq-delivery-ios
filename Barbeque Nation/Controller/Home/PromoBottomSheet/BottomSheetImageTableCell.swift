//
//  BottomSheetImageTableCell.swift
//  BottomSheet
//
//  Created by Chandan Singh on 25/07/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

class BottomSheetImageTableCell: UITableViewCell {
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var promoOverlayView: PromosOverlayView!
    @IBOutlet weak var promoOverlayViewTopConstraint: NSLayoutConstraint!
    
    //var promoOverlayType: PromosOverlayType = .Collapsed
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.promoOverlayView.backgroundColor = .promoOverlayColor
        self.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
    }
    
    func configureCell(promoTitle: String, promoDesc: String, promoDuration: String) {
        self.promoOverlayView.promoTitleLabel.text = promoTitle
        self.promoOverlayView.promoDescTextView.text = promoDesc
        
        self.promoOverlayViewTopConstraint.constant = 188
        self.promoOverlayView.promoPromoTitleTopConstraint.constant = 14
    }
}
