//
//  BottomSheetImagesViewController.swift
//  BottomSheet
//
//  Created by Chandan Singh on 25/07/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

enum PromoScreenType {
    case Home, Hamburger, Notification, OutletInfo
}

protocol BottomSheetImagesViewControllerProtocol {
    
    func didPressedTableReservationFromPromotion()
    
}

class BottomSheetImagesViewController: UIViewController {
    
    //MARK: IBOutlets
    @IBOutlet weak var imagesTableView: UITableView!
    @IBOutlet weak var handleView: UIView!
    @IBOutlet var containerView: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    
    var promoDelegate : BottomSheetImagesViewControllerProtocol?
    
    //MARK: Properties
    fileprivate let cellId = Constants.CellIdentifiers.bottomSheetImageCell
    private let detailScreenNib = Constants.CellIdentifiers.voucherDetailsControllerNib
    var promosData: [PromosData]? = nil
    var promoScreenType: PromoScreenType = .Home
    
    weak var dataSource: BottomSheetImagesDataSource? = nil
    weak var delegate: BottomSheetImagesDelegate? = nil
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        AnalyticsHelper.shared.triggerEvent(type: .SO01)
        // Do any additional setup after loading the view.
        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        imagesTableView.reloadData()
    }
    
    //MARK:- Setup UI
    private func setupUI() {

        var userTitle = ""
        if let firstName = BBQUserDefaults.sharedInstance.UserName.components(separatedBy: " ").first {
            let name = String(firstName.filter { !" \n".contains($0) })
            userTitle = "\(kHomeBottomSheetDefaultHeaderTitleUser) \(name)"
        }
        self.headerLabel.text =
            BBQUserDefaults.sharedInstance.isGuestUser ? kHomeBottomSheetDefaultHeaderTitle : userTitle
        
        self.imagesTableView.register(UINib(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
        self.handleView.backgroundColor = UIColor.bottomSheetHandleColor
        self.handleView.roundCorners(cornerRadius: handleView.frame.size.height/2, borderColor: .clear, borderWidth: 0.0)
        self.containerView.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
    }
}

extension BottomSheetImagesViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let promoData = self.promosData else {
            return 0
        }
        
        return promoData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? BottomSheetImageTableCell else {
            return UITableViewCell()
        }
        
        guard let data = self.promosData else {
            return UITableViewCell()
        }
        
        let cellData = data[indexPath.section]
        LazyImageLoad.setImageOnImageViewFromURL(imageView: cell.backgroundImageView, url: cellData.promoImage) { (image) in
            
        }
        
        cell.configureCell(promoTitle: cellData.promoTitle ?? "", promoDesc: cellData.promoDescription ?? "", promoDuration: cellData.promoDuration ?? "")

        return cell
    }
}

extension BottomSheetImagesViewController: UITableViewDelegate, BBQPromotionDetailScreenProtocol {
    
    func didPressedReserveTableFromPromotionDetails() {
        navigateToHomePage()
        if let tabBarController = getTabBarVC() {
            tabBarController.selectedIndex = 0
        }
        self.dismiss(animated: true) {
            if let promoDelegate = self.promoDelegate{
                promoDelegate.didPressedTableReservationFromPromotion()
            }else{
                NotificationCenter.default.post(name: Notification.Name(Constants.NotificationName.showBookingScreen),
                                                object: nil)
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 238.0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        AnalyticsHelper.shared.triggerEvent(type: .SO02)
        guard let data = self.promosData else {
            return
        }
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: 0.0)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        let promotionDetailScreen = UIStoryboard.loadPromotionDetailScreen()
        promotionDetailScreen.promotionDeelegate = self
        promotionDetailScreen.isCameFromSpeciallyScreen = true
        promotionDetailScreen.promotionId = Int(data[indexPath.section].promoId ?? "0") ?? 0
        bottomSheetController.present(promotionDetailScreen, on: self)
        
        /*let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: 0.0)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        
        let detailsViewController = BBQVoucherDetailsController(nibName: detailScreenNib, bundle: nil)
        detailsViewController.promotionId = Int(data[indexPath.section].promoId ?? "0") ?? 0
        detailsViewController.detailScreenType = .Promotion
        bottomSheetController.present(detailsViewController, on: self)*/
    }
    
    
}
