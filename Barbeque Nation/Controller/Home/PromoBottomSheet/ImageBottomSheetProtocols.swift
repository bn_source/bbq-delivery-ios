//
//  ImageBottomSheetProtocols.swift
//  BottomSheet
//
//  Created by Chandan Singh on 29/07/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

protocol BottomSheetImagesDataSource: AnyObject {
    //Optional
    func textforTableFooter() -> String
}

extension BottomSheetImagesDataSource {
    func textforTableFooter() -> String {
        return kHomeBottomSheetDefaultFooterTitle
    }
}

protocol BottomSheetImagesDelegate: AnyObject {
    
}
