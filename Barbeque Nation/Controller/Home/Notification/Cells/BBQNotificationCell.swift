//
//  Created by Rabindra L on 10/09/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQNotificationCell.swift
//

import UIKit

class BBQNotificationCell: UITableViewCell {
    
    @IBOutlet weak var backGroundView:UIView!
    @IBOutlet weak var iconImageView:UIImageView!
    @IBOutlet weak var headingLabel:UILabel!
    @IBOutlet weak var descriptionLabel:UILabel!
    @IBOutlet weak var leftBarImageView:UIImageView!
    @IBOutlet weak var notificationImageView:UIImageView!
    @IBOutlet weak var dotView:UIView!
    @IBOutlet weak var transparantView:UIView!
    @IBOutlet weak var imageTitleLabel:UILabel!
    @IBOutlet weak var notifyImageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var notificationImageHeightConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backGroundView.layer.cornerRadius = Constants.App.NotificationPage.backGroundViewCornerRadius
        //self.backGroundView.dropShadow()
        self.backGroundView.layer.masksToBounds = true
        self.backGroundView.layer.borderColor = UIColor.white.cgColor
        self.backGroundView.layer.borderWidth = 1.0
        
        self.dotView.layer.cornerRadius = self.dotView.frame.size.width/2.0
        self.dotView.layer.masksToBounds = true
    }
    
   
    
}
