//
//  Created by Rabindra L on 10/09/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQNotificationViewController.swift
//

import UIKit
import SDWebImage

class BBQNotificationViewController: BBQBaseViewController {
    
    @IBOutlet weak var notificationTableView:UITableView!
    @IBOutlet weak var noNotifications: UILabel!
    @IBOutlet weak var notificationHeaderLabel: UILabel!
    
    var notificationData: [NotificationModel]? = nil
    var notificationListArray:NSMutableArray? = nil
    let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
    private let detailScreenNib = Constants.CellIdentifiers.voucherDetailsControllerNib
    var readunreadCount = 0
    var promotionsArray : [PromotionModel] = []
    var vouchersArray : [VouchersModel] = []
    lazy var homeViewModel : HomeViewModel = {
        let homeModel = HomeViewModel()
        return homeModel
    }()
    
    var promoViewModel: PromosViewModel? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        let notificationCell = UINib(nibName:Constants.NibName.notificationCellNibName, bundle:nil)
        self.notificationTableView.register(notificationCell, forCellReuseIdentifier: Constants.CellIdentifiers.notificationCell)
        //String Constants
        notificationHeaderLabel.text = kNotifications
        noNotifications.text = kNoNewNotifications
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.refreshNotificationPage),
            name: NSNotification.Name(rawValue: Constants.App.NotificationPage.refreshNotification),
            object: nil)
        getDataFromPlistFile()
        checkNotificationLimit()
        notificationIconWithDotStatus()
        self.latitudeValue = BBQUserDefaults.sharedInstance.latitudeValue
        self.longitudeValue = BBQUserDefaults.sharedInstance.longitudeValue
        self.branchIDValue = BBQUserDefaults.sharedInstance.branchIdValue
        self.getPromotionsAndVouchersFrom(branchLatitude: self.latitudeValue, branchLongitude: self.longitudeValue, branchID: self.branchIDValue)
        self.headerView.notificationButton.isHidden = true
    }
    
    @objc func refreshNotificationPage(notification: NSNotification){
        getDataFromPlistFile()
        checkNotificationLimit()
        notificationIconWithDotStatus()
    }
    
    // MARK: - getPromotionsAndVouchers
    func getPromotionsAndVouchersFrom(branchLatitude:Double,branchLongitude:Double,branchID:String){
        BBQActivityIndicator.showIndicator(view: self.view)
        self.homeViewModel.getPromosVouchers(latitide: branchLatitude, longitude: branchLongitude, brach_ID: branchID) {(isSuccess) in
            BBQActivityIndicator.hideIndicator(from: self.view)
            if(self.promotionsArray.count>0){
                self.promotionsArray.removeAll()
            }
            if(self.vouchersArray.count>0){
                self.vouchersArray.removeAll()
            }
            if isSuccess {
                self.promotionsArray = self.homeViewModel.getPromotionsDataData()
                self.promoViewModel = PromosViewModel(promosDataArray: self.promotionsArray)
                self.vouchersArray = self.homeViewModel.getVouchersData()
            }
        }
    }
    
    //load the notification data from the plist
    func getDataFromPlistFile() {
        if let appDelegate = appDelegate {
            self.notificationListArray = appDelegate.loadNotificationData()
        }
        
        let timeIndexSet = NSMutableIndexSet()
        if let notificationArray = self.notificationListArray {
            for (index,data) in notificationArray.enumerated() {
                let modelDict = data as! NSMutableDictionary
                if let date_String = modelDict.value(forKey: Constants.App.NotificationPage.expirytime) as? String {
                    print("date_String------->>",date_String)
                    if let dateValue = Double(date_String) {
                        let expiryDate = NSDate(timeIntervalSince1970: dateValue)
                        if (expiryDate.timeIntervalSinceNow.sign == .minus) {
                            print("********Expired********")
                            timeIndexSet.add(index)
                            modelDict.setValue(true, forKey: Constants.App.NotificationPage.readunread)
                        }
                        if (expiryDate.timeIntervalSinceNow.sign == .plus) {
                            print("********Not Expired********")
                        }
                    }
                }
            }
        }
        if timeIndexSet.count > 0 {
            // Delete the expired notifications
            self.notificationListArray?.removeObjects(at: timeIndexSet as IndexSet)
            if let appDelegate = appDelegate {
                appDelegate.saveNotificationData(dataArray: self.notificationListArray ?? [])
            }
        }
        self.notificationTableView.reloadData()
        //  Display no notifications if count is zero
        if let dataCount = self.notificationListArray?.count {
            if dataCount > 0 {
                self.noNotifications.isHidden = true
            }
        }else{
            self.noNotifications.isHidden = false
        }
    }
    
    // check notification limit if reached more than 50 delete last record
    func checkNotificationLimit() {
        // Notification List Max count should be 50
        if let ListArray = self.notificationListArray, let count = self.notificationListArray?.count {
            if count > 50 {
                ListArray.removeObject(at: count-1)
                if let appDelegate = appDelegate {
                    appDelegate.saveNotificationData(dataArray: ListArray)
                }
                self.notificationTableView.reloadData()
            }
        }
    }
    
    //Unread or Read notification status in notification icon
    func notificationIconWithDotStatus() {
        let filteredData = self.notificationListArray?.filter{
            let bool = ($0 as AnyObject).value(forKey: Constants.App.NotificationPage.readunread) as! Bool
            return bool
        }
        readunreadCount = filteredData?.count ?? 0
        if let count = self.notificationListArray?.count {
            if readunreadCount == count {
                UserDefaults.standard.set(false, forKey: Constants.App.NotificationPage.notificationActive)
                NotificationCenter.default.post(name: Notification.Name(Constants.App.NotificationPage.receiveNotification), object: nil)
            }
        }
    }
    
    // MARK:- IBActions
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func showVoucherScreen(detailedVoucherId:Int? = 0) {
        /*let viewController = BBQAllOffersViewController()
        viewController.screenType = .Vouchers
        var tempVoucherArray = self.vouchersArray
        for (index, voucherModel) in self.vouchersArray.enumerated() {
            if let voucherId = voucherModel.voucherId,let detailedVoucherID = detailedVoucherId {
                if detailedVoucherID == voucherId {
                    let model = tempVoucherArray.remove(at: index)
                    tempVoucherArray.insert(model, at: 0)
                }
            }
        }
        viewController.vouchersArray = tempVoucherArray
        
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: self.view.frame.size.height - viewController.view.frame.size.height)
        let bottomSheet = BottomSheetController(configuration: configuration)
        viewController.delegate = self
        viewController.bottomSheet = bottomSheet
        bottomSheet.present(viewController, on: self)*/
    }
    
    func showPromotionScreen() {
        //Create instance for the controller that will be shown
        let viewController = BottomSheetImagesViewController()
        viewController.promosData = self.promoViewModel?.promosArray
        //Present your controller over current controller
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: self.view.frame.size.height - viewController.view.frame.size.height - 100)
        let bottomSheet = BottomSheetController(configuration: configuration)
        bottomSheet.present(viewController, on: self)
    }
    
    func showAfterDiningScreen(bookingId:String) {
        let diningVC = UIStoryboard.loadAfterDining()
        diningVC.bookingIDString = bookingId
        diningVC.view.layoutIfNeeded()
        let proposedHeight =  UIScreen.main.bounds.height -  (500)
        let configuration = BottomSheetConfiguration(animationDuration: 0.4,
                                                     minimumTopSpacing: CGFloat(proposedHeight))
        let bottomSheetController = BottomSheetController(configuration: configuration)
        diningVC.diningBottomSheet = bottomSheetController
        bottomSheetController.present(diningVC, on: self)
    }
}

// MARK: - TableView Delegate Methods
extension BBQNotificationViewController:UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let data = self.notificationListArray else {
            return 0
        }
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.notificationCell) as? BBQNotificationCell{
            guard let data = self.notificationListArray else {
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            //let model:NotificationModel
            let modelDict = data[indexPath.row] as! NSMutableDictionary
            if let titleString = modelDict.value(forKey: Constants.App.NotificationPage.title) as? String {
                cell.headingLabel.text = titleString
            }
            if let messageString = modelDict.value(forKey: Constants.App.NotificationPage.body) as? String {
                cell.descriptionLabel.text = messageString
            }
            if let tagline = modelDict.value(forKey: Constants.App.NotificationPage.tagline) as? String {
                cell.imageTitleLabel.text = tagline
            }
            cell.leftBarImageView.backgroundColor = UIColor.blue
            cell.iconImageView.image = UIImage(named: Constants.App.NotificationPage.icon_calendar_tick)
            if let channel = modelDict.value(forKey: Constants.App.NotificationPage.channel) as? String {
                switch channel{
                case Constants.App.NotificationPage.booking:
                    cell.leftBarImageView.backgroundColor = UIColor.blue
                    cell.iconImageView.image = UIImage(named: Constants.App.NotificationPage.icon_calendar_tick)
                case Constants.App.NotificationPage.event:
                    cell.leftBarImageView.backgroundColor = UIColor.green
                    cell.iconImageView.image = UIImage(named: Constants.App.NotificationPage.icon_celebration)
                case Constants.App.NotificationPage.loyalty:
                    cell.leftBarImageView.backgroundColor = UIColor.orange
                    cell.iconImageView.image = UIImage(named: Constants.App.NotificationPage.icon_bbqn_points)
                case Constants.App.NotificationPage.voucher,Constants.App.NotificationPage.coupen,Constants.App.NotificationPage.promotion,Constants.App.NotificationPage.offer:
                    cell.leftBarImageView.backgroundColor = UIColor.green
                    cell.iconImageView.image = UIImage(named: Constants.App.NotificationPage.icon_offer)
                default:
                    print("")
                }
            }
            if let imageUrlString = modelDict.value(forKey: Constants.App.NotificationPage.image) as? String  {
                if imageUrlString == "" {
                    cell.notifyImageViewHeightConstraint.constant = Constants.App.NotificationPage.notifyImageViewZeroHeightConstraint
                    cell.notificationImageHeightConstraint.constant = Constants.App.NotificationPage.notificationImageZeroHeightConstraint
                    cell.transparantView.isHidden = true
                    cell.imageTitleLabel.isHidden = true
                }else if imageUrlString != "" {
                    cell.notifyImageViewHeightConstraint.constant = Constants.App.NotificationPage.notifyImageViewHeightConstraint
                    cell.notificationImageHeightConstraint.constant = Constants.App.NotificationPage.notificationImageHeightConstraint
                    cell.transparantView.isHidden = false
                    cell.imageTitleLabel.isHidden = false
                    LazyImageLoad.setImageOnImageViewFromURL(imageView: cell.notificationImageView, url: imageUrlString) { (image) in
                    }
                }
            }
            if let readunread = modelDict.value(forKey: Constants.App.NotificationPage.readunread) as? Bool{
                if readunread == true{
                    cell.dotView.isHidden = true
                }else if readunread == false{
                    cell.dotView.isHidden = false
                }
            }
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let notificationArray = self.notificationListArray {
            let modelDict = notificationArray[indexPath.row] as! NSMutableDictionary
            // Below code is for read unread
            if modelDict.value(forKey:Constants.App.NotificationPage.readunread) as? Bool == false {
                modelDict.setValue(true, forKey: Constants.App.NotificationPage.readunread)
                notificationArray[indexPath.row] = modelDict
                tableView.reloadRows(at: [indexPath], with: .none)
                if let appDelegate = appDelegate {
                    appDelegate.saveNotificationData(dataArray: notificationArray)
                }
                self.notificationIconWithDotStatus()                
            }
            // Deeplink when select on specific category like booking,voucher,events
            if let channel = modelDict.value(forKey: Constants.App.NotificationPage.channel) as? String {
                switch channel {
                case Constants.App.NotificationPage.booking:
                    if (BBQUserDefaults.sharedInstance.accessToken.count == 0) {
                        //guest user
                    }else{
                        if let bookingId = modelDict.value(forKey: Constants.App.NotificationPage.entity_id) as? String, bookingId != ""{
                                print(bookingId)
                                let reservationDetailsVC = ReservationDetailsVC()
                                reservationDetailsVC.bookingID = bookingId
                                
                                self.navigationController?.pushViewController(reservationDetailsVC, animated: true)
                            
                        }else {
                            let  bookingHistoryController = UIStoryboard.loadReservationViewController()
                           self.navigationController?.pushViewController(bookingHistoryController, animated: true)
                        }
                    }
                case Constants.App.NotificationPage.loyalty:
                    if (BBQUserDefaults.sharedInstance.accessToken.count == 0) {
                        //guest user
                        ToastHandler.showToastWithMessage(message: KLoyaltyLoginText)
                    }else {
                        let  loyalityPointsViewController = UIStoryboard.loadLoyalityPointsViewController()
                        self.navigationController?.pushViewController(loyalityPointsViewController, animated: true)
                    }
                case Constants.App.NotificationPage.voucher:
                    if BBQUserDefaults.sharedInstance.accessToken != "" {
                        let voucherHistoryController = UIStoryboard.loadHappinesscardViewController()
                        voucherHistoryController.isComingFromNotification = true
                        self.navigationController?.pushViewController(voucherHistoryController, animated: true)
                    }
                    //if let voucherId = modelDict.value(forKey: Constants.App.NotificationPage.entity_id) as? String {
                        //showVoucherScreen(detailedVoucherId: Int(voucherId))
                        //print(voucherId)
                        /*let voucherDetailScreen = UIStoryboard.loadVoucherDetailScreen()
                        voucherDetailScreen.voucherId = Int(voucherId) ?? 0
                        voucherDetailScreen.comingFrom = Constants.App.NotificationPage.Notification
                        self.navigationController?.pushViewController(voucherDetailScreen, animated: true)*/
//                    }else{
//                        showVoucherScreen()
//                    }
                case Constants.App.NotificationPage.promotion,Constants.App.NotificationPage.coupen,Constants.App.NotificationPage.offer:
                    if let promotionID = modelDict.value(forKey: Constants.App.NotificationPage.entity_id) as? String {
                        //redirect to promotion detail screen
                        debugPrint(promotionID)
                        if promotionID != "" {
                            let promotionDetailScreen = UIStoryboard.loadPromotionDetailScreen()
                            promotionDetailScreen.promotionId = Int(promotionID) ?? 0
                            promotionDetailScreen.comingFrom = Constants.App.NotificationPage.Notification
                            self.navigationController?.pushViewController(promotionDetailScreen, animated: true)
                        }else{
                            showPromotionScreen()
                        }
                    }else{
                        showPromotionScreen()
                    }
                case Constants.App.NotificationPage.delivery :
                    
                    // for delivery orders navigate to order history if booking id not available
                    // if booking id available go to order details screen add validation to check which screen to move depending on status and booking id
                    //get a substring  separated by "_"  example - "entity_id" = "1462293_Placed"
                    //If orderIDWithoutAppendedIndex is nil just navigate to hist
                    if BBQUserDefaults.sharedInstance.accessToken != "" {
                                                
                        if let orderIDWithoutAppendedIndex = (modelDict.value(forKey: Constants.App.NotificationPage.entity_id) as? String)?.range(of:"_")?.lowerBound {
                            
                            if let orderID = ((modelDict.value(forKey: Constants.App.NotificationPage.entity_id) as? String)?[..<orderIDWithoutAppendedIndex]){
                                
                                let orderDetailsVC = UIUtils.viewController(storyboardId: "DeliveryHome", vcId: "OrderDetailsViewController") as! OrderDetailsViewController
                                orderDetailsVC.orderIdVal = String(orderID)
                                orderDetailsVC.orderViewModel = OrderHistoryViewModel()

                                self.navigationController?.pushViewController(orderDetailsVC, animated: true)
                            } else {
                                
                                let orderHistoryController = UIStoryboard.loadOrderHistoryViewController()
                                orderHistoryController.orderViewModel = OrderHistoryViewModel()
                                self.navigationController?.pushViewController(orderHistoryController, animated: true)
                            }

                        }else {

                                let orderHistoryController = UIStoryboard.loadOrderHistoryViewController()
                                orderHistoryController.orderViewModel = OrderHistoryViewModel()
                                self.navigationController?.pushViewController(orderHistoryController, animated: true)
                        }
                    }

                default:
                    //if channel is blank go to reservation tab
                    navigateToHome()
                    print("No Deeplink for Events")
                }
            }
        }
    }
}

extension BBQNotificationViewController: BBQAllOffersProtocol {
    func buyNowButtonClicked(bottomSheet: BottomSheetController?) {
        if let presentSheet = bottomSheet {
            presentSheet.dismissBottomSheet(on: self)
        }
        
        self.perform(#selector(self.cartButtonClicked), with: nil, afterDelay: 1.0)
    }
    
    func navigateToLoginScreen() {
        self.perform(#selector(self.navigateToLogin), with: nil, afterDelay: 1.0)
    }
    
    @objc func navigateToLogin() {
        self.navigationFromScreen = .Voucher
//        let viewController = UIStoryboard.loadLoginViewController()
//        viewController.delegate = self
//        viewController.navigationFromScreen = .Voucher
//        viewController.shouldCloseTouchingOutside = true
//        viewController.currentTheme = .dark
//        viewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//        self.present(viewController, animated: true, completion: nil)
        let loginViewcontroller = UIStoryboard.loadLoginThroughPhoneController()
        loginViewcontroller.navigationFromScreen = .Voucher
        loginViewcontroller.delegate = self
        self.navigationController?.pushViewController(loginViewcontroller, animated: false)
    }
    
    func cartUpdated() {
        checkCartData()
    }
}

extension BBQNotificationViewController: LoginControllerDelegate {
    func gotoHomeScreen(isExistingUser: Bool, navigationFrom: LoginNavigationScreen) {
        if isExistingUser {
            navigateToVoucherScreen()
        } else {
            showRegistrationConfirmationPage(navigationFrom: navigationFrom)
        }
    }
    
    func navigateToVoucherScreen() {
        if navigationFromScreen == .Voucher {
            showVoucherScreen()
            doPostAccountProcessing()
        }
    }
    
    func showRegistrationConfirmationPage(navigationFrom: LoginNavigationScreen) {
        let registrationConfirmationVC = UIStoryboard.loadRegistrationConfirmation()
        registrationConfirmationVC.modalPresentationStyle = .overFullScreen
        registrationConfirmationVC.titleText = kSignUpConfirmationTitleText
        registrationConfirmationVC.navigationText = kSignUpConfirmationNavigationText
        
        self.present(registrationConfirmationVC, animated: false) {
            sleep(UInt32(2.0))
            self.dismiss(animated: false) {
                self.navigateToVoucherScreen()
            }
        }
    }
    
    func navigateToHome()  {
        
        //If we are on the same navigation , just pop from the given screen

        if let tabBarController1 = navigationController?.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
            tabBarController1.selectedIndex = 0
            
            if tabBarController1.selectedViewController != nil {
                // Check whether the top-most view controller on the stack is an
                
                let currentNavigationStack = navigationController?.viewControllers
                guard let modifyingStack = currentNavigationStack else {
                    return
                }
                if let lastController = modifyingStack.last {
                    //Check if navigation controller stack includes this tabBarController1
                    //pop to the respected  tabbar
                    if ((lastController.isKind(of: BBQHomeViewController.self))) {
                        //not required to do any thing
                    }
                    else if let viewControllers = self.navigationController?.viewControllers {
                        for vc in viewControllers {
                            if vc.isKind(of: DeliveryTabBarController.classForCoder()) {
                                self.navigationController?.popToViewController(vc, animated: true)
                            }
                        }
                    }
                    else {
                        let  homeViewController = UIStoryboard.loadHomeViewController()
                        self.navigationController?.pushViewController(homeViewController, animated: false)
                    }
                }
                
            }
        }
    }
}
