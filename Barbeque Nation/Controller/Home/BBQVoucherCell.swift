//
//  BBQVoucherCell.swift
//  Barbeque Nation
//
//  Created by Maya R on 25/07/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit


class BBQVoucherCell: UICollectionViewCell {
    //MARK: IBOutlets
    @IBOutlet weak var voucherLabel:UILabel!
    @IBOutlet weak var voucherImageView:UIImageView!
    @IBOutlet weak var voucherPriceLabel:UILabel!
    @IBOutlet weak var voucherPurchaseView: BBQVoucherPurchaseView!
    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var lblDescriptions: UILabel!
    
    var cartViewModel = CartViewModel()
    
    //MARK: Life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
        self.voucherImageView.frame.size.width = self.frame.size.width
        voucherPurchaseView.isCustomiseAddBtn = false
        viewForContainer.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
        //viewForContainer.dropShadow()
        
        self.setShadow()
        //self.voucherPurchaseView.serviceDelegate = self
    }
    
    //MARK: Configure Cell
    func setVoucherCellvalues(_ voucherObj: VouchersModel, voucherQuatity: Int) {
        self.voucherLabel.text = "\(voucherObj.voucherName ?? "")"
        LazyImageLoad.setImageOnImageViewFromURL(imageView: self.voucherImageView, url: voucherObj.voucherImage!) { (image) in
        }
        let priceObj:VouchersPrice = voucherObj.voucherPrice!
        let priceString = priceObj.currencyCode == "INR" ? "₹" : ""
        let formattedPrice = String(format: "%.0f", priceObj.number ?? "")
        let combined = " \(priceString)\(formattedPrice.toCurrencyFormat())"
        self.voucherPriceLabel.text = combined
        self.voucherPurchaseView.cartType = .CartTypeHome
        self.voucherPurchaseView.tag = self.tag
        self.voucherPurchaseView.voucherId = voucherObj.voucherId ?? 0
        self.voucherPurchaseView.voucherTitle = voucherObj.voucherName
        self.voucherPurchaseView.currentVoucherQuantity = voucherQuatity
        self.voucherPurchaseView.showButtonsWithQuantity()
        self.voucherPurchaseView.cartViewModel = self.cartViewModel
        self.lblDescriptions.text = voucherObj.voucherDesc
    }
}
