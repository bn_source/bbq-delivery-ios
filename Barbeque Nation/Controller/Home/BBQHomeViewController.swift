//
//  BBQHomeViewController.swift
//  Barbeque Nation
//
//  Created by Maya R on 23/07/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit
import CoreLocation

import Firebase
import ShimmerSwift
import StoreKit

class BBQHomeViewController: BBQBaseViewController, BottomSheetCalendarProtocol {
    // MARK: - IBOutlets
    @IBOutlet weak var promotionCollectionView:UICollectionView!
    @IBOutlet weak var vouchersCollectionView:UICollectionView!
    @IBOutlet weak var upcomingReservationCollectionView:UICollectionView!
    @IBOutlet weak var homeScrollView: UIScrollView!
    @IBOutlet weak var scrollBackgroundView:UIView!
    @IBOutlet weak var reserveConfirmView:UIView!
    @IBOutlet weak var reserveView:UIView!
    @IBOutlet weak var locationNameButton:UIButton!

    @IBOutlet weak var overlayView:UIView!
    @IBOutlet weak var VouchersForYouHeaderLabel: UILabel!
    
    @IBOutlet weak var pageControl: UIPageControl!
    // Removinfg reserve buttons from home. Add back if requirment changes :)
   // @IBOutlet weak var reserveSmallButton: UIButton!
   // @IBOutlet weak var reserveBigButton: UIButton!
    
    @IBOutlet weak var todayButton: UIButton!
    @IBOutlet weak var tomorrowButton: UIButton!
    @IBOutlet weak var selectDateButton: UIButton!
    @IBOutlet weak var outletInfoButton: UIButton!
    
    @IBOutlet weak var shimmerViewPromo: UIView!
    
    @IBOutlet weak var pageControlUpCommingReservation: UIPageControl! // to show number of upcomming reservation
    @IBOutlet weak var stackViewForUpcomingReservation: UIStackView!
    @IBOutlet weak var lblUpcomingCount: UILabel!
    @IBOutlet weak var lblVoucherCount: UILabel!
    @IBOutlet weak var stackViewForBrands: UIStackView!
    @IBOutlet weak var collectionViewForBrands: UICollectionView!
    @IBOutlet weak var stackViewForNearbyOutlets: UIStackView!
    @IBOutlet weak var collectionViewForNearbyOutlets: UICollectionView!
    @IBOutlet weak var btnSmilesSection: UIButton!
    @IBOutlet weak var btnCouponSection: UIButton!
    @IBOutlet weak var btnVoucherSection: UIButton!
    @IBOutlet weak var lblRedeemTItle: UILabel!
    @IBOutlet weak var lblRedeemCount: UILabel!
    @IBOutlet weak var lblAvailableTitle: UILabel!
    @IBOutlet weak var lblAvailableCount: UILabel!
    @IBOutlet weak var lblExpiredTitle: UILabel!
    @IBOutlet weak var lblExpiredCount: UILabel!
    @IBOutlet weak var constraintForLiner: NSLayoutConstraint!
    @IBOutlet weak var viewForSmilesCoins: UIView!
    @IBOutlet weak var lblSmilesCoins: UILabel!
    @IBOutlet weak var viewForVouchers: UIView!
    @IBOutlet weak var lblVoucherValue: UILabel!
    @IBOutlet weak var viewForCoupons: UIView!
    @IBOutlet weak var lblCouponsValue: UILabel!
    @IBOutlet weak var viewAnimationMybenefitsBg: UIView!
    @IBOutlet weak var viewAnimationMybenefits: UIView!
    @IBOutlet weak var appReferalView: AppReferralView!
    @IBOutlet weak var AppReferralViewIconOnTop : UIView!
    // MARK: For open orders
    var  cartItem: CartItems?
    var isFirstTimeLoaded = false
    var promosFlowLayout = BBQPromosFlowLayout()
    private var userDeniedLocation :Bool = false
    var selectedVoucher: Int = 0
    private var isUserLoggedOut:Bool = false
    var bottomSheetController: BottomSheetController?
    private var selectedDateType: BBQBookingDateType = .Today
    private var upcomingArray :[UpcomingBookingModel] = []
    lazy var homeViewModel : HomeViewModel = {
        let homeModel = HomeViewModel()
        return homeModel
    }()
    lazy var loyalityViewModel : BBQLoyalityPointsViewModel = {
        let viewModel = BBQLoyalityPointsViewModel()
        return viewModel
    }()
    lazy var voucherCouponListWithCountViewModel : VoucherAndCouponListWithCountViewModel = {
        let viewModel = VoucherAndCouponListWithCountViewModel()
        return viewModel
    }()
    
    private var voucherCouponModelModel: VoucherAndCouponListWithCountModel? = nil
    var bbqLastVisitedViewModel = BBQLastVisitedViewModel()
    var upcomingReservationModel = UpcomingReserevationViewModel()
    var promoViewModel: PromosViewModel? = nil
    var promotionsArray : [PromotionModel] = []
    var vouchersArray : [VouchersModel] = []
    var upcomingReservationsArray : [UpcomingBookingModel] = []
    var refreshControl = UIRefreshControl()
    
    var shimmerView : ShimmeringView?
    var shimmerViewMyBenefits : ShimmeringView?
    var shimmerViewVouchers : ShimmeringView?
    
    private let reserveViewHeight: CGFloat = 121 //164
    private let reserveMiniViewHeight: CGFloat = 121 //73
    
    @IBOutlet weak var openOrdersCollectionView: UICollectionView!
    @IBOutlet weak var pageControlForOpenOrders: UIPageControl!
    //increase the scroll size when , there are open orderts
    // since there a collection view added on top of view
    @IBOutlet weak var openorderBlankViewHeight: NSLayoutConstraint!
    //@IBOutlet weak var lblSmileCountCart: UILabel!
    
    @IBOutlet weak var myBenifitsCountView: UIView!
    
    // MARK: - Update Application outlet
    
    @IBOutlet weak var updateView :UIView!
    @IBOutlet  var updateViewBottomWithHomeScrollView: NSLayoutConstraint!
    @IBOutlet  var updateViewExcludedBottomHeightWithHomeScrollView: NSLayoutConstraint!
    @IBOutlet  var lblReleaseNotesForUpdate: UILabel!

    
    
    // MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        AnalyticsHelper.shared.triggerEvent(type: .H01)
        self.setupReserveButtons()
        self.upcomingReservationCollectionView.isHidden = true
        self.openOrdersCollectionView.isHidden = true
        
        var userTitle = ""
        if let firstName = BBQUserDefaults.sharedInstance.UserName.components(separatedBy: " ").first {
            let name = String(firstName.filter { !" \n".contains($0) })
            userTitle = "\(kShareSmileTitleUser) \(name)"
        }
        self.VouchersForYouHeaderLabel.text = BBQUserDefaults.sharedInstance.isGuestUser ? kShareSmileTitle : userTitle
      
        myBenifitsCountView.isHidden = BBQUserDefaults.sharedInstance.isGuestUser ? true : false
        overlayView.isHidden = true

        
        // If user had selected any outlet, did logout and login if there is any saved location of the user need to show the promotion and vouchers of that outlet
        self.showSelectedOuletNameOnButton(BBQUserDefaults.sharedInstance.CurrentSelectedLocation)
        
        shimmerView = ShimmeringView(frame: scrollBackgroundView.bounds)
        shimmerViewMyBenefits = ShimmeringView(frame: viewAnimationMybenefitsBg.bounds)
        
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: self.view.frame.size.height - 258.0)
        bottomSheetController = BottomSheetController(configuration: configuration)
        upcomingReservationModel.homeVC = self
        /* Removing Pull to refresh functionality from home for now.
        //Adding pull to refres to scrollview
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.tintColor = .white
        refreshControl.addTarget(self, action: #selector(self.refreshUI), for: UIControl.Event.valueChanged)
        homeScrollView.isScrollEnabled = true
        homeScrollView.alwaysBounceVertical = true
        homeScrollView.addSubview(refreshControl)
        */
        
        headerView.pointsButton.isHidden = true
        let overlayTap = UITapGestureRecognizer(target: self, action: #selector(self.overlayTapped(_:)))
        overlayView.addGestureRecognizer(overlayTap)
        let reserveMiniViewTap = UITapGestureRecognizer(target: self, action: #selector(self.reserveMiniTaped(_:)))
        self.reserveView.addGestureRecognizer(reserveMiniViewTap)
        
        
        appReferalView.delegetToReferal = self
        appReferalView.setShadow(color: .lightGray, opacity: 0.2, offSet: .zero, radius: 10.0, scale: true)
        appReferalView.setBackGroundColor(color: UIColor.white)
        appReferalView.layer.cornerRadius = 10.0
        
        NotificationCenter.default.addObserver(self, selector: #selector(bookingConfirmed(_:)), name: Notification.Name(rawValue:Constants.NotificationName.bookingConfirmed), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(bookingCancelled(_:)), name: Notification.Name(rawValue:Constants.NotificationName.bookingCancelled), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(bookingFlowCancelled(_:)), name: Notification.Name(rawValue:Constants.NotificationName.bookingFlowCancelled), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateUserStatusWithLogout(_:)), name: Notification.Name(rawValue:Constants.NotificationName.userLoggedOut), object:nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateWithUserProfile(_:)), name: Notification.Name(rawValue:Constants.NotificationName.receiedUserProfileData), object:nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateHeader(_:)), name: Notification.Name(rawValue:Constants.NotificationName.upadateHomeHeader), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(cartDataUpdated(_:)), name: Notification.Name(rawValue:Constants.NotificationName.cartDataUpdated), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showBookingFromHamMenu(_:)), name: Notification.Name(rawValue:Constants.NotificationName.showBookingScreen), object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(BBQHomeViewController.appMovedToForeground),
                                               name:UIApplication.willEnterForegroundNotification,
                                               object: nil)
        //MARK: - Fix for DEL-1778
        BBQServiceManager.getInstance().getLocalSavedBranches {
            self.getTheNearbyOutletsFromV1()
        }
        appUpdateService()
        //END
    }
    
    func setupReserveButtons(){
        if upcomingArray.count < 1 {
            self.stackViewForUpcomingReservation.isHidden = true
            self.view.layoutSubviews()
        }

        reserveView.layer.shadowPath = UIBezierPath(rect: reserveView.bounds).cgPath
        reserveView.layer.shadowRadius = 5
        reserveView.layer.shadowOffset = .zero
        reserveView.layer.shadowOpacity = 0.1
        
        
        self.todayButton.dropShadow()
        self.tomorrowButton.dropShadow()
        self.selectDateButton.dropShadow()
        
        self.view.layoutIfNeeded()
    }
    
    
    func showAlertForAppUpdate(localVersion: String, updatedVersion:String ,releaseText: String ) {
        
        if BBQUserDefaults.sharedInstance.appCheckedVersion != updatedVersion{
            BBQUserDefaults.sharedInstance.appCheckedVersion = updatedVersion
            let myAlert =  UIStoryboard.loadupdateAppScreen()
            myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            myAlert.setValues(localV: localVersion, storeV: updatedVersion, releaseNote: releaseText)
            
            myAlert.callBack = {
                
                self.openStoreProductWithiTunesItemIdentifier("1080269411")
                DispatchQueue.main.async {
                    self.dismiss(animated: false, completion: nil)
                }

            }

            self.present(myAlert, animated: true, completion: nil)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        

        
        
        
        self.screenName(name: .Reservation_Home_Screen)
        self.fetchCouponAndVoucherCount()
        if !isFirstTimeLoaded{
            isFirstTimeLoaded = true
            self.checkAndUpdateHomeDataWithLocation()
        }else{
            if let branch = findBranchBy(id: BBQUserDefaults.sharedInstance.branchIdValue), branch.only_delivery{
                getTheNearbyOutletsFrom(userLatitude: branch.latitude, userLongitude: branch.longitude)
            }else if branchName != BBQUserDefaults.sharedInstance.CurrentSelectedLocation{
                self.branchIDValue = BBQUserDefaults.sharedInstance.branchIdValue
                self.branchName = BBQUserDefaults.sharedInstance.CurrentSelectedLocation
                self.showSelectedOuletNameOnButton(branchName)
                headerView.locationLabel.text = branchName
                self.saveOuletValues()
                getPromotionsAndVouchersFrom(branchLatitude: 0.0, branchLongitude: 0.0, branchID: BBQUserDefaults.sharedInstance.branchIdValue)
            }
        }
        self.getOrderFromQueue()
        openorderBlankViewHeight.constant = 0

        self.tabBarController?.tabBar.isHidden = false
        AnalyticsHelper.shared.triggerEvent(type: .H01A)
        self.hideNavigationBar(true, animated: animated)
        self.showShimmering()
        self.doPostAccountProcessing()

        NotificationCenter.default.addObserver(self, selector: #selector(userUpdatedLocation(_:)), name: Notification.Name(rawValue:Constants.NotificationName.userLocationChanged), object:nil)
        
        if let branch = findBranchBy(id: BBQUserDefaults.sharedInstance.branchIdValue), branch.only_delivery{
            showOnlyDeliveryAlert()
        }
        checkForReservationDetails()
        scrollToHappinessCard()
        
        if isAppReferAndEarnActivated == true {
            appReferalView.isHidden = false
            AppReferralViewIconOnTop.isHidden = false
            if BBQUserDefaults.sharedInstance.isGuestUser == true {
                appReferalView.isHidden = true
                AppReferralViewIconOnTop.isHidden = true
            }
        }else{
            appReferalView.isHidden = true
            AppReferralViewIconOnTop.isHidden = true

            
        }
        
    }
    
    
    private func getOrderFromQueue() {
        //BBQUserDefaults.sharedInstance.customerId
       // if BBQUserDefaults.sharedInstance.is
        guard !BBQUserDefaults.sharedInstance.accessToken.isEmpty else {
            return
        }
        
        cartItem = nil
        BBQServiceManager.getInstance().getPreviousSelectedItems(customerId: BBQUserDefaults.sharedInstance.customerId, branchId: BBQUserDefaults.sharedInstance.DeliveryBranchIdValue) { [self] (error, response) in
            if let cartItems = response {
                cartItem = cartItems
                print(cartItems.onGoingOrders)
                if cartItems.onGoingOrders.count > 0 {
                    
                    //show the UI at bootom for order details
                    pageControlForOpenOrders.numberOfPages = cartItems.onGoingOrders.count
                    self.openOrdersCollectionView.isHidden = false
                    self.openOrdersCollectionView.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
                    self.openOrdersCollectionView.setShadow()
                    self.openOrdersCollectionView.reloadData ()
                    openorderBlankViewHeight.constant = 60

                    
                }else{
                    self.openOrdersCollectionView.isHidden = true
                    openorderBlankViewHeight.constant = 0


                }
                setupUIBasedOnReservations()
                let itemIds = cartItems.items.map({$0.id})
                UIUtils.updateCartTabBadgeCount(itemIds, count: cartItems.totalItemsCount)
                self.view.layoutIfNeeded()
            }

        }
        
    }
    
    private func appUpdateService(){
        VersionCheck.shared.isUpdateAvailable() {( hasUpdates ,versionLocal, versionAppStore , releaseNotes)  in
            
            if hasUpdates {
                self.showAlertForAppUpdate(localVersion :versionLocal,  updatedVersion: versionAppStore, releaseText: releaseNotes)
                
                DispatchQueue.main.async {
                    
                    self.lblReleaseNotesForUpdate.text = releaseNotes
                    self.updateViewBottomWithHomeScrollView.isActive = true
                    self.updateViewBottomWithHomeScrollView.priority = .defaultHigh
                    self.updateViewExcludedBottomHeightWithHomeScrollView.isActive = false
                    self.updateViewExcludedBottomHeightWithHomeScrollView.priority = .defaultLow
                    self.updateView.isHidden = false
                }
            }else{
                self.updateViewBottomWithHomeScrollView.isActive = false
                self.updateViewBottomWithHomeScrollView.priority = .defaultLow
                self.updateViewBottomWithHomeScrollView.isActive = true
                self.updateViewBottomWithHomeScrollView.priority = .defaultHigh
                self.updateView.isHidden = true

            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.initializeInitialItemQuatity()
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue:Constants.NotificationName.userLocationChanged), object: nil)

    }
    
    func initializeInitialItemQuatity() {
        if self.itemsQuantity.count > 0 { self.itemsQuantity.removeAll() }
        
        for _ in self.vouchersArray {
            self.itemsQuantity.append(0)
        }
        self.vouchersCollectionView.reloadData()
    }
    
    //MARK : - refresh called when pull to refreshis called
    @objc func refreshUI(sender:AnyObject) {
        // Code to refresh table view
//        self.getPromotionsAndVouchersFrom(branchLatitude:self.latitudeValue, branchLongitude: self.longitudeValue, branchID: self.branchIDValue)
    }
    
    //MARK : - setupPromosCollectionFlowLayout called to make the layout changes
    
    func checkAndUpdateHomeDataWithLocation(){
        //Checking whether location is enabled in Settings
        //If location is allowed by user then get the nearby outlet for that lat and long else show the cutom alert

        var userLatitudeValue:Double = 0.0
        var userLongitudeValue:Double = 0.0
        if(BBQLocationManager.shared.currentLocationManager.location != nil){
            userLatitudeValue = (BBQLocationManager.shared.currentLocationManager.location?.coordinate.latitude)!
            userLongitudeValue = (BBQLocationManager.shared.currentLocationManager.location?.coordinate.longitude)!
            getTheNearbyOutletsFrom(userLatitude: userLatitudeValue, userLongitude: userLongitudeValue)
        }else{
            self.branchIDValue = BBQUserDefaults.sharedInstance.branchIdValue
            self.branchName = BBQUserDefaults.sharedInstance.CurrentSelectedLocation
            self.showSelectedOuletNameOnButton(branchName)
            headerView.locationLabel.text = branchName
            self.saveOuletValues()
            getPromotionsAndVouchersFrom(branchLatitude: userLatitudeValue, branchLongitude: userLongitudeValue, branchID: BBQUserDefaults.sharedInstance.branchIdValue)
            
        }
        
    }
    
    //MARK : - userUpdatedLocation called when location changes
    @objc func userUpdatedLocation(_ notification: Notification) {
        if let dictionary = notification.userInfo as NSDictionary? {
            let latValue = dictionary["latitude"] as! Double
            let longValue = dictionary["longitude"] as! Double
            if latValue == 0.0, longValue == 0.0 {
                BBQUserDefaults.sharedInstance.userDeniedLocation = true

                userDeniedLocation = true
                if BBQUserDefaults.sharedInstance.branchIdValue != ""{
                    self.getPromotionsAndVouchersFrom(branchLatitude: self.latitudeValue, branchLongitude: self.longitudeValue, branchID: BBQUserDefaults.sharedInstance.branchIdValue)
                }else{
                    getTheNearbyOutletsFrom(userLatitude: latValue, userLongitude: longValue)
                }
            }else{
                userDeniedLocation = false
                getTheNearbyOutletsFrom(userLatitude: latValue, userLongitude: longValue)
            }
        }
    }
    
    @objc func updateWithUserProfile(_ notification: Notification) {
        
        
        var userTitle = ""
        if let firstName = BBQUserDefaults.sharedInstance.UserName.components(separatedBy: " ").first {
            let name = String(firstName.filter { !" \n".contains($0) })
            userTitle = "\(kShareSmileTitleUser) \(name)"
        }
        self.VouchersForYouHeaderLabel.text = BBQUserDefaults.sharedInstance.isGuestUser ? kShareSmileTitle : userTitle
        
        if let dictionary = notification.userInfo as NSDictionary? {
            let savedUserLocation = dictionary["userSelectedLocation"] as! String
            let savedBrachId = dictionary["selectedBrachID"] as! String
            self.showSelectedOuletNameOnButton(savedUserLocation)
            self.isUserLoggedOut = true
            self.headerView.configureNameUI()
            if BBQLocationManager.shared.currentLocationManager.location != nil{
                let latValue = (BBQLocationManager.shared.currentLocationManager.location?.coordinate.latitude)!
                let longValue = (BBQLocationManager.shared.currentLocationManager.location?.coordinate.longitude)!
                getTheNearbyOutletsFrom(userLatitude: latValue, userLongitude: longValue)
            }else{
                if savedBrachId != ""{
                    self.getPromotionsAndVouchersFrom(branchLatitude: 0.0, branchLongitude: 0.0, branchID: savedBrachId)
                }
            }
        }
    }
    
    
    //MARK : - updateUserStatus called when user logout from app
    @objc func updateUserStatusWithLogout(_ notification: Notification) {
        self.isUserLoggedOut = true
        
        var userTitle = ""
        if let firstName = BBQUserDefaults.sharedInstance.UserName.components(separatedBy: " ").first {
            let name = String(firstName.filter { !" \n".contains($0) })
            userTitle = "\(kShareSmileTitleUser) \(name)"
        }
        self.VouchersForYouHeaderLabel.text = BBQUserDefaults.sharedInstance.isGuestUser ? kShareSmileTitle : userTitle
        
        if(BBQLocationManager.shared.currentLocationManager.location != nil){
          let userLatitudeValue = (BBQLocationManager.shared.currentLocationManager.location?.coordinate.latitude)!
          let userLongitudeValue = (BBQLocationManager.shared.currentLocationManager.location?.coordinate.longitude)!
            getTheNearbyOutletsFrom(userLatitude: userLatitudeValue, userLongitude: userLongitudeValue)
        }else{
            getPromotionsAndVouchersFrom(branchLatitude: 0, branchLongitude: 0, branchID: "")
        }

        self.showSelectedOuletNameOnButton(BBQUserDefaults.sharedInstance.CurrentSelectedLocation)
    }
    
    //MARK : - getOutletForBooking
    @objc func getOutletForBooking() {
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: self.view.frame.size.height - 250.0)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        let viewController = BBQLocationViewController()
        viewController.location = BBQLocationManager.shared.currentLocationManager.location
        viewController.setupPresentingController(controller: bottomSheetController)
        bottomSheetController.present(viewController, on: self, viewHeight: UIScreen.main.bounds.height - 165)
    }
    
    func saveOuletValues() {
        BBQUserDefaults.sharedInstance.latitudeValue = self.latitudeValue
        BBQUserDefaults.sharedInstance.longitudeValue = self.longitudeValue
        headerView.locationLabel.text = self.branchName
//        if BBQUserDefaults.sharedInstance.accessToken != "" {
//            BBQUserDefaults.sharedInstance.CurrentSelectedLocation = self.branchName
//            //self.headerView.locationButton?.setAttributedTitle(setAttrubuttedTitleForButtonsWith(title:  " \(self.branchName) "), for: .normal)
//            
//        }
      //  BBQUserDefaults.sharedInstance.branchIdValue = self.branchIDValue
    }
    
    @IBAction func outletNameClicked(){
        if (upcomingReservationsArray.count > 0) {
            self.overlayView.isHidden = true
            self.stackViewForUpcomingReservation.isHidden = false
        } else {
            self.overlayView.isHidden = true
            self.stackViewForUpcomingReservation.isHidden = true
        }
        self.setupReserveButtons()
        self.getOutletForBooking()
        self.setupUIBasedOnReservations()
    }
    
    //MARK : - locationDataUpdated called when location changes
    override func locationDataUpdated(_ notification: Notification) {
        if let dictionary = notification.userInfo as NSDictionary? {
            self.latitudeValue = (dictionary["lat"] as! NSString).doubleValue
            self.longitudeValue = (dictionary["long"] as! NSString).doubleValue
            self.branchIDValue = dictionary["branchId"] as! String
            let branchName = dictionary["branchName"] as! String
            BBQUserDefaults.sharedInstance.CurrentSelectedLocation = branchName
            BBQUserDefaults.sharedInstance.branchIdValue = branchIDValue
            self.showSelectedOuletNameOnButton(branchName)
            self.branchName = branchName
            //self.headerView.locationButton?.setAttributedTitle(setAttrubuttedTitleForButtonsWith(title:  " \(branchName) "), for: .normal)
            headerView.locationLabel.text = branchName
            saveOuletValues()
            if let branch = findBranchBy(id: BBQUserDefaults.sharedInstance.branchIdValue), branch.only_delivery{
                showOnlyDeliveryAlert()
            }else{
                self.getPromotionsAndVouchersFrom(branchLatitude: self.latitudeValue, branchLongitude: self.longitudeValue, branchID: self.branchIDValue)
            }
        }
    }
    
    override func dismissOutletSelection() {
        if let branch = findBranchBy(id: BBQUserDefaults.sharedInstance.branchIdValue), branch.only_delivery{
            showOnlyDeliveryAlert()
        }
    }
    
    // MARK: Tap Geture Methods

    @objc func overlayTapped(_ sender: UITapGestureRecognizer? = nil) {
        
        UIView.animate(withDuration: 0.25, animations: {
            self.stackViewForUpcomingReservation.isHidden = false
            self.view.layoutIfNeeded()
        })
        self.overlayView.isHidden = true
    }
    
    @objc func reserveMiniTaped(_ sender: UITapGestureRecognizer? = nil) {

        if self.upcomingReservationsArray.count < 1 {
            return
        }
        
        UIView.animate(withDuration: 0.25, animations: {
            self.stackViewForUpcomingReservation.isHidden = true
            self.view.layoutIfNeeded()
        })
        self.showSelectedOuletNameOnButton(BBQUserDefaults.sharedInstance.CurrentSelectedLocation)
        self.overlayView.isHidden = false

    }
    
    // MARK: - todayPressed action
    @IBAction func todayPressed(_ sender: UIButton) {
        self.selectedDateType = .Today
        self.setupReserveButtons()
        if self.handleOverlayAndOutletSelection() {
            self.showBookingBottomSheet(for: .Today)
        }
        self.setupUIBasedOnReservations()
    }
    
    // MARK: - tomorrowPressed action
    @IBAction func tomorrowPressed(_ sender: UIButton) {
        self.selectedDateType = .Tomorrow
        self.setupReserveButtons()
        if self.handleOverlayAndOutletSelection() {
            self.showBookingBottomSheet(for: .Tomorrow)
        }
        self.setupUIBasedOnReservations()
    }
    
    // MARK: - updateDateFromCalendar action
    func updateDateFromCalendar(originalDate: String, formattedDate: String) {
        self.perform(#selector(self.showBookingForSelectedDate(formattedDate:)), with: formattedDate, afterDelay: 0.7)
    }
    
    @objc private func showBookingForSelectedDate(formattedDate: String) {
        self.showBookingBottomSheet(for: .Custom, dateForCustomSelection: formattedDate)
    }
    
    // MARK: - selectadatePressed action
    @IBAction func selectadatePressed(_ sender: UIButton) {
        self.selectedDateType = .Custom
        self.setupReserveButtons()
        if self.handleOverlayAndOutletSelection() {
            showCalendarBottomSheet()
        }
        self.setupUIBasedOnReservations()
    }
    
    private func setupUIBasedOnReservations(){
        if(upcomingReservationsArray.count>0){
            self.stackViewForUpcomingReservation.isHidden = false
        } else {
            self.stackViewForUpcomingReservation.isHidden = true
        }

        self.view.layoutIfNeeded()
    }
    
    private func handleOverlayAndOutletSelection() -> Bool {
        if (upcomingReservationsArray.count > 0) {
            self.overlayView.isHidden = true
            self.stackViewForUpcomingReservation.isHidden = false
        } else {
            self.overlayView.isHidden = true
            self.stackViewForUpcomingReservation.isHidden = true
        }
        self.view.layoutIfNeeded()
        if BBQUserDefaults.sharedInstance.CurrentSelectedLocation.count == 0 {
            getOutletForBooking()
            return false
        }
        
        return true
    }
    
    // MARK: - showCalendarBottomSheet action
    private func showCalendarBottomSheet() {
        //Create instance for the controller that will be shown
        let viewController = BottomSheetCalendarViewController()
        viewController.delegate = self
        //Present your controller over current controller
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: self.view.frame.size.height - viewController.view.frame.size.height)
        let bottomSheet = BottomSheetController(configuration: configuration)
        bottomSheet.present(viewController, on: self)
    }
    
    // MARK: - reserveClicked action
    @IBAction func reserveClicked(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .H13)
        self.selectedDateType = .Today
        if(upcomingReservationsArray.count>0) {
            self.stackViewForUpcomingReservation.isHidden = false
        } else{
            self.stackViewForUpcomingReservation.isHidden = true
        }
        if self.handleOverlayAndOutletSelection() {
            self.showBookingBottomSheet(for: .Today)
        }
        self.view.layoutIfNeeded()
    }
    
    @IBAction func onClickBtnViewDetailsCard(_ sender: UIButton) {
        if (BBQUserDefaults.sharedInstance.accessToken.count == 0) {
            //guest user
            ToastHandler.showToastWithMessage(message: KLoyaltyLoginText)
        }else{
            let smileHistoryViewController = UIStoryboard.loadMyBenefitsController()
            smileHistoryViewController.selectedTab = sender.tag
            //            smileHistoryViewController.loyaltyTransactionsArray  = []
            //            smileHistoryViewController.loyalityModel = nil
            smileHistoryViewController.voucherCouponModelModel = self.voucherCouponModelModel
            //   smileHistoryViewController.vouchers = self.vouchers
            self.navigationController?.pushViewController(smileHistoryViewController, animated: true)
            if sender.tag == 0{
                AnalyticsHelper.shared.triggerEvent(type: .H05)
            }else if sender.tag == 1{
                AnalyticsHelper.shared.triggerEvent(type: .H05A)
            }else if sender.tag == 2{
                AnalyticsHelper.shared.triggerEvent(type: .H05B)
            }
        }
    }
    
    @IBAction func onClickBtnCardHeader(_ sender: UIButton) {
    
        UIView.animate(withDuration: 0.3, delay: 0, options: .autoreverse) {
            self.setUnSelectedCard(sender)
        } completion: { isCompleted in
            self.setUnSelectedCard(sender)
        }

//        UIView.animate(withDuration: 0.3, delay: 0, options: .allowUserInteraction) {
//
//        } completion: { isCompleted in
//
//        }

        
    }
    private func setUnSelectedCard(_ sender: UIButton) {
        if sender == btnSmilesSection, !sender.isSelected {
            btnSmilesSection.isSelected = true
            btnCouponSection.isSelected = false
            btnVoucherSection.isSelected = false
            constraintForLiner.constant = 2
        }else if sender == btnCouponSection, !sender.isSelected {
            btnSmilesSection.isSelected = false
            btnCouponSection.isSelected = true
            btnVoucherSection.isSelected = false
            constraintForLiner.constant = btnCouponSection.frame.size.width
        }else if sender == btnVoucherSection, !sender.isSelected {
            btnSmilesSection.isSelected = false
            btnCouponSection.isSelected = false
            btnVoucherSection.isSelected = true
            constraintForLiner.constant = (btnCouponSection.frame.size.width * 2) - 2
        }
    }
    
    @IBAction func didPressedOutletInfoButton(_ sender: UIButton) {
        AnalyticsHelper.shared.triggerEvent(type: .H12)
        let controller = UIStoryboard.loadOutletInfoViewController()
        controller.view.layoutIfNeeded()
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func didPressedDiningButton(_ sender: UIButton) {
        AnalyticsHelper.shared.triggerEvent(type: .H06)
//        let controller = UIStoryboard.loadOutletInfoViewController()
//        controller.view.layoutIfNeeded()
//        self.navigationController?.pushViewController(controller, animated: true)
        self.selectedDateType = .Today
        if(upcomingReservationsArray.count>0) {
            self.stackViewForUpcomingReservation.isHidden = false
        } else{
            self.stackViewForUpcomingReservation.isHidden = true
        }
        if self.handleOverlayAndOutletSelection() {
            self.showBookingBottomSheet(for: .Today)
        }
        self.view.layoutIfNeeded()
    }
    
    @IBAction func actionOnDeliveryTakeawayBtn(_ sender: Any) {
//        if let tabBarController = navigationController?.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
//            tabBarController.selectedIndex = 2
//        }
        AnalyticsHelper.shared.triggerEvent(type: .H04)
        let smileHistoryViewController = UIStoryboard.loadSmileHistoryViewController()
        self.navigationController?.pushViewController(smileHistoryViewController, animated: true)
    }
    
    @IBAction func actionOnBarbequeInABoxBtn(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .H03)
        if let tabBarController = navigationController?.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
            tabBarController.selectedIndex = 2
        }
    }
    
    func callNumber(phone : String)
    {
        let phoneUrl = URL(string: "telprompt://\(phone.replacingOccurrences(of: " ", with: ""))")
        let phoneFallbackUrl = URL(string: "tel://\(phone.replacingOccurrences(of: " ", with: ""))")
        if(phoneUrl != nil && UIApplication.shared.canOpenURL(phoneUrl!)) {
            UIApplication.shared.open(phoneUrl!) { (success) in
                if(!success) {
                    // Show an error message: Failed opening the url
                }
            }
        } else if(phoneFallbackUrl != nil && UIApplication.shared.canOpenURL(phoneFallbackUrl!)) {
            UIApplication.shared.open(phoneFallbackUrl!) { (success) in
                if(!success) {
                    // Show an error message: Failed opening the url
                }
            }
        } else {
            // Show an error message: Your device can not do phone calls.
        }
    }
    
    // MARK: Test Method , TODO: Remove after review
    
    func showAfterDining()  {
        let diningVC = UIStoryboard.loadAfterDining()
        diningVC.view.layoutIfNeeded()
        let proposedHeight =  UIScreen.main.bounds.height - (diningVC.tableView.contentSize.height + 150)
        let configuration = BottomSheetConfiguration(animationDuration: 0.4,
                                                     minimumTopSpacing: CGFloat(proposedHeight))
        let bottomSheetController = BottomSheetController(configuration: configuration)
        diningVC.diningBottomSheet = bottomSheetController
        bottomSheetController.present(diningVC, on: self)
        
    }
    
    func checkForReservationDetails(){
        if let bookingId = bookingIdForPrint, bookingId != ""{
            bookingIdForPrint = nil
            let reservationDetailsVC = ReservationDetailsVC()
            reservationDetailsVC.bookingID = bookingId
            self.navigationController?.pushViewController(reservationDetailsVC, animated: true)
        }
        
    }
    
    //MARK :Show Overlay when Reserve Clicked
    @IBAction func confirmationReserveClicked(_ sender: Any){
    }
    
    private func showBookingBottomSheet(for dateType: BBQBookingDateType, dateForCustomSelection: String = "") {
        let bookingBSHeight = CGFloat(465.00) // As per UX
        let minimumTop = UIScreen.main.bounds.height - bookingBSHeight
        let configuration = BottomSheetConfiguration(animationDuration: 0.4,
                                                     minimumTopSpacing: minimumTop)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        bottomSheetController.isFromBookingScreen = true
        let bookingVC = BBQBookingMasterViewController()
        bookingVC.bookingBottomSheet = bottomSheetController
        bookingVC.selectedDateType = dateType
        bookingVC.selectedformattedDate = dateForCustomSelection
        bookingVC.showCalendarInBooking = false
        bookingVC.delegate = self
        bookingVC.superVC = self.navigationController?.topViewController
        self.navigationController?.pushViewController(bookingVC, animated: true)
    }
    
    // MARK: - showOnlyDeliveryAlert called when user select only delivery branch
    func showOnlyDeliveryAlert() {
        PopupHandler.showTwoButtonsPopup(title: "Dining not available",
                                         message: String(format: "%@ not having dining facility please select other branch", BBQUserDefaults.sharedInstance.CurrentSelectedLocation),
                                         image: nil,
                                         titleImageFrame: CGRect(x: 0, y: -1, width: 16, height: 20),
                                         on: self,
                                         firstButtonTitle: "Change Branch", firstAction: {
                                            self.outletClicked()
        },
                                         secondButtonTitle: "Goto Delivery") {
            self.navigateToHomePage()
            if let tabBarController = self.navigationController?.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
                tabBarController.selectedIndex = 2
            }
        }
    }
    
    
    
    // MARK: - showLocationAlert called when user not given location permission
    func showLocationAlert() {
        PopupHandler.showTwoButtonsPopup(title: kUpdateLocationTitle,
                                         message: kUpdateLocationMessage,
                                         image: UIImage(named: "icon_location_orange"),
                                         titleImageFrame: CGRect(x: 0, y: -1, width: 16, height: 20),
                                         on: self,
                                         firstButtonTitle: kAlertAllow, firstAction: {
                                            BBQLocationManager.shared.updateUserLocationWithPermission()
                                            
                                            if(self.userDeniedLocation){
                                                self.showLocationStatusDeniedAlert()
                                            }
        },
                                         secondButtonTitle: kAlertDeny) {
                                            if BBQUserDefaults.sharedInstance.branchIdValue != ""{
                                                self.branchIDValue = BBQUserDefaults.sharedInstance.branchIdValue
                                            }
                                            self.getPromotionsAndVouchersFrom(branchLatitude: self.latitudeValue, branchLongitude: self.longitudeValue, branchID: self.branchIDValue)
        }
    }
    
    
    // MARK: - showLocationStatusDeniedAlert called when user denied the location permission
    func showLocationStatusDeniedAlert() {
        PopupHandler.showTwoButtonsPopup(title: kLocationDisabledTitle,
                                         message: kLocationDisabledMessage,
                                         on: self,
                                         firstButtonTitle: kCancelString,
                                         firstAction: {
                                            if BBQUserDefaults.sharedInstance.branchIdValue != ""{
                                                self.getPromotionsAndVouchersFrom(branchLatitude: self.latitudeValue, branchLongitude: self.longitudeValue, branchID: BBQUserDefaults.sharedInstance.branchIdValue)
                                            }else{
                                                self.getPromotionsAndVouchersFrom(branchLatitude: self.latitudeValue, branchLongitude: self.longitudeValue, branchID: self.branchIDValue)

                                            }
        },
                                         secondButtonTitle: kOpenSettings) {
                                            if #available(iOS 10.0, *) {
                                                let settingsURL = URL(string: UIApplication.openSettingsURLString)!
                                                UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
                                            } else {
                                                if let url = NSURL(string:UIApplication.openSettingsURLString) {
                                                    UIApplication.shared.openURL(url as URL)
                                                }
                                            }
        }
    }
    //Mark : bookingConfirmed - notification called when booking confirmed
    @objc func bookingConfirmed(_ notification: Notification) {
        
        self.dismiss(animated: false) {
            if let dictionary = notification.userInfo as NSDictionary?, let type = dictionary["type"] as? String, type == "reschedule" {
                self.dismiss(animated: false, completion: nil)
            }
        }
        
        self.getTheUpcomingReservation()
        self.getUpdatedPoints()
        
        if let dictionary = notification.userInfo as NSDictionary?, let type = dictionary["type"] as? String, type == "booking" {
            self.showRatingswith(bookingId: (dictionary["booking_id"] as? String)!)
        }
//        self.getPromotionsAndVouchersFrom(branchLatitude:self.latitudeValue, branchLongitude: self.longitudeValue, branchID: self.branchIDValue)
    }
    
    //Mark : bookingCancelled - notification called when booking cancelled
    @objc func bookingCancelled(_ notification: Notification){
        self.getPromotionsAndVouchersFrom(branchLatitude:self.latitudeValue, branchLongitude: self.longitudeValue, branchID: self.branchIDValue)
    }
    
    //Mark : bookingCancelled - notification called when booking cancelled diring booking process in booking confirmation screen
    @objc func bookingFlowCancelled(_ notification: Notification){
        self.upcomingReservationCollectionView.reloadData()
//        if let dictionary = notification.userInfo as NSDictionary? {
//            self.showRatingswith(bookingId: (dictionary["booking_id"] as? String)!)
//        }
    }
    
    private func openBookingFeedbackScreen(_ notification: Notification){
        self.upcomingReservationCollectionView.reloadData()
        if let dictionary = notification.userInfo as NSDictionary? {
            self.showRatingswith(bookingId: (dictionary["booking_id"] as? String)!)
        }
    }
    
    @objc func showBookingFromHamMenu(_ notification: Notification) {
        self.perform(#selector(self.performBookingScreenPresentation), with: nil, afterDelay: 0.7)
    }
    
    @objc func performBookingScreenPresentation() {
        self.selectedDateType = .Today
        if self.handleOverlayAndOutletSelection() {
            self.showBookingBottomSheet(for: .Today)
        }
    }
    
    @objc func cartDataUpdated(_ notification: Notification) {
        self.initializeInitialItemQuatity()
    }
    
    @objc func updateHeader(_ notification: Notification) {
        self.initializeHeaderView()
        self.headerView.addTheInitialValues()
        self.checkCartData()
        self.getUpdatedPoints()
    self.showSelectedOuletNameOnButton(BBQUserDefaults.sharedInstance.CurrentSelectedLocation)
    }
    
    func fecthTheSmilesCount() {
        self.loyalityViewModel.getLoyalityPoints(pageNumber: 0, pageSize: 0, sort: Constants.App.LoyalitiPage.sortValue) { (isSuccess) in
            if isSuccess {
//                BBQActivityIndicator.hideIndicator(from: self.view)
//                self.lblNameTitle.text = BBQUserDefaults.sharedInstance.UserName
//                self.lblPhoneNumberSmile.text = BBQUserDefaults.sharedInstance.customerId
//                self.loyaltyTransactionsArray.removeAll()
//                self.loyalityModel = self.loyalityViewModel.getLoyalityModel
//                if let loyaltyTransactions = self.loyalityModel?.loyaltyTransactions {
//                    self.loyaltyTransactionsArray.append(contentsOf: loyaltyTransactions)
//                }
//
//                if let availablePoints = self.loyalityModel?.availablePoints{
//                    self.lblAvailableSmile.text = String(format: "%d Smiles", availablePoints)
//                    self.lblNumberOfSmilesToShare.text =  String(format: "%d Smiles", availablePoints)
//                }
//                self.tblTransactionHistory.isHidden = false
//                self.tblTransactionHistory.reloadData()
                
            }else{
                BBQActivityIndicator.hideIndicator(from: self.view)
            }
        }
    }
    
    // MARK: - getTheNearbyOutletsFrom called to get the neaby outlets of the given lattitude and longitude
    func getTheNearbyOutletsFrom(userLatitude:Double,userLongitude:Double){
        self.homeViewModel.getNearbyOutlets(latitide:userLatitude, longitude: userLongitude) { (isSuccess) in
            if(self.homeViewModel.getNearbyLocationData().count>0){
                let locationArray:[NearbyOutelt] = self.homeViewModel.getNearbyLocationData()
                if(locationArray.count>0){
                    let nearbyOutletObj = locationArray[0]
                    self.latitudeValue = ((nearbyOutletObj.nearbyBranchLatitude ?? "") as NSString).doubleValue
                    self.longitudeValue = ((nearbyOutletObj.nearbyBranchLongitude ?? "") as NSString).doubleValue
                    self.branchIDValue = nearbyOutletObj.nearbyBranchId ?? ""
                    BBQUserDefaults.sharedInstance.branchIdValue = self.branchIDValue
                    if let locationName = nearbyOutletObj.nearbyBranchName{
                        BBQUserDefaults.sharedInstance.CurrentSelectedLocation = locationName
                    }
                    self.branchName = nearbyOutletObj.nearbyBranchName ?? ""
                    if BBQUserDefaults.sharedInstance.accessToken != "" {
                        self.bbqLastVisitedViewModel.getLastVisitedBranch(id: self.branchIDValue) {_ in
                            //                print(self.bbqLastVisitedViewModel)
                        }
                        
                    }
                }else{
                    self.latitudeValue = 0.0
                    self.longitudeValue = 0.0
                    self.branchIDValue = ""
                }
            }else{
                self.branchName = BBQUserDefaults.sharedInstance.CurrentSelectedLocation
            }
            self.showSelectedOuletNameOnButton(self.branchName)
            self.saveOuletValues()

            self.getPromotionsAndVouchersFrom(branchLatitude: self.latitudeValue, branchLongitude: self.longitudeValue, branchID: self.branchIDValue)
            self.collectionViewForNearbyOutlets.reloadData()
            
        }
    }
    
    func getTheNearbyOutletsFromV1(){
//        if brandlistForHomePage.count > 2{
//            collectionViewForBrands.reloadData()
//            stackViewForBrands.isHidden = false
//        }else{
        var lat: CGFloat = 0
        var long: CGFloat = 0
            if let branch = findBranchBy(id: BBQUserDefaults.sharedInstance.branchIdValue){
                lat = branch.latitude
                long = branch.longitude
            }else{
                return
            }
            self.homeViewModel.getNearbyOutletsV1(latitide: lat, longitude: long, isInitialLoad: true) { (isSuccess) in
                if brandlistForHomePage.count > 1{
                    self.hideUnhideBrands(isHidden: false)
                    self.collectionViewForBrands.reloadData{
                        self.collectionViewForBrands.reloadData()
                    }
                }else{
                    self.hideUnhideBrands(isHidden: true)
                }
            }
//        }
        
    }
    
    func hideUnhideBrands(isHidden: Bool) {
        UIView.animate(withDuration: 0.2) {
            self.stackViewForBrands.isHidden = isHidden
        } completion: { isCompleted in
            self.stackViewForBrands.isHidden = isHidden
        }

    }
    
    // MARK: - getPromotionsAndVouchers called to show promotion and vouchers when there is a neabyoutlet
    func getPromotionsAndVouchersFrom(branchLatitude:Double,branchLongitude:Double,branchID:String){
        
        self.showShimmering()
        self.getOrderFromQueue()

        self.homeViewModel.getPromosVouchers(latitide: branchLatitude, longitude: branchLongitude, brach_ID: branchID) {(isSuccess) in
            
            self.stopShimmering()
            
            if(self.promotionsArray.count>0){
                self.promotionsArray.removeAll()
            }
            if(self.vouchersArray.count>0){
                self.vouchersArray.removeAll()
            }
            if  self.refreshControl.isRefreshing{
                self.refreshControl.endRefreshing()
            }
            self.pageControl.isHidden = true

            if isSuccess {
                self.promotionsArray = self.homeViewModel.getPromotionsDataData()
                //self.promotionPageControl.isHidden = false
                if self.promotionsArray.count > 0 {
                    self.pageControl.numberOfPages = self.promotionsArray.count
                    self.pageControl.isHidden = false
                }
                self.promoViewModel = PromosViewModel(promosDataArray: self.promotionsArray)
                self.vouchersArray = self.homeViewModel.getVouchersData()
                //lblUpcomingCount.text = String(format: "%li of %li", 1, upcomingReservationsArray.count)
                if self.vouchersArray.count > 0 {
                    
                    self.lblVoucherCount.text = String(format: "1 of %li",  self.vouchersArray.count)
                }
                self.initializeInitialItemQuatity()
            } else {
            }
            DispatchQueue.main.async {
                self.promotionCollectionView .reloadData()
                self.vouchersCollectionView.reloadData()
                //Fixed crash issue by Sakir Sherasiya
                self.promotionCollectionView.setContentOffset(CGPoint.zero, animated: true)
            }
            if BBQUserDefaults.sharedInstance.accessToken.count>0{
                self.getTheUpcomingReservation()

            }else{
                self.upcomingReservationCollectionView.isHidden = true
                self.stackViewForUpcomingReservation.isHidden = true
                self.setupReserveButtons()
                self.viewDidLayoutSubviews()
            }
            
            //self.getTheNearbyOutletsFromV1()
            self.view.layoutIfNeeded()
            
        }
    }
    
    
    // MARK: - fetchUserVoucherAndCouponCount
    func fetchCouponAndVoucherCount() {
        
        if !isFirstTimeLoaded{
            showShimmeringMyBenefits()
        }

        lblSmilesCoins.text = "\(BBQUserDefaults.sharedInstance.UserLoyaltyPoints)"
        if BBQUserDefaults.sharedInstance.accessToken != "" {
            self.voucherCouponListWithCountViewModel.getCouponAndVoucherListWithCount()  { ( isSuccess ) in
                self.lblSmilesCoins.text = "\(BBQUserDefaults.sharedInstance.UserLoyaltyPoints)"
                if isSuccess {
                    //Received all values in voucherCouponListWithCountViewModel , now display on UI and retain to send to myBenifit controller
                    self.voucherCouponModelModel = self.voucherCouponListWithCountViewModel.getVouchersAndCouponsModel
                    
                    if let referalAvailable = self.voucherCouponModelModel?.referral_available {
                        isAppReferAndEarnActivated  = referalAvailable
                        self.hideUnhideReferralView()
                    }
                    
                    if let available = self.voucherCouponModelModel?.VCData?.couponCounts?.available{
                        self.viewForCoupons.isHidden = false
                        self.lblCouponsValue.text = "\(available)"
                    }else{
                        self.viewForCoupons.isHidden = true
                    }
                    
                    if let available = self.voucherCouponModelModel?.VCData?.voucherCounts?.available{
                        self.viewForVouchers.isHidden = false
                        self.lblVoucherValue.text = "\(available)"
                    }else{
                        self.viewForVouchers.isHidden = true
                    }
                    
                }else{
                    BBQActivityIndicator.hideIndicator(from: self.view)
                    self.viewForVouchers.isHidden = true
                    self.viewForCoupons.isHidden = true
                }
                self.stopShimmeringMyBenefits()
            }
        }
        self.view.layoutIfNeeded()
    }
    
    private func hideUnhideReferralView(){
        if isAppReferAndEarnActivated == true {
            appReferalView.isHidden = false
            AppReferralViewIconOnTop.isHidden = false
            if BBQUserDefaults.sharedInstance.isGuestUser == true {
                appReferalView.isHidden = true
                AppReferralViewIconOnTop.isHidden = true
            }
        }else{
            appReferalView.isHidden = true
            AppReferralViewIconOnTop.isHidden = true

            
        }
    }
    // MARK: - getTheUpcomingReservation called to get the 5 upcoming reservation of the logged in user
    func getTheUpcomingReservation(){
        
        if BBQUserDefaults.sharedInstance.accessToken != "" {
            if !self.isUserLoggedOut{
                self.showShimmering()
                
            }
            
            //self.upcomingArray.removeAll()
            self.homeViewModel.getUpcomingReservations { (isSuccess) in
                if !self.isUserLoggedOut{
                    self.stopShimmering()
                }

                if isSuccess {
                    self.upcomingReservationsArray = self.homeViewModel.getReservationsData()
                    DispatchQueue.main.async {
                        print(self.upcomingReservationsArray)
                        //self.pageControlUpCommingReservation.numberOfPages = self.upcomingReservationsArray.count
                        self.pageControlUpCommingReservation.isHidden = true
                        self.lblUpcomingCount.text = String(format: "%li of %li", 1, self.upcomingReservationsArray.count)
                        
                        //MARK: - DEL-1622 fix for incorrect upcoming reservations
                        
                        self.pageControlUpCommingReservation.numberOfPages = self.upcomingReservationsArray.count
//                        if self.upcomingReservationsArray.count > 5{
//                            self.pageControlUpCommingReservation.numberOfPages = 5
//                            self.lblUpcomingCount.text = String(format: "%li of %li", 1, 5)
//                        }
                        
                        //END
                        if(self.upcomingReservationsArray.count>0){
                            AnalyticsHelper.shared.triggerEvent(type: .H14)
                            self.upcomingReservationCollectionView.isHidden = false
                            self.stackViewForUpcomingReservation.isHidden = false
                            
                        }else{
                            self.upcomingReservationCollectionView.isHidden = true
                            self.stackViewForUpcomingReservation.isHidden = true
                            self.setupReserveButtons()

                            self.viewDidLayoutSubviews()
                        }
                        self.upcomingReservationCollectionView .reloadData()
                    }
                }else{
                    self.upcomingReservationCollectionView.isHidden = true
                    self.stackViewForUpcomingReservation.isHidden = true
                    self.setupReserveButtons()
                    self.homeScrollView.frame.size.height = self.homeScrollView.frame.size.height - 153
                    self.viewDidLayoutSubviews()
                    
                }
            }
        }else{
            self.upcomingReservationCollectionView.isHidden = true
            self.stackViewForUpcomingReservation.isHidden = true
            self.setupReserveButtons()
            self.homeScrollView.frame.size.height = self.homeScrollView.frame.size.height - 153
            self.viewDidLayoutSubviews()
        }
        
         self.view.layoutIfNeeded()
    }
    
    // MARK: - showSelectedOuletNameOnButton called to show the selected outelt name
    func showSelectedOuletNameOnButton(_ branch_name:String){
        if branch_name != ""{
            let nameText = " \(branch_name) "
            self.locationNameButton.contentHorizontalAlignment = .left
            let attributedString1 = NSMutableAttributedString(string:"\(nameText)")
            attributedString1.addAttribute(.font,
                                           value: UIFont.appThemeSemiBoldWith(size: 14.0),
                                           range: NSRange(location: 0,length: attributedString1.length))
            self.locationNameButton.setAttributedTitle(attributedString1, for: .normal)
            self.outletInfoButton.isHidden = false
        } else{
            let defaultLocation = NSMutableAttributedString(string: kDefaultLocation)
            self.outletInfoButton.isHidden = true
            defaultLocation.addAttribute(.font, value: UIFont.appThemeRegularWith(size: 14.0),
                                         range: NSRange(location: 0,length: defaultLocation.length))
            self.locationNameButton.setAttributedTitle(defaultLocation, for: .normal)
        }
    }
    
    override func outletClicked() {
        self.setupReserveButtons()
        self.getOutletForBooking()
        self.setupUIBasedOnReservations()

    }
    
    func showRatingswith(bookingId:String) {
        if BBQUserDefaults.sharedInstance.isUserRatedBBQApp {
            return // Not doing any action once user rated BBQ App already
        }
        let configuration = BottomSheetConfiguration(animationDuration: 0.4,
                                                     minimumTopSpacing: UIScreen.main.bounds.size.height - 300)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        let cancellationViewController = BBQUsersRatingCell()
        cancellationViewController.selectedBookingID = bookingId
        cancellationViewController.currentBottomSheet = bottomSheetController
        cancellationViewController.ratingcellDelegate = self
        
        //cancellationViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        bottomSheetController.present(cancellationViewController, on: BBQHomeViewController.getTopViewControllerInWindow() ?? self )
    }
    
    // MARK: Shimmer Methods
    
    private func showShimmering() {
        self.scrollBackgroundView.isHidden = false
        
        if let shimmer = self.shimmerView {
            self.scrollBackgroundView.addSubview(shimmer)
            shimmer.contentView = shimmerViewPromo
            shimmer.shimmerAnimationOpacity = 0.2
            shimmer.shimmerSpeed = 500.00
            shimmer.isShimmering = true
        }
      
    }
    
    private func stopShimmering() {

        if let shimmer = self.shimmerView {
            shimmer.isShimmering = false
            shimmer.isHidden = true
        }
        self.shimmerViewPromo.isHidden = true
    }
    
    
    private func showShimmeringMyBenefits() {
        self.viewAnimationMybenefitsBg.isHidden = false
        
        if let shimmer = self.shimmerViewMyBenefits {
            self.viewAnimationMybenefitsBg.addSubview(shimmer)
            shimmer.contentView = viewAnimationMybenefits
            shimmer.shimmerAnimationOpacity = 0.2
            shimmer.shimmerSpeed = 500.00
            shimmer.isShimmering = true
        }
      
    }
    
    private func stopShimmeringMyBenefits() {

        if let shimmer = self.shimmerViewMyBenefits {
            shimmer.isShimmering = false
            shimmer.isHidden = true
        }
        self.viewAnimationMybenefitsBg.isHidden = true
    }
}

extension BBQHomeViewController: BBQBookingMasterViewControllerProtocol {
    func didSelectLocationOutletButton(bottomSheet: BottomSheetController?, selectedDateType: BBQBookingDateType, selectedformattedDate: String) {
        self.selectedDateType = selectedDateType
        bottomSheet?.dismissBottomSheet(on: self, completion: { (isDismissed) in
            if isDismissed {
                if selectedDateType == .Custom {
                    self.showBookingBottomSheet(for: selectedDateType, dateForCustomSelection: selectedformattedDate)
                } else {
                    self.showBookingBottomSheet(for: selectedDateType, dateForCustomSelection: "")
                }
            }
        })
    }
    
    func didSelectCancelPayButton(bottomSheet: BottomSheetController?) {
        bottomSheet?.dismissBottomSheet(on: self)
    }
    
    //This delegate is called when outlet selecteion is changed from inside bottom sheet
    //And in selected outlet dining is not available
    func didCanceledDueToDineInUnavailable(bottomSheet: BottomSheetController?) {
        bottomSheet?.dismissBottomSheet(on: self, completion: { (isDismissed) in
            if isDismissed {
                self.dismissOutletSelection()
            }
        })
    }
}
extension BBQHomeViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionViewForNearbyOutlets{
            return self.homeViewModel.getNearbyLocationData().count
        }
        if collectionView == collectionViewForBrands{
            return brandlistForHomePage.count
        }
        if(collectionView.tag == Constants.TagValues.promotionCellTag) {
            return promotionsArray.count
        } else if(collectionView.tag == Constants.TagValues.voucherCellTag) {
            return vouchersArray.count
        }else if(collectionView.tag == Constants.TagValues.openOrderCellTag) {
            
            guard  let itemExists = cartItem else {
                return 0
            }
            return itemExists.onGoingOrders.count
            
        }else{
            // If upcoming reservation count is more than 5 show only 5 reservations
            //MARK: - DEL-1622 Fix for incorrect upcoming reservations
//            if(self.upcomingReservationsArray.count>5){
//                return 5
//            }else{
//                return self.upcomingReservationsArray.count
//            }
            return self.upcomingReservationsArray.count
            //END
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionViewForNearbyOutlets{
            if self.homeViewModel.getNearbyLocationData().count > indexPath.row, let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NearbyOutletCell", for: indexPath) as? NearbyOutletCell{
                cell.setCellData(outlet: self.homeViewModel.getNearbyLocationData()[indexPath.row])
                return cell
            }
            return collectionView.defaultCell(indexPath: indexPath)
        }
        if collectionView == collectionViewForBrands{
            if brandlistForHomePage.count > indexPath.row, let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BBQHomeBrandCell", for: indexPath) as? BBQHomeBrandCell{
                cell.setCellData(brandInfo: brandlistForHomePage[indexPath.row])
                return cell
            }
            return collectionView.defaultCell(indexPath: indexPath)
        }
        var cell  = collectionView.defaultCell(indexPath: indexPath)
        if(collectionView.tag == Constants.TagValues.promotionCellTag){
            let promoCell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CellIdentifiers.promotionCell, for: indexPath) as! BBQPromotionCell
            if(promotionsArray.count>0){
                let  promodel:PromotionModel = promotionsArray[indexPath.row]
                promoCell.setPrmotionCellValues(promodel)
                
            }
            cell = promoCell
        } else  if(collectionView.tag == Constants.TagValues.voucherCellTag) {
            let voucherCell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CellIdentifiers.voucherCell, for: indexPath) as! BBQVoucherCell
            if (vouchersArray.count > 0) {
                let  vouchModel:VouchersModel = vouchersArray[indexPath.row]
                voucherCell.tag = indexPath.row
                voucherCell.cartViewModel = self.cartViewModel
                voucherCell.setVoucherCellvalues(vouchModel, voucherQuatity: self.itemsQuantity[indexPath.row])
                voucherCell.voucherPurchaseView.loginDelegate = self
                voucherCell.voucherPurchaseView.serviceDelegate = self
            }
            cell = voucherCell
        } else if(collectionView.tag == Constants.TagValues.openOrderCellTag){
            
            let openOrderCell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CellIdentifiers.openOrderCell, for: indexPath) as! BBQOpenOrderCollectionViewCell
            if (cartItem?.onGoingOrders.count ?? 0 > 0) {
                let  orderModel:Order = (cartItem?.onGoingOrders[indexPath.row])!
                openOrderCell.tag = indexPath.row
                openOrderCell.openCellDelegate = self
                openOrderCell.rateButton.tag = indexPath.row
                openOrderCell.setData(orderDetails: orderModel)
                switch  (orderModel.state.lowercased() ) {
                    
                case  "received" :  openOrderCell.backgroundColor = UIColor.hexStringToUIColor(hex: "#F7B030")
                    openOrderCell.imageView.image = UIImage(named: "StatusAccepted")
                    
                    break
                case  "dispatched" :  openOrderCell.backgroundColor = UIColor.hexStringToUIColor(hex: "#F58840")
                    openOrderCell.imageView.image = UIImage(named: "StatusDispatched")
                    break
                
                case  "delivered"  : openOrderCell.backgroundColor = UIColor.hexStringToUIColor(hex: "#2B9C4C")
                    openOrderCell.imageView.image = UIImage(named: "StatusDelivered")
                    break
                 
                case  "prepared" : openOrderCell.backgroundColor = UIColor.hexStringToUIColor(hex: "#F58840")
                    openOrderCell.imageView.image = UIImage(named: "StatusDispatched")
                    break
                    
                case  "accepted" : openOrderCell.backgroundColor = UIColor.hexStringToUIColor(hex: "#F04B24")
                    openOrderCell.imageView.image = UIImage(named: "StatusAccepted")
                    break
                    
                default:
                    
                    print("Dont change color")
                }
                openOrderCell.layoutIfNeeded()
                return openOrderCell

            }
            return openOrderCell
          //  cell = openOrderCell
            
        }else {
            let reservationCell:BBQUpcomingReservationCell = collectionView.dequeueReusableCell(withReuseIdentifier:Constants.CellIdentifiers.upcomingReservationCell, for: indexPath) as! BBQUpcomingReservationCell
            if (self.upcomingReservationsArray.count>0){
                if(indexPath.row < 5){
                    let reveremodel:UpcomingBookingModel = upcomingReservationsArray[indexPath.row]
                    reservationCell.setUpcomingReservationCellvalues(reveremodel, delegate: upcomingReservationModel)
                }
            }
            cell = reservationCell
        }
        return cell
    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 0
//    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 0
//    }
    
}

extension BBQHomeViewController:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = view.frame.size.width
        if collectionView == collectionViewForNearbyOutlets{
            return CGSize(width: 282, height: 282)
        }
        if collectionView ==  collectionViewForBrands{
            var count = 2
            if brandlistForHomePage.count >= 3{
                count = 3
            }
            return CGSize(width: (collectionView.frame.size.width / CGFloat(count)), height: collectionViewForBrands.frame.size.height - 4)
        }else if(collectionView.tag == Constants.TagValues.promotionCellTag){
            // in case you you want the cell to be 40% of your controllers view
            //return CGSize(width: width * 0.9, height: 238)
            return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
        } else if(collectionView.tag == Constants.TagValues.reservationCellTag){
            return CGSize(width: collectionView.frame.width, height: 150)
        }
        else if(collectionView.tag == Constants.TagValues.openOrderCellTag){

//            return CGSize(width: (collectionView.frame.width - 10), height: (openOrdersCollectionView.frame.height  - 20))
            return collectionView.frame.size

        }else{
            return CGSize(width: collectionView.frame.width, height: 190)
        }
    }
}

extension BBQHomeViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionViewForBrands{
            if brandlistForHomePage.count > indexPath.row, brandlistForHomePage[indexPath.row].id == ""{
                openReservationController()
                return
            }
            if let tabBarController = navigationController?.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
                if brandlistForHomePage.count > indexPath.row{
                    tempSelectedBrandForDelivery = brandlistForHomePage[indexPath.row].id
                    if let branch = findBranchBy(id: BBQUserDefaults.sharedInstance.branchIdValue){
                        brandSelectedLat = branch.latitude
                        brandSelectedLong = branch.longitude
                    }
                }
                tabBarController.selectedIndex = 2
            }
        }else if( collectionView.tag == Constants.TagValues.promotionCellTag) {
            showPromotionScreen(selectedRow: indexPath.row, screenType: .Home)
        } else if (collectionView.tag == Constants.TagValues.reservationCellTag){
            //Upcoming Reservation
//            AnalyticsHelper.shared.triggerEvent(type: .H14A)
//            let reveremodel:UpcomingBookingModel = upcomingReservationsArray[indexPath.row]
//            print(reveremodel.bookingId as Any)
//
//            // pusing the booking history storyboard
//            let  bookingHistoryController = UIStoryboard.loadReservationViewController()
//            //setting the booking id for booking history screen
//            bookingHistoryController.bookingID = reveremodel.bookingId!
//            bookingHistoryController.isComingFromNotification = false
//            self.navigationController?.pushViewController(bookingHistoryController, animated: true)
        }  else if (collectionView.tag == Constants.TagValues.openOrderCellTag){
            if (cartItem?.onGoingOrders.count ?? 0) < indexPath.row{
                return
            }
            if let is_schedule_delivery = cartItem?.onGoingOrders[indexPath.row].is_schedule_delivery, is_schedule_delivery{
                return
            }
            //If order is already delivered , do not show status view controller,
            //instead directly open rating page
            if cartItem?.onGoingOrders[indexPath.row].state == "Delivered" {
                
                AnalyticsHelper.shared.triggerEvent(type: .DH05)
                let  rateViewController = UIStoryboard.loadFeedBackViewController()
                if  let order = cartItem?.onGoingOrders[indexPath.row] {
                    rateViewController.orderID = order.orderId
                    rateViewController.transactionType = order.transaction_type ?? 1
                    rateViewController.branchName = order.branchName
                    rateViewController.brandlogo = order.brand_logo
                    rateViewController.orderDate = order.createdOn
                    rateViewController.deliveryThemeColor = order.backgroudColor
                    rateViewController.deliveryThemeTextColor = order.textColor
                    rateViewController.feedbackDelegate = self
                    bottomSheetController?.present(rateViewController, on: self, viewHeight: 480)
                }
                return
            }
            
            let deliveryOrderDetailsVC = UIUtils.viewController(storyboardId: "OrderStatus", vcId: "BBQOrderStatusVIewControllerViewController") as! BBQOrderStatusVIewControllerViewController
             deliveryOrderDetailsVC.viewModel = DeliveryStatusViewModel()
            if (cartItem?.onGoingOrders.count ?? 0 > 0) {
                guard let  orderModel = cartItem?.onGoingOrders[indexPath.row] else {
                    return
                }
                deliveryOrderDetailsVC.orderId =  orderModel.orderId
                deliveryOrderDetailsVC.orderTransactionType =  orderModel.transaction_type ?? 1

                
            }
            AnalyticsHelper.shared.triggerEvent(type: .H15)
            self.navigationController?.pushViewController(deliveryOrderDetailsVC, animated: true)
        }else {
            
            if !(BBQUserDefaults.sharedInstance.isGuestUser){
                //Vouchers
                AnalyticsHelper.shared.triggerEvent(type: .H07)
                AnalyticsHelper.shared.triggerEvent(type: .Home_Happinesscard)
                showVoucherScreen(selectedRow: indexPath.row)
            }else{
                
                ToastHandler.showToastWithMessage(message: kHamMenuLoginTitle)

            }
        }
    }
    
    func showPromotionScreen(selectedRow: Int, screenType: PromoScreenType) {
        AnalyticsHelper.shared.triggerEvent(type: .H02)
        if let promoData = self.promoViewModel?.promosArray {
            let promotion = promoData[selectedRow]
            if promotion.promotion_url.lowercased() == "ubq"{
                if let tabBarController = navigationController?.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
                    tabBarController.selectedIndex = 2
                    isClickedOnUbq = true
                }
            }else if promotion.promotion_url.lowercased() == "res"{
                NotificationCenter.default.post(name: Notification.Name(Constants.NotificationName.showBookingScreen),
                                                object: nil)
            }else if promotion.promotion_url.lowercased() == "hpc"{
                isClickedOnHpc = true
                scrollToHappinessCard()
            } else if promotion.promotion_url.lowercased() == "ref" {
                
                AnalyticsHelper.shared.triggerEvent(type: .RE012)

                let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: 80)
                bottomSheetController = BottomSheetController(configuration: configuration)
                
                let vc = BBQShareReferalCodeViewController(nibName: "BBQShareReferalCodeViewController", bundle: nil)
                bottomSheetController?.present(vc, on: self, viewHeight: self.view.frame.size.height - (self.view.frame.size.height * (20/100)))
            }else{
                let promotionDetailScreen = UIStoryboard.loadPromotionDetailScreen()
                promotionDetailScreen.promotionDeelegate = self
                promotionDetailScreen.promotionId = Int(promoData[selectedRow].promoId ?? "0") ?? 0
                promotionDetailScreen.modalPresentationStyle = .fullScreen
                self.present(promotionDetailScreen, animated: true, completion: nil)
            }
        }
    }
    
    private func scrollToHappinessCard(){
        if isClickedOnHpc{
            isClickedOnHpc = false
            homeScrollView.scrollRectToVisible(vouchersCollectionView.frame, animated: true)
        }
    }
    
    func showVoucherScreen(selectedRow: Int) {
        let voucherDetailScreen = UIStoryboard.loadVoucherDetailScreen()
        voucherDetailScreen.delegate = self
        voucherDetailScreen.voucherId = Int(self.vouchersArray[selectedRow].voucherId ?? 0)
        voucherDetailScreen.voucherTitle = self.vouchersArray[selectedRow].voucherName
        voucherDetailScreen.modalPresentationStyle = .fullScreen
        self.present(voucherDetailScreen, animated: true, completion: nil)
    }
    
}

extension BBQHomeViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        for collect in scrollView.subviews{
            if collect.isKind(of: BBQPromotionCell.self){
                let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
                pageControl.currentPage = Int(pageNumber)
            }
            
            if collect.isKind(of: BBQOpenOrderCollectionViewCell.self){
                
              let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
                pageControlForOpenOrders.currentPage = Int(pageNumber)
                //Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
            }
            
            if collect.isKind(of: BBQUpcomingReservationCell.self){
                
              let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
                //pageControlUpCommingReservation.currentPage = Int(pageNumber)
                lblUpcomingCount.text = String(format: "%li of %li", Int(pageNumber)+1, upcomingReservationsArray.count)
            }
            
            if collect.isKind(of: BBQVoucherCell.self){
                
              let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
                //pageControlUpCommingReservation.currentPage = Int(pageNumber)
                lblVoucherCount.text = String(format: "%li of %li", Int(pageNumber)+1, vouchersArray.count)
            }
        }
    }
}

extension BBQHomeViewController: BBQAllOffersProtocol, BBQPromotionDetailScreenProtocol,
BottomSheetImagesViewControllerProtocol {
    
    func didPressedTableReservationFromPromotion() {
        self.showBookingForToday()
    }
    
    func didPressedReserveTableFromPromotionDetails() {
        self.showBookingForToday()
    }
    
    func userVoucherSelectedWith(voucherList: [VoucherCouponsData], type: ScreenType, termsAndConditions: [String]) {
        
    }
    
    func userVoucherSelectedWith(voucherList: [VoucherCouponsData], type: ScreenType) {
        
    }
   
    func buyNowButtonClicked(bottomSheet: BottomSheetController?) {
        if let presentSheet = bottomSheet {
            presentSheet.dismissBottomSheet(on: self)
        }
        
        self.perform(#selector(self.cartButtonClicked), with: nil, afterDelay: 1.0)
    }
    
    private func showBookingForToday() {
        self.selectedDateType = .Today
        self.setupReserveButtons()
        if self.handleOverlayAndOutletSelection() {
            self.showBookingBottomSheet(for: .Today)
        }
        self.setupUIBasedOnReservations()
    }

}

extension BBQHomeViewController:BBQUsersRatingProtocol{
    func ratingDismissed(){
        self.upcomingReservationCollectionView.reloadData()
        if upcomingReservationsArray.count > 1{
            self.upcomingReservationCollectionView?.scrollToItem(at: IndexPath(row: 0, section: 0),
                                                                 at: .centeredHorizontally,
                                                                 animated: true)
        }
    }
}

extension BBQHomeViewController: BBQVoucherPurchaseViewProtocol, LoginControllerDelegate {
    func cartUpdated() {
        self.checkCartData()
    }
    
    func proceedToLoginScreen() {
        self.navigateToLogin()
    }
    
    func navigateToLogin() {
//        let viewController = UIStoryboard.loadLoginViewController()
//        viewController.delegate = self
//        viewController.navigationFromScreen = .Voucher
//        viewController.shouldCloseTouchingOutside = true
//        viewController.currentTheme = .dark
//        viewController.modalPresentationStyle = .overCurrentContext
       // self.present(viewController, animated: true, completion: nil)
        let tabViewController = UIStoryboard.loadLoginThroughPhoneController()
        tabViewController.delegate = self
        tabViewController.navigationFromScreen = .Booking
    self.navigationController?.pushViewController(tabViewController, animated: false)
        
    }
    
    func gotoHomeScreen(isExistingUser: Bool, navigationFrom: LoginNavigationScreen) {
        if !isExistingUser &&  !(BBQUserDefaults.sharedInstance.isGuestUser){
            self.showRegistrationConfirmationPage(navigationFrom: navigationFrom)
        }
        myBenifitsCountView.isHidden = BBQUserDefaults.sharedInstance.isGuestUser ? true : false

        NotificationCenter.default.post(name: Notification.Name(Constants.NotificationName.upadateHomeHeader), object: nil)
    }
    
    func showRegistrationConfirmationPage(navigationFrom: LoginNavigationScreen) {
        let registrationConfirmationVC = UIStoryboard.loadRegistrationConfirmation()
        registrationConfirmationVC.modalPresentationStyle = .overCurrentContext
        registrationConfirmationVC.titleText = kSignUpConfirmationTitleText
        registrationConfirmationVC.navigationText = kSignUpConfirmationNavigationText
        
        let keyWindow = UIApplication.shared.keyWindow?.rootViewController?.presentedViewController
        keyWindow?.present(registrationConfirmationVC, animated: false) {
            sleep(UInt32(2.0))
            self.dismiss(animated: false) {
                
            }
        }
    }
}

extension BBQHomeViewController: BBQVoucherPurchaseViewServiceProtocol {
    func itemQuatityDidAdd(index: Int) {
       
        /* Disabling unit incraese in home screen
        let currentQuatity = self.itemsQuantity[index]
        self.itemsQuantity.remove(at: index)
        self.itemsQuantity.insert(currentQuatity + 1, at: index)
        self.vouchersCollectionView.reloadData() */
        AnalyticsHelper.shared.triggerEvent(type: .H08)
        self.cartUpdated()
        self.cartButtonClicked()
    }
    
    func itemQuatityDidSubtract(index: Int) {
        let currentQuatity = self.itemsQuantity[index]
        self.itemsQuantity.remove(at: index)
        self.itemsQuantity.insert(currentQuatity - 1, at: index)
        self.vouchersCollectionView.reloadData()
        
        self.cartUpdated()
    }
    
    func itemQuatityDidRemove(index: Int) {
        self.itemsQuantity.remove(at: index)
        self.itemsQuantity.insert(0, at: index)
        self.vouchersCollectionView.reloadData()
        
        self.cartUpdated()
    }
    
}

extension BBQHomeViewController: BBQVoucherDetailScreenProtocol {
    func didPressProceedToCart() {
        self.cartButtonClicked()
    }
    
    // MARK:- App update check methods
    
    private func checkAppUpdate() {
        self.homeViewModel.getAppupdateInformation { (isUpdateRequired) in
            if isUpdateRequired {
                self.showAppUpdateWarining(doLocationUpdate: true)
            } else {
                self.checkAndUpdateHomeDataWithLocation()
            }
        }
    }
    
    private func showAppUpdateWarining(doLocationUpdate: Bool) {
        
        if homeViewModel.isForceUpdate {
            PopupHandler.showSingleButtonsPopup(title: kAppUpdateTitle,
                                                message: kAppUpdateMessage,
                                                on: self,
                                                firstButtonTitle: kAppUpdateButtonString) {
                                                    self.openAppstoreForUpdate()
            }
        } else if homeViewModel.isRecommendUpdate {
            PopupHandler.showTwoButtonsPopup(title: kAppUpdateTitle,
                                             message: kAppUpdateMessage,
                                             isCentered: true,
                                             on: self,
                                             firstButtonTitle: kAppUpdateButtonString,
                                             firstAction: {
                                                self.openAppstoreForUpdate()
                                                
            }, secondButtonTitle: kCancelString) {
                if doLocationUpdate {
                    self.checkAndUpdateHomeDataWithLocation()
                }
            }
        }
        
    }
    
    private func openAppstoreForUpdate() {
        if let url = URL(string: "itms-apps://itunes.apple.com/app/id1080269411"),
            UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @objc func appMovedToForeground() {
        self.homeViewModel.getAppupdateInformation { (isUpdateRequired) in
            if isUpdateRequired {
                self.showAppUpdateWarining(doLocationUpdate: false)
            }
        }
    }
}

extension BBQHomeViewController: BaseViewControllerDelegate{
    func didUpdatedLoyalityPoints(points: String) {
        //self.lblSmileCountCart.text = points
    }
}
extension BBQHomeViewController :OrderStatusViwDelegate {
    func goToOrderDetails(orderID: String) {
        
        //remove view
        if let viewWithTag = self.view.viewWithTag(991) {
                viewWithTag.removeFromSuperview()
            }else{
            }
        //go to track order page
        
        let deliveryOrderDetailsVC = UIUtils.viewController(storyboardId: "OrderStatus", vcId: "BBQOrderStatusVIewControllerViewController") as! BBQOrderStatusVIewControllerViewController
         deliveryOrderDetailsVC.viewModel = DeliveryStatusViewModel()
         deliveryOrderDetailsVC.orderId = orderID
         self.navigationController?.pushViewController(deliveryOrderDetailsVC, animated: true)
        
    }
    
}
//this delegate is called when user click on rate order for a completed order
extension BBQHomeViewController : OpenOrderCellDelegate {
    
    func clickedOnRateOrder( selectedIndex: Int) {
        
        AnalyticsHelper.shared.triggerEvent(type: .H16)

        let  rateViewController = UIStoryboard.loadFeedBackViewController()
        
        if let order = cartItem?.onGoingOrders[selectedIndex] {
            rateViewController.orderID = order.orderId
            rateViewController.transactionType = order.transaction_type ?? 1
            rateViewController.branchName = order.branchName
            rateViewController.brandlogo = order.brand_logo
            rateViewController.orderDate = order.createdOn
            rateViewController.deliveryThemeColor = order.backgroudColor
            rateViewController.deliveryThemeTextColor = order.textColor
            bottomSheetController?.present(rateViewController, on: self, viewHeight: 480)
        }
        
    }
    
}

extension BBQHomeViewController: DeliveryTabBarControllerDelegate{
    func nearbyOutletsFrom(userLatitude: Double, userLongitude: Double) {
        if userLatitude != 0, userLongitude != 0{
            self.latitudeValue = userLatitude
            self.longitudeValue = userLongitude
            getTheNearbyOutletsFrom(userLatitude: userLatitude, userLongitude: userLongitude)
        }else{
            getTheNearbyOutletsFrom(userLatitude: latitudeValue, userLongitude: longitudeValue)
        }
    }
    
    func promotionsAndVouchersFrom(branchLatitude: Double, branchLongitude: Double, branchID: String) {
        if branchLatitude != 0, branchLongitude != 0, branchID != ""{
            self.branchIDValue = branchID
            getPromotionsAndVouchersFrom(branchLatitude: branchLatitude, branchLongitude: branchLongitude, branchID: branchID)
        }else if branchLatitude != 0, branchLongitude != 0{
            getPromotionsAndVouchersFrom(branchLatitude: branchLatitude, branchLongitude: branchLongitude, branchID: self.branchIDValue)
        }else if branchID != ""{
            self.branchIDValue = branchID
            getPromotionsAndVouchersFrom(branchLatitude: self.latitudeValue, branchLongitude: self.longitudeValue, branchID: branchID)
        }else{
            getPromotionsAndVouchersFrom(branchLatitude: self.latitudeValue, branchLongitude: self.longitudeValue, branchID: self.branchIDValue)
        }
    }
}

//MARK: On click of ratings dismiss the rate main view and go to rating detail page
extension BBQHomeViewController :FeedbackDetailedDelegate {
    
    func actionOnDone(viewController : FeedbackDetailedViewController){
    
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
extension BBQHomeViewController: SKStoreProductViewControllerDelegate {
    func openStoreProductWithiTunesItemIdentifier(_ identifier: String) {
        
        AnalyticsHelper.shared.triggerEvent(type: .VU01)

        UIUtils.showLoader(message: "loading app store")
        
        
        let storeViewController = SKStoreProductViewController()
        storeViewController.delegate = self

        let parameters = [ SKStoreProductParameterITunesItemIdentifier : identifier]
        storeViewController.loadProduct(withParameters: parameters) { [weak self] (loaded, error) -> Void in
            if loaded {
                
                DispatchQueue.main.async {
                    UIUtils.hideLoader()
                    self?.present(storeViewController, animated: true, completion: nil)
                }
            }else{
                    
                    
                }
            
        }
    }
    private func productViewControllerDidFinish(viewController: SKStoreProductViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    //click on install button at bottom of home screen
    
    @IBAction func installUpdatedApp(_ sender: UIButton){
        
        AnalyticsHelper.shared.triggerEvent(type: .VU02)

        self.openStoreProductWithiTunesItemIdentifier("1080269411")

    }
}
extension BBQHomeViewController:  ReferalDelegate {
    func shareReferalCode() {
        
        AnalyticsHelper.shared.triggerEvent(type: .RE01B)

        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: 80)
        bottomSheetController = BottomSheetController(configuration: configuration)
        
        let vc = BBQShareReferalCodeViewController(nibName: "BBQShareReferalCodeViewController", bundle: nil)
        bottomSheetController?.present(vc, on: self, viewHeight: self.view.frame.size.height - (self.view.frame.size.height * (20/100)))
    }
    
    @IBAction func clickedShareYourRefereal(_ sender: UIButton){
        
        AnalyticsHelper.shared.triggerEvent(type: .RE01A)

        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: 80)
        bottomSheetController = BottomSheetController(configuration: configuration)
        
        let vc = BBQShareReferalCodeViewController(nibName: "BBQShareReferalCodeViewController", bundle: nil)
        bottomSheetController?.present(vc, on: self, viewHeight: self.view.frame.size.height - (self.view.frame.size.height * (20/100)))
    }
    
}

