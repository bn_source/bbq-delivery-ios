//
//  BBQPromotionCell.swift
//  Barbeque Nation
//
//  Created by Maya R on 24/07/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

class BBQPromotionCell: UICollectionViewCell {
    @IBOutlet weak var promotionLabel:UILabel!
    @IBOutlet weak var promotionImageView:UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //self.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
    }
    
    func setPrmotionCellValues(_ promotionObj:PromotionModel){
        self.promotionLabel.text = "\(promotionObj.promotion_title ?? "")"
        LazyImageLoad.setImageOnImageViewFromURL(imageView: self.promotionImageView, url: promotionObj.promotion_image!) { (image) in
        }
    }
}
