//
 //  Created by Arpana on 25/12/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified BBQUpdateAlertviewViewController.swift
 //

import UIKit

class BBQUpdateAlertviewViewController: UIViewController {

    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var lblVersionMsg: UILabel!
    @IBOutlet weak var lblReleaseNote: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblAppstoreVersion: UILabel!
    var releaseNoteForUpdatedVersion: String = "Minor updates and improvements."
     var title1: String? = "Update App"
    var appLocalVersion : String = ""
    var appStoreVersion: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
     

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lblVersionMsg.text = title1
        lblReleaseNote.text = releaseNoteForUpdatedVersion

        containerView.layer.masksToBounds = false
        containerView.layer.shadowColor = UIColor.lightGray.cgColor
        containerView.layer.shadowOffset = CGSize(width: 0, height: 3)
        containerView.layer.shadowRadius = 12
        containerView.layer.shadowOpacity = 0.5
        lblAppstoreVersion.text = String(format: "Version %@", appStoreVersion)
        
        
        
    }
    var callBack: (()-> Void)?
    
    func setValues(localV:String , storeV : String , releaseNote: String) {
        
         appLocalVersion = localV
         appStoreVersion = storeV
        
        let msg =  String(format: "We've got news! Our new Barbequenation app update is now available.\nUpdate now to enjoy the best barbeque experience with our all-new app!")
        title1 =  msg
        releaseNoteForUpdatedVersion = releaseNote
        
      }
    
    @IBAction func doDismiss(_ sender:Any) {
           self.presentingViewController?.dismiss(animated: true)
       }
    
    override func viewDidLayoutSubviews() {
           super.viewDidLayoutSubviews()
           preferredContentSize.height = containerView.frame.size.height + containerView.frame.origin.y + 30
       // preferredContentSize.width =  self.view.superview?.frame.self.width ?? 10

       }

    @IBAction func okTapped(_ sender: Any) {
        
       
        callBack?()
          self.dismiss(animated: false, completion: nil)
      }
}
