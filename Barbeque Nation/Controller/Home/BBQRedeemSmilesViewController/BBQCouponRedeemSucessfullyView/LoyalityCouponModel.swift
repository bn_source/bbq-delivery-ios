//
 //  Created by Mahmadsakir on 22/12/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified LoyalityCouponModel.swift
 //

import Foundation

// MARK: - Welcome
class LoyalityCoupon: Codable {
    let data: [Datum]

    init(data: [Datum]) {
        self.data = data
    }
    
    // MARK: - Datum
    class Datum: Codable {
        let message, messageType: String
        var status: Bool = true
        let vouchers: [Voucher]

        enum CodingKeys: String, CodingKey {
            case message
            case messageType = "message_type"
            case vouchers
        }

        init(message: String, messageType: String, vouchers: [Voucher]) {
            self.message = message
            self.messageType = messageType
            self.vouchers = vouchers
            self.status = message == "success" ? true : false
        }
        
        
        // MARK: - Voucher
        class Voucher: Codable {
            let barCode: String
            let denomination: Int
            let message, validity: String
            let voucherHeadID: Int
            let pax_applicable: Int

            enum CodingKeys: String, CodingKey {
                case barCode = "bar_code"
                case denomination, message, validity
                case voucherHeadID = "voucher_head_id"
                case pax_applicable = "pax_applicable"
            }

            init(barCode: String, denomination: Int, message: String, validity: String, voucherHeadID: Int, pax_applicable: Int) {
                self.barCode = barCode
                self.denomination = denomination
                self.message = message
                self.validity = validity
                self.voucherHeadID = voucherHeadID
                self.pax_applicable = pax_applicable
            }
        }
    }
}
