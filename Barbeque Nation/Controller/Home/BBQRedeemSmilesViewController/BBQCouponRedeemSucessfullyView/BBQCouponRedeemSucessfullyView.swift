//
 //  Created by Mahmadsakir on 17/12/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified BBQCouponRedeemSucessfullyView.swift
 //

import UIKit

class BBQCouponRedeemSucessfullyView: UIViewController {
    
    //MARK:- View properties
    @IBOutlet weak var lblSuccessMessage: UILabel!
    @IBOutlet weak var lblUsedSmile: UILabel!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var lblUsedDate: UILabel!
    @IBOutlet weak var lblCouponCode: UILabel!
    @IBOutlet weak var lblCouponAmount: UILabel!
    @IBOutlet weak var btnViewCoupon: UIButton!
    @IBOutlet weak var btnSmileHistory: UIButton!
    @IBOutlet weak var btnCopyCouponCode: UIButton!
    @IBOutlet weak var viewForCoupon: UIView!
    
    //MARK:- Class properties
    var delegate: CouponRedeemSucessfullyViewDelegate?
    var voucher: LoyalityCoupon.Datum.Voucher!
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setTheme()
        setData()
    }
    
    private func setTheme() {
        lblSuccessMessage.setTheme(size: 17, weight: .semibold, color: .text)
        lblUsedSmile.setTheme(size: 14, weight: .regular, color: .white)
        lblUsedDate.setTheme(size: 14, weight: .regular, color: .white)
        lblCouponCode.setTheme(size: 14, weight: .regular, color: .white)
        lblCouponAmount.setTheme(size: 28, weight: .regular, color: .white)
        btnViewCoupon.titleLabel?.setTheme(size: 10, weight: .semibold, color: .white)
        btnSmileHistory.titleLabel?.setTheme(size: 10, weight: .semibold, color: .white)
        btnShare.titleLabel?.setTheme(size: 8, weight: .semibold, color: .theme)
        
        btnViewCoupon.imageView?.contentMode = .scaleAspectFit
        btnSmileHistory.imageView?.contentMode = .scaleAspectFit
        viewForCoupon.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
        btnShare.roundCorners(cornerRadius: btnShare.frame.size.height/2.0, borderColor: .clear, borderWidth: 0)
        btnSmileHistory.roundCorners(cornerRadius: btnSmileHistory.frame.size.height/2.0, borderColor: .clear, borderWidth: 0)
        btnViewCoupon.roundCorners(cornerRadius: btnViewCoupon.frame.size.height/2.0, borderColor: .clear, borderWidth: 0)
    }
    
    func setData() {
        lblUsedSmile.attributedText = setAttributedCouponData("Smiles Used", String(voucher.denomination))
        lblUsedDate.attributedText = setAttributedCouponData("Valid till", voucher.validity)
        lblCouponCode.attributedText = setAttributedCouponData("Coupon Code", voucher.barCode)
        lblCouponAmount.attributedText = setAttributedCouponAmount(Constants.CurrencySymbol.rupeeSymbol + String(voucher.denomination))
    }
    
    private func setAttributedCouponData(_ title: String, _ value: String) -> NSAttributedString{
        let atrString = NSMutableAttributedString(string: title, attributes: [ NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.systemFontOfSize(size: 14, weight: .semibold)])
        atrString.append(NSAttributedString(string: ": \(value)", attributes: [ NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.systemFontOfSize(size: 14, weight: .regular)]))
        return atrString
    }
    
    private func setAttributedCouponAmount(_ amount: String) -> NSAttributedString{
        let atrString = NSMutableAttributedString(string: "\(amount)\n", attributes: [ NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.systemFontOfSize(size: 29, weight: .bold)])
        atrString.append(NSAttributedString(string: "Coupon", attributes: [ NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.systemFontOfSize(size: 18, weight: .regular)]))
        return atrString
    }
    

    //MARK:- Button Action
    @IBAction func onClickBtnClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickBtnShareCoupon(_ sender: Any) {
        
    }
    @IBAction func onClickBtnCopyCouponCode(_ sender: Any) {
        UIPasteboard.general.string = voucher.barCode
    }
    @IBAction func onClickBtnViewCoupons(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        delegate?.didSelectCouponHistory(view: self)
    }
    @IBAction func onClickBtnSmileHistory(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        delegate?.didSelectSmileHistory(view: self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
protocol CouponRedeemSucessfullyViewDelegate{
    func didSelectSmileHistory(view: BBQCouponRedeemSucessfullyView)
    func didSelectCouponHistory(view: BBQCouponRedeemSucessfullyView)
}
