//
 //  Created by Mahmadsakir on 16/12/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified BBQRedeemSmilesViewController.swift
 //

import UIKit

class BBQRedeemSmilesViewController: UIViewController {
    
//    //MARK:- View Properties
//    @IBOutlet weak var lblTitle: UILabel!
//    @IBOutlet weak var lblSmilesLabel: UILabel!
    @IBOutlet weak var lblSmilesCount: UILabel!
//    @IBOutlet weak var btnPartialPoints: UIButton!
//    @IBOutlet weak var btnFullPoints: UIButton!
//    @IBOutlet weak var btnApplyPoints: UIButton!
//    @IBOutlet weak var txtRedeemPoint: UITextField!
    @IBOutlet weak var btnRedeemPoint: UIButton!
    @IBOutlet weak var viewForContainer: UIView!
//    @IBOutlet weak var viewForTextView: UIView!
//    @IBOutlet weak var stackViewForTextView: UIStackView!
//    @IBOutlet weak var lblBlankLabel: UILabel!
//    @IBOutlet weak var viewForSmileCount: UIView!
    
    @IBOutlet weak var lblpartial: UILabel!
    @IBOutlet weak var lblAll: UILabel!
    @IBOutlet weak var lblTitleForInputSmiley: UILabel!
    
    @IBOutlet weak var viewAllSmiles: UIView!
    @IBOutlet weak var partialsmileBtn:UIButton!
    @IBOutlet weak var allSmileBtn:UIButton!
    @IBOutlet weak var partialSmiles : UIView!
    @IBOutlet weak var viewSmileCount: UIView!
    @IBOutlet weak var txtFieldpartialSmiles: UITextField!
    @IBOutlet weak var partialSmileStackView: UIStackView!
    
    //MARK:- Class properties
    var loyalityModel : BBQLoyalityPointsDataModel? = nil
    var delegate: BBQRedeemSmilesViewDelegate?
    
    
    var selectedTag: Int? = 0
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        setTheme()
        setData()
    }

    private func setTheme() {

        btnRedeemPoint.dropShadow()
        
        viewAllSmiles.setCornerRadius(8)
       // viewSmileCount.setCornerRadius(8)
        partialSmiles.setCornerRadius(8)
                    btnRedeemPoint.alpha = 0.5
                    btnRedeemPoint.isEnabled = false
        btnRedeemPoint.setCornerRadius(8)
        txtFieldpartialSmiles.setCornerRadius(12)
        txtFieldpartialSmiles.addLeftView( UIView(frame: CGRect(x: 0, y: 0, width: 15, height: txtFieldpartialSmiles.frame.height)))
        
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.hexStringToUIColor(hex: "#666666") ,
                          .font: UIFont(name:Constants.App.BBQAppFont.ThemeRegular, size: 16.0)]
        
        txtFieldpartialSmiles.attributedPlaceholder = NSAttributedString(string: "Enter smile to be used",
                                                                         attributes: attributes as [NSAttributedString.Key : Any])
        
    }
    
    private func setData(){
        if let availablePoints = self.loyalityModel?.availablePoints{
            lblSmilesCount.text = String(format: "%d Smiles available", availablePoints)
        }
    }
    
    private func unSelectButton(_ sender: UIButton){
        sender.isSelected = false
        sender.alpha = 0.5
    }
    
    private func selectButton(_ sender: UIButton){
        sender.isSelected = true
        sender.alpha = 1.0
    }
    
    //MARK:- Button Action
//    @IBAction func onClickBtnUsePoints(_ sender: UIButton) {
//        if sender == btnPartialPoints{
//            unSelectButton(btnFullPoints)
//            viewForTextView.isHidden = false
//            lblBlankLabel.isHidden = true
//            onEditingOfRedeemAmount(txtRedeemPoint as Any)
//        }else{
//            unSelectButton(btnPartialPoints)
//            viewForTextView.isHidden = true
//            lblBlankLabel.isHidden = false
//            btnRedeemPoint.isEnabled = true
//            btnRedeemPoint.alpha = 1.0
//        }
//        selectButton(sender)
//    }
    
    @IBAction func onClickBtnApplyPoints(_ sender: Any) {
        
    }
    @IBAction func onClickBtnRedeemNow(_ sender: Any) {
        
        var isOptionSelected = false
        if partialsmileBtn.tag == 1 || allSmileBtn.tag == 1{
            isOptionSelected = true
        }
        if !isOptionSelected {
            UIUtils.showToast(message: "Please select an option first")
            return
        }
        var points = 0
        if let availablePoints = self.loyalityModel?.availablePoints{
            points = availablePoints
        }else{
            return
        }
        if allSmileBtn.tag == 1{
            convertIntoCoupons(points: String(points))
        }else if let redeemPoint = Int(txtFieldpartialSmiles.text ?? "0"), redeemPoint <= points, points > 0{
            convertIntoCoupons(points: String(redeemPoint))
        }else{
            UIUtils.showToast(message: "Please enter valid number of smilies") }

    }
    
    @IBAction func onEditingOfRedeemAmount(_ sender: Any) {
        var points = 0
        if let availablePoints = self.loyalityModel?.availablePoints{
            points = availablePoints
        }else{
            return
        }
        if let amount = Int(txtFieldpartialSmiles.text ?? "0"), amount > 0, amount <= points{
            btnRedeemPoint.isEnabled = true
            btnRedeemPoint.alpha = 1.0
        }else{
            btnRedeemPoint.isEnabled = false
            btnRedeemPoint.alpha = 0.5
        }
    }
    
    //MARK:- Web Service
    func convertIntoCoupons(points: String)  {
        
        UIUtils.showToast(message: "Not implemented yet ")
        return
  //      UIUtils.showLoader()
//        BBQLoyalityPointsManager.postConvertLoyalityToCoupons(currency_code: Constants.App.NotificationPage.INR, mobile_number: String(format: "%li", SharedProfileInfo.shared.profileData?.mobileNumber ?? ""), points: points) { (loyalityCoupon, isSuccess) in
//            UIUtils.hideLoader()
//            if let isSuccess = isSuccess, isSuccess, let loyalityCoupon = loyalityCoupon?.data.first{
//                if loyalityCoupon.status, let voucher = loyalityCoupon.vouchers.first{
//                    self.dismiss(animated: true) {
//                        self.delegate?.onSuccess(view: self, loyalityCoupon: voucher)
//                    }
//                }else{
//                    ToastHandler.showToastWithMessage(message: loyalityCoupon.message)
//                }
//            }else{
//
//            }
//        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

protocol BBQRedeemSmilesViewDelegate{
    func cancel(view: BBQRedeemSmilesViewController)
    func onSuccess(view: BBQRedeemSmilesViewController, loyalityCoupon: LoyalityCoupon.Datum.Voucher)
}
extension BBQRedeemSmilesViewController {
    
    @IBAction func onClickBtnAllSmiles(_ sender: UIButton) {

        switch sender.tag {
        case 0:
            //its not selected select it
            if partialsmileBtn.tag == 1 {
                partialSmileStackView.isUserInteractionEnabled = false
            }
            enableDisabledTag(isEnable: false, view: partialSmiles, label: lblpartial, btn : partialsmileBtn)
            enableDisabledTag(isEnable: true, view: viewAllSmiles, label: lblAll,  btn : allSmileBtn)
            if let availablePoints = self.loyalityModel?.availablePoints{
                txtFieldpartialSmiles.text = String(format: "%d", availablePoints)
            }
            btnRedeemPoint.isEnabled = true
            btnRedeemPoint.alpha = 1.0
            break
        case 1:
            enableDisabledTag(isEnable: false, view: viewAllSmiles, label: lblAll,  btn : allSmileBtn)

            break
        
        default: break
            
        }
        
    }
   
    
    @IBAction func onClickBtnPartialSmiles(_ sender: UIButton) {
        
        
        //check if it was not selected select it else deselect
        
        btnRedeemPoint.isEnabled = false
        btnRedeemPoint.alpha = 0.5
        
        switch sender.tag {
        case 0:
            //its not selected select it
            enableDisabledTag(isEnable: true, view: partialSmiles, label: lblpartial, btn : partialsmileBtn)
            enableDisabledTag(isEnable: false, view: viewAllSmiles, label: lblAll , btn : allSmileBtn)
            partialSmileStackView.isUserInteractionEnabled = true
            txtFieldpartialSmiles.text = ""

            break
        case 1:

            enableDisabledTag(isEnable: false, view: partialSmiles, label: lblpartial , btn : partialsmileBtn)

            partialSmileStackView.isUserInteractionEnabled = false

            break
        
        default: break
            
        }
        
    }

    private func enableDisabledTag(isEnable: Bool, view: UIView, label: UILabel , btn : UIButton){
        if isEnable{
            view.backgroundColor = UIColor.hexStringToUIColor(hex: "FEF1E4")
            view.layer.borderColor = UIColor.hexStringToUIColor(hex: "F04B24").cgColor
            label.textColor = UIColor.hexStringToUIColor(hex: "#F04B24")
            view.layer.borderWidth = 1.0
            btn.tag = 1
            //Selected Images
           
        }else{
            view.backgroundColor =  UIColor.hexStringToUIColor(hex: "F5F5F5")
            view.layer.borderColor = UIColor.text.cgColor
            label.textColor =   UIColor.hexStringToUIColor(hex: "666666")
            view.layer.borderWidth = 0.0
            btn.tag = 0
            }
        }
        
      
    @IBAction func  crossbtnClicked(_ sender : UIButton){
        
        self.dismiss(animated: true)
    }

}

class lblTitleForInputSmiley  : UILabel {
    
    override func drawText(in rect: CGRect) {
            super.drawText(in: rect.inset(by: UIEdgeInsets(top: 0, left: 30, bottom: 0, right: 20)))
        }
    }
