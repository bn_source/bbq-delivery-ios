//
 //  Created by Arpana on 28/04/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified BBQSmileyDetailedViewController.swift
 //

import UIKit

class BBQSmileyDetailedViewController: UIViewController {

    @IBOutlet weak var AvailableView: UIView!
    
    @IBOutlet weak var expiryView: UIView!
    @IBOutlet weak var redeemedView: UIView!
    @IBOutlet weak var totalEarnedView: UIView!
    
    @IBOutlet weak var lblExpired: UILabel!
    @IBOutlet weak var lblRedeemed: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblAvailable: UILabel!

    
    var loyalityModel : BBQLoyalityPointsDataModel? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        setUI()
    }

    func setUI(){
        
        expiryView.setCornerRadius(8.0)
        redeemedView.setCornerRadius(8.0)
        totalEarnedView.setCornerRadius(8.0)
        AvailableView.setCornerRadius(8.0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setData()
    }

    

    @IBAction func crossBtnClicked(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    func setData(){
        
        if self.loyalityModel != nil {
            
          
            if let pointsSpent = self.loyalityModel?.pointsSpent{
                self.lblRedeemed.text = String(pointsSpent)
            }
            if let availablePoints = self.loyalityModel?.availablePoints{
                self.lblAvailable.text = String(availablePoints)
            }
            if let totalearnedPoints = self.loyalityModel?.pointsEarned{
                self.lblTotal.text = String(totalearnedPoints)
            }
            
            let expiredSmiles  = (self.loyalityModel?.pointsEarned ?? 0) - (self.loyalityModel?.pointsSpent ?? 0) - (self.loyalityModel?.availablePoints ?? 0)
            
            self.lblExpired.text = String(expiredSmiles)
            
            
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
