//
 //  Created by Sakir Sherasiya on 14/09/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified NearbyOutletCell.swift
 //

import UIKit

class NearbyOutletCell: UICollectionViewCell {
    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var imgOutletImage: UIImageView!
    @IBOutlet weak var btnReserve: UIButton!
    @IBOutlet weak var btnOrder: UIButton!
    @IBOutlet weak var lblBrandName: UILabel!
    @IBOutlet weak var lblBranchAddress: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    
    var outlet: NearbyOutelt?
    
    private func setTheme(){
        viewForContainer.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0)
        viewForContainer.dropShadow()
        btnOrder.roundCorners(cornerRadius: 5.0, borderColor: .theme, borderWidth: 1.0)
        btnReserve.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0)
        btnReserve.backgroundColor = .theme
        btnOrder.setTitleColor(.theme, for: .normal)
        btnReserve.setTitleColor(.white, for: .normal)
        lblDistance.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0)
        imgOutletImage.maskByRoundingCorners(cornerRadius: 10, maskedCorners: CornerMask.topCornersMask)
    }
    
    func setCellData(outlet: NearbyOutelt){
        self.outlet = outlet
        setTheme()
        lblBranchAddress.text = outlet.nearbyBranchName
        lblDistance.text = outlet.getDistance()
        imgOutletImage.setImage(url: outlet.nearbyBranchImage ?? "", isForPromotions: true)
    }
    
    
}
