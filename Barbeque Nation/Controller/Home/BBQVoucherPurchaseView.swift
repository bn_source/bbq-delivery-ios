//
 //  Created by Chandan Singh on 06/01/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified BBQVoucherPurchaseView.swift
 //

import UIKit

enum BBQCartType : String {
    case CartTypeHome = "HomeScreen"
    case CartTypeVoucherDetails = "VoucherScreen"
}

protocol BBQVoucherPurchaseViewProtocol: AnyObject {
    func proceedToLoginScreen()
    func cartUpdated()
    func showCartButton(isHidden: Bool)
}

extension BBQVoucherPurchaseViewProtocol {
    func cartUpdated() { }
    func showCartButton(isHidden: Bool) { }
}

protocol BBQVoucherPurchaseViewServiceProtocol: AnyObject {
    func itemQuatityDidAdd(index: Int)
    func itemQuatityDidSubtract(index: Int)
    func itemQuatityDidRemove(index: Int)
}

class BBQVoucherPurchaseView: UIView {
    
    //MARK: IBOutlets
    @IBOutlet weak var voucherAddButton: UIButton!
    @IBOutlet weak var voucherUnitView: BBQAllOffersOrderUnit!
    
    //MARK: Properties
    weak var loginDelegate: BBQVoucherPurchaseViewProtocol? = nil
    weak var serviceDelegate: BBQVoucherPurchaseViewServiceProtocol? = nil
    
    var cartType : BBQCartType?
    var voucherId = 0
    var currentVoucherQuantity = 0
    var voucherTitle: String?
    var isCustomiseAddBtn = true
    
//    lazy var cartViewModel : CartViewModel = {
//        let viewModel = CartViewModel()
//        return viewModel
//    }()
    
    var cartViewModel = CartViewModel()
    
    //MARK: Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupUI()
    }
    
    //MARK: Setup UI
    private func setupUI() {
        if isCustomiseAddBtn {
            
            self.voucherUnitView.setCornerRadius(self.frame.width * 0.06)
            self.voucherUnitView.setShadow()
            
            self.voucherAddButton.setCornerRadius(self.frame.width * 0.06)
            self.voucherAddButton.setShadow()

        }
        self.voucherUnitView.serviceDelegate = self
    }
    
    func showButtonsWithQuantity() {
        if BBQUserDefaults.sharedInstance.isGuestUser {
            self.voucherAddButton.isHidden = false
            //self.voucherUnitView.isHidden = true
        } else {
            if self.currentVoucherQuantity == 0 {
                self.voucherAddButton.isHidden = false
                //self.voucherUnitView.isHidden = true
            } else {
                self.voucherAddButton.isHidden = true
                //self.voucherUnitView.isHidden = true
                self.voucherUnitView.setupUI(withQuatity: self.currentVoucherQuantity)
            }
        }
    }
    
    //MARK: Button Actions
    @IBAction func voucherAddButtonPressed(_ sender: UIButton) {
        
        if BBQUserDefaults.sharedInstance.isGuestUser, let delegate = self.loginDelegate {
            delegate.proceedToLoginScreen()
            return
        }
        
        if cartType == .CartTypeVoucherDetails {
            self.voucherAddButton.isHidden = true
            //self.voucherUnitView.isHidden = false
        }
        
        self.addVoucher()
    }
}

extension BBQVoucherPurchaseView: BBQAllOffersServiceProtocol {
    func itemQuatityDidAdd() {
        self.addVoucher()
    }
    
    func itemQuatityDidSubtract(quantity: Int) {
        if quantity == 0 {
            self.removeVoucher()
        } else {
            self.subtractVoucher(quantity: quantity)
        }
    }
}

//MARK: Service Calls
extension BBQVoucherPurchaseView {
    private func updateCartData() {
        if let delegate = self.loginDelegate {
            delegate.cartUpdated()
        }
    }
    
    private func addVoucher() {
        if self.cartViewModel.getItemQuantity(id: self.voucherId) > 0{
            // The below line is commented as it is also called after add to cart api
//            if let delegate = self.serviceDelegate {
//                delegate.itemQuatityDidAdd(index: self.tag)
//            }

            //This delegate is Applicable for only Voucher details screen
            if let delegate = self.loginDelegate {
                delegate.showCartButton(isHidden: false)
            }
           // return
        }
        self.cartViewModel.addToCart(productId: self.voucherId, quantity: 1) { [weak self] (isSuccess) in
            if let self = self, isSuccess {
                let cartModel = self.cartViewModel.getModifyCartModel
                if let model = cartModel, let orderItems = model.orderItems {
                    for item in orderItems {
                        if item.productId == self.voucherId {
                            if let delegate = self.serviceDelegate {
                                AnalyticsHelper.shared.triggerEvent(type: .Happinesscard_Added, properties: [.Happiness_Card_Title: item.itemTitle ?? self.voucherTitle])
                                delegate.itemQuatityDidAdd(index: self.tag)
                            }

                            //This delegate is Applicable for only Voucher details screen
                            if let delegate = self.loginDelegate {
                                delegate.showCartButton(isHidden: false)
                            }
                        }
                    }
                }
            }
        }
    }
    
    private func subtractVoucher(quantity: Int) {
        let addCartModel = self.cartViewModel.getModifyCartModel
        if let model = addCartModel, let orderItems = model.orderItems {
            for orderItem in orderItems {
                if orderItem.productId == Int(voucherId) {
                    let orderId = String(format: "%d", addCartModel?.orderId ?? "0")
                    let orderItemId = String(format: "%d", orderItem.orderItemId ?? "0")
                    
                    let updatedQuantity = (orderItem.quantity ?? 1) - 1
                    self.cartViewModel.updateOrderItem(orderId: orderId, orderItem: orderItemId, quantity: updatedQuantity) { (isSuccess) in
                        
                        //changes By Arpana
                        //Crashed: com.apple.main-thread
                        //Below methods include Ui update , and it was not moved on main thraid,
                        //So added below code
                        DispatchQueue.main.async {
                            
                            if isSuccess {
                                let modifyCartModel = self.cartViewModel.getModifyCartModel
                                for orderItem in modifyCartModel?.orderItems ?? [ModifyOrderItems]() {
                                    if orderItem.productId == self.voucherId {
                                        if let delegate = self.serviceDelegate {
                                            delegate.itemQuatityDidSubtract(index: self.tag)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    private func removeVoucher() {
        let addCartModel = self.cartViewModel.getModifyCartModel
        if let model = addCartModel, let orderItems = model.orderItems {
            for orderItem in orderItems {
                if orderItem.productId == Int(voucherId) {
                    if orderItem.quantity == 1 {
                        let orderId = String(format: "%d", addCartModel?.orderId ?? "0")
                        let orderItemId = String(format: "%d", orderItem.orderItemId ?? "0")
                        
                        self.cartViewModel.deleteOrderItem(orderId: orderId, orderItemId: orderItemId) { (isSuccess) in
                            
                            if isSuccess {
                                self.voucherAddButton.isHidden = false
                                //self.voucherUnitView.isHidden = true
                                
                                if let delegate = self.serviceDelegate {
                                    delegate.itemQuatityDidRemove(index: self.tag)
                                }
                                
                                //This delegate is Applicable for only Voucher details screen
                                if let delegate = self.loginDelegate {
                                    delegate.showCartButton(isHidden: true)
                                }
                            }
                        }
                    } else {
                        let orderId = String(format: "%d", addCartModel?.orderId ?? "0")
                        let orderItemId = String(format: "%d", orderItem.orderItemId ?? "0")
                        
                        let updatedQuantity = (orderItem.quantity ?? 1) - 1
                        self.cartViewModel.updateOrderItem(orderId: orderId, orderItem: orderItemId, quantity: updatedQuantity) { (isSuccess) in
                            
                            if isSuccess {
                                let modifyCartModel = self.cartViewModel.getModifyCartModel
                                for orderItem in (modifyCartModel?.orderItems)! {
                                    if orderItem.productId == self.voucherId {
                                        if let delegate = self.serviceDelegate {
                                            delegate.itemQuatityDidSubtract(index: self.tag)
                                        }
                                        
                                        //This delegate is Applicable for only Voucher details screen
                                        if let delegate = self.loginDelegate {
                                            self.voucherAddButton.isHidden = false
                                            //self.voucherUnitView.isHidden = true
                                            delegate.showCartButton(isHidden: true)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
