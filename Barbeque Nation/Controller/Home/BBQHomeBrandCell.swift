//
 //  Created by Sakir Sherasiya on 16/08/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified BBQHomeBrandCell.swift
 //

import UIKit

class BBQHomeBrandCell: UICollectionViewCell {
    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblLogo: UILabel!
    
    func setCellData(brandInfo: BrandInfo) {
        viewForContainer.roundCorners(cornerRadius: 5, borderColor: .clear, borderWidth: 0)
        viewForContainer.backgroundColor = .white
        imgLogo.roundCorners(cornerRadius: 5, borderColor: .clear, borderWidth: 0)
        viewForContainer.setShadow(color: brandInfo.backgroudColor, radius: 5.0)
        imgLogo.setImagePNG(url: brandInfo.logo)
        lblLogo.text = brandInfo.name
    }
}
