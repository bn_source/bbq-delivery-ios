//
 //  Created by Maya R on 30/08/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQUpcomingReservationCell.swift
 //

import UIKit

class BBQUpcomingReservationCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var upcomingReservationLabel:UILabel!
    @IBOutlet weak var reservationDate:UILabel!
    @IBOutlet weak var reservationLocation:UILabel!
    @IBOutlet weak var btnPayNow: UIButton!
    @IBOutlet weak var stackViewOfActions: UIStackView!
    @IBOutlet weak var btnViewBill: UIButton!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    var upcomingBooking: UpcomingBookingModel!
    var delegate: UpcomingReserevationDelegate!
    //@IBOutlet weak var reservationBackImageView:UIImageView!

    override func awakeFromNib() {
        containerView.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
        btnPayNow.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0.0)
        btnViewBill.roundCorners(cornerRadius: 20.0, borderColor: .clear, borderWidth: 0.0)
        //btnPayNow.adjustImageAndTitleOffsets()
        btnViewBill.adjustImageAndTitleOffsets()
        self.setShadow()
       // self.reservationBackImageView.frame.size.width = self.frame.size.width
    }
    
    func setUpcomingReservationCellvalues(_ upcomingObj:UpcomingBookingModel, delegate: UpcomingReserevationDelegate){
        self.upcomingBooking = upcomingObj
        self.delegate = delegate
        upcomingReservationLabel.text = "#" + upcomingObj.bookingId!
        if let status = upcomingObj.bookingStatus, status != "" {
            self.reservationDate.text = dateTimeConverter(dateAndTime: upcomingObj.reservationDate!).date
            self.lblTime.text = dateTimeConverter(dateAndTime: upcomingObj.reservationDate!).time
        } else{
            self.reservationDate.text = upcomingObj.reservationDate!
        }
        self.reservationLocation.text = "@ " + upcomingObj.branchName!
        btnPayNow.isHidden = false
        if upcomingObj.bookingStatus == "PRE_RECEIPT"{
            //stackViewOfActions.isHidden = true
            btnViewBill.isHidden = true
        }else{
            //stackViewOfActions.isHidden = true
            btnViewBill.isHidden = true
        }
        self.lblAmount.text = String(format: "%@%.0f", getCurrency(), upcomingObj.billTotal ?? 0)
        let status = getStatus()
        lblStatus.backgroundColor = status.1.withAlphaComponent(0.1)
        lblStatus.textColor = status.1
        lblStatus.text = status.0
    }
    
    func getTheAttributedString(_ inputString:String,upadteString:String)->NSMutableAttributedString{
        let longString = inputString
        let longestWord = upadteString
        let longestWordRange = (longString as NSString).range(of: longestWord)
        let attributedString1 = NSMutableAttributedString(string:longString)
        attributedString1.addAttribute(NSAttributedString.Key.font, value: UIFont.appThemeLightWith(size: 14.0) as Any, range: longestWordRange)
        return attributedString1
    }
    
    func dateTimeConverter(dateAndTime: String)-> (date: String, time: String){
        let dateAndTimeArray = dateAndTime.components(separatedBy: " ")
        let dateString = dateAndTimeArray[0]
        let timeString = dateAndTimeArray[1].components(separatedBy: ".")
        let originalTimeString = timeString[0]
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: dateString)
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "dd MMM, EEEE"
        let convertedDate = dateformatter.string(from: date!)
        //converting time to 12 hr format

        let time = BBQBookingUtilies.shared.getDisplayTimeIn12HourFormat(originalTimeString)
        return (convertedDate, time.uppercased())
    }
    
    private func getStatus() -> (String, UIColor) {
        if upcomingBooking.bookingStatus == "CONFIRMED" || upcomingBooking.bookingStatus == "EXPECTED" || upcomingBooking.bookingStatus == "Rescheduled" || upcomingBooking.bookingStatus == "UPDATE RESERVATION"{
            return ("UPCOMING", UIColor(red: 255.0/255.0, green: 136.0/255.0, blue: 0.0/255.0, alpha: 1.0))
        }else if upcomingBooking.bookingStatus == "ARRIVED" || upcomingBooking.bookingStatus == "SEATED"{
            return ("ON GOING", UIColor(red: 255.0/255.0, green: 136.0/255.0, blue: 0.0/255.0, alpha: 1.0))
        }else if upcomingBooking.bookingStatus == "PRE_RECEIPT"{
            return ("PENDING", UIColor.red)
        }else if upcomingBooking.bookingStatus == "SETTLED"{
            return ("COMPLETED", UIColor(red: 5.0/255.0, green: 166.0/255.0, blue: 96.0/255.0, alpha: 1.0))
        }else if upcomingBooking.bookingStatus == "CANCELLED" || upcomingBooking.bookingStatus == "NO SHOW"{
            return ("CANCELLED", UIColor(red: 60.0/255.0, green: 72.0/255.0, blue: 88.0/255.0, alpha: 1.0))
        }else{
            return (upcomingBooking.bookingStatus ?? "", UIColor(red: 255.0/255.0, green: 136.0/255.0, blue: 0.0/255.0, alpha: 1.0))
        }
    }
    
    
   
    @IBAction func onClickBtnPayNow(_ sender: Any) {
        delegate.onClickBtnViewDetails(cell: self, upcomingBooking: upcomingBooking)
    }
    @IBAction func onClickBtnViewBill(_ sender: Any) {
        delegate.onClickBtnViewBill(cell: self, upcomingBooking: upcomingBooking)
    }
    
}

protocol UpcomingReserevationDelegate {
    func onClickBtnCab(cell: BBQUpcomingReservationCell, upcomingBooking: UpcomingBookingModel)
    func onClickBtnNavigation(cell: BBQUpcomingReservationCell, upcomingBooking: UpcomingBookingModel)
    func onClickBtnCall(cell: BBQUpcomingReservationCell, upcomingBooking: UpcomingBookingModel)
    func onClickBtnRestaurantInfo(cell: BBQUpcomingReservationCell, upcomingBooking: UpcomingBookingModel)
    func onClickBtnPayNow(cell: BBQUpcomingReservationCell, upcomingBooking: UpcomingBookingModel)
    func onClickBtnViewDetails(cell: BBQUpcomingReservationCell, upcomingBooking: UpcomingBookingModel)
    func onClickBtnViewBill(cell: BBQUpcomingReservationCell, upcomingBooking: UpcomingBookingModel)
}
