//
 //  Created by Arpana on 28/09/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified BBQTermAndConditionsViewController.swift
 //

import UIKit

class BBQTermAndConditionsViewController: UIViewController {

    @IBOutlet weak var txtView: UITextView!
    var terms = "NA"
    var termsInWebFormat = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
    
    }

    func setData(){
        
       txtView.text = terms
    }
    
    
    func setWebData(){
        
        guard let data = termsInWebFormat.data(using: String.Encoding.unicode) else { return }
        do {
            txtView.attributedText = try NSAttributedString(data: data,
                                                                     options: [.documentType:NSAttributedString.DocumentType.html],
                                                                     documentAttributes: nil)
        } catch (let errorStr) {
            print(errorStr.localizedDescription)
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if terms == "NA"{
            setWebData()
        }else {
            setData()
        }
    }

    

    @IBAction func crossBtnClicked(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
 

}
