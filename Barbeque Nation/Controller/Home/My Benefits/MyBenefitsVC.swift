//
 //  Created by Sakir Sherasiya on 22/09/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified MyBenefitsVC.swift
 //

import UIKit
import ShimmerSwift

class MyBenefitsVC: BaseViewController, UITextViewDelegate {
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mainview: UIView!
    @IBOutlet weak var heightForTextView: NSLayoutConstraint!
    var bottomSheetController: BottomSheetController?

    @IBOutlet weak var smileCardView: UIStackView!
    
    //MARK:- Class properties
    lazy var loyalityViewModel : BBQLoyalityPointsViewModel = {
        let viewModel = BBQLoyalityPointsViewModel()
        return viewModel
    }()
    
    
    lazy var vouchorHistoryViewModel : BBQVoucherHistoryViewModel = {
      
        let viewModel = BBQVoucherHistoryViewModel()
        return viewModel
    }()
    
    lazy var voucherCouponListWithCountViewModel : VoucherAndCouponListWithCountViewModel = {
        let viewModel = VoucherAndCouponListWithCountViewModel()
        return viewModel
    }()

    var totalItems = 0
    var pageNumberValue = 0
    var pageSizeValue = 20
    var sortValue = Constants.App.LoyalitiPage.sortValue
    var loyaltyTransactionsArray = [LoyaltyTransactions]()
    var loyalityModel : BBQLoyalityPointsDataModel? = nil
    var voucherCouponModelModel: VoucherAndCouponListWithCountModel? = nil
    var vouchers = [Coupon]()
    var selectedTab: Int = -1
    var isVocherDataFetched = false

    @IBOutlet weak var shimmerSupportView: UIView!
    @IBOutlet weak var shimmerView: UIView!
    
    var isPullToRefreshClicked = false
    @IBOutlet weak var lblAvailableSmileCount: UILabel!
    
    @IBOutlet weak var lblReddemedSmileCount: UILabel!
    @IBOutlet weak var lblAvailableSmile: UILabel!
    @IBOutlet weak var lblExpiryInfo: UILabel!
    @IBOutlet weak var textViewMessage: UITextView!
    
    var tablePageLimit = 0
    var voucherHistoryPageLimit = 0
    var vocherHistoryTablePageNo = 0
    var vouchersData : [VouchersLists] = []
    var isSmileRefreshing = false

    
    @IBOutlet weak var lblExpiredSmileCount: UILabel!
    
    var isShimmerRunning : Bool = false
    var shimmerControl : ShimmeringView?

//    lazy var refreshControl: UIRefreshControl = {
//        let refreshControl = UIRefreshControl()
//        refreshControl.addTarget(self, action: #selector(MyBenefitsVC.handleRefresh(_:)), for: UIControl.Event.valueChanged)
//        return refreshControl
//    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        AnalyticsHelper.shared.triggerEvent(type: .MY01)
        //  collectionView.tag = Constants.TagValues.voucherCellTag
        
        
        mainview.layer.masksToBounds = false
        mainview.layer.shadowColor = UIColor.lightGray.cgColor
        mainview.layer.shadowOffset = CGSize(width: 0, height: 0)
        mainview.layer.shadowRadius = 2
        mainview.layer.shadowOpacity = 0.5
        
        segmentControl.clipsToBounds = true
        segmentControl.layer.cornerRadius = 5
        
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.appThemeMediumWith(size: 10)], for: .normal)
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.lightGray], for: .disabled)
        
        segmentControl.backgroundColor = .lightThemeBackground.withAlphaComponent(0.15)
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.text], for: .normal)
        if #available(iOS 13.0, *) {
            segmentControl.selectedSegmentTintColor = .theme
        } else {
            segmentControl.tintColor = .theme
        }
        
        let mutableString = NSMutableAttributedString(attributedString: textViewMessage.attributedText)
        _ = mutableString.setAsLink(textToFind: "More details…", linkURL: "https://www.barbequenation.com/smiles")
        textViewMessage.attributedText = mutableString
        
//        textViewMessage.delegate = self
        textViewMessage.dataDetectorTypes = .link
        self.textViewMessage.linkTextAttributes = [
            .foregroundColor: UIColor.blue,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        
        heightForTextView.constant = textViewMessage.contentSize.height
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(MyBenefitsVC.tapFunction(_ :)))
        
        // Gesture recognizer TextView
        textViewMessage.addGestureRecognizer(tap)
        
        loadTransactions()
    }
    
    @objc func tapFunction(_ gesture: UITapGestureRecognizer) {
        
        
        let controller = UIStoryboard.loadHelpSupportViewController()
        controller.accessibilityLabel = "MyBenifits"
        self.navigationController?.pushViewController(controller, animated: false)
        
}

      func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if selectedTab == -1 {
            selectedTab = 0
        }
        
        segmentControl.selectedSegmentIndex = selectedTab
        
        smileCardView.isHidden = selectedTab == 0 ?false :true
        
        setDataAsPerSelectedTab()
        
        
//        isPullToRefreshClicked = false
//        voucherHistoryPageLimit = 0
//        self.vouchersData = []
//        //  self.responseStatusLabel.text = "You don’t have any Happiness Card"
//        self.showShimmering()
//        fetchVoucherHistoryData(pageNo: String(vocherHistoryTablePageNo))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        heightForTextView.constant = textViewMessage.contentSize.height
    }
    
    //MARK:- Web Service
    func fetchCouponAndVoucherCount() {
        if BBQUserDefaults.sharedInstance.accessToken != "" {
            self.voucherCouponListWithCountViewModel.getCouponAndVoucherListWithCount()  { ( isSuccess ) in
                if isSuccess {
                    //Received all values in voucherCouponListWithCountViewModel , now display on UI and retain to send to myBenifit controller
                    self.voucherCouponModelModel = self.voucherCouponListWithCountViewModel.getVouchersAndCouponsModel
                }
                self.tableView.reloadData()
            }
        }
    }
    
    func loadTransactions() {
        BBQActivityIndicator.showIndicator(view: self.view)
        self.loyalityViewModel.getLoyalityPoints(pageNumber: pageNumberValue, pageSize: pageSizeValue, sort: sortValue) { (isSuccess) in
            if isSuccess {
                BBQActivityIndicator.hideIndicator(from: self.view)
                if self.pageNumberValue == 0{
                    self.loyaltyTransactionsArray.removeAll()
                }
                self.totalItems = self.loyalityViewModel.getLoyalityModel?.totalElements ?? 0
                self.loyalityModel = self.loyalityViewModel.getLoyalityModel
                if let loyaltyTransactions = self.loyalityModel?.loyaltyTransactions {
                    self.loyaltyTransactionsArray.append(contentsOf: loyaltyTransactions)
                }

                
                if let availablePoints = self.loyalityModel?.availablePoints{
                    self.lblAvailableSmile.text = String(format: "%d", availablePoints)
                  //  self.lblNumberOfSmilesToShare.text =  String(format: "%d Smiles", availablePoints)
                }
                    
                  
                if let pointsSpent = self.loyalityModel?.pointsSpent{
                    self.lblReddemedSmileCount.text = String(pointsSpent)
                }
                    
                if let totalearnedPoints = self.loyalityModel?.pointsEarned{
                    self.lblAvailableSmileCount.text = String(totalearnedPoints)
                }
                    
                let expiredSmiles  = (self.loyalityModel?.pointsEarned ?? 0) - (self.loyalityModel?.pointsSpent ?? 0) - (self.loyalityModel?.availablePoints ?? 0)
                    
                self.lblExpiredSmileCount.text = String(expiredSmiles)
                    
                
                self.tableView.isHidden = false
                self.tableView.reloadData()
            }else{
                BBQActivityIndicator.hideIndicator(from: self.view)
            }
            self.isSmileRefreshing = false
        }
    }

    
    
//    func fetchVoucherHistoryData(pageNo: String){
//        if isPullToRefreshClicked == false{
//            if !self.isShimmerRunning {
//                BBQActivityIndicator.showIndicator(view: self.view)
//            }
//            self.isPullToRefreshClicked = true
//        }
//        vouchorHistoryViewModel.getVoucherHistoryDetails(pageNo: pageNo) { (result) in
//            if result == true{
//               // self.refreshControl.endRefreshing()
//                self.stopShimmering()
//                BBQActivityIndicator.hideIndicator(from: self.view)
//                self.isVocherDataFetched = false
//                if let data = self.vouchorHistoryViewModel.getVoucherHistoryDetailsData{
//                    if data.count != 0{
////                        self.responseStatusLabel.isHidden = true
////                      //  self.happinessCardTitleLabelHeader.isHidden = false
////                        self.stackViewForEmpty.isHidden = true
//
//
//                        // TODO: Temporary fix to avoid duplicates in first page in voucher history.
//                        if self.vocherHistoryTablePageNo > 0 {
//                           // self.vouchersData.append(contentsOf: data)
//                            self.voucherHistoryPageLimit = self.voucherHistoryPageLimit + data.count
//                        } else {
//                            self.vouchersData = data
//                            self.voucherHistoryPageLimit = data.count
//                        }
//
//                    }else{
//                       // self.myOrderTableView.isHidden = true
//                      //  self.responseStatusLabel.isHidden = true
//                     //   self.stackViewForEmpty.isHidden = false
//                     //   self.happinessCardTitleLabelHeader.isHidden = true
//                    }
//                }
//                self.tableView.reloadData()
//            }
//            else{
//                BBQActivityIndicator.hideIndicator(from: self.view)
//                self.isVocherDataFetched = false
//                self.tableView.isHidden = true
//               // self.responseStatusLabel.isHidden = false
//
//            }
//        }
//    }
    
    
//    func loadVoucherData() {
//
//        isPullToRefreshClicked = false
//        voucherHistoryPageLimit = 0
//        self.vouchersData = []
//     //   self.responseStatusLabel.text = "You don’t have any Happiness Card"
//      //  self.showShimmering()
//        fetchVoucherHistoryData(pageNo: String(vocherHistoryTablePageNo))
//    }
    
    func setDataAsPerSelectedTab() {
        
        switch selectedTab {
            
        case 0:
            
            if let availablePoints = self.loyalityModel?.availablePoints{
                self.lblAvailableSmile.text = String(format: "%d", availablePoints)
              //  self.lblNumberOfSmilesToShare.text =  String(format: "%d Smiles", availablePoints)
            }
            
            if self.loyalityModel != nil {
                
              
                if let pointsSpent = self.loyalityModel?.pointsSpent{
                    self.lblReddemedSmileCount.text = String(pointsSpent)
                }
                
                if let totalearnedPoints = self.loyalityModel?.pointsEarned{
                    self.lblAvailableSmileCount.text = String(totalearnedPoints)
                }
                
                let expiredSmiles  = (self.loyalityModel?.pointsEarned ?? 0) - (self.loyalityModel?.pointsSpent ?? 0) - (self.loyalityModel?.availablePoints ?? 0)
                
                self.lblExpiredSmileCount.text = String(expiredSmiles)
                
                
             //   self.lblExpiryInfo.text = String(format: "%d Smiles expiring on %@.", self.loyalityModel?)
            }
            
            self.tableView.reloadData()
            break
        
        case 1:
            
            self.tableView.reloadData()

            break
            
        case 2:
            
//            if vouchers.count == 0 {
//                self.fetchVoucherHistoryData(pageNo:String(vocherHistoryTablePageNo))
//            }
            self.tableView.reloadData()

            break
            
        default :
            if let availablePoints = self.loyalityModel?.availablePoints{
                self.lblAvailableSmile.text = String(format: "%d", availablePoints)
              //  self.lblNumberOfSmilesToShare.text =  String(format: "%d Smiles", availablePoints)
            }
          //  self.tblTransactionHistory.isHidden = false
            
            if self.loyalityModel != nil {
                
              
                if let pointsSpent = self.loyalityModel?.pointsSpent{
                    self.lblReddemedSmileCount.text = String(pointsSpent)
                }
                
                if let totalearnedPoints = self.loyalityModel?.pointsEarned{
                    self.lblAvailableSmileCount.text = String(totalearnedPoints)
                }
                
                let expiredSmiles  = (self.loyalityModel?.pointsEarned ?? 0) - (self.loyalityModel?.pointsSpent ?? 0) - (self.loyalityModel?.availablePoints ?? 0)
                
                self.lblExpiredSmileCount.text = String(expiredSmiles)
                
                
            }
            
        }
    }

    @IBAction func onClickBtnBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func segmentControlAction(_ sender: Any) {
        selectedTab = segmentControl.selectedSegmentIndex
        //self.segmentControl.reload()
        tableView.tag = selectedTab
        smileCardView.isHidden = selectedTab == 0 ? false :true
        setDataAsPerSelectedTab()
        if selectedTab == 0{
            AnalyticsHelper.shared.triggerEvent(type: .MY02)
        }else if selectedTab == 1{
            AnalyticsHelper.shared.triggerEvent(type: .MY03)
        }else if selectedTab == 2{
            AnalyticsHelper.shared.triggerEvent(type: .MY04)
        }
    }


}

extension MyBenefitsVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch selectedTab {
            
        case 2:

            if (self.voucherCouponModelModel?.VCData?.vouchersLists?.count ?? 0) > indexPath.row, let tempVoucher = self.voucherCouponModelModel?.VCData?.vouchersLists?[indexPath.row]
            {
                AnalyticsHelper.shared.triggerEvent(type: .MY04C)
                let detailsViewController = UIStoryboard.loadHappinesscardDetailViewController()
                detailsViewController.voucher = Vouchers(voucher: tempVoucher)
                detailsViewController.delegate = self
                self.navigationController?.pushViewController(detailsViewController, animated: true)
            }
            
        default: break
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch selectedTab {
            
        case 2:
            
            guard let cell  = tableView.dequeueReusableCell(withIdentifier: "MyVouchersCell", for: indexPath)  as? MyVouchersCell else { return UITableViewCell() }
            cell.delegate = self
            cell.termDelegate = self
            //cell.setCellData(voucher: vouchersData[indexPath.row], indexPath: indexPath)
            
            guard let model = voucherCouponModelModel?.VCData?.vouchersLists?[indexPath.row] as? VouchersLists else {
                return cell
            }
            cell.setCellData(voucher: model, indexPath:indexPath)
            
          //  cell.delegate = self
            return cell
            
            
        case 1:
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "MyCouponVoucherCell", for: indexPath) as? MyCouponVoucherCell else { return UITableViewCell() }
            
            guard let model = voucherCouponModelModel?.VCData?.couponsLists?[indexPath.row] as? CouponsLists else {
                return cell
            }
            cell.setCellData(coupon: model, indexPath: indexPath, delegate: self)
            return cell
            
            
        case 0:
            
            guard let cell  = tableView.dequeueReusableCell(withIdentifier: "MySmileCell", for: indexPath)  as? MySmileCell else { return UITableViewCell() }
            cell.setCellData(loyaltyTransactions: loyaltyTransactionsArray[indexPath.row])
            return cell
            
            
        default:
            
            return tableView.defaultCell(indexPath: indexPath)
            
            
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch selectedTab {
            
        case 0:
            
            return loyaltyTransactionsArray.count
            
        case 1:
            
            if voucherCouponModelModel?.VCData?.couponsLists?.count ?? 0 == 0 {
                   self.tableView.setEmptyMessage("No coupons available")
               } else {
                   self.tableView.restore()
               }
            
            return voucherCouponModelModel?.VCData?.couponsLists?.count ?? 0
            
        case 2:
            if voucherCouponModelModel?.VCData?.vouchersLists?.count ?? 0 == 0 {
                   self.tableView.setEmptyMessage("No vouchers available")
               } else {
                   self.tableView.restore()
               }
            return voucherCouponModelModel?.VCData?.vouchersLists?.count ?? 0

        default :
            return 12
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        switch selectedTab {
        case 0:
            if scrollView.contentSize.height > view.frame.size.height{
                if scrollView.contentOffset.y <= 20{
                    UIView.animate(withDuration: 0.2) {
                        self.smileCardView.isHidden = false
                    } completion: { isCompleted in
                        self.smileCardView.isHidden = false
                    }
                }else if scrollView.contentOffset.y >= 60{
                    UIView.animate(withDuration: 0.4) {
                        self.smileCardView.isHidden = true
                    } completion: { isCompleted in
                        self.smileCardView.isHidden = true
                    }
                }
            }
        default: break
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch selectedTab {
        case 0:
            isPullToRefreshClicked = false
            
            if indexPath.row < totalItems - 1 {
                if indexPath.row == loyaltyTransactionsArray.count - 1 && !isSmileRefreshing{
                    isSmileRefreshing = true
                    pageNumberValue = pageNumberValue + 1
                    self.loadTransactions()
                }
            }
        default: break
            
        }
    }
    
}


extension MyBenefitsVC: BBQMyHappinesscardCellDelegate ,  BBQVoucherGiftingViewControllerProtocol , BBQMyVoucherCellTermDelegate, BBQMyCouponVoucherCellTermDelegate, BBQHappinesscardDetailsVCDelegate{
    
    // MARK: BBQVoucherGiftingViewControllerProtocol Methods
    
    func didFinishedVoucherGifting() {
        fetchCouponAndVoucherCount()
    }
    func onClickGiftVoucher(indexPath: IndexPath) {
       // if let barCode = voucherCouponModelModel?.VCData?.vouchersLists?[indexPath.row].barcode {
        if (self.voucherCouponModelModel?.VCData?.vouchersLists?.count ?? 0) > indexPath.row, let barCode = self.voucherCouponModelModel?.VCData?.vouchersLists?[indexPath.row].barCode {
            let giftingVC = UIStoryboard.loadGiftVoucherScreen()
            giftingVC.view.layoutIfNeeded()
            giftingVC.giftingDelegate = self
            giftingVC.voucherBarCode = barCode
            giftingVC.modalPresentationStyle = .overFullScreen
            self.present(giftingVC, animated: true, completion: nil)
        }
    }
    
    func onClickTermsAnDCondition(indexPath: IndexPath) {
        

        if let  termAndConditions =  voucherCouponModelModel?.terms_conditions?.joined(separator: "\n\n-") {
            let newTerms = "-" + termAndConditions
            let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: 100)
            bottomSheetController = BottomSheetController(configuration: configuration)
            
            let BBQTermAndConditionsViewController = BBQTermAndConditionsViewController(nibName: "BBQTermAndConditionsViewController", bundle: nil)

            BBQTermAndConditionsViewController.terms = newTerms
            
            bottomSheetController?.present(BBQTermAndConditionsViewController, on: self)
            
        }
    }
    
    func onClickTermsCondition(indexPath: IndexPath) {
        
        if indexPath.row < (voucherCouponModelModel?.VCData?.couponsLists?.count ?? 0) {
            guard let model = voucherCouponModelModel?.VCData?.couponsLists?[indexPath.row] as? CouponsLists else {return}
            if let  termAndConditions =  model.terms_condition {
                
                let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: 100)
                bottomSheetController = BottomSheetController(configuration: configuration)
                
                let BBQTermAndConditionsViewController = BBQTermAndConditionsViewController(nibName: "BBQTermAndConditionsViewController", bundle: nil)
                let newTerms = "-" + termAndConditions
                let terms = newTerms.components(separatedBy: ".")
                BBQTermAndConditionsViewController.terms = terms.joined(separator: "\n\n-")
                
                bottomSheetController?.present(BBQTermAndConditionsViewController, on: self)
                
            }
        }
    }
}




extension MyBenefitsVC{
    // MARK: Shimmer Methods
    
    private func showShimmering() {
        self.shimmerSupportView.isHidden = false
        
        if let shimmer = self.shimmerControl {
            self.shimmerSupportView.addSubview(shimmer)
            shimmer.contentView = shimmerView
            shimmer.shimmerAnimationOpacity = 0.2
            shimmer.shimmerSpeed = 500.00
            shimmer.isHidden = false
            shimmer.isShimmering = true
            self.isShimmerRunning = true
        }
    }
    
    private func stopShimmering() {
        self.shimmerSupportView.isHidden = true
        
        if let shimmer = self.shimmerControl {
            shimmer.isShimmering = false
            shimmer.isHidden = true
            self.isShimmerRunning = false
        }
    }
}
