//
 //  Created by Sakir Sherasiya on 22/09/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified MyCouponVoucherCell.swift
 //

import UIKit

class MyCouponVoucherCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblExpiryDate: UILabel!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var lblTerms: UILabel!
    @IBOutlet weak var lblDeliverDineIn: UILabel!
    var delegate: BBQMyCouponVoucherCellTermDelegate?
    var indexPath: IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setTheme()
    }
    
    func setThemeUI() {
        viewForContainer.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0.0)
        viewForContainer.dropShadow()
        let tap = UITapGestureRecognizer(target: self, action: #selector(MyCouponVoucherCell.tapFunction(_ :)))
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
        //contentView.setShadow()UIColor.hexStringToUIColor(hex: "18191F")
        mainView.setCornerRadius(12)
        mainView.setShadow(color: UIColor.hexStringToUIColor(hex: "55606D1A") , opacity: 0.2, offSet: .zero, radius: 7, scale: true)
        
    }
    
    private func setTheme(){
        viewForContainer.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
        // viewForData.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
//        viewForData.layer.maskedCorners = [ .layerMinXMaxYCorner, .layerMinXMaxYCorner]
//        viewForData.clipsToBounds = true
//        viewForData.layer.cornerRadius = 10
        
        // viewForStatus.roundCorners(cornerRadius: 4.0, borderColor: .clear, borderWidth: 0.0)
        
    }
  
    
    @IBAction func onClickBtnCopy(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .MY03A)
        UIPasteboard.general.string = lblCode.text 
        UIUtils.showToast(message: "COUPON code: " + (lblCode.text ?? "") + " is copied")
    }
    
    @objc func tapFunction(_ gesture: UITapGestureRecognizer) {
        AnalyticsHelper.shared.triggerEvent(type: .MY03B)
        if let indexPath = indexPath{
            delegate?.onClickTermsCondition(indexPath: indexPath)
        }
    }
    @IBAction func onClickBtnTC(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .MY03B)
        if let indexPath = indexPath{
            delegate?.onClickTermsCondition(indexPath: indexPath)
        }
    }
    
  
    
    func setCellData(coupon: CouponsLists, indexPath: IndexPath, delegate: BBQMyCouponVoucherCellTermDelegate) {
        self.delegate = delegate
        self.indexPath = indexPath
        lblTitle.text = coupon.title
        lblCode.text = coupon.barcode
        lblDesc.text = coupon.description
        lblTerms.text = coupon.terms_condition
        //lblExpiryDate.text = coupon.expiring_date
        var displayDate: String = ""

        if let date = coupon.expiring_date{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"
            if let date = dateFormatter.date(from: date) {
                let dateformatter = DateFormatter()
                dateformatter.dateFormat = "dd-MMM-yyyy"
                let convertedDate = dateformatter.string(from: date)
                displayDate = convertedDate
            }
           // lblExpiryDate.text = displayDate

        }
            
        
        
    }
}

protocol BBQMyCouponVoucherCellTermDelegate{
    func onClickTermsCondition(indexPath : IndexPath)
}
