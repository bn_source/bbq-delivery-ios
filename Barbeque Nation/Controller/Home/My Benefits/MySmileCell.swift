//
 //  Created by Arpana on 28/09/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified MySmileCell.swift
 //

import UIKit

class MySmileCell: UITableViewCell {
        
        //MARK:- View Properties
        @IBOutlet weak var viewForContainer: UIView!
        @IBOutlet weak var lblSmileHistory: UILabel!
        @IBOutlet weak var lblSmileUsed: UILabel!
        @IBOutlet weak var lblSmilePoints: UILabel!
        @IBOutlet weak var lblDateAndTime : UILabel!
        @IBOutlet weak var lblTime : UILabel!
        @IBOutlet weak var imgViewIcon: UIImageView!
        //MARK:- Class Properties
        var loyaltyTransactions: LoyaltyTransactions!
        
        @IBOutlet weak var lblSmileCount: UILabel!
        //MARK:- View Methods
        override func awakeFromNib() {
            super.awakeFromNib()
            setThemeUI()
        }

        func setThemeUI() {
            viewForContainer.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0.0)
            viewForContainer.dropShadow()
        }
        func setTheme() {
            lblSmileHistory.setTheme(size: 16.0, weight: .regular, color: .theme)
            lblSmileUsed.setTheme(size: 12.0, weight: .regular, color: .text)
            lblSmilePoints.setTheme(size: 14, weight: .bold, color: .darkGreen)
        }
        
        func setCellData(loyaltyTransactions: LoyaltyTransactions) {
            //setTheme()
            lblSmilePoints.setCircleView()
            self.loyaltyTransactions = loyaltyTransactions
            lblSmileHistory.text = ""
            if let points = loyaltyTransactions.points{
                if loyaltyTransactions.transaction_type == Constants.App.LoyalitiPage.credit{
                    lblSmileCount.text =  String(format: "+%li", points)
                    lblSmileHistory.text = String(format: "Smiles Coins Earned")
                    imgViewIcon.image = UIImage(named: "SmileEarned")
                   // lblSmilePoints.text = String(format: "+%li", points)
                }else{
                    lblSmileCount.text =  String(format: "-%li", points)
                    lblSmileHistory.text = String(format: "Smiles Coins Redeemed")
                    imgViewIcon.image = UIImage(named: "SmileShared")
                   // lblSmileHistory.text = String(format: "-%li Smiles Coins Redeemed", points)
                  //  lblSmilePoints.text = String(format: "-%li", points)
                }
                var strText = ""
                var strTime = ""
                if let interval = loyaltyTransactions.created_on{
                    let date = Date(timeIntervalSince1970: interval)
                    strText = String(format: "%@", date.string(format: "dd MMM yyyy, E"))
                    strTime = String(format: "%@", date.string(format: "hh:mm a"))

                }
                lblDateAndTime.text = strText
                lblTime.text = strTime
                if let eventType = loyaltyTransactions.eventType, eventType.contains("DELIVERY_REDEMPTION"){
                    lblSmileUsed.text = "Delivery/TakeAway Order"
                }else if let eventType = loyaltyTransactions.eventType, eventType.contains("DELIVERY_COMPLETED"){
                    lblSmileUsed.text = "Delivery/TakeAway Order" //"Delivery/Takeaway order dated"
                } else{
                    lblSmileUsed.text = loyaltyTransactions.description?.components(separatedBy: " on ").first ?? ""
                }
                if loyaltyTransactions.transaction_type == Constants.App.LoyalitiPage.credit, let expireydate = loyaltyTransactions.expiryDate{
                    let expiredDate = Date(timeIntervalSince1970: TimeInterval(expireydate))
                    if expiredDate > Date(){
                        lblSmileUsed.text = (lblSmileUsed.text ?? "") + "\nSmiles Coins will expire on " + expiredDate.string(format: "dd/MM/yyyy")
                    }
                }
                
            }
            
        }
        

    }
