//
 //  Created by Arpana on 27/09/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified MyVouchersCell.swift
 //

import UIKit

protocol BBQMyVoucherCellTermDelegate{
    func onClickTermsAnDCondition(indexPath : IndexPath)
}

class MyVouchersCell: UITableViewCell {

    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var deactivatedViewForContainer: UIView!
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var viewForData: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var viewForStatus: UIView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var btnGift: UIButton!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var  expireStackView : UIStackView!
    @IBOutlet weak var btnTermAndCondition: UIButton!

    var voucher: VouchersLists?
    
    var indexPath: IndexPath!
    var delegate: BBQMyHappinesscardCellDelegate?
    var termDelegate : BBQMyVoucherCellTermDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setTheme()
    }
    
    private func setTheme(){
        viewForContainer.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
        deactivatedViewForContainer.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
        viewForStatus.roundCorners(cornerRadius: 4, borderColor: .clear, borderWidth: 0.0)
        btnGift.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0.0)
        btnTermAndCondition.roundCorners(cornerRadius: 4.0, borderColor: .clear, borderWidth: 0.0)
        
        //only left corners to data view
        viewForData.clipsToBounds = true
        viewForData.layer.cornerRadius = 10
        viewForData.layer.maskedCorners = [ .layerMinXMinYCorner, .layerMinXMaxYCorner]
        
        
        //only right corners to imageview
        imgBackground.clipsToBounds = true
        imgBackground.layer.cornerRadius = 10
        imgBackground.layer.maskedCorners = [ .layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        
        self.setShadow()
        
        
    }
    
    
    func setCellData(voucher: VouchersLists , indexPath: IndexPath) {
        viewForStatus.isHidden = false
        expireStackView.isHidden = true
        self.voucher = voucher
        self.indexPath = indexPath
        lblName.text = voucher.title
        lblAmount.text = getCurrency() + String(voucher.denomination ?? 0)

        let voucherStatus = getStatus()
        viewForStatus.backgroundColor = voucherStatus.1.withAlphaComponent(0.15)
        lblStatus.textColor = voucherStatus.1
        lblStatus.text = voucherStatus.0
        btnGift.isHidden = !voucherStatus.3
        
        if voucher.status == "Active" {

            //show expiery date time at different place
            //hide status view from its postition
            expireStackView.isHidden = false
            viewForStatus.isHidden = true
            lblDateTime.text = (voucher.validity ?? "")
        }
        if !voucherStatus.2{
                  self.deactivatedViewForContainer.isHidden = false
              }else if voucher.giftTo != nil, voucher.giftTo != ""{
                  self.deactivatedViewForContainer.isHidden = true
      
              }
            
        

        
        if voucher.giftTo != nil, voucher.giftTo != ""{
            lblStatus.text = "Gifted to \(voucher.giftTo ?? "")"
            btnGift.isHidden = true
            viewForStatus.isHidden = false
            lblStatus.textColor = .white
        }
        if voucher.giftReceived != nil, voucher.giftReceived != ""{
            lblStatus.text = "Gift received from \(voucher.giftReceived ?? "")"
            lblStatus.textColor = .white
            viewForStatus.backgroundColor = UIColor.darkGreen.withAlphaComponent(0.15)
            btnGift.isHidden = true
            viewForStatus.isHidden = false
        }
    }

    
    
    @IBAction func onClickTermCondition(_ sender: Any) {
        termDelegate?.onClickTermsAnDCondition(indexPath: indexPath)
        AnalyticsHelper.shared.triggerEvent(type: .MY04B)
    }
    @IBAction func onClickBtnGift(_ sender: Any) {
        delegate?.onClickGiftVoucher(indexPath: indexPath)
        AnalyticsHelper.shared.triggerEvent(type: .MY04A)
    }
    private func getStatus() -> (String?, UIColor, Bool, Bool){
        if voucher?.status == "Active"{
            return ("Expires on \(voucher?.validity ?? "")", .themeYello, true, true)
        }else if voucher?.status == "Redeemed" || voucher?.status == "Gifted"{
            return (voucher?.status, UIColor.hexStringToUIColor(hex: "74342D"), false, false)
        }else if voucher?.status == "In-Active" || voucher?.status == "Expired"{
            return (voucher?.status, UIColor.hexStringToUIColor(hex: "74342D"), false, false)
        }
        return ("", .theme, false, false)
    }
    
}
