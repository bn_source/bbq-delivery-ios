//
 //  Created by Ajith CP on 10/03/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified BBQPromoItemsCollectionViewCell.swift
 //

import UIKit

class BBQPromoItemsCollectionViewCell: UICollectionViewCell {
    
    

    @IBOutlet weak var itemTitleLabel: UILabel!
    
    @IBOutlet weak var selectionIndicatorView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            contentView.leftAnchor.constraint(equalTo: leftAnchor),
            contentView.rightAnchor.constraint(equalTo: rightAnchor),
            contentView.topAnchor.constraint(equalTo: topAnchor),
            contentView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
        
    }
    
    // MARK: Public Methods
    
    public func updateButtonCell(for index: Int, selectedIndex: Int) {
        
        self.selectionIndicatorView.isHidden = !(index == selectedIndex)
        
    }
    
}
