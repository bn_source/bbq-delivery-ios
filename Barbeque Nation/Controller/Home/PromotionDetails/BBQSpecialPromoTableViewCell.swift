//
 //  Created by Ajith CP on 10/03/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified BBQSpecialPromoTableViewCell.swift
 //

import UIKit

class BBQSpecialPromoTableViewCell: UITableViewCell {

    @IBOutlet weak var itemDescriptionLabel: UILabel!
    @IBOutlet weak var itemTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    // MARK: Public Methods
    
    public func updatePromotionDetails(with promoData: [String:String], index: Int) {
     
        for title in promoData.keys {
            self.itemTitleLabel.text = title
        }
        
        for value in promoData.values {
            self.itemDescriptionLabel.text = value
        }
        
        
        
    }

}
