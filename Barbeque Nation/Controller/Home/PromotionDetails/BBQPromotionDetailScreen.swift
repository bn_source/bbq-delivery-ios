//
//  Created by Bhamidipati Kishore on 18/10/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQPromotionDetailScreen.swift
//

import UIKit

class MyTapGesture: UITapGestureRecognizer {
    var completeText = String()
    var tapableText = String()
    var urlScheme =  URL(string: "")
    var typeOfLink : NSTextCheckingResult.CheckingType?
    var label: UILabel?
}

protocol BBQPromotionDetailScreenProtocol {
    
    func didPressedReserveTableFromPromotionDetails()
}

class BBQPromotionDetailScreen: UIViewController {
    
    @IBOutlet weak var promotionDetailsTableView:UITableView!
    @IBOutlet weak var promotionImageView:UIImageView!
    @IBOutlet weak var imageTitle:UILabel!
    
    @IBOutlet weak var reserveTableButton: UIButton!
    var isCameFromSpeciallyScreen = false
    
    
    var promotionDeelegate: BBQPromotionDetailScreenProtocol?
    
    var comingFrom:String = ""
    var promotionId:Int = 0

    var sectionNames = [String]()
    var selectedRow = -1
    private var promotionDetailsModel: PromotionDetailsModel? = nil
    lazy var voucherDetailsViewModel: VoucherDetailsViewModel = {
        let viewModel = VoucherDetailsViewModel()
        return viewModel
    }()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        //Register the cells
        let promotionDetailCell = UINib(nibName:Constants.CellIdentifiers.BBQPromotionDetailCell, bundle:nil)
        self.promotionDetailsTableView.register(promotionDetailCell, forCellReuseIdentifier: Constants.CellIdentifiers.BBQPromotionDetailCell)
        
        let descriptionCell = UINib(nibName:Constants.CellIdentifiers.BBQDescriptionCell, bundle:nil)
        self.promotionDetailsTableView.register(descriptionCell, forCellReuseIdentifier: Constants.CellIdentifiers.BBQDescriptionCell)
        
        self.promotionDetailsTableView.tableFooterView = UIView()

        
        getPromosDetailsData(promotionId: promotionId)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        BBQPulseAnimation.showPulseAnimation(inView: self.reserveTableButton,
                                             presentingView: self.view)
    }
    
    func setUpUI(){
        guard let imageUrlString = self.promotionDetailsModel?.promotionImage else {
            return
        }
        LazyImageLoad.setImageOnImageViewFromURL(imageView: promotionImageView, url: imageUrlString) { (image) in
        }
        if let title = self.promotionDetailsModel?.promotionTitle {
            self.imageTitle.text = title
        }
    }
    
    private func getPromosDetailsData(promotionId: Int) {
        BBQActivityIndicator.showIndicator(view: self.view)
        self.voucherDetailsViewModel.getPromotionDetails(promotionId: promotionId) { [weak self] (isSuccess) in
            if let self = self {
                BBQActivityIndicator.hideIndicator(from: self.view)
                if isSuccess {
                    //Populate the UI
                    self.promotionDetailsModel = self.voucherDetailsViewModel.promotionDetailsData
                    self.setSections()
                    self.setUpUI()
                    self.promotionDetailsTableView.reloadData()
                }
            }
        }
    }
    
    func setSections() {
        
        // TODO: Move to view model
        if let policy = self.promotionDetailsModel?.privacyAndPolicy, policy != "" {
           sectionNames.append("Policy")
        }
        var timingString = ""
        if let timings = self.promotionDetailsModel?.promotionTimings, timings.count > 0 {
            for obj in timings {
                if let day = obj.day, let time = obj.timings {
                    timingString = timingString + day + "\t" + time + "\n"
                }
            }
            sectionNames.append("Timings")
        }

        if let validOn = self.promotionDetailsModel?.promotionNotValidOn, validOn != "" {
            sectionNames.append("Not valid on")
        }

        if let validity = self.promotionDetailsModel?.promotionValidity, validity != "" {
            sectionNames.append("Valid till")
        }
        var regionString = ""
        if let promotionRegion = self.promotionDetailsModel?.promotionRegion, promotionRegion.count > 0 {
            for region in promotionRegion {
                regionString = regionString + region + "\n"
            }
            sectionNames.append("Applicable at")
        }
        if let promoTimigs = self.promotionDetailsModel?.promotionRegionTiming, promoTimigs != "" {
            sectionNames.append("Promotion region timing")
        }
        if let thingsToRemember = self.promotionDetailsModel?.thingsToRemember, thingsToRemember != "" {
            sectionNames.append("Things to remember")
        }
    }
    
    @IBAction func reserveTableButtonPressed(_ sender: Any) {
        if isCameFromSpeciallyScreen{
            AnalyticsHelper.shared.triggerEvent(type: .SO03)
        }
        self.dismiss(animated: false) {
            self.promotionDeelegate?.didPressedReserveTableFromPromotionDetails()
        }
    }
    
    
    // MARK:- IBActions
    @IBAction func backButtonPressed(_ sender: Any) {
        if comingFrom == Constants.App.NotificationPage.Notification {
            self.navigationController?.popViewController(animated: true)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
}

extension BBQPromotionDetailScreen: UITableViewDelegate, UITableViewDataSource {
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sectionNames.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row ==  0 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.BBQDescriptionCell) as? BBQDescriptionCell{
                cell.setupReserveButtonFor(promotion: true)
                if let description = self.promotionDetailsModel?.promotionDescription {
                    cell.descriptionDetailLabel.text = description
                    cell.descriptionDetailLabel.isUserInteractionEnabled = false
                    
                    //Find the clickable link
                    let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
                    let matches = detector.matches(in: description, options: [], range: NSRange(location: 0, length: description.utf16.count))

                    //If found clickable link, then set attributed text for the description
                    //to identify link and able to go at particular page
                    for match in matches {
                        guard let range =  Range(match.range, in: description) else {
                            cell.descriptionDetailLabel.text = description
                            continue
                            
                        }
                        let urlFound = description[range]
                        print(urlFound)
                        
                        let attributedString = NSMutableAttributedString(string: description)
                        attributedString.addAttribute(.link, value: urlFound, range: NSRange(range, in: description))
                        
                        cell.descriptionDetailLabel.attributedText = attributedString
                        cell.callSetUpTap()
                        cell.tapgesture.completeText = description
                        cell.tapgesture.tapableText = String(urlFound)
                        cell.descriptionDetailLabel.isUserInteractionEnabled = true
                        cell.tapgesture.numberOfTapsRequired = 1
                        cell.descriptionDetailLabel.addGestureRecognizer( cell.tapgesture)
                    
                    }

                }
             

                return cell
            }
        }else  {
            if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.BBQPromotionDetailCell) as? BBQPromotionDetailCell{
                cell.selectionStyle = .none
                cell.titleLabel.text = self.sectionNames[indexPath.row-1]
                //cell.tag = indexPath.row
                if selectedRow == indexPath.row {
                    cell.arrowImage.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                    switch self.sectionNames[indexPath.row-1] {
                    case "Policy":
                        if let policy = self.promotionDetailsModel?.privacyAndPolicy {
                            cell.detailsLabel.text = policy.htmlToString
                        }
                    case "Timings":
                        var timingString = ""
                        if let timings = self.promotionDetailsModel?.promotionTimings{
                            for obj in timings {
                                if let day = obj.day, let time = obj.timings {
                                    timingString = timingString + day + "\t" + time + "\n"
                                }
                            }
                            cell.detailsLabel.text = timingString
                        }
                    
                    case "Not valid on":
                        if let notValid = self.promotionDetailsModel?.promotionNotValidOn {
                            cell.detailsLabel.text = notValid
                        }

                    case "Valid till":
                        if let valid = self.promotionDetailsModel?.promotionValidity {
                            cell.detailsLabel.text = valid
                        }
                    case "Applicable at":
                        var regionString = ""
                        if let promotionRegion = self.promotionDetailsModel?.promotionRegion {
                            for region in promotionRegion {
                                regionString = regionString + region + "\n"
                            }
                            cell.detailsLabel.text = regionString
                        }
                    case "Promotion region timing":
                        if let promotionRegionTiming = self.promotionDetailsModel?.promotionRegionTiming {
                            cell.detailsLabel.text =  promotionRegionTiming
                        }
                    case "Things to remember":
                        if let thingstoremember = self.promotionDetailsModel?.thingsToRemember {
                            cell.detailsLabel.text = thingstoremember.htmlToString
                        }
                    default:
                        print("")
                    }
                } else {
                    cell.arrowImage.transform = CGAffineTransform(rotationAngle: 0)
                    cell.detailsLabel.text = ""
                }
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let oldIndexPath = IndexPath(row: selectedRow, section: 0)
        if selectedRow != indexPath.row {
            selectedRow = indexPath.row
            if oldIndexPath.row > 0 {
                self.promotionDetailsTableView.reloadRows(at: [oldIndexPath, indexPath], with: UITableView.RowAnimation.none)
            } else {
                self.promotionDetailsTableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.none)
            }
        } else {
            selectedRow = -1
            self.promotionDetailsTableView.reloadRows(at: [oldIndexPath], with: UITableView.RowAnimation.none)
        }
    }
 
}

