//
 //  Created by Bhamidipati Kishore on 05/11/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQDescriptionCell.swift
 //

import UIKit

protocol BBQDescriptionCellProtocol {
    func didPressedReserveTableButton()
}

class BBQDescriptionCell: UITableViewCell {

    @IBOutlet weak var reserveButton: UIButton!

    @IBOutlet weak var descriptionHeadingLabel:UILabel!
    @IBOutlet weak var descriptionDetailLabel:UILabel!
    var headerDelegate : BBQDescriptionCellProtocol?
    var tapgesture = MyTapGesture()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        descriptionHeadingLabel.text = kdescriptionText
                
    }
    
    func setupReserveButtonFor(promotion: Bool, isButtonHidden: Bool = false) {
        // Setup reserve button
        let customFont = UIFont.appThemeBoldWith(size: 16)
        var textToDisplay = kReserveButtonText.uppercased()
        if promotion {
            self.reserveButton.isHidden = true
        } else {
            textToDisplay = kBuyNowButtonText
            let buttonAttributes: [NSAttributedString.Key: Any] = [.font: customFont,
                                                                   .foregroundColor: Constants.App.themeColor.white,
                                                                   .underlineStyle: NSUnderlineStyle.single.rawValue]
            let attributeString = NSMutableAttributedString(string: textToDisplay,
                                                            attributes: buttonAttributes)
            self.reserveButton.setAttributedTitle(attributeString, for: .normal)
            self.reserveButton.contentHorizontalAlignment = .center
            self.reserveButton.backgroundColor = UIColor.hexStringToUIColor(hex: "#F04B24")
            self.reserveButton.layer.cornerRadius = 4.0 //self.reserveButton.frame.height/2
            self.reserveButton.isHidden = isButtonHidden
        }
    }

    func callSetUpTap()  {
      tapgesture = MyTapGesture(target: self, action: #selector(tappedOnLabel(_ :)))

    }
    @IBAction func reserveButtonPressed(_ sender: UIButton) {
        self.headerDelegate?.didPressedReserveTableButton()
    }
    
    //MARK:- tappedOnLabel
    @objc func tappedOnLabel(_ gesture: MyTapGesture) {
        
        let text = gesture.completeText
        let conditionsRange = (text as NSString).range(of: gesture.tapableText)
        
        if gesture.didTapAttributedTextInLabel(label: self.descriptionDetailLabel, inRange: conditionsRange) {
           
            if let url = URL(string: gesture.tapableText),
                UIApplication.shared.canOpenURL(url)
            {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
                        
        }
    }
}
