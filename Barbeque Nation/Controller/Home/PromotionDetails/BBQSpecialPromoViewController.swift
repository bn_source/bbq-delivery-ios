//
 //  Created by Ajith CP on 05/03/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified BBQSpecialPromoViewController.swift
 //

import UIKit

protocol BBQPromoDetailScreenProtocol {
    func didPressedReserveTableFromPromotionDetails()
}

class BBQSpecialPromoViewController: UIViewController {

    // MARK: IBOutlets
    
    @IBOutlet weak var buttonCollectionView: UICollectionView!
    
    @IBOutlet weak var promoTableView: UITableView!
    
    @IBOutlet weak var buttonsView: UIView!
    @IBOutlet weak var promoContentView: UIView!
    @IBOutlet weak var promoDetailsView: UIView!
    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var opacityView: UIView!
    
    @IBOutlet weak var promoBannerImageView: UIImageView!
    
    @IBOutlet weak var promoTitleLabel: UILabel!
    @IBOutlet weak var promoDescriptionLabel: UILabel!
    
    @IBOutlet weak var reserveTableButton: UIButton!
    
    @IBOutlet var promoTitleBottomConstraint: NSLayoutConstraint!
    @IBOutlet var imageBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var detailsViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var mediaViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var promoTitleTopConstraint: NSLayoutConstraint!
    
    var promotionDeelegate: BBQPromoDetailScreenProtocol?


    @IBOutlet weak var collectionLayout: UICollectionViewFlowLayout! {
        didSet {
            collectionLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        }
    }
    
    private var selectedButtonIndex : Int = 0
    private var promoDetilsList : [[String:String]]?
    private var termsDataList   : [[String:String]]?
    
    public var promotionId:Int = 0
    
    // Using same viewmodel for data manipulations
    lazy var voucherDetailsViewModel: VoucherDetailsViewModel = {
        let viewModel = VoucherDetailsViewModel()
        return viewModel
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupPromoUI()
        self.setupGetures()
        
        self.getPromosDetailsData(promotionId: self.promotionId)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.screenName(name: .Special_Offer_Screen)
    }
    
    // MARK: Setup Methods
    
    private func setupPromoUI() {
        
        BBQPulseAnimation.showPulseAnimation(inView: self.reserveTableButton, presentingView: self.promoContentView)
        
        self.detailsViewTopConstraint.constant = self.promoContentView.frame.maxY + 20
        
        self.promoDetailsView.maskByRoundingCorners(cornerRadius: 10.0,
                                                    maskedCorners: CornerMask.topCornersMask)
        
        // Setup overlay when expand
        self.overlayView.layer.opacity = 0.9
        self.overlayView.isHidden = true

    }
    
    private func setupGetures() {
        
        let expandTap = UITapGestureRecognizer(target: self,
                                              action: #selector(self.expandAction))
        expandTap.cancelsTouchesInView = false
        self.promoDetailsView.addGestureRecognizer(expandTap)
        
        let collapseTap = UITapGestureRecognizer(target: self,
                                              action: #selector(self.collapseAction))
        self.promoContentView.addGestureRecognizer(collapseTap)
        collapseTap.cancelsTouchesInView = false
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(self.expandAction))
        swipeUp.direction = UISwipeGestureRecognizer.Direction.up
        self.promoDetailsView.addGestureRecognizer(swipeUp)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.collapseAction))
        swipeDown.direction = UISwipeGestureRecognizer.Direction.down
        self.promoDetailsView.addGestureRecognizer(swipeDown)
    }
    
    // MARK: Data Fetch Methods
    
    private func getPromosDetailsData(promotionId: Int) {
        BBQActivityIndicator.showIndicator(view: self.view)
        self.voucherDetailsViewModel.getPromotionDetails(promotionId: promotionId) { [weak self] (isSuccess) in
            if let self = self {
                BBQActivityIndicator.hideIndicator(from: self.view)
                if isSuccess {
                    self.fillPromoData()
                    self.promoDetilsList = self.voucherDetailsViewModel.getPromoDetailsSections()
                    self.termsDataList   = self.voucherDetailsViewModel.getPrivacyAndPolicyData()
                    self.promoTableView.reloadData()
                }
            }
        }
    }
    
    // MARK: Action Listeners
    
    @objc func expandAction(_ sender: UITapGestureRecognizer? = nil) {
        UIView.animate(withDuration: 0.3) {
            
            self.imageBottomConstraint.isActive = false
            
            self.promoTitleTopConstraint.constant = 44
            self.view.layoutIfNeeded()
            
            self.detailsViewTopConstraint.constant = self.promoDescriptionLabel.frame.minY

            self.overlayView.isHidden = false
            self.view.layoutIfNeeded()
        }
        self.overlayView.blurView(with: 1.0)
    }
    
    
    @objc func collapseAction(_ sender: UITapGestureRecognizer? = nil) {
        UIView.animate(withDuration: 0.3) {
            
            self.imageBottomConstraint.isActive = true
            self.view.layoutIfNeeded()
            
            self.detailsViewTopConstraint.constant = self.promoDescriptionLabel.frame.maxY + 20
            self.overlayView.isHidden = true
            self.view.layoutIfNeeded()
        }

    }
       
    
    @IBAction func reserveTableButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: false) {
            self.promotionDeelegate?.didPressedReserveTableFromPromotionDetails()
        }
    }

    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: Private Methods
    
    private func fillPromoData() {
        let promoDetails = self.voucherDetailsViewModel.promoDetails()
        self.promoTitleLabel.text = promoDetails.title
        self.promoDescriptionLabel.text = promoDetails.description
        
        self.promoBannerImageView.sd_setImage(with:  URL(string: promoDetails.image)) { (image, error, type, url) in
            
            self.view.layoutIfNeeded()
            self.detailsViewTopConstraint.constant = self.promoDescriptionLabel.frame.maxY + 20
            let gradient = CAGradientLayer()
            gradient.frame = self.promoBannerImageView.bounds
            gradient.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
            
            self.promoBannerImageView.layer.insertSublayer(gradient, at: 20)
        }
    
    }

    
}

extension BBQSpecialPromoViewController : UITableViewDelegate, UITableViewDataSource,
UICollectionViewDelegate, UICollectionViewDataSource {
   
    // MARK: CollectionView Methods

    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let itemCell = self.buttonCollectionView.dequeueReusableCell(withReuseIdentifier:"BBQSpecialPromoItemCellID",
                                                                     for: indexPath) as! BBQPromoItemsCollectionViewCell
        itemCell.itemTitleLabel.text = (indexPath.row == 0) ? "Details" : "Terms and Conditions"
        itemCell.updateButtonCell(for: indexPath.item, selectedIndex:  selectedButtonIndex)

        return itemCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedButtonIndex = indexPath.item
    
        self.buttonCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        self.buttonCollectionView.reloadData()
        self.promoTableView.reloadData()
    }

    // MARK: TableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch selectedButtonIndex {
        case 0:
            return self.promoDetilsList?.count ?? 0
        default:
            return self.termsDataList?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let promoDetailsCell = self.promoTableView.dequeueReusableCell(withIdentifier:"BBQSpecialPromoTableViewCellID")
            as! BBQSpecialPromoTableViewCell
        
        if let promoData = self.promoDetilsList?[indexPath.row], selectedButtonIndex == 0 {
            promoDetailsCell.updatePromotionDetails(with: promoData, index: indexPath.row)
        } else if let termsData = self.termsDataList?[indexPath.row] {
            promoDetailsCell.updatePromotionDetails(with: termsData, index: indexPath.row)
        }
        return promoDetailsCell
    }
    
    
    
}

