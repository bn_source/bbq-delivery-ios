//
//  BottomSheetImagesViewController.swift
//  BottomSheet
//
//  Created by Chandan Singh on 25/07/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

enum PromotionOfffersScreenType {
    case Home, Hamburger, Notification, OutletInfo
}

protocol PromotionOfffersProtocol {
    
    func didPressedTableReservationFromPromotion()
    
}

class PromotionOfffersVC: BaseViewController {
    
    //MARK: IBOutlets
    @IBOutlet weak var imagesTableView: UITableView!
    @IBOutlet weak var headerLabel: UILabel!
    
    var promoDelegate : BottomSheetImagesViewControllerProtocol?
    
    //MARK: Properties
    fileprivate let cellId = "PromotionOfffersCell"
    private let detailScreenNib = Constants.CellIdentifiers.voucherDetailsControllerNib
    var promosData: [PromosData]? = nil
    var promoScreenType: PromotionOfffersScreenType = .Home
    
    weak var dataSource: BottomSheetImagesDataSource? = nil
    weak var delegate: BottomSheetImagesDelegate? = nil
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        AnalyticsHelper.shared.triggerEvent(type: .SO01)
        // Do any additional setup after loading the view.
        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        imagesTableView.reloadData()
    }
    
    //MARK:- Setup UI
    private func setupUI() {

       
        self.headerLabel.text = "Offers & Promotions"
        
        self.imagesTableView.register(UINib(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
    }
    
    @IBAction func onClickBtnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension PromotionOfffersVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let promoData = self.promosData else {
            return 0
        }
        
        return promoData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? PromotionOfffersCell else {
            return UITableViewCell()
        }
        
        guard let data = self.promosData else {
            return UITableViewCell()
        }
        
        let cellData = data[indexPath.row]
        LazyImageLoad.setImageOnImageViewFromURL(imageView: cell.backgroundImageView, url: cellData.promoImage) { (image) in
            
        }
        
        cell.configureCell(promoTitle: cellData.promoTitle ?? "", promoDesc: cellData.promoDescription ?? "", promoDuration: cellData.promoDuration ?? "")

        return cell
    }
}

extension PromotionOfffersVC: UITableViewDelegate, BBQPromotionDetailScreenProtocol {
    
    func didPressedReserveTableFromPromotionDetails() {
        navigateToHomePage()
        if let tabBarController = getTabBarVC() {
            tabBarController.selectedIndex = 0
        }
        self.dismiss(animated: true) {
            if let promoDelegate = self.promoDelegate{
                promoDelegate.didPressedTableReservationFromPromotion()
            }else{
                NotificationCenter.default.post(name: Notification.Name(Constants.NotificationName.showBookingScreen),
                                                object: nil)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        AnalyticsHelper.shared.triggerEvent(type: .SO02)
        
        /*let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: 0.0)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        
        let detailsViewController = BBQVoucherDetailsController(nibName: detailScreenNib, bundle: nil)
        detailsViewController.promotionId = Int(data[indexPath.section].promoId ?? "0") ?? 0
        detailsViewController.detailScreenType = .Promotion
        bottomSheetController.present(detailsViewController, on: self)*/
        
        if let promoData = self.promosData, promoData.count > indexPath.row {
            let promotion = promoData[indexPath.row]
            if promotion.promotion_url.lowercased() == "ubq"{
                openBIBDeliveryHomepage()
            }else if promotion.promotion_url.lowercased() == "res"{
                openReservationController()
            }else if promotion.promotion_url.lowercased() == "hpc"{
                openHappinesscardController()
            }else{
                let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: 0.0)
                let bottomSheetController = BottomSheetController(configuration: configuration)
                let promotionDetailScreen = UIStoryboard.loadPromotionDetailScreen()
                promotionDetailScreen.promotionDeelegate = self
                promotionDetailScreen.isCameFromSpeciallyScreen = true
                promotionDetailScreen.promotionId = Int(promotion.promoId ?? "0") ?? 0
                bottomSheetController.present(promotionDetailScreen, on: self)
            }
        }
    }
    
    
}
