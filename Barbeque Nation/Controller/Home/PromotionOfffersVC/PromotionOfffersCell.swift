//
//  BottomSheetImageTableCell.swift
//  BottomSheet
//
//  Created by Chandan Singh on 25/07/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

class PromotionOfffersCell: UITableViewCell {
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var stackViewForContainer: UIView!
    
    //var promoOverlayType: PromosOverlayType = .Collapsed
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        stackViewForContainer.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
        viewForContainer.setShadow(color: .lightGray, opacity: 0.2, offSet: .zero, radius: 10.0, scale: true)
    }
    
    func configureCell(promoTitle: String, promoDesc: String, promoDuration: String) {
        self.lblTitle.text = promoTitle
        self.lblDesc.text = promoDesc
    }
}
