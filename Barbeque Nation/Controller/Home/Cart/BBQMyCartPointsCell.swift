//
 //  Created by Chandan Singh on 05/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQMyCartPointsCell.swift
 //

import UIKit

protocol BBQMyCartPointsCellProtocol: AnyObject {
    func addPointsPressed()
    func removePointsPressed()
}

class BBQMyCartPointsCell: UITableViewCell {
    
    @IBOutlet weak var pointsContainerView: UIView!
    @IBOutlet weak var bbqPointImageView: UIImageView!
    @IBOutlet weak var bbqPointLabel: UILabel!
    @IBOutlet weak var bbqPoints: UILabel!
    @IBOutlet weak var pointsAddButton: UIButton!
    @IBOutlet weak var pointsDeleteButton: UIButton!
    @IBOutlet weak var pointsHeightConstraint: NSLayoutConstraint!
    weak var pointCellDelegate : BBQBookingPointsTableViewCellDelegate? //BBQBookingPointsTableViewCellDelegate?

    private var isPointsAdded : Bool?

    weak var delegate: BBQMyCartPointsCellProtocol? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        setupUI()
    }
    
    private func setupCell() {
            self.isPointsAdded = false
        }
    
    //MARK: UI Methods
    private func setupUI() {
        self.separatorInset = UIEdgeInsets(top: 0.0, left: 1000000.0, bottom: 0.0, right: 0.0)
        
       // bbqPointImageView.image = UIImage(named: "icon_bbqn_points")
        self.pointsAddButton.setTitle(kMyCartAddCouponsText, for: .normal)
     //   self.pointsDeleteButton.setTitle(kMyCartRemoveCouponsText, for: .normal)
        
             self.setupCell()
             self.pointsContainerView.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
             self.pointsContainerView.dropShadow()
        
    }
    
    func setupPointsView(isPointApplied: Bool, appliedPoint: String, appliedAmount: String, totalPoints: String,  isPointEnabled: Bool) {
//        if isPointApplied {
//            if appliedAmount == "0" || totalPoints == "0" {
////self.disabledMode()
//            } else {
//                self.isUserInteractionEnabled = true
//                self.contentView.layer.opacity = 1.0
//
//                self.bbqPointLabel.text = String(format: "%@ [%@ Smiles]", kMyCartBBQNPointText, totalPoints)
//
//                self.bbqPoints.isHidden = false
//                self.bbqPoints.text = "-" + appliedAmount
//
//                self.pointsAddButton.isHidden = true
//               // self.pointsDeleteButton.isHidden = false
//                self.pointsContainerView.backgroundColor = .appliedPointsColor
//            }
//        } else {
//            self.isUserInteractionEnabled = true
//            self.contentView.layer.opacity = 1.0
//
//            self.bbqPointLabel.text = String(format: "%@ [%@ Smiles]", kMyCartBBQNPointText, totalPoints)
//            self.bbqPoints.isHidden = true
//
//            self.pointsAddButton.isHidden = false
//           // self.pointsDeleteButton.isHidden = true
//            self.pointsContainerView.backgroundColor = .white
//
//            if totalPoints == "0" {
//                self.pointsAddButton.isEnabled = false
//            }
//
            self.isPointsAdded = isPointApplied
            
            if isPointApplied {
                self.contentView.layer.opacity = isPointEnabled ?  1.0 : 0.4
                self.isUserInteractionEnabled = isPointEnabled
                
                if (isPointApplied == true), let redAmount = totalPoints as? String ,
                   let redPoints = appliedPoint as? String {
                    self.bbqPoints.text = "-\(getCurrency())\(redAmount)"
                    self.bbqPoints.textColor = .darkGreen
                    self.bbqPointLabel.text = "[\(redPoints) \(kPoints)]"
                }
                
                if isPointEnabled ==  false  { // if holdingPoints <= 0 || isPointApplied {
                    self.isUserInteractionEnabled = false
                    self.contentView.layer.opacity = 0.4
                }
                
                if isPointApplied {
                    self.pointsAddButton.applyAddRemoveButtonTheme(title: kMyCartRemoveCouponsText, color: .theme)
                }else{
                    self.pointsAddButton.applyAddRemoveButtonTheme(title: kMyCartAddCouponsText, color: .darkGreen)
                }
                
                if isPointApplied {
                   self.hideShowbbqPoints(isHidden: isPointApplied)
               }
                    //else {
//                    self.hideShowbbqPoints(isHidden: didAddNewPoints)
//                }
            }else{
                self.pointsAddButton.applyAddRemoveButtonTheme(title: kMyCartAddCouponsText, color: .darkGreen)

            }
            self.layoutIfNeeded()
            
        }
    
        // MARK: Private Methods
        
        private func hideShowPointsValueLabel(isHidden: Bool) {
            if isHidden {
                self.bbqPoints.isHidden = false
                self.pointsHeightConstraint.constant = 20
                //self.contentView.backgroundColor = UIColor.hexStringToUIColor(hex: "F7F7F7")
            } else {
                self.bbqPoints.isHidden = true
                self.pointsHeightConstraint.constant = 0
               // self.contentView.backgroundColor = .clear
            }
        }
    
    
    func disabledMode() {
        self.isUserInteractionEnabled = false
        self.contentView.layer.opacity = 0.4
        self.bbqPointLabel.text = String(format: "%@ [%d Smiles]", kMyCartBBQNPointText, BBQUserDefaults.sharedInstance.UserLoyaltyPoints)
    }
    
    //MARK: UI Actions
    @IBAction func pointsAddButtonPressed(_ sender: UIButton) {
//        if let delegate = self.delegate {
//            delegate.addPointsPressed()
//        }
        self.isPointsAdded = !self.isPointsAdded!
            self.pointCellDelegate?.didPressedPointsAddRemoveButton(isAdd:  self.isPointsAdded!)
        
    }
    
    func loadLoyaltyPointCell(isAdded: Bool, pointModel: BBQPointsValueModel, isPointEnabled: Bool) {
           self.isPointsAdded = isAdded
           
           self.contentView.layer.opacity = isPointEnabled ?  1.0 : 0.4
           self.isUserInteractionEnabled = isPointEnabled
           
           
       
           if isAdded, let redAmount = pointModel.redeemAmount, let availablePoints = pointModel.availablePoints  {
               self.bbqPoints.text = "-\(getCurrency())\(redAmount)"
               self.bbqPoints.textColor = .darkGreen
               self.bbqPointLabel.text = "[\(availablePoints) \(kPoints)]" //"\(kPoints) [\(availablePoints) \(kPoints)]"
           } else {
               let holdingPoints = BBQUserDefaults.sharedInstance.UserLoyaltyPoints
               self.bbqPointLabel.text = "[\(holdingPoints) \(kPoints)]"//"\(kPoints) [\(holdingPoints) \(kPoints)]"
               if holdingPoints <= 0 && !BBQUserDefaults.sharedInstance.isGuestUser {
                   self.isUserInteractionEnabled = false
                   self.contentView.layer.opacity = 0.4 // Disabling points row if 0 points are available.
               }
           }
          
           if isAdded{
               self.pointsAddButton.applyAddRemoveButtonTheme(title: kMyCartRemoveCouponsText, color: .theme)
           }else{
               self.pointsAddButton.applyAddRemoveButtonTheme(title: kMyCartAddCouponsText, color: .darkGreen)
           }
           
           self.hideShowbbqPoints(isHidden: isAdded)
           self.layoutIfNeeded()

       }

    
      func loadAppliedPointsCell(pointsModel: BBQPointsValueModel, didAppliedAlready: Bool,
                                 didAddNewPoints: Bool, isPointEnabled: Bool) {
          
          self.isPointsAdded = didAddNewPoints
          
          self.contentView.layer.opacity = isPointEnabled ?  1.0 : 0.4
          self.isUserInteractionEnabled = isPointEnabled
          
          if (didAddNewPoints || didAppliedAlready), let redAmount = pointsModel.redeemAmount,
              let redPoints = pointsModel.redeemablePoints {
              self.bbqPoints.text = "-\(getCurrency())\(redAmount)"
              self.bbqPoints.textColor = .darkGreen
              self.bbqPointLabel.text = "[\(redPoints) \(kPoints)]"
          } else {
              let holdingPoints = BBQUserDefaults.sharedInstance.UserLoyaltyPoints
              self.bbqPointLabel.text = "[\(holdingPoints) \(kPoints)]"
          }
          
          let holdingPoints = BBQUserDefaults.sharedInstance.UserLoyaltyPoints
          if holdingPoints <= 0 { //|| didAppliedAlready 
              self.isUserInteractionEnabled = false
              self.contentView.layer.opacity = 0.4
          }
          
          if didAppliedAlready || didAddNewPoints{
              self.pointsAddButton.applyAddRemoveButtonTheme(title: kMyCartRemoveCouponsText, color: .theme)
          }else{
              self.pointsAddButton.applyAddRemoveButtonTheme(title: kMyCartAddCouponsText, color: .darkGreen)
          }
          
          if didAppliedAlready {
             self.hideShowbbqPoints(isHidden: didAppliedAlready)
          } else {
              self.hideShowbbqPoints(isHidden: didAddNewPoints)
          }
          
          self.layoutIfNeeded()
          
      }
      
      // MARK: Private Methods
      
      private func hideShowbbqPoints(isHidden: Bool) {
          if isHidden {
              self.bbqPoints.isHidden = false
              self.pointsHeightConstraint.constant = 20
              //self.contentView.backgroundColor = UIColor.hexStringToUIColor(hex: "F7F7F7")
          } else {
              self.bbqPoints.isHidden = true
              self.pointsHeightConstraint.constant = 0
             // self.contentView.backgroundColor = .clear
          }
      }
  
    @IBAction func pointsDeleteButtonPressed(_ sender: UIButton) {
//        if let delegate = self.delegate {
//            delegate.removePointsPressed()//        }
    }
}

