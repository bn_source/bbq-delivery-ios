//
 //  Created by Chandan Singh on 12/11/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQCartFinalCell.swift
 //

import UIKit

protocol BBQMyCartFinalCellProtocol: AnyObject {
    func paymentButtonPressed()
}

class BBQCartFinalCell: UITableViewCell {
    
    //MARK: Outlets
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var paymentButton: UIButton!
    
    //MARK: Properties
    var currencyString: String = "INR"
    weak var delegate: BBQMyCartFinalCellProtocol? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setupUI()
    }
    
    //MARK: UI Methods
    func setupUI() {
      //  self.priceTermsLabel.text = String(format: "%@ %@", kMyCartPriceTermsText, currencyString)
        self.priceLabel.text = String(format: "%@", currencyString)

        self.paymentButton.setTitle(kMyCartPaymentButtonText, for: .normal)
        self.paymentButton.roundCorners(cornerRadius: 4, borderColor: .clear, borderWidth: 0.0)
        
        //self.separatorInset = UIEdgeInsets(top: 0.0, left: 1000000.0, bottom: 0.0, right: 0.0)
    }
    
    @IBAction func paymentButtonPressed(_ sender: UIButton) {
        if let delegate = self.delegate {
            delegate.paymentButtonPressed()
        }
    }
}
