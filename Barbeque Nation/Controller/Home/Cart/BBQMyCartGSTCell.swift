//
 //  Created by Chandan Singh on 14/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQMyCartGSTCell.swift
 //

import UIKit


class BBQMyCartGSTCell: UITableViewCell {
    
    
    //MARK: Outlets
    @IBOutlet weak var cgstTextLabel: UILabel!
    @IBOutlet weak var sgstTextLabel: UILabel!
    @IBOutlet weak var cgstPriceLabel: UILabel!
    @IBOutlet weak var sgstPriceLabel: UILabel!
//
//    //MARK: Life cycle
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        // Initialization code
//    }
//
//    //MARK: UI Methods
//    func setupUIWith(cgstPrice: String, sgstPrice: String) {
//        self.cgstTextLabel.text = kMyCartCGSTText
//        self.sgstTextLabel.text = kMyCartSGSTText
//        self.cgstPriceLabel.text = cgstPrice
//        self.sgstPriceLabel.text = sgstPrice
//    }
    
    
    @IBOutlet weak var amountSplitTableView: UITableView!
    
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewForContainer: UIView!
    
    
    weak var amountCellDelegate: BBQBookingAmountDetailsTableViewCellDelegate?

    private var taxList : [CardTax]?
    
    private var bookingStatus : PaymentStatus?
    private var paidAmount: Double?
    
    var subTotalAmount : Double = 0.0
//    {
//        didSet {
//            self.slotsViewModel?.bookingAmountSubTotal = subTotalAmount
//        }
//    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupAmountSplitTable()
        viewForContainer.dropShadow()
    }
    
    // MARK: Setup Methods
    
    func setupAmountSplitTable() {
        self.amountSplitTableView.delegate = self
        self.amountSplitTableView.dataSource = self
        
        self.amountSplitTableView.estimatedRowHeight = 100
        self.amountSplitTableView.rowHeight = UITableView.automaticDimension
        
        self.amountSplitTableView.register(UINib(nibName: "BBQAmountEntryTableViewCell",
                                                 bundle: nil),
                                           forCellReuseIdentifier: "BBQAmountEntryTableViewCellID")
    }

    // MARK: Public Methods
    
    
    func loadTaxAmountDetails(taxListReceived: [CardTax]) {
        
        self.taxList = taxListReceived
        
        self.amountSplitTableView.reloadData {
            self.amountCellDelegate?.contentDidChange(cell: self,
                                                      contentHeight: self.amountSplitTableView.contentSize.height)
        }
    }
    
    
    
}


extension BBQMyCartGSTCell : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let entryCell = tableView.dequeueReusableCell(withIdentifier: "BBQAmountEntryTableViewCellID",
                                                      for: indexPath) as! BBQAmountEntryTableViewCell
        
//        if bookingStatus == .AfterDining, indexPath.row > 0 {
        if let taxList = taxList, taxList.count >= indexPath.row{
            let rowData = AmountRowData(amountTitle: kDiningTaxServiceCharge, amountValue: "\(getCurrency())\(taxList[indexPath.row].taxPercentage ?? "0")")
            entryCell.loadAmountEntries(with: rowData)
            entryCell.entryDelegate = self
            
            return entryCell
        }
        return UITableViewCell()

    }
    
    
}
extension BBQMyCartGSTCell: BBQAmountEntryTableViewCellDelegate{
   
    
    func didClickOnDetails(cell: BBQAmountEntryTableViewCell) {
        amountCellDelegate?.didClickOnDetails(cell: cell)

    }
    
    func contentDidChange(cell: BBQMyCartGSTCell) {
        
    }
    
}

