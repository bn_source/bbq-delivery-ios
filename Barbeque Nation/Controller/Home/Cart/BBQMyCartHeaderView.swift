//
 //  Created by Chandan Singh on 14/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQMyCartHeaderView.swift
 //

import UIKit

protocol BBQMyCartHeaderViewProtocol {
    
    func didPressedEmptyCartButton()
}

class BBQMyCartHeaderView: UIView {
    
    //MARK: Outlets
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var headerImageView: UIImageView!
    
    @IBOutlet weak var emptyCartButton: UIButton!
    
    var headerDelegate : BBQMyCartHeaderViewProtocol?
    
    //MARK: Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 15, y: self.frame.height - 2, width: UIScreen.main.bounds.width - 30, height: 0.2)
        bottomLine.backgroundColor = UIColor.init(red:54/255,green: 64/255,blue: 68/255,alpha: 1).cgColor
        self.layer.addSublayer(bottomLine)
    }
    
    //MARK: Setup
    func setUIWith(isDataAvailable: Bool) {
        self.headerLabel.text = kMyCartText
        
        if isDataAvailable {
            self.headerImageView.image = UIImage(named: Constants.AssetName.cartFilledIcon)
        } else {
            self.headerImageView.image = UIImage(named: Constants.AssetName.cartEmptyIcon)
        }
    }
    
    // MARK: IBAction Methods
    
    @IBAction func emptyCartButtonPressed(_ sender: UIButton) {
        self.headerDelegate?.didPressedEmptyCartButton()
    }
}
