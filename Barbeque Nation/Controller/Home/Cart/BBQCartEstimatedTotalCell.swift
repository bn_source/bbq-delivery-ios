//
 //  Created by Chandan Singh on 14/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQCartEstimatedTotalCell.swift
 //

import UIKit

class BBQCartEstimatedTotalCell: UITableViewCell {
    //MARK: Outlets
    @IBOutlet weak var estimatedTotalTextLabel: UILabel!
    @IBOutlet weak var estimatedTotalPriceLabel: UILabel!
    
    //MARK: Life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK: UI Methods
    func setupUIWith(price: String) {
        self.estimatedTotalTextLabel.text = kMyCartEstimatedTotalText
        self.estimatedTotalPriceLabel.text = price.toCurrencyFormat()
    }
}
