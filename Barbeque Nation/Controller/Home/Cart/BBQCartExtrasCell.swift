//
 //  Created by Chandan Singh on 14/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQCartExtrasCell.swift
 //

import UIKit

protocol BBQCartExtrasCellProtocol: AnyObject {
    func addCouponsPressed()
    func removeCouponsPressed()
    func termsAndConditionsPressed()
}

class BBQCartExtrasCell: UITableViewCell {
    //MARK: Outlets
    @IBOutlet weak var couponContainerView: UIView!
    @IBOutlet weak var addedCouponImageView: UIImageView!
    @IBOutlet weak var couponNameLabel: UILabel!
    @IBOutlet weak var tickMarkImageView: UIImageView!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var couponPriceLabel: UILabel!
    @IBOutlet weak var couponRemoveButton: UIButton!
    @IBOutlet weak var appliedCouponNameLabel: UILabel!
    @IBOutlet weak var tncButton: UIButton!
    
    weak var delegate: BBQCartExtrasCellProtocol? = nil
    
    //MARK: Life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        setupUI()
    }
    
    //MARK: UI Methods
    private func setupUI() {
        self.separatorInset = UIEdgeInsets(top: 0.0, left: 1000000.0, bottom: 0.0, right: 0.0)
        
        self.couponNameLabel.text = kMyCartCouponsText
        
        addedCouponImageView.image = UIImage(named: "icon_coupon_black")
        tickMarkImageView.image = UIImage(named: "icon_tick_green")
        
        self.addButton.setTitle(kMyCartAddCouponsText, for: .normal)
        self.couponRemoveButton.setTitle(kMyCartRemoveCouponsText, for: .normal)
    }
    
    func configureCoupons(isCouponAdded: Bool, couponName: String, couponPrice: Double) {
        
        if isCouponAdded {
            self.couponContainerView.backgroundColor = .appliedPointsColor
            appliedCouponNameLabel.text = couponName + " " + "applied"
            appliedCouponNameLabel.isHidden = false
            tickMarkImageView.isHidden = false
            
            self.addButton.isHidden = true
            self.couponRemoveButton.isHidden = false
            
            couponPriceLabel.isHidden = true
            /*let priceString = String(format: "-%.2lf", couponPrice)
            couponPriceLabel.text = priceString*/
            
            self.tncButton.isHidden = false
        } else {
            self.couponContainerView.backgroundColor = .white
            appliedCouponNameLabel.text = ""
            appliedCouponNameLabel.isHidden = true
            tickMarkImageView.isHidden = true
            
            self.addButton.isHidden = false
            self.couponRemoveButton.isHidden = true
            
            couponPriceLabel.isHidden = true
            
            self.tncButton.isHidden = true
        }
        self.isUserInteractionEnabled = true
        self.contentView.layer.opacity = 1.0
    }
    
    func disabledMode() {
        self.isUserInteractionEnabled = false
        self.contentView.layer.opacity = 0.4
    }
    
    @IBAction func addCouponsPressed(_ sender: UIButton) {
        if let delegate = self.delegate {
            delegate.addCouponsPressed()
        }
    }
    
    @IBAction func removeButtonPressed(_ sender: UIButton) {
        if let delegate = self.delegate {
            delegate.removeCouponsPressed()
        }
    }
    
    @IBAction func tncButtonPressed(_ sender: UIButton) {
        if let delegate = self.delegate {
            delegate.termsAndConditionsPressed()
        }
    }
}
