//
//  Created by Chandan Singh on 14/09/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQMyCartViewController.swift
//

import UIKit
import FirebaseAnalytics
import AMPopTip
import WebKit
import Razorpay

enum MyCartRowType {
    case contentRow , PointsRow,  GSTRow , subTotalRow,  ExtrasRow, extimatedTotalRow, finalRow, none
}

protocol BBQMyCartViewControllerProtocol: AnyObject {
    func didPressProceedToPayment(points: Int, barCode: String, amount: String, currency: String, discount: String, bottomSheet: BottomSheetController?)
    func updateCartData()
}

class BBQMyCartViewController: BaseViewController {
    
    //MARK: Outlets
    @IBOutlet var containerView: UIView!
    @IBOutlet weak var cartTableView: UITableView!
    @IBOutlet weak var noDataLabel: UILabel!
    @IBOutlet weak var headerView: BBQMyCartHeaderView!
    //MARK: Properties
    weak var delegate: BBQMyCartViewControllerProtocol? = nil
    var cartViewModel : CartViewModel? = nil
    var cartModel : ModifyCartModel? = nil
    private var rowType: MyCartRowType = .contentRow
    
    var couponsArray = [VoucherCouponsData]()
    internal var pointsValueModel: BBQPointsValueModel? = nil
    
    private var cartCalculationData = CartCalculationsData()
    
    private var isPointRemovalPopupShown = false
    private var isCouponRemovalPopupShown = false
    private var couponsTerms: [String]?
    
    //MARK: Constants
    private let contentCellId = Constants.CellIdentifiers.cartContentCell
    private let subTotalCellId = Constants.CellIdentifiers.cartSubTotalCell
    private let gstCellId = Constants.CellIdentifiers.cartGSTCell
    private let pointsCellId = Constants.CellIdentifiers.cartPointsCell
    private let extrasCellId = Constants.CellIdentifiers.cartExtrasCell
    private let estimatedTotalCellId = Constants.CellIdentifiers.cartEstimatedTotalCell
    private let finalCellId = Constants.CellIdentifiers.cartFinalCell
    
    private var popTip = PopTip()
    
    var superVC: UIViewController?
    
    internal var isTaxSplitRefreshNeeded : Bool = true
    internal var isCardsResetWarned : Bool = false
    internal var isPointsResetWarned : Bool = false
    
    private var didAddPoints     : Bool = false
    private var didAddCoupons    : Bool = false
    private var didAddVouchers   : Bool = false
    private var didAddCorpOffers : Bool = false
    
    private var didAppliedPointsAlready : Bool =  false
    
    internal var appliedCouponAmount  : Double = 0.00
    internal var appliedVoucherAmount : Double = 0.00
    internal var appliedPointsAmount : Double = 0.00
    internal var appliedPoints : Int? = nil
    
    
    internal var appliedCoupons    : [VoucherCouponsData]?
    internal var initialAppliedVouchers   : [VoucherCouponsData]?
    internal var appliedVouchers   : [VoucherCouponsData]?
    internal var appliedCorpOffers : [VoucherCouponsData]?
    internal var vouchersTerms : [String]?
    
    var paymentFetchMethods: [AnyHashable:Any]?
    var razorpay: RazorpayCheckout!
    
    lazy private var paymentControllerViewModel : MakePaymentViewModel = {
        let paymentVM = MakePaymentViewModel()
        return paymentVM
    } ()
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AnalyticsHelper.shared.screenName(name: "MyCart", className: "BBQMyCartViewController")
        AnalyticsHelper.shared.triggerEvent(type: .HC01)
        
        
        // Do any additional setup after loading the view.
        self.setupUI()
        self.getPointsFromServer(isUpdatePointView: false)
        AnalyticsHelper.shared.triggerEvent(type: .Happinesscard_Checkout, properties: [.Happiness_Card_Cart_Count : cartModel?.getTotalQuantity() as Any, .Happiness_Cart_Checkout_Amount: cartModel?.totalPrice ?? 0])
        loadCOupons()
        initialAppliedVouchers
        initiateThePaymentData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.screenName(name: .Happiness_Cart_Screen)
    }
    
    //MARK: UI Methods
    private func setupUI() {
        noDataLabel.text = kMyCartNoItemsText
        self.containerView.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        
        self.cartTableView.register(UINib(nibName: contentCellId, bundle: nil), forCellReuseIdentifier: contentCellId)
        self.cartTableView.register(UINib(nibName: subTotalCellId, bundle: nil), forCellReuseIdentifier: subTotalCellId)
        self.cartTableView.register(UINib(nibName: gstCellId, bundle: nil), forCellReuseIdentifier: gstCellId)
        self.cartTableView.register(UINib(nibName: pointsCellId, bundle: nil), forCellReuseIdentifier: pointsCellId)
        self.cartTableView.register(UINib(nibName: extrasCellId, bundle: nil), forCellReuseIdentifier: extrasCellId)
        self.cartTableView.register(UINib(nibName: estimatedTotalCellId, bundle: nil), forCellReuseIdentifier: estimatedTotalCellId)
        self.cartTableView.register(UINib(nibName: finalCellId, bundle: nil), forCellReuseIdentifier: finalCellId)
        
        
        self.cartTableView.register(UINib(nibName: "BBQCouponsTableViewCell",
                                          bundle: nil),
                                    forCellReuseIdentifier: "BBQCouponsTableViewCellID")
        
        self.cartTableView.reloadData()
        
        popTip.bubbleColor = .white
        popTip.cornerRadius = 5.0
        popTip.shouldShowMask = true
    }
    
    //MARK: Helper Methods
    func detectRowType(rowIndex: Int) -> MyCartRowType {
        if rowIndex < (self.cartModel?.orderItems!.count)! {
            return .contentRow
        } else {
            if rowIndex == (self.cartModel?.orderItems!.count)! {
                return .PointsRow
            } else if rowIndex == (self.cartModel?.orderItems!.count)! + 1 {
                return .GSTRow
            } else if rowIndex == (self.cartModel?.orderItems!.count)! + 2 {
                return .ExtrasRow
            } else if rowIndex == (self.cartModel?.orderItems!.count)! + 3 {
                return .subTotalRow
            }else if rowIndex == (self.cartModel?.orderItems!.count)! + 4 {
                return .finalRow
            }
            //            else if rowIndex == (self.cartModel?.orderItems!.count)! + 4 {
            //                return .extimatedTotalRow
            //            }
        }
        return .none
    }
    @IBAction func backButtonPressed(_ sender : UIButton){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func loadCOupons(){
        
        self.cartViewModel?.getVoucherAndCoupons(completion: { (isSuccess) in
            //  UIUtils.hideLoader()
            if isSuccess {
                if (self.couponsArray.count > 0) {
                    self.couponsArray.removeAll()
                }
                //  self.termsAndCondtions = (self.cartViewModel.vouchersAndCouponsModel?.voucherTermsAndConditions!)!
                
                if self.cartViewModel?.getActiveCoupons.count ?? 0 > 0 {
                    self.couponsArray = self.cartViewModel?.getActiveCoupons ?? []
                    //                    self.noDataLabel.isHidden = true
                    //                    self.corporateOffersListView.isHidden = false
                    //                    self.corporateOffersListView.reloadData()
                } else {
                    //                    self.noDataLabel.isHidden = false
                    //                    self.noDataLabel.text = kNoCouponsAvailable
                    //                    self.corporateOffersListView.isHidden = true
                }
            }
        })
    }
    
}

extension BBQMyCartViewController: UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let cartDataModel = self.cartModel, let data = cartDataModel.orderItems, data.count > 0 {
            let contentRowCount = self.cartModel?.orderItems?.count ?? 0
            return contentRowCount + 5 // tax, subtotal, smile, coupon, finalprice
        }
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let dataModel = self.cartModel else {
            return tableView.defaultCell(indexPath: indexPath)
        }
        
        let rowType = detectRowType(rowIndex: indexPath.row)
        
        if rowType == .contentRow {
            
            guard let contentCell = tableView.dequeueReusableCell(withIdentifier: contentCellId, for: indexPath) as? BBQCartContentCell else {
                return UITableViewCell()
            }
            
            contentCell.tag = indexPath.row
            contentCell.serviceDelegate = self
            contentCell.configureCellFor(orderItem: (self.cartModel?.orderItems![indexPath.row])!)
            return contentCell
        } else  if rowType == .PointsRow {
            
            guard let pointsCell = tableView.dequeueReusableCell(withIdentifier: pointsCellId, for: indexPath) as? BBQMyCartPointsCell else {
                return UITableViewCell()
            }
            // pointsCell.delegate = self
            pointsCell.pointCellDelegate = self
            let price = (dataModel.totalPrice ?? 0.0) - self.cartCalculationData.couponPrice
            if self.cartCalculationData.totalPoint == 0 {
                cartCalculationData.isPointEnabled = false
            } else if self.cartCalculationData.pointApplied { //}, price <= 0.0 {
                //Do not disable view as, points can be removed using remove button
                cartCalculationData.isPointEnabled = true
            } else {
                
                cartCalculationData.isPointEnabled = true
            }
            
            pointsCell.loadAppliedPointsCell(pointsModel:pointsValueModel ?? BBQPointsValueModel(),
                                             didAppliedAlready:  self.cartCalculationData.pointApplied,
                                             didAddNewPoints: didAddPoints,
                                             isPointEnabled:  cartCalculationData.isPointEnabled)
            
            
            return pointsCell
        } else if rowType == .subTotalRow {
            
            guard let subTotalCell = tableView.dequeueReusableCell(withIdentifier: subTotalCellId, for: indexPath) as? BBQMyCartSubTotalCell else {
                return UITableViewCell()
            }
            let price = (dataModel.totalPrice ?? 0.0) - self.cartCalculationData.couponPrice
            let subTotalPrice = String(format: "%.0f", price)
            subTotalCell.setupUIWith(price: subTotalPrice)
            return subTotalCell
        } else if rowType == .GSTRow {
            
            guard let gstCell = tableView.dequeueReusableCell(withIdentifier: gstCellId, for: indexPath) as? BBQMyCartGSTCell else {
                return UITableViewCell()
            }
            
            //            guard let cgst = Double(dataModel.taxes?.first?.taxPercentage ?? "0"), let sgst = Double(dataModel.taxes?.last?.taxPercentage ?? "0"), let price = dataModel.totalPrice else {
            //                return UITableViewCell()
            //            }
            //gstCell.setupUIWith(cgstPrice: "\(price/100 * cgst )", sgstPrice: "\(price/100 * sgst)")
            
            
            gstCell.amountCellDelegate = self
            
            if self.isTaxSplitRefreshNeeded {
                if let _ = dataModel.taxes {
                    gstCell.loadTaxAmountDetails(taxListReceived:  dataModel.taxes ?? [])
                }
                self.isTaxSplitRefreshNeeded = false
            }
            
            return gstCell
        }  else if rowType == .ExtrasRow {
            
            
            
            let price = (dataModel.totalPrice ?? 0.0) - Double(self.cartCalculationData.applicablePoint)
            //            if self.cartCalculationData.pointApplied, !self.cartCalculationData.isCouponAdded, price <= 0.0 {
            //                extrasCell.disabledMode()
            //            } else {
            //                extrasCell.delegate = self
            //                extrasCell.configureCoupons(isCouponAdded: self.cartCalculationData.isCouponAdded, couponName: self.cartCalculationData.couponName, couponPrice: self.cartCalculationData.couponPrice)
            //            }
            
            var enabled = false
            if self.cartCalculationData.pointApplied, !self.cartCalculationData.isCouponAdded, price <= 0.0 {
                enabled = false
            } else {
                enabled = true
            }
            
            let couponCell =
            self.cartTableView.dequeueReusableCell(withIdentifier: "BBQCouponsTableViewCellID")
            as! BBQCouponsTableViewCell
            couponCell.couponDelegate = self
            couponCell.loadCouponsData(with: self.appliedCoupons ?? [VoucherCouponsData](),
                                       didAdd: self.cartCalculationData.isCouponAdded,
                                       isCardEnabled: enabled)
            return couponCell
            
            
            //        } else if rowType == .extimatedTotalRow {
            //
            //            guard let estimatedTotalCell = tableView.dequeueReusableCell(withIdentifier: estimatedTotalCellId, for: indexPath) as? BBQCartEstimatedTotalCell else {
            //                return UITableViewCell()
            //            }
            //            guard let cgst = Double(dataModel.taxes?.first?.taxPercentage ?? "0"), let sgst = Double(dataModel.taxes?.last?.taxPercentage ?? "0"), let price = dataModel.totalPrice else {
            //                return UITableViewCell()
            //            }
            //
            //
            //            let subTotalPrice = price + (price/100 * cgst) + (price/100 * sgst)
            //
            //            var bbqAmount = 0.0
            //            if self.cartCalculationData.pointApplied {
            //                bbqAmount = Double(self.cartCalculationData.applicableAmount)
            //            }
            //
            //            var extimatedTotal = 0.0
            //            if subTotalPrice != 0 {
            //                extimatedTotal = subTotalPrice - bbqAmount - self.cartCalculationData.couponPrice
            //                if extimatedTotal < 0 {
            //                    extimatedTotal = 0.0
            //                }
            //            }
            //
            //            self.cartCalculationData.extimatedTotalPrice = String(format: "%.0f", extimatedTotal)
            //            estimatedTotalCell.setupUIWith(price: self.cartCalculationData.extimatedTotalPrice)
            //            return estimatedTotalCell
        }else if rowType == .finalRow {
            
            guard let finalCell = tableView.dequeueReusableCell(withIdentifier: finalCellId, for: indexPath) as? BBQCartFinalCell else {
                return UITableViewCell()
            }
            
            guard let cgst = Double(dataModel.taxes?.first?.taxPercentage ?? "0"), let sgst = Double(dataModel.taxes?.last?.taxPercentage ?? "0"), let price = dataModel.totalPrice else {
                return UITableViewCell()
            }
            
            
            let subTotalPrice = price + (price/100 * cgst) + (price/100 * sgst)
            
            var bbqAmount = 0.0
            if self.cartCalculationData.pointApplied {
                bbqAmount = Double(self.cartCalculationData.applicableAmount)
            }
            
            var extimatedTotal = 0.0
            if subTotalPrice != 0 {
                extimatedTotal = subTotalPrice - bbqAmount - self.cartCalculationData.couponPrice
                if extimatedTotal < 0 {
                    extimatedTotal = 0.0
                }
            }
            
            self.cartCalculationData.extimatedTotalPrice = String(format: "%.0f", extimatedTotal)
            
            finalCell.delegate = self
            finalCell.priceLabel.text =  (String(format: "%.0f", extimatedTotal)).toCurrencyFormat()
            return finalCell
        }
        
        return UITableViewCell()
    }
}

extension BBQMyCartViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    //    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    //        return 65.0
    //    }
    
    //    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    //        let headerView = UINib(nibName: Constants.CellIdentifiers.cartHeaderViewNib, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! BBQMyCartHeaderView
    //
    //        var isDataAvailable = false
    //        if let cartDataModel = self.cartModel, let data = cartDataModel.orderItems, data.count > 0 {
    //            isDataAvailable = true
    //        }
    //
    //        headerView.headerDelegate = self
    //        headerView.setUIWith(isDataAvailable: isDataAvailable)
    //        return headerView
    //    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    func adjustTableViewHeight() {
        self.cartTableView.beginUpdates()
        self.cartTableView.endUpdates()
    }
    
}

extension BBQMyCartViewController: BBQMyCartFinalCellProtocol {
    func getAppliedDiscountAndPoints() -> (points: Int, discount: String){
        let pointsToBeApplied = self.cartCalculationData.pointApplied ? self.cartCalculationData.applicablePoint : 0
        
        //Total Discount calculation
        var totalDiscount = 0.0
        if self.cartCalculationData.extimatedTotalPrice == "0" {
            //Estimated total is 0. Dont calculate anything.
            totalDiscount = 0
        } else {
            if self.cartCalculationData.pointApplied && !self.cartCalculationData.isCouponAdded {
                //Points applied but coupon not applied
                totalDiscount = self.cartCalculationData.applicableAmount
            } else if !self.cartCalculationData.pointApplied && self.cartCalculationData.isCouponAdded {
                //Coupon applied but ponis not applied
                totalDiscount = self.cartCalculationData.couponPrice
            } else if self.cartCalculationData.pointApplied && self.cartCalculationData.isCouponAdded {
                //Points and coupon both are applied
                totalDiscount = self.cartCalculationData.applicableAmount + self.cartCalculationData.couponPrice
            } else {
                totalDiscount = 0
            }
        }
        
        let totalDiscountInString = String(format: "%.0lf", totalDiscount)
        return (pointsToBeApplied, totalDiscountInString)
    }
    
    func paymentButtonPressed() {
//        if let delegate = self.delegate {
//            let data = getAppliedDiscountAndPoints()
//
//            delegate.didPressProceedToPayment(points: data.points,
//                                              barCode: self.cartCalculationData.couponBarcode,
//                                              amount: self.cartCalculationData.extimatedTotalPrice,
//                                              currency: self.cartModel?.countryCode ?? "INR",
//                                              discount: data.discount,
//                                              bottomSheet: nil)
//
//            AnalyticsHelper.shared.triggerEvent(type: .Happinesscard_Pay)
//        }
        AnalyticsHelper.shared.triggerEvent(type: .HC10)
        AnalyticsHelper.shared.triggerEvent(type: .Happinesscard_Pay)
        showPaymentForm()
    }
    
    // MARK: - BBQRazorpayControllerProtocol Methods
    internal func showPaymentForm() {
        
        let paymenVc = UIStoryboard.loadPayment()
        let paymentData = getWebViewPaymentId(paymentVC: paymenVc, order_id: nil)
        paymenVc.viewModel = CustomPaymentViewModel.init(razorPay: self.razorpay,razorpayOptions: paymentData.options ?? [:], delegate: paymenVc,webView: paymentData.webView, response: paymentFetchMethods)
        paymenVc.delegate = self
        self.navigationController?.pushViewController(paymenVc, animated: true)
    }
    private func initiateThePaymentData() {
        if razorpay == nil{
            let webView = WKWebView.init(frame: self.view.frame)
            let razorPayAppKey = Bundle.main.object(forInfoDictionaryKey: Constants.Payment.razorpayPublicKey)
            self.razorpay = Razorpay.initWithKey(razorPayAppKey as! String, andDelegate: self, withPaymentWebView: webView)
        }
        
        self.razorpay.getPaymentMethods(withOptions: nil) { (response) in
            self.paymentFetchMethods = response
        } andFailureCallback: { (error) in
            print("Error fetching payment methods:\(error)")
        }
    }
    private func getWebViewPaymentId(paymentVC: BBQCustomPaymentViewController, order_id: String?) -> (webView: WKWebView, options: [String: Any]?){
        let amount = Int(round((Double(self.cartCalculationData.extimatedTotalPrice) ?? 0) * 100))
        let options: [String:Any] = [
            "amount": amount,
            "currency": "INR",
            "description": "Advance amount to be paid",
            "order_id": order_id as Any,
            //            "name": paymentModel.name ?? "",
            "email" : SharedProfileInfo.shared.profileData?.email  ?? BBQUserDefaults.sharedInstance.customPaymentEmail,
            "contact": SharedProfileInfo.shared.profileData?.mobileNumber as Any,
            //Comment me for release
            //            "contact": 9844019983
            
        ]
        
        let webView = WKWebView.init(frame: self.view.frame)
        let razorPayAppKey = Bundle.main.object(forInfoDictionaryKey: Constants.Payment.razorpayPublicKey)
        self.razorpay = Razorpay.initWithKey(razorPayAppKey as! String, andDelegate: paymentVC, withPaymentWebView: webView)
        return (webView, options)
    }
}

extension BBQMyCartViewController: BBQMyCartPointsCellProtocol {
    func addPointsPressed() {
        AnalyticsHelper.shared.triggerEvent(type: .HC04)
        self.getPointsFromServer(isUpdatePointView: true)
    }
    
    func removePointsPressed() {
        AnalyticsHelper.shared.triggerEvent(type: .HC05)
        self.cartCalculationData.pointApplied = !self.cartCalculationData.pointApplied
        self.cartCalculationData.applicableAmount = 0
        self.cartCalculationData.applicablePoint = 0
        self.cartTableView.reloadData {
            self.adjustTableViewHeight()
        }
    }
    
    private func getPointsFromServer(isUpdatePointView: Bool) {
        guard let dataModel = self.cartModel else {
            return
        }
        var priceInDouble = dataModel.totalPrice ?? 0.0
        if self.cartCalculationData.isCouponAdded, priceInDouble != 0.0 {
            priceInDouble -= self.cartCalculationData.couponPrice
        }
        let price = String(format: "%.0lf", priceInDouble)
        UIUtils.showLoader()
        self.cartViewModel?.getLoyaltyPointsValue(amount: price, currency: self.cartModel?.countryCode ?? "INR", completion: { (isSuccess) in
            UIUtils.hideLoader()
            
            if isSuccess {
                AnalyticsHelper.shared.triggerEvent(type: .HC04A)
                self.cartCalculationData.applicableAmount = self.cartViewModel?.getPointValueModel?.redeemAmount ?? 0.00
                
                self.pointsValueModel = self.cartViewModel?.getPointValueModel
                
                
                
                if  self.cartCalculationData.applicableAmount != 0 {
                    
                    //We have some p[oint in smilies , So it will be enabled
                    self.cartCalculationData.isPointEnabled = true
                }
                
                if isUpdatePointView, self.cartCalculationData.applicableAmount != 0 {
                    self.cartCalculationData.pointApplied = true
                }
                
                self.cartCalculationData.applicablePoint = self.cartViewModel?.getPointValueModel?.redeemablePoints ?? 0
                self.cartTableView.reloadData {
                    self.adjustTableViewHeight()
                }
            }else{
                AnalyticsHelper.shared.triggerEvent(type: .HC04B)
            }
        })
        
        var isDataAvailable = false
        if let cartDataModel = self.cartModel, let data = cartDataModel.orderItems, data.count > 0 {
            isDataAvailable = true
        }
        
        headerView.headerDelegate = self
        headerView.setUIWith(isDataAvailable: isDataAvailable)
    }
}

extension BBQMyCartViewController: BBQCartExtrasCellProtocol {
    func removeCouponsPressed() {
        AnalyticsHelper.shared.triggerEvent(type: .HC07)
        self.cartCalculationData.isCouponAdded = false
        self.cartCalculationData.couponName = ""
        self.cartCalculationData.couponBarcode = ""
        self.cartCalculationData.couponPrice = 0.0
        self.cartTableView.reloadData {
            self.adjustTableViewHeight()
        }
        AnalyticsHelper.shared.triggerEvent(type: .HC07A)
        //If point is applied, refresh points
        if self.cartCalculationData.pointApplied {
            self.getPointsFromServer(isUpdatePointView: false)
        }
    }
    
    func addCouponsPressed() {
        AnalyticsHelper.shared.triggerEvent(type: .H06)
        if self.cartCalculationData.pointApplied {
            if !self.isPointRemovalPopupShown {
                self.isPointRemovalPopupShown = true
                PopupHandler.showTwoButtonsPopup(title: kMyCartRemovePointTitle,
                                                 message: kMyCartRemovePointDesc,
                                                 on: self,
                                                 firstButtonTitle: kCancelButton, firstAction: { },
                                                 secondButtonTitle: kOkButton) {
                    self.removePointsPressed()
                    self.showCouponsBottomSheet()
                }
            } else {
                self.removePointsPressed()
                self.showCouponsBottomSheet()
            }
        } else {
            self.showCouponsBottomSheet()
        }
    }
    
    func showCouponsBottomSheet() {
        //Coupons
        let viewController = BBQAllOffersViewController()
        viewController.screenType = .Coupans
        
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: UIScreen.main.bounds.height - viewController.view.frame.size.height)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        viewController.delegate = self
        viewController.bottomSheet = bottomSheetController
        viewController.modalPresentationStyle = .fullScreen
        //bottomSheetController.present(viewController, on: self)
        self.present(viewController, animated: true, completion: nil)
    }
    
    func termsAndConditionsPressed() {
        let termsVC = UIStoryboard.loadTermsAndConditionsScreen()
        termsVC.view.layoutIfNeeded()
        termsVC.modalPresentationStyle = .overFullScreen
        if let couponsTermsData = self.couponsTerms {
            termsVC.loadTermsControllerWith(termsData: couponsTermsData)
            self.present(termsVC, animated: true, completion: nil)
        }
    }
}

extension BBQMyCartViewController: BBQCartContentCellServiceProtocol {
    @discardableResult func performEditAction(for voucherId: String, isAdd: Bool) -> Bool {
        guard let viewModel = self.cartViewModel, let model = self.cartModel else {
            return false
        }
        
        for orderItem in (model.orderItems)! {
            if orderItem.productId == Int(voucherId) {
                let orderId = String(format: "%d", model.orderId ?? "0")
                let orderItemId = String(format: "%d", orderItem.orderItemId ?? "0")
                let quantity = isAdd ? (orderItem.quantity ?? 0) + 1 : (orderItem.quantity ?? 0) - 1
                if quantity == 0{
                    AnalyticsHelper.shared.triggerEvent(type: .HC09)
                }else if isAdd{
                    AnalyticsHelper.shared.triggerEvent(type: .HC03)
                }else{
                    AnalyticsHelper.shared.triggerEvent(type: .HC08)
                }
                UIUtils.showLoader()
                if quantity == 0 {
                    viewModel.deleteOrderItem(orderId: orderId, orderItemId: orderItemId) { (isSuccess) in
                        UIUtils.hideLoader()
                        
                        if isSuccess {
                            self.performPostProcessing(viewModel: viewModel)
                            AnalyticsHelper.shared.triggerEvent(type: .HC09A)
                        }else{
                            AnalyticsHelper.shared.triggerEvent(type: .HC09B)
                        }
                    }
                } else {
                    viewModel.updateOrderItem(orderId: orderId, orderItem: orderItemId, quantity: quantity) { (isSuccess) in
                        UIUtils.hideLoader()
                        
                        if isSuccess {
                            self.performPostProcessing(viewModel: viewModel)
                            if isAdd{
                                AnalyticsHelper.shared.triggerEvent(type: .HC03A)
                            }else{
                                AnalyticsHelper.shared.triggerEvent(type: .HC08A)
                            }
                        }else{
                            if isAdd{
                                AnalyticsHelper.shared.triggerEvent(type: .HC03B)
                            }else{
                                AnalyticsHelper.shared.triggerEvent(type: .HC09B)
                            }
                        }
                    }
                }
            }
        }
        return true
    }
    
    func performPostProcessing(viewModel: CartViewModel) {
        self.cartModel = viewModel.getModifyCartModel
        self.cartTableView.reloadData {
            if let cartDataModel = self.cartModel, let data = cartDataModel.orderItems, data.count > 0, data.count < 4 {
                self.adjustTableViewHeight()
            }
        }
        //self.getPointsFromServer(isUpdatePointView: false)
        
        if let cartDataModel = self.cartModel, let data = cartDataModel.orderItems, data.count > 0 {
            self.noDataLabel.isHidden = true
            // self.cartTableView.separatorStyle = .singleLine
            self.cartTableView.isHidden = false
        } else {
            self.noDataLabel.isHidden = false
            self.cartTableView.separatorStyle = .none
            self.cartTableView.isHidden = true
            self.navigationController?.popViewController(animated: true)
        }
        
        if let delegate = self.delegate {
            delegate.updateCartData()
        }
    }
    
    func itemQuatityDidAdd(for voucherId: String, cellIndex: Int) {
        self.checkCouponsApplied(for: voucherId, isAdd: true)
    }
    
    func itemQuatityDidSubtract(for voucherId: String, cellIndex: Int, quantity: Int) {
        self.checkCouponsApplied(for: voucherId, isAdd: false)
    }
    
    func checkCouponsApplied(for voucherId: String, isAdd: Bool) {
        if self.cartCalculationData.isCouponAdded {
            if !self.isCouponRemovalPopupShown {
                self.isCouponRemovalPopupShown = true
                PopupHandler.showTwoButtonsPopup(title: kMyCartRemoveCouponTitle,
                                                 message: kMyCartRemoveCouponDesc,
                                                 on: self,
                                                 firstButtonTitle: kCancelButton, firstAction: { },
                                                 secondButtonTitle: kOkButton) {
                    self.removeCouponsPressed()
                    self.addOrRemoveVoucher(for: voucherId, isAdd: isAdd)
                }
            } else {
                self.removeCouponsPressed()
                self.addOrRemoveVoucher(for: voucherId, isAdd: isAdd)
            }
        } else {
            self.addOrRemoveVoucher(for: voucherId, isAdd: isAdd)
        }
    }
    
    func addOrRemoveVoucher(for voucherId: String, isAdd: Bool) {
        self.performEditAction(for: voucherId, isAdd: isAdd)
    }
}

extension BBQMyCartViewController: BBQAllOffersProtocol, BBQMyCartHeaderViewProtocol {
    
    func didPressedEmptyCartButton() {
        PopupHandler.showTwoButtonsPopup(title: "Empty Cart", message: kEmptyCartWarning,
                                         on: self, firstButtonTitle: kCancelButton, firstAction: {},
                                         secondButtonTitle: kButtonOkay) {
            self.clearAllItemsinCart()
        }
    }
    
    func selectedCorporateOffer(corporateOffer: CorporateOffer) {
        
    }
    
    func addButtonClickedAt(index: Int) {
        AnalyticsHelper.shared.triggerEvent(type: .HC06A)
        self.cartCalculationData.isCouponAdded = true
        self.cartCalculationData.couponName = self.couponsArray[index].title ?? "No Name"
        self.cartTableView.reloadData {
            self.adjustTableViewHeight()
        }
    }
    
    func buyNowButtonClicked(bottomSheet: BottomSheetController?) { }
    
    func userVoucherSelectedWith(voucherList:[VoucherCouponsData], type: ScreenType,termsAndConditions:[String]) {
        
        self.cartCalculationData.isCouponAdded = true
        self.cartCalculationData.couponName = voucherList[0].title ?? "No Name"
        self.cartCalculationData.couponBarcode = voucherList[0].barcode ?? ""
        self.cartCalculationData.couponPrice = voucherList[0].denomination ?? 0.0
        self.couponsTerms = termsAndConditions
        self.cartTableView.reloadData {
            self.adjustTableViewHeight()
        }
    }
    
    private func clearAllItemsinCart()  {
        AnalyticsHelper.shared.triggerEvent(type: .HC02)
        if let cartOrderID = cartModel?.orderId {
            UIUtils.showLoader()
            self.cartViewModel?.emptyCartFor(cartID: "\(cartOrderID)", completion: { (isEmptied) in
                UIUtils.hideLoader()
                if isEmptied {
                    self.cartModel = nil
                    AnalyticsHelper.shared.triggerEvent(type: .HC02A)
                    // Copied from when no cart scenario
                    self.noDataLabel.isHidden = false
                    self.cartTableView.separatorStyle = .none
                    self.cartTableView.isHidden = true
                    self.navigationController?.popViewController(animated: true)
                    
                    if let delegate = self.delegate {
                        delegate.updateCartData()
                    }
                    
                } else {
                    AnalyticsHelper.shared.triggerEvent(type: .HC02B)
                    ToastHandler.showToastWithMessage(message: "Something went wrong! try again")
                }
            })
        }
    }
}

extension BBQMyCartViewController : BBQBookingAmountDetailsTableViewCellDelegate,
                                        BBQBookingPointsTableViewCellDelegate,
                                    BBQCouponsTableViewCellDelegate, BBQApplyVouchersTableViewCellDelegate, BBQCorpOffersTableViewCellDelegate {
    
    
    // MARK: Private Methods
    
    //        func adjustBottomSheetComponent() {
    //
    ////            self.tableView.beginUpdates()
    ////            self.tableView.endUpdates()
    ////
    ////            let modifiedHeight = self.tableView.contentSize.height + 180
    ////            self.expandCollapseBottomSheet(newHeight: CGFloat(modifiedHeight), isExpand: true)
    //        }
    
    
    
    
    
    //    func getTaxAmountValue(with taxPercentage: Double) -> Double {
    //        return self.bookingAmountSubTotal * (taxPercentage/100)
    //    }
    
    func didPressedCouponsTermsAndConditions() {
        let termsVC = UIStoryboard.loadTermsAndConditionsScreen()
        termsVC.view.layoutIfNeeded()
        termsVC.modalPresentationStyle = .overFullScreen
        if let couponsTermsData = self.couponsTerms {
            termsVC.loadTermsControllerWith(termsData: couponsTermsData)
            self.present(termsVC, animated: true, completion: nil)
        }
    }
    
    func didPressedCorpOffersAddRemoveButton(isAdded: Bool) {
        
        if isAdded {
            AnalyticsHelper.shared.triggerEvent(type: .H06)
            guard let dataModel = self.cartModel else {
                return
            }
            self.showSmilesResetWarning { (doReset) in
                if doReset {
                    let finalAmount = (dataModel.totalPrice ?? 0.0) - self.cartCalculationData.couponPrice
                    //To check for user has logged in or not
                    if BBQUserDefaults.sharedInstance.isGuestUser {
                        // self.navigateToLogin()
                    } else {
                        self.showCorporateOffersController()
                    }
                }
            }
            
        } else {
            
            self.isTaxSplitRefreshNeeded = true
            
        }
    }
    
    //        internal func showCalendarBottomSheet() {
    //            let viewController = BottomSheetCalendarViewController()
    //            viewController.delegate = self
    //            let configuration = BottomSheetConfiguration(animationDuration: 0.4,
    //                                                         minimumTopSpacing: UIScreen.main.bounds.height - viewController.view.frame.size.height)
    //            let bottomSheetController = BottomSheetController(configuration: configuration)
    //
    //            bottomSheetController.present(viewController, on: self)
    //        }
    
    internal func showAllOffersController(type: ScreenType) {
        if BBQUserDefaults.sharedInstance.isGuestUser {
            //  self.navigateToLogin()
        } else {
            //call vouchers and coupons
            let viewController = BBQAllOffersViewController()
            viewController.screenType = type
            if type == .CouponsForYou {
                // Manual coupon code entry only for copons.
                viewController.doManualCoupon = true
            }
            let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: UIScreen.main.bounds.size.height - viewController.view.frame.size.height)
            let bottomSheet = BottomSheetController(configuration: configuration)
            viewController.delegate = self
            
            viewController.bottomSheet = bottomSheet
            //            bottomSheet.present(viewController, on: self)
            viewController.modalPresentationStyle = .fullScreen
            self.present(viewController, animated: true, completion: nil)
        }
    }
    
    func showCorporateOffersController() {
        //            let corpVC = BBQBottomSheetCorporateViewController()
        //            corpVC.componentTitle = kCorporateOffers
        //            corpVC.componentTitleLogo = UIImage(named:Constants.AssetName.corporateIcon)
        //            corpVC.buffetsViewModel = self.buffetsViewModel
        //            corpVC.emailType = BBQEmailType.corporate
        //          //  corpVC.validatorDelegate = self
        //            corpVC.modalPresentationStyle = .fullScreen
        //            let configuration = BottomSheetConfiguration(animationDuration: 0.4,
        //                                                         minimumTopSpacing: UIScreen.main.bounds.size.height - corpVC.view.frame.size.height)
        //            let bottomSheetController = BottomSheetController(configuration: configuration)
        //            self.corpBottomSheet = bottomSheetController
        //            self.present(corpVC, animated: true, completion: nil)
        //bottomSheetController.present(corpVC, on: self)
    }
    func didPressedTermsAndConditions() {
        //        let termsVC = UIStoryboard.loadTermsAndConditionsScreen()
        //        termsVC.view.layoutIfNeeded()
        //        termsVC.modalPresentationStyle = .overFullScreen
        //        if let offerData = self.appliedCorpOffers {
        //            termsVC.loadTermsControllerWith(termsData: offerData.corporateTerms)
        //        }
        //        self.present(termsVC, animated: true, completion: nil)
    }
    
    func getTaxBreakUpModel() -> [TaxBreakUp]{
        var taxBreakups = [TaxBreakUp]()
        
        guard let dataModel = self.cartModel else {
            return []
        }
        
        if dataModel.taxes != nil {
            var totalValue = 0
            
            
            for item in dataModel.taxes! {
                //(format: "%.2f", self.getTaxAmountValue(with: item.taxPercentage ?? 0.0))
                
                let tax = TaxBreakUp(label: item.tax ?? "", value: String(format: "%.2f", item.taxPercentage ?? 0.0))
                taxBreakups.append(tax)
                totalValue += Int(item.taxPercentage ?? "0") ?? 0/100
            }
            let tax = TaxBreakUp(label: kDiningServiceCharge, value: String(format: "%.2f", totalValue ))
            taxBreakups.append(tax)
            return taxBreakups
        }
        return taxBreakups
    }
    
    
    func didClickOnDetails(cell: BBQAmountEntryTableViewCell) {
        
        let popupVC = BillDetailsPopVC(nibName: "BillDetailsPopVC", bundle: nil)
        popupVC.modalPresentationStyle = UIModalPresentationStyle .popover
        popupVC.preferredContentSize = CGSize(width: 170, height: 130)
        popupVC.strTitle = kDiningTaxServiceCharge
        popupVC.taxBreakUp = getTaxBreakUpModel()
        popupVC.view.frame.size.width = 250 > self.view.frame.size.width ? self.view.frame.size.width : 250
        popupVC.view.frame.size.height = CGFloat(60 + (getTaxBreakUpModel().count * 26))
        var originFrame = cell.valueLabel.frame
        originFrame.size.width = 40
        originFrame.origin.x = originFrame.origin.x + 35
        let frame = cell.convert(originFrame, to: self.view)
        popTip.show(customView: popupVC.view, direction: .auto, in: self.view, from: frame)
        
    }
    
    
    
    
    
    func didPressOpenBill(with recieptURL: String) {
        //        if let url = URL(string: recieptURL) {
        //            UIApplication.shared.open(url)
        //        }
    }
    
    
    func didPressedVoucherAddRemoveButton(isAdd: Bool) {
        //        if isAdd {
        //            AnalyticsHelper.shared.triggerEvent(type: .AP06)
        //            self.showSmilesResetWarning { (doReset) in
        //                if doReset {
        //                    self.showAllOffersController(type: .VouchersForYou)
        //                }
        //            }
        //        } else {
        //            AnalyticsHelper.shared.triggerEvent(type: .AP07)
        //            self.didAddVouchers = false
        //            self.appliedVouchers?.removeAll()
        //            self.afterDiningViewModel.adjustTotalAmountForPayment(with: self.appliedVoucherAmount,
        //                                                            isAddition: false,
        //                                                            offerType: .Vouchers)
        //            let smilesIndex = IndexPath(row: 1, section: 0)
        //            let couponIndex = IndexPath(row: 2, section: 0)
        //            let voucherIndex = IndexPath(row: 3, section: 0)
        //            let paymentIndex = self.appliedCorpOffers == nil ? IndexPath(row: 4, section: 0) :
        //                IndexPath(row: 5, section: 0)
        //            self.cartTableView.reloadRows(at: [smilesIndex, couponIndex, voucherIndex, paymentIndex], with: .none)
        //            self.adjustBottomSheetComponent()
        //        }
    }
    
    func didPressedVouchersTermsAndConditions() {
        let termsVC = UIStoryboard.loadTermsAndConditionsScreen()
        termsVC.view.layoutIfNeeded()
        termsVC.modalPresentationStyle = .overFullScreen
        if let vouchersTermsData = self.vouchersTerms {
            termsVC.loadTermsControllerWith(termsData: vouchersTermsData)
            self.present(termsVC, animated: true, completion: nil)
        }
    }
    
    func didPressedAddRemoveCouponButton(isAdd: Bool) {
        if isAdd {
            AnalyticsHelper.shared.triggerEvent(type: .AP04)
            self.showSmilesResetWarning { (doReset) in
                if doReset {
                    self.showAllOffersController(type: .Coupans)
                }
            }
        } else {
            AnalyticsHelper.shared.triggerEvent(type: .AP05)
            self.didAddCoupons = false
            self.cartTableView.reloadData()
            self.adjustBottomSheetComponent()
        }
    }
    
    
    
    
    func didPressedPointsAddRemoveButton(isAdd: Bool) {
        if isAdd {
            AnalyticsHelper.shared.triggerEvent(type: .AP02)
            self.didAddPoints = true
            self.getPointsFromServer(isUpdatePointView: isAdd)
            
        } else {
            AnalyticsHelper.shared.triggerEvent(type: .AP03)
            self.didAddPoints = false
            
            AnalyticsHelper.shared.triggerEvent(type: .HC05)
            self.cartCalculationData.pointApplied = !self.cartCalculationData.pointApplied
            self.cartCalculationData.applicableAmount = 0
            self.cartCalculationData.applicablePoint = 0
            cartTableView.reloadData()
            self.adjustBottomSheetComponent()
        }
    }
    
    func contentDidChange(cell: BBQBookingAmountDetailsTableViewCell, contentHeight: CGFloat) {
        //  self.amountDetailsHeight = contentHeight
        self.adjustBottomSheetComponent()
    }
    
    // MARK: Private Methods
    
    func adjustBottomSheetComponent() {
        
        self.cartTableView.beginUpdates()
        self.cartTableView.endUpdates()
    }
    
    func resetCards() {
        
        self.didAddCoupons = false
        self.didAddVouchers = false
        self.didAddCorpOffers = false
        self.didAddPoints = false
        
        self.appliedPoints = 0
        self.appliedPointsAmount = 0.00
        self.pointsValueModel = nil
        
        
        self.appliedCoupons?.removeAll()
        self.appliedVouchers?.removeAll()
        self.appliedCorpOffers?.removeAll()
    }
    
    private func showSmilesResetWarning(completion: @escaping (_ doReset: Bool)->()) {
        if !self.isPointsResetWarned && self.didAddPoints {
            PopupHandler.showTwoButtonsPopup(title: "Applied Smiles will be removed",
                                             message: "All applied Smiles will be removed if you apply any coupon/happiness card/ corporate offers on top of Smiles. Please re -apply Smiles again.",
                                             on: self, firstButtonTitle: "Cancel",
                                             firstAction: {
                completion(false)
                
            }, secondButtonTitle: "OK", secondAction: {
                completion(true)
            })
            self.isPointsResetWarned = true
        } else {
            completion(true)
        }
        
    }
    
    private func resetPoints() {
        if didAddPoints {
            self.didAddPoints = false
            //            self.afterDiningViewModel.adjustTotalAmountForPayment(with: self.appliedPointsAmount,
            //                                                            isAddition: false,
            //                                                            offerType: .Smiles)
        }
    }
    
}

extension BBQMyCartViewController: BBQCustomPaymentViewControllerDelegate, RazorpayPaymentCompletionProtocol{
    func onPaymentSuccess(_ payment_id: String, andData response: [AnyHashable : Any]) {
        self.navigationController?.popToViewController(self, animated: true)
        
        AnalyticsHelper.shared.triggerEvent(type: .HC10A)
        
        
//        let paymentGateway = Constants.BBQBookingStatus.razorPayment
        
        UIUtils.showLoader()
        let data = getAppliedDiscountAndPoints()
        paymentControllerViewModel.paymentCheckout(points: data.points, barCode: self.cartCalculationData.couponBarcode, razorpayPaymentId: response["razorpay_payment_id"] as? String ?? "", razorpayOrderId: response["razorpay_order_id"] as? String ?? "", razorpaySignature: response["razorpay_signature"] as? String ?? "", completion: { isSuccess in
            UIUtils.hideLoader()

            if isSuccess {
                //Show Payment Success
                AnalyticsHelper.shared.triggerEvent(type: .HC11A)
                self.showPaymentSuccessScreen()
                AnalyticsHelper.shared.triggerEvent(type: .Happinesscard_Order_Status, properties: [.Happiness_Card_Success: AnalyticsHelper.MoEngageAttrValue.Success.rawValue])
            } else {
                //Show Payment Failed error
                AnalyticsHelper.shared.triggerEvent(type: .HC11B)
                self.showTransactionFailed()
                AnalyticsHelper.shared.triggerEvent(type: .Happinesscard_Order_Status, properties: [.Happiness_Card_Success: AnalyticsHelper.MoEngageAttrValue.Failed.rawValue, .Error: kMyCartTransactionFailedMessage])
            }
        })
    }
    
    @objc func showPaymentSuccessScreen() {
        self.delegate?.updateCartData()
        self.navigationController?.popToViewController(self, animated: true)
        
        let registrationConfirmationVC = UIStoryboard.loadRegistrationConfirmation()
        registrationConfirmationVC.modalPresentationStyle = .overFullScreen
        registrationConfirmationVC.titleText = kPaymentConfirmationTitleText
        registrationConfirmationVC.navigationText = kVoucherPurchase
        
        self.present(registrationConfirmationVC, animated: true) {
            sleep(UInt32(2.0))
            self.dismiss(animated: true) {
                if let superVC = self.superVC{
                    let bookingHappinesscardController = UIStoryboard.loadHappinesscardViewController()
                    bookingHappinesscardController.isComingFromNotification = false
                    bookingHappinesscardController.superVC = superVC
                    superVC.navigationController?.pushViewController(bookingHappinesscardController, animated: true)
                }else{
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    private func showTransactionFailed() {
        self.navigationController?.popToViewController(self, animated: true)
        
        let keyWindow = UIApplication.shared.keyWindow?.rootViewController?.presentedViewController ?? UIApplication.shared.keyWindow?.rootViewController
        
        guard let window = keyWindow else {
            return
        }
        
        AnalyticsHelper.shared.triggerEvent(type: .Happinesscard_Order_Status, properties: [.Happiness_Card_Success: AnalyticsHelper.MoEngageAttrValue.Failed.rawValue, .Error: kMyCartTransactionFailedMessage])
        
        PopupHandler.showSingleButtonsPopup(title: kMyCartTransactionFailedTitle,
                                            message: kMyCartTransactionFailedMessage,
                                            on: window,
                                            firstButtonTitle: kButtonOkay) { }
    }
    
    
    func onPaymentError(_ code: Int32, description str: String, andData response: [AnyHashable : Any]) {
        self.navigationController?.popToViewController(self, animated: true)
        AnalyticsHelper.shared.triggerEvent(type: .HC10B)
        switch code {
        case 0,1,3:
            PopupHandler.showSingleButtonsPopup(title: kFailedTitle,
                                                message: str,
                                                on: self, firstButtonTitle: kOkButtonTitle) { }
        case 2:
            self.showTransactionCancelled()
            
        default:
            print("");
        }
    }
    
    func showTransactionCancelled() {
        PopupHandler.showSingleButtonsPopup(title: kMyCartTransactionCancelTitle,
                                            message: kMyCartTransactionCancelMessage,
                                            on: self,
                                            firstButtonTitle: kButtonOkay) { }
    }
    
    func createRazorPayPaymentId(paymentVC: BBQCustomPaymentViewController, completion: @escaping (WKWebView, [String : Any]?, String?, Razorpay?, String?) -> Void) {
        
        let data = getAppliedDiscountAndPoints()
        
        self.paymentControllerViewModel.createCartPaymentOrder(discount: data.discount, loyalty_points: data.discount, bar_code: self.cartCalculationData.couponBarcode) { orderID in
            // Procceed only for valid order id
            guard orderID.count > 0 else {
                self.navigationController?.popToViewController(self, animated: true)
                Timer.scheduledTimer(withTimeInterval: 0.75, repeats: false) { timer in
                    self.onPaymentError(3, description: kRazorpayCrateOrderFailedDescription, andData: [:])
                    timer.invalidate()
                }
                return
            }
            
            let data = self.getWebViewPaymentId(paymentVC: paymentVC, order_id: orderID)
            completion(data.webView, data.options, orderID, self.razorpay, "")
        }
        
    }
}
