//
 //  Created by Chandan Singh on 14/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQMyCartSubTotalCell.swift
 //

import UIKit

class BBQMyCartSubTotalCell: UITableViewCell {
    //MARK: Outlets
    @IBOutlet weak var subTotalTextLabel: UILabel!
    @IBOutlet weak var subTotalPriceLabel: UILabel!
    @IBOutlet weak var viewForContainer: UIView!

    //MARK: Life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewForContainer.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.bottomCornersMask)
        self.viewForContainer.dropShadow()
        
    }
    
    //MARK: UI Methods
    func setupUIWith(price: String) {
        self.subTotalTextLabel.text = kMyCartSubTotalText
        self.subTotalPriceLabel.text = price.toCurrencyFormat()
    }
}
