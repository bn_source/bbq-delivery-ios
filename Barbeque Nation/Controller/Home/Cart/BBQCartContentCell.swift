//
 //  Created by Chandan Singh on 14/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQCartContentCell.swift
 //

import UIKit

protocol BBQCartContentCellServiceProtocol: AnyObject {
    func itemQuatityDidAdd(for voucherId: String, cellIndex: Int)
    func itemQuatityDidSubtract(for voucherId: String, cellIndex: Int, quantity: Int)
}

class BBQCartContentCell: UITableViewCell {
    //MARK: Outlets
    @IBOutlet weak var voucherNameLabel: UILabel!
    @IBOutlet weak var voucherUnitPriceLabel: UILabel!
    @IBOutlet weak var voucherTotalPriceLabel: UILabel!
    @IBOutlet weak var offerOrderUnitView: BBQAllOffersOrderUnit!
    @IBOutlet weak var mainView : UIView!
    weak var serviceDelegate: BBQCartContentCellServiceProtocol? = nil
    
    private var orderItems: ModifyOrderItems? = nil
    
    //MARK: Life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        //offerOrderUnitView.delegate = self
        self.offerOrderUnitView.serviceDelegate = self
        self.setupUI()
    }
    
    //MARK: setup
    func setupUI() {
//        self.offerOrderUnitView.roundCorners(cornerRadius: 4, borderColor: UIColor.hexStringToUIColor(hex: "#F04B24"), borderWidth: 0.0)
        
        offerOrderUnitView.setCornerRadius(5)
        offerOrderUnitView.setShadow(color: UIColor.darkGray , opacity: 0.2, offSet: .zero, radius: 5, scale: true)
    }
    
    func configureCellFor(orderItem: ModifyOrderItems) {
        self.orderItems = orderItem
        
        self.voucherNameLabel.text = orderItem.itemTitle
        
        let unitPrice = String(format: "%.0f", orderItem.unitPrice ?? 0.0)
        self.voucherUnitPriceLabel.text = unitPrice.toCurrencyFormat()
        
        let totalPrice = String(format: "₹ %.0f", orderItem.totalPrice ?? 0.0)
        self.voucherTotalPriceLabel.text = totalPrice.toCurrencyFormat()
        
        self.offerOrderUnitView.setupUI(withQuatity: orderItem.quantity ?? 0)
    }
    
    func updateQuantity(quantity: Int) {
        self.offerOrderUnitView.setupUI(withQuatity: quantity)
    }
}

extension BBQCartContentCell: BBQAllOffersServiceProtocol {
    func itemQuatityDidAdd() {
        guard let delegate = self.serviceDelegate else {
            return
        }
        let voucherId = String(format: "%d", self.orderItems?.productId ?? 0)
        delegate.itemQuatityDidAdd(for: voucherId, cellIndex: self.tag)
    }
    
    func itemQuatityDidSubtract(quantity: Int) {
        guard let delegate = self.serviceDelegate else {
            return
        }
        let voucherId = String(format: "%d", self.orderItems?.productId ?? 0)
        delegate.itemQuatityDidSubtract(for: voucherId, cellIndex: self.tag, quantity: quantity)
    }
}
