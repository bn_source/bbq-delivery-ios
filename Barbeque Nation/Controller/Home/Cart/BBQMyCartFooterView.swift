//
 //  Created by Chandan Singh on 14/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQMyCartFooterView.swift
 //

import UIKit

protocol BBQMyCartFooterViewProtocol: AnyObject {
    func paymentButtonPressed()
}

class BBQMyCartFooterView: UIView {
    //MARK: Outlets
    @IBOutlet weak var priceTermsLabel: UILabel!
    @IBOutlet weak var paymentButton: UIButton!
    
    //MARK: Properties
    var currencyString: String = "INR"
    weak var delegate: BBQMyCartFooterViewProtocol? = nil
    
    //MARK: Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    //MARK: UI Methods
    func setupUI() {
        self.priceTermsLabel.text = String(format: "%@ %@", kMyCartPriceTermsText, currencyString)
        self.paymentButton.setTitle(kMyCartPaymentButtonText, for: .normal)
        self.paymentButton.roundCorners(cornerRadius: self.paymentButton.frame.size.height/2, borderColor: .clear, borderWidth: 0.0)
    }
    
    @IBAction func paymentButtonPressed(_ sender: UIButton) {
        if let delegate = self.delegate {
            delegate.paymentButtonPressed()
        }
    }
}
