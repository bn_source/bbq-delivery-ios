//
 //  Created by Chandan Singh on 12/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQAllOffersOrderUnit.swift
 //

import UIKit

protocol BBQAllOffersServiceProtocol: AnyObject {
    func itemQuatityDidAdd()
    func itemQuatityDidSubtract(quantity: Int)
}

class BBQAllOffersOrderUnit: UIView {
    
    //MARK: Outlets
    @IBOutlet weak var unitLabel: UILabel!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var plusButton: UIButton!
    
    private var itemQuatity: Int = 0 
    
    weak var serviceDelegate: BBQAllOffersServiceProtocol? = nil

    //MARK: Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        plusButton.setCornerRadius(plusButton.frame.width * 0.15)
//        plusButton.setShadow()
        self.backgroundColor = UIColor.white
        plusButton.titleLabel?.textColor = UIColor.theme
        minusButton.titleLabel?.textColor =  UIColor.theme
        unitLabel.textColor = UIColor.theme


    }
    
    //MARK: UI
    func setupUI(withQuatity: Int) {
        self.itemQuatity = withQuatity
        self.unitLabel.text = "\(withQuatity)"
    }
    
    //MARK: Actions
    @IBAction func minusButtonPressed(_ sender: UIButton) {
        guard let delegate = self.serviceDelegate else {
            return
        }
        delegate.itemQuatityDidSubtract(quantity: self.itemQuatity - 1)
    }
    
    @IBAction func plusButtonPressed(_ sender: UIButton) {
        guard let delegate = self.serviceDelegate else {
            return
        }
        delegate.itemQuatityDidAdd()
    }
}
