    //
    //  Created by Maya R on 22/09/19
    //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
    //  All rights reserved.
    //  Last modified BBQAllOffersHeaderView.swift
    //
    
import UIKit
      
class BBQAllOffersFooterView: UIView {
        
@IBOutlet weak var doneButton:UIButton!
@IBOutlet weak var footerTitleLabel:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        footerTitleLabel.text = kkVouchersAndCouponsBottomText
        doneButton.roundCorners(cornerRadius: 5.0, borderColor: .orange, borderWidth: 2.0)
    }
    
   
}
