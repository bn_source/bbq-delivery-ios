//
 //  Created by Chandan Singh on 12/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQAllOffersAmountSummary.swift
 //

import UIKit

protocol BBQAllOffersAmountSummaryProtocol: AnyObject {
    func buyNowClicked()
}

class BBQAllOffersAmountSummary: UIView {
    
    //MARK: IBOutlets
    @IBOutlet weak var itemCountLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var taxLabel: UILabel!
    @IBOutlet weak var buyNowButton: UIButton!
    
    weak var delegate: BBQAllOffersAmountSummaryProtocol? = nil
    
    //MARK: Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupInitialUI()
    }
    
    //MARK: UI methods
    private func setupInitialUI() {
        self.taxLabel.text = kTaxInfoText
        self.buyNowButton.roundCorners(cornerRadius: buyNowButton.frame.size.height/2, borderColor: .clear, borderWidth: 0.0)
        self.buyNowButton.setTitle(kBuyNowButtonText, for: .normal)
    }
    
    func setUIElements(itemCount: Int, price: Double) {
        self.priceLabel.text = String(format: "₹%.0lf", price)
        
        if itemCount == 1 {
            self.itemCountLabel.text = "\(itemCount) " + kSingleItemText
        } else {
            self.itemCountLabel.text = "\(itemCount) " + kMultipleItemText
        }
    }
    
    //MARK: Action
    @IBAction func buyNowPressed(_ sender: UIButton) {
        if let delegate = self.delegate {
            delegate.buyNowClicked()
        }
    }
}
