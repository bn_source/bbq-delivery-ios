//
//  Created by Chandan Singh on 03/09/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQAllOffersViewController.swift
//

import UIKit

enum ScreenType {
    case Vouchers, Coupans, CorporateOffers, VouchersForYou, CouponsForYou, PromotionsOffers
}

protocol BBQAllOffersProtocol: AnyObject {
    //    func test(voucherList:[VoucherCouponsData], type: ScreenType,termsAndConditions:[String])
    func cartUpdated()
    func addButtonClickedAt(index: Int)
    func buyNowButtonClicked(bottomSheet: BottomSheetController?)
    func userVoucherSelectedWith(voucherList:[VoucherCouponsData], type: ScreenType,termsAndConditions:[String])
    func selectedCorporateOffer(corporateOffer:CorporateOffer)
    
}

extension BBQAllOffersProtocol {
    //Default Implementation. So that methods become optional.
    
    func cartUpdated() { }
    func addButtonClickedAt(index: Int) { }
    func buyNowButtonClicked(bottomSheet: BottomSheetController) { }
    func userVoucherSelectedWith(voucherList:[VoucherCouponsData], type: ScreenType,termsAndConditions:[String]) { }
    func selectedCorporateOffer(corporateOffer:CorporateOffer){}
}

class BBQAllOffersViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet var containerView: UIView!
    @IBOutlet weak var corporateOffersListView:UITableView!
    @IBOutlet weak var amountSummaryView: BBQAllOffersAmountSummary!
    @IBOutlet weak var amountSummaryViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var noDataLabel: UILabel!
    @IBOutlet weak var voucherBottomView: UIView!
    @IBOutlet weak var voucherDoneButton: UIButton!
    @IBOutlet weak var applyCouponButton: UIButton!
    
    @IBOutlet weak var applyCouponField: UITextField!
    @IBOutlet weak var couponCodeCompoHeightConstraint: NSLayoutConstraint!
    
    private let detailScreenNib = Constants.CellIdentifiers.voucherDetailsControllerNib
    private var itemsQuantity = [Int]()
    lazy var vouchersArray = [VouchersModel]()
    lazy var couponsArray = [VoucherCouponsData]()
    lazy var corporateCouponsArray = [CorporateOffer]()
    lazy var userVouchersArray = [VoucherCouponsData]()
    lazy var userCouponsArray = [VoucherCouponsData]()
    
    var selectedIndexPaths = NSMutableSet()
    var doManualCoupon : Bool = false
    
    var coversValue:Int = 0
    var corporateEmailID: String = ""
    var bottomSheet: BottomSheetController? = nil
    var screenType: ScreenType = .Vouchers
    var isAfterDiningScreen = false
    weak var delegate: BBQAllOffersProtocol? = nil
    private var termsAndCondtions:[String] = []
    
    private var isUserSelectedVoucher = false
    lazy var couponVoucherModel : VouchersCouponsViewModel = {
        let viewModel = VouchersCouponsViewModel()
        return viewModel
    }()
    
    lazy var cartViewModel : CartViewModel = {
        let viewModel = CartViewModel()
        return viewModel
    }()
    
    lazy var corporateViewModel : CorporateBookingViewModel = {
        let viewModel = CorporateBookingViewModel()
        return viewModel
    }()
    
    var buffetsViewModel: BBQBookingBuffetsViewModel?
    
    // MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setupManualCouponCodeComponent()
        
        self.noDataLabel.isHidden = true
        self.corporateOffersListView.isHidden = false
        self.voucherBottomView.isHidden = true
        BBQUserDefaults.sharedInstance.bookingEstimatedTotal = 0.0
    }
    
    override func viewWillLayoutSubviews() {
        //self.view.frame = CGRect(x: self.view.frame.origin.x, y: UIScreen.main.bounds.size.height - 535.0, width: UIScreen.main.bounds.size.width, height: 535.0)
    }
    
    // MARK: - Setup Methods
    
    private func setupManualCouponCodeComponent() {
        if doManualCoupon {
            couponCodeCompoHeightConstraint.constant = 40
            applyCouponButton.isHidden = false
        } else {
            couponCodeCompoHeightConstraint.constant = 0
            applyCouponButton.isHidden = true
        }
    }
    
    private func setupUI() {
        
    
        if doManualCoupon {
            self.applyCouponField.delegate = self
            let outerTap = UITapGestureRecognizer(target: self, action: #selector(self.outerViewTapped(_:)))
            view.addGestureRecognizer(outerTap)
        }
        
        self.containerView.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        
        let customCell = UINib(nibName: Constants.NibName.corporateOffersCellNibName, bundle: nil)
        self.corporateOffersListView.register(customCell, forCellReuseIdentifier: Constants.CellIdentifiers.corporateOffersCell)
        
        
        corporateOffersListView.register(UINib(nibName: "BBQVoucherForDiningTableViewCell", bundle: nil), forCellReuseIdentifier: "BBQVoucherForDiningTableViewCell")
        self.amountSummaryViewHeightConstraint.constant = 0
        self.amountSummaryViewHeightConstraint.constant = 0
        
        self.amountSummaryView.delegate = self
        
        self.setupHeaderTitle()
        self.initializeInitialItemQuatity()
        //To get the vouchers
        if self.screenType == .Coupans {
            BBQActivityIndicator.showIndicator(view: self.view)
            self.cartViewModel.getVoucherAndCoupons(completion: { (isSuccess) in
                BBQActivityIndicator.hideIndicator(from: self.view)
                if isSuccess {
                    if (self.couponsArray.count > 0) {
                        self.couponsArray.removeAll()
                    }
                    self.termsAndCondtions = (self.cartViewModel.vouchersAndCouponsModel?.voucherTermsAndConditions!)!

                    if self.cartViewModel.getActiveCoupons.count > 0 {
                        self.couponsArray = self.cartViewModel.getActiveCoupons
                        self.noDataLabel.isHidden = true
                        self.corporateOffersListView.isHidden = false
                        self.corporateOffersListView.reloadData()
                    } else {
                        self.noDataLabel.isHidden = false
                        self.noDataLabel.text = kNoCouponsAvailable
                        self.corporateOffersListView.isHidden = true
                    }
                }
            })
        } else if self.screenType == .VouchersForYou {
            self.corporateOffersListView.allowsSelection = true
            BBQActivityIndicator.showIndicator(view: self.view)
            self.couponVoucherModel.getAllTheVouchersForBooking{
                (isSuccess)in
                BBQActivityIndicator.hideIndicator(from: self.view)
                if(isSuccess){
                    if let model = self.couponVoucherModel.vouchersAndCouponsModel{
                        if(self.couponsArray.count>0){
                            self.couponsArray.removeAll()
                        }
                        if model.couponsAndVouchers != nil && model.couponsAndVouchers!.count>0{
                            self.couponsArray = model.couponsAndVouchers!
                            self.noDataLabel.isHidden = true
                            self.termsAndCondtions = model.voucherTermsAndConditions!
                            self.corporateOffersListView.isHidden = false
                            
                            if let buffmodel = self.buffetsViewModel, let bookingData = buffmodel.bookingDataForCorporateOffers {
                                BBQUserDefaults.sharedInstance.bookingEstimatedTotal = bookingData.totalAmount
                            }
                            self.corporateOffersListView.reloadData()
                        }else{
                            self.noDataLabel.isHidden = false
                            self.noDataLabel.text = kNoVoucherAvailable
                            self.corporateOffersListView.isHidden = true
                        }
                    }
                }else{
                    self.noDataLabel.isHidden = false
                    self.noDataLabel.text = kNoVoucherAvailable
                    self.corporateOffersListView.isHidden = true
                }
            }
        }else if self.screenType == .CouponsForYou{
            self.corporateOffersListView.allowsSelection = false
            
            BBQActivityIndicator.showIndicator(view: self.view)
            if let buffmodel = self.buffetsViewModel, let bookingData = buffmodel.bookingDataForCorporateOffers  {
                self.couponVoucherModel.getAllTheCouponsForBooking(noOfPack:bookingData.packs,
                                                                   amount:bookingData.totalAmount,
                                                                   branchID:bookingData.branchId,
                                                                   reservationDate:bookingData.reservationDate,
                                                                   slotID:bookingData.slotId,
                                                                   currency:bookingData.currency){
                                                                    (isSuccess)in
                                                                    BBQActivityIndicator.hideIndicator(from: self.view)
                                                                    if isSuccess{
//                                                                         self.corporateOffersListView.delegate = self
                                                                        self.screenType = .CouponsForYou
                                                                        if let model = self.couponVoucherModel.vouchersAndCouponsModel{
                                                                            if(self.couponsArray.count>0){
                                                                                self.couponsArray.removeAll()
                                                                            }
                                                                            self.termsAndCondtions = model.voucherTermsAndConditions!
                                                                            if model.couponsAndVouchers != nil && model.couponsAndVouchers!.count>0{
                                                                                self.couponsArray = model.couponsAndVouchers!
                                                                                self.noDataLabel.isHidden = true
                                                                                self.corporateOffersListView.isHidden = false
                                                                                self.corporateOffersListView.reloadData()
                                                                            }else{
                                                                                self.noDataLabel.isHidden = false
                                                                                self.noDataLabel.text = kNoCouponsAvailable
                                                                                self.corporateOffersListView.isHidden = true

                                                                            }
                                                                        }
                                                                    }else{
                                                                        self.noDataLabel.isHidden = false
                                                                        self.noDataLabel.text = kNoCouponsAvailable
                                                                        self.corporateOffersListView.isHidden = true
                                                                    }
                                                                    
                }
            }
        }else if self.screenType == .CorporateOffers{
            self.corporateOffersListView.allowsSelection = false
            BBQActivityIndicator.showIndicator(view: self.view)
            if let model = self.buffetsViewModel, let bookingData = model.bookingDataForCorporateOffers {
                self.corporateViewModel.getCorporateOfferCoupons(corporateEmail: self.corporateEmailID, packs: bookingData.packs, reservationDate: bookingData.reservationDate, branchID: bookingData.branchId, totalAmount: bookingData.totalAmount, slotId: bookingData.slotId)
                { (isSuccess) in
                    BBQActivityIndicator.hideIndicator(from: self.view)
                    
                    if isSuccess{
                        self.corporateCouponsArray = self.corporateViewModel.getCoporateOffersList()
                        if(self.corporateCouponsArray.count>0){
                            self.corporateOffersListView.reloadData()
                        }
                    }else{
                        self.noDataLabel.isHidden = false
                        self.noDataLabel.text = kNoCorpOffersAvailable
                        self.corporateOffersListView.isHidden = true
                    }
                }
            }else{
                
            }
            
        }
    }
    @IBAction func onClickBtnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func applyManualCouponButtonPressed(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        if let couponCodeText = self.applyCouponField.text, couponCodeText.count > 0,
            let buffmodel = self.buffetsViewModel,
            let bookingData = buffmodel.bookingDataForCorporateOffers {
            BBQActivityIndicator.showIndicator(view: self.view)
            self.corporateViewModel.verifyManualCouponCode(barcodeValue: couponCodeText,
                                                           packs:bookingData.packs,
                                                           branch_ID: bookingData.branchId,
                                                           reservationDate: bookingData.reservationDate,
                                                           slotID: bookingData.slotId,
                                                           completion: { (data, error) in
                                                            BBQActivityIndicator.hideIndicator(from: self.view)

                                                            if let couponData = data {
                                                                // On success of validation passing coupon data in to booking sheet and dismissting the coupon sheet.
                                                                self.delegate?.userVoucherSelectedWith(voucherList: [couponData],
                                                                                                       type: self.screenType,
                                                                                                       termsAndConditions: self.termsAndCondtions)
                                                                
                                                                if let presentingController = self.bottomSheet{
                                                                    presentingController.dismissBottomSheet(on: self)
                                                                }
                                                                
                                                            } else {
                                                                //ToastHandler.showToastWithMessage(message: kInvalidCouponCode)
                                                            }
            })
        } else {
            //ToastHandler.showToastWithMessage(message: kEnterValidCoupon)
        }
    }
    
    
    private func setupHeaderTitle() {
        
        var userTitle = ""
        var titleName = ""
        if let firstName = BBQUserDefaults.sharedInstance.UserName.components(separatedBy: " ").first {
            titleName = String(firstName.filter { !" \n".contains($0) })
        }
        
        if self.screenType == .Vouchers {
            userTitle = "\(kHeaderTitleVouchersUser) \(titleName)"
            self.headerTitleLabel.text =
                BBQUserDefaults.sharedInstance.isGuestUser ? kHeaderTitleVouchers : userTitle
        } else if self.screenType == .CorporateOffers {
            self.headerTitleLabel.text = kHeaderTitleCorporateOffers
        } else if self.screenType == .VouchersForYou{
            userTitle = "\(kVouchersForYouTextUser) \(titleName)"
            self.headerTitleLabel.text =
                BBQUserDefaults.sharedInstance.isGuestUser ? kVouchersForYouText : userTitle
        } else if self.screenType == .CouponsForYou || self.screenType == .Coupans {
            userTitle = "\(kCouponsForYouTextUser) \(titleName)"
            self.headerTitleLabel.text =
                BBQUserDefaults.sharedInstance.isGuestUser ? kCouponsForYouText : userTitle
        } else {
            self.headerTitleLabel.text = kHeaderTitleCoupans
        }
    }
    
    private func initializeInitialItemQuatity() {
        for _ in self.vouchersArray {
            self.itemsQuantity.append(0)
        }
    }
    
    @discardableResult private func calculateTotalItemsAndPrice() -> (items: Int, price: Double) {
        let totalItems = self.itemsQuantity.reduce(0, +)
        
        var totalPrice = 0.0
        for (index, voucherModel) in self.vouchersArray.enumerated() {
            let countInDouble = Double(itemsQuantity[index])
            let calculatedPrice = (voucherModel.voucherPrice?.number)! * countInDouble
            totalPrice += calculatedPrice
        }
        
        if totalItems == 0 {
            if self.amountSummaryView.isHidden == false {
                self.amountSummaryView.isHidden = true
                self.bottomSheet?.expandOrCollapse(viewController: self, withHeight: 80, isExpand: false)
            }
            self.amountSummaryViewHeightConstraint.constant = 0
        } else {
            if self.amountSummaryView.isHidden == true {
                self.amountSummaryView.isHidden = false
                self.bottomSheet?.expandOrCollapse(viewController: self, withHeight: 80, isExpand: true)
            }
            self.amountSummaryView.setUIElements(itemCount: totalItems, price: totalPrice)
            self.amountSummaryViewHeightConstraint.constant = 80
        }
        
        return (totalItems, totalPrice)
    }
    
    @IBAction func userVouchersDoneClicked(){
        if self.selectedIndexPaths.count>0{
            for index  in selectedIndexPaths{
                let selectIndex:NSIndexPath = index as! NSIndexPath
                self.userVouchersArray.append(self.couponsArray[selectIndex.row])
            }
            
        }
        
        
        guard let delegate = self.delegate else {
            return
        }
        delegate.userVoucherSelectedWith(voucherList: self.userVouchersArray,
                                         type: self.screenType, termsAndConditions: self.termsAndCondtions)
        
        if let presentingController = self.bottomSheet{
            presentingController.dismissBottomSheet(on: self)
        }
        
    }
    
    @objc func outerViewTapped(_ sender: UITapGestureRecognizer? = nil) {
        self.view.endEditing(true)
    }
}

extension BBQAllOffersViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
//        if self.screenType == .Vouchers  {
//            return vouchersArray.count
//        } else if self.screenType == .CorporateOffers {
//            return corporateCouponsArray.count
//        } else if self.screenType == .Coupans { //Coupans
//            return couponsArray.count
//        } else if self.screenType == .CouponsForYou { //Coupons
//            //Coupons count
//            return couponsArray.count
//        }else if self.screenType == .VouchersForYou { //Coupons
//            //vouchers count
//            return couponsArray.count
//        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.screenType == .Vouchers  {
                  return vouchersArray.count
              } else if self.screenType == .CorporateOffers {
                  return corporateCouponsArray.count
              } else if self.screenType == .Coupans { //Coupans
                  return couponsArray.count
              } else if self.screenType == .CouponsForYou { //Coupons
                  //Coupons count
                  return couponsArray.count
              }else if self.screenType == .VouchersForYou { //Coupons
                  //vouchers count
                  return couponsArray.count
              }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
        
        if self.screenType == .Vouchers {
            let voucherModel: VouchersModel = vouchersArray[indexPath.row]
            
            let cell1: BBQVoucherForDiningTableViewCell  = (tableView.dequeueReusableCell(withIdentifier: "BBQVoucherForDiningTableViewCell", for: indexPath) as? BBQVoucherForDiningTableViewCell)!
            
            //            cell.serviceDelegate = self
            //            cell.screenType = .Vouchers
            cell1.configureCellFor(voucher: voucherModel, itemQuality: itemsQuantity[indexPath.row])
            cell1.selectionStyle = .none
            return cell1
        } else if self.screenType == .CorporateOffers {
            let cell: BBQAllOffersCell  = (tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.corporateOffersCell, for: indexPath) as? BBQAllOffersCell)!
            cell.screenType = .CorporateOffers

            let corpModel: CorporateOffer = self.corporateCouponsArray[indexPath.row]
            cell.screenType = .CorporateOffers
            cell.buffetsViewModel = self.buffetsViewModel
            cell.selectionStyle = .none
            cell.delegate = self
            cell.configureCellForCorporateOffers(offers: corpModel)
            return cell
        } else if self.screenType == .Coupans { //Coupans
            // For cart purpose only
            let cell: BBQAllOffersCell  = (tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.corporateOffersCell, for: indexPath) as? BBQAllOffersCell)!
            
            let couponModel: VoucherCouponsData = couponsArray[indexPath.row]
            cell.screenType = .Coupans
            cell.serviceDelegate = self
            cell.delegate = self
            cell.screenType = self.screenType
            cell.tag = indexPath.row
            cell.offerAddButton.tag = indexPath.row
            cell.selectionStyle = .none

            cell.configureCellFor(coupons: couponModel)
            return cell
        } else if self.screenType == .VouchersForYou{
            //VouchersForYou
            let cell1: BBQVoucherForDiningTableViewCell  = (tableView.dequeueReusableCell(withIdentifier: "BBQVoucherForDiningTableViewCell", for: indexPath) as? BBQVoucherForDiningTableViewCell)!
              cell1.screenType  = .VouchersForYou
            let couponModel: VoucherCouponsData = couponsArray[indexPath.row]
            cell1.buffetsViewModel = self.buffetsViewModel
            cell1.delegate = self
            cell1.applyBtn.tag = indexPath.row
            cell1.couponsModel = couponModel
            cell1.selectionStyle = .none

            self.configure(cell: cell1, forRowAtIndexPath: indexPath as NSIndexPath, coupons: couponModel)
            
            return cell1
            
        } else if self.screenType == .CouponsForYou{
            
            let cell: BBQAllOffersCell  = (tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.corporateOffersCell, for: indexPath) as? BBQAllOffersCell)!
            //CouponsForYou
            let couponModel: VoucherCouponsData = couponsArray[indexPath.row]
            cell.buffetsViewModel = self.buffetsViewModel
            cell.delegate = self
            cell.screenType = self.screenType
            cell.tag = indexPath.row
            cell.offerAddButton.tag = indexPath.row
            cell.screenType  = .CouponsForYou
            cell.isDinedAlready = isAfterDiningScreen
            cell.voucherCouponModel = self.couponVoucherModel.vouchersAndCouponsModel
            cell.configureCellFor(coupons: couponModel)
            cell.selectionStyle = .none

            return cell
        }
        
        let cell = corporateOffersListView.defaultCell(indexPath: indexPath)
        return cell
    }
    
    
    
    func configure(cell: BBQVoucherForDiningTableViewCell, forRowAtIndexPath indexPath: NSIndexPath,coupons: VoucherCouponsData) {
        cell.lblName.text = coupons.title
        
        var formattedPrice = String(format: "%@%.0f", getCurrency(), coupons.denomination ?? 0)
        if coupons.pax_applicable == 1{
            formattedPrice = "1 Person"
        }else if coupons.pax_applicable > 0{
            formattedPrice = String(format: "%li Persons", coupons.pax_applicable)
        }
        cell.lblAmount.text = formattedPrice
        cell.lblDescription.text  = coupons.title
        cell.lblDateTime.text = (coupons.validity ?? "")

       // cell.shareButton.isHidden = true
       // cell.infoButton.isHidden = false
        if selectedIndexPaths.contains(indexPath) {
            // selected
            cell.applyBtn.setTitle(kCouponsVouchersValueRemoveTitle, for: .normal)
            cell.applyBtn.backgroundColor = UIColor.darkGreen //.white
            cell.applyBtn.setTitleColor(.white, for: .normal)
        }
        else {
            // not selected
            cell.applyBtn.setTitle(kCouponsVouchersAddTitle , for: .normal)
            cell.applyBtn.backgroundColor = .white
            cell.applyBtn.setTitleColor( UIColor(named: "ThemeColor"), for: .normal)
        }
    }
    
}

extension BBQAllOffersViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 1.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        if self.screenType == .VouchersForYou {
            return 46.0
        }else{
            return 46.0
            
        }
        
        return 1.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if self.screenType == .Vouchers {
//            return 238.0
//        }
//        return 176
        UITableView.automaticDimension
    }
//
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
     
        if self.screenType == .VouchersForYou {
            let voucherFooterView = UINib(nibName: Constants.CellIdentifiers.bottomSheetFooterCell, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! BottomSheetImageFooterView
            voucherFooterView.footerLabel.text = kkVouchersAndCouponsBottomText
            return voucherFooterView
        } else if self.screenType == .CouponsForYou || self.screenType == .Coupans {
            let footerView = UINib(nibName: Constants.CellIdentifiers.bottomSheetFooterCell, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! BottomSheetImageFooterView
            footerView.footerLabel.text = kSelectOneCupon
            return footerView
        }else if self.screenType == .CorporateOffers {
            let footerView = UINib(nibName: Constants.CellIdentifiers.bottomSheetFooterCell, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! BottomSheetImageFooterView
            footerView.footerLabel.text = kHomeBottomSheetDefaultFooterTitle
            return footerView
        }


        return nil
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.screenType == .Vouchers {
            
            let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: 0.0)
            let bottomSheetController = BottomSheetController(configuration: configuration)
            
            let voucherDetailScreen = UIStoryboard.loadVoucherDetailScreen()
            voucherDetailScreen.voucherId = Int(vouchersArray[indexPath.row].voucherId ?? 0)
            voucherDetailScreen.voucherTitle = vouchersArray[indexPath.row].voucherName
            bottomSheetController.present(voucherDetailScreen, on: self)
        }
    }
}

extension BBQAllOffersViewController: BBQAllOffersCellProtocol {
    
    func userVouchersAddedwith(currentIndex:NSIndexPath)  {
        selectedIndexPaths.add(currentIndex)
        let couponModel: VoucherCouponsData = couponsArray[currentIndex.row]
        BBQUserDefaults.sharedInstance.bookingEstimatedTotal = BBQUserDefaults.sharedInstance.bookingEstimatedTotal - couponModel.denomination!
        self.amountSummaryView.isHidden = true
        self.voucherBottomView.isHidden = false
        let cell = corporateOffersListView.cellForRow(at: currentIndex as IndexPath)!
        configure(cell: cell as! BBQVoucherForDiningTableViewCell, forRowAtIndexPath: currentIndex, coupons: couponModel)
        self.self.voucherBottomView.isHidden = false
        self.amountSummaryViewHeightConstraint.constant = 80
    }
    func userVouchersRemoved(currentIndex:NSIndexPath)  {
        selectedIndexPaths.remove(currentIndex)
        let couponModel: VoucherCouponsData = couponsArray[currentIndex.row]
        BBQUserDefaults.sharedInstance.bookingEstimatedTotal = BBQUserDefaults.sharedInstance.bookingEstimatedTotal + couponModel.denomination!
        let cell = corporateOffersListView.cellForRow(at: currentIndex as IndexPath)!
        configure(cell: cell as! BBQVoucherForDiningTableViewCell, forRowAtIndexPath: currentIndex, coupons: couponModel)
        if selectedIndexPaths.count == 0 {
            self.voucherBottomView.isHidden = true
            self.amountSummaryViewHeightConstraint.constant = 0
        }
    }
    
    func didInfoTappedWith(infoTitle:String,infoDescription:String) {
        
        let infoAlertController = UIAlertController(title: infoTitle, message: infoDescription, preferredStyle: .alert)
        let okayAction = UIAlertAction(title: kMaxOtpTryOkayButton , style: .default) { (_ action) in
            //code here…
        }
        okayAction.setValue(UIColor.black, forKey: "titleTextColor")
        infoAlertController.addAction(okayAction)
        self.present(infoAlertController, animated: true, completion: nil)
    }
    
    func didTotalAmountReachedNegative(){
        PopupHandler.showSingleButtonsPopup(title: kMaximumAmountReached, message: kCannotAddMoreVouchers, on: self, firstButtonTitle: kMaxOtpTryOkayButton) {
        }
    }
    
    func didCartCouponVerifiedAt(couponData: VoucherCouponsData) {
        print(couponData)
        guard let delegate = self.delegate else {
            return
        }
        delegate.userVoucherSelectedWith(voucherList: [couponData],
                                         type: self.screenType, termsAndConditions: self.termsAndCondtions)
        
        if let presentingController = self.bottomSheet{
            presentingController.dismissBottomSheet(on: self)
        }
        
    }
    
    func userVouchersAdded(for index: Int, termsAndConditions: [String]) {
        if self.screenType == .CorporateOffers{
            if(self.corporateCouponsArray.count>0){
                guard let delegate = self.delegate else {
                    return
                }
                delegate.selectedCorporateOffer(corporateOffer: self.corporateCouponsArray[index])
            }
        } else  if self.screenType == .CouponsForYou{
            if(couponsArray.count>0){
                self.userCouponsArray = [couponsArray[index]]
                guard let delegate = self.delegate else {
                    return
                }
                delegate.userVoucherSelectedWith(voucherList: self.userCouponsArray,
                                                 type: self.screenType, termsAndConditions: self.termsAndCondtions)
            }
        }
        else if screenType  == .Coupans{
            if(couponsArray.count>0){
                self.userCouponsArray = [couponsArray[index]]
                guard let delegate = self.delegate else {
                    return
                }
                delegate.userVoucherSelectedWith(voucherList: self.userCouponsArray,
                                                 type: self.screenType, termsAndConditions: termsAndConditions)
            }
        }
        if let presentingController = self.bottomSheet{
            presentingController.dismissBottomSheet(on: self)
        }
    }
    
    func itemQuatityDidUpdate(quantity: Int, forCell: Int) {
        guard let delegate = self.delegate else {
            return
        }
        //   Handle other screen types here - Corporate offers and Coupans and Voucher&Coupons
        self.dismiss(animated: true, completion: nil)
        bottomSheet?.dismissBottomSheet(on: self)
        delegate.addButtonClickedAt(index: forCell)
    }
    
    func shareButtonClicked(for index: Int) {
        var contentToShare: ShareModel? = nil
        switch screenType {
        case .Vouchers:
            
            let voucherShareText = "Hey! I just found this interesting SMILE Card on Barbeque Nation website. I felt it might interest you! Please check the same on \(vouchersArray[index].voucherShareUrl ?? "")"
            
            contentToShare = ShareModel(data:voucherShareText , type: .Text, attributedText: nil)
        default:
            contentToShare = ShareModel(data: "https://drupal-stage.bnhl.in/product/25", type: .Url, attributedText: nil)
        }
        
        if let content = contentToShare {
            BBQShareHandler.shareContent(contentToShare: content, on: self, isBottomSheetPresenter: true)
        }
    }
    
    func didShowBarCodeForApplyCoupon(for barCode: String){
        let barCodeGreetingString = barCode
        PopupHandler.showSingleButtonsPopup(title: "Barcode",
                                            message: barCodeGreetingString,
                                            on: self,
                                            firstButtonTitle: "Okay") {
             self.dismiss(animated: true, completion: nil)
        }

    }
}

extension BBQAllOffersViewController: BBQAllOffersCellServiceProtocol {
    func itemQuatityDidAdd(for voucherId: String, cellIndex: Int) {
        if BBQUserDefaults.sharedInstance.isGuestUser {
            self.navigateToLogin()
            return
        }
        
        cartViewModel.addToCart(productId: Int(voucherId) ?? 0, quantity: 1) { [weak self] (isSuccess) in
            if let self = self, isSuccess {
                let addCartModel = self.cartViewModel.getModifyCartModel
                for orderItem in (addCartModel?.orderItems)! {
                    if orderItem.productId == Int(voucherId) {
                        let currentQuatity = self.itemsQuantity[cellIndex]
                        self.itemsQuantity.remove(at: cellIndex)
                        self.itemsQuantity.insert(currentQuatity + 1, at: cellIndex)
                        self.calculateTotalItemsAndPrice()
                        self.amountSummaryView.isHidden = false
                        self.voucherBottomView.isHidden = true
                        
                        if let cell = self.corporateOffersListView.cellForRow(at: IndexPath(row: 0, section: cellIndex)) as? BBQAllOffersCell {
                          //  cell.updateQuantity(quantity: currentQuatity + 1)
                        }
                        
                        guard let delegate = self.delegate else {
                            return
                        }
                        delegate.cartUpdated()
                    }
                }
            }
        }
    }
    
    func itemQuatityDidSubtract(for voucherId: String, cellIndex: Int, quantity: Int) {
        let addCartModel = self.cartViewModel.getModifyCartModel
        for orderItem in (addCartModel?.orderItems)! {
            if orderItem.productId == Int(voucherId) {
                let orderId = String(format: "%d", addCartModel?.orderId ?? "0")
                let orderItemId = String(format: "%d", orderItem.orderItemId ?? "0")
                
                BBQActivityIndicator.showIndicator(view: self.view)
                let updatedQuantity = (orderItem.quantity ?? 1) - 1
                self.cartViewModel.updateOrderItem(orderId: orderId, orderItem: orderItemId, quantity: updatedQuantity) { (isSuccess) in
                    BBQActivityIndicator.hideIndicator(from: self.view)
                    
                    if isSuccess {
                        let modifyCartModel = self.cartViewModel.getModifyCartModel
                        for orderItem in (modifyCartModel?.orderItems)! {
                            if orderItem.productId == Int(voucherId) {
                                let currentQuatity = self.itemsQuantity[cellIndex]
                                self.itemsQuantity.remove(at: cellIndex)
                                self.itemsQuantity.insert(currentQuatity - 1, at: cellIndex)
                                self.calculateTotalItemsAndPrice()
                                
                                let cell = self.corporateOffersListView.cellForRow(at: IndexPath(row: 0, section: cellIndex)) as! BBQAllOffersCell
                           //     cell.updateQuantity(quantity: currentQuatity - 1)
                                
                                guard let delegate = self.delegate else {
                                    return
                                }
                                delegate.cartUpdated()
                            }
                        }
                    }
                }
            }
        }
    }
    
    func removeItem(for voucherId: String, cellIndex: Int) {
        let addCartModel = self.cartViewModel.getModifyCartModel
        for orderItem in (addCartModel?.orderItems)! {
            if orderItem.productId == Int(voucherId) {
                if orderItem.quantity == 1 {
                    let orderId = String(format: "%d", addCartModel?.orderId ?? "0")
                    let orderItemId = String(format: "%d", orderItem.orderItemId ?? "0")
                    
                    BBQActivityIndicator.showIndicator(view: self.view)
                    self.cartViewModel.deleteOrderItem(orderId: orderId, orderItemId: orderItemId) { (isSuccess) in
                        BBQActivityIndicator.hideIndicator(from: self.view)
                        
                        if isSuccess {
                            self.itemsQuantity.remove(at: cellIndex)
                            self.itemsQuantity.insert(0, at: cellIndex)
                            self.calculateTotalItemsAndPrice()
                            
                            let cell = self.corporateOffersListView.cellForRow(at: IndexPath(row: 0, section: cellIndex)) as! BBQAllOffersCell
                           // cell.updateQuantity(quantity: 0)
                            
                            guard let delegate = self.delegate else {
                                return
                            }
                            delegate.cartUpdated()
                        }
                    }
                } else {
                    let orderId = String(format: "%d", addCartModel?.orderId ?? "0")
                    let orderItemId = String(format: "%d", orderItem.orderItemId ?? "0")
                    
                    BBQActivityIndicator.showIndicator(view: self.view)
                    let updatedQuantity = (orderItem.quantity ?? 1) - 1
                    self.cartViewModel.updateOrderItem(orderId: orderId, orderItem: orderItemId, quantity: updatedQuantity) { (isSuccess) in
                        BBQActivityIndicator.hideIndicator(from: self.view)
                        
                        if isSuccess {
                            let modifyCartModel = self.cartViewModel.getModifyCartModel
                            for orderItem in (modifyCartModel?.orderItems)! {
                                if orderItem.productId == Int(voucherId) {
                                    let currentQuatity = self.itemsQuantity[cellIndex]
                                    self.itemsQuantity.remove(at: cellIndex)
                                    self.itemsQuantity.insert(currentQuatity - 1, at: cellIndex)
                                    self.calculateTotalItemsAndPrice()
                                    
                                    let cell = self.corporateOffersListView.cellForRow(at: IndexPath(row: 0, section: cellIndex)) as! BBQAllOffersCell
                                //    cell.updateQuantity(quantity: currentQuatity - 1)
                                    
                                    guard let delegate = self.delegate else {
                                        return
                                    }
                                    delegate.cartUpdated()
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

extension BBQAllOffersViewController: BBQAllOffersAmountSummaryProtocol, UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func buyNowClicked() {
        if let delegate = self.delegate {
            //bottomSheet?.dismissBottomSheet(on: self)
            delegate.buyNowButtonClicked(bottomSheet: self.bottomSheet ?? nil)
        }
    }
}

extension BBQAllOffersViewController: LoginControllerDelegate {
    func navigateToLogin() {
        //self.navigationFromScreen = .Voucher
//        let viewController = UIStoryboard.loadLoginViewController()
//        viewController.delegate = self
//        viewController.navigationFromScreen = .Voucher
//        viewController.shouldCloseTouchingOutside = true
//        viewController.currentTheme = .dark
//        viewController.modalPresentationStyle = .overCurrentContext
//        self.present(viewController, animated: true, completion: nil)
        let loginViewcontroller = UIStoryboard.loadLoginThroughPhoneController()
        loginViewcontroller.delegate = self
        loginViewcontroller.navigationFromScreen = .Voucher
    self.navigationController?.pushViewController(loginViewcontroller, animated: false)
        
    }
    
    func gotoHomeScreen(isExistingUser: Bool, navigationFrom: LoginNavigationScreen) {
        if isExistingUser {
            //navigateToVoucherScreen()
        } else {
            showRegistrationConfirmationPage(navigationFrom: navigationFrom)
        }
        NotificationCenter.default.post(name: Notification.Name(Constants.NotificationName.upadateHomeHeader), object: nil)
    }
    
 
    
    func showRegistrationConfirmationPage(navigationFrom: LoginNavigationScreen) {
        let registrationConfirmationVC = UIStoryboard.loadRegistrationConfirmation()
        registrationConfirmationVC.modalPresentationStyle = .overCurrentContext
        registrationConfirmationVC.titleText = kSignUpConfirmationTitleText
        registrationConfirmationVC.navigationText = kSignUpConfirmationNavigationText
        
        let keyWindow = UIApplication.shared.keyWindow?.rootViewController?.presentedViewController
        keyWindow?.present(registrationConfirmationVC, animated: false) {
            sleep(UInt32(2.0))
            self.dismiss(animated: false) {
                //self.navigateToVoucherScreen()
            }
        }
    }
}


