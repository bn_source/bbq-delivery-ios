//
//  Created by Chandan Singh on 03/09/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQAllOffersCell.swift
//

import UIKit

protocol BBQAllOffersCellProtocol: AnyObject {
    func itemQuatityDidUpdate(quantity: Int, forCell: Int)
    func shareButtonClicked(for index: Int)
    // when vouchers are added
    func userVouchersAdded(for index: Int,termsAndConditions:[String])
    // when vouchers are removed
    func didCartCouponVerifiedAt(couponData:VoucherCouponsData)
    // when  remove button in each voucher is clicked
    func userVouchersRemoved(currentIndex:NSIndexPath)
    // when add button in each voucher is clicked
    func userVouchersAddedwith(currentIndex:NSIndexPath)
    
    //    //onclick of terms and conditions
    func termsAndConditionsClicked()
    
    //When total amont is less than 0 after applying voucher
    func didTotalAmountReachedNegative()
    //When info button clicked
    func didInfoTappedWith(infoTitle:String,infoDescription:String)
    
    func didShowBarCodeForApplyCoupon(for barCode: String)
    
    func didPressedVoucherGift(for voucherItemIndex: Int)
    
    
    
}

extension BBQAllOffersCellProtocol {
    func itemQuatityDidUpdate(quantity: Int, forCell: Int) { }
    func shareButtonClicked(for index: Int) { }
    // when vouchers are added
    func userVouchersAdded(for index: Int, termsAndConditions:[String]) { }
    func termsAndConditionsClicked() { }
    
    // when  remove button in each voucher is clicked
    func userVouchersRemoved(currentIndex:NSIndexPath) { }
    // when add button in each voucher is clicked
    func userVouchersAddedwith(currentIndex:NSIndexPath){}
 
    func didTotalAmountReachedNegative(){}

    func didShowBarCodeForApplyCoupon(for barCode: String) {}
    
    //When info button clicked
     func didInfoTappedWith(infoTitle:String,infoDescription:String){}
    
    func didPressedVoucherGift(for voucherItemIndex: Int) {}

}

protocol BBQAllOffersCellServiceProtocol: AnyObject {
    func itemQuatityDidAdd(for voucherId: String, cellIndex: Int)
    func itemQuatityDidSubtract(for voucherId: String, cellIndex: Int, quantity: Int)
    func removeItem(for voucherId: String, cellIndex: Int)
}

class BBQAllOffersCell: UITableViewCell {
    
    //MARK: Outlets
    @IBOutlet weak var corporateOfferName: UILabel!
    @IBOutlet weak var offerAddButton: UIButton!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var barCodeLbl: UILabel!
    @IBOutlet weak var voucherTermsAndConditionButton: UIButton!
    @IBOutlet weak var voucherTitleLabel: UILabel!
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var lblDescription : UILabel!
    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var viewForData: UIView!
    var screenType: ScreenType = .Vouchers
    var buffetsViewModel: BBQBookingBuffetsViewModel?
    weak var delegate: BBQAllOffersCellProtocol? = nil
    weak var serviceDelegate: BBQAllOffersCellServiceProtocol? = nil
    var bookingTotalAmount:Double  = 0.0
    private var voucherModel: VouchersModel? = nil
    var couponsModel: VoucherCouponsData? = nil
    private var corpOfferModel: CorporateOffer? = nil
    var voucherCouponModel:VoucherCouponsModel? = nil
    var selectedIndexPath: NSIndexPath?
    private var total :Double = 0.0
    var isDinedAlready = false
    @IBOutlet weak var mainview: UIView!
    private var userVouchersArray:[VoucherCouponsData] = []
    var vocuherCoverCount:Int = 0
    
    @IBOutlet weak var imgViewActivatedCard: UIImageView!
    //MARK: Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        setupUI()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTouch))
        //self.infoButton.setTitle(kInfoTitle, for: .normal)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
    }
    
    @objc func handleTouch() { }

    //MARK:- termsAndConditionsButtonPressed
    @IBAction func termsAndConditionsButtonTapped(_ sender: UIButton) {
        self.delegate?.termsAndConditionsClicked()
    }
    
    @IBAction func giftVoucherButtonPressed(_ sender: UIButton) {
        self.delegate?.didPressedVoucherGift(for: sender.tag)
    }
    
    
    //MARK: Setup UI
    private func setupUI() {
        
        
       // self.offerAddButton.roundCorners(cornerRadius: offerAddButton.frame.size.height/2, borderColor: .white, borderWidth: 1.0)

        
        
        self.viewForContainer.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
        mainview.layer.masksToBounds = false
        mainview.layer.shadowColor = UIColor.lightGray.cgColor
        mainview.layer.shadowOffset = CGSize(width: 0, height: 3)
        mainview.layer.shadowRadius = 2
        mainview.layer.shadowOpacity = 0.5
        offerAddButton.titleLabel?.font =  UIFont(name: Constants.App.BBQAppFont.NutinoSansSemiBold, size: 14)!
        if self.screenType == .Vouchers  {
            self.infoButton.isHidden = true
        }
       
        offerAddButton.titleLabel?.font = UIFont(name: Constants.App.BBQAppFont.ThemeMedium, size: 11.0)
        offerAddButton.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0.0)
        viewForData.layer.maskedCorners = [ .layerMinXMaxYCorner, .layerMinXMaxYCorner]
        viewForData.clipsToBounds = true
        viewForData.layer.cornerRadius = 10
    }
    
    func configureCellFor(voucher: VouchersModel, itemQuality: Int) {
        self.voucherModel = voucher
        self.corporateOfferName.text = voucher.voucherName
        self.priceLabel.text = String(format: "₹%.0f", voucher.voucherPrice?.number ?? "")
       
        
        if itemQuality == 0 {
            self.offerAddButton.isHidden = false
        } else {
            self.offerAddButton.isHidden = true
        }
    }
    
    func configureCellFor(coupons: VoucherCouponsData) {
        self.couponsModel = coupons
        self.corporateOfferName.text = coupons.title
        if coupons.voucherApplicability!{
            self.offerAddButton.isEnabled = true
            offerAddButton.setTitleColor(.deliveryThemeColor, for: .normal)
            imgViewActivatedCard.image = UIImage(named: "halfOrageCard")
        }else{
            self.offerAddButton.isEnabled = false
            offerAddButton.setTitleColor(UIColor.hexStringToUIColor(hex: "9D9D9D"), for: .normal)
            imgViewActivatedCard.image = UIImage(named: "DeactivatedCard")


        }
        self.barCodeLbl.text = coupons.barcode
        self.lblDescription.text = coupons.voucherDescription
        self.infoButton.isHidden = false
        self.priceLabel.text = "₹\(coupons.denomination ?? 0)"
      
        
        
        
        if self.isDinedAlready {
            self.offerAddButton.setTitle("Barcode", for: .normal)
        }
    }
    
    func configureCellForCorporateOffers(offers:CorporateOffer) {
        self.infoButton.isHidden = false
        self.corpOfferModel = offers
        self.priceLabel.isHidden = true
        self.corporateOfferName.text = offers.corporateTitle
       
    
        self.offerAddButton.isEnabled = true
        offerAddButton.setTitleColor(.deliveryThemeColor, for: .normal)
        imgViewActivatedCard.image = UIImage(named: "halfOrageCard")
        
        self.barCodeLbl.text = offers.corporateBarCode
        self.lblDescription.text = offers.corporateDescrition
        self.infoButton.isHidden = false
        self.priceLabel.text = "₹\(offers.corporateDenomination)"
        
        if self.isDinedAlready {
            self.offerAddButton.setTitle("Barcode", for: .normal)
        }
    }
    @IBAction func infoButtonTapped(_ sender:UIButton){
        guard self.delegate != nil else {
            return
        }
        if self.screenType == .CorporateOffers{
            self.delegate?.didInfoTappedWith(infoTitle: self.corpOfferModel!.corporateTitle, infoDescription:self.corpOfferModel!.corporateDescrition )
        }else{
            self.delegate?.didInfoTappedWith(infoTitle: self.couponsModel!.title!, infoDescription: self.couponsModel!.voucherDescription!)
        }
    }
    
    //MARK: Actions
    @IBAction func addButtonPressed(_ sender: UIButton) {
        
        if screenType == .Vouchers{
            AnalyticsHelper.shared.triggerEvent(type: .vouchers_status)
        }else{
            AnalyticsHelper.shared.triggerEvent(type: .coupon_status)
        }
        
        if self.isDinedAlready {
            self.delegate?.didShowBarCodeForApplyCoupon(for: self.couponsModel?.barcode ?? "")
            if screenType == .Vouchers{
                AnalyticsHelper.shared.triggerEvent(type: .vouchers_failure_value)
            }else{
                AnalyticsHelper.shared.triggerEvent(type: .coupon_failure_value)
            }
            return // Not asllows to apply coupons after dining, but showing barcode. It will be handled in POS.
        }
        
        guard self.delegate != nil else {
            return
        }

        if screenType != .Vouchers {
            if screenType == .VouchersForYou{
                self.selectedIndexPath =  IndexPath(row: sender.tag, section:0) as NSIndexPath
                if sender.titleLabel?.text == kCouponsVouchersAddTitle{
                    //If the total amount is greater than or less than the voucher amount allow user to add vouchers
                    if(BBQUserDefaults.sharedInstance.bookingEstimatedTotal > Double(self.couponsModel!.denomination!)){
                        self.verifyVoucherForBooking(couponsData:self.couponsModel!, sender: sender)
                    }else {
                        if ((BBQUserDefaults.sharedInstance.bookingEstimatedTotal == 0)||(BBQUserDefaults.sharedInstance.bookingEstimatedTotal < 0)){
                            print("not applicable")
                        //    self.offerAddButton.isHidden = true
                            self.delegate?.didTotalAmountReachedNegative()
                            AnalyticsHelper.shared.triggerEvent(type: .coupon_failure_value)
                        }else{
                            self.verifyVoucherForBooking(couponsData:self.couponsModel!, sender: sender)

                        }
                    }

                }else{
                    sender.setTitle(kCouponsVouchersAddTitle, for: .normal)
                  //  sender.backgroundColor = .clear
                    sender.setTitleColor(.white, for: .normal)
                    self.offerAddButton.isEnabled = true

                    self.delegate?.userVouchersRemoved(currentIndex: self.selectedIndexPath!)
                }
            }else if screenType == .CorporateOffers{
                  self.offerAddButton.isEnabled = false
                self.verifyBookingCouponsVouchersCorporate(couponData: self.corpOfferModel!)
            }else if screenType == .CouponsForYou{
                self.verifyCouponsForBooking(couponsData:self.couponsModel!)
            }else if screenType  == .Coupans{
                self.verifyPurchasedCoupons(couponsData: self.couponsModel!)
            }
            
        } else {
            guard let delegate = self.serviceDelegate else {
                return
            }
            delegate.itemQuatityDidAdd(for: String(self.voucherModel?.voucherId ?? 0), cellIndex: self.tag)
        }
    }
    
    //MARK:- shareButtonPressed
    @IBAction func shareButtonPressed(_ sender: UIButton) {
        guard let delegate = self.delegate else {
            return
        }
        delegate.shareButtonClicked(for: self.tag)
    }
    
    //MARK:- verifyUserPurchasedCoupons called to verify the coporate coupons
    func verifyBookingCouponsVouchersCorporate(couponData:CorporateOffer){
        let corporateModel = CorporateBookingViewModel()
        var noOfpacks:Int = 0
        var totalAmount = 0.0
        var brachID = ""
        var reservationDate = ""
        var slotID = 0
        
        if let model = self.buffetsViewModel, let bookingData = model.bookingDataForCorporateOffers{
            noOfpacks = bookingData.packs
            totalAmount = bookingData.totalAmount
            brachID = bookingData.branchId
            reservationDate = bookingData.reservationDate
            slotID = bookingData.slotId
        }
        UIUtils.showLoader()
        corporateModel.verifyVouchersAndCoupons(barcodeValue: couponData.corporateBarCode, packs: noOfpacks, amount: totalAmount, branch_ID:brachID, reservationDate: reservationDate, slotID: slotID) { (isSuccess) in
            if(isSuccess){
                self.delegate?.userVouchersAdded(for: self.tag, termsAndConditions: [])
                
                //                self.delegate?.userVouchersAdded(for: self.tag)
                self.offerAddButton.isHidden = false
                
            }else{
                self.offerAddButton.isHidden = true
                
            }
            UIUtils.hideLoader()
        }
        
    }
    
    func verifyVoucherForBooking(couponsData:VoucherCouponsData,sender:UIButton){
        let corporateModel = CorporateBookingViewModel()
        var noOfpacks:Int = 0
        var totalAmount = 0.0
        var brachID = ""
        var reservationDate = ""
        var slotID = 0
        self.offerAddButton.isEnabled   = false
        
        if let model = self.buffetsViewModel, let bookingData = model.bookingDataForCorporateOffers{
            noOfpacks = bookingData.packs
            totalAmount = bookingData.totalAmount
            brachID = bookingData.branchId
            reservationDate = bookingData.reservationDate
            slotID = bookingData.slotId
        }
        UIUtils.showLoader()
        
        corporateModel.verifyVouchersAndCoupons(barcodeValue: couponsData.barcode!, packs: noOfpacks, amount: totalAmount, branch_ID:brachID, reservationDate: reservationDate, slotID: slotID) { (isSuccess) in
        
            
            if(isSuccess){
                AnalyticsHelper.shared.triggerEvent(type: .coupon_success_value)
                self.offerAddButton.isHidden   = false
                self.offerAddButton.isEnabled   = true
                self.delegate!.userVouchersAddedwith(currentIndex:self.selectedIndexPath!)
            }else{
                AnalyticsHelper.shared.triggerEvent(type: .coupon_failure_value)
                ToastHandler.showToastWithMessage(message: BBQUserManager.networkResponse!.message)
                self.offerAddButton.isHidden   = true
                
            }
            UIUtils.hideLoader()
        }
    }
    
    
    
    func verifyCouponsForBooking(couponsData:VoucherCouponsData){
        let corporateModel = CorporateBookingViewModel()
        var noOfpacks:Int = 0
        var totalAmount = 0.0
        var brachID = ""
        var reservationDate = ""
        var slotID = 0
        
        if let model = self.buffetsViewModel, let bookingData = model.bookingDataForCorporateOffers{
            noOfpacks = bookingData.packs
            totalAmount = bookingData.totalAmount
            brachID = bookingData.branchId
            reservationDate = bookingData.reservationDate
            slotID = bookingData.slotId
        }
        UIUtils.showLoader()
        corporateModel.verifyVouchersAndCoupons(barcodeValue: couponsData.barcode!, packs: noOfpacks, amount: totalAmount, branch_ID:brachID, reservationDate: reservationDate, slotID: slotID) { (isSuccess) in
            BBQActivityIndicator.hideIndicator(from: self)
              self.offerAddButton.isEnabled = true
            if(isSuccess){
                if(isSuccess){
                    self.delegate?.userVouchersAdded( for: self.tag, termsAndConditions: self.voucherCouponModel!.voucherTermsAndConditions!)
                }else{
                    ToastHandler.showToastWithMessage(message: BBQUserManager.networkResponse!.message)
                    self.offerAddButton.isHidden   = true
                }
                
            }
            UIUtils.hideLoader()
        }
    }
    
    func verifyPurchasedCoupons(couponsData:VoucherCouponsData){
        let copuonModel = VouchersCouponsViewModel()
        UIUtils.showLoader()
        copuonModel.verifyPurchasedCouponAndVoucher(barcodeValue: couponsData.barcode!, mobileNumber: "", totalAmount: couponsData.denomination!, countryCode: "") { (isSuccess) in
            BBQActivityIndicator.hideIndicator(from: self)
            if(isSuccess){
                self.delegate?.didCartCouponVerifiedAt(couponData: copuonModel.vouchersAndCouponsData!)
            }else{
                
            }
            UIUtils.hideLoader()
        }
    }
    
    // MARK: Private Method
    
    private func showBarCodeAlertController() {
        
    }
}

extension BBQAllOffersCell: BBQAllOffersServiceProtocol {
    func itemQuatityDidAdd() {
        guard let delegate = self.serviceDelegate else {
            return
        }
        delegate.itemQuatityDidAdd(for:String( self.voucherModel?.voucherId ?? 0), cellIndex: self.tag)
    }
    
    func itemQuatityDidSubtract(quantity: Int) {
        guard let delegate = self.serviceDelegate else {
            return
        }
        if quantity == 0 {
            delegate.removeItem(for:String( self.voucherModel?.voucherId ?? 0), cellIndex: self.tag)
        } else {
            delegate.itemQuatityDidSubtract(for:String( self.voucherModel?.voucherId ?? 0), cellIndex: self.tag, quantity: quantity)
        }
    }
}

func itemQuatityDidUpdate(quantity: Int, forCell: Int) {
    
}

