//
 //  Created by Arpana on 15/12/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified BBQVoucherForDiningTableViewCell.swift
 //

import UIKit

class BBQVoucherForDiningTableViewCell: UITableViewCell {

    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var deactivatedViewForContainer: UIView!
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var viewForData: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var  expireStackView : UIStackView!
    @IBOutlet weak var btnTermAndCondition: UIButton!
    @IBOutlet weak var applyBtn: UIButton!
    weak var serviceDelegate: BBQAllOffersCellServiceProtocol? = nil

    var voucher: Coupon!
    var appliedCouoons = [ListTender]()
    var barcode = ""
    private var voucherModel: VouchersModel? = nil
    var isDinedAlready = false

    var indexPath: IndexPath!
   // var delegate : DeliveryCouponCellDelegate?
    var amount = 0

    weak var delegate: BBQAllOffersCellProtocol? = nil

    var screenType: ScreenType = .Vouchers

    var buffetsViewModel: BBQBookingBuffetsViewModel?
    var couponsModel: VoucherCouponsData? = nil
    var selectedIndexPath: NSIndexPath?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setTheme()
    }
    
    private func setTheme(){
        applyBtn.titleLabel?.font = UIFont(name: Constants.App.BBQAppFont.ThemeMedium, size: 11.0)
        applyBtn.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0.0)
        viewForContainer.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
        // viewForData.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
        viewForData.layer.maskedCorners = [ .layerMinXMaxYCorner, .layerMinXMaxYCorner]
        viewForData.clipsToBounds = true
        viewForData.layer.cornerRadius = 10
                
    }
    
    
    func configureCellFor(voucher: VouchersModel, itemQuality: Int) {
        self.voucherModel = voucher
        self.lblName.text = voucher.voucherName
        self.lblDescription.text = voucher.voucherDesc
        self.lblAmount.text = String(format: "₹%.0f", voucher.voucherPrice?.number ?? "")
      //  self.barcode = voucher.barcode

        expireStackView.isHidden = false
//       lblDateTime.text = (voucher.validity ?? "")
        
        //orderUnitView.delegate = self
      //  self.orderUnitView.serviceDelegate = self
        
//        if itemQuality == 0 {
//            self.offerAddButton.isHidden = false
//            self.orderUnitView.isHidden = true
//        } else {
//            self.offerAddButton.isHidden = true
//            self.orderUnitView.isHidden = false
//            self.orderUnitView.setupUI(withQuatity: itemQuality)
//        }
    }

    

    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    private func getStatus() -> (String?, UIColor, Bool, Bool){
        if voucher?.status == "Active"{
            return ("Expires on \(voucher?.validity ?? "")", .themeYello, true, true)
        }else if voucher?.status == "Redeemed" || voucher?.status == "Gifted"{
            return (voucher?.status, UIColor.hexStringToUIColor(hex: "74342D"), false, false)
        }else if voucher?.status == "In-Active" || voucher?.status == "Expired"{
            return (voucher?.status, UIColor.hexStringToUIColor(hex: "74342D"), false, false)
        }
        return ("", .theme, false, false)
    }
    
    @IBAction func actionOnApplyBtn(_ sender: UIButton) {

                AnalyticsHelper.shared.triggerEvent(type: .vouchers_status)
            
            
            if self.isDinedAlready {
                self.delegate?.didShowBarCodeForApplyCoupon(for: self.couponsModel?.barcode ?? "")
                if screenType == .Vouchers{
                    AnalyticsHelper.shared.triggerEvent(type: .vouchers_failure_value)
                }else{
                    AnalyticsHelper.shared.triggerEvent(type: .coupon_failure_value)
                }
                return // Not asllows to apply coupons after dining, but showing barcode. It will be handled in POS.
            }
            
            guard self.delegate != nil else {
                return
            }

            if screenType != .Vouchers {
                if screenType == .VouchersForYou{
                    self.selectedIndexPath =  IndexPath(row: sender.tag,  section: 0) as NSIndexPath
                    if sender.titleLabel?.text == kCouponsVouchersAddTitle{
                        //If the total amount is greater than or less than the voucher amount allow user to add vouchers
                        if(BBQUserDefaults.sharedInstance.bookingEstimatedTotal > Double(self.couponsModel!.denomination!)){
                            self.verifyVoucherForBooking(couponsData:self.couponsModel!, sender: sender)
                        }else {
                            if ((BBQUserDefaults.sharedInstance.bookingEstimatedTotal == 0)||(BBQUserDefaults.sharedInstance.bookingEstimatedTotal < 0)){
                                print("not applicable")
                            //    self.offerAddButton.isHidden = true
                                self.delegate?.didTotalAmountReachedNegative()
                                AnalyticsHelper.shared.triggerEvent(type: .coupon_failure_value)
                            }else{
                                self.verifyVoucherForBooking(couponsData:self.couponsModel!, sender: sender)

                            }
                        }

                    }else{
                        sender.setTitle(kCouponsVouchersAddTitle, for: .normal)
                      //  sender.backgroundColor = .clear
                        sender.setTitleColor(.white, for: .normal)
                        self.applyBtn.isEnabled = true

                        self.delegate?.userVouchersRemoved(currentIndex: self.selectedIndexPath!)
                    }
                }
                
            } else {
                guard let delegate = self.serviceDelegate else {
                    return
                }
                delegate.itemQuatityDidAdd(for: String(self.voucherModel?.voucherId ?? 0), cellIndex: self.tag)
            }
        }
        
        //}
    
    @IBAction func clickTermsAndCondition(_ sender: Any) {

         //   delegate?.actionOnTermsBtn(code: barcode,  tenderKey:   "VOUCHER", index: indexPath)
        
    }

    
    func verifyVoucherForBooking(couponsData:VoucherCouponsData,sender:UIButton){
        let corporateModel = CorporateBookingViewModel()
        var noOfpacks:Int = 0
        var totalAmount = 0.0
        var brachID = ""
        var reservationDate = ""
        var slotID = 0
        self.applyBtn.isEnabled   = false
        
        if let model = self.buffetsViewModel, let bookingData = model.bookingDataForCorporateOffers{
            noOfpacks = bookingData.packs
            totalAmount = bookingData.totalAmount
            brachID = bookingData.branchId
            reservationDate = bookingData.reservationDate
            slotID = bookingData.slotId
        }
        BBQActivityIndicator.showIndicator(view: self)
        
        corporateModel.verifyVouchersAndCoupons(barcodeValue: couponsData.barcode!, packs: noOfpacks, amount: totalAmount, branch_ID:brachID, reservationDate: reservationDate, slotID: slotID) { (isSuccess) in
            BBQActivityIndicator.hideIndicator(from: self)
            
            if(isSuccess){
                AnalyticsHelper.shared.triggerEvent(type: .coupon_success_value)
                self.applyBtn.isHidden   = false
                self.applyBtn.isEnabled   = true
                self.delegate!.userVouchersAddedwith(currentIndex:self.selectedIndexPath!)
            }else{
                AnalyticsHelper.shared.triggerEvent(type: .coupon_failure_value)
                ToastHandler.showToastWithMessage(message: BBQUserManager.networkResponse!.message)
                self.applyBtn.isHidden   = true
                
            }
        }
    }

    
    @IBAction func onClickBtnViewDetails(_ sender: Any) {
      //  delegate?.onClickBtnDetails(coupon: coupon, isEnabled: applyBtn.isEnabled, isApplied: !applyBtn.isUserInteractionEnabled)
    }
    

    
    private func checkForAppliedVoucher(coupon: Coupon) -> Bool{
        for voucher in appliedCouoons{
            if coupon.barcode == voucher.promocode{
                return true
            }
        }
        return false
    }
}
