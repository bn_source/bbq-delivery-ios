//
 //  Created by Bhamidipati Kishore on 23/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQVoucherDetailCell.swift
 //

import UIKit

class BBQVoucherDetailCell: UITableViewCell {

    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var arrowImage: UIImageView!
    @IBOutlet weak var subView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        arrowImage.setImageColor(color: #colorLiteral(red: 1, green: 0.3882352941, blue: 0.003921568627, alpha: 1))
        
//        subView.layer.cornerRadius = 13
//        subView.layer.shadowColor = UIColor.gray.cgColor
//        subView.layer.shadowOpacity = 0.25
//        subView.layer.shadowRadius = 10
//        subView.layer.shadowOffset = CGSize(width: 5, height: 5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
