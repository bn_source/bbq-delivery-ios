//
 //  Created by Chandan Singh on 07/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQVoucherDetailsTableHeader.swift
 //

import UIKit

class BBQVoucherDetailsTableHeader: UIView {

    @IBOutlet weak var headerTextLabel: UILabel!
    
    //MARK:- LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
