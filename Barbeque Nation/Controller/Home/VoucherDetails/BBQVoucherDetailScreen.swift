//
//  Created by Bhamidipati Kishore on 22/10/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQVoucherDetailScreen.swift
//

import UIKit

protocol BBQVoucherDetailScreenProtocol: AnyObject {
    func didPressProceedToCart()
}

class BBQVoucherDetailScreen: UIViewController {
    
    @IBOutlet weak var voucherDetailsTableView:UITableView!
    @IBOutlet weak var voucherImageView:UIImageView!
    @IBOutlet weak var imageTitle:UILabel!
    @IBOutlet weak var voucherPurchaseView: BBQVoucherPurchaseView!
    
    weak var delegate: BBQVoucherDetailScreenProtocol? = nil
    
    var comingFrom:String = ""
    var voucherId:Int = 0
    var voucherTitle: String?
    
    var sectionNames = [String]()
    var selectedRow: Int?
    
    private var voucherLocalQuatity = 0
    
    private var isDescriptionCellButtonHidden = true
    
    private var voucherDetailsModel: VoucherDetails? = nil
    lazy var voucherDetailsViewModel: VoucherDetailsViewModel = {
        let viewModel = VoucherDetailsViewModel()
        return viewModel
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Register the cells
        let voucherDetailCell = UINib(nibName:Constants.CellIdentifiers.BBQVoucherDetailCell, bundle:nil)
        self.voucherDetailsTableView.register(voucherDetailCell, forCellReuseIdentifier: Constants.CellIdentifiers.BBQVoucherDetailCell)
        
        let descriptionCell = UINib(nibName:Constants.CellIdentifiers.BBQDescriptionCell, bundle:nil)
        self.voucherDetailsTableView.register(descriptionCell, forCellReuseIdentifier: Constants.CellIdentifiers.BBQDescriptionCell)
        
        self.voucherDetailsTableView.tableFooterView = UIView()
        
        self.voucherPurchaseView.voucherId = self.voucherId
        self.voucherPurchaseView.voucherTitle = self.voucherTitle
        self.voucherPurchaseView.loginDelegate = self
        self.voucherPurchaseView.serviceDelegate = self
        self.voucherPurchaseView.cartType = .CartTypeVoucherDetails
        //API call
        print("voucherId=",voucherId)
        getVoucherDetailsData(voucherId: voucherId)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func setUpSection() {
        var timingString = ""
        if let timings = self.voucherDetailsModel?.voucherTimings, timings.count > 0 {
            for obj in timings {
                if let day = obj.day, let time = obj.time {
                    timingString = timingString + day + "\t" + time + "\n"
                }
            }
            sectionNames.append("Timing")
        }
   
        var branchString = ""
        if let notValid = self.voucherDetailsModel?.voucherNotValidOn, notValid.count > 0 {
            sectionNames.append("Not valid on")
        }
        if let validity = self.voucherDetailsModel?.voucherValidity, validity != "" {
            sectionNames.append("Valid till")
        }
        if let howtoUse = self.voucherDetailsModel?.voucherHowToUse, howtoUse != "" {
            sectionNames.append("How to use")
        }
        if let tNc = self.voucherDetailsModel?.voucherTnC, tNc != "" {
            sectionNames.append("Terms and Conditions")
        }
        if let policy = self.voucherDetailsModel?.voucherCancellationPolicy, policy != "" {
            sectionNames.append("Cancellation Policy")
        }
        if let applicableon = self.voucherDetailsModel?.voucherApplicableBranches, applicableon.count > 0 {
            for branch in applicableon {
                if let branchName = branch.voucherBranchName {
                    branchString = branchString + " " + branchName + "\n"
                }
            }
            sectionNames.append("Applicable at")
        }

    }
    
    func setUpUI(){
        var titleString = ""
        guard let imageUrlString = self.voucherDetailsModel?.voucherBannerPath else {
            return
        }
        LazyImageLoad.setImageOnImageViewFromURL(imageView: voucherImageView, url: imageUrlString) { (image) in
        }
        if let title = self.voucherDetailsModel?.voucherName {
            titleString = title
        }
        if let currency = self.voucherDetailsModel?.voucherCurrency, let price = self.voucherDetailsModel?.voucherPrice {
            if currency == Constants.App.NotificationPage.INR {
                self.imageTitle.text = titleString + " | " + "₹" + String(format: "%.0lf", price).toCurrencyFormat()
            }
        }
    }
    
    private func getVoucherDetailsData(voucherId: Int) {
        BBQActivityIndicator.showIndicator(view: self.view)
        self.voucherDetailsViewModel.getVoucherDetails(voucherId: voucherId) { [weak self] (isSuccess) in
            if let self = self {
                BBQActivityIndicator.hideIndicator(from: self.view)
                if isSuccess {
                    //Populate the UI
                    self.voucherDetailsModel = self.voucherDetailsViewModel.voucherDetailsData
                    self.setUpUI()
                    self.setUpSection()
                    self.voucherDetailsTableView.reloadData()
                }
            }
        }
    }
    
    // MARK:- IBActions
    @IBAction func backButtonPressed(_ sender: Any) {
        if comingFrom == Constants.App.NotificationPage.Notification {
            self.navigationController?.popViewController(animated: true)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}

extension BBQVoucherDetailScreen: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sectionNames.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row ==  0 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.BBQDescriptionCell) as? BBQDescriptionCell {
                cell.setupReserveButtonFor(promotion: false, isButtonHidden: self.isDescriptionCellButtonHidden)
                if let description = self.voucherDetailsModel?.voucherDescription {
                    cell.descriptionDetailLabel.text = description.htmlToString
                }
                cell.headerDelegate = self
                return cell
            }
        }else {

            if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.BBQVoucherDetailCell) as? BBQVoucherDetailCell{
                cell.selectionStyle = .none
                cell.titleLabel.text = self.sectionNames[indexPath.row-1]
                cell.tag = indexPath.row
                if selectedRow == indexPath.row {
                 cell.arrowImage.isHidden  = false
                   // cell.arrowImage.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                    switch self.sectionNames[indexPath.row-1] {
                    case "Timing":
                        cell.arrowImage.isHidden  = true

                        var timingString = ""
                        if let timings = self.voucherDetailsModel?.voucherTimings{
                            for obj in timings {
                                if let day = obj.day, let time = obj.time {
                                    timingString = timingString + day + "\t" + time //+ "\n"
                                }
                            }
                            cell.detailsLabel.text = timingString
                        }
                    case "Cancellation Policy":
                        cell.arrowImage.isHidden  = false
                        cell.arrowImage.transform = CGAffineTransform(rotationAngle: 0)

                        if let cancellation = self.voucherDetailsModel?.voucherCancellationPolicy {
                            cell.detailsLabel.text = cancellation.htmlToString
                        }
                    case "Applicable at":
                        cell.arrowImage.isHidden  = false

                            cell.arrowImage.transform = CGAffineTransform(rotationAngle: 0)
                        var branchString = ""
                        if let applicableon = self.voucherDetailsModel?.voucherApplicableBranches {
                            for branch in applicableon {
                                if let branchName = branch.voucherBranchName {
                                    branchString = branchString + " " + branchName + "\n"
                                }
                            }
                            cell.detailsLabel.text = branchString
                        }
                    case "Not valid on":
                        cell.arrowImage.isHidden  = true

                        if let notValidity = self.voucherDetailsModel?.voucherNotValidOn {
                            cell.detailsLabel.text = notValidity.joined(separator: "\n")
                        }
                    case "Valid till":
                        cell.arrowImage.isHidden  = true

                        if let validity = self.voucherDetailsModel?.voucherValidity {
                            cell.detailsLabel.text = validity
                        }
                    case "How to use":
                        cell.arrowImage.isHidden  = true

                        if let howtouse = self.voucherDetailsModel?.voucherHowToUse {
                            cell.detailsLabel.text = howtouse.htmlToString
                        }
                    case "Terms and Conditions":
                        cell.arrowImage.isHidden  = false

                            cell.arrowImage.transform = CGAffineTransform(rotationAngle: 0)
                        if let terms = self.voucherDetailsModel?.voucherTnC {
                            cell.detailsLabel.text = terms.htmlToString
                        }
                    
                    default:
                        print("")
                    }
                } else {
                    
                    switch self.sectionNames[indexPath.row-1] {
                    
                    case "Applicable at":
                        cell.arrowImage.isHidden  = false

                        cell.arrowImage.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                        cell.detailsLabel.text = ""
                   
                    case "Cancellation Policy":
                        cell.arrowImage.isHidden  = false
                        cell.arrowImage.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                           cell.detailsLabel.text = ""
                    
                    case "Valid till":
                        
                        cell.arrowImage.isHidden  = true

                        if let validity = self.voucherDetailsModel?.voucherValidity {
                            cell.detailsLabel.text = validity
                        }
                    case "Timing":
                        cell.arrowImage.isHidden  = true

                        var timingString = ""
                        if let timings = self.voucherDetailsModel?.voucherTimings{
                            for obj in timings {
                                if let day = obj.day, let time = obj.time {
                                    timingString = timingString + day + "\t" + time // + "\n"
                                }
                            }
                            cell.detailsLabel.text = timingString
                        }
                        
                    case "Terms and Conditions":
                        cell.arrowImage.isHidden  = false

                        cell.arrowImage.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                        cell.detailsLabel.text = ""
                    default:
                        print("")
                    }
                  
                }
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let selectedRow = selectedRow{
            let oldIndexPath = IndexPath(row: selectedRow, section: 0)
            if selectedRow != indexPath.row {
                self.selectedRow = indexPath.row
                self.voucherDetailsTableView.reloadRows(at: [oldIndexPath,indexPath], with: UITableView.RowAnimation.none)
            } else {
                self.selectedRow = nil
                self.voucherDetailsTableView.reloadRows(at: [oldIndexPath], with: UITableView.RowAnimation.none)
            }
        }else{
            self.selectedRow = indexPath.row
            self.voucherDetailsTableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.none)
        }
    }
}

extension BBQVoucherDetailScreen: BBQVoucherPurchaseViewProtocol, LoginControllerDelegate {
    func showCartButton(isHidden: Bool) {
        self.isDescriptionCellButtonHidden = isHidden
        let indexPath = IndexPath(row: 0, section: 0)
        if let cell = self.voucherDetailsTableView.cellForRow(at: indexPath) as? BBQDescriptionCell {
            cell.reserveButton.isHidden = isHidden
        }
    }
    
    func proceedToLoginScreen() {
        self.navigateToLogin()
    }
    
    func navigateToLogin() {
//        let viewController = UIStoryboard.loadLoginViewController()
//        viewController.delegate = self
//        viewController.navigationFromScreen = .Voucher
//        viewController.shouldCloseTouchingOutside = true
//        viewController.currentTheme = .dark
//        viewController.modalPresentationStyle = .overCurrentContext
//        self.present(viewController, animated: true, completion: nil)
        let loginViewcontroller = UIStoryboard.loadLoginThroughPhoneController()
        loginViewcontroller.navigationFromScreen = .Voucher
        loginViewcontroller.delegate = self
        
        self.navigationController?.pushViewController(loginViewcontroller, animated: false)
    }
    
    func gotoHomeScreen(isExistingUser: Bool, navigationFrom: LoginNavigationScreen) {
        if !isExistingUser {
            self.showRegistrationConfirmationPage(navigationFrom: navigationFrom)
        }
        
        NotificationCenter.default.post(name: Notification.Name(Constants.NotificationName.upadateHomeHeader), object: nil)
    }
    
    func showRegistrationConfirmationPage(navigationFrom: LoginNavigationScreen) {
        let registrationConfirmationVC = UIStoryboard.loadRegistrationConfirmation()
        registrationConfirmationVC.modalPresentationStyle = .overCurrentContext
        registrationConfirmationVC.titleText = kSignUpConfirmationTitleText
        registrationConfirmationVC.navigationText = kSignUpConfirmationNavigationText
        
        let keyWindow = UIApplication.shared.keyWindow?.rootViewController?.presentedViewController
        keyWindow?.present(registrationConfirmationVC, animated: false) {
            sleep(UInt32(2.0))
            self.dismiss(animated: false) {
                
            }
        }
    }
}

extension BBQVoucherDetailScreen : BBQDescriptionCellProtocol {
    
    func didPressedReserveTableButton() {
        self.dismiss(animated: true) {
            if let delegate = self.delegate {
                delegate.didPressProceedToCart()
            }
        }
    }
}

extension BBQVoucherDetailScreen: BBQVoucherPurchaseViewServiceProtocol {
    func itemQuatityDidAdd(index: Int) {
        self.voucherLocalQuatity += 1
        self.voucherPurchaseView.voucherUnitView.setupUI(withQuatity: self.voucherLocalQuatity)
    }
    
    func itemQuatityDidSubtract(index: Int) {
        self.voucherLocalQuatity -= 1
        self.voucherPurchaseView.voucherUnitView.setupUI(withQuatity: self.voucherLocalQuatity)
    }
    
    func itemQuatityDidRemove(index: Int) {
        self.voucherLocalQuatity = 0
        self.voucherPurchaseView.voucherUnitView.setupUI(withQuatity: self.voucherLocalQuatity)
    }
}
