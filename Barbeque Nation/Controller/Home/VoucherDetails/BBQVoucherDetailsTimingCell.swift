//
 //  Created by Chandan Singh on 10/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQVoucherDetailsTimingCell.swift
 //

import UIKit

class BBQVoucherDetailsTimingCell: UITableViewCell {

    @IBOutlet weak var dayTimingLabel: UILabel!
    @IBOutlet weak var afternoonTimingLabel: UILabel!
    @IBOutlet weak var eveningTimingLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
