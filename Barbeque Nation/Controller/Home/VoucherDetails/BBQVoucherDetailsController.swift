//
 //  Created by Chandan Singh on 05/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQVoucherDetailsController.swift
 //

import UIKit

enum DetailScreenType {
    case Promotion, Voucher, none
}

enum VoucherDetailsTableHeader: Int, CaseIterable {
    case timings, applicableOn, cancellationPolicy, validTill, howToUse, thingsToRemember
    
    static var caseCount: Int { return self.allCases.count }
    
    var description: String {
        get {
            switch(self) {
            case .timings:
                return kVoucherTimingsText
            case .applicableOn:
                return kVoucherApplicableText
            case .cancellationPolicy:
                return kVoucherCancellationText
            case .validTill:
                return kVoucherValidityText
            case .howToUse:
                return kVoucherHowToText
            case .thingsToRemember:
                return kVoucherThingsText
            }
        }
    }
}

class BBQVoucherDetailsController: UIViewController {

    @IBOutlet weak var topContainerView: UIView!
    @IBOutlet weak var topImageView: UIImageView!
    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var detailsTableView: UITableView!
    @IBOutlet weak var voucherNameLabel: UILabel!
    @IBOutlet weak var voucherPriceLabel: UILabel!
    @IBOutlet weak var noDataLabel: UILabel!
    @IBOutlet weak var seperatorImageView: UIImageView!
    
    var voucherId: Int = 0
    private var voucherDetailsModel: VoucherDetails? = nil
    
    var promotionId: Int = 0
    private var promotionDetailsModel: PromotionDetailsModel? = nil
    
    var detailScreenType: DetailScreenType = .none
    
    fileprivate let tableViewEstimatedRowHeight: CGFloat = 31.0
    private let dataCellId = Constants.CellIdentifiers.voucherDetailsDataCell
    private let timingsCellId = Constants.CellIdentifiers.voucherDetailsTimingsCell
    private let headerCellId = Constants.CellIdentifiers.voucherDetailsHeaderCell
    
    lazy var voucherDetailsViewModel: VoucherDetailsViewModel = {
        let viewModel = VoucherDetailsViewModel()
        return viewModel
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.configureGenericUI()
        
        //Get the data from server
        self.getDataFromServer()
    }
    
    //MARK:- Setup UI
    private func configureGenericUI() {
        //Set Overlay background translucent color
        self.overlayView.backgroundColor = .promoOverlayColor
        self.noDataLabel.text = kVouchersDetailsUnavailableText
        
        //register Table Cell
        self.detailsTableView.estimatedRowHeight = tableViewEstimatedRowHeight
        self.detailsTableView.register(UINib(nibName: dataCellId, bundle: nil), forCellReuseIdentifier: dataCellId)
        self.detailsTableView.register(UINib(nibName: timingsCellId, bundle: nil), forCellReuseIdentifier: timingsCellId)
    }
    
    private func setupUI() {
        //Set the data in UI
        self.voucherNameLabel.text = self.voucherDetailsModel?.voucherName
        
        var currencySymbol = ""
        if self.voucherDetailsModel?.voucherCurrency == "INR" {
            currencySymbol = "₹"
        }
        self.voucherPriceLabel.text = String(format: "%@%.0lf", currencySymbol, self.voucherDetailsModel?.voucherPrice ?? 0)
        
        LazyImageLoad.setImageOnImageViewFromURL(imageView: self.topImageView, url: self.voucherDetailsModel?.voucherBannerPath) { (image) in
            
        }
        
        self.detailsTableView.reloadData()
    }
    
    private func configureDataUI(isData: Bool) {
        if isData {
            self.topContainerView.isHidden = false
            self.detailsTableView.isHidden = false
            self.noDataLabel.isHidden = true
        } else {
            self.topContainerView.isHidden = true
            self.detailsTableView.isHidden = true
            self.noDataLabel.isHidden = false
        }
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Get Data
    private func getDataFromServer() {
        if self.detailScreenType == .Voucher {
            self.getVoucherDetailsData(voucherId: self.voucherId)
        } else if self.detailScreenType == .Promotion {
            self.getPromosDetailsData(promotionId: self.promotionId)
        }
    }
    
    private func getVoucherDetailsData(voucherId: Int) {
        BBQActivityIndicator.showIndicator(view: self.view)
        self.voucherDetailsViewModel.getVoucherDetails(voucherId: voucherId) { [weak self] (isSuccess) in
            if let self = self {
                BBQActivityIndicator.hideIndicator(from: self.view)
                if isSuccess {
                    //Populate the UI
                    self.voucherDetailsModel = self.voucherDetailsViewModel.voucherDetailsData
                    self.configureDataUI(isData: true)
                    self.setupUI()
                } else {
                    self.configureDataUI(isData: false)
                }
            }
        }
    }
    
    private func getPromosDetailsData(promotionId: Int) {
        BBQActivityIndicator.showIndicator(view: self.view)
        self.voucherDetailsViewModel.getPromotionDetails(promotionId: promotionId) { [weak self] (isSuccess) in
            if let self = self {
                BBQActivityIndicator.hideIndicator(from: self.view)
                if isSuccess {
                    //Populate the UI
                    self.promotionDetailsModel = self.voucherDetailsViewModel.promotionDetailsData
                    self.configureDataUI(isData: true)
                    self.setupUI()
                } else {
                    self.configureDataUI(isData: false)
                }
            }
        }
    }
}

extension BBQVoucherDetailsController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return VoucherDetailsTableHeader.caseCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 { //
            guard let voucherModel = self.voucherDetailsModel, let timings = voucherModel.voucherTimings else {
                return 1
            }
            return timings.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if let voucherModel = self.voucherDetailsModel, voucherModel.voucherTimings != nil {
                guard let timingsCell = tableView.dequeueReusableCell(withIdentifier: timingsCellId, for: indexPath) as? BBQVoucherDetailsTimingCell else {
                    return UITableViewCell()
                }
                
                guard let voucherModel = self.voucherDetailsModel else {
                    return timingsCell
                }
                
                if let timings = voucherModel.voucherTimings {
                    let timingsData = timings[indexPath.row]
                    timingsCell.dayTimingLabel.text = timingsData.day
                    timingsCell.afternoonTimingLabel.text = timingsData.time
                }
                
                return timingsCell
            } else {
                guard let dataCell = tableView.dequeueReusableCell(withIdentifier: dataCellId, for: indexPath) as? BBQVoucherDetailsDataCell else {
                    return UITableViewCell()
                }
                
                dataCell.dataTextLabel.text = kVoucherTimingsUnavailable
                return dataCell
            }
        } else {
            guard let dataCell = tableView.dequeueReusableCell(withIdentifier: dataCellId, for: indexPath) as? BBQVoucherDetailsDataCell else {
                return UITableViewCell()
            }
            
            guard let voucherModel = self.voucherDetailsModel else {
                dataCell.dataTextLabel.text = kVoucherDataUnavailable
                return dataCell
            }
            
            switch indexPath.section {
            case 1: //Applicable at
                var cellString = kVoucherDataUnavailable
                if voucherModel.voucherApplicableBranches == nil {
                    cellString = ""
                } else {
                    cellString = kVoucherParticularlyOnText
                    for (index, data) in voucherModel.voucherApplicableBranches!.enumerated() {
                        if index == voucherModel.voucherApplicableBranches!.count - 1 { //Last Object
                            if let name = data.voucherBranchName {
                                cellString += name
                            }
                        } else {
                            if let name = data.voucherBranchName {
                                cellString += name + ", "
                            }
                        }
                    }
                    if let treatType = voucherModel.voucherTreatType {
                        cellString += "\n" + kVoucherValidForText + treatType
                    }
                    dataCell.dataTextLabel.text = cellString
                }
                
            case 2: //Cancellation Policy
                if voucherModel.voucherCancellationPolicy == "" {
                    dataCell.dataTextLabel.text = kVoucherDataUnavailable
                } else {
                    dataCell.dataTextLabel.text = voucherModel.voucherCancellationPolicy?.htmlToString
                }
                
            case 3://Valid till
                if voucherModel.voucherNotValidOn == nil {
                    dataCell.dataTextLabel.text = kVoucherDataUnavailable
                } else {
                    dataCell.dataTextLabel.text = voucherModel.voucherNotValidOn?[0]
                }
                
            case 4://How to use
                if voucherModel.voucherHowToUse == "" {
                    dataCell.dataTextLabel.text = kVoucherDataUnavailable
                } else {
                    var howToUse = voucherModel.voucherHowToUse
                    howToUse = howToUse?.replacingOccurrences(of: "\t", with: "")
                    dataCell.dataTextLabel.attributedText = voucherModel.voucherHowToUse?.htmlToAttributedString
                }
                
            case 5://Things to Remember
                if voucherModel.voucherTnC == "" {
                    dataCell.dataTextLabel.text = kVoucherDataUnavailable
                } else {
                    dataCell.dataTextLabel.attributedText = voucherModel.voucherTnC?.htmlToAttributedString
                }
                
            default:
                dataCell.dataTextLabel.text = kVoucherDataUnavailable
            }
            
            return dataCell
        }
    }
}

extension BBQVoucherDetailsController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UINib(nibName: headerCellId, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! BBQVoucherDetailsTableHeader
        
        header.headerTextLabel.text = VoucherDetailsTableHeader(rawValue: section)?.description
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        //More height for Last Section footer. So that the table comes up to the back button.
        if section == VoucherDetailsTableHeader.caseCount - 1 {
            return 84.0
        }
        return 1.0
    }
}
