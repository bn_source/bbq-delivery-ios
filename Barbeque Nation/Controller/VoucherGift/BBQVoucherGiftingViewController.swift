//
 //  Created by Ajith CP on 02/01/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified BBQVoucherGiftingViewController.swift
 //

import UIKit
import ContactsUI

protocol BBQVoucherGiftingViewControllerProtocol {
    
    func didFinishedVoucherGifting()
}

class BBQVoucherGiftingViewController: UIViewController {
    
    private static let keyboardAdjustDistance = -50
    private static let keyboardAdjustDuration = 0.3

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var emailErrorLabel: UILabel!
    @IBOutlet weak var nameErrorLabel: UILabel!
    @IBOutlet weak var phoneNumberErrorLabel: UILabel!
    
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var phoneCodeTextField: UITextField!
    
    @IBOutlet weak var contentView: UIView!
   // @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var phoneNumberView: UIView!
    
    @IBOutlet weak var giftButton: UIButton!
    
    @IBOutlet weak var emailPlaceHolderBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var namePlaceHolderBottomConstraint: NSLayoutConstraint!
    
    var voucherBarCode : String = ""
    var isFromDetailsPage = false
    private var countryCode : String = Constants.App.defaultValues.phoneNumberCode
    private var updatedMobileNumber : String = ""
    private var emailID : String = ""
    private var giftedTo : String = ""
    
    
    private var isMobileValid : Bool  = true
    private var isNameValid : Bool    = true
    private var isEmailIDValid : Bool = true
        
    var giftingDelegate : BBQVoucherGiftingViewControllerProtocol?
    
    // MARK: - Lazy Initializer
    
    lazy private var voucherGiftVM : BBQVoucherGiftingViewModel = {
        let giftVM = BBQVoucherGiftingViewModel()
        return giftVM
    } ()
    
    // MARK: Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupGiftVoucherComponent()
      //  self.setupPhoneNumberCell()

    }
    
    // MARK: Setup Methods
    
//    private func setupPhoneNumberCell() {
//        phoneNumberCell = Bundle.main.loadNibNamed("PhoneNumberCell",
//                                            owner: self,
//                                            options: nil)?[0] as? PhoneNumberCell
//
//        phoneNumberCell?.currentTheme = .dark
//        phoneNumberCell?.errorLabel.text = "Enter Mobile Number"
//        phoneNumberCell?.countryCodeButton.addTarget(self,
//                                         action:#selector(countryCodeSelectionButtonPresed),
//                                         for: .touchUpInside)
//        phoneNumberCell?.delegate = self
//        phoneNumberView.addSubview(phoneNumberCell?.contentView ?? UIView())
//        phoneNumberCell?.contentView.pinEdgesToSuperView()
//        self.view.layoutIfNeeded()
//    }
    
    private func setupGiftVoucherComponent() {
        
        self.nameErrorLabel.isHidden  = true
        self.emailErrorLabel.isHidden = true
        self.phoneNumberErrorLabel.isHidden = true
        
        self.giftButton.roundCorners(cornerRadius: 10.0,
                                     borderColor: .clear,
                                     borderWidth: 0.0)
//
//        self.overlayView.backgroundColor = .black
//        self.overlayView.layer.opacity = 0.7
        
        self.contentView.roundCorners(cornerRadius: 15.0,
                                      borderColor: .clear,
                                      borderWidth: 0.0)
        
        self.contentView.addGestureRecognizer(UITapGestureRecognizer(target: self,
                                                                     action: #selector(endEditing)))
        
        emailTextField.setCornerRadius(10)
        emailTextField.addLeftView(UIView(frame: CGRect(x: 0, y: 0, width: 10, height: emailTextField.frame.size.height)))
        
        nameTextField.setCornerRadius(10)
        nameTextField.addLeftView(UIView(frame: CGRect(x: 0, y: 0, width: 10, height: emailTextField.frame.size.height)))
        
        phoneTextField.setCornerRadius(10)
        phoneTextField.addLeftView(UIView(frame: CGRect(x: 0, y: 0, width: 10, height: emailTextField.frame.size.height)))
        
        phoneCodeTextField.setCornerRadius(10)
        phoneCodeTextField.addTarget(self,
                                                action:#selector(countryCodeSelectionButtonPresed),
                                                for: .editingDidBegin)
        
//        self.overlayView.addGestureRecognizer(UITapGestureRecognizer(target: self,
//                                                                     action: #selector(dismissController)))

        
    }
    
    // MARK: IBActions
    
    @IBAction func giftVoucherButtonPressed(_ sender: UIButton) {
        if isFromDetailsPage{
            AnalyticsHelper.shared.triggerEvent(type: .HD03)
        }else{
            AnalyticsHelper.shared.triggerEvent(type: .HH03)
        }
        
        self.endEditing()
      
        isMobileValid = voucherGiftVM.isValidMobileNumber(countyryCode: self.countryCode,
                                                          mobileNumber: self.updatedMobileNumber)
        isEmailIDValid = voucherGiftVM.isValidEmailAddress(emailText: self.emailID)
        isNameValid = voucherGiftVM.isValidNameEntered(nameText: self.giftedTo)
        self.refreshGiftingErrorLabels()
        if voucherGiftVM.isGiftingPageValidated(mobileValid: isMobileValid,
                                                emailValid: isEmailIDValid,
                                                nameValid: isNameValid) {
            BBQActivityIndicator.showIndicator(view: self.contentView)
            voucherGiftVM.giftVoucherTo(barCode: self.voucherBarCode,
                                        countryCode: self.countryCode,
                                        phoneNumber: self.updatedMobileNumber,
                                        email: self.emailID,
                                        name: self.giftedTo) { (isGifted) in
                                         BBQActivityIndicator.hideIndicator(from: self.contentView)
                                            if isGifted {
                                                if self.isFromDetailsPage{
                                                    AnalyticsHelper.shared.triggerEvent(type: .HD03A)
                                                }else{
                                                    AnalyticsHelper.shared.triggerEvent(type: .HH03A)
                                                }
                                                self.giftingDelegate?.didFinishedVoucherGifting()
                                                self.dismissController()
                                            }else{
                                                if self.isFromDetailsPage{
                                                    AnalyticsHelper.shared.triggerEvent(type: .HD03B)
                                                }else{
                                                    AnalyticsHelper.shared.triggerEvent(type: .HH03B)
                                                }
                                            }
            }
        }
    }
    
    // MARK: Private Methods
    
    private func refreshGiftingErrorLabels() {
        self.phoneNumberErrorLabel.isHidden = isMobileValid
        nameErrorLabel.isHidden = isNameValid
        emailErrorLabel.isHidden = isEmailIDValid
    }

}

extension BBQVoucherGiftingViewController : PhoneNumberCellDelegate, PickerViewDelegate {
    
    // MARK: PickerViewDelegate Methods
    
    func countryCodeSelected(phoneCode: String) {
        if self.countryCode != phoneCode {
            phoneCodeTextField.text = ""
            phoneCodeTextField.text = phoneCode
            phoneCodeTextField.resignFirstResponder()
           // phoneNumberCell?.countryCodeButton.setTitle(phoneCode, for: .normal)
           // phoneNumberCell?.changeLabelPosition(position: 4.0)
        }
        self.countryCode = phoneCode
    }

    func titleSelected(title: String) {
        // Implementation not needed here.
    }
    
    func pickerClosed() {
        // Implementation not needed here.
    }
    
    // MARK: PhoneNumberCellDelegate Methods
   
    func phoneNumberEntered(length: Int, text: String) {
        self.updatedMobileNumber = text
        
    }
    
    func textFieldStartsEditing(_ textField: UITextField) {
        textField.becomeFirstResponder()
        if textField == emailTextField {
            isEmailIDValid = true
        }else if textField == nameTextField {
                isNameValid = true
        }
        refreshGiftingErrorLabels()
    }
    
    func updateErrorLabel (_ textField: UITextField) {
        
        if textField == emailTextField {
            isEmailIDValid = true
        }else if textField == nameTextField {
                isNameValid = true
        }
        
        refreshGiftingErrorLabels()

    }
    func textFieldStopsEditing(_ textField: UITextField) {
        let countryCode = self.countryCode
        let mobileNumber = textField.text ?? ""
        self.isMobileValid = voucherGiftVM.isValidMobileNumber(countyryCode: countryCode,
                                                               mobileNumber: mobileNumber)
        if textField.text?.count ?? 0 > 0 {
            self.refreshGiftingErrorLabels()
        }
        
        textField.resignFirstResponder()
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {    //delegate method
        textField.becomeFirstResponder()
        
        if textField == phoneTextField {
            isMobileValid = true
        }
        if textField == emailTextField {
            isEmailIDValid = true
        }else if textField == nameTextField {
                isNameValid = true
        }
        refreshGiftingErrorLabels()
    }
    
    
    private func animateNameTextField(textField: UITextField, up: Bool) {
        let movementDistance:CGFloat = CGFloat(BBQVoucherGiftingViewController.keyboardAdjustDistance)
        let movementDuration: Double = BBQVoucherGiftingViewController.keyboardAdjustDuration
        var movement:CGFloat = 0
        if up{
            movement = movementDistance
        }else{
            movement = -movementDistance
        }
        UIView.beginAnimations(Constants.NotificationName.animateTextField, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
    
    @objc func countryCodeSelectionButtonPresed(with textField: UITextField) {
        let pickerViewController = UIStoryboard.loadPickerViewController()
        pickerViewController.delegate = self
        pickerViewController.type = .countryCode
        pickerViewController.isComingFromProfile = false
        self.present(pickerViewController, animated: false, completion: nil)
    }
   
    @objc func endEditing(){
        self.view.endEditing(true)
    }
    
    @objc func dismissController() {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func backBtnPressed(_ sender : UIButton){
        
        dismissController()
    }
}


extension BBQVoucherGiftingViewController : UITextFieldDelegate {
    
   // func textFieldDidBeginEditing(_ textField: UITextField) {
//        if let constraint = textField == emailTextField ? emailPlaceHolderBottomConstraint : namePlaceHolderBottomConstraint{
//            changeLabelPosition(labelConstriant: constraint, isExpand: true)
//        }
//        if textField == nameTextField {
//            animateNameTextField(textField: textField, up: true)
//        }
//    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == phoneCodeTextField  {
            
            return false
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        
        if textField == emailTextField  {
            self.isEmailIDValid = voucherGiftVM.isValidEmailAddress(emailText: textField.text ?? "")
            if isEmailIDValid {
                self.emailID = textField.text ?? ""
            }
        } else if textField == nameTextField {
            self.isNameValid = voucherGiftVM.isValidNameEntered(nameText: textField.text ?? "")
            if isNameValid {
                self.giftedTo = textField.text ?? ""
            }
            
        }
       else  if textField == phoneTextField  {
            
            let countryCode = self.countryCode
            let mobileNumber = textField.text ?? ""
            self.isMobileValid = voucherGiftVM.isValidMobileNumber(countyryCode: countryCode,
                                                                   mobileNumber: mobileNumber)
           if self.isMobileValid{
               self.updatedMobileNumber = mobileNumber
           }
        }
    
        if textField.text?.count ?? 0 > 0 {
            self.refreshGiftingErrorLabels()
            return
        }
        
//        if let lblConstraint = textField == emailTextField ? emailPlaceHolderBottomConstraint : namePlaceHolderBottomConstraint{
//            changeLabelPosition(labelConstriant: lblConstraint, isExpand: false)
//        }
    }
    
//    private func changeLabelPosition(labelConstriant: NSLayoutConstraint, isExpand: Bool) {
//        labelConstriant.constant = isExpand ? 28 : 8
//
//        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
//            self.view.layoutIfNeeded()
//        }, completion: { finished in
//
//        })
//    }
    
}

extension BBQVoucherGiftingViewController: CNContactPickerDelegate{
    @IBAction func onClickBtnContacts(_ sender: Any) {
        let contactPicker = CNContactPickerViewController()
        contactPicker.delegate = self
        contactPicker.displayedPropertyKeys = [CNContactGivenNameKey, CNContactPhoneNumbersKey, CNContactEmailAddressesKey]
        self.present(contactPicker, animated: true, completion: nil)
    }
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        nameTextField.text = contact.givenName + " " + contact.familyName
      // changeLabelPosition(labelConstriant: namePlaceHolderBottomConstraint, isExpand: true)
        if contact.phoneNumbers.count > 0{
            if let numbers = contact.phoneNumbers.first?.value.stringValue.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "-", with: "").replacingOccurrences(of: "(", with: "").replacingOccurrences(of: ")", with: ""){
                phoneTextField.text = numbers
                if numbers.count > 10{
                    phoneTextField.text = String(numbers.suffix(10))
                }
               //phoneNumberCell?.changeLabelPosition(position: -19.0)
            }
        }
        if let email = contact.emailAddresses.first{
            emailTextField.text = email.value as String
           // changeLabelPosition(labelConstriant: emailPlaceHolderBottomConstraint, isExpand: true)
        }
    }
}
