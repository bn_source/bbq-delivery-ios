/*
 *  Created by Nischitha on 17/09/19.
 *  Copyright © BARBEQUE NATION HOSPITALITY LIMITED 2019. All rights reserved.
 
 * CHANGE LOG
 * —------------------------------------------------------------------------------
 * ID          Bug ID      Author Name          Date                  Description
 * —------------------------------------------------------------------------------
 * 1            NA         Nischitha         17/09/19       Initial Version
 * 2            NA         Nischitha         1/10/19        Review comments
 */

import UIKit
import SwiftyGif

class SplashScreenViewController: UIViewController {
    
    // This dispatch group has aligned with calling two APIs we had in splash screen.
    // Extra task is also added for animating gif atleast once.
    private var dispatchGroup : DispatchGroup?
    
    // MARK:- Properties
    
    let logoAnimationView = LogoAnimationView()
    let welcomeViewModel = WelcomeViewModel()
    var newTimestamp:String? = ""
    var welcomeData:[WelcomeData]?
    var isFirstTime = true
    var viewModel = DeliveryTabViewModel()
    // MARK: Lazy Inits
    
    lazy private var profileViewModel : BBQProfileViewModel = {
        let profileVM = BBQProfileViewModel()
        return profileVM
    } ()
    
    lazy var notificationViewModel : BBQNotificationTokenViewModel = {
        let viewModel = BBQNotificationTokenViewModel()
        return viewModel
    }()
    
    
    //MARK:- Life Cycle method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(logoAnimationView)
        logoAnimationView.pinEdgesToSuperView()
        logoAnimationView.logoGifImageView.delegate = self
        self.dispatchGroup = DispatchGroup()
        self.dispatchGroup?.enter() // Adding 1st Task
        self.logoAnimationView.logoGifImageView.startAnimatingGif()
        
        checkAppUpdate()
        
        VersionCheck.shared.isUpdateAvailable() {( hasUpdates ,versionLocal, versionAppStore , releaseNotes)  in
            
            // Use the singleton like this
            let singleton = GlobalAppVersion.sharedAppInformaion
            
             let  appInformationRetrieved = AppVersionInformation(localV: versionLocal, appStoreV: versionAppStore, notes: releaseNotes, needToUpdate: hasUpdates)
            
            // Add the struct instance to singleton
            singleton.informationAboutVersion = appInformationRetrieved
        }
        
    }
        // MARK: Data Fetch Methods
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !isFirstTime {
            goToLoginPage()
        }
        isFirstTime = false
    }
    
    private func loadAppData(){
        if !BBQUserDefaults.sharedInstance.isGuestUser {
            self.checkTokenExpiry()
        }
        
        self.fetchInitialLoadData()
    }
    
    func checkTokenExpiry() {
        if BBQSessionManager.isSessionExpired() == true {
            self.dispatchGroup?.enter() // Adding 3rd Task
            BBQSessionManager.refreshSessionTokens { (isRefreshed) in
                self.dispatchGroup?.leave()
                isTokenExpiredChecked = true
                print("Token Refreshed",isRefreshed)
            }
        }else{
            isTokenExpiredChecked = true
        }
    }
    
    private func fetchInitialLoadData() {
        // Profile information fetch in case of logged in user
        if !BBQUserDefaults.sharedInstance.isGuestUser {
            
            self.dispatchGroup?.enter() // Adding 3rd Task
            self.profileViewModel.getUserProfile { (isFetched) in
                // Leaving 3rd Task
                self.dispatchGroup?.leave()
                if let userModel =  self.profileViewModel.profileData{
                    BBQUserDefaults.sharedInstance.CurrentSelectedLocation = (userModel.lastVisitedBranchName)!
                    BBQUserDefaults.sharedInstance.branchIdValue = userModel.lastVisitedBranch ?? BBQUserDefaults.sharedInstance.branchIdValue
                    BBQUserDefaults.sharedInstance.signUpReferralCode = (userModel.referralCode)
                    
                    let userDict = ["userSelectedLocation": userModel.lastVisitedBranchName,
                                    "selectedBrachID": userModel.lastVisitedBranch]
                    NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.NotificationName.receiedUserProfileData), object: nil, userInfo: userDict as [AnyHashable : Any])
                    
                }
            }
        }
        
//        if BBQUserDefaults.sharedInstance.signupCount < Constants.App.Validations.signupCount && BBQUserDefaults.sharedInstance.accessToken == ""{
//            // Welcome page data fetch
//            self.dispatchGroup?.enter() // Adding 4th Task
//            self.welcomeViewModel.getWelcomeInfo { (result) in
//
//                // Leaving 4th Task
//                self.dispatchGroup?.leave()
//
//                if result == true {
//                    if let model = self.welcomeViewModel.welcomeModel {
//                        self.newTimestamp = model.timestamp
//                    }
//                } else {
//                    print("no data")
//                }
//            }
//        }
        
        
        self.dispatchGroup?.notify(queue: DispatchQueue.main) {
            self.notificationViewModel.checkAnUpdateFCMToken()
            self.navigateFromSplashScreen()
        }
        
    }
    
    
    // MARK:- UI method
    
    func goToWelcomePage(newTimeStamp: String) {
        let viewController = UIStoryboard.loadWelcomePage()
      //  viewController.newTimeStamp = newTimeStamp
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
//    func goToHomePage() {
//        //let tabViewController = UIStoryboard.loadHomeViewController()
//        //self.navigationController?.pushViewController(tabViewController, animated: true)
//        let tabBarController = DeliveryTabBarController()
//        ///CustomUI:
////        let viewController = UIStoryboard.loadPayment()
////        self.navigationController?.pushViewController(viewController, animated: true)
//        self.navigationController?.pushViewController(tabBarController, animated: true)
//
//    }
    
//    func goToLoginPage() {
//        let tabViewController = UIStoryboard.loadLoginBackground()
//        self.navigationController?.pushViewController(tabViewController, animated: true)
//    }
    
    func navigateFromSplashScreen()  {
        self.logoAnimationView.isHidden = true
        var isChecked = false
        if let newTimestamp = newTimestamp, newTimestamp != ""{
            isChecked = true
        }
//        if isChecked && (BBQUserDefaults.sharedInstance.welcomeTimeStamp != newTimestamp || newTimestamp != "") {
//            goToWelcomePage(newTimeStamp: newTimestamp!)
//        } else {
        if BBQUserDefaults.sharedInstance.accessToken == "" {
            if BBQUserDefaults.sharedInstance.signupCount >= Constants.App.Validations.signupCount {
                goToHomePage()
            } else {
                goToLoginPage()
            }
        } else {
            goToHomePage()
        }
//        }
    }
    
}

extension SplashScreenViewController: SwiftyGifDelegate {
    
    func gifDidStop(sender: UIImageView) {
        self.dispatchGroup?.leave()
    }
    
    func showCancellationOption() {
        let cancellationVC = UIStoryboard.loadAfterDining()
        cancellationVC.modalPresentationStyle = .overFullScreen
        self.present(cancellationVC, animated: true) {
        }
    }
}
extension SplashScreenViewController{
    private func checkAppUpdate() {
        self.viewModel.getAppupdateInformation { (isUpdateRequired) in
            if isUpdateRequired {
                self.showAppUpdateWarining(doLocationUpdate: true)
            } else {
                self.loadAppData()
            }
        }
    }
    
    private func showAppUpdateWarining(doLocationUpdate: Bool) {
        
        if viewModel.isForceUpdate {
            PopupHandler.showSingleButtonsPopup(title: kAppUpdateTitle,
                                                message: kAppUpdateMessage,
                                                on: self,
                                                firstButtonTitle: kAppUpdateButtonString) {
                                                    self.openAppstoreForUpdate()
            }
        } else if viewModel.isRecommendUpdate {
            PopupHandler.showTwoButtonsPopup(title: kAppUpdateTitle,
                                             message: kAppUpdateMessage,
                                             isCentered: true,
                                             on: self,
                                             firstButtonTitle: kAppUpdateButtonString,
                                             firstAction: {
                                                self.openAppstoreForUpdate()
                                                
            }, secondButtonTitle: kCancelString) {
                if doLocationUpdate {
                    self.loadAppData()
                }
            }
        }
        
    }
    
    private func openAppstoreForUpdate() {
        if let url = URL(string: "itms-apps://itunes.apple.com/app/id1080269411"),
            UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}
