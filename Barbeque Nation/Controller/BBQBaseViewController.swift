//
//  Created by Chandan Singh on 21/09/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQBaseViewController.swift
//

import UIKit

class BBQBaseViewController: UIViewController, BBQRazorpayControllerProtocol, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var headerView: BBQHomeHeaderView!
    
    //MARK: Location Properties
    var latitudeValue: Double = 0.0
    var longitudeValue: Double = 0.0
    var branchIDValue: String = ""
    var branchName: String = ""
    
    var itemsQuantity = [Int]()
    
    private var cartPoints = 0
    private var cartCouponBarCode = ""
    private var totalCartDiscount = "0"
    private var isCartPaymentInProgress = false
    var isNeedLoadLoyalityPoints = true
    
    var delegate: BaseViewControllerDelegate?
    var navigationFromScreen: LoginNavigationScreen = .Splash
    
    //MARK: Cart Properties
    var cartModel: ModifyCartModel? = nil
    lazy var cartViewModel : CartViewModel = {
        let viewModel = CartViewModel()
        return viewModel
    }()
    
   
    //MARK: RazorPay Properties
    lazy private var paymentControllerViewModel : MakePaymentViewModel = {
        let paymentVM = MakePaymentViewModel()
        return paymentVM
    }()
    
    let appDelegateReference: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
    var notificationArray:NSMutableArray? = nil
    var readCount = 0
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if isNeedLoadLoyalityPoints{
            self.getUpdatedPoints()
        }

        // Do any additional setup after loading the view.
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(locationDataUpdated(_:)), name: Notification.Name(rawValue:Constants.NotificationName.locationDataUpdate), object:nil)
        NotificationCenter.default.addObserver(self, selector: #selector(dismissOutletSelection), name: Notification.Name(rawValue:Constants.NotificationName.dismissOutletSelection), object:nil)

       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        //checkCartData()
        initializeHeaderView()
    }

    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    //MARK: Header Methods
     func initializeHeaderView() {
        guard let headerObj = self.headerView else {
            return
        }
        headerObj.homeHeaderDelegate = self
        if BBQUserDefaults.sharedInstance.CurrentSelectedLocation == "" {
            //headerObj.locationButton?.setAttributedTitle(setAttrubuttedTitleForButtonsWith(title: kSelectLocation), for: .normal)
            headerObj.locationLabel.text = "Select location"
        } else {
            let branchName = BBQUserDefaults.sharedInstance.CurrentSelectedLocation
            //headerObj.locationButton?.setAttributedTitle(setAttrubuttedTitleForButtonsWith(title:  " \(branchName) "), for: .normal)
            headerObj.locationLabel.text = branchName
        }
    }
    
    func hideNavigationBar(_ hidden: Bool, animated: Bool) {
        navigationController?.setNavigationBarHidden(hidden, animated: animated)
    }
    
    
    //MARK: Cart Methods
    @objc func checkCartData(completion: ((_ success: Bool) -> Void)? = nil) {
        if !BBQUserDefaults.sharedInstance.isGuestUser {
            self.headerView.cartButton.isHidden = true
            cartViewModel.getCart { [weak self] (isSuccess) in
                if let self = self {
                    if isSuccess {
                        self.cartModel = self.cartViewModel.getModifyCartModel
                        
                        if let model = self.cartModel, let data = model.orderItems, data.count > 0 {
                            self.headerView.cartButton.isHidden = false
                            self.headerView.setCartButtonIcon(dataAvailable: true)
                        } else {
                            //Hiding the cart icon if nothing is present in cart.
                            //Uncomment below line and comment last 2 line to show empty cart icon.
                            //self.headerView.setCartButtonIcon(dataAvailable: false)
                            self.headerView.cartButton.isHidden = true
                            //self.dismiss(animated: true, completion: nil)
                        }
                        completion?(true)
                    } else {
                        self.headerView.cartButton.isHidden = true
                        self.headerView.setCartButtonIcon(dataAvailable: false)
                        completion?(false)
                    }
                }
            }
        } else {
            self.headerView.cartButton.isHidden = true
        }
    }
    
    //MARK: Location update
    @objc func locationDataUpdated(_ notification: Notification) {
        if let dictionary = notification.userInfo as NSDictionary? {
            self.latitudeValue = (dictionary["lat"] as! NSString).doubleValue
            self.longitudeValue = (dictionary["long"] as! NSString).doubleValue
            self.branchIDValue = dictionary["branchId"] as! String
            
            let outletBranchName = dictionary["branchName"] as! String
            BBQUserDefaults.sharedInstance.CurrentSelectedLocation = outletBranchName
            AnalyticsHelper.shared.triggerEvent(type: .location_success_status)
            AnalyticsHelper.shared.triggerEvent(type: .H11A)
            //self.headerView.locationButton?.setAttributedTitle(setAttrubuttedTitleForButtonsWith(title:  " \(outletBranchName) "),for: .normal)
            if headerView != nil, headerView.locationLabel != nil{
                headerView.locationLabel.text = outletBranchName
            }
        }
    }
    
    @objc func dismissOutletSelection(){
        
    }
    
    func setAttrubuttedTitleForButtonsWith(title:String)->NSAttributedString {
        let fullString = NSMutableAttributedString(string: " " + title)
        let range = (title as NSString).range(of: title)
        fullString.addAttribute(NSAttributedString.Key.foregroundColor,
                                value: UIColor.white ,
                                range: range)
        
        let imageAttachment = NSTextAttachment()
        imageAttachment.image = UIImage(named: Constants.AssetName.location)
        let iconsSize = CGRect(x: 0, y: -1, width: 10, height: 12)
        imageAttachment.bounds = iconsSize
        
        let imageString = NSAttributedString(attachment: imageAttachment)
        fullString.append(imageString)
        fullString.append(NSAttributedString(string: "  "))
        return fullString
    }
    
    //Get the Notification Data to Maintain the readunread status
    //This function will trigger when logout
    func loadNotificationList() {
        if let appDelegate = appDelegateReference {
            self.notificationArray = appDelegate.loadNotificationData()
        }
        
        let filteredData = self.notificationArray?.filter{
            let bool = ($0 as AnyObject).value(forKey: Constants.App.NotificationPage.readunread) as! Bool
            return bool
        }
        readCount = filteredData?.count ?? 0
        if let count = self.notificationArray?.count {
            if readCount == count {
                UserDefaults.standard.set(false, forKey: Constants.App.NotificationPage.notificationActive)
            }else{
                UserDefaults.standard.set(true, forKey: Constants.App.NotificationPage.notificationActive)
            }
        }
    }
    //MARK: User Account Action
    func doPostAccountProcessing() {
        getUpdatedPoints()
        self.headerView.addTheInitialValues()
        checkCartData()
        self.initializeHeaderView()
    }
    
    //MARK: RazorPay Payment Methods
    func onRazorpayPaymentError(_ code: Int32, description str: String) {
        //Show Payment Failed error
        AnalyticsHelper.shared.triggerEvent(type: .HC10B)
        self.isCartPaymentInProgress = false
        if code == 2 { //Payment Cancelled
            self.showTransactionCancelled()
        }
    }
    
    func onRazorpayPaymentSuccess(_ paymentData: BBQPaymentDataModel) {
        AnalyticsHelper.shared.triggerEvent(type: .HC10A)
        self.performCartCheckout(paymentData: paymentData)
    }
    
    @objc func showTransactionCancelled() {
        let keyWindow = UIApplication.shared.keyWindow?.rootViewController?.presentedViewController ?? UIApplication.shared.keyWindow?.rootViewController
        
        guard let window = keyWindow else {
            return
        }
        
        PopupHandler.showSingleButtonsPopup(title: kMyCartTransactionCancelTitle,
                                            message: kMyCartTransactionCancelMessage,
                                            on: window,
                                            firstButtonTitle: kButtonOkay) { }
    }
    
    func performCartCheckout(paymentData: BBQPaymentDataModel) {
      //  let keyWindow = UIApplication.shared.keyWindow?.rootViewController?.presentedViewController
        BBQActivityIndicator.showIndicator(view: self.view)
        AnalyticsHelper.shared.triggerEvent(type: .HC11)
        paymentControllerViewModel.paymentCheckout(points: self.cartPoints, barCode: self.cartCouponBarCode, razorpayPaymentId: paymentData.paymentID ?? "", razorpayOrderId: paymentData.razorpayOrderID ?? "", razorpaySignature: paymentData.razorpaySignature ?? "") { (isSuccess) in
            BBQActivityIndicator.hideIndicator(from: self.view)
            self.isCartPaymentInProgress = false
            if isSuccess {
                //Show Payment Success
                AnalyticsHelper.shared.triggerEvent(type: .HC11A)
                self.showPaymentSuccessScreen()
                AnalyticsHelper.shared.triggerEvent(type: .Happinesscard_Order_Status, properties: [.Happiness_Card_Success: AnalyticsHelper.MoEngageAttrValue.Success.rawValue])
            } else {
                //Show Payment Failed error
                AnalyticsHelper.shared.triggerEvent(type: .HC11B)
                self.showTransactionFailed()
                AnalyticsHelper.shared.triggerEvent(type: .Happinesscard_Order_Status, properties: [.Happiness_Card_Success: AnalyticsHelper.MoEngageAttrValue.Failed.rawValue, .Error: kMyCartTransactionFailedMessage])
            }
        }
    }
    
    @objc func showPaymentSuccessScreen() {
        let registrationConfirmationVC = UIStoryboard.loadRegistrationConfirmation()
        registrationConfirmationVC.modalPresentationStyle = .overFullScreen
        registrationConfirmationVC.titleText = kPaymentConfirmationTitleText
        registrationConfirmationVC.navigationText = kVoucherPurchase
        
        let keyWindow = UIApplication.shared.keyWindow?.rootViewController?.presentedViewController ?? UIApplication.shared.keyWindow?.rootViewController
        keyWindow?.present(registrationConfirmationVC, animated: false) {
            sleep(UInt32(2.0))
            
            self.dismiss(animated: true) {
                self.checkCartData()
                self.goToHome()
                self.getUpdatedPoints()
            }
        }
    }
    
    private func showTransactionFailed() {
        let keyWindow = UIApplication.shared.keyWindow?.rootViewController?.presentedViewController ?? UIApplication.shared.keyWindow?.rootViewController
        
        guard let window = keyWindow else {
            return
        }
        
        AnalyticsHelper.shared.triggerEvent(type: .Happinesscard_Order_Status, properties: [.Happiness_Card_Success: AnalyticsHelper.MoEngageAttrValue.Failed.rawValue, .Error: kMyCartTransactionFailedMessage])
        
        PopupHandler.showSingleButtonsPopup(title: kMyCartTransactionFailedTitle,
                                            message: kMyCartTransactionFailedMessage,
                                            on: window,
                                            firstButtonTitle: kButtonOkay) { }
    }
    
     func getUpdatedPoints() {
        if BBQUserDefaults.sharedInstance.accessToken != "" {
            self.cartViewModel.getLoyaltyPointsCount { (isSuccess) in
                if isSuccess {
                    if let remainingPointsModel = self.cartViewModel.getPointCountModel, let remainingPoints = remainingPointsModel.remainingPoints {
                        BBQUserDefaults.sharedInstance.UserLoyaltyPoints = remainingPoints
                      //  self.headerView.configureBBQPointUI()
                        self.delegate?.didUpdatedLoyalityPoints(points: String(remainingPoints))
                    }
                }
            }
        }
    }
    
    func outletClicked(){
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: UIScreen.main.bounds.height - 165)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        let viewController = BBQLocationViewController()
        //        if BBQLocationManager.shared.currentLocationManager.location == nil{
        //            BBQUserDefaults.sharedInstance.userDeniedLocation = true
        //        }
        viewController.location = BBQLocationManager.shared.currentLocationManager.location
        viewController.setupPresentingController(controller: bottomSheetController)
        bottomSheetController.present(viewController, on: self, viewHeight: UIScreen.main.bounds.height - 165)

    }
    
    func shareYourReferalCode(completion: ((_ success: Bool) -> Void)? = nil){
        
        
        BBQServiceManager.getInstance().ShareYourReferralCodeToShare(params: ["referral_code": BBQUserDefaults.sharedInstance.YourReferalCode , "customer_id" : BBQUserDefaults.sharedInstance.customerId]) { error, response in
            
            if error == nil && response != nil {
                completion?(true)

            }
            
        }

                completion?(false)
        }
    
}

//MARK: Header View Protocol
extension BBQBaseViewController: BBQHomeHeaderViewProtocol {
    func hamburgerClicked(actionSender: UIButton) {
        if let rootVC = UIApplication.shared.keyWindow?.rootViewController as? DeliveryHostMenuViewController{
            rootVC.showSideMenu(viewController: self, delegate: self)
        }
//        let menuViewController = UIStoryboard.loadMenuViewController()
//        menuViewController.delegate = self
//        self.addChild(menuViewController)
//        self.view.addSubview(menuViewController.view)
//        self.view.endEditing(true)
//        menuViewController.didMove(toParent: self)
    }
    
    func notificationClicked(actionSender: UIButton) {
        AnalyticsHelper.shared.triggerEvent(type: .H10)
        let  notificationController = UIStoryboard.loadNotificationViewController()
        replaceControllerInCurrentStack(new: notificationController)
    }
    
    func mapClicked(actionSender: UIButton) {
        AnalyticsHelper.shared.triggerEvent(type: .location_status)
        AnalyticsHelper.shared.triggerEvent(type: .H11)
        self.outletClicked()
    }
    
    
    
    @objc func cartButtonClicked() {
        //Cart
        AnalyticsHelper.shared.triggerEvent(type: .H09)
       BBQActivityIndicator.showIndicator(view: self.view)
        checkCartData { (isSuccess) in
            BBQActivityIndicator.hideIndicator(from: self.view)
            if isSuccess {
                self.showCartBottomSheet()
            }
        }
    }
    
    func showCartBottomSheet() {
        guard let cartDataModel = self.cartModel, let data = cartDataModel.orderItems, data.count > 0 else {
            return
        }
        let cartViewController = BBQMyCartViewController(nibName: Constants.CellIdentifiers.cartControllerNib, bundle: nil)
        cartViewController.cartViewModel = self.cartViewModel
        cartViewController.cartModel = cartDataModel
        cartViewController.superVC = self.navigationController?.viewControllers.last
//        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: UIScreen.main.bounds.size.height - CGFloat(self.calculateCartScreenInitialHeight()))
//        let bottomSheetController = BottomSheetController(configuration: configuration)
        cartViewController.delegate = self
     //   cartViewController.bottomSheet = bottomSheetController
       // bottomSheetController.present(cartViewController, on: self)
        self.navigationController?.pushViewController(cartViewController, animated: true)
    }
    
    func calculateCartScreenInitialHeight() -> Int {
        if let cartDataModel = self.cartModel, let data = cartDataModel.orderItems, data.count > 0 {
            let totalItemCount = (data.count > 4) ? 4 : data.count
            var totalHeight = totalItemCount * 72 //Content Cells height - Show Max 4
            
            totalHeight += 65 + 40 + 35 + 35 + 57 + 103 + Int(BBQUserDefaults.sharedInstance.getBottomSafeAreaHeight())
            return totalHeight
        }
        return Int(UIScreen.main.bounds.size.height)
    }
    
    func pointsClicked(actionSender: UIButton) {
        AnalyticsHelper.shared.triggerEvent(type: .H05)
        if (BBQUserDefaults.sharedInstance.accessToken.count == 0) {
            //guest user
            ToastHandler.showToastWithMessage(message: KLoyaltyLoginText)
        }else{
            //let  loyalityPointsViewController = UIStoryboard.loadLoyalityPointsViewController()
            let smileHistoryViewController = UIStoryboard.loadSmileHistoryViewController()
            self.navigationController?.pushViewController(smileHistoryViewController, animated: true)
        }
    }
}

//MARK: Cart Protocol
extension BBQBaseViewController: BBQMyCartViewControllerProtocol {
    func didPressProceedToPayment(points: Int, barCode: String, amount: String, currency: String, discount: String, bottomSheet: BottomSheetController?) {
        AnalyticsHelper.shared.triggerEvent(type: .HC10)
        self.cartPoints = points
        self.cartCouponBarCode = barCode
        self.totalCartDiscount = discount
        self.isCartPaymentInProgress = true
        
        let amountRoundedUp = Double(amount) ?? 0.0
        let amountString = String(format: "%.2lf", amountRoundedUp)
        let paymentParameters = [amountString, currency]
        if amountString != "0.00" {
            self.initiatePayment(paymentParameters: paymentParameters)
        } else {
            //Send everything empty.
            self.performCartCheckout(paymentData: BBQPaymentDataModel())
        }
    }
    
    @objc func initiatePayment(paymentParameters: [String]) {
        let paymentController = BBQRazorpayContainerController()
        paymentController.payDelegate = self
        paymentController.modalPresentationStyle = .overCurrentContext
        let keyWindow = UIApplication.shared.keyWindow?.rootViewController?.presentedViewController ?? UIApplication.shared.keyWindow?.rootViewController
        keyWindow?.present(paymentController, animated: true, completion: nil)
        
        let amount = Double(paymentParameters[0])
        if self.isCartPaymentInProgress {
            paymentController.createOrderForCartPayment(with: amount ?? 0.0, currency: paymentParameters[1], title: "", and: "", discount: self.totalCartDiscount, loyalty_points: String(self.cartPoints), bar_code: self.cartCouponBarCode)
        } else {
            paymentController.createOrderForPayment(with: amount ?? 0.0, currency: paymentParameters[1], title: "", and: "", bookingID: "", loyalty_points: self.cartPoints)
        }
    }
    
    func updateCartData() {
        checkCartData()
        
        NotificationCenter.default.post(name: Notification.Name(Constants.NotificationName.cartDataUpdated), object: nil)
    }
}

extension BBQBaseViewController: BBQMenuViewControllerProtocol {
    func showBookingScreen() {
        if let tabBarController = navigationController?.viewControllers.first(where: {$0 is BBQTabbarController}) as? DeliveryTabBarController {
            tabBarController.selectedIndex = 0
        }
        NotificationCenter.default.post(name: Notification.Name(Constants.NotificationName.showBookingScreen),
                                        object: nil)
    }
    
    func postUserAccountProcessing() {
        //self.getUpdatedPoints()
        self.loadNotificationList()
        self.doPostAccountProcessing()
        self.goToHome()
    }
    
    func showPromotionsAndOffers() {
        BBQActivityIndicator.showIndicator(view: self.view)
        let homeModel = HomeViewModel()
        homeModel.getPromosVouchers(latitide: self.latitudeValue, longitude: self.longitudeValue, brach_ID: BBQUserDefaults.sharedInstance.branchIdValue) { (isSuccess) in
            
            BBQActivityIndicator.hideIndicator(from: self.view)
            
            let promosArray = homeModel.getPromotionsDataData()
            let promoViewModel = PromosViewModel(promosDataArray: promosArray)
            
            let viewController = PromotionOfffersVC()
            viewController.promoDelegate = self as? BottomSheetImagesViewControllerProtocol
            viewController.promoScreenType = .Hamburger
            viewController.promosData = promoViewModel.promosArray            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func goToHome() {
        navigateToHomePage()
        if BBQUserDefaults.sharedInstance.accessToken.count>0{
            
            self.initializeHeaderView()
        }else{
            self.initializeHeaderView()
            NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.NotificationName.userLoggedOut), object: nil, userInfo: nil)
        }
    }
}

extension UIViewController {
    //MARK: Navigation Stack Modification
    func replaceControllerInCurrentStack(new controller: UIViewController) {
        self.navigationController?.pushViewController(controller, animated: true)
//        let currentNavigationStack = self.navigationController?.viewControllers
//
//        guard var modifyingStack = currentNavigationStack else {
//            return
//        }
//
//        var controllerFoundIndex = -1
//        //Iterate through current navigation stack
//        for (index, item) in modifyingStack.enumerated() {
//            if ((item.isKind(of: controller.classForCoder))) {
//                controllerFoundIndex = index
//                break
//            } else {
//                controllerFoundIndex = -1
//            }
//        }
//
//        if controllerFoundIndex != -1 {
//            if controllerFoundIndex != modifyingStack.count - 1 {
//                //Controller is found in the stack but it is not the last in stack. Remove later items from stack.
//                modifyingStack.removeSubrange((controllerFoundIndex + 1)...(modifyingStack.count - 1))
//                //isOperationCompleted = true
//            } else {
//                //Controller is found in the stack and it is the last. Do not do anything.
//                //isOperationCompleted = true
//            }
//        } else {
//            //Controller not found in the stack. Remove last item and insert
//            if let lastController = modifyingStack.last {
//                if ((lastController.isKind(of: BBQHomeViewController.self))) {
//                    modifyingStack.append(controller)
//                } else {
//                    modifyingStack.removeLast()
//                    modifyingStack.append(controller)
//                }
//            }
//        }
//
//        self.navigationController?.viewControllers = modifyingStack
    }
}
protocol BaseViewControllerDelegate{
    func didUpdatedLoyalityPoints(points: String)
}


// MARK: - UINavigationControllerDelegate

extension BBQBaseViewController: UINavigationControllerDelegate {

    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
//        guard let swipeNavigationController = navigationController else { return }
//
//        duringPushAnimation = false
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
            return true
        }
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        // Disable pop gesture in two situations:
        // 1) when the pop animation is in progress
        // 2) when user swipes quickly a couple of times and animations don't have time to be performed
        if let _ = navigationController?.viewControllers.last as? DeliveryTabBarController{
            if headerView != nil{
                headerView.hamburgerButtonClicked(headerView.hamburgerButton)
            }
        }else{
            if (navigationController?.viewControllers.count ?? 0) > 1 {
                AnalyticsHelper.shared.triggerEvent(type: .UI01)
                self.navigationController?.popViewController(animated: true)
            }
        }
        return false
    }
}

