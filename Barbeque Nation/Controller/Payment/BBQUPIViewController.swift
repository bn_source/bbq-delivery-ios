//
 //  Created by vinoth kumar on 25/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQUPIViewController.swift
 //

import UIKit

class BBQUPIViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var upiTextField:UITextField!
    @IBOutlet weak var viewForTextField: UIView!
    @IBOutlet weak var btnPay: UIButton!
    @IBOutlet weak var btnSaveUPI: UIButton!
    var isFirstCell: Bool = false
    var isLastCell: Bool = false
    var delegate: BBQUPIViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        upiTextField.delegate = self
        viewForTextField.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 1.0)
        btnPay.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0)
        //upiTextField.becomeFirstResponder()
        view.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        // Do any additional setup after loading the view.
    }
    @IBAction func payPressed(_ sender:UIButton) {
        if(self.upiTextField.text?.isValidVPA ?? false) {
            delegate?.proccess(withUpiID: upiTextField.text ?? "", save: btnSaveUPI.isSelected)
            self.dismiss(animated: true)
        }
        else {
            self.alert(message: "Please enter valid VPA");
        }
    }
    @IBAction func saveButtonClicked(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    @IBAction func onClickBtnBack(_ sender: Any) {
        delegate?.cancelTheUpiAdd()
        self.dismiss(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

protocol BBQUPIViewControllerDelegate{
    func cancelTheUpiAdd()
    func proccess(withUpiID upiId: String, save: Bool)
}
