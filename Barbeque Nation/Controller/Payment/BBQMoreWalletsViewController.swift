//
 //  Created by vinoth kumar on 12/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQMoreBanksViewController.swift
 //

import UIKit

class BBQMoreWalletsViewController: UIViewController {

    @IBOutlet weak var walletTableView:UITableView!
    var sections:[CellSection] = [CellSection.init(data: [], type: BBQNetBankTableViewCell.self)]
    var isFirstCell: Bool = false
    var isLastCell: Bool = false
    var delegate: BBQMoreWalletsViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        walletTableView.delegate = self
        walletTableView.dataSource = self
        // Do any additional setup after loading the view.
    }
    func setupUI() {
        view.maskByRoundingCorners(cornerRadius: 10, maskedCorners: CornerMask.topCornersMask)
        for i in sections {
            let string = String.init(describing: i.type.self)
            walletTableView.register(string.nib, forCellReuseIdentifier: string)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.walletTableView.reloadData()
    }
    
    @IBAction func onClickBtnBack(_ sender: Any) {
        delegate?.cancelTheWallet()
        self.dismiss(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension BBQMoreWalletsViewController:UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        sections[section].data?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.getCell(cell: sections[indexPath.section].type as! UITableViewCell.Type, indexPath: indexPath)
        cell.selectionStyle = .none
        if var DR = cell as? CellDataRecevier {
            DR.didSetData(data: (sections[indexPath.section].data?[indexPath.row])!)
            DR.delegate = self
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let wallet = self.sections[indexPath.section].data?[indexPath.row] as? Wallet {
            self.delegate?.proccess(wallet: wallet)
            self.dismiss(animated: true)
        }
    }

}
//extension BBQMoreBanksViewController:SelectedPaymentDelegate {
//    func didSelectNetbank(bank: NetBank) {
//    }
//}
protocol BBQMoreWalletsViewControllerDelegate{
    func cancelTheWallet()
    func proccess(wallet: Wallet)
}
