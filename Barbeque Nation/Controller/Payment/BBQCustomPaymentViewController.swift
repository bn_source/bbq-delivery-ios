//
//  Created by vinoth kumar on 08/01/21
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
//  All rights reserved.
//  Last modified BBQCustomPaymentViewController.swift
//

import UIKit
import ShimmerSwift
import WebKit
import Razorpay

class BBQCustomPaymentViewController: BaseViewController {
    
    @IBOutlet weak var mainTableView:UITableView!
    @IBOutlet weak var windlessViewContainer: UIView!
    @IBOutlet weak var windlessView: UIView!
    
    var bottomSheetController: BottomSheetController?
    var shimmerView : ShimmeringView?
    var delegate: BBQCustomPaymentViewControllerDelegate?
    
    var viewModel:CustomPaymentViewModel!
    var emailAddCompletion: ((Bool) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        mainTableView.delegate = self
        mainTableView.dataSource = self
        showShimmering()
        
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: 80)
        bottomSheetController = BottomSheetController(configuration: configuration)
        viewModel.delegate = self
        fetchPaymentMethods()
        AnalyticsHelper.shared.triggerEvent(type: .CP01)
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
        
    }
    
    func showShimmering() {
        
        self.windlessView.isHidden = false
        self.windlessViewContainer.isHidden = false
        
        if let shimmer = self.shimmerView{
            self.windlessViewContainer.addSubview(shimmer)
            shimmer.contentView = windlessView
            
            shimmer.shimmerAnimationOpacity = 0.2
            shimmer.shimmerSpeed = 500.00
            shimmer.isShimmering = true
        }else{
            shimmerView = ShimmeringView(frame: windlessViewContainer.frame)
            if let shimmer = self.shimmerView{
                self.windlessViewContainer.addSubview(shimmer)
                shimmer.contentView = windlessView
                
                shimmer.shimmerAnimationOpacity = 0.2
                shimmer.shimmerSpeed = 500.00
                shimmer.isShimmering = true
            }
        }
    }
    
    func stopShimmering() {
        
        if let shimmer = self.shimmerView {
            shimmer.isShimmering = false
        }
        self.windlessView.isHidden = true
        self.windlessViewContainer.isHidden = true
    }
    
    override func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    func setupUI() {
        let header = CustomPaymentHeader.init(title: "Payment Methods", title1: "", subtitle: "Total: ", subtitle1: "₹\(self.viewModel.billAmount)")
        _ = self.addHeader(data: header)
        for i in viewModel.sections {
            let string = String.init(describing: i.type.self)
            mainTableView.register(string.nib, forCellReuseIdentifier: string)
        }
        let string = String.init(describing: BBQSavedCardTableViewCell.self)
        let UpiCard = String.init(describing: BBQSavedUPITableViewCell.self)
        let moreWallets = String.init(describing: MoreWalletsTableViewCell.self)
        let upiApps = String.init(describing: BBQAppUPITableViewCell.self)
        
        mainTableView.register(string.nib, forCellReuseIdentifier: string)
        mainTableView.register(UpiCard.nib, forCellReuseIdentifier: UpiCard)
        mainTableView.register(moreWallets.nib, forCellReuseIdentifier: moreWallets)
        mainTableView.register(upiApps.nib, forCellReuseIdentifier: upiApps)
    }
    
    func fetchPaymentMethods() {
        self.viewModel.createCustomer { (isSucess) in
            self.viewModel.fetchUserTokens { status in
                
                DispatchQueue.main.async {
                    self.stopShimmering()
                    if(!status) {
                        self.view.makeToast("Error fetching payment methods, Please try again after some time")
                    }
                    self.mainTableView.reloadData()
                }
            }
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension BBQCustomPaymentViewController:UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        viewModel.sections.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == CustomPaymentSection.wallets.rawValue, (viewModel.sections[section].data?.count ?? 0) > 3{
            return 4
        }
        return viewModel.sections[section].data?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.getCell(cell: viewModel.sections[indexPath.section].type as! UITableViewCell.Type, indexPath: indexPath)
        if var data = viewModel.sections[indexPath.section].data?[indexPath.row]{
            data.isFirstCell = false
            data.isLastCell = false
            if  indexPath.row == 0{
                data.isFirstCell = true
            }
            if ((viewModel.sections[indexPath.section].data?.count ?? 0) - 1) == indexPath.row{
                data.isLastCell = true
            }
            viewModel.sections[indexPath.section].data?[indexPath.row] = data
        }
        
        //Exception for saved cards
        if(((viewModel.sections[indexPath.section].data?[indexPath.row]) as? SavedCard) != nil) {
            cell = tableView.getCell(cell: BBQSavedCardTableViewCell.self, indexPath: indexPath)
        }else if(((viewModel.sections[indexPath.section].data?[indexPath.row]) as? SavedUPI) != nil) {
            cell = tableView.getCell(cell: BBQSavedUPITableViewCell.self, indexPath: indexPath)
        }else if(((viewModel.sections[indexPath.section].data?[indexPath.row]) as? UPIApp) != nil) {
            cell = tableView.getCell(cell: BBQAppUPITableViewCell.self, indexPath: indexPath)
        }else if indexPath.section == CustomPaymentSection.wallets.rawValue, indexPath.row >= 3{
            cell = tableView.getCell(cell: MoreWalletsTableViewCell.self, indexPath: indexPath)
        }
        
        cell.selectionStyle = .none
        if var DR = cell as? CellDataRecevier {
            if indexPath.section == CustomPaymentSection.wallets.rawValue, indexPath.row >= 3{
                DR.didSetData(data: StringData(text: "More Wallets",image: ""))
            }else{
                DR.didSetData(data: (viewModel.sections[indexPath.section].data?[indexPath.row])!)
            }
            DR.delegate = self
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        viewModel.sections[section].options?.header
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if viewModel.sections[section].options?.separator ?? false {
            return 0
        }
        return 0.01
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
}
extension BBQCustomPaymentViewController:SelectedPaymentDelegate, BBQPaymentEmailViewControllerDelegate {
    func didSelectAppUPI(upi: UPIApp) {
        if let data = self.viewModel.sections[CustomPaymentSection.cards.rawValue].data {
            for i in 0..<data.count {
                self.viewModel.sections[CustomPaymentSection.cards.rawValue].data?[i].isSelected = false
            }
        }
        if let data = self.viewModel.sections[CustomPaymentSection.upi.rawValue].data {
            for i in 0..<data.count {
                
                self.viewModel.sections[CustomPaymentSection.upi.rawValue].data?[i].isSelected = false
                if let saved = data[i] as? UPIApp {
                    if(upi.appPackageNam == saved.appPackageNam) {
                        self.viewModel.sections[CustomPaymentSection.upi.rawValue].data?[i].isSelected = true
                    }
                }
            }
        }
        self.mainTableView.reloadData()
    }
    
    func didSelectPayAppUPI(upi: UPIApp) {
        createPaymentId { paymentId, errorString in
            if let _ = paymentId{
                AnalyticsHelper.shared.triggerEvent(type: .CP02A)
                self.presentWebView(section: .upi)
                self.viewModel.payusingAppUPI(upi: upi)
            }
        }
    }
    
    func cancelTheEmailAdd() {
        if let emailAddCompletion = emailAddCompletion{
            emailAddCompletion(false)
        }
        emailAddCompletion = nil
    }
    
    func proccess(withEmailId emailId: String) {
        BBQUserDefaults.sharedInstance.customPaymentEmail = emailId
        if let emailAddCompletion = self.emailAddCompletion{
            emailAddCompletion(true)
        }
        self.emailAddCompletion = nil
    }
    
    func addEmailPressed(completion: @escaping (Bool) -> Void) {
        if (SharedProfileInfo.shared.profileData?.email ?? "") != "" || BBQUserDefaults.sharedInstance.customPaymentEmail != "" {
            completion(true)
            self.emailAddCompletion = nil
        }else{
            if let vc = UIStoryboard.loadFromPayment("BBQPaymentEmailViewController") as? BBQPaymentEmailViewController{
                self.emailAddCompletion = completion
                vc.delegate = self
                bottomSheetController?.present(vc, on: self, viewHeight: 240)
                AnalyticsHelper.shared.triggerEvent(type: .CP02B)
            }
        }
    }
    
    func moreWalletsClicked() {
        let vc = UIStoryboard.loadMoreWallets()
        vc.delegate = self
        vc.sections = [CellSection.init(data: self.viewModel.paymentMethod?.wallet?.sorted(by: { (wallet, wallet1) -> Bool in
            return wallet.name < wallet1.name
        }) ?? [], type: BBQWalletTableViewCell.self)];
        bottomSheetController?.present(vc, on: self, viewHeight: view.frame.size.height-160)
    }
    
    func presentWebView(section: CustomPaymentSection) {
        let vc = UIStoryboard.loadFromPayment("BBQWebViewViewController") as! BBQWebViewViewController
        vc.webview = self.viewModel.webView
        self.viewModel.selectedOptions = section
        self.viewModel.triggerPaymentStartEvent()
        vc.viewmodel = self.viewModel
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func addNewCardPressed() {
        let vc = UIStoryboard.loadEnterCard()
        vc.delegate = self
        bottomSheetController?.present(vc, on: self, viewHeight: 400)
        AnalyticsHelper.shared.triggerEvent(type: .CP03B)
    }
    func moreBanksClicked() {
        let vc = UIStoryboard.loadMoreBanks()
        vc.delegate = self
        vc.sections = [CellSection.init(data: self.viewModel.paymentMethod?.netbank?.sorted(by: { (bank, bank1) -> Bool in
            return bank.bankName < bank1.bankName
        }) ?? [], type: BBQNetBankTableViewCell.self)];
        bottomSheetController?.present(vc, on: self, viewHeight: view.frame.size.height-160)
    }
    func addUPIPressed() {
        if let vc = UIStoryboard.loadFromPayment("BBQUPIViewController") as? BBQUPIViewController{
            vc.delegate = self
            bottomSheetController?.present(vc, on: self, viewHeight: 280)
            AnalyticsHelper.shared.triggerEvent(type: .CP02B)
        }
    }
    
    func didSelectPayCard(card: SavedCard) {
        if (card.cvv?.isEmpty ?? true) || card.cvv?.count ?? 0 < 3 {
            self.alert(message: "Please enter valid cvv.")
            return
        }
        createPaymentId { paymentId, errorString in
            if let _ = paymentId{
                AnalyticsHelper.shared.triggerEvent(type: .CP03A)
                self.presentWebView(section: .cards)
                self.viewModel.payusingSavedCard(card: card)
            }
        }
    }
    func didSelectSavedCard(card: SavedCard) {
        
        if let data = self.viewModel.sections[CustomPaymentSection.cards.rawValue].data {
            for i in 0..<data.count {
                
                self.viewModel.sections[CustomPaymentSection.cards.rawValue].data?[i].isSelected = false
                if let saved = data[i] as? SavedCard {
                    if(card.id == saved.id) {
                        self.viewModel.sections[CustomPaymentSection.cards.rawValue].data?[i].isSelected = true
                    }
                }
            }
        }
        if let data = self.viewModel.sections[CustomPaymentSection.upi.rawValue].data {
            for i in 0..<data.count {
                self.viewModel.sections[CustomPaymentSection.upi.rawValue].data?[i].isSelected = false
            }
        }
        self.mainTableView.reloadData()
    }
    
    func didSelectSavedUPI(upi: SavedUPI) {
        if let data = self.viewModel.sections[CustomPaymentSection.cards.rawValue].data {
            for i in 0..<data.count {
                self.viewModel.sections[CustomPaymentSection.cards.rawValue].data?[i].isSelected = false
            }
        }
        if let data = self.viewModel.sections[CustomPaymentSection.upi.rawValue].data {
            for i in 0..<data.count {
                
                self.viewModel.sections[CustomPaymentSection.upi.rawValue].data?[i].isSelected = false
                if let saved = data[i] as? SavedUPI {
                    if(upi.id == saved.id) {
                        self.viewModel.sections[CustomPaymentSection.upi.rawValue].data?[i].isSelected = true
                    }
                }
            }
        }
        self.mainTableView.reloadData()
    }
    
    func didSelectPayUPI(upi: SavedUPI) {
        createPaymentId { paymentId, errorString in
            if let _ = paymentId{
                AnalyticsHelper.shared.triggerEvent(type: .CP02A)
                self.presentWebView(section: .upi)
                self.viewModel.payusingSavedUPI(upi: upi)
            }
        }
    }
    
    func didSelectWallet(wallet: Wallet) {
        createPaymentId { paymentId, errorString in
            if let _ = paymentId{
                self.presentWebView(section: .wallets)
                self.viewModel.payusingWallet(wallet: wallet)
            }
        }
    }
    func didSelectNetbank(bank: NetBank) {
        createPaymentId { paymentId, errorString in
            if let _ = paymentId{
                self.presentWebView(section: .netBanking)
                self.viewModel.payusingNetbank(bank: bank)
            }
        }
    }
}

extension BBQCustomPaymentViewController: BBQUPIViewControllerDelegate, BBQMoreBanksViewControllerDelegate, BBQEnterCardViewControllerDelegate, BBQMoreWalletsViewControllerDelegate{
    func cancelTheWallet() {
        
    }
    
    func proccess(wallet: Wallet) {
        createPaymentId { paymentId, errorString in
            if let _ = paymentId{
                self.viewModel.payusingWallet(wallet: wallet)
                self.presentWebView(section: .wallets)
            }
        }
    }
    
    func cancelTheUpiAdd() {
        
    }
    
    func proccess(withUpiID upiId: String, save: Bool) {
        createPaymentId { paymentId, errorString in
            if let _ = paymentId{
                if save{
                    AnalyticsHelper.shared.triggerEvent(type: .CP02S)
                }
                AnalyticsHelper.shared.triggerEvent(type: .CP02BA)
                self.presentWebView(section: .upi)
                self.viewModel.payusingUPI(upi: upiId, save: save)
            }
        }
    }
    
    func cancelTheNetBanking() {
        
    }
    
    func proccess(netbank: NetBank) {
        createPaymentId { paymentId, errorString in
            if let _ = paymentId{
                self.viewModel.payusingNetbank(bank: netbank)
                self.presentWebView(section: .netBanking)
            }
        }
    }
    
    func cancelTheCardAdd() {
        
    }
    
    func proccess(cardNumber: String, cardName: String, expiry: String, cvv: String, save: Bool) {
        var card = CardDetails.init(cardNumber: cardNumber, cardName: cardName, expiry: expiry, cvv: cvv)
        card.save = save
        createPaymentId { paymentId, errorString in
            if let _ = paymentId{
                if save{
                    AnalyticsHelper.shared.triggerEvent(type: .CP03S)
                }
                AnalyticsHelper.shared.triggerEvent(type: .CP03BA)
                self.presentWebView(section: .cards)
                self.viewModel.payusingCard(card: card)
            }
        }
        
    }
    
    func isCardValid(cardNumber: String) -> Bool {
        return self.viewModel.razorPay.isCardValid(cardNumber)
    }
    
    private func createPaymentId(completion: @escaping (_ paymentId: String?, _ errorString: String?) -> Void){
        addEmailPressed { isSuccess in
            if isSuccess{
                self.delegate?.createRazorPayPaymentId(paymentVC: self, completion: { webView, options, paymentId, razorPay, errorMessage in
                    if let paymentId = paymentId, let razorPay = razorPay{
                        self.viewModel.razorPayoptions = options ?? [:]
                        self.viewModel.webView = webView
                        self.viewModel.razorPay = razorPay
                        completion(paymentId, errorMessage)
                    }
                })
            }
        }
    }
    
    
    func getCardNetwork(fromCardNumber card_number: String) -> String {
        return self.viewModel.razorPay.getCardNetwork(fromCardNumber: card_number)
    }
    
}

extension BBQCustomPaymentViewController: CustomPaymentViewModelDelegate{
    func paymentCancelled() {
        self.navigationController?.popViewController(animated: true)
        self.viewModel.triggerPaymentCancelEvent()
        openPaymentFailedDialog(title: "Cancelled")
    }
    
    func paymentFailed() {
        self.navigationController?.popViewController(animated: true)
        
    }
    private func openPaymentFailedDialog(title: String) {
        if let vc = UIStoryboard.loadFromPayment("BBQPaymentFailedVC") as? BBQPaymentFailedVC{
            vc.arrData = viewModel.getTokens()
            vc.delegate = self
            vc.strTitle = title
            vc.amount = String(format: "%@%@", getCurrency(), viewModel.billAmount)
            let height:CGFloat = CGFloat(232 + (vc.arrData.count > 2 ? 3 : vc.arrData.count + 1) * 56)
            Timer.scheduledTimer(withTimeInterval: 0.75, repeats: false) { timer in
                self.bottomSheetController?.present(vc, on: self, viewHeight: height)
                timer.invalidate()
            }
            
        }
        
    }
}

extension BBQCustomPaymentViewController: BBQPaymentFailedVCDelegate{
    func onClickRetryPayment(upi: UPIApp) {
        AnalyticsHelper.shared.triggerEvent(type: .CP07B)
        didSelectPayAppUPI(upi: upi)
    }
    
    func onClickRetryPayment(card: SavedCard) {
        AnalyticsHelper.shared.triggerEvent(type: .CP07A)
        didSelectPayCard(card: card)
    }
    
    func onClickRetryPayment(upi: SavedUPI) {
        AnalyticsHelper.shared.triggerEvent(type: .CP07B)
        didSelectPayUPI(upi: upi)
    }
    
    func onClickBtnCancel() {
        AnalyticsHelper.shared.triggerEvent(type: .CP07D)
        backAction()
    }
    
    func onClickTryAnotherMethod() {
        AnalyticsHelper.shared.triggerEvent(type: .CP07C)
    }
}

extension BBQCustomPaymentViewController: RazorpayPaymentCompletionProtocol{
    func onPaymentSuccess(_ payment_id: String, andData response: [AnyHashable : Any]) {
        self.viewModel.triggerPaymentSuccessEvent()
        //        self.presentedViewController?.dismiss(animated: true, completion: nil)
        delegate?.onPaymentSuccess(payment_id, andData: response)
    }
    
    func onPaymentError(_ code: Int32, description str: String, andData response: [AnyHashable : Any]) {
        if  self.navigationController?.viewControllers.last(where: {$0 is BBQWebViewViewController}) != nil{
            self.navigationController?.popViewController(animated: true)
        }
        switch code {
        case 0,1,3:
            self.viewModel.triggerPaymentFailEvent()
            openPaymentFailedDialog(title: "Declined")
        case 2:
            self.viewModel.triggerPaymentCancelEvent()
            openPaymentFailedDialog(title: "Cancelled")
        default:
            self.viewModel.triggerPaymentFailEvent()
            openPaymentFailedDialog(title: "Declined")
        }
        
    }
    
}

protocol BBQCustomPaymentViewControllerDelegate{
    func createRazorPayPaymentId(paymentVC: BBQCustomPaymentViewController, completion: @escaping (_ webView: WKWebView, _ options: [String:Any]?, _ paymentId: String?, _ razorPay: Razorpay?, _ errorString: String?) -> Void)
    func onPaymentSuccess(_ payment_id: String, andData response: [AnyHashable : Any])
    func onPaymentError(_ code: Int32, description str: String, andData response: [AnyHashable : Any])
}
