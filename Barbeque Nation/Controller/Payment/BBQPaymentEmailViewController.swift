//
 //  Created by vinoth kumar on 25/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQUPIViewController.swift
 //

import UIKit

class BBQPaymentEmailViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var viewForTextField: UIView!
    @IBOutlet weak var btnPay: UIButton!
    
    var delegate: BBQPaymentEmailViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtEmail.delegate = self
        viewForTextField.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 1.0)
        btnPay.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0)
        //upiTextField.becomeFirstResponder()
        view.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        // Do any additional setup after loading the view.
    }
    @IBAction func payPressed(_ sender:UIButton) {
        if(self.txtEmail.text?.isValidEmailID() ?? false) {
            delegate?.proccess(withEmailId: txtEmail.text ?? "")
            self.dismiss(animated: true)
        }
        else {
            self.alert(message: "Please enter valid email id");
        }
    }
    @IBAction func saveButtonClicked(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    @IBAction func onClickBtnBack(_ sender: Any) {
        delegate?.cancelTheEmailAdd()
        self.dismiss(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

protocol BBQPaymentEmailViewControllerDelegate{
    func cancelTheEmailAdd()
    func proccess(withEmailId emailId: String)
}
