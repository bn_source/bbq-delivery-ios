//
 //  Created by vinoth kumar on 12/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQNetBankTableViewCell.swift
 //

import UIKit

class BBQNetBankTableViewCell: UITableViewCell,CellDataRecevier {
    
    func didSetData(data: CellDataProvider) {
        if let datum = data as? NetBank {
            self.nameLabel.text = "\(datum.bankCode) - \(datum.bankName)"
            bankLogoImageView.sd_setImage(with: datum.bankLogo) { (image, error, SDImageCacheTypeNone, url) in
                self.bankLogoImageView.image = image
            }

        }
    }

    @IBOutlet weak var nameLabel:UILabel!
    @IBOutlet weak var bankLogoImageView:UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        bankLogoImageView.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
