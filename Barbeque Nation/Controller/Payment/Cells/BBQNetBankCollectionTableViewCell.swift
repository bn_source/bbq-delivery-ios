//
 //  Created by vinoth kumar on 10/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQNetBankCollectionTableViewCell.swift
 //

import UIKit

class BBQNetBankCollectionTableViewCell: UITableViewCell,CellDataRecevier {
    
    func didSetData(data: CellDataProvider) {
        self.datum = data as? CellSection
        self.setupUI()

        netbankCollectionView.reloadData()
        
    }

    var datum:CellSection?
    var delegate: Any?
    var parentDelegate:SelectedPaymentDelegate? {
        return delegate as? SelectedPaymentDelegate
    }

    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var netbankCollectionView:UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
        netbankCollectionView.delegate = self
        netbankCollectionView.dataSource = self
        // Initialization code
    }
    @IBAction func pressedMoreBanks(_ sender:UIButton) {
        parentDelegate?.moreBanksClicked()
    }
    func setupUI() {
        if let data = datum {
            let string = String.init(describing: data.type.self)
            netbankCollectionView.register(string.nib, forCellWithReuseIdentifier: string)
            if data.isLastCell && data.isFirstCell{
                viewForContainer.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0)
            }else if data.isLastCell {
                viewForContainer.maskByRoundingCorners(cornerRadius: 5.0, maskedCorners: CornerMask.bottomCornersMask)
            }else if data.isFirstCell {
                viewForContainer.maskByRoundingCorners(cornerRadius: 5.0, maskedCorners: CornerMask.topCornersMask)
            }
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension BBQNetBankCollectionTableViewCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return datum?.data?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.getCell(cell: datum?.type as! UICollectionViewCell.Type, indexPath: indexPath)
        if let DR = cell as? CellDataRecevier {
            DR.didSetData(data: (datum?.data?[indexPath.row])!)
         }
         return cell
     }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.getCellSize(cell: datum!.type as! UICollectionViewCell.Type, section: datum!)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let selected = self.datum?.data?[indexPath.row] as? NetBank {
            parentDelegate?.didSelectNetbank(bank: selected)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize.init(width: 0.01, height: 0.01)
    }
    
}
