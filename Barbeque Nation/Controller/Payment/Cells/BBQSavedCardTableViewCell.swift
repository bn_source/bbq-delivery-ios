//
 //  Created by vinoth kumar on 05/05/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQSavedCardTableViewCell.swift
 //

import UIKit

class BBQSavedCardTableViewCell: UITableViewCell,CellDataRecevier {
    var delegate: Any?

    func didSetData(data: CellDataProvider) {
        if let savedCard = data as? SavedCard {
            card = savedCard;
            cardNumberLabel.text =  "**** **** **** \(savedCard.last4 ?? "")"
            cardImageView.image = UIImage.init(named: savedCard.network?.lowercased() ?? "visa")
            amountLabel.text = "PAY ₹\(savedCard.amountToPaid ?? "")"
            checkButton.isSelected = (card?.isSelected ?? false)
            if(checkButton.isSelected) {
                secondStackView.isHidden = false
            }
            else {
                secondStackView.isHidden = true
            }
            if data.isLastCell && data.isFirstCell{
                viewForContainer.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0)
            }else if data.isLastCell {
                viewForContainer.maskByRoundingCorners(cornerRadius: 5.0, maskedCorners: CornerMask.bottomCornersMask)
            }else if data.isFirstCell {
                viewForContainer.maskByRoundingCorners(cornerRadius: 5.0, maskedCorners: CornerMask.topCornersMask)
            }
        }
    }
    var card:SavedCard?

    @IBOutlet var cardImageView:UIImageView!
    @IBOutlet var cardNumberLabel:UILabel!
    @IBOutlet var cvvTextfield:UITextField!
    @IBOutlet var checkButton:UIButton!

    @IBOutlet var firstStackView:UIView!
    @IBOutlet var secondStackView:UIView!
    @IBOutlet var amountLabel:UILabel!
    @IBOutlet weak var viewForContainer: UIView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        cvvTextfield.layer.borderWidth = 1.0
        cvvTextfield.layer.borderColor = UIColor.theme.cgColor
        cvvTextfield.isSecureTextEntry = true;
        // Initialization code
    }
    @IBAction func checkPressed(_ sender:UIButton) {
        (self.delegate as? SelectedPaymentDelegate)?.didSelectSavedCard(card: card!)
    }

    @IBAction func payPressed(_ sender:UIButton) {
        card?.cvv = self.cvvTextfield.text
        (self.delegate as? SelectedPaymentDelegate)?.didSelectPayCard(card: card!)

    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension BBQSavedCardTableViewCell:UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textVal = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        return textVal.count <= 4

    }
}
