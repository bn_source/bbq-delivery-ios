//
 //  Created by Sakir Sherasiya on 09/09/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified MoreWalletsTableViewCell.swift
 //

import UIKit

class MoreWalletsTableViewCell: UITableViewCell, CellDataRecevier {
    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    var delegate: Any?

    func didSetData(data: CellDataProvider) {
        if let stringData = data as? StringData {
            lblTitle.text = stringData.text
            viewForContainer.maskByRoundingCorners(cornerRadius: 5.0, maskedCorners: CornerMask.bottomCornersMask)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func onClickBtnMoreWallets(_ sender: Any) {
        (self.delegate as? SelectedPaymentDelegate)?.moreWalletsClicked()
    }
    
}
