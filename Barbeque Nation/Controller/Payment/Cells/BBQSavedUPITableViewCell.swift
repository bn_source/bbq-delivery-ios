//
 //  Created by Sakir Sherasiya on 07/09/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified BBQSavedCardTableViewCell.swift
 //

import UIKit

class BBQSavedUPITableViewCell: UITableViewCell,CellDataRecevier {
    var delegate: Any?

    func didSetData(data: CellDataProvider) {
        if let savedUpi = data as? SavedUPI {
            upi = savedUpi;
            lblUpiID.text =  upi?.upiID
            imgUpiLogo.image = upi?.getUpiImage()
            amountLabel.text = "PAY ₹\(savedUpi.amountToPaid ?? "")"
            checkButton.isSelected = (upi?.isSelected ?? false)
            if(checkButton.isSelected) {
                secondStackView.isHidden = false
            }
            else {
                secondStackView.isHidden = true
            }
            if data.isLastCell && data.isFirstCell{
                viewForContainer.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0)
            }else if data.isLastCell {
                viewForContainer.maskByRoundingCorners(cornerRadius: 5.0, maskedCorners: CornerMask.bottomCornersMask)
            }else if data.isFirstCell {
                viewForContainer.maskByRoundingCorners(cornerRadius: 5.0, maskedCorners: CornerMask.topCornersMask)
            }
        }
    }
    var upi:SavedUPI?

    @IBOutlet var imgUpiLogo:UIImageView!
    @IBOutlet var lblUpiID:UILabel!
    @IBOutlet var checkButton:UIButton!

    @IBOutlet var firstStackView:UIView!
    @IBOutlet var secondStackView:UIView!
    @IBOutlet var amountLabel:UILabel!
    @IBOutlet weak var viewForContainer: UIView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func checkPressed(_ sender:UIButton) {
        if let upi = upi {
            (self.delegate as? SelectedPaymentDelegate)?.didSelectSavedUPI(upi: upi)
        }
    }

    @IBAction func payPressed(_ sender:UIButton) {
        if let upi = upi {
            (self.delegate as? SelectedPaymentDelegate)?.didSelectPayUPI(upi: upi)
        }

    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

