//
 //  Created by Sakir Sherasiya on 07/09/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified BBQSavedCardTableViewCell.swift
 //

import UIKit

class BBQAppUPITableViewCell: UITableViewCell,CellDataRecevier {
    var delegate: Any?

    func didSetData(data: CellDataProvider) {
        if let appUPI = data as? UPIApp {
            upi = appUPI
            lblUpiID.text =  appUPI.appName
            imgUpiLogo.setImagePNG(url: appUPI.appLogo, placeholderImage: UIImage(named: "upi_logo"))
            amountLabel.text = "PAY ₹\(appUPI.amountToPaid ?? "")"
            checkButton.isSelected = (upi?.isSelected ?? false)
            if(checkButton.isSelected) {
                secondStackView.isHidden = false
            }
            else {
                secondStackView.isHidden = true
            }
            if data.isLastCell && data.isFirstCell{
                viewForContainer.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0)
            }else if data.isLastCell {
                viewForContainer.maskByRoundingCorners(cornerRadius: 5.0, maskedCorners: CornerMask.bottomCornersMask)
            }else if data.isFirstCell {
                viewForContainer.maskByRoundingCorners(cornerRadius: 5.0, maskedCorners: CornerMask.topCornersMask)
            }
        }
    }
    
    var upi: UPIApp?

    @IBOutlet var imgUpiLogo:UIImageView!
    @IBOutlet var lblUpiID:UILabel!
    @IBOutlet var checkButton:UIButton!

    @IBOutlet var firstStackView:UIView!
    @IBOutlet var secondStackView:UIView!
    @IBOutlet var amountLabel:UILabel!
    @IBOutlet weak var viewForContainer: UIView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func checkPressed(_ sender:UIButton) {
        if let upi = upi {
            (self.delegate as? SelectedPaymentDelegate)?.didSelectAppUPI(upi: upi)
        }
    }

    @IBAction func payPressed(_ sender:UIButton) {
        if let upi = upi {
            (self.delegate as? SelectedPaymentDelegate)?.didSelectPayAppUPI(upi: upi)
        }

    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

