//
 //  Created by vinoth kumar on 07/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQPaymentWalletTableViewCell.swift
 //

import UIKit
import SDWebImage
class BBQPaymentWalletTableViewCell: UITableViewCell,CellDataRecevier {
    func didSetData(data: CellDataProvider) {
        if let datum = data as? Wallet {
            wallet = datum
            let first = String(datum.name.prefix(1)).capitalized
            let other = String(datum.name.dropFirst())
            self.nameLabel.text = first + other
            self.logoImageView.sd_setImage(with: datum.logoURL) { (image, error, SDImageCacheTypeNone, url) in
                self.logoImageView.image = image;
            }
            if data.isLastCell && data.isFirstCell{
                viewForContainer.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0)
            }else if data.isLastCell {
                viewForContainer.maskByRoundingCorners(cornerRadius: 5.0, maskedCorners: CornerMask.bottomCornersMask)
            }else if data.isFirstCell {
                viewForContainer.maskByRoundingCorners(cornerRadius: 5.0, maskedCorners: CornerMask.topCornersMask)
            }
        }
    }
    @IBOutlet weak var nameLabel:UILabel!
    @IBOutlet weak var logoImageView:UIImageView!
    @IBOutlet weak var viewForContainer: UIView!
    var delegate: Any?
    var wallet:Wallet?


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func linkAccountPressed(_ sender:UIButton) {
        (self.delegate as? SelectedPaymentDelegate)?.didSelectWallet(wallet: wallet!)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
