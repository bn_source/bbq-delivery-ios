//
 //  Created by vinoth kumar on 13/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified SelectedPaymentDelegate.swift
 //

import Foundation
protocol SelectedPaymentDelegate {
    func didSelectWallet(wallet:Wallet)
    func addNewCardPressed()
    func addUPIPressed()
    func didSelectSavedCard(card:SavedCard)
    func didSelectPayCard(card:SavedCard)
    func didSelectSavedUPI(upi:SavedUPI)
    func didSelectPayUPI(upi:SavedUPI)
    func moreBanksClicked()
    func moreWalletsClicked()
    func didSelectNetbank(bank:NetBank)
    func didSelectAppUPI(upi:UPIApp)
    func didSelectPayAppUPI(upi:UPIApp)
}
extension SelectedPaymentDelegate {
    func didSelectWallet(wallet:Wallet) {}
    func addNewCardPressed() {}
    func addUPIPressed() {}
    func moreBanksClicked() {}
    func didSelectNetbank(bank:NetBank) {}
}
