//
 //  Created by vinoth kumar on 12/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQNetBankTableViewCell.swift
 //

import UIKit

class BBQWalletTableViewCell: UITableViewCell,CellDataRecevier {
    
    func didSetData(data: CellDataProvider) {
        if let datum = data as? Wallet {
            self.nameLabel.text = "\(datum.name)"
            walletLogoImageView.sd_setImage(with: datum.logoURL) { (image, error, SDImageCacheTypeNone, url) in
                self.walletLogoImageView.image = image
            }

        }
    }

    @IBOutlet weak var nameLabel:UILabel!
    @IBOutlet weak var walletLogoImageView:UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        walletLogoImageView.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
