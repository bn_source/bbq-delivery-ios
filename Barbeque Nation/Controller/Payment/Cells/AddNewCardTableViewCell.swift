//
 //  Created by vinoth kumar on 10/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified AddNewCardTableViewCell.swift
 //

import UIKit

class AddNewCardTableViewCell: UITableViewCell,CellDataRecevier {
    var delegate: Any?
    @IBOutlet weak var cardLogoImageView:UIImageView!
    @IBOutlet weak var addNewButton:UIButton!
    @IBOutlet weak var viewForContainer: UIView!
    
    func didSetData(data: CellDataProvider) {
         datum = data as? StringData
        cardLogoImageView.image = UIImage.init(named: datum?.image ?? "")
        addNewButton.setTitle(datum?.text, for: UIControl.State.normal)
        if data.isLastCell && data.isFirstCell{
            viewForContainer.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0)
        }else if data.isLastCell {
            viewForContainer.maskByRoundingCorners(cornerRadius: 5.0, maskedCorners: CornerMask.bottomCornersMask)
        }else if data.isFirstCell {
            viewForContainer.maskByRoundingCorners(cornerRadius: 5.0, maskedCorners: CornerMask.topCornersMask)
        }
    }
    var datum:StringData?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func addCardPressed(_ sender:UIButton) {
        switch datum?.image ?? "" {
        case "CardLogos":
            (delegate as? SelectedPaymentDelegate)?.addNewCardPressed()
        case "upiLogos":
            (delegate as? SelectedPaymentDelegate)?.addUPIPressed()
        default:
            break;
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
