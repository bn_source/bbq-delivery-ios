//
 //  Created by vinoth kumar on 07/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQNetbankingCollectionViewCell.swift
 //

import UIKit

class BBQNetbankingCollectionViewCell: UICollectionViewCell,CellDataRecevier {

    func didSetData(data: CellDataProvider) {
        if let datum = data as? NetBank {
            bankCodeLabel.text = String(datum.bankName.split(separator: " ").first ?? "")
            bankLogoImageView.sd_setImage(with: datum.bankLogo) { (image, error, SDImageCacheTypeNone, url) in
                self.bankLogoImageView.image = image
            }
        }
    }
    @IBOutlet weak var bankLogoImageView:UIImageView!
    @IBOutlet weak var bankCodeLabel:UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
