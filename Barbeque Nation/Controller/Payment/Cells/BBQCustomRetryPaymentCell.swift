//
 //  Created by Sakir Sherasiya on 03/10/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified BBQCustomRetryPaymentCell.swift
 //

import UIKit

class BBQCustomRetryPaymentCell: UITableViewCell {
    @IBOutlet weak var btnSelected: UIButton!
    @IBOutlet weak var imgCardIcon: UIImageView!
    @IBOutlet weak var txtCVV: UITextField!
    @IBOutlet weak var lblTokenId: UILabel!
    @IBOutlet weak var viewForCVV: UIView!
    @IBOutlet weak var viewForPaymentData: UIView!
    @IBOutlet weak var lblLastCell: UILabel!
    
    private var card: SavedCard?
    private var upi: SavedUPI?
    private var upiApp: UPIApp?
    private var isCardCell = false
    private var isLastCell = false
    private var delegate: BBQCustomRetryPaymentCellDelegate?
    private var index: Int?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUITheme()
    }

    func setCellData(data: Any?, isSelected: Bool, index: Int, delegate: BBQCustomRetryPaymentCellDelegate?){
        self.delegate = delegate
        self.index = index
        
        isLastCell = false
        lblLastCell.isHidden = true
        viewForPaymentData.isHidden = false
        viewForCVV.isHidden = !isCardCell
        btnSelected.isSelected = isSelected
        
        if let card = data as? SavedCard{
            self.card = card
            self.upi = nil
            self.upiApp = nil
            isCardCell = true
            lblTokenId.text =  "**** **** **** \(card.last4 ?? "")"
            imgCardIcon.image = UIImage.init(named: card.network?.lowercased() ?? "visa")
            viewForCVV.isHidden = !isSelected
        }else if let upi = data as? SavedUPI{
            self.upi = upi
            self.card = nil
            self.upiApp = nil
            isCardCell = false
            lblTokenId.text =  upi.upiID
            imgCardIcon.image = upi.getUpiImage()
        }else if let upi = data as? UPIApp{
            self.upiApp = upi
            self.card = nil
            self.upi = nil
            isCardCell = false
            lblTokenId.text =  upi.appName
            imgCardIcon.setImagePNG(url: upi.appLogo, placeholderImage: UIImage(named: "upi_logo"))
        }else{
            isLastCell = true
            lblLastCell.isHidden = false
            viewForPaymentData.isHidden = true
        }
        
    }
    
    func setUITheme(){
        viewForCVV.roundCorners(cornerRadius: 5.0, borderColor: .theme, borderWidth: 1.0)
    }
    
    @IBAction func onClickCheckBox(_ sender: UIButton) {
        if isLastCell{
            delegate?.selectAnotherPayment()
            return
        }
        if !btnSelected.isSelected{
            txtCVV.text = ""
            if isCardCell{
                delegate?.selectThePayment(index: index ?? 0, card: card, isActive: false)
            }else if upi != nil{
                delegate?.selectThePayment(index: index ?? 0, upi: upi)
            }else if upiApp != nil{
                delegate?.selectThePayment(index: index ?? 0, upi: upiApp)
            }else{
                delegate?.selectAnotherPayment()
            }
        }
        
    }
}

extension BBQCustomRetryPaymentCell: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textVal = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        return textVal.count <= 4
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        card?.cvv = txtCVV.text
        delegate?.selectThePayment(index: index ?? 0, card: card, isActive: textField.text?.count ?? 0 >= 3)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.text?.count ?? 0 >= 3
    }
    
}

protocol BBQCustomRetryPaymentCellDelegate{
    func selectThePayment(index: Int, card: SavedCard?, isActive: Bool)
    func selectThePayment(index: Int, upi: SavedUPI?)
    func selectThePayment(index: Int, upi: UPIApp?)
    func selectAnotherPayment()
}
