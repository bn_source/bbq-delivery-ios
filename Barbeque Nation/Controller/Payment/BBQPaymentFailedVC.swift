//
 //  Created by Sakir Sherasiya on 03/10/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified BBQPaymentFailedVC.swift
 //

import UIKit

class BBQPaymentFailedVC: UIViewController {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var btnPayNow: UIButton!
    @IBOutlet weak var viewForPayNow: UIView!
    @IBOutlet weak var heightForTableView: NSLayoutConstraint!
    @IBOutlet weak var imgPaymentType: UIImageView!
    
    var delegate: BBQPaymentFailedVCDelegate?
    var arrData = [Any]()
    var selectedindex: Int?
    var strTitle: String = "Cancelled"
    var amount: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "BBQCustomRetryPaymentCell", bundle: nil), forCellReuseIdentifier: "BBQCustomRetryPaymentCell")
        
        setTheme()
        AnalyticsHelper.shared.triggerEvent(type: .CP07)
    }
    
    func setTheme(){
        lblTitle.text = strTitle
        lblAmount.text = amount
        lblMessage.text = String(format: "Your transaction has been %@.\nPlease try one of the options below", strTitle.lowercased())
        tableView.reloadData{
            self.heightForTableView.constant = self.tableView.contentSize.height
        }
        buttonEnable(isEnabled: false)
        
        view.maskByRoundingCorners(cornerRadius: 20.0, maskedCorners: CornerMask.topCornersMask)
    }
    

    @IBAction func onClickBtnPayNow(_ sender: Any) {
        guard let selectedindex = selectedindex else { return }
        
        if arrData.count > selectedindex || selectedindex == 2{
            if selectedindex == 2{
                delegate?.onClickTryAnotherMethod()
            }else if let card = arrData[selectedindex] as? SavedCard{
                delegate?.onClickRetryPayment(card: card)
            }else if let upi = arrData[selectedindex] as? SavedUPI{
                delegate?.onClickRetryPayment(upi: upi)
            }else if let upi = arrData[selectedindex] as? UPIApp{
                delegate?.onClickRetryPayment(upi: upi)
            }else{
                delegate?.onClickTryAnotherMethod()
            }
            self.dismiss(animated: true)
        }
        
    }
    
    @IBAction func onClickBtnBack(_ sender: Any) {
        delegate?.onClickBtnCancel()
        self.dismiss(animated: true)
    }
    
    private func buttonEnable(isEnabled: Bool){
        btnPayNow.isEnabled = isEnabled
        viewForPayNow.alpha = isEnabled ? 1 : 0.5
    }
    /*
     // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}



extension BBQPaymentFailedVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count > 2 ? 3 : arrData.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "BBQCustomRetryPaymentCell", for: indexPath) as? BBQCustomRetryPaymentCell else { return UITableViewCell() }
        var data: Any?
        if arrData.count > indexPath.row && indexPath.row != 2{
            data = arrData[indexPath.row]
        }
        cell.setCellData(data: data, isSelected: indexPath.row == selectedindex ? true : false, index: indexPath.row, delegate: self)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if arrData.count <= indexPath.row || indexPath.row == 2{
            self.dismiss(animated: true)
            delegate?.onClickTryAnotherMethod()
        }
        if indexPath.row != selectedindex{
            selectedindex = indexPath.row
            if arrData.count > indexPath.row, let upi = arrData[indexPath.row] as? SavedUPI{
                imgPaymentType.image = upi.getUpiImage()
                imgPaymentType.isHidden = false
                buttonEnable(isEnabled: true)
            }else if arrData.count > indexPath.row, let card = arrData[indexPath.row] as? SavedCard{
                imgPaymentType.image = UIImage.init(named: card.network?.lowercased() ?? "visa")
                imgPaymentType.isHidden = false
            }else if arrData.count > indexPath.row, let card = arrData[indexPath.row] as? UPIApp{
                imgPaymentType.setImagePNG(url: card.appLogo)
                imgPaymentType.isHidden = false
                buttonEnable(isEnabled: true)
            }
            tableView.reloadData()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }
    
    
}

extension BBQPaymentFailedVC: BBQCustomRetryPaymentCellDelegate{
    func selectThePayment(index: Int, upi: UPIApp?) {
        selectedindex = index
        buttonEnable(isEnabled: true)
        if let upi = upi{
            imgPaymentType.setImagePNG(url: upi.appLogo, placeholderImage: UIImage(named: "upi_logo"))
        }else{
            imgPaymentType.image = UIImage(named: "upi_logo") ?? UIImage()
        }
        imgPaymentType.isHidden = false
        tableView.reloadData()
    }
    
    func selectThePayment(index: Int, card: SavedCard?, isActive: Bool) {
        selectedindex = index
        arrData[index] = card as Any
        buttonEnable(isEnabled: isActive)
        imgPaymentType.image = UIImage.init(named: card?.network?.lowercased() ?? "visa")
        imgPaymentType.isHidden = false
        tableView.reloadData()
    }
    
    func selectThePayment(index: Int, upi: SavedUPI?) {
        selectedindex = index
        buttonEnable(isEnabled: true)
        imgPaymentType.image = upi?.getUpiImage()
        imgPaymentType.isHidden = false
        tableView.reloadData()
    }
    
    func selectAnotherPayment() {
        delegate?.onClickTryAnotherMethod()
        self.dismiss(animated: true)
    }
    
    
}

protocol BBQPaymentFailedVCDelegate{
    func onClickRetryPayment(card: SavedCard)
    func onClickRetryPayment(upi: SavedUPI)
    func onClickRetryPayment(upi: UPIApp)
    func onClickBtnCancel()
    func onClickTryAnotherMethod()
}
