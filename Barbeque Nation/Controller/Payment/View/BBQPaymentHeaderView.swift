//
 //  Created by vinoth kumar on 09/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQPaymentHeaderView.swift
 //

import UIKit

class BBQPaymentHeaderView: UIView,CellDataRecevier {
    func didSetData(data: CellDataProvider) {
        if let datum = data as? CustomPaymentHeader {
            if let title = datum.getAttribtedTitle(){
                titleLable.attributedText = title
            }else{
                titleLable.text = datum.title
            }
            if let title1 = datum.getAttribtedSubTitle(){
                titleLable1.attributedText = title1
            }else{
                titleLable1.text = datum.title1
            }
        }
    }
    @IBOutlet weak var titleLable:UILabel!
    @IBOutlet weak var titleLable1:UILabel!
    @IBOutlet weak var backButton:UIButton!


    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
