//
//  GBButton.swift
//  GoBaskt
//
//  Created by vinoth kumar on 13/03/20.
//  Copyright © 2020 vinoth kumar. All rights reserved.
//

import UIKit

@IBDesignable class RoundedView: UIView {
    
    private var shadowLayer: CAShapeLayer!
    private var fillColor: UIColor = UIColor(red: 0.161, green: 0.161, blue: 0.165, alpha: 0.04)
    // the color applied to the shadowLayer, rather than the view's backgroundColor
    
    @IBInspectable var shadowColor:UIColor = UIColor(red: 0.161, green: 0.161, blue: 0.165, alpha: 0.04) {
        
        didSet {
            self.updateShadow()
        }
    }
    @IBInspectable var shadowOffSet:CGSize = CGSize(width: 0, height: 4) {
        didSet {
                   self.updateShadow()
               }
    }
    @IBInspectable var shadowRadius:CGFloat = 12 {
        didSet {
                   self.updateShadow()
               }
    }
    @IBInspectable var dropShadow:Bool = false {
        didSet {
            
                self.updateView()
            
        }
    }
    
    @IBInspectable var addShadow:Bool = false {
        didSet {
            
                self.updateView()
            
        }
    }
    @IBInspectable var borderColor:UIColor = .black {
        didSet {
            self.updateView()
        }
    }

    @IBInspectable var border:CGFloat = 0 {
        didSet {
            self.updateView()
        }
    }
    @IBInspectable var cornerRadius:CGFloat = 25 {
        didSet {
            self.updateView()
        }
    }
    func updateView()  {
        
        if(dropShadow){
            self.dropShadow()
        }
        if(border > 0) {
            self.layer.borderWidth = border
            self.layer.borderColor = borderColor.cgColor
        }
        
        self.layer.cornerRadius = cornerRadius
//        self.layer.masksToBounds = true

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if(addShadow) {
            self.updateShadow()
        }
        self.layer.cornerRadius = cornerRadius



    }
    func updateShadow() {
        if shadowLayer == nil && addShadow {
                shadowLayer = CAShapeLayer()
              
                shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
            shadowLayer.fillColor = self.backgroundColor?.cgColor

                shadowLayer.shadowColor = shadowColor.cgColor
                shadowLayer.shadowPath = shadowLayer.path
                shadowLayer.shadowOffset = shadowOffSet
                shadowLayer.shadowOpacity = 1
                shadowLayer.shadowRadius = shadowRadius

                layer.insertSublayer(shadowLayer, at: 0)
            }
    }
    
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
