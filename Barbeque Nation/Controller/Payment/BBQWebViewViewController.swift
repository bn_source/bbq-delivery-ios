//
 //  Created by vinoth kumar on 13/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQWebViewViewController.swift
 //

import UIKit
import WebKit
class BBQWebViewViewController: UIViewController {

    @IBOutlet weak var webviewContainer:UIView!
    @IBOutlet weak var cancelButton:UIView!

    var webview:WKWebView!
    var viewmodel:CustomPaymentViewModel!
//    var razorpay:Razorpay!
//    var razorPayDelegate:Any?
    override func viewDidLoad() {
        super.viewDidLoad()
        webview.frame = CGRect.init(x: 0, y: 0, width: webviewContainer.frame.width, height: webviewContainer.frame.height)
        self.webviewContainer.addSubview(webview)
        constrainView(view: webview, toView: webviewContainer)

        webview.navigationDelegate = self
        
        self.view.bringSubviewToFront(cancelButton)

        // Do any additional setup after loading the view.
    }
    func constrainView(view:UIView, toView contentView:UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
    }
    override func viewWillAppear(_ animated: Bool) {

    }
    @IBAction func cancelAction(_ sender:UIButton) {
        var actions: [(String, UIAlertAction.Style)] = []
        actions.append(("No, Wait", UIAlertAction.Style.cancel))
        actions.append(("Yes, Cancel" , UIAlertAction.Style.destructive))
        
             //self = ViewController
            Alerts.showActionsheet(viewController: self, title: "Confirmation", message: "Do you want cancel the payment?", actions: actions) { (index) in
                print("call action \(index)")
                
                switch index{
                    
                case 0: break
                    
                case 1:
                    //self.viewmodel.razorPay.userCancelledPayment()
                    self.viewmodel.razorPay.close()
                    self.viewmodel.cancelledPayment()
                    break
                default: break
                    
                }
            }
    
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension BBQWebViewViewController:WKNavigationDelegate {
    public func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!){
        self.viewmodel.razorPay.webView(webView, didCommit: navigation)
    }
    
    public func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError er: Error) {
        self.viewmodel.razorPay.webView(webView, didFailProvisionalNavigation: navigation, withError: er)
    }
    
    public func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError er: Error){
        self.viewmodel.razorPay.webView(webView, didFail: navigation, withError: er)
    }
    
    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!){
        DispatchQueue.main.async {
            self.viewmodel.razorPay.webView(webView, didFinish: navigation)
        }
    }
}
