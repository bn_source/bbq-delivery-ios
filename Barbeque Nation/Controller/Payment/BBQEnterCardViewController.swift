//
 //  Created by vinoth kumar on 11/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQEnterCardViewController.swift
 //

import UIKit
struct CardDetails {
    var cardNumber:String
    var cardName:String
    var expiry:String
    var cvv:String
    var save:Bool = false
    var expiryMonth:Int {
        return Int(expiry.split(separator: "/").first ?? "0") ?? 0
    }
    var expiryYear:Int {
        return Int(expiry.split(separator: "/").last ?? "0") ?? 0
    }
}
class BBQEnterCardViewController: UIViewController {

    @IBOutlet weak var cardNumberTextField:UITextField!
    @IBOutlet weak var cardNameTextField:UITextField!
    @IBOutlet weak var validThruTextField:UITextField!
    @IBOutlet weak var cvvTextField:UITextField!
    @IBOutlet weak var checkBoxButton:UIButton!
    @IBOutlet weak var btnPay: UIButton!
    @IBOutlet weak var viewForScanCard: UIView!
    @IBOutlet weak var imgCardType: UIImageView!
    
    var delegate: BBQEnterCardViewControllerDelegate?
    var card_type: (cardlogo: UIImage, card_length: Int, cvv_length: Int, input_format: String)?
   

    override func viewDidLoad() {
        super.viewDidLoad()
        cardNumberTextField.delegate = self
        cardNameTextField.delegate = self
        validThruTextField.delegate = self
        cvvTextField.delegate = self
        validThruTextField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        btnPay.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0)
        view.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        if #available(iOS 13.0, *) {
            viewForScanCard.isHidden = true
        } else {
            viewForScanCard.isHidden = true
        }
    }
    @IBAction func checkBoxPressed(_ sender:UIButton) {
        sender.isSelected = !sender.isSelected
        
    }
    @IBAction func onClickBtnScanCard(_ sender: Any) {
        openCardScanner()
    }
    @IBAction func onClickBtnBack(_ sender: Any) {
        delegate?.cancelTheCardAdd()
        self.dismiss(animated: true)
    }
    @IBAction func payButtonPressed(_ sender:UIButton) {
        var cardNumber = (cardNumberTextField.text != "" ? cardNumberTextField.text : "1") ?? "1"
        let cardName = cardNameTextField.text ?? ""
        let ValidThru = validThruTextField.text ?? ""
        let cvv = cvvTextField.text ?? ""
        cardNumber = cardNumber.replacingOccurrences(of: " ", with: "")
        if !(self.delegate?.isCardValid(cardNumber: cardNumber) ?? false) {
            self.alert(message: "Please enter valid card number.")
            return;
        }
        if (cardName.isEmpty) {
            self.alert(message: "Please enter name on the card.")
            return;
        }
        if (ValidThru.isEmpty) || ValidThru.count < 5 {
            self.alert(message: "Please enter valid expiry month and year.")
            return;

        }
        if (cvv.isEmpty) || cvv.count < 3 {
            self.alert(message: "Please enter valid cvv.")
            return;
        }
        self.dismiss(animated: true)
        delegate?.proccess(cardNumber: cardNumber, cardName: cardName, expiry: ValidThru, cvv: cvv, save: self.checkBoxButton.isSelected)
    }
    @objc func textFieldDidChange(textField: UITextField) {
        switch textField {
        case validThruTextField:
            if var text = textField.text {
                if(text.count > 2 && !text.contains("/")) {
                    text.insert("/", at: text.index(text.startIndex, offsetBy: 2))
                    textField.text = text
                }
            }
            
        default:
            return;
        }
    }
    
    private func openCardScanner(){
        if #available(iOS 13.0, *) {
            let scannerVC = BBQCardScannerVC()
            scannerVC.modalPresentationStyle = .overFullScreen
            present(scannerVC, animated: true)
//            let ccScanner = CCScanner()
//            ccScanner.delegate = self
//            ccScanner.startScanner(viewController: self)
        } else {
            // Fallback on earlier versions
        }
        
        
//        let scannerVC = SharkCardScanViewController(viewModel: CardScanViewModel(noPermissionAction: { [weak self] in
//
//            UIUtils.showToast(message: "Allow camera permission from the settings.")
//
//        }, successHandler: { (response) in
//            print("Expiry 💣: \(response.expiry ?? "")")
//            print("Card Number 💳: \(response.number)")
//            print("Holder name 🕺: \(response.holder ?? "")")
//        }))
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension BBQEnterCardViewController:UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == validThruTextField) {
            let pickerView = MonthYearPickerView(frame: CGRect(origin: CGPoint(x: 0, y: (view.bounds.height - 216) / 2), size: CGSize(width: view.bounds.width, height: 216)))
            pickerView.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)

            pickerView.minimumDate = Date()
            if(self.validThruTextField.text == "") {
                let df = DateFormatter.init()
                df.dateFormat = "MM/YY"
                self.validThruTextField.text = df.string(from:pickerView.date);
            }
            validThruTextField.inputView = pickerView
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
      
    }
    @objc func dateChanged(_ picker: MonthYearPickerView) {
        print("date changed: \(picker.date)")
        let df = DateFormatter.init()
        df.dateFormat = "MM/YY"
        validThruTextField.text = df.string(from: picker.date)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textVal = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        if textField == cardNumberTextField{
            let cleanedCardNumber = textVal.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "-", with: "")
            if cleanedCardNumber.count == 6 || (cleanedCardNumber.count >= 6 && self.card_type == nil){
                if let card_type = self.delegate?.getCardNetwork(fromCardNumber: cleanedCardNumber){
                    self.card_type = textVal.identifyCardType(card_type: card_type)
                }
            }else if cleanedCardNumber.count <= 5{
                self.card_type = nil
            }
            
        }
        
        imgCardType.image = card_type?.cardlogo ?? nil
//        if (card_type?.cvv_length ?? 3) == 4{
//            cvvTextField.text = cvvTextField.format(card_type?.input_format ?? "NNNN NNNN NNNN NNNN", oldString: textField.text ?? "")
//        }else{
//
//        }
        
        switch textField {
        case cardNumberTextField:
            textField.text = textVal.format(card_type?.input_format ?? "NNNN NNNN NNNN NNNN", oldString: textField.text ?? "")
            
            var isValid = self.delegate?.isCardValid(cardNumber: textVal.replacingOccurrences(of: " ", with: "")) ?? false
            //print(isValid)
            return false
        case cardNameTextField:
            return textVal.isvalidName
        case cvvTextField:
            return textVal.count <= card_type?.cvv_length ?? 4
        case validThruTextField:
            textField.text = textVal.format("NN/NN", oldString: textField.text ?? "")
        default:
            break;
        }
       return true
    }
    
}

extension BBQEnterCardViewController: CCScannerDelegate{
    func ccScannerCompleted(cardNumber: String, expDate: String, cardType: String) {
        // Do what you wish with the data.
        print(cardNumber, expDate, cardType)
    }
}


protocol BBQEnterCardViewControllerDelegate{
    func cancelTheCardAdd()
    func proccess(cardNumber: String, cardName: String, expiry: String, cvv: String, save: Bool)
    func isCardValid(cardNumber: String) -> Bool
    func getCardNetwork(fromCardNumber: String) -> String
}
