//
 //  Created by Arpana on 13/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified FeedbackServiceManager.swift
 //

import UIKit

class FeedbackServiceManager: BaseManager {
/*
    class func getFeedbackDetails(orderID:String, completion: @escaping (_ model: FeedbackModel?, _ result: Bool?)->()) {
        
        let endPoint = FeedbackRouter.getFeedbackDetails(orderID: orderID )
        
        //(params: ["customer_id": BBQUserDefaults.sharedInstance.customerId, "order_id": orderId])
            BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
                print(responseData as Any)
                let modelValue = FeedbackServiceManager.parseGetFeedbackDetails(data: responseData, error: responseError)
                if let value = modelValue {
                    DispatchQueue.main.async {
                        completion(value, true)
                    }
                } else {
                    DispatchQueue.main.async {
                        completion(nil, false)
                    }
                }
            }
        }
  */
    class func postFeedbackData(name: String, completion: @escaping (_ model: FeedbackModel?, _ result: Bool?)->()) {
        let endPoint = FeedbackRouter.postFeedbackData(name: "")
            BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
                print(responseData as Any)
                //what ever respose you get parse accordingly
//                let modelValue = FeedbackServiceManager.parseLoyalityCouponData(data: responseData, error: responseError)
//                if let value = modelValue {
                    DispatchQueue.main.async {
                        completion(nil,true)
                  //  }
                }
//                else {
//                    DispatchQueue.main.async {
//                        completion(nil, false)
//                    }
               // }
            }
        }
    }

    //MARK: Parsing
    extension FeedbackServiceManager {
        
      /*  private class func parseGetFeedbackDetails(data: Data?, error: Error?) -> FeedbackModel? {
            if error == nil {
                do {
                    let decoder = JSONDecoder()
                    guard let responseData = data else {
                        return nil
                    }
                    let feedbackModel = try decoder.decode(FeedbackModel.self, from: responseData)
                    return feedbackModel
                    
                } catch {
                    print(error)
                    return nil
                }
            } else {
                if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse!.statusCode >= 300 {
                    BBQUserManager.configureErrorModel(data: data)
                }
                return nil
            }
        }*/
        
        private class func submitFilledRatingData(data: Data?, error: Error?) -> Bool? {
            if error == nil {
                do {
                    _ = JSONDecoder()
                    guard data != nil else {
                        return nil
                    }
//                    let loyalityModel = try decoder.decode(LoyalityCoupon.self, from: responseData)
//                    return loyalityModel
                    return true
                    
//                } catch {
//                    print(error)
//                    return nil
                }
            } else {
                if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse!.statusCode >= 300 {
                    BBQUserManager.configureErrorModel(data: data)
                }
                return nil
            }
        }
    }
/*    func getOrderFeedBackDetails(_ orderId: String, completionHandler: @escaping(_ orderDetail: Order) -> Void) {
 UIUtils.showLoader()
 BBQServiceManager.getInstance().getOrderDetails(params: ["customer_id": BBQUserDefaults.sharedInstance.customerId, "order_id": orderId]) { (error, response) in
     UIUtils.hideLoader()
     if let orderDetail = response?.data {
         print(orderDetail)
         
         completionHandler(orderDetail)
     }
 }
}*/
