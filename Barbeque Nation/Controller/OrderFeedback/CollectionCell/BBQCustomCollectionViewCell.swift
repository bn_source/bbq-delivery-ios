//
 //  Created by Arpana on 15/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQCustomCollectionViewCell.swift
 //

import UIKit

class BBQCustomCollectionViewCell: UICollectionViewCell {
        
        let containerView = UIView()
        
        //initialize  the properties for cell
        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            
            self.contentView.translatesAutoresizingMaskIntoConstraints = false

            //Create a view to make the corner radius for uiview
            //since  masksToBounds == false required for shadow and masksToBounds == true required for corner radius
            //So need one more uiview
           // let containerView = UIView()
            
          //  self.contentView.backgroundColor =  .white
            
            
             if #available(iOS 12.0, tvOS 12.0, *)
             {
                self.contentView.translatesAutoresizingMaskIntoConstraints = false
                
                let leftConstraint = contentView.leftAnchor.constraint(equalTo: leftAnchor)
                let rightConstraint = contentView.rightAnchor.constraint(equalTo: rightAnchor)
                let topConstraint = contentView.topAnchor.constraint(equalTo: topAnchor)
                let bottomConstraint = contentView.bottomAnchor.constraint(equalTo: bottomAnchor)
                NSLayoutConstraint.activate([leftConstraint, rightConstraint, topConstraint, bottomConstraint])
            }
            
            
          //  containerView.layer.masksToBounds = true; // its must be true for corner
            //decide how much corner you want
            self.setShadow()
            addSubview(containerView)

//            containerView.roundCorners(cornerRadius: (self.containerView.frame.width / (self.containerView.frame.width / 6)), borderColor: .lightGray, borderWidth: 0.25)

         
           // add constraints
            containerView.translatesAutoresizingMaskIntoConstraints = false
            
            // pin the containerView to the edges to the view
            //top/bottom/leadng/trailing same as contentview
            containerView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
            containerView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
            containerView.topAnchor.constraint(equalTo: topAnchor).isActive = true
            containerView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
            
            
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
        }
        
        override init(frame: CGRect) {
               super.init(frame: frame)
           }
        
        
     /*      override open func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
               if #available(iOS 12.0, tvOS 12.0, *)
               {
                   /*
                    In iOS 12, there are issues with self-sizing cells. The fix is to call updateConstraintsIfNeeded() and turn on translatesAutoResizingMaskIntoConstraints as described in Apples release notes:
                    "You might encounter issues with systemLayoutSizeFitting(_:) when using a UICollectionViewCell subclass that requires updateConstraints(). (42138227) — Workaround: Don't call the cell's setNeedsUpdateConstraints() method unless you need to support live constraint changes. If you need to support live constraint changes, call updateConstraintsIfNeeded() before calling systemLayoutSizeFitting(_:)."
                    */
                   self.updateConstraintsIfNeeded()


               }
               return super.preferredLayoutAttributesFitting(layoutAttributes)

           }*/
    }
