//
 //  Created by Arpana on 15/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQPackagingCollectionViewCell.swift
 //

import UIKit

class BBQPackagingCollectionViewCell: BBQCustomCollectionViewCell {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    var cornerRadiusValue: CGFloat = 0.0
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        nameLabel.font = UIFont(name: "NunitoSans-Regular", size: 12)
          self.view.backgroundColor =  .white
//        cornerRadiusValue =  ( (self.frame.width / 20))

 
       // view.roundCorners(cornerRadius: cornerRadiusValue, borderColor: .lightGray, borderWidth: 0.50)
    }

}
