//
 //  Created by Arpana on 15/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQTasteTableViewCell.swift
 //

import UIKit

//MARK: Delegate to be called on click of cell rating pressed
@objc protocol NotifyelectdStarToTableViewDelegate {
   
    @objc optional func notifyStarCount(indexPath: IndexPath , starCount: Int )

}

class BBQTasteTableViewCell: UITableViewCell {

    @IBOutlet weak var vegNonVegImgView: UIImageView!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var rateView: FloatRatingView!
    weak var statrDelegate: NotifyelectdStarToTableViewDelegate?
    var currentIndex: IndexPath?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        itemName.font = UIFont.appThemeRegularWith(size: 14)
        rateView.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setDat(object: Questions)  {
        itemName.text = object.question_name
        vegNonVegImgView.image = object.food_type == "0" ? #imageLiteral(resourceName: "Group 846") : #imageLiteral(resourceName: "Group 845")
        //how much max rating we can provide for question
    }
    
}
extension BBQTasteTableViewCell : FloatRatingViewDelegate {
    
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double){
        
        //notify coltroller about star selected and update model with rating id and questionid
        
        guard  currentIndex != nil else {
            return
            
        }
        statrDelegate?.notifyStarCount?(indexPath: currentIndex!, starCount: Int(rating))
        
    }
}
