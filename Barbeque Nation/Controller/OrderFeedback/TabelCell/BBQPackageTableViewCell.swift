//
 //  Created by Arpana on 15/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQPackageTableViewCell.swift
 //

import UIKit

class BBQPackageTableViewCell: UITableViewCell {

     var collectionViewController: BBQTableCellCollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //        // Initialization code
        
        //  Need to update row height when collection view finishes its layout.
        initCollectionView()
    }
    
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func setData(dataArray:[Question_set], tit: String){
        collectionViewController.setData(dataArray: dataArray, titleHeader: tit)
        collectionViewController.didLayoutAction = updateRowHeight
        self.layoutIfNeeded()

    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    private func initCollectionView() {
        
        collectionViewController = BBQTableCellCollectionView(nibName: String(describing: BBQTableCellCollectionView.self), bundle: nil)
        
    
        //  Need to update row height when collection view finishes its layout.
        
        contentView.addSubview(collectionViewController.view)
        collectionViewController.view.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            collectionViewController.view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10),
            collectionViewController.view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 10),
            collectionViewController.view.topAnchor.constraint(equalTo: contentView.topAnchor ,constant: 20),
            collectionViewController.view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor , constant: 20)
        ])
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        collectionViewController.didLayoutAction = updateRowHeight

    }
    
    private func updateRowHeight() {
        DispatchQueue.main.async  { [weak self] in
            self?.tableView?.updateRowHeightsWithoutReloadingRows()
            self?.layoutIfNeeded()
            
        }
    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        
        return CGSize(width: collectionViewController.collectionView.contentSize.width, height: collectionViewController.collectionView.contentSize.height  + 30 )
        
        //this reflect on collectionview  height, not on cell height
    }
    
}

    

