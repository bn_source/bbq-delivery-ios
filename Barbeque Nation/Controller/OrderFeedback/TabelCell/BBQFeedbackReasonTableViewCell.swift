//
 //  Created by Arpana on 18/10/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified BBQFeedbackReasonTableViewCell.swift
 //

import UIKit

class BBQFeedbackReasonTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tickImageView: UIImageView!

    override func setSelected(_ selected: Bool, animated: Bool) {
       super.setSelected(selected, animated: animated)//This was missing
    }

//    func setData(locationInformation: Datum)  {
//        titleLabel.text =  locationInformation.storeName
//        if (locationInformation.isSelected == true) {
//            self.tickImageView.image = UIImage(named: "CheckBox-Selected")
//        }else {
//            self.tickImageView.image = UIImage(named: "CheckBox-Normal")
//        }
//    }
    
    //Handles the cell selected state
    var checked: Bool! {
        didSet {
            if (self.checked == true) {
                self.tickImageView.image = UIImage(named: "SelectedBrand")
            }else {
                self.tickImageView.image = UIImage(named: "deSelectedBrand")
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        checked = false
        self.layoutMargins = UIEdgeInsets.zero
        self.separatorInset = UIEdgeInsets.zero
    }
}
