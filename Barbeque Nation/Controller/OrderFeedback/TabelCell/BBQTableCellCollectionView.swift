//
 //  Created by Arpana on 15/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQTableCellCollectionView.swift
 //

import UIKit

//MARK: Delegate to be called on click of cell selection
 protocol NotifyTableCellOnSelectionOfCollectionCell  {
   
    func cellSelectedDelegate(indexPath: IndexPath , isSelected: Bool , typeOFFeedback: String, objectTobeSet : Question_set)

}

private let reuseIdentifier = "Cell"

class BBQTableCellCollectionView: UICollectionViewController, UICollectionViewDelegateFlowLayout  {
    
    //  Set this action during initialization to get a callback when the collection view finishes its layout.
    //  To prevent infinite loop, this action should be called only once. Once it is called, it resets itself
    //  to nil.
    var didLayoutAction: (() -> Void)?
    var mainArray: [Question_set] = [] // to hold actual data
    var dataArray: [Question_set] = [] // will be modified be adding removing data depending on questionType
    var titileHead = ""
    //MARK: collectionview attributes to set spacing betwwen
    var minimumInteritemSpacingForSection: CGFloat = 10.0
    var minimumLineSpaceBetweenCollectionViewCell:CGFloat = 10.0
    var collectionViewHeight : CGFloat = 10.0
    var cornerRadiusValue : CGFloat = 0.0
    var borderWidth : CGFloat = 1.0
    let selectedCellHighlightTextColor = UIColor(red: 240/255, green: 75/255, blue: 36/255, alpha: 1)
    let selectedCellHighlightColor = UIColor(red: 254/255, green: 241/255, blue: 228/255, alpha: 1)

     var cellSelectionDelegate: NotifyTableCellOnSelectionOfCollectionCell?
    var collectWidthToBeConsidered :CGFloat = 0.0
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        if let flowLayout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
//            flowLayout.estimatedItemSize = CGSize(width: self.collectionView.frame.size.width - 20  , height :180)
//
//        }
        self.collectionView.collectionViewLayout = TagFlowLayout()
        self.collectionView.register((UINib(nibName:String(describing: BBQPackagingCollectionViewCell.self), bundle: nil)), forCellWithReuseIdentifier: "BBQPackagingCollectionViewCell")
        self.collectionView.register((UINib(nibName:String(describing: BBQFeedBackToggleCollectionViewCell.self), bundle: nil)), forCellWithReuseIdentifier: "BBQFeedBackToggleCollectionViewCell")
        self.collectionView.register((UINib(nibName:String(describing: BBQStarQuestionTypeXCellCollectionViewCell.self), bundle: nil)), forCellWithReuseIdentifier: "BBQStarQuestionTypeXCellCollectionViewCell")
        collectWidthToBeConsidered = self.collectionView.bounds.width - 60 //consider leading and trailing 20, it on tablevie cell that 10

        self.collectionView.isScrollEnabled = false
        self.collectionView.allowsMultipleSelection = true
      //  self.collectionView.contentInsetAdjustmentBehavior = .always
        self.collectionView.clipsToBounds = true
        cornerRadiusValue =  ( (self.view.frame.height * 0.032))
        collectionView.contentMode = .left
          
        print("collectionView width \(self.collectionView.bounds.width)")
        print(" width \(self.view.bounds.width)")

        
    }

    
    
    func setData(dataArray:[Question_set], titleHeader : String){
        self.dataArray.removeAll()
        self.dataArray = dataArray
        mainArray = dataArray // store actiual array
        let foundItems = dataArray.filter { $0.question_type == "single_select" }
        
        //we need not to show single_select type by default.
        //it will only come when we click on toogle type and its questionid matches with parentid of any other element of questionset array
        if foundItems.count == 1 {
            
            if let index = dataArray.firstIndex(where: {$0.question_type == "single_select"}) {
                self.dataArray.remove(at: index)
            }
            
        }
        titileHead = titleHeader
        self.collectionView.reloadData()
    }
    

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        didLayoutAction?()
        didLayoutAction = nil   //  Call only once
    }
    
    // MARK: UICollectionViewDataSource

    
    // set inset for collectionview cell item
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets{
        return UIEdgeInsets(top: 0, left: 0 , bottom: 0, right: 0)
    }
    
    

    
    //give intrinsic space for a item
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return minimumInteritemSpacingForSection
    }
    
    // space between two items
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return minimumLineSpaceBetweenCollectionViewCell
    }
    
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return    dataArray.count
        
    }
        
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return    dataArray[section].questions?.count ?? 0

//        if titileHead  ==  CategorySelected.Package.SectionNameString {
//            return    dataArray[section].questions?.count ?? 0
//        }
//        if titileHead  ==  CategorySelected.Delivery.SectionNameString {
//           return    dataArray[section].questions?.count ?? 0
//        }
//        return 0
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       

            let question_type = dataArray[indexPath.section].question_type
            
            guard let questionModel =   dataArray[indexPath.section].questions?[indexPath.row] else{
                return collectionView.defaultCell(indexPath: indexPath)

            }
            if question_type == "stars" {
                
                
                let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: BBQStarQuestionTypeXCellCollectionViewCell.self), for: indexPath) as! BBQStarQuestionTypeXCellCollectionViewCell
                cell1.rateQuestion.text = questionModel.question_name
                cell1.foodTypeImageView.isHidden = true
               
                if dataArray[indexPath.section].isItem == true{
                    cell1.foodTypeImageView.isHidden = false
                    cell1.rateQuestion.font = UIFont.appThemeRegularWith(size: 14)
                    cell1.foodTypeImageView.image = questionModel.food_type == "0" ? #imageLiteral(resourceName: "Group 846") : #imageLiteral(resourceName: "Group 845")
                    cell1.questionLabelLeadingConstrint.constant = 17.0

                    //its is item data, show nonveg/veg imagview
                }else{
                    //its other type of question
                    cell1.foodTypeImageView.isHidden = true
                    cell1.questionLabelLeadingConstrint.constant = 0.0
                }
                cell1.rateQuestion.sizeToFit()
                cell1.currentIndex = indexPath
                //assign id to label, since index changed with hide ans show of single select type
                cell1.rateQuestion.accessibilityLabel = questionModel.question_id
                cell1.driverRateView.maxRating = dataArray[indexPath.section].rating_set?.count ?? 5
                cell1.statrDelegate = self
 
                cell1.layoutIfNeeded()

                return cell1
            
            }else if question_type == "single_select"{
                
                let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: BBQFeedBackToggleCollectionViewCell.self), for: indexPath) as! BBQFeedBackToggleCollectionViewCell
                cell1.questionLabel.text = dataArray[indexPath.section].questions?[indexPath.row].question_name
                cell1.wrongLButton.setTitle(dataArray[indexPath.section].rating_set?[0].rating_desc, for: .normal)
                cell1.wrongLButton.tag = dataArray[indexPath.section].rating_set?[0].rating_id ?? 0
                cell1.vegNonVegImageView.image = dataArray[indexPath.section].questions?[indexPath.row].food_type  == "0" ? #imageLiteral(resourceName: "Group 846") : #imageLiteral(resourceName: "Group 845")
                cell1.missingButton.setTitle(dataArray[indexPath.section].rating_set?[1].rating_desc, for: .normal)
                cell1.missingButton.tag = dataArray[indexPath.section].rating_set?[1].rating_id ?? 0
                cell1.indexPathForTheItem = indexPath
                cell1.optionDelegate = self
                cell1.layoutIfNeeded()
                return cell1
            }else {
                
                //it will be toogle
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: BBQPackagingCollectionViewCell.self), for: indexPath) as! BBQPackagingCollectionViewCell
                cell.setCellData(isSelected: cell.isCellSelected, questionName: dataArray[indexPath.section].questions?[indexPath.row].question_name ?? "")
                
                cell.layoutIfNeeded()
                return cell
            }

    }
    

    
    
    var selectedIndexPath: IndexPath?

    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
      print("didSelectItemAt \(indexPath)" )

            //delivery
            //get the question type , dispaly cell accordingly
            let question_type = dataArray[indexPath.section].question_type
            
        if question_type == "single_select"  {
            
            selectedIndexPath = indexPath

            
        }else if question_type == "toggle" {
            
            //it will be toogle
            selectedIndexPath = indexPath
            let cell = collectionView.cellForItem(at: indexPath) as! BBQPackagingCollectionViewCell
            cell.isCellSelected = true
            if cell.isCellSelected{
                cell.viewForContainer.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 1.0)
                cell.viewForContainer.backgroundColor = selectedCellHighlightColor
                cell.nameLabel.textColor = selectedCellHighlightTextColor
            }
            
            //check if this particular id is contained as parent_id for any array in question set
            guard let questionModel =   dataArray[indexPath.section].questions?[indexPath.row] else{
                return
            }
            
            
            if let index = dataArray.firstIndex(where: { $0.parent_question_id == Int(questionModel.question_id ?? "0") }) {
                
                //we have associated question list with this particular question
                
                self.dataArray.remove(at: index)
                
                collectionView.deleteSections(NSIndexSet(index:  (indexPath.section )) as IndexSet)
                
                
                
            }else {
                if let index = mainArray.firstIndex(where: { $0.parent_question_id == Int(questionModel.question_id ?? "0") }) {
                    
                    let itemToBeadded = mainArray[index]
                    //we have associated question list with this particular question
                    
                    dataArray.insert(itemToBeadded, at: indexPath.section + 1)
                    collectionView.insertSections(NSIndexSet(index: indexPath.section + 1) as IndexSet)
                    
                    
                }
                else {
                    
                    setYesNoForParticularQuestion(indexPath: indexPath, isSelected: true)
                    
                }
            }
            
        }
            

    }

    
    override func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        let question_type = dataArray[indexPath.section].question_type
        
        if question_type == "toggle" {
            
            let cell = collectionView.cellForItem(at: indexPath )as! BBQPackagingCollectionViewCell
            
            cell.isCellSelected = false
            // cell.nameLabel.accessibilityLabel =  dataArray.first?.questions
            if !cell.isCellSelected {
                cell.viewForContainer.roundCorners(cornerRadius: 5.0, borderColor:  UIColor.hexStringToUIColor(hex:"1C2C40") , borderWidth: 1.0)
                cell.viewForContainer.backgroundColor = .clear
                cell.nameLabel.textColor =  UIColor.hexStringToUIColor(hex:"1C2C40")
            }
            
            //check if this particular id is contained as parent_id for any array in question set
            guard let questionModel =   dataArray[indexPath.section].questions?[indexPath.row] else{
                return
            }
            if let index = dataArray.firstIndex(where: { $0.parent_question_id == Int(questionModel.question_id ?? "0") }) {
                
                //we have associated question list with this particular question
                
                self.dataArray.remove(at: index)
                
                collectionView.deleteSections(NSIndexSet(index:  (index )) as IndexSet)
                
                
            }
            
        }
        print("didDeselectItemAt \(indexPath)" )
    }
    
  //  set size for item in collection view
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        guard  let question_type = dataArray[indexPath.section].question_type else {
             
            return CGSize(width: collectWidthToBeConsidered  , height: 50)
        }
        
        if question_type == "toggle" {
            
            
            var width =   self.dataArray[indexPath.section].questions?[indexPath.row].question_name?.width(withConstrainedHeight: 20.0 , font: UIFont.appThemeRegularWith(size: 14))
         
            //retrieve height of text
            
            var textHeight: CGFloat = 50
            
            if (width ?? 0.0) / collectWidthToBeConsidered > 1 {
                textHeight = CGFloat((Int((width ?? 0.0) / collectWidthToBeConsidered) + 1) * 22)
                textHeight += 25
                width = collectWidthToBeConsidered - 30

            }

            return CGSize(width: (width ?? 0 ) + 30 , height: textHeight)
            

            
        }else if question_type == "single_select" {
            return CGSize(width: collectWidthToBeConsidered  , height: 50)

                    
        }else {
            //In case of other than taste  we consider extra height to display border,
            //so for taste  if its item reduce the height
            return CGSize(width: collectWidthToBeConsidered  , height: dataArray[indexPath.section].isItem == true ? 30 : 60)

        }
    }
    
    
}

extension BBQTableCellCollectionView {
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        coordinator.animate(alongsideTransition: { context in
            // This is called during the animation
        }, completion: {[unowned self] context  in
            print("\(self.view.frame.width)")
            // This is called after the rotation is finished. Equal to deprecated `didRotate`
            self.collectionView.collectionViewLayout.invalidateLayout()
            self.collectionView.reloadInputViews()
            self.collectionView.reloadData()
            
        })
        
    }
    
    
    func setYesNoForParticularQuestion(indexPath : IndexPath , isSelected: Bool)  {
        
        //isSelected means send the id of YES , else send the id for NO
        //1st get the id of respective seslection from rate_set[]
        AnalyticsHelper.shared.triggerEvent(type: .FB11) //toogle question

        var yesID = 0; var noID = 0
        let ratingArray = self.dataArray[indexPath.section].rating_set
        //now yes ratingIDand No ratingId
        
        ratingArray?.forEach { rateSet in
            if rateSet.rating_desc == "Yes"{
                
                yesID = rateSet.rating_id ?? 0
            }else{
                noID = rateSet.rating_id ?? 0

            }
        }
        
        //now check if user input is yes or no  using isSelected variable
        
        //get the question model
        var questionModelToUpdate =  self.dataArray[indexPath.section].questions?[indexPath.row]
        
        if isSelected {
            //user has set yes
            questionModelToUpdate?.answer = String(yesID)
        }else{
            
            //user has set no
            questionModelToUpdate?.answer = String(noID)

        }
        
        self.dataArray[indexPath.section].questions?.remove(at: indexPath.row)
        
        self.dataArray[indexPath.section].questions?.insert(questionModelToUpdate!, at: indexPath.row)
        
        if selectedIndexPath == indexPath {
            
            cellSelectionDelegate?.cellSelectedDelegate(indexPath: selectedIndexPath!, isSelected: false, typeOFFeedback: titileHead , objectTobeSet: self.dataArray[indexPath.section])

            self.selectedIndexPath = nil

            return
        }
    }
                
}

extension BBQTableCellCollectionView: NotifyWrongOrMissedValueToCollectionView {
    
    
    //on click of wrong/missing button pressed, didselect cell does not get called
    //So we can not match selected indexpath == indexPath
    
    func optionSelected(indexPath: IndexPath , questionID: String? , buttonClicked: UIButton ){
        
        AnalyticsHelper.shared.triggerEvent(type: .FB10)//single select question

        //get the same object from data array , add id to the answer key
        var questionRetreived =  self.dataArray[indexPath.section].questions?[indexPath.row]
        
       // if questionRetreived?.question_id ==
        questionRetreived?.answer = String(buttonClicked.tag)
        
        print(questionRetreived.debugDescription)
        
        self.dataArray[indexPath.section].questions?.remove(at: indexPath.row)
        
        self.dataArray[indexPath.section].questions?.insert(questionRetreived!, at: indexPath.row)
        
            
        cellSelectionDelegate?.cellSelectedDelegate(indexPath: indexPath, isSelected: false, typeOFFeedback: titileHead, objectTobeSet : self.dataArray[indexPath.section])
        
        
    }
}
extension BBQTableCellCollectionView : NotifyelectdStarToCollectionViewDelegate {
    
    func notifyStarCount(indexPath: IndexPath , starCount: Int ,  questionID: String){
        
        //update the question model with provided rating
        
        // get the value for passing to cellDelegate, to know which one we have to modify
        // is it Packaging or delivery
        //Do this by checking titleHead
        
        AnalyticsHelper.shared.triggerEvent(type: .FB09)//star select

        
        guard let indexInDataArry  = dataArray.firstIndex(where: { ($0.questions?.contains(where: { $0.question_id == questionID }))! }) else { return  }
        
        
        var questionRetreived =  self.dataArray[indexInDataArry].questions?[indexPath.row]
        
        //get the same object from data array , add id to the answer key
      //  var questionRetreived1 =  self.dataArray[indexPath.section].questions?[indexPath.row]
        
     
        questionRetreived?.answer = String(starCount)
        
        print(questionRetreived.debugDescription)
        
        self.dataArray[indexInDataArry].questions?.remove(at: indexPath.row)
        
        self.dataArray[indexInDataArry].questions?.insert(questionRetreived!, at: indexPath.row)
        
       
            
            // get the index of this in main array, and pass the same indexpath
            // we can not calculate index with dataArray as, we have modify it by inserting and removing section for "single select type

            //first find the section of same questionid from main array
            let indexToChange =  mainArray.firstIndex(where: { ($0.questions?.contains(where: { $0.question_id == questionRetreived?.question_id }))! })
            
            if let indexToChange = indexToChange {
                
                //Just confirm if item available on same index
                guard self.mainArray.count > indexToChange else {
                    return
                }
                //when founf change the question set on detail view with the question set in main array, as it include answer for the respective question
                cellSelectionDelegate?.cellSelectedDelegate(indexPath: IndexPath(row: indexPath.row, section: indexToChange), isSelected: false, typeOFFeedback: titileHead, objectTobeSet: self.dataArray[indexInDataArry])
            

            return
        }
        
       // self.selectedIndexPath = indexPath
        
        //Notify tableview about the selection , depending on its value expand the table cell
        
        if self.selectedIndexPath != nil {
            cellSelectionDelegate?.cellSelectedDelegate(indexPath: selectedIndexPath!, isSelected: true, typeOFFeedback: titileHead, objectTobeSet: self.dataArray[indexPath.section])
        }
        
        
    }
}

