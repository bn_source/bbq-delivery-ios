//
 //  Created by Arpana Rani on 16/03/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQFeedbackToggleTableViewCell.swift
 //

import UIKit

class BBQFeedbackToggleTableViewCell: UITableViewCell {

    @IBOutlet weak var vegNonVegImgView: UIImageView!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var wrongLabel: UILabel!
    @IBOutlet weak var missingLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
