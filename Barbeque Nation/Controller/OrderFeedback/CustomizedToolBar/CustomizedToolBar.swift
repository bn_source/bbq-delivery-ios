  
//
//  Created by Arpana on 29/010/22.
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
//  All rights reserved.
//  Last modified Customizedself.swift
//

import UIKit

@objc protocol ToolBarDoneAndCancelDelegate  {
    
    @objc optional func cancelButtonPressed()
    @objc optional func doneButtonPressed()


}
class CustomizedToolBar: UIToolbar {

    let titleButton =  UIBarButtonItem(title:"" , style: .plain, target: CustomizedToolBar.self, action: nil)
    weak var toolBarDelegate: ToolBarDoneAndCancelDelegate?

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        initUI()
    }
   
    func initUI() {

        
         let flexibleSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil)
        
        
        let flexibleSpace1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil)
  
        
        let addButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneButtonTapped))
        addButton.tintColor = .deliveryThemeColor
        
        
        
        
        self.items = [  flexibleSpace1,titleButton, flexibleSpace, addButton]
        self.backgroundColor =  UIColor.white
        
    }
    
   
    func setTitle(title: String)  {
        
        titleButton.title = title
        
    }
    
     @objc func doneButtonTapped(_ sender: UIButton) {
        toolBarDelegate?.doneButtonPressed?()
   }
    
     @objc func cancelButtonTapped(_ sender: UIButton) {
        toolBarDelegate?.cancelButtonPressed?()
       
   }
}

