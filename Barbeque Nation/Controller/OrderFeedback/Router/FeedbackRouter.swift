//
 //  Created by Arpana on 13/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified FeedbackRouter.swift
 //

import UIKit
import Alamofire

enum FeedbackRouter: Router {

        
    case getFeedbackDetails(orderID:String, transactionType: Int)
    case postFeedbackData(name: String?)
       
        
    var method: HTTPMethod {
        switch self {
        case .getFeedbackDetails:
            return .get
        case .postFeedbackData:
            return .post
        }
    }
        
        var path: String {
            switch self {
            case .getFeedbackDetails:
                return ServicePath.getFeedbackQQuestion.rawValue
            case .postFeedbackData:
                return ServicePath.submitFeedBackData.rawValue
            }
        }
        
        var headers: HTTPHeaders? {
            switch self {
            case .getFeedbackDetails:
                return ["Authorization": EnviornmentConfiguration().environment.authorization]
            case .postFeedbackData:
                return ["Authorization": EnviornmentConfiguration().environment.authorization]
            }
        }
        
        var params: Json? {
            switch self {
            case .getFeedbackDetails(orderID: let orderID , transactionType: let transaction):
                return [
                    "order_id": orderID,
                    "transaction_type" : transaction]
                
            case .postFeedbackData:
                return nil
            }
        }
        
        var bodyParams: Any? {
            switch self {
            case .getFeedbackDetails:
                return nil
            case .postFeedbackData(name: let name):
                return [
                  "name": name]
            }
        }
    }
