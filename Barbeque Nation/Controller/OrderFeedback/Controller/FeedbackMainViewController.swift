//
 //  Created by Arpana on 13/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified FeedbackMainViewController.swift
 //

import UIKit
import ShimmerSwift

protocol FeedbackDetailedDelegate: AnyObject {
    func actionOnDone(viewController : FeedbackDetailedViewController)
   
}


class FeedbackMainViewController: BaseViewController {

    @IBOutlet var floatRatingView: FloatRatingView!
    @IBOutlet var addressLabel: UILabel!
    var headingTitle = ""
    var orderID: String?
    var feedbackViewModel = FeedbackViewModel()
    var feedbackModelResponse: FeedbackModel?
    var transactionType: Int = 1
    var branchName = "BBQN"
    var brandlogo = ""
    var orderDate = ""
    var feedbackDelegate: FeedbackDetailedDelegate?
    var deliveryThemeColor = UIColor.theme
    var deliveryThemeTextColor = UIColor.white
    @IBOutlet weak var rateYourMealLabel: UILabel!
    @IBOutlet weak var itemWithPriceLabel: UILabel!
    @IBOutlet weak var branchNameLabel: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    //@IBOutlet var updatedLabel: UILabel!
    
    @IBOutlet weak var shimmerContainer: UIView!
    
    
    var shimmerView : ShimmeringView?
    @IBOutlet weak var shimmerViewDetails: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        shimmerView = ShimmeringView(frame: self.view.bounds)

        styleUI()
        
    
  
    }
    func styleUI() {
 
        // Reset float rating view's background color
        floatRatingView.backgroundColor = UIColor.clear

        /** Note: With the exception of contentMode, type and delegate,
         all properties can be set directly in Interface Builder **/
        floatRatingView.delegate = self
        floatRatingView.contentMode = UIView.ContentMode.scaleAspectFit
        floatRatingView.type = .wholeRatings
        floatRatingView.rating = 0

        self.navigationController?.view.backgroundColor = .white
        self.navigationController?.navigationBar.tintColor = UIColor(named: "TextColor")
              setOrderIDLabel()
        btnNext.applyMultiBrandTheme(backgroundColor: deliveryThemeColor, textColor: deliveryThemeTextColor)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.screenName(name: .Order_Feedback_Screen)
        hideNavigationBar(true, animated: true)
        view.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
    }
    
    
    private func setOrderIDLabel(){
        
        //Update the bill ui
//        let orderNumber = NSMutableAttributedString(string: "ORDER")

//        if orderID != nil {
//
//            let orderNumberID =  NSAttributedString(string: " #" + (orderID ?? ""), attributes: [NSAttributedString.Key.font: UIFont.appThemeBoldWith(size: 16.0), NSAttributedString.Key.foregroundColor: UIColor(red: 1, green: 0.388, blue: 0.004, alpha: 1)] )
//
//            orderNumber.append(orderNumberID)
//            branchNameLabel.attributedText = orderNumber
//        }
        if branchName != "" {
            addressLabel.text = String(format: "%@", branchName )
        }else {
            addressLabel.text = "BBQN"

        }
        
    }
    private func showShimmering() {
        self.shimmerView?.isHidden = false
        shimmerContainer.isHidden = false
        shimmerViewDetails.isHidden = false
        if let shimmer = self.shimmerView{
            self.shimmerContainer.addSubview(shimmer)
            shimmer.contentView = shimmerViewDetails
            shimmer.shimmerAnimationOpacity = 0.2
            shimmer.shimmerSpeed = 500.00
            shimmer.isShimmering = true
        }
        //sleep(5)

    }
    
    private func stopShimmering() {
        if let shimmer = self.shimmerView {
            shimmer.isShimmering = false
            shimmer.isHidden = true
        }
        shimmerContainer.isHidden = true
        self.shimmerViewDetails.isHidden = true
       
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        hideNavigationBar(true, animated: true)
    }
    
    @IBAction func ratingTypeChanged(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            floatRatingView.type = .wholeRatings
        case 1:
            floatRatingView.type = .halfRatings
        case 2:
            floatRatingView.type = .floatRatings
        default:
            floatRatingView.type = .wholeRatings
        }
    }
    
    @IBAction func nextScreenBtnPressed(_ sender: Any) {
        
        
        if self.floatRatingView.rating == 0 {
            UIUtils.showToast(message: "Please rate first")
            return
        }
        
        let  controller = UIStoryboard.loadFeedBackDetailsViewController()//loadAboutViewController()
        controller.userRatingValue = self.floatRatingView.rating
        controller.orderID = orderID
        controller.transactionType  = transactionType
        controller.feedbackModelResponse = feedbackModelResponse
        controller.brandlogo = brandlogo
        controller.branchName = branchName
        controller.orderDate = orderDate
        controller.deliveryThemeTextColor = deliveryThemeTextColor
        controller.deliveryThemeColor = deliveryThemeColor
        self.dismiss(animated: true, completion: nil)

        feedbackDelegate?.actionOnDone(viewController: controller)
    }
    
    @IBAction func backBtnPressed(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
}

extension FeedbackMainViewController: FloatRatingViewDelegate {

    // MARK: FloatRatingViewDelegate
    
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double) {
        print( String(format: "%.2f", self.floatRatingView.rating))
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
        print( String(format: "%.2f", self.floatRatingView.rating))
        
    }
    
    
    
}


