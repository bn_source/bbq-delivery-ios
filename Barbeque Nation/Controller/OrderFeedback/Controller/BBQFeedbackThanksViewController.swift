//
 //  Created by Arpana Rani on 19/03/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQFeedbackThanksViewController.swift
 //

import UIKit

class BBQFeedbackThanksViewController: UIViewController {

    @IBOutlet weak var thanksMsgLabel: UILabel!
    @IBOutlet weak var smilyImageView: UIImageView!
    @IBOutlet weak var btnMyOrder: UIButton!
    @IBOutlet weak var lblBrandName: UILabel!
    var thanksMsg : String = "Thanks"
    var imagNam : String = "SmilyStar"
    var overallRating: Int = 1
    var timer = Timer()
    var OrderIDForRatedFood : String = ""
    var branchName = "BBQN"
    var orderDate = ""
    var brandlogo = ""
    var deliveryThemeColor = UIColor.theme
    var deliveryThemeTextColor = UIColor.white
    override func viewDidLoad() {
        super.viewDidLoad()
        setData()
        

     
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
      //  timer =    Timer.scheduledTimer(timeInterval: 0.8, target: self, selector: #selector(navigateToOrderHistoryPage), userInfo: nil, repeats: false)

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
      //  timer.invalidate()
    }
    func setData()  {
        btnMyOrder.applyMultiBrandTheme(backgroundColor: deliveryThemeColor, textColor: deliveryThemeTextColor)
        thanksMsgLabel.text = thanksMsg
        lblBrandName.text = branchName
        if brandlogo != ""{
            smilyImageView.setImagePNG(url: brandlogo)
        }
//        if overallRating > 3{
//            imagNam = "HappySmile"
//        }
//        smilyImageView.image = UIImage(named: imagNam)
        
        
    }
    @IBAction func backBtnPressed(_ sender: Any) {
        navigateToOrderHistoryPage()
    }
    
    @objc func navigateToOrderHistoryPage() {
        
        var isFound = false
        
        if let tabBarController = navigationController?.viewControllers{
            for vc in tabBarController {
                if vc.isKind(of: OrderHistoryViewController.self){
                    
                    let transition = CATransition()
                    transition.duration = 0.9
                    transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
                    transition.type = CATransitionType.fade
                    (vc as? OrderHistoryViewController)?.foodratedOrderID = OrderIDForRatedFood
                    self.navigationController?.view.layer.add(transition, forKey: kCATransition)
                    self.navigationController?.popToViewController(vc, animated: false)
                    isFound = true
                    break
                }
            }
        }
        if !isFound {
            if let tabBarController = navigationController?.viewControllers{
                for vc in tabBarController {
                    
                    if vc.isKind(of: DeliveryTabBarController.self){
                        self.navigationController?.popToViewController(vc, animated: false)
                        
                        isFound = true
                        break
                    }
                    
                }
                
            }
        }
        
    }
}
