//
 //  Created by Arpana on 10/10/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified BBQSideMenuFeedbackViewController.swift
 //

import UIKit
import PhotosUI

import AVFoundation

import Photos
import MobileCoreServices
import QuickLook
import AVKit



//MARK: Delegate to remove selected product on click of cross button on collection view cell
@objc protocol RemoveSeletedImageForInvoiceChallanDelegate  {
   
    @objc optional func removeImageAtIndex( uniqueIdentifier :String, index:Int , collectionView: UICollectionView?)

}


class  PhotoModel  {
    
    var uniqueIdentifier: String?
    var image: Data?
    var url: URL?
  //  var attachementType: attachementType? = .isImage
    var filenameFromServerResponse :String = ""
    
    //Added for PO images, as it needs file type for challan and invoice
    var FileType : String?
    //file name string
    init(uniqueIdentifier:String, image:Data?, url: URL? ){
    
        self.uniqueIdentifier = uniqueIdentifier
        self.image = image
        self.url = url
       // self.attachementType = typAttachement
    }
    
}



struct FeedbackSmilyModel {
    var id : String = "0"
    var title: String =  ""
    var imageName: String?
    
     init(localId: String , localTitle: String, localImageName: String) {
        
        id = localId
        title = localTitle
        imageName = localImageName
    }
}

class BBQSideMenuFeedbackViewController: BBQBaseViewController {
    
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var collectionViewBrand : UICollectionView!
    @IBOutlet weak var collectionViewPhotos : UICollectionView!
    @IBOutlet weak var collectionViewSmileyFeedback : UICollectionView!
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblEmail : UILabel!
    @IBOutlet weak var lblPhone : UILabel!
    @IBOutlet weak var txtFieldOutletSelected: UITextField!
    @IBOutlet weak var txtFieldReservationNo : UITextField!
    var photoUrl = [URL]()
    @IBOutlet weak var txtFieldName: UITextField!
    @IBOutlet weak var txtFieldPhone: UITextField!
    @IBOutlet weak var txtFieldEmail: UITextField!
    lazy  var toolBar : CustomizedToolBar? =  CustomizedToolBar()
    @IBOutlet weak var btnChecTerms: UIButton!
    
    var selectedCity = ""
    @IBOutlet weak var txtFieldReasonDropdown: UITextField! {
        didSet {
            txtFieldReasonDropdown.tintColor = UIColor.lightGray
            if #available(iOS 13.0, *) {
                txtFieldReasonDropdown.setRightButton((UIImage(systemName: "arrowtriangle.down")! ))
            } else {
                    // Fallback on earlier versions
                }
        }
     }
    @IBOutlet weak var picker: UIPickerView!
    //  //////////////////All required outlets for data picker input///////////////////////////////////////////////
    @IBOutlet weak var pickerUIView: UIView!
    @IBOutlet weak var pickerBackgroundView : UIView!
    
    var pickerData: [FeedBackQuestionMaster]?
    var feedbackModelResponse : SideMenuFeedBackRequestModel?
    var feedbackReasonListResponse : FeedbackReasonListModel?

    @IBOutlet weak var txtViewMessage: UITextView!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var viewSubmitBtn: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var vewEditDetails: UIView!
    @IBOutlet weak var viewPreFilledDetails: UIView!
    @IBOutlet weak var btnUpload: UIButton!
    var selectedImagesArray : [PhotoModel]?
  //  @IBOutlet weak var tbleViewFeedbackType: UITableView!
   // @IBOutlet weak var tblViewReasonHeight : NSLayoutConstraint!
    @IBOutlet weak var heightOfImageCollectionView: NSLayoutConstraint!
    @IBOutlet weak var deliveryAndDineInSegmentControl: UISegmentedControl!
    var activeTextField: UITextField = UITextField()
    var deliveryDineInValue = 1
    
    @IBOutlet weak var viewTableList : UIView!
    var selectedReason : String?
    var isTermAccepted = false
    //for toggle button selection/unselection
    var unSelectedBorderColor =  UIColor(red: 206/255, green: 207/255, blue: 219/255, alpha:1 )
    var unSelectedTextColor =  UIColor(red: 90/255, green: 102/255, blue: 121/255, alpha:1 )
    var selectedBackgroundColor = UIColor.hexStringToUIColor(hex: "FB4A1E")
    
    var isFeedBackBySelf = true
    var itemProviders : [NSItemProvider] = []
    var iterator  : IndexingIterator<[NSItemProvider]>?
    
        lazy var homeViewModel : HomeViewModel = {
            let homeModel = HomeViewModel()
            return homeModel
        }()
    
// FeedbackSmilyModel(localId: "5", localTitle: "Amazing", localImageName: "SmileyAmazing")]
    lazy var feedbackTypeList : [FeedbackSmilyModel] = {
        let list = [FeedbackSmilyModel(localId: "4", localTitle: "Excellent", localImageName: "SmileyAmazing"),
                    FeedbackSmilyModel(localId: "3", localTitle: "Good", localImageName: "SmileyGood"),
                    FeedbackSmilyModel(localId: "2", localTitle: "Average", localImageName: "SmileyMeh"),
                    FeedbackSmilyModel(localId: "1", localTitle: "Poor", localImageName: "SmileySad")]
        return list
    }()
    
    var selectedFeedbackTypeID = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getreasonList()
        // Connect data:
        self.picker.delegate = self
        self.picker.dataSource = self
        pickerBackgroundView.isHidden = true
        setToolBarForPicker()
        btnSubmit.isEnabled = false
        btnSubmit.alpha = 0.5

        
    }
    
    
    override func viewWillAppear(_ animated : Bool){
        super.viewWillAppear(animated)
         hideNavigationBar(true, animated: true)

        NotificationCenter.default.addObserver(self, selector: #selector(locationDataUpdatedForFeedbackPurpose(_:)), name: Notification.Name(rawValue:Constants.NotificationName.locationDataUpdatedForFeedbackPurpose), object:nil)
        if brandlistForHomePage.count == 0 {
            
             getBrands()

        }
        self.setUI()
        setData()
        
    }
    
    
    func getreasonList() {
              
        UIUtils.showLoader()
        BBQServiceManager.getInstance().feedbackReasonListFromSideMenu() { [self]
            (error, response) in
            UIUtils.hideLoader()
             // stopShimmering()
            
            if let response = response {
                if response.message_type == "success" {
                    
                    AnalyticsHelper.shared.triggerEvent(type: .FB02A)
                    
                    feedbackReasonListResponse = response
                    if (feedbackReasonListResponse?.feedBackQuestionMaster != nil) && (feedbackReasonListResponse?.feedBackQuestionMaster?.count ?? 0 > 0 ){
                        pickerData = feedbackReasonListResponse?.feedBackQuestionMaster ?? []
                        picker.reloadAllComponents()
                    }
                } else {
                    //AnalyticsHelper.shared.triggerEvent(type: .FB02B)
                }
            }
            view.layoutIfNeeded()
        }
  
    }

//    override func observeValue(forKeyPath keyPath: String?, of object: Any?,
//                               change: [NSKeyValueChangeKey : Any]?,
//                               context: UnsafeMutableRawPointer?) {
//        if (object as? UITableView) == tbleViewFeedbackType && (keyPath == "contentSize") {
//
//            tbleViewFeedbackType.layer.removeAllAnimations()
//            tblViewReasonHeight.constant = tbleViewFeedbackType.contentSize.height + 40
//            UIView.animate(withDuration: 0.5) {
//                self.updateViewConstraints()
//            }
//        }
//    }
    
    func setToolBarForPicker()   {
        self.navigationController!.isNavigationBarHidden = false;
        // ToolBar
        // add toolbar for picker view
        toolBar?.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44.0)
        //below two textfield will have pickerview as input
        txtFieldReasonDropdown.inputAccessoryView = toolBar
        txtFieldReasonDropdown.inputView = self.pickerUIView
         toolBar?.toolBarDelegate = self
        toolBar?.setTitle(title: "")
        
        if let pickerRetrieved =   self.picker  {
            pickerRetrieved.backgroundColor = UIColor.white
        }

        
    }
    
    @IBAction func checkTermsAndConditions(_ sender : UIButton){
        
        sender.isSelected = sender.isSelected == true ? false : true
        isTermAccepted = sender.isSelected
        if sender.isSelected == true {
            if #available(iOS 13.0, *) {
                btnChecTerms.setImage(UIImage(systemName: "checkmark.square.fill"), for: .normal)
            } else {
                // Fallback on earlier versions
            }
            
        }else {
            if #available(iOS 13.0, *) {
                btnChecTerms.setImage(UIImage(systemName: "square"), for: .normal)
            } else {
                // Fallback on earlier versions
            }
            
        }
        if (enableSubmitBtn()){
            btnSubmit.isEnabled = true
            btnSubmit.alpha = 1.0
        }else
            {
            btnSubmit.isEnabled = false
            btnSubmit.alpha = 0.5

        }
        
    }
    
    //MARK: Remove it after finalize code
    func fd() {
        if  let provider = iterator?.next() , provider.canLoadObject(ofClass: UIImage.self) {
           // let previousImage = imgView.image
            if #available(iOS 14.0, *) {
                if provider.hasItemConformingToTypeIdentifier(UTType.image.identifier) {
                    provider.loadItem(forTypeIdentifier: UTType.image.identifier, options: [:]) { [self] (imageURL, error) in
                        print("resullt:", imageURL, error)
                        //                    DispatchQueue.main.async {
                        //                        if let url = videoURL as? URL {
                        //                            let player = AVPlayer(url: url)
                        //                            let playerVC = AVPlayerViewController()
                        //                            playerVC.player = player
                        //                            present(playerVC, animated: true, completion: nil)
                        //                        }
                        //                    }
                        
                        if let url = imageURL as? URL {
                            
                            photoUrl.append(url )
                        }
                    }
                    
                }
            } else {
                // Fallback on earlier versions
            }
            
            print(photoUrl)
            }
    }
    
    /*
    func displayNextImage() {
        if  let itemProvider = iterator?.next() , itemProvider.canLoadObject(ofClass: UIImage.self) {
            let previousImage = imgView.image
            itemProvider.loadObject(ofClass: UIImage.self ) { [weak self] img , err in
                
                DispatchQueue.main.async {
                    guard let self = self, let image = img as? UIImage, self.imgView.image == previousImage else {
                        return
                    }
                    
                    
                    let photoM =  PhotoModel(uniqueIdentifier:String(self.selectedImagesArray?.count ?? 1), image: image.pngData(), url: nil )
                    if self.selectedImagesArray == nil {
                        self.selectedImagesArray = Array.init()
                    }
                    self.selectedImagesArray?.append(photoM)
                    
                    
                }
                
                self?.collectionViewPhotos.reloadData()
            }
            
        }
    }*/
    func displayNextImage() {
        if  let itemProvider = iterator?.next() , itemProvider.canLoadObject(ofClass: UIImage.self) {
            itemProvider.loadObject(ofClass: UIImage.self ) { [weak self] img , err in
                
                DispatchQueue.main.async {
                    guard let self = self, let image = img as? UIImage else {
                        return
                    }
                    
                    
                    let photoM =  PhotoModel(uniqueIdentifier:String(self.selectedImagesArray?.count ?? 1), image: image.pngData(), url: nil )
                    if self.selectedImagesArray == nil {
                        self.selectedImagesArray = Array.init()
                    }
                    self.selectedImagesArray?.append(photoM)

                    if self.selectedImagesArray?.count == 3 {
                        
                        self.collectionViewPhotos.reloadData()

                    }
                }
                
            }
            
        }
        

    }
    
    private func showPicker() {
        
        if #available(iOS 14.0, *) {
            var configuration = PHPickerConfiguration()
            configuration.selectionLimit = 4
            configuration.filter = .any(of: [.livePhotos, .images])
            let picker = PHPickerViewController(configuration: configuration)
            picker.delegate = self
            self.present(picker, animated: true, completion: nil)
        } else {
            // Fallback on earlier versions
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
                    let imagePicker = UIImagePickerController()
                    imagePicker.delegate = self
                    imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
                    imagePicker.mediaTypes = [ kUTTypeImage as String ]
                    imagePicker.allowsEditing = true
                    self.present(imagePicker, animated: true, completion: nil)
                }
               
            
        }
    }
    
    func setUI(){
        
        self.navigationView.layer.masksToBounds = false
        self.navigationView.layer.shadowOpacity = 1
        self.navigationView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.02).cgColor
        self.navigationView.layer.shadowOffset = CGSize(width: 0 , height: 2)
        self.navigationView.layer.shadowPath = UIBezierPath(roundedRect:  self.navigationView.bounds, cornerRadius: 0).cgPath
        self.navigationView.layer.shadowRadius = 4
        
        
        self.viewSubmitBtn.layer.masksToBounds = false
        self.viewSubmitBtn.layer.shadowOpacity = 1
        self.viewSubmitBtn.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        self.viewSubmitBtn.layer.shadowOffset = CGSize(width: 0 , height: 2)
        self.viewSubmitBtn.layer.shadowPath = UIBezierPath(roundedRect:  self.navigationView.bounds, cornerRadius: 0).cgPath
        self.viewSubmitBtn.layer.shadowRadius = 4
        self.btnSubmit.backgroundColor = .deliveryThemeColor
        self.btnEdit.tintColor = .deliveryThemeColor
        self.btnEdit.setTitleColor(.deliveryThemeColor, for: .normal)
        self.btnChecTerms.tintColor = .deliveryThemeColor
        //tbleViewFeedbackType.addObserver(self, forKeyPath: "contentSize" , options: .new, context: nil)
        deliveryAndDineInSegmentControl.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.appThemeMediumWith(size: 12)], for: .normal)
        deliveryAndDineInSegmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.lightGray], for: .disabled)
        // selected option color
        deliveryAndDineInSegmentControl.setTitleTextAttributes([.foregroundColor: UIColor.white], for: .selected)
        deliveryAndDineInSegmentControl.selectedSegmentIndex = 0
        
        deliveryAndDineInSegmentControl.backgroundColor = UIColor.hexStringToUIColor(hex: "FFFBF5")
        
        deliveryAndDineInSegmentControl.tintColor =  UIColor.hexStringToUIColor(hex: "FFFBF5")
        
        deliveryAndDineInSegmentControl.setTitleTextAttributes([.foregroundColor: UIColor.hexStringToUIColor(hex: "303030")], for: .normal)

        if #available(iOS 13.0, *) {
            deliveryAndDineInSegmentControl.selectedSegmentTintColor = selectedBackgroundColor
        } else {
            deliveryAndDineInSegmentControl.tintColor = selectedBackgroundColor
        }

        setHeightOfCollectionView()
    }
    
    func setData(){
        
        lblName.text = BBQUserDefaults.sharedInstance.UserName
        lblEmail.text = SharedProfileInfo.shared.profileData?.email
        lblPhone.text = SharedProfileInfo.shared.profileData?.mobileNumber
        
    }
    
    
    override func viewWillDisappear(_ animated : Bool){
        
      //  tbleViewFeedbackType.removeObserver(self, forKeyPath: "contentSize")

    }
    //MARK : - locationDataUpdated called when location changes
    @objc func locationDataUpdatedForFeedbackPurpose(_ notification: Notification) {
        if let dictionary = notification.userInfo as NSDictionary? {
            let branchName = dictionary["branchName"] as! String
            let cityName = dictionary["cityName"] as! String

            selectedCity = cityName
            txtFieldOutletSelected.text = branchName
            txtFieldOutletSelected.accessibilityLabel = dictionary["branchId"] as? String
            
        }
        if (enableSubmitBtn()){
            btnSubmit.isEnabled = true
            btnSubmit.alpha = 1.0
        }else
            {
            btnSubmit.isEnabled = false
            btnSubmit.alpha = 0.5

        }
    }
    

    
    
    @IBAction func valueChangedOnDeliveryAndDineInegmentControl(_ sender: Any) {
        
      //  viewTableList.isHidden = false

        //call reason list depending on delivery/dinein value
    }
   
    @IBAction func backButtonTapped(_ sender: Any) {
        
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func editDetailsClicked(_ sender: Any) {
        
        
        isFeedBackBySelf = false
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
            //Show entry text field for details
            self.vewEditDetails.isHidden = false
            self.viewPreFilledDetails.isHidden = true
        } completion: { (isCompleted) in
            //Show entry text field for details
            self.vewEditDetails.isHidden = false
            self.viewPreFilledDetails.isHidden = true
        }
    
        if (enableSubmitBtn()){
            btnSubmit.isEnabled = true
            btnSubmit.alpha = 1.0
        }else
            {
            btnSubmit.isEnabled = false
            btnSubmit.alpha = 0.5

        }
    }
    
    func enableSubmitBtn() -> Bool {
        
        if isFeedBackBySelf == false {
           
            if txtFieldName.text == ""  || txtFieldName.text  == nil {
                
                 return false
            }
            if txtFieldPhone.text  == "" || txtFieldPhone.text  == nil {
                
                 return false
            }
            
            if txtFieldEmail.text  == ""  || txtFieldEmail.text  == nil {
                
                 return false
            }
            
        }
        if txtViewMessage.text == "" || txtViewMessage.text == nil  || txtViewMessage.text == "Write your message here" {
            
            return false
        }
        if (txtFieldOutletSelected.accessibilityLabel == "")  || (txtFieldOutletSelected.accessibilityLabel == nil) {
            
             return false
        }
        if (txtFieldReasonDropdown.accessibilityLabel == "")  || (txtFieldReasonDropdown.accessibilityLabel == nil) {
            
             return false
        }
        if selectedFeedbackTypeID == "" {
            
             return false
        }

        if isTermAccepted == false {
            
             return false
        }
       return true
    }
    
    func validateData() -> Bool {
        
        if isFeedBackBySelf == false {
           
            if txtFieldName.text == "" {
                
                UIUtils.showToast(message: "Please enter name")
                 return false
            }
            if txtFieldPhone.text  == "" {
                
                UIUtils.showToast(message: "Please enter phone no.")
                 return false
            }
            
            var validNumber = BBQPhoneNumberValidation.isValidNumber(number: txtFieldPhone.text ?? "", for: BBQUserDefaults.sharedInstance.phoneCode)
            
            if validNumber == true {
                
                validNumber =   txtFieldPhone.text?.count ?? 0 < 10 ?false :true
                
            }
            if validNumber == false {
               
                UIUtils.showToast(message: "Please enter valid mobile number")
                 return false
            }
            
            if txtFieldEmail.text  == ""  || txtFieldEmail.text  == nil {
                
                UIUtils.showToast(message: "Please enter email")
                 return false
            }
            
            if !(txtFieldEmail.text!.isValidEmailID()) {
                
                UIUtils.showToast(message: "Please enter valid email")
                 return false
            }
            
        }
        if txtViewMessage.text == "" || txtViewMessage.text == nil  || txtViewMessage.text == "Write your message here" {
            
            UIUtils.showToast(message: "Please enter your message")
            return false
        }
        if (txtFieldOutletSelected.accessibilityLabel == "")  || (txtFieldOutletSelected.accessibilityLabel == nil) {
            
            UIUtils.showToast(message: "Please select outlet")
             return false
        }
        if (txtFieldReasonDropdown.accessibilityLabel == "")  || (txtFieldReasonDropdown.accessibilityLabel == nil) {
            
            UIUtils.showToast(message: "Please select feedback subject")
             return false
        }
        if selectedFeedbackTypeID == "" {
            
            UIUtils.showToast(message: "Please rate your experience")
             return false
        }

        if isTermAccepted == false {
            
            UIUtils.showToast(message: "Please authorize")
             return false
        }
        btnSubmit.isEnabled = true
        btnSubmit.alpha = 1.0
       return true
    }
    
    @IBAction func submitBtnClicked( _ sender :Any){
        
        if validateData() {
            
            self.feedbackModelResponse = SideMenuFeedBackRequestModel()
            
            self.feedbackModelResponse?.outlet = txtFieldOutletSelected.text
            self.feedbackModelResponse?.branch_id = txtFieldOutletSelected.accessibilityLabel
            self.feedbackModelResponse?.feedback_message = txtViewMessage.text
            self.feedbackModelResponse?.feedback_type_id =  deliveryDineInValue
            self.feedbackModelResponse?.feedback_question_id =  Int(txtFieldReasonDropdown.accessibilityLabel ?? "0" )
            self.feedbackModelResponse?.order_id =  txtFieldReservationNo.text ?? ""
            self.feedbackModelResponse?.rating = Int(selectedFeedbackTypeID ?? "0")
            self.feedbackModelResponse?.city = selectedCity
            
            if isFeedBackBySelf {
                
                
                if  SharedProfileInfo.shared.profileData?.email == "" || SharedProfileInfo.shared.profileData?.email == nil {
                    
                    UIUtils.showToast(message: "Please enter valid email by Click on Edit")
                    
                    txtFieldName.text = BBQUserDefaults.sharedInstance.UserName
                    txtFieldPhone.text = SharedProfileInfo.shared.profileData?.mobileNumber
                    txtFieldEmail.becomeFirstResponder()
                    editDetailsClicked(btnEdit)
                    return
                }
                
                self.feedbackModelResponse?.user_email = SharedProfileInfo.shared.profileData?.email //lblEmail.text
                self.feedbackModelResponse?.user_name = BBQUserDefaults.sharedInstance.UserName //lblName.text
                self.feedbackModelResponse?.user_phone = SharedProfileInfo.shared.profileData?.mobileNumber //lblPhone.text
            }else{
                self.feedbackModelResponse?.user_email = txtFieldEmail.text
                self.feedbackModelResponse?.user_name = txtFieldName.text
                self.feedbackModelResponse?.user_phone = txtFieldPhone.text
            }
            
            self.feedbackModelResponse?.user_auth_key =  self.isTermAccepted == true ?1 :0
            self.feedbackModelResponse?.images_url = []
            
            //   AnalyticsHelper.shared.triggerEvent(type: .FB03)
            
            let JSONString = feedbackModelResponse?.toJSONString(prettyPrint: true)
            
            guard let jsonToSend = jsonToDictionary(from: JSONString!) else { return  }
            
            UIUtils.showLoader()
            btnSubmit.isEnabled = false
            AnalyticsHelper.shared.triggerEvent(type: .FB07)
            
            BBQServiceManager.getInstance().submitFeedbackResponseFromSideMenu(params: jsonToSend) { (error, result) in
                DispatchQueue.main.async {
                    
                    UIUtils.hideLoader()
                    self.btnSubmit.isEnabled = true
                    if let response = result {
                        if response.message_type == "failed" {
                            UIUtils.showToast(message: response.message ?? "failed")
                            AnalyticsHelper.shared.triggerEvent(type: .FB07B)
                        } else {
                            AnalyticsHelper.shared.triggerEvent(type: .FB07A)
                            let  controller = UIStoryboard.loadSideMenuFeedBackThanksViewController()
                            controller.overallRating = Int(self.selectedFeedbackTypeID ?? "1") ?? 1
                            controller.thanksMsg = "Thanks"
                            self.navigationController?.pushViewController(controller, animated: false)
                        }
                    } else {
                        UIUtils.showToast(message: error?.message ?? UNEXPECTED_ERROR)
                    }
                }
            }
        }
                            
    }
    @IBAction func editDetailsCloseClicked(_ sender: Any) {
        
        if (SharedProfileInfo.shared.profileData?.email == "" || SharedProfileInfo.shared.profileData?.email == nil)  &&  txtFieldEmail.text != "" {
            
            //This case is to resolve issue of blank email id for few logged in user
            SharedProfileInfo.shared.profileData?.email = txtFieldEmail.text
        }
        
        isFeedBackBySelf = true

        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
            //hide entry text field for details
            self.vewEditDetails.isHidden = true
            self.viewPreFilledDetails.isHidden = false
        } completion: { (isCompleted) in
            //Show entry text field for details
            self.vewEditDetails.isHidden = true
            self.viewPreFilledDetails.isHidden = false
        }
        if (enableSubmitBtn()){
            btnSubmit.isEnabled = true
            btnSubmit.alpha = 1.0
        }else
            {
            btnSubmit.isEnabled = false
            btnSubmit.alpha = 0.5

        }
  
    }
    
    @IBAction func addAttachementClicked(_ sender: Any) {
        
        showAtert()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            view.endEditing(true)
        activeTextField.resignFirstResponder()

        if self.pickerBackgroundView.isHidden == false {
            self.pickerBackgroundView.isHidden = true
        }
        
        if (enableSubmitBtn()){
            btnSubmit.isEnabled = true
            btnSubmit.alpha = 1.0
        }else
            {
            btnSubmit.isEnabled = false
            btnSubmit.alpha = 0.5

        }
    }
    
}
extension BBQSideMenuFeedbackViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collectionViewBrand {
            return  brandlistForHomePage.count
        }else if collectionView == collectionViewPhotos {
            
            return selectedImagesArray?.count ?? 0
        }else {
            
            return feedbackTypeList.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionViewBrand {
            
            if brandlistForHomePage.count > indexPath.row, let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeedbackBrandCollectionViewCell", for: indexPath) as? FeedbackBrandCollectionViewCell{
                cell.setCellData(brandInfo: brandlistForHomePage[indexPath.row])
                return cell
            }
            return collectionView.defaultCell(indexPath: indexPath)
        }
        else if collectionView == collectionViewPhotos {
            
            if selectedImagesArray?.count ?? 0 > indexPath.row, let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotosCollectionViewCell", for: indexPath) as? PhotosCollectionViewCell{
                
                cell.imageView.image = UIImage(data: selectedImagesArray?[indexPath.row].image ?? Data())
                fixImageOrientation(cell.imageView.image! )

                cell.removeDelegate = self
                cell.removeButton.accessibilityLabel = selectedImagesArray?[indexPath.row].uniqueIdentifier
                return cell
            }
            return collectionView.defaultCell(indexPath: indexPath)
        }
        else{
            
            if feedbackTypeList.count > indexPath.row, let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeedbackSmileyCollectionViewCell", for: indexPath) as? FeedbackSmileyCollectionViewCell  {
                cell.imgViewSmileyIcon.image = UIImage(named:  feedbackTypeList[indexPath.row].imageName ?? "SmileyAmazing" )
                cell.lblTitle.text = feedbackTypeList[indexPath.row].title
                return cell
            }
            return collectionView.defaultCell(indexPath: indexPath)
            
        }
        
    }
}
extension BBQSideMenuFeedbackViewController:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionViewBrand {

//            var count = 2
//            if brandlistForHomePage.count >= 3{
//                count = 3
//            }
//            return CGSize(width: (collectionView.frame.size.width / CGFloat(count)), height: collectionViewBrand.frame.size.height - 4)
            return  CGSize(width: collectionView.frame.size.width * 0.34, height: 70 )

       }
        else if collectionView == collectionViewSmileyFeedback {

            return CGSize(width: (collectionView.frame.size.width / CGFloat(4)), height: collectionView.frame.size.height - 4)

       }
            
           return  CGSize(width: 90, height: collectionView.frame.size.height - 4)
    }
}

extension BBQSideMenuFeedbackViewController: UICollectionViewDelegate {
       
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collectionViewBrand {
            
            let cell = collectionView.cellForItem(at: indexPath) as! FeedbackBrandCollectionViewCell
            cell.toggleSelected()
        }else if collectionView == collectionViewSmileyFeedback {
            let cell = collectionView.cellForItem(at: indexPath) as! FeedbackSmileyCollectionViewCell
            collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            cell.toggleSelected()
            
            if cell.isSelected == true {
                
                selectedFeedbackTypeID = feedbackTypeList[indexPath.row].id
            }
            
        }
        
        if (enableSubmitBtn()){
            btnSubmit.isEnabled = true
            btnSubmit.alpha = 1.0
        }else
            {
            btnSubmit.isEnabled = false
            btnSubmit.alpha = 0.5

        }
        
    }

    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        if collectionView == collectionViewBrand {
            let cell = collectionView.cellForItem(at: indexPath) as! FeedbackBrandCollectionViewCell
            cell.toggleSelected()
        }else if collectionView == collectionViewSmileyFeedback {
            
            let cell = collectionView.cellForItem(at: indexPath) as! FeedbackSmileyCollectionViewCell
            cell.toggleSelected()
        }
    }
    

    
}



// MARK: Api calls
extension BBQSideMenuFeedbackViewController {
    
    
    func getBrands(){
        let lat: CGFloat = 0
        let long: CGFloat = 0
            self.homeViewModel.getNearbyOutletsV1(latitide: lat, longitude: long) { (isSuccess) in
                if brandlistForHomePage.count > 1{
                    self.collectionViewBrand.reloadData{
                        self.collectionViewBrand.reloadData()
                    }
                }
            }
//        }
        
    }
}

extension BBQSideMenuFeedbackViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        activeTextField = textField
        if textField == txtFieldOutletSelected {
            txtFieldOutletSelected.accessibilityLabel = ""
            txtFieldOutletSelected.resignFirstResponder()
            activeTextField.resignFirstResponder()
            self.view.endEditing(true)
            selectOutlet()
        }
        
        if textField == txtFieldReasonDropdown {
//            txtFieldReasonDropdown.resignFirstResponder()
//            view.endEditing(true)
            if let pickerview =   self.picker  {
                pickerview.removeFromSuperview()
            }
            txtFieldReasonDropdown.inputView = self.picker;
           
            txtFieldReasonDropdown.becomeFirstResponder()
            pickerTextTapped()
            
            if picker != nil {
            picker.reloadAllComponents()
            }

        }

    }
    
    
     func selectOutlet() {
        
         activeTextField.resignFirstResponder()
         txtFieldOutletSelected.resignFirstResponder()
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: self.view.frame.size.height - 250.0)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        let viewController = BBQLocationViewController()
         viewController.isFromFeedbackScreen = true
        viewController.location = BBQLocationManager.shared.currentLocationManager.location
        viewController.setupPresentingController(controller: bottomSheetController)
        bottomSheetController.present(viewController, on: self, viewHeight: UIScreen.main.bounds.height - 165)
        self.view.bringSubviewToFront(viewController.view)

    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
       
        if textField == txtFieldOutletSelected {
            
            activeTextField.resignFirstResponder()
            txtFieldOutletSelected.resignFirstResponder()
            view.endEditing(true)
        }
        if (enableSubmitBtn()){
            btnSubmit.isEnabled = true
            btnSubmit.alpha = 1.0
        }else
            {
            btnSubmit.isEnabled = false
            btnSubmit.alpha = 0.5

        }

        if isFeedBackBySelf == true && textField == txtFieldEmail {
            SharedProfileInfo.shared.profileData?.email =  txtFieldEmail.text
        }

    }
    
    //the below method will allow text field to jump to next textfield on press of enter
    //Since we have assigned tag for each textfield , so once return is call the textfield withh +1 tag will become forst responder
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        
        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            //tag 3 is for country, so need to dismiss keyboard
            nextResponder.becomeFirstResponder()
        }
        textField.resignFirstResponder()
        
        return true
    }
    

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        activeTextField.resignFirstResponder()
        txtFieldOutletSelected.resignFirstResponder()
        self.view.endEditing(true)
    }
    
    //this method is called aver time a character is typed
    
    func  textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if textField == txtFieldOutletSelected  || textField == txtFieldReasonDropdown {
            return false
        }
        
        if textField == txtFieldPhone {
            
            let textVal = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
                            
                if textVal.count >  10 {
                    
                    //unhide nextButton
                  return false
                    
                }
        }
        return true
    }
    
    
}


extension BBQSideMenuFeedbackViewController : UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if (txtViewMessage.text == "Write your message here")
        {
            txtViewMessage.text = nil
            txtViewMessage.font = UIFont(name: Constants.App.BBQAppFont.ThemeRegular, size: 14.0)
        }
        
    }

    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if txtViewMessage.text.isEmpty
        {
            txtViewMessage.text = "Write your message here"
            txtViewMessage.font = UIFont(name: Constants.App.BBQAppFont.ThemeLight, size: 12.0)
        }
        txtViewMessage.resignFirstResponder()
        if (enableSubmitBtn()){
            btnSubmit.isEnabled = true
            btnSubmit.alpha = 1.0
        }else
            {
            btnSubmit.isEnabled = false
            btnSubmit.alpha = 0.5

        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let currentText = textView.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: text)
        return updatedText.count <= 100
    }
    
}
extension BBQSideMenuFeedbackViewController :PHPickerViewControllerDelegate {
    
    @available(iOS 14.0, *)
    func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
        dismiss(animated: true)
        
        let itemProviders = results.map(\.itemProvider)
//        for item in itemProviders {
//            if item.canLoadObject(ofClass: UIImage.self) {
//                item.loadObject(ofClass: UIImage.self) { (image, error) in
//                    DispatchQueue.main.async {
//                        if let image = image as? UIImage {
//                            self.imgView.image = nil
//                            self.imgView.image = image
//
//                            print(image.imageData)
//                        }
//                    }
//                }
//            }
//        }
        
                for item in itemProviders {
                    if item.canLoadObject(ofClass: UIImage.self) {
                        item.loadObject(ofClass: UIImage.self) {  [weak self] (image, error) in
                            
                            DispatchQueue.main.async {
                                guard let self = self, let image = image as? UIImage else {
                                    return
                                }
                                
                                
                                let photoM =  PhotoModel(uniqueIdentifier:String(self.selectedImagesArray?.count ?? 1), image: image.pngData(), url: nil )
                                if self.selectedImagesArray == nil {
                                    self.selectedImagesArray = Array.init()
                                }
                                self.selectedImagesArray?.append(photoM)
                                
                              if  itemProviders.count == self.selectedImagesArray?.count ?? 0 {
                                    
                                    self.collectionViewPhotos.reloadData()
                                    self.setHeightOfCollectionView() }

                            }
                        }
                    }
                    

                }
        

//        iterator = itemProviders.makeIterator()
//        displayNextImage()
        //fd()
    }
    
   
}

//MARK: Images picker related method
extension BBQSideMenuFeedbackViewController  :  UIImagePickerControllerDelegate {
    
    
    //MARK:- ***************  UIImagePickerController delegate Methods ****************

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        
        // The info dictionary may contain multiple representations of the image. You want to use the original.
        
        let mediaType = info[UIImagePickerController.InfoKey.mediaType] as! CFString
        
        
        
        if let selectedImage = info[.originalImage] as? UIImage  {
            
            _ = selectedImage.resized(withPercentage: 0.50)
            
            
            let photoM =  PhotoModel(uniqueIdentifier:String(selectedImagesArray?.count ?? 1), image: selectedImage.pngData(), url: nil  )
            //  selectedImagesArray?.append(("String", selectedImage.pngData(), info [.imageURL] as! URL   ))
            if selectedImagesArray == nil {
                selectedImagesArray = Array.init()
            }
            selectedImagesArray?.append(photoM)
            collectionViewPhotos.reloadData()
           setHeightOfCollectionView()
          //  self.submitButton.isEnabled = false
  
        }
        dismiss(animated: true, completion: nil)

    }
    func showAtert(){
        
        let alert = UIAlertController(title: "Choose Attachement", message: nil, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default){
            UIAlertAction in
            self.openCamera()
        }
        let galleryAction = UIAlertAction(title: "Gallery", style: .default){
            UIAlertAction in
            self.showPicker()
        }
       
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel){
            UIAlertAction in
            
            alert.dismiss(animated: true, completion: nil)
            
        }
        
        // Add the actions
        alert.addAction(cameraAction)
        alert.addAction(galleryAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
        // b
        
    
       func openCamera(){
           
        
        guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
            //Show error to user?
            return
        }

        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.mediaTypes = [kUTTypeMovie as String, kUTTypeVideo as String , kUTTypeImage as String ]
                imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true, completion: nil)
            }
            else
            {
                let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
       }
    
    
       func openGallery(){
        
       showPicker()
       }

    
    func setHeightOfCollectionView() {
        
        if selectedImagesArray == nil || selectedImagesArray?.count == 0 {
            

            collectionViewPhotos.isHidden = true
            btnUpload.isUserInteractionEnabled = false
            btnUpload.isEnabled = false
        
          
        }else{
            collectionViewPhotos.isHidden = false
            btnUpload.isUserInteractionEnabled = true
            btnUpload.isEnabled = true


        }
        
      //  Show disabled upload button when image is attached
        if btnUpload.isEnabled == true {

            btnUpload.alpha = 1
//            btnUpload.layer.borderColor  = UIColor.clear.cgColor
//            btnUpload.layer.borderWidth = 0.0
//            btnUpload.backgroundColor = selectedBackgroundColor
//            btnUpload.setTitleColor(.white, for: .normal)
        }else{

            btnUpload.alpha = 0.4
//
//            btnUpload.layer.cornerRadius = 04.0
//            btnUpload.layer.borderColor  = unSelectedBorderColor.cgColor
//            btnUpload.layer.borderWidth = 1.0
//            btnUpload.backgroundColor = .clear
//            btnUpload.setTitleColor(unSelectedTextColor, for: .normal)
        }
    }
}

extension BBQSideMenuFeedbackViewController : removeSeletedImageDelegate {
    
    
    func removeImageAtIndex(uniqueIdentifier: String , index :Int)  {
        //remove the selected index and reload the collection view cell
        
       guard let temselectedArray = selectedImagesArray  else{
            return
        }
        
        //find index of that country
        
        let indexOfSelctedItem = temselectedArray.firstIndex(where: { (photoAsset) -> Bool in
            photoAsset.uniqueIdentifier == uniqueIdentifier
        })
        
        
        guard  indexOfSelctedItem != nil else {
            return
        }
        //remove the at found index
        selectedImagesArray?.remove(at: indexOfSelctedItem!)
        
        
        defer {
            if selectedImagesArray?.count == 0 {
                
                setHeightOfCollectionView()
                UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
                    self.view.layoutIfNeeded()
                }, completion: nil)
            }
        }
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.collectionViewPhotos.deleteItems(at: [IndexPath(row: indexOfSelctedItem!, section: 0)])
            
            self.view.layoutIfNeeded()
            
        }, completion: nil)
        
        
    }
}


extension BBQSideMenuFeedbackViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return  5//reasonList?.count ?? 0

    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        guard let btn = (tableView.cellForRow(at: indexPath) as! BBQFeedbackReasonTableViewCell).imageView else {
                  return
              }
        btn.image = UIImage(named: "deSelectedBrand")
    }
        
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        guard let reasonValue = reasonList?[safe: indexPath.row] else {
//
//            return}
        
        
        guard let imgView = (tableView.cellForRow(at: indexPath) as! BBQFeedbackReasonTableViewCell).imageView else {
                  return
              }
        imgView.image = UIImage(named: "SelectedBrand")
        
//        selectedReason = incompleteReasonModelDatum()
//        selectedReason = reasonValue
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BBQFeedbackReasonTableViewCell", for: indexPath) as? BBQFeedbackReasonTableViewCell
//        guard let reasonValue = reasonList?[safe: indexPath.row] else {
//           return UITableViewCell()
//        }
        
        cell?.selectionStyle = .none
        if indexPath.row == 2 {
            
            cell?.titleLabel.text = " Packaging was not good enough Packaging was not good enough"
        }
//            if reasonValue.isSelected  == true{
//            cell?.tickImageView.image = UIImage(named: "CircleSelected")
//        }else{
//            cell?.tickImageView.image = UIImage(named: "Circle")
//        }
//        cell?.titleLabel.text = reasonValue.name
        return cell!
    }
    

    func fixImageOrientation(_ image: UIImage)->UIImage {
        UIGraphicsBeginImageContext(image.size)
        image.draw(at: .zero)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage ?? image
    }

    
    func jsonToDictionary(from text: String) -> [String: Any]? {
        guard let data = text.data(using: .utf8) else { return nil }
        let anyResult = try? JSONSerialization.jsonObject(with: data, options: [])
        //print(anyResult)
        return anyResult as? [String: Any]
    }
}


extension BBQSideMenuFeedbackViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    override func didReceiveMemoryWarning()  {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData?.count ?? 0
    }
    
    // The data to return fopr the row and component (column) that's being passed in
    //    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    //        return pickerData[row]
    //    }
    //
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        
        txtFieldReasonDropdown.text = pickerData?[row].feed_back_question
        txtFieldReasonDropdown.accessibilityLabel = String(pickerData?[row].feed_back_question_id ?? 0)
        deliveryDineInValue = pickerData?[row].feed_back_type_id ?? 1
       // txtFieldReasonDropdown.resignFirstResponder()
        
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        let label: UILabel
        
        
        if let view = view {
            label = view as! UILabel
        }
        else {
            label = UILabel(frame: CGRect(x: 0, y: 0, width: pickerView.frame.width, height: 400))
        }
        
        let title = pickerData?[row].feed_back_question ?? ""
        
        //label.font = UIFont (name: Constants.App.BBQAppFont.ThemeRegular,  size: 14)
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.hexStringToUIColor(hex: "303030"),
                          NSAttributedString.Key.font: UIFont(name: Constants.App.BBQAppFont.ThemeRegular, size: 14.0)!]
        label.attributedText = NSAttributedString(string: title, attributes: attributes)
        
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .center
        label.sizeToFit()
        return label
    }
    
}
    //MARK: The below two methods are called on click of picker done and cancel button
    extension BBQSideMenuFeedbackViewController : ToolBarDoneAndCancelDelegate {
        
        // called when textfield has to take data from picker
        func pickerTextTapped() {
            
            self.txtFieldReasonDropdown.text = ""
            self.txtFieldReasonDropdown.accessibilityLabel = ""
            
            if picker  != nil{
                self.picker.isHidden = false
            }
            self.pickerBackgroundView.isHidden = false // just show the background screen to diable complete UI for pressing other textfield
            
            self.view.layoutIfNeeded()
        }
        
        func doneButtonPressed() {
            
            self.view.layoutIfNeeded()
            if txtFieldReasonDropdown.text == "" {
                
                txtFieldReasonDropdown.text = pickerData?[0].feed_back_question
                txtFieldReasonDropdown.accessibilityLabel = String(pickerData?[0].feed_back_question_id ?? 0)
                deliveryDineInValue = pickerData?[0].feed_back_type_id ?? 1
            }
            txtFieldReasonDropdown.resignFirstResponder()
            self.pickerBackgroundView.isHidden = true
        }
        
        func cancelButtonPressed() {
            
            activeTextField.resignFirstResponder()
            self.txtFieldReasonDropdown.text = ""
            self.txtFieldReasonDropdown.accessibilityLabel = ""
            self.pickerBackgroundView.isHidden = true
        }
    }

