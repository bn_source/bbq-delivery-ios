//
 //  Created by Arpana on 02/11/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified BBQSideMenuFeedBackThanksViewController.swift
 //

import UIKit

class BBQSideMenuFeedBackThanksViewController: BBQBaseViewController {

    @IBOutlet weak var thanksMsgLabel: UILabel!
    @IBOutlet weak var smilyImageView: UIImageView!
    @IBOutlet weak var btnMyOrder: UIButton!
    @IBOutlet weak var lblBrandName: UILabel!
    var thanksMsg : String = "Thanks"
    var imagNam : String = "SmilyStar"
    var overallRating: Int = 1
    var timer = Timer()
    var brandlogo = ""
    var deliveryThemeColor = UIColor.theme
    var deliveryThemeTextColor = UIColor.white
    override func viewDidLoad() {
        super.viewDidLoad()
        setData()
        

     
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    func setData()  {
        btnMyOrder.applyMultiBrandTheme(backgroundColor: deliveryThemeColor, textColor: deliveryThemeTextColor)
        
        
    }
    @IBAction func backBtnPressed(_ sender: Any) {
        navigateToHomePage()
    }
    
}
