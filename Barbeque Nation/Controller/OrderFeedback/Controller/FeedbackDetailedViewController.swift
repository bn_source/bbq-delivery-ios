//
 //  Created by Arpana on 13/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified FeedbackDetailedViewController.swift
 //

import UIKit
import ShimmerSwift
@objc enum CategorySelected :Int, RawRepresentable {
    
    case Taste = 9
    case Package
    case Delivery
    case tem
    
    public var SectionNameString: String {
        switch self {
        case .Taste        : return "Taste"
        case .Package       : return "Packaging"
        case .Delivery      : return "Delivery"
        case .tem      : return "tem"

        }
        
    }
}

class FeedbackDetailedViewController: BaseViewController  {

    @IBOutlet weak var tasteTableView: UITableView!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var tasteButton : UIButton!
    @IBOutlet weak var pacakgingButton : UIButton!
    @IBOutlet weak var deliveryButton : UIButton!
    @IBOutlet weak var submitButton : UIButton!
    @IBOutlet weak var sugestionTextView: UITextView!
    @IBOutlet weak var tableHeightConstraint: NSLayoutConstraint!
 
    @IBOutlet weak var deliveryButtonView: UIView!
    @IBOutlet weak var branchNameLabel: UILabel!
    @IBOutlet weak var imgBrandLogo: UIImageView!
    @IBOutlet weak var lblOrderDate: UILabel!
    var feedbackModelResponse : FeedbackModel?

    @IBOutlet weak var shimmerContainer: UIView!
    
    var shimmerView : ShimmeringView?
    @IBOutlet weak var shimmerViewDetails: UIView!
    
    var orderID: String?
    var transactionType :Int?
    
    @IBOutlet weak var itemWithPriceLabel: UILabel!
    
    @IBOutlet weak var shimmerViewDeliveryButton: UIView!
    @IBOutlet weak var questionLabel: UILabel!
    @IBAction func btnMyOrder(_ sender: Any) {
    }
    var userRatingValue = 1.0

    var sectionTitle  :[CategorySelected ] = []

    var minimumInteritemSpacingForSection: CGFloat = 10.0
    var branchName = "BBQN"
    var brandlogo = ""
    var orderDate = ""
    var deliveryThemeColor = UIColor.theme
    var deliveryThemeTextColor = UIColor.white
    
    override func viewDidLoad() {
        AnalyticsHelper.shared.triggerEvent(type: .FB01)

        super.viewDidLoad()
        styleUI()
        registerTableViewCell()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tasteTableView.addObserver(self, forKeyPath: "contentSize" , options: .new, context: nil)
        setOrderIDLabelData()
       // showHidepackageDeliveryQuestionBasisOfRating()
        setFeedBackData()
    }
    
    func styleUI() {
 
        shimmerView = ShimmeringView(frame: self.shimmerViewDetails.bounds)
        shimmerView?.center.x = self.shimmerViewDetails.center.x // for horizontal


        if transactionType == 2 {
            
            shimmerViewDeliveryButton.isHidden = true
            deliveryButtonView.isHidden = true

            //in case of take away , do not show deliverybutton for shimmering too
            //It causes flick while moving from shimmer to main view
            
        }
        
        showShimmering()

        // Reset float rating view's background color
        ratingView.backgroundColor = UIColor.clear

        /** Note: With the exception of contentMode, type and delegate,
         all properties can be set directly in Interface Builder **/
        ratingView.contentMode = UIView.ContentMode.scaleAspectFit
        ratingView.type = .wholeRatings
        ratingView.isUserInteractionEnabled = true
        ratingView.delegate = self
        ratingView.rating = userRatingValue
        self.navigationController?.view.backgroundColor = .white
        self.navigationController?.navigationBar.tintColor = UIColor(named: "TextColor")
        self.navigationController?.navigationBar.topItem?.title = ""
        
        submitButton.setCornerRadius(5.0)
//        let borderColor : UIColor = UIColor(red: 0.85, green: 0.85, blue: 0.85, alpha: 1.0)
//        sugestionTextView.layer.borderWidth = 0.5
//        sugestionTextView.layer.borderColor = borderColor.cgColor
        sugestionTextView.textContainerInset = UIEdgeInsets(top: 4, left: 6, bottom: 4, right: 4)

        sugestionTextView.layer.cornerRadius = 5.0
        submitButton.isEnabled = false
        //depending of rating value show, normal star image or smily star image
         if userRatingValue > 3 {
            ratingView.fullImage = UIImage(named: "HappySmile") // happy face
        }else{
            ratingView.fullImage = UIImage(named: "SmilyStar") //sad face

        }
        
        //if user has rated 5, taste button image will be changed
        Int(userRatingValue) == 5 ? tasteButton.setImage(UIImage.init(named: "smileUnSelectedTaste"), for: .normal) : tasteButton.setImage(UIImage.init(named: "UnselectedTaste"), for: .normal)

        submitButton.applyMultiBrandTheme(backgroundColor: deliveryThemeColor, textColor: deliveryThemeTextColor)
        imgBrandLogo.setImagePNG(url: brandlogo)
        lblOrderDate.text = orderDate
    }

private func showShimmering() {
    self.shimmerView?.isHidden = false
    shimmerContainer.isHidden = false
    shimmerViewDetails.isHidden = false
    if let shimmer = self.shimmerView, !shimmerContainer.subviews.contains(shimmer){
        self.shimmerContainer.addSubview(shimmer)
        shimmer.contentView = shimmerViewDetails
        shimmer.shimmerAnimationOpacity = 0.2
        shimmer.shimmerSpeed = 500.00
        shimmer.isShimmering = true
    }

}

    private func stopShimmering() {
    
        if let shimmer = self.shimmerView {
            shimmer.isShimmering = false
            shimmer.isHidden = true
        }
        
        self.shimmerView?.isHidden = true
        shimmerContainer.isHidden = true
        shimmerViewDetails.isHidden = true

    }
    
    func registerTableViewCell(){
        
        tasteTableView?.estimatedRowHeight = 100
        tasteTableView?.rowHeight = UITableView.automaticDimension
        tasteTableView?.sectionHeaderHeight = 70
        tasteTableView?.separatorStyle = .none
        tasteTableView.register(UINib(nibName: "BBQTasteTableViewCell", bundle: nil), forCellReuseIdentifier: "BBQTasteTableViewCell")
        tasteTableView.register(UINib(nibName: "BBQPackageTableViewCell", bundle: nil), forCellReuseIdentifier: "BBQPackageTableViewCell")

        
    }
    
    private func setOrderIDLabelData(){
        

        if orderID != "" {
            //Update header ui
            let orderNumber = NSMutableAttributedString(string: "ORDER")
            
            
            let orderNumberID =  NSAttributedString(string: " #" + (String(orderID ?? "0")), attributes: [NSAttributedString.Key.font: UIFont.appThemeMediumWith(size: 14.0), NSAttributedString.Key.foregroundColor: deliveryThemeColor] )
            
            orderNumber.append(orderNumberID)
            branchNameLabel.attributedText = orderNumber
            
        }
                
    }
    
    func setFeedBackData(){
        
        guard let orderIdVal = orderID else {
            return
        }
        
        AnalyticsHelper.shared.triggerEvent(type: .FB02)

        BBQServiceManager.getInstance().getOrderFeedBackDetails(params: ["order_id": Int(orderIdVal ) ?? 0,
                                                                         "transaction_type": transactionType ?? 1]) { [self]
            (error, response) in


              stopShimmering()
            
            if let response = response {
                if response.message_type == "success" {
                    
                    AnalyticsHelper.shared.triggerEvent(type: .FB02A)
                    
                    feedbackModelResponse = response
                  // feedbackModelResponse?.data?.delivery?.question_set?[0].questions?[1].question_name = "tkjhfjkdhgjherdjkghjkersndfjgrdfjghjkldfhgkhkjhgjklfhdkljghkjfdhgjklh dfkljghs"
                    //total rating wict need to be appaear
                    fillRatingData()
                } else {
                    AnalyticsHelper.shared.triggerEvent(type: .FB02B)
                }
            } else {
              print("cjhjbj")
            }
            view.layoutIfNeeded()
        }
  
    }
    
    private func fillRatingData() {
        guard let retrivedData = feedbackModelResponse else {
            return
        }
        feedbackModelResponse = retrivedData
      // feedbackModelResponse?.data?.delivery?.question_set?[0].questions?[1].question_name = "tkjhfjkdhgjherdjkghjkersndfjgrdfjghjkldfhgkhkjhgjklfhdkljghkjfdhgjklh dfkljghs"
        //total rating wict need to be appaear
        self.ratingView.maxRating = retrivedData.data?.overall_rating?.count ?? 5
        //rating which user has given from previous screen
        self.ratingView.rating = self.userRatingValue
        let indexToaGetData = Int(self.userRatingValue - 1)
        if indexToaGetData != -1 {
            guard let questionText = retrivedData.data?.overall_rating?[indexToaGetData].text else {
                return
            }
            self.questionLabel.text = questionText
        }
        submitButton.isEnabled = true
        tasteTableView.reloadData()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
    }
    

    override func observeValue(forKeyPath keyPath: String?, of object: Any?,
                                 change: [NSKeyValueChangeKey : Any]?,
                                 context: UnsafeMutableRawPointer?) {
        if (object as? UITableView) == tasteTableView && (keyPath == "contentSize") {

            tasteTableView.layer.removeAllAnimations()
                tableHeightConstraint.constant = tasteTableView.contentSize.height
                UIView.animate(withDuration: 0.5) {
                    self.updateViewConstraints()
                }
        }
        
    }
    override func updateViewConstraints() {
        tableHeightConstraint.constant = tasteTableView.contentSize.height
        super.updateViewConstraints()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
         super.viewWillDisappear(animated)
         tasteTableView.removeObserver(self,
                                  forKeyPath: "contentSize")
     }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sumbitBtnPresses(_ sender : UIButton){
        
        AnalyticsHelper.shared.triggerEvent(type: .FB03)

        // suggestion text
        feedbackModelResponse?.data?.suggestion = sugestionTextView.text!
        
        //add rating_ id on basis of total rating count
        feedbackModelResponse?.data?.overall_rating_id = String(Int(ratingView.rating))
        
        let JSONString = feedbackModelResponse!.toJSONString(prettyPrint: true)
        
        guard let jsonToSend = jsonToDictionary(from: JSONString!) else { return  }
        
        UIUtils.showLoader()
        AnalyticsHelper.shared.triggerEvent(type: .FB07)

        BBQServiceManager.getInstance().getSubmitFeedBackResponse(params: jsonToSend) { (error, result) in
            UIUtils.hideLoader()

            if let response = result {
                if response.message_type == "failed" {
                    UIUtils.showToast(message: response.message ?? "failed")
                   AnalyticsHelper.shared.triggerEvent(type: .FB07B)
                } else {
                    AnalyticsHelper.shared.triggerEvent(type: .FB07A)
                    let  controller = UIStoryboard.loadFeedBackThanksViewController()
                    controller.overallRating = Int(self.userRatingValue)
                    controller.thanksMsg = response.message ?? "Thanks"
                    controller.OrderIDForRatedFood = self.orderID ?? ""
                    controller.brandlogo = self.brandlogo
                    controller.branchName = self.branchName
                    controller.deliveryThemeTextColor = self.deliveryThemeTextColor
                    controller.deliveryThemeColor = self.deliveryThemeColor
                    controller.orderDate = self.orderDate
                    self.navigationController?.pushViewController(controller, animated: false)
                }
            } else {
                UIUtils.showToast(message: error?.message ?? UNEXPECTED_ERROR)
            }
        }
        
    }
    
    
    func jsonToDictionary(from text: String) -> [String: Any]? {
        guard let data = text.data(using: .utf8) else { return nil }
        let anyResult = try? JSONSerialization.jsonObject(with: data, options: [])
        //print(anyResult)
        return anyResult as? [String: Any]
    }
}


extension FeedbackDetailedViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionTitle.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        let sectionType = sectionTitle[section]
        
        switch sectionType {
        case CategorySelected.Taste:
            
            guard  (feedbackModelResponse?.data?.taste?.question_set) != nil else {
                return 0
            }
            guard  feedbackModelResponse?.data?.taste?.question_set?.count != 0 else{
                return 0
            }
            return 1
            
        case CategorySelected.Package:
            guard  (feedbackModelResponse?.data?.packaging?.question_set) != nil else {
                return 0
            }
            return 1
        case CategorySelected.Delivery:
            guard  (feedbackModelResponse?.data?.delivery?.question_set) != nil else {
                return 0
            }
            return 1

        default:
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let sectionType = sectionTitle[indexPath.section]

        
        switch sectionType
        {
        case CategorySelected.Taste:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "BBQPackageTableViewCell" , for: indexPath)  as? BBQPackageTableViewCell {
                cell.collectionViewController.cellSelectionDelegate =  self
                
                guard  let temTasteArray = feedbackModelResponse?.data?.taste?.question_set else {
                    
                    return UITableViewCell()
                }
 
                cell.setData(dataArray: temTasteArray, tit: sectionType.SectionNameString )
                return cell
            }

            return UITableViewCell()
         
        case CategorySelected.Package:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "BBQPackageTableViewCell" , for: indexPath)  as? BBQPackageTableViewCell {
                cell.collectionViewController.cellSelectionDelegate =  self
                
                guard  let temTasteArray = feedbackModelResponse?.data?.packaging?.question_set else {
                    
                    return UITableViewCell()
                }
 
                cell.setData(dataArray: temTasteArray, tit: sectionType.SectionNameString )
                return cell
            }

        case CategorySelected.Delivery:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "BBQPackageTableViewCell" , for: indexPath)  as? BBQPackageTableViewCell {
            cell.collectionViewController.cellSelectionDelegate =  self
            guard  let temDeliveryArray = feedbackModelResponse?.data?.delivery?.question_set else {
                
                return UITableViewCell()
            }
            cell.setData(dataArray: temDeliveryArray, tit: sectionType.SectionNameString )
            return cell
            }
        default:
            return UITableViewCell()
            
        }
        return UITableViewCell()
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    

     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30;
    }
    
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let view = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:30))
        let label = UILabel(frame: CGRect(x:10, y:10, width:tableView.frame.size.width, height:20))
       // label.text = sectionTitle[section]
         label.font = UIFont.appThemeBoldWith(size: 16)
        view.addSubview(label);
        
        let sectionType = sectionTitle[section]

        switch sectionType {
        case CategorySelected.Taste:
            
            label.text = CategorySelected.Taste.SectionNameString
            
        case CategorySelected.Package:
            label.text = CategorySelected.Package.SectionNameString
            
        case CategorySelected.Delivery:
            label.text = CategorySelected.Delivery.SectionNameString
            
            
        case CategorySelected.tem:
            label.text = CategorySelected.tem.SectionNameString
        default:
            label.text = ""

        }
        
        return view

    }
    
}

extension FeedbackDetailedViewController {
    
    //If the respective button is selected
    //assign its tag, and show the table rows respectively
    //Ex, if delivery is not selected, show only two sections one for taste  and other for package
    // tag change is for selecting deselecting
    @IBAction func tasteButtonPressed(_ sender : UIButton){
        
        
        if  feedbackModelResponse?.data?.taste?.question_set?.count == 0{
            
            UIUtils.showToast(message: kNoTasteData)
            return
        }
        
        tasteButton.tag = tasteButton.tag == 0 ?CategorySelected.Taste.rawValue : 0
        
        if tasteButton.tag == 0 {
        
            //change the taste button image to deselected on, if user has rated 5 , then show simed image , else sad image
            Int(userRatingValue) == 5 ? tasteButton.setImage(UIImage.init(named: "smileUnSelectedTaste"), for: .normal) : tasteButton.setImage(UIImage.init(named: "UnselectedTaste"), for: .normal)
        //Check if section array includes tasete , remove
            
            
            let section = sectionTitle.firstIndex(of: CategorySelected.Taste)
            if section != nil {
                sectionTitle.remove(at: section!)
                tasteTableView.deleteSections(NSIndexSet(index: section ?? 0) as IndexSet, with: .fade)
            }
//        tasteButton.tag = CategorySelected.Taste.rawValue
        }else{
            //change the taste button image to selected on
            AnalyticsHelper.shared.triggerEvent(type: .FB04)

            Int(userRatingValue) == 5 ? tasteButton.setImage(UIImage.init(named: "selectedtasteSmile"), for: .normal) : tasteButton.setImage(UIImage.init(named: "Taste"), for: .normal)
           // tasteButton.setImage(UIImage.init(named: "Taste"), for: .normal)

            // check if taste is not added in section array add it ,
            if sectionTitle.contains(CategorySelected.Taste) == false{
                sectionTitle.insert((CategorySelected.Taste), at: 0)
                
                tasteTableView.insertSections(NSIndexSet(index: 0) as IndexSet, with: .fade)

                }
            }
      //  tasteTableView.reloadData()
        }
    
    @IBAction func packageButtonPressed(_ sender : UIButton) {
        
        if  feedbackModelResponse?.data?.packaging?.question_set?.count == 0{
            
            UIUtils.showToast(message: kNoPackageData)
            return
        }
        pacakgingButton.tag = pacakgingButton.tag == 0 ? CategorySelected.Package.rawValue : 0
        
    
        if pacakgingButton.tag == 0 {
            
            //change the taste button image to deselected on
            pacakgingButton.setImage(UIImage.init(named: "UnselectedPacakge"), for: .normal)
            //Check if section array includes tasete , remove
            
            
            if userRatingValue == 5 {
                
                //no need to display question
                return
            }
            
            if sectionTitle.contains(CategorySelected.Package){
            
                
                let section = sectionTitle.firstIndex(of: CategorySelected.Package)
                sectionTitle.remove(at: section!)
                tasteTableView.deleteSections(NSIndexSet(index: section ?? 0) as IndexSet, with: .fade)
                            

                
            }
            //        tasteButton.tag = CategorySelected.Taste.rawValue
        }else{
            AnalyticsHelper.shared.triggerEvent(type: .FB05)

            //change the taste button image to deselected on
            pacakgingButton.setImage(UIImage.init(named: "Package"), for: .normal)
            
            if userRatingValue == 5 {
                
                //no need to display question
                return
            }
            // check if pakage is not added in section array add it ,
            if sectionTitle.contains(CategorySelected.Package) == false{
                //Add package array , after the taste (if exist)
                //So check if taste exist in section, if yes add at index 1
                if sectionTitle.contains(CategorySelected.Taste){
                    sectionTitle.insert((CategorySelected.Package), at: 1)

                    tasteTableView.insertSections(NSIndexSet(index: 1) as IndexSet, with: .fade)
                }
                else
                {
                    sectionTitle.insert((CategorySelected.Package), at: 0)
                    tasteTableView.insertSections(NSIndexSet(index: 0) as IndexSet, with: .fade)


                }
            }
        }
       // tasteTableView.reloadData()
            self.view.layoutIfNeeded()
        
    }
    
    @IBAction func deliveryButtonPressed(_ sender : UIButton){
        
        if  feedbackModelResponse?.data?.delivery?.question_set?.count == 0{
            
            UIUtils.showToast(message: kNoDeliveryData)
            return
        }
       
        deliveryButton.tag = deliveryButton.tag == 0 ? CategorySelected.Delivery.rawValue : 0


        
        if deliveryButton.tag == 0 {
            
            //change the taste button image to deselected on
            deliveryButton.setImage(UIImage.init(named: "UnSelectedDelivery"), for: .normal)
        //Check if section array includes tasete , remove
            
            if userRatingValue == 5 {
                
                //no need to display question
                return
            }
            
            if sectionTitle.contains(CategorySelected.Delivery){
//                sectionTitle.removeAll { (title) -> Bool in
//                    title == CategorySelected.Delivery
//                }
                let row = sectionTitle.firstIndex(of: CategorySelected.Delivery)
                sectionTitle.remove(at: row!)
                tasteTableView.deleteSections(NSIndexSet(index: row ?? 0) as IndexSet, with: .fade)
                
            }
            
        }else{
            AnalyticsHelper.shared.triggerEvent(type: .FB06)

            //change the taste button image to deselected on
            deliveryButton.setImage(UIImage.init(named: "SelectedDelivery"), for: .normal)
            
            
            if userRatingValue == 5 {
                
                //no need to display question
                return
            }
            
            //            // check if pakage is not added in section array add it ,
            if sectionTitle.contains(CategorySelected.Delivery) == false{
                //Add delivery array , at the last (if exist)
                // sectionTitle.append(CategorySelected.Delivery)
                
                if sectionTitle.contains(CategorySelected.Taste) == true {
                    if sectionTitle.contains(CategorySelected.Package) == false{
                        
                        sectionTitle.insert((CategorySelected.Delivery), at: 1 )
                        tasteTableView.insertSections(NSIndexSet(index: 1) as IndexSet, with: .fade)
                    }else{
                        //inser at index 2
                        
                        sectionTitle.insert((CategorySelected.Delivery), at: 2 )
                        tasteTableView.insertSections(NSIndexSet(index: 2) as IndexSet, with: .fade)
                    }
                    
                }else{
                    
                    if sectionTitle.contains(CategorySelected.Package) == false{
                        
                        sectionTitle.insert((CategorySelected.Delivery), at: 0 )
                        tasteTableView.insertSections(NSIndexSet(index: 0) as IndexSet, with: .fade)
                    }else{
                        //inser at index 1
                        sectionTitle.insert((CategorySelected.Delivery), at: 1 )
                        tasteTableView.insertSections(NSIndexSet(index: 1) as IndexSet, with: .fade)
                    }
                    
                }
            }
        }
            self.view.layoutIfNeeded()

    }
    
    func showHidepackageDeliveryQuestionBasisOfRating(){
        
        // if user has rated 5 then delivery/packaging questiopn not requirednot required
        if userRatingValue == 5 {
            
            deliveryButton.isEnabled = false
            pacakgingButton.isEnabled = false
            tasteButtonPressed( tasteButton)

        }

    }
}
extension FeedbackDetailedViewController : NotifyTableCellOnSelectionOfCollectionCell{
    
    func cellSelectedDelegate(indexPath: IndexPath , isSelected: Bool , typeOFFeedback: String , objectTobeSet: Question_set) {
        
        if  typeOFFeedback == CategorySelected.Delivery.SectionNameString {
            
            // just replace the feedback response array at provide index path
            //check if  model present at aprticular path
            guard (feedbackModelResponse?.data?.delivery?.question_set?[indexPath.section]) != nil else {
                
                return
            }

            
            self.feedbackModelResponse?.data?.delivery?.question_set?.remove(at: indexPath.section)
            
            self.feedbackModelResponse?.data?.delivery?.question_set?.insert(objectTobeSet, at: indexPath.section)
            
            
        } else if typeOFFeedback  == CategorySelected.Package.SectionNameString {
    
            guard (feedbackModelResponse?.data?.packaging?.question_set?[indexPath.section]) != nil else {
                
                return
            }
            self.feedbackModelResponse?.data?.packaging?.question_set?.remove(at: indexPath.section)
            
            self.feedbackModelResponse?.data?.packaging?.question_set?.insert(objectTobeSet, at: indexPath.section)
            
   
    } else if typeOFFeedback  == CategorySelected.Taste.SectionNameString {
        
        guard (feedbackModelResponse?.data?.taste?.question_set?[indexPath.section]) != nil else {
            
            return
        }
        self.feedbackModelResponse?.data?.taste?.question_set?.remove(at: indexPath.section)
        
        self.feedbackModelResponse?.data?.taste?.question_set?.insert(objectTobeSet, at: indexPath.section)
        

}
          
    }
}

extension FeedbackDetailedViewController : NotifyelectdStarToTableViewDelegate {
    
    func notifyStarCount(indexPath: IndexPath , starCount: Int ){
        
        
        AnalyticsHelper.shared.triggerEvent(type: .FB09)

        //update the question model with provided rating
        
        //get the same object from data array , add id to the answer key
        guard var  questionRetreived =  self.feedbackModelResponse?.data?.taste?.question_set?[indexPath.section].questions?[indexPath.row] else{
            
            return
        }
        
       // if questionRetreived?.question_id ==
        questionRetreived.answer = String(starCount)
        
        print(questionRetreived)
        
        self.feedbackModelResponse?.data?.taste?.question_set?[indexPath.section].questions?.remove(at: indexPath.row)
        
        self.feedbackModelResponse?.data?.taste?.question_set?[indexPath.section].questions?.insert(questionRetreived, at: indexPath.row)
        
    }
}

extension FeedbackDetailedViewController : UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if (sugestionTextView.text == "Enter your feedback")
        {
            sugestionTextView.text = nil
            sugestionTextView.textColor = UIColor.hexStringToUIColor(hex: "666666")
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if sugestionTextView.text.isEmpty
        {
            sugestionTextView.text = "Enter your feedback"
            sugestionTextView.textColor = UIColor.lightGray
        }
        textView.resignFirstResponder()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let currentText = textView.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: text)
        return updatedText.count <= 100
    }
    
}
extension FeedbackDetailedViewController: FloatRatingViewDelegate {

    // MARK: FloatRatingViewDelegate
    
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double) {
        print( String(format: "%.2f", rating))
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
        self.userRatingValue = rating
        if userRatingValue > 3 {
           ratingView.fullImage = UIImage(named: "HappySmile") // happy face
       }else{
           ratingView.fullImage = UIImage(named: "SmilyStar") //sad face

       }
        sectionTitle = [CategorySelected]()
        resetButtons()
        setFeedBackData()
    }
    
    private func resetButtons(){
        Int(userRatingValue) == 5 ? tasteButton.setImage(UIImage.init(named: "smileUnSelectedTaste"), for: .normal) : tasteButton.setImage(UIImage.init(named: "UnselectedTaste"), for: .normal)
        pacakgingButton.setImage(UIImage.init(named: "UnselectedPacakge"), for: .normal)
        deliveryButton.setImage(UIImage.init(named: "UnSelectedDelivery"), for: .normal)
        tasteButton.tag = 0
        pacakgingButton.tag = 0
        deliveryButton.tag = 0
    }
}
