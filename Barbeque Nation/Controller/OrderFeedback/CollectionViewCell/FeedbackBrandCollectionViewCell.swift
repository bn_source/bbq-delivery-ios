//
 //  Created by Arpana on 10/10/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified FeedbackBrandCollectionViewCell.swift
 //

import UIKit
    
    class FeedbackBrandCollectionViewCell: UICollectionViewCell {
        @IBOutlet weak var viewForContainer: UIView!
        @IBOutlet weak var imgLogo: UIImageView!
        @IBOutlet weak var imageSelectDeselect : UIImageView!
        
        func setCellData(brandInfo: BrandInfo) {
//            viewForContainer.roundCorners(cornerRadius: 5, borderColor: .clear, borderWidth: 0)
//            viewForContainer.setShadow()
           // imgLogo.setImage(imageUrl:  brandInfo.logo, placeholderImage: nil)
            imgLogo.setImagePNG(url: brandInfo.logo)
            imgLogo.setCornerRadius(0)
        }
        
        
        func toggleSelected ()
        {
            if (isSelected){
                imageSelectDeselect.image = UIImage(named: "SelectedBrand")
            }else{
                
                imageSelectDeselect.image = UIImage(named: "deSelectedBrand")
                
            }
        }
        
    }
