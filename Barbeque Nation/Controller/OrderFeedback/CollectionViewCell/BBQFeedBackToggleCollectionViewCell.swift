//
 //  Created by Arpana Rani on 17/03/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQFeedBackToggleCollectionViewCell.swift
 //

import UIKit


//MARK: Delegate to be called on click of cell selection
@objc protocol NotifyWrongOrMissedValueToCollectionView {
   
    @objc optional func optionSelected(indexPath: IndexPath , questionID: String? , buttonClicked: UIButton )

}
class BBQFeedBackToggleCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var wrongLButton: UIButton!
    @IBOutlet weak var missingButton: UIButton!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var vegNonVegImageView: UIImageView!
    var indexPathForTheItem :IndexPath?
    weak var optionDelegate : NotifyWrongOrMissedValueToCollectionView?
    
    let textColor = UIColor.hexStringToUIColor(hex:"2B3849")
    let selectedCellHighlightTextColor = UIColor(red: 240/255, green: 75/255, blue: 36/255, alpha: 1)
    let selectedCellHighlightColor = UIColor(red: 254/255, green: 241/255, blue: 228/255, alpha: 1)

    override func awakeFromNib() {
        super.awakeFromNib()
        wrongLButton.titleLabel?.font = UIFont.appThemeRegularWith(size: 12)
        missingButton.titleLabel?.font = UIFont.appThemeRegularWith(size: 12)
        missingButton.setTitleColor(textColor, for: .normal)
        wrongLButton.setTitleColor(textColor, for: .normal)
        questionLabel.font = UIFont.appThemeRegularWith(size: 12)
        wrongLButton.roundCorners(cornerRadius: 10.0, borderColor: textColor, borderWidth: 1.0)//16.0
        missingButton.roundCorners(cornerRadius: 10.0, borderColor: textColor, borderWidth: 1.0)//16.0
        
        // Initialization code
    }

    @IBAction func wrongChoosedBtnPressed(_ sender : UIButton){
    
        sender.isSelected = sender.isSelected == true ? false : true
        if sender.isSelected == true{
            missingButton.roundCorners(cornerRadius: 10.0, borderColor: textColor, borderWidth: 1.0)//16.0
            missingButton.backgroundColor = .clear
            missingButton.setTitleColor(textColor, for: .normal)
            wrongLButton.isSelected = false
            wrongLButton.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 1.0)
            wrongLButton.backgroundColor = selectedCellHighlightColor
            wrongLButton.setTitleColor(selectedCellHighlightTextColor, for: .normal)
            
        }else{
            wrongLButton.roundCorners(cornerRadius: 10.0, borderColor: textColor, borderWidth: 1.0)//16.0
            wrongLButton.backgroundColor = .clear
            wrongLButton.setTitleColor(textColor, for: .normal)


        }
        
        guard indexPathForTheItem != nil  else {
            return
        }
        optionDelegate?.optionSelected?(indexPath: indexPathForTheItem! , questionID: nil, buttonClicked: sender)
    }
    
    @IBAction func missingChoosedBtnPressed(_ sender : UIButton){
        
        sender.isSelected = sender.isSelected == true ? false : true
        if sender.isSelected == true{
            wrongLButton.roundCorners(cornerRadius: 10.0, borderColor: textColor, borderWidth: 1.0)//16.0
            wrongLButton.backgroundColor = .clear
            wrongLButton.setTitleColor(textColor, for: .normal)
            missingButton.isSelected = false
            missingButton.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 1.0)
            missingButton.backgroundColor = selectedCellHighlightColor
            missingButton.setTitleColor(selectedCellHighlightTextColor, for: .normal)
        }else{
            missingButton.roundCorners(cornerRadius: 10.0, borderColor: textColor, borderWidth: 1.0)//16.0
            missingButton.backgroundColor = .clear
            missingButton.setTitleColor(textColor, for: .normal)

        }
        guard indexPathForTheItem != nil  else {
            return
        }
        optionDelegate?.optionSelected?(indexPath: indexPathForTheItem! , questionID: nil, buttonClicked: sender)
    }
}
