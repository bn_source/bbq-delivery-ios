//
 //  Created by Arpana on 10/10/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified FeedbackSmileyCollectionViewCell.swift
 //

import UIKit

class FeedbackSmileyCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgViewSmileyIcon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewForSelection: UIView!
    
    override  func awakeFromNib() {
        super.awakeFromNib()
        self.setUI()
        
    }
    
    private func setUI(){
        
        viewForSelection.layer.masksToBounds = false
        viewForSelection.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        viewForSelection.layer.shadowOffset = CGSize(width: 0, height: 3)
        viewForSelection.layer.shadowRadius = 2
        viewForSelection.layer.shadowOpacity = 0.5
    }
    func toggleSelected ()
    {
        if (isSelected){
            
            viewForSelection.roundCorners(cornerRadius: 8.0, borderColor: UIColor.white, borderWidth: 1.0)
            lblTitle.font = UIFont(name: Constants.App.BBQAppFont.ThemeSemiBold, size: 12)
            lblTitle.textColor = UIColor.white
            viewForSelection.backgroundColor =  .deliveryThemeColor
            
        }else {
            
            viewForSelection.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
            lblTitle.font = UIFont(name: Constants.App.BBQAppFont.ThemeRegular, size: 12)
            lblTitle.textColor =  UIColor.hexStringToUIColor(hex: "BABABA")
            viewForSelection.backgroundColor = UIColor.clear
            
            
            backgroundColor = UIColor.white
        }
    }
    
    
}


