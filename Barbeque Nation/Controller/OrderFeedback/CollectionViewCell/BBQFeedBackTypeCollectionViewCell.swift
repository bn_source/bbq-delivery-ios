//
 //  Created by Arpana Rani on 15/03/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQFeedBackTypeCollectionViewCell.swift
 //

import UIKit

class BBQFeedBackTypeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var buttonForType: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        titleLabel.font = UIFont.appThemeRegularWith(size: 12)

    }
}
