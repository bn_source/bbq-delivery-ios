//
 //  Created by Arpana Rani on 17/03/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQStarQuestionTypeXCellCollectionViewCell.swift
 //

import UIKit

//MARK: Delegate to be called on click of cell selection
@objc protocol NotifyelectdStarToCollectionViewDelegate {
   
    @objc optional func notifyStarCount(indexPath: IndexPath , starCount: Int , questionID: String)

}

class BBQStarQuestionTypeXCellCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var driverRateView: FloatRatingView!
    @IBOutlet weak var foodTypeImageView:  UIImageView!
    @IBOutlet weak var rateQuestion:  UILabel!
    @IBOutlet  var questionLabelLeadingConstrint: NSLayoutConstraint!
    var currentIndex: IndexPath?
    weak var statrDelegate: NotifyelectdStarToCollectionViewDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        driverRateView.delegate = self
        rateQuestion.font = UIFont.appThemeSemiBoldWith(size: 14)

        // Initialization code
    }

}
extension BBQStarQuestionTypeXCellCollectionViewCell : FloatRatingViewDelegate {
    
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double){
        
        //notify coltroller about star selected and update model with rating id and questionid

        guard  currentIndex != nil else {
            return

        }
        statrDelegate?.notifyStarCount?(indexPath: currentIndex!, starCount: Int(rating), questionID: rateQuestion.accessibilityLabel ?? "0")
   
        
    }

}
