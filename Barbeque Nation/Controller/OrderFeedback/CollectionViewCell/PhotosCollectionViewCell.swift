//
 //  Created by Arpana on 11/10/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified PhotosCollectionViewCell.swift
 //


    import UIKit
    import Alamofire
    import AVFoundation
    import AVKit



//MARK: Delegate to remove selected product on click of cross button on collection view cell
@objc protocol removeSeletedImageDelegate  {
   
    @objc optional func removeImageAtIndex( uniqueIdentifier :String, index:Int)

}

class PhotosCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var lblAttachementType: UILabel!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    weak var removeDelegate: removeSeletedImageDelegate?
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 12
        
        
    }
    
   
    
    @IBAction func removeProductBtnPressed(_ sender : Any) {
        
        removeDelegate?.removeImageAtIndex!(uniqueIdentifier: (sender as? UIButton)!.accessibilityLabel ?? "", index: removeButton.tag)
    }
    
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        
    }
    
    // MARK: - Lifecycle Methods
    
}
