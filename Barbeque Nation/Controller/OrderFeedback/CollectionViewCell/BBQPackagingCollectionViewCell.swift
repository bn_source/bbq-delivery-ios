//
 //  Created by Arpana on 15/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQPackagingCollectionViewCell.swift
 //

import UIKit

class BBQPackagingCollectionViewCell: BBQCustomCollectionViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var viewForContainer: UIView!
    var isCellSelected = false
    let textColor = UIColor.hexStringToUIColor(hex:"1C2C40")
        

    var cornerRadiusValue: CGFloat = 0.0
    override func awakeFromNib() {
        super.awakeFromNib()
        nameLabel.textColor = textColor
        viewForContainer.layer.borderWidth = 1.0
        viewForContainer.layer.borderColor = textColor.cgColor
        // Initialization code
    }

    @IBAction  func tagButtonPressed(_ sender : UIButton){
        
    }
    
    
    func setCellData(isSelected: Bool, questionName: String) {
        if !isSelected {
            viewForContainer.roundCorners(cornerRadius: 5.0, borderColor: textColor, borderWidth: 1.0)//16.0
        }
        nameLabel.text = questionName
    }
    
}
