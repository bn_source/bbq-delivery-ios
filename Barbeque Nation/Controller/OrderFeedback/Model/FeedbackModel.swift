//
 //  Created by Arpana on 13/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified FeedbackModel.swift
 //

import UIKit

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? newJSONDecoder().decode(FeedbackModel.self, from: jsonData)

import Foundation

import ObjectMapper

struct FeedbackModel : Mappable {
    var message : String?
    var message_type : String?
    var data : FeedBackStatusResponse?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        message <- map["message"]
        message_type <- map["message_type"]
        data <- map["data"]
    }

}

struct FeedBackStatusResponse : Mappable {
    var overall_rating : [Overall_rating]?
    var taste : Taste?
    var packaging : Packaging?
    var delivery : Delivery?
    var order_id  : Int?
    var overall_rating_id : String?
    var suggestion: String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        overall_rating <- map["overall_rating"]
        taste <- map["taste"]
        packaging <- map["packaging"]
        delivery <- map["delivery"]
        order_id  <- map["order_id"]
        overall_rating_id <- map["overall_rating_id"]
        suggestion <- map["suggestion"]

    }
}
struct Delivery : Mappable {
    var question_set : [Question_set]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        question_set <- map["question_set"]
    }

}
    struct Overall_rating : Mappable {
        var key : String?
        var text : String?
        
        init?(map: Map) {
            
        }
        
        mutating func mapping(map: Map) {
            
            key <- map["key"]
            text <- map["text"]
        }
        
    }
struct Packaging : Mappable {
    var question_set : [Question_set]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        question_set <- map["question_set"]
    }

}

    struct Question_set : Mappable {
        var question_type : String?
        var rating_set : [Rating_set]?
        var questions : [Questions]?
        var parent_question_id: Int?
        var  isItem: Bool?

        init?(map: Map) {

        }

        mutating func mapping(map: Map) {
            question_type <- map["question_type"]
            parent_question_id   <- map["parent_question_id"]
            rating_set <- map["rating_set"]
            questions <- map["questions"]
            isItem <- map["items"]
        }

    }

    
    struct Questions : Mappable {
        var question_id : String?
        var question_name : String?
        var answer : String?
        var max_rating : Int?
        var food_type : String?

        init?(map: Map) {
            
        }
        
        mutating func mapping(map: Map) {
            
            question_id <- map["question_id"]
            question_name <- map["question_name"]
            answer <- map["answer"]
            max_rating <- map["max_rating"]
            food_type <- map["food_type"]

        }
        
    }
struct Rating_set : Mappable {
    var rating_id : Int?
    var rating_desc : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        rating_id <- map["rating_id"]
        rating_desc <- map["rating_desc"]
    }

}

struct Taste : Mappable {
    var question_set : [Question_set]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        question_set <- map["question_set"]
    }

}
