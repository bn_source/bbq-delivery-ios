//
 //  Created by Arpana on 13/01/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified FeedbackViewModel.swift
 //

import UIKit

class FeedbackViewModel : BaseViewModel {
    
    private var feedbackModel: FeedbackModel? = nil
    
    var getLoyalityModel: FeedbackModel? {
        return feedbackModel
    }
}

    //MARK: Service Call Handler
//    extension FeedbackViewModel {
//        
//          func getFeedBackDetails(_ orderId: String, completion: @escaping (Bool)->()) {
//            FeedbackServiceManager.getFeedbackDetails(orderID: orderId ) { (model, result) in
//                if let resultValue = result {
//                    //Assign to model
//                    if resultValue {
//                      //  self.feedbackModel = model
//                        completion(true)
//                    }else{
//                        completion(false)
//                    }
//                }
//            }
//        }
// 
//     
//    }
