//
 //  Created by Arpana on 28/10/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified FeedbackReasonListModel.swift
 //

import Foundation
import ObjectMapper


struct FeedbackReasonListModel : Mappable {
    var message : String?
    var message_type : String?
    var feedBackQuestionMaster : [FeedBackQuestionMaster]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        message <- map["message"]
        message_type <- map["message_type"]
        feedBackQuestionMaster <- map["feedBackQuestionMaster"]
    }

}

struct FeedBackQuestionMaster : Mappable {
    var feed_back_type_id : Int?
    var feed_back_question_id : Int?
    var feed_back_question : String?
    var feed_back_type : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        feed_back_type_id <- map["feed_back_type_id"]
        feed_back_question_id <- map["feed_back_question_id"]
        feed_back_question <- map["feed_back_question"]
        feed_back_type <- map["feed_back_type"]
    }

}
