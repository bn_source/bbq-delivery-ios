/* 
Copyright (c) 2022 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

struct SideMenuFeedBackRequestModel : Mappable {
	var user_name : String?
	var user_email : String?
	var user_phone : String?
	var feedback_type_id : Int?
	var feedback_question_id : Int?
	var city : String?
	var outlet : String?
	var branch_id : String?
	var order_id : String?
	var images_url : [String?]?
	var rating : Int?
	var feedback_message : String?
	var user_auth_key : Int = 1
    var source: String = "ios"
    
    //newly added for multilocation
    var delivery_date: String?

	init?(map: Map) {

	}

    init() {}
	mutating func mapping(map: Map) {

		user_name <- map["user_name"]
		user_email <- map["user_email"]
		user_phone <- map["user_phone"]
		feedback_type_id <- map["feedback_type_id"]
		feedback_question_id <- map["feedback_question_id"]
		city <- map["city"]
		outlet <- map["outlet"]
		branch_id <- map["branch_id"]
		order_id <- map["order_id"]
		images_url <- map["images_url"]
		rating <- map["rating"]
		feedback_message <- map["feedback_message"]
		user_auth_key <- map["user_auth_key"]
        source <- map["source"]
        delivery_date <- map["delivery_date"]

	}

}
final class SubmitFeedBackResponseModel: Mappable {
    var message = ""
    var message_type = ""
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        message <- map["message"]
        message_type <- map["message_type"]
    }
}
