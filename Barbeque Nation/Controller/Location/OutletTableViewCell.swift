//
//  OutletTableViewCell.swift
//  location
//
//  Created by Abhijit on 13/08/19.
//  Copyright © 2019 Abhijit. All rights reserved.
//

import UIKit

class OutletTableViewCell: UITableViewCell {

    @IBOutlet weak var outletsLabel: UILabel!
}
