//
 //  Created by Rabindra L on 30/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified LocationRoundedRectButton.swift
 //

import UIKit

class LocationRoundedRectButton: RoundedRectButton {
    override var frame: CGRect {
        didSet {
            let color = UIColor.theme
            self.setTitleColor(color, for: .normal)
            self.roundCorners(cornerRadius: 5.0, borderColor: color, borderWidth: 1.0)
        }
    }
}
