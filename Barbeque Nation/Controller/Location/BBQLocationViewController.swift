//  BBQLocationViewController.swift
/*
 *  Created by Abhijit Singh on 20/08/19.
 *  Copyright © BARBEQUE NATION HOSPITALITY LIMITED 2019. All rights reserved.
 *  Last modified 03/09/19
 
 * CHANGE LOG
 * —------------------------------------------------------------------------------
 * ID          Bug ID      Author Name          Date                  Description
 * —------------------------------------------------------------------------------
 *
 *  1                       Maya Ranjith       11/10/2019            Bug Fixes
 */

import UIKit
import CoreLocation
import FirebaseAnalytics

enum TableViewCellTypes {
    case City, Outlet, Other,UseCurrent
}

protocol BBQLocationViewControllerProtocol: AnyObject {
    func navigateToBookingScreen()
}

class BBQLocationViewController: UIViewController,UIGestureRecognizerDelegate {
    
    var popularCities:[Cities]  = []
    var selectedCity:Bool = false
    var homeViewController = BBQHomeViewController()
    
    //MARK: Properties
    private var outletList = [Branches]()
    private var filteredData:[Cities]!
    private var isFiltered:Bool!
    private var expandedToKeyboardHeight:Bool!
    private var reducedToKeyboardHeight:Bool!
    private var isCitySelected:Bool = false
    private var isExpanded:Bool!
    private var keyBoardViewEndFrame:CGRect! = nil
    private var locationViewModel = BBQLocationViewModel()
    private var startingHeight:CGFloat!
    private var maxHeight:CGFloat!
    //private let myHeaderView = BBQHomeHeaderView()
    private var lastSelectedIndexPath:Any? = nil
    //private static let shared = BBQLocationViewController()
    private var selectedCellType: TableViewCellTypes = .City
    private var presentingController: BottomSheetController? = nil
    //    private var userDeniedLocation :Bool = false
    private var userAllowedCurrentLocation:Bool = false
    private var nearbyOutletArray:[NearbyOutelt] = []
    
    var isFromFeedbackScreen = false
    var userLatitudeValue:Double = 0.0
    var userLongitudeValue:Double = 0.0
    
    var bbqLastVisitedViewModel = BBQLastVisitedViewModel()
    var myindexPath = IndexPath(row:0,section: 0)
    
    var isNavigatingToBooking = false
    weak var delegate: BBQLocationViewControllerProtocol? = nil
    
    lazy var homeViewModel : HomeViewModel = {
        let homeModel = HomeViewModel()
        return homeModel
    }()
    
    
    var location: CLLocation! {
        didSet {
//            if let location = location {
//                locationViewModel.getCities(lat: Float(location.coordinate.latitude), long: Float(location.coordinate.longitude)) { [weak self] (isSuccess) in
//                    if let self = self {
//                        self.menuCity()
//                    }
//                }
//            }
        }
    }
    //MARK: Outlets
    @IBOutlet weak var cityButtonView: BBQCityButtonView!
    @IBOutlet weak var popularChoices: UILabel!
    @IBOutlet weak var pleaseSelectACityLabel: UILabel!
    @IBOutlet weak var serchImage: UIImageView!
    @IBOutlet weak var cityTableView: UITableView?
    @IBOutlet weak var selectCityTextField: UITextField!
    @IBOutlet weak var pencilImageView: UIImageView?
    @IBOutlet weak var changeButton: UIButton!
    @IBOutlet weak var selectedCityLabel: UILabel!
    @IBOutlet weak var citiesView: UIView!
    @IBOutlet weak var noDataLabel: UILabel!
    @IBOutlet weak var viewForCityButton: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIUtils.showLoader()
        AnalyticsHelper.shared.screenName(name: "LocationScreen", className: "BBQLocationViewController")
        Analytics.logEvent("Tap on Location", parameters: [
            "name": "Location Entry" as NSObject,
            "full_text": "Location page" as NSObject
            ])
        //selectCityTextField.isHidden = true
        self.view.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        filteredData = locationViewModel.cityList
        isFiltered = false
        startingHeight = 290
        maxHeight = UIScreen.main.bounds.height - 165
        cityTableView?.isHidden = false
        cityTableView?.reloadData()
        pencilImageView?.isHidden = true
        changeButton.isHidden = true
        selectedCityLabel.isHidden = true
        expandedToKeyboardHeight = false
        reducedToKeyboardHeight = false
        isExpanded = false
        self.noDataLabel.text = kNoDataFound
        self.noDataLabel.isHidden = true
        pleaseSelectACityLabel.isUserInteractionEnabled  = true
        if self.selectedCellType == .City{
            cityTableView?.register(UINib(nibName: Constants.LocationNibName.locationHeaderCellNibName, bundle: nil), forCellReuseIdentifier: Constants.LocationNibName.locationHeaderCellNibName)
        }
        pleaseSelectACityLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(endEditing)))
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: selectCityTextField.frame.height - 2, width: UIScreen.main.bounds.size.width, height: 1)
        
        bottomLine.backgroundColor = UIColor.locationTextFieldBottonLineColor.cgColor
        selectCityTextField.layer.addSublayer(bottomLine)
        
        locationViewModel.getCities(lat: Float(BBQLocationManager.shared.user_current_latitude), long: Float(BBQLocationManager.shared.user_current_longitude)) { (isSuccess) in
            UIUtils.hideLoader()
            if isSuccess {
                self.menuCity()
            }
        }
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(keyboard(notification: )), name: UIResponder.keyboardWillHideNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(keyboard(notification: )), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
        notificationCenter.addObserver(self, selector: #selector(userUpdatedLocation(_:)), name: Notification.Name(rawValue:Constants.NotificationName.userLocationChanged), object:nil)
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let presentingController = self.presentingController {
            presentingController.expandOrCollapse(viewController: self, withHeight:  maxHeight, isExpand: true)
        }
    }
    
    
   
    //MARK : - userUpdatedLocation called when location changes
    @objc func userUpdatedLocation(_ notification: Notification) {
        if let dictionary = notification.userInfo as NSDictionary? {
            let latValue = dictionary["latitude"] as! Double
            let longValue = dictionary["longitude"] as! Double
            if latValue == 0.0, longValue == 0.0 {
                self.userAllowedCurrentLocation = false
            //    self.showLocationStatusDeniedAlert()
                if let presentingController = self.presentingController {
                    presentingController.dismissBottomSheet(on: self)
                }
            }else{
                self.userAllowedCurrentLocation = true
                self.getTheNearbyOutelt(lattitude: latValue, longitude: longValue)
                
                userLatitudeValue = latValue
                userLongitudeValue = longValue
            }
        }
    }
    
    // Mark:Funtion for Long Press Selection
    func setupLongPressGesture() {
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress))
        longPressGesture.minimumPressDuration = 1.0 // 1 second press
        longPressGesture.delegate = self
        self.cityTableView!.addGestureRecognizer(longPressGesture)
    }
    @objc func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer){
        if gestureRecognizer.state == .began {
            
            let touchPoint = gestureRecognizer.location(in: self.cityTableView)
            if let indexPath:NSIndexPath = cityTableView!.indexPathForRow(at: touchPoint) as NSIndexPath? {
                var outlet: Branches? = nil
                if self.selectedCellType == .UseCurrent {
                    outlet = locationViewModel.branchesarray[indexPath.row]
                } else {
                    outlet = outletList[indexPath.row]
                    
                }
                guard let selectedOutelt = outlet
                    
                    else {
                        return
                }
                if BBQUserDefaults.sharedInstance.accessToken != "" {
                    bbqLastVisitedViewModel.getLastVisitedBranch(id: selectedOutelt.branch_id ?? "0") {_ in
                        //                    print(self.bbqLastVisitedViewModel)
                    }
                }
                
                BBQLocationManager.shared.outlet_branch_id = selectedOutelt.branch_id!
                BBQLocationManager.shared.outlet_branch_name = selectedOutelt.branch_name!
                
                let dataDict = ["lat": selectedOutelt.latitude!,
                                "long": selectedOutelt.longitude!,
                                "branchId": selectedOutelt.branch_id!,
                                "branchName": selectedOutelt.branch_name!]
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.NotificationName.locationDataUpdate), object: nil, userInfo: dataDict)
                
                if let presentingController = self.presentingController {
                    presentingController.dismissBottomSheet(on: self)
                }
            }
        }
    }
    

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.NotificationName.dismissOutletSelection), object: nil, userInfo: nil)
    }
    

    
    //Mark : Function for Use Current Location
    @IBAction func  useCurrentLocationTapped(){
        // BBQLocationManager.shared.updateUserLocationWithPermission()
        
        //Checking whether location is enabled in Settings
        if(BBQLocationManager.shared.currentLocationManager.location != nil){
            userLatitudeValue = (BBQLocationManager.shared.currentLocationManager.location?.coordinate.latitude)!
            userLongitudeValue = (BBQLocationManager.shared.currentLocationManager.location?.coordinate.longitude)!
            self.userAllowedCurrentLocation = true
            self.getTheNearbyOutelt(lattitude:userLatitudeValue, longitude: userLongitudeValue)
            //self.selectedCellType = .UseCurrent
        }else{
            self.userAllowedCurrentLocation = false
            showLocationAlert()
        }
        
    }
    
    func getTheNearbyOutelt(lattitude:Double,longitude:Double){
        BBQActivityIndicator.showIndicator(view: self.view)
        self.homeViewModel.getNearbyOutlets(latitide: lattitude, longitude: longitude) { (isSuccess) in
            BBQActivityIndicator.hideIndicator(from: self.view)
            if(isSuccess){
                if(self.homeViewModel.getNearbyLocationData().count>0){
                    //self.selectedCellType = .UseCurrent
                    self.nearbyOutletArray = self.homeViewModel.getNearbyLocationData()
                    if(self.nearbyOutletArray.count>0){
                        self.userAllowedCurrentLocation = true
                        self.showTheNearByOutletList(nearbyList: self.nearbyOutletArray)
                        self.selectedCellType = .UseCurrent
                        self.noDataLabel.isHidden = true
                        self.cityTableView!.isHidden = false
                        self.popularChoices.isHidden = false
                        self.cityTableView?.reloadData()
                    }
                }else{
                    self.userAllowedCurrentLocation = false
                    self.noDataLabel.isHidden = true
                    self.showTheNearByOutletList(nearbyList: [])
                    self.cityTableView!.isHidden = false
                    self.popularChoices.isHidden = false
                    
                    
                }
            }else{
                self.userAllowedCurrentLocation = false
                self.noDataLabel.isHidden = false
                self.popularChoices.isHidden = false
                self.showTheNearByOutletList(nearbyList: [])
                
            }
            
            
        }
    }
    func showTheNearByOutletList(nearbyList:[NearbyOutelt]){
        
        //self.selectedCellType = .UseCurrent
//        citiesView.isHidden = false
        viewForCityButton.isHidden = true
        cityTableView?.reloadData()
        cityTableView?.isHidden = false
        selectCityTextField.text = ""
        self.changeButton.isHidden = true
        popularChoices.isHidden = false
        popularChoices.text = kPleaseSelectAnOutlet
        pleaseSelectACityLabel.text = kSelectedCity
        selectCityTextField.isHidden = true
        selectCityTextField.isEnabled = false
        selectedCityLabel.isHidden = false
        if(nearbyList.count>0){
            let neabyCityObj:NearbyOutelt = nearbyList[0]
            selectedCityLabel.text = neabyCityObj.nearbyCityName
            
        }else{
            selectedCityLabel.text = ""

        }
        serchImage.isHidden = false
        pencilImageView?.isHidden = false
        changeButton.isHidden = false
        serchImage.image = UIImage (named: kIcoSearch )
        if !isExpanded {
            isExpanded = true
            if self.presentingController != nil {
                //presentingController.expandOrCollapse(viewController: self, withHeight:  maxHeight - self.view.frame.height  , isExpand: true)
            }
            cityTableView?.isHidden = false
//            citiesView.isHidden = false
            
        }
        self.selectedCellType = .Outlet
        expandedToKeyboardHeight = false
        reducedToKeyboardHeight = false
        
    }
    
    private  func showLocationAlert() {
        PopupHandler.showTwoButtonsPopup(title: kUpdateLocationTitle,
                                         message: kUpdateLocationMessage,
                                         image: UIImage(named: "icon_location_orange"),
                                         titleImageFrame: CGRect(x: 0, y: -1, width: 16, height: 20),
                                         on: self,
                                         firstButtonTitle: kAlertAllow, firstAction: {
                                            if BBQUserDefaults.sharedInstance.userDeniedLocation{
                                                self.showLocationStatusDeniedAlert()
                                            }else{
                                                BBQLocationManager.shared.updateUserLocationWithPermission()
                                            }
        },
                                         secondButtonTitle: kAlertDeny) {
                                            if let presentingController = self.presentingController {
                                                presentingController.dismissBottomSheet(on: self)
                                            }
        }
    }
    
    // show the  user deied location alert
    // MARK: - showLocationStatusDeniedAlert
    private  func showLocationStatusDeniedAlert() {
        PopupHandler.showTwoButtonsPopup(title: kLocationDisabledTitle,
                                         message: kLocationDisabledMessage,
                                         on: self,
                                         firstButtonTitle: kCancelString,
                                         firstAction: {
                                            //                                            Dismiss bottomsheet when user denies location permission
                                            if let presentingController = self.presentingController {
                                                presentingController.dismissBottomSheet(on: self)
                                            }
        },
                                         
                                         secondButtonTitle: kOpenSettings) {
                                            if #available(iOS 10.0, *) {
                                                let settingsURL = URL(string: UIApplication.openSettingsURLString)!
                                                UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
                                            } else {
                                                if let url = NSURL(string:UIApplication.openSettingsURLString) {
                                                    UIApplication.shared.openURL(url as URL)
                                                }
                                            }
        }
    }
    //Mark :Change Button
    @IBAction func change(_ sender: UIButton) {
        selectCityTextField.text = ""
        self.noDataLabel.isHidden = true
        self.changeButton.isHidden = true
        pleaseSelectACityLabel.text = kPleaseSelectACity
        popularChoices.text = kPopularChoices
        selectCityTextField.isHidden = false
        selectCityTextField.isEnabled = true
        selectedCityLabel.isHidden = true
        serchImage.isHidden = false
        pencilImageView?.isHidden = true
        serchImage.image = UIImage (named: kIcoSearch )
        if isExpanded {
            isExpanded = false
            if self.presentingController != nil {
                //presentingController.expandOrCollapse(viewController: self, withHeight:  maxHeight - self.view.frame.height  , isExpand: true)
            }
            
            serchImage.isHidden = false
            serchImage.image = UIImage (named: kIcoSearch )
        }
        cityTableView?.isHidden = false
//            citiesView.isHidden = false
        viewForCityButton.isHidden = false
        expandedToKeyboardHeight = false
        reducedToKeyboardHeight = false
        selectedCellType = .City
        cityTableView?.reloadData()
    }
    //Mark : Function for calling dynamic buttons
    func menuCity() {
        cityButtonView.cityButtonDelegate = self
        popularCities = locationViewModel.cityList
        //Sort the city list based on weightage in asending order
        
//        popularCities = demoCategory.sorted(by: {$0.cityWeightage! < $1.cityWeightage!})
        
        var cityName = [String]()
        // Only 4 popular choice should be shown
         for index in 0..<popularCities.count {
            if index > 3 {
                break
            }
            let cityObj = popularCities[index]
            cityName.append(cityObj.city_name ?? "")
        }
        cityButtonView.initiateButtons(titleArray: cityName)
        cityTableView?.reloadData()
    }
    func setupTableViewCell()
    {
        cityTableView?.reloadData()
    }
    @objc func endEditing(){
        self.view.endEditing(true)
    }
    //Mark :Search Bar Function
    @IBAction func selectCitySearchBar(_ sender: UITextField) {
       // popularChoices.isHidden = true
    }
    @objc func keyboard(notification:Notification){
        let userInfo = notification.userInfo!
        let keyBoardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        keyBoardViewEndFrame = view.convert(keyBoardScreenEndFrame,from:view.window)
        if notification.name == UIResponder.keyboardWillHideNotification{
            if !reducedToKeyboardHeight  && !isExpanded{
                if self.presentingController != nil {
                    //presentingController.expandOrCollapse(viewController: self, withHeight:  maxHeight - self.view.frame.height  , isExpand: true)
                }
                reducedToKeyboardHeight  = true
                expandedToKeyboardHeight = false
            }
        }
        else{
            if !expandedToKeyboardHeight{
                expandedToKeyboardHeight = true
                reducedToKeyboardHeight = false
                if self.presentingController != nil {
                    //presentingController.expandOrCollapse(viewController: self, withHeight: keyBoardViewEndFrame.height  , isExpand: true)
                    
                }
            }
        }
    }
}
//Mark :Table View Delegates
extension BBQLocationViewController:UITableViewDataSource{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        selectCityTextField.endEditing(true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.selectedCellType == .City {
            if isFiltered{
                return filteredData.count
            }
            else{
                return locationViewModel.cityList.count
            }
        }
        else {
            
            if self.selectedCellType == .UseCurrent {
                return self.nearbyOutletArray.count
            }else{
                return outletList.count
                
            }
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.selectedCellType == .City {
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.LocationNibName.locationHeaderCellNibName, for: indexPath) as? CountryTableViewCell else {
                return UITableViewCell()
            }
            cell.tickImage.isHidden = true
            
            if isFiltered{
                if filteredData.count <= indexPath.row{
                    return UITableViewCell()
                }
                let cityObj:Cities = filteredData[indexPath.row]
                cell.countryLabel.text = cityObj.city_name
            }
            else {
                if locationViewModel.cityList.count <= indexPath.row{
                    return UITableViewCell()
                }
                let cityObj:Cities = locationViewModel.cityList[indexPath.row]
                cell.countryLabel.text = cityObj.city_name
            }
            cell.imgIcon.image = UIImage(named: "location")
            cell.viewForIcon.backgroundColor = .clear
            
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.LocationNibName.locationHeaderCellNibName, for: indexPath) as? CountryTableViewCell else {
                return UITableViewCell()
            }
            cell.tickImage.isHidden = true
            if self.selectedCellType == .UseCurrent {
                if(self.userAllowedCurrentLocation){
                    if nearbyOutletArray.count <= indexPath.row{
                        return UITableViewCell()
                    }
                    let branchObj:NearbyOutelt = nearbyOutletArray[indexPath.row]
                    cell.countryLabel.text = branchObj.nearbyBranchName
                    
                }
                
            } else {
                if outletList.count <= indexPath.row{
                    return UITableViewCell()
                }
                let branchObj:Branches = outletList[indexPath.row]
                cell.countryLabel.text = branchObj.branch_name
                
            }
            cell.imgIcon.image = UIImage(named: "bbq_logo_white")
            cell.viewForIcon.backgroundColor = .theme
            return cell
        }
    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 64.0//Choose your custom row height
//    }
    

}

// MARK:- UITableViewDelegate
extension BBQLocationViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var branchID:String = ""
        var branchName:String = ""
        var branchLat:String = ""
        var branchLong:String = ""
        var cityName : String = ""
//        var branchDist:String = ""
        guard let cell:CountryTableViewCell = tableView.cellForRow(at: indexPath) as? CountryTableViewCell else {
            return
        }
        if self.selectedCellType == .City {
            var cityObj:Cities = locationViewModel.cityList[indexPath.row]
            if isFiltered{
                cityObj = filteredData[indexPath.row]
            }
            
            let cityArray :[Branches] = locationViewModel.getOutlet(for: cityObj.city_name!)
            
            outletList = cityArray.sorted(by: { $0.branch_name!.lowercased() < $1.branch_name!.lowercased() })
//            outletList
            self.isCitySelected = true
            selectCityTextField.isEnabled = false
            popularChoices.isHidden = false
            viewForCityButton.isHidden = true
            popularChoices.text = kPleaseSelectAnOutlet
            pleaseSelectACityLabel.text = kSelectedCity
            serchImage.image = UIImage (named: kIcoEdit )
            cityTableView?.allowsSelection = true
            cell.tickImage.isHidden = true
            cell.backgroundColor = UIColor.clear
            self.selectedCellType = .Outlet
            self.serchImage.isHidden = true
            self.pencilImageView?.isHidden = false
            self.changeButton.isHidden = false
            selectCityTextField.text = ""
            selectCityTextField.isHidden = true
            selectedCityLabel.isHidden = false
            selectedCityLabel.text = cityObj.city_name
          

            cell.backgroundColor =  UIColor.clear
            cell.tickImage.isHidden = true
            
            if lastSelectedIndexPath != nil {
                if let lastCell = cityTableView?.cellForRow(at: lastSelectedIndexPath as! IndexPath) as? CountryTableViewCell {
                    lastCell.tickImage.isHidden = true
                    lastCell.backgroundColor = UIColor.clear
                }
            }
            
            if let currentCell = cityTableView?.cellForRow(at: indexPath) as? CountryTableViewCell {
                if !isCitySelected {
                    currentCell.tickImage.isHidden = false
                } else {
                    currentCell.tickImage.isHidden = true
                }
            }
            setupTableViewCell()
            if isNavigatingToBooking, let _ = self.presentingController {
                //presentingController.expandOrCollapse(viewController: self, withHeight:  maxHeight, isExpand: true)
            }
        }else{
            if self.selectedCellType == .UseCurrent {
                let branchObj:NearbyOutelt = nearbyOutletArray[indexPath.row]
                branchID = branchObj.nearbyBranchId!
                branchName = branchObj.nearbyBranchName!
                branchLat = branchObj.nearbyBranchLatitude!
                branchLong = branchObj.nearbyBranchLongitude!
                branchLong = branchObj.nearbyBranchDistance!
                cityName = branchObj.nearbyCityName!
            } else if self.selectedCellType == .Outlet{
                let outlet: Branches = outletList[indexPath.row]
                branchID = outlet.branch_id!
                branchName = outlet.branch_name!
                branchLat = outlet.latitude!
                branchLong = outlet.longitude!
                cityName = selectedCityLabel.text!
            }
            
            if isFromFeedbackScreen == false {
                
                if BBQUserDefaults.sharedInstance.accessToken != "" {
                    bbqLastVisitedViewModel.getLastVisitedBranch(id: branchID) {_ in
                        //                print(self.bbqLastVisitedViewModel)
                    }
                }
                BBQUserDefaults.sharedInstance.branchIdValue = branchID
                BBQLocationManager.shared.outlet_branch_id = branchID
                BBQUserDefaults.sharedInstance.CurrentSelectedLocation = branchName
                BBQLocationManager.shared.outlet_branch_name = branchName
            }
            let dataDict = ["lat": branchLat,
                            "long": branchLong,
                            "branchId": branchID,
                            "branchName": branchName,
                            "cityName": cityName]
            cell.tickImage.isHidden = false
            

            
            if let presentingController = self.presentingController {
                presentingController.dismissBottomSheet(on: self)
                let secondsToDelay = 0.1
                DispatchQueue.main.asyncAfter(deadline: .now() + secondsToDelay) {
                    if self.isFromFeedbackScreen {
                        
                        NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.NotificationName.locationDataUpdatedForFeedbackPurpose), object: nil, userInfo: dataDict)
                        return
                    }
                    // Put any code you want to be delayed here
                    NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.NotificationName.locationDataUpdate), object: nil, userInfo: dataDict)
                }
                
            }else{
                if self.isFromFeedbackScreen {
                    
                    NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.NotificationName.locationDataUpdatedForFeedbackPurpose), object: nil, userInfo: dataDict)
                    return
                }
                NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.NotificationName.locationDataUpdate), object: nil, userInfo: dataDict)
            }
            if isNavigatingToBooking, let delegate = self.delegate {
                delegate.navigateToBookingScreen()
            }
        }
        if !isNavigatingToBooking, let _ = self.presentingController {
            //presentingController.expandOrCollapse(viewController: self, withHeight:  maxHeight, isExpand: true)
        }
    }
}

//Mark :TextField Delegate
extension BBQLocationViewController:UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        self.selectedCellType = .City
       // popularChoices.isHidden = true
//        citiesView.isHidden = false
        
        if newString.length > 0  {
            let fiteredArray  = (locationViewModel.cityList).filter() { $0.city_name?.lowercased().contains((newString as String).lowercased()) ?? false }
          self.filteredData =  fiteredArray.sorted(by: { $0.city_name!.lowercased() < $1.city_name!.lowercased() })
            
//            filteredData = (locationViewModel.cityList).filter() { $0.city_name?.lowercased().starts(with: (newString as String).lowercased()) ?? false }
        
                isFiltered = true
                 self.cityTableView!.isHidden = false
                self.noDataLabel.isHidden = true
                viewForCityButton.isHidden = true
                setupTableViewCell()

            
            if !isExpanded
                    {
                        isExpanded = true
                        if let _ = self.presentingController {
                            //presentingController.expandOrCollapse(viewController: self, withHeight:  maxHeight - self.view.frame.height  , isExpand: true)
                        }
                    }
            
//            else{
//
//                isExpanded = false
//                if let presentingController = self.presentingController {
//                   presentingController.expandOrCollapse(viewController: self, withHeight: maxHeight - self.view.frame.height , isExpand: false)
//            }
//         }
            
            
            
         }else{
            isFiltered = false
           self.cityTableView!.isHidden = false
            self.noDataLabel.isHidden = true
            viewForCityButton.isHidden = false
         }
//        if newString == ""{
//            self.view.endEditing(true)
//            isFiltered = false
//            self.cityTableView!.isHidden = true
//            self.noDataLabel.isHidden = true
//            selectCityTextField.text = ""
//            self.noDataLabel.isHidden = true
//            self.changeButton.isHidden = true
//            pleaseSelectACityLabel.text = kPleaseSelectACity
//            popularChoices.text = kPopularChoices
//            selectCityTextField.isHidden = false
//            selectCityTextField.isEnabled = true
//            selectedCityLabel.isHidden = true
//            serchImage.isHidden = false
//            pencilImageView?.isHidden = true
//            serchImage.image = UIImage (named: kIcoSearch )
//            if isExpanded {
//                isExpanded = false
//                if let presentingController = self.presentingController {
//                    presentingController.expandOrCollapse(viewController: self, withHeight: self.view.frame.height - startingHeight, isExpand: false)
//                }
//                cityTableView?.isHidden = true
//                citiesView.isHidden = false
//                cityButtonView.isHidden = false
//                serchImage.isHidden = false
//                serchImage.image = UIImage (named: kIcoSearch )
//            }
//            expandedToKeyboardHeight = false
//            reducedToKeyboardHeight = false
//            selectedCellType = .City
//
//
//        }

        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
}
extension BBQLocationViewController: BBQCityButtonViewProtocol {
    func didTapOnMenuCategoryAt(index tagIndex: Int) {
        self.selectedCellType = .Other
        let cityObj:Cities = popularCities[tagIndex]
        let outletArray:[Branches] = locationViewModel.getOutlet(for: cityObj.city_name!)
        //Sorting the outlet names alphabetically
        let sortedArray = outletArray.sorted(by: { $0.branch_name!.lowercased() < $1.branch_name!.lowercased() })
        outletList = sortedArray
//        citiesView.isHidden = false
        viewForCityButton.isHidden = true
        self.noDataLabel.isHidden = true
        cityTableView?.reloadData()
        cityTableView?.isHidden = false
        selectCityTextField.text = ""
        self.changeButton.isHidden = true
        popularChoices.isHidden = false
        popularChoices.text = kPleaseSelectAnOutlet
        pleaseSelectACityLabel.text = kSelectedCity
        selectCityTextField.isHidden = true
        selectCityTextField.isEnabled = false
        selectedCityLabel.isHidden = false
        selectedCityLabel.text = cityObj.city_name
        serchImage.isHidden = false
        pencilImageView?.isHidden = false
        changeButton.isHidden = false
        serchImage.image = UIImage (named: kIcoSearch )
        if !isExpanded {
            isExpanded = true
            if let _ = self.presentingController {
                //presentingController.expandOrCollapse(viewController: self, withHeight:  maxHeight - self.view.frame.height  , isExpand: true)
            }
            cityTableView?.isHidden = false
//            citiesView.isHidden = false
            
        }
        self.selectedCellType = .Outlet
        expandedToKeyboardHeight = false
        reducedToKeyboardHeight = false
    }
    func setupPresentingController(controller: BottomSheetController) {
        self.presentingController = controller
    }
}
