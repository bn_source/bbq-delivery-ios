//
//  CountryTableViewCell.swift
//  location
//
//  Created by Abhijit on 12/08/19.
//  Copyright © 2019 Abhijit. All rights reserved.
//

import UIKit

class CountryTableViewCell: UITableViewCell {

    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var viewForIcon: UIView!
    
    
    @IBOutlet weak var tickImage: UIImageView!
    
    var tickImageSelection:Bool = true
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        tickImage.isHidden = true
    }
    
    
}
