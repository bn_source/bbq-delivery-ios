//
 //  Created by Abhijit on 27/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQCityButtonView.swift
 //

import UIKit


protocol BBQCityButtonViewProtocol: AnyObject {
    func didTapOnMenuCategoryAt(index tagIndex: Int)
}

class BBQCityButtonView: UIView {
    
    private let insetPadding = 15
    private let linePadding = 5
    
    weak var cityButtonDelegate: BBQCityButtonViewProtocol? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func initiateButtons(titleArray: [String]) {
        
        let inset = insetPadding * 2
        let line = ((titleArray.count - 1) * linePadding)
        let totalAvailableWidth = UIScreen.main.bounds.size.width - (CGFloat(inset) + CGFloat(line))
        var buttonWidth = (totalAvailableWidth / CGFloat(titleArray.count))
        let maxButtonWidth = (totalAvailableWidth / CGFloat(4))
        
        if buttonWidth > maxButtonWidth {
            buttonWidth = maxButtonWidth
        }
        
        for (index, item) in titleArray.enumerated() {
            let rounderRectButton = LocationRoundedRectButton(withTitle: item)
            rounderRectButton.tag = index
            rounderRectButton.titleLabel?.lineBreakMode = .byTruncatingTail
            
            //process actions
            rounderRectButton.addTarget(self, action: #selector(self.tappedOnButton(sender:)), for: .touchUpInside)
            
            //calculate frame
            let frameX = CGFloat(insetPadding) + (buttonWidth * CGFloat(index)) + (CGFloat(linePadding) * CGFloat(index))
            rounderRectButton.frame = CGRect(x: frameX, y: 4, width: buttonWidth, height: 30)
            self.addSubview(rounderRectButton)
        }
    }
    
    @objc func tappedOnButton(sender: RoundedRectButton) {
        //Mark the selected one
        cityButtonDelegate?.didTapOnMenuCategoryAt(index: sender.tag)
    }
    
}
