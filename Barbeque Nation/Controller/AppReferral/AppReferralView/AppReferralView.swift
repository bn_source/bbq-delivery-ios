//
 //  Created by Sakir Sherasiya on 29/11/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified AppReferralView.swift
 //


protocol ReferalDelegate {
    func shareReferalCode()
}

import UIKit


class AppReferralView: UIView {

    let nibName = "AppReferralView"
    @IBOutlet weak var colorView : UIView!
    var delegetToReferal : ReferalDelegate?

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit() 
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()

    }

    func setBackGroundColor(color: UIColor){
        
        colorView.backgroundColor = color
    }

    
    func commonInit() {
        let bundle = Bundle.init(for: AppReferralView.self)
        if let viewsToAdd = bundle.loadNibNamed(nibName, owner: self, options: nil), let contentView = viewsToAdd.first as? UIView {
            addSubview(contentView)
            contentView.frame = self.bounds
            contentView.autoresizingMask = [.flexibleHeight,
                                            .flexibleWidth]
        }
        
//        if   BBQUserDefaults.sharedInstance.YourReferalCode == ""  {
//
//            self.getYourReferalCode()
//        }

    }
    
    
    @IBAction func reddemBtnClicked(_ sender : UIButton){
        
        if delegetToReferal != nil {
            delegetToReferal?.shareReferalCode()
        }
    }

}
