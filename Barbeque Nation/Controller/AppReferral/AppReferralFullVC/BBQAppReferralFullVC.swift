//
 //  Created by Sakir Sherasiya on 28/11/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified BBQAppReferralFullVC.swift
 //

import UIKit
import ShimmerSwift

class BBQAppReferralFullVC: BBQBaseViewController {
    
    @IBOutlet weak var  lblCode :UILabel!
    @IBOutlet weak var lblTitle1 :UILabel!
    @IBOutlet weak var lblTitle2 :UILabel!
    @IBOutlet weak var  lblTitle3:UILabel!
    
    @IBOutlet weak var tbleView: UITableView!
    @IBOutlet weak var shimmerViewDetails: UIView!
    @IBOutlet  var tbleViewHeight: NSLayoutConstraint!

    @IBOutlet weak var btnRedeem: UIButton!
    @IBOutlet weak var shimmerContainer: UIView!
    var shimmerView : ShimmeringView?
    var modelToRefer : ReferalModel?
    private func showShimmering() {
        self.shimmerView?.isHidden = false
        shimmerContainer.isHidden = false
        shimmerViewDetails.isHidden = false
        if let shimmer = self.shimmerView{
            self.shimmerContainer.addSubview(shimmer)
            shimmer.contentView = shimmerViewDetails
            shimmer.shimmerAnimationOpacity = 0.2
            shimmer.shimmerSpeed = 500.00
            shimmer.isShimmering = true
        }
    }
    
    private func stopShimmering() {

        if let shimmer = self.shimmerView {
            shimmer.isShimmering = false
            shimmer.isHidden = true
        }
        shimmerContainer.isHidden = true
        self.shimmerViewDetails.isHidden = true
       
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        shimmerView = ShimmeringView(frame: self.view.bounds)

        //call api
        self.getYourReferalCode()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AnalyticsHelper.shared.triggerEvent(type: .RE02)
        tbleView.estimatedRowHeight = 100
        tbleView.estimatedRowHeight = UITableView.automaticDimension
        tbleView.addObserver(self, forKeyPath: "contentSize", options: .prior, context: nil)
        btnRedeem.setShadow()
            
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tbleView.removeObserver(self, forKeyPath: "contentSize")

    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize"{
            self.tbleViewHeight.constant = self.tbleView.contentSize.height
            self.view.layoutSubviews()
        }
    }
    
    func getYourReferalCode(){
        
        showShimmering()
        
        BBQServiceManager.getInstance().getReferralCodeToShare(params: ["mobile_number": BBQUserDefaults.sharedInstance.customerId , "customer_id" : BBQUserDefaults.sharedInstance.customerId , "customer_name" : BBQUserDefaults.sharedInstance.UserName ]) { error, model in

            DispatchQueue.main.async {
           
                self.stopShimmering()

                if error != nil {
                    
                    UIUtils.showToast(message: "Could not fetch your referal code")
                    self.lblCode.text = ""
                    self.btnRedeem.isEnabled = false
                }
                if error == nil && model != nil {
                    
                    self.btnRedeem.isEnabled = true
                    
                    BBQUserDefaults.sharedInstance.YourReferalCode = model?.data?.referral_code ?? ""
                    BBQUserDefaults.sharedInstance.YourReferalCodeURL = model?.data?.short_url ?? ""
                    
                    self.modelToRefer = model
                    
                    self.tbleView.reloadData()
//                    if  let text1 = model?.data?.text_messages?[0].data(using: .utf8) as? Data{
//                        var attributedString = try? NSMutableAttributedString(
//                            data: text1,
//                            options: [.documentType: NSAttributedString.DocumentType.html ],
//                            documentAttributes: nil)
//                        
////                        let myAttribute = [ NSAttributedString.Key.font: UIFont(name: Constants.App.BBQAppFont.ThemeMedium, size: 18.0)! ]
////                        attributedString?.addAttributes(myAttribute, range: NSMakeRange(0, attributedString?.length ?? 0))
//                        self.lblTitle1.attributedText = attributedString
//                    }
//                    if  let text2 = model?.data?.text_messages?[1].data(using: .utf8)! {
//                        var attributedString = try? NSMutableAttributedString(
//                            data: text2,
//                            options: [.documentType: NSAttributedString.DocumentType.html ],
//                            documentAttributes: nil)
////                        let myAttribute = [ NSAttributedString.Key.font: UIFont(name: Constants.App.BBQAppFont.ThemeMedium, size: 18.0)! ]
////
////                        attributedString?.addAttributes(myAttribute, range: NSMakeRange(0, attributedString?.length ?? 0))
//
//                        self.lblTitle2.attributedText = attributedString
//                    }
//                    if  let text3 = model?.data?.text_messages?[2].data(using: .utf8)! {
//                        var attributedString = try? NSMutableAttributedString(
//                            data: text3,
//                            options: [.documentType: NSAttributedString.DocumentType.html ],
//                            documentAttributes: nil)
//                        
//                        self.lblTitle3.attributedText = attributedString
//                    }
                    self.lblCode.text = BBQUserDefaults.sharedInstance.YourReferalCode
                    
                }
                
            }
        }
    }
    
    @IBAction func redeemNowBtnClicked(_ sender : UIButton){

        shareMethod()
       
    }
    
    
    func shareMethod() {
    
        var  textToShare = Constants.App.LoyalitiPage.InviteFriends +  BBQUserDefaults.sharedInstance.YourReferalCodeURL
        
        if    modelToRefer?.data?.share_text != ""  && modelToRefer?.data?.share_text != nil {
            
            textToShare = (modelToRefer?.data?.share_text)!
        }
        let pasteboard = UIPasteboard.general
        pasteboard.string = textToShare
        
        let shareModel = ShareModel(data: textToShare, type: .Text, attributedText: nil)
        BBQShareHandler.shareContent(contentToShare: shareModel, on: self, isBottomSheetPresenter: true)
    }    /*  func shareMethod() {
     let  textToShare = Constants.App.LoyalitiPage.InviteFriends + Constants.App.LoyalitiPage.referAndEarnShortLink
     let pasteboard = UIPasteboard.general
     pasteboard.string = textToShare
     
     let shareModel = ShareModel(data: textToShare, type: .Text, attributedText: nil)
     BBQShareHandler.shareContent(contentToShare: shareModel, on: self, isBottomSheetPresenter: true)
 }?*/
    @IBAction func copyBtnClicked(_ sender : UIButton){
        
        if self.lblCode.text == "" {
            UIUtils.showToast(message: "could not copy referal code!")
            return
        }
        AnalyticsHelper.shared.triggerEvent(type: .RE03)
        UIPasteboard.general.string =  self.lblCode.text
        UIUtils.showToast(message: String(format:" %@ is copied!", lblCode.text!) )
    }
     

    @IBAction func backBtnClicked(_ sender : UIButton){
        
        self.navigationController?.popViewController(animated: false)
    }
}

extension BBQAppReferralFullVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.modelToRefer?.data?.text_messages?.count ?? 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BBQReferslTableViewCell", for: indexPath) as! BBQReferslTableViewCell
        
        if  let text1  = modelToRefer?.data?.text_messages?[indexPath.row].messgae {
            
                    if indexPath.row == (modelToRefer?.data?.text_messages?.count ?? 0) - 1 {
                        //this is the last row in section.
                        cell.dashedLine.isHidden = true
                    }
            
            let normalFont =  UIFont(name: Constants.App.BBQAppFont.ThemeRegular, size: 14.0)
            let boldSearchFont =  UIFont(name: Constants.App.BBQAppFont.ThemeSemiBold, size: 14.0)
            cell.lblTitleMessage.attributedText = addBoldText( fullString: (text1 as? NSString) ?? "", boldPartOfString: (modelToRefer?.data?.text_messages?[indexPath.row].bold_text?.first ?? "Smiles") as NSString, font: normalFont!, boldFont: boldSearchFont!)
        }
        return cell
        
    }
    
    
    
    func addBoldText(fullString: NSString, boldPartOfString: NSString, font: UIFont!, boldFont: UIFont!) -> NSAttributedString {
        let nonBoldFontAttribute = [NSAttributedString.Key.font:font!]
        let boldFontAttribute = [NSAttributedString.Key.font:boldFont!]
        let boldString = NSMutableAttributedString(string: fullString as String, attributes:nonBoldFontAttribute)
        boldString.addAttributes(boldFontAttribute, range: fullString.range(of: boldPartOfString as String))
        return boldString
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
