//
 //  Created by Nischitha on 12/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified PromoAndOfferCell.swift
 //

import UIKit

class PromoAndOfferCell: UICollectionViewCell {
    @IBOutlet weak var promotionLabel: UILabel!
    @IBOutlet weak var promotionImage: UIImageView!
    @IBOutlet weak var promotionLabelView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
      promotionLabelView.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.bottomCornersMask)
        
        
    }
}
