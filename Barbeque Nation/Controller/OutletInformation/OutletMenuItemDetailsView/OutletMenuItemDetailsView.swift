//
 //  Created by Mahmadsakir on 29/12/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified ItemDetailViewController.swift
 //

import UIKit

class OutletMenuItemDetailsView: UIView {

    @IBOutlet weak var foodTypeIcon: UIImageView!
    @IBOutlet weak var menuDescLbl: UILabel!
    @IBOutlet weak var menuTitleLbl: UILabel!
    @IBOutlet weak var menuIcon: UIImageView!
    @IBOutlet weak var lblLiner: UILabel!
    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var imageRatio: NSLayoutConstraint!
    @IBOutlet weak var backgroundAlpha: UIView!
    @IBOutlet weak var viewForImageView: UIView!
    
    
   
    var isAnimationCompleted = true
    var item: MenuModelDetails?
    
    
    //MARK:- View Configuration
    func initWith(item: MenuModelDetails) -> OutletMenuItemDetailsView {
        let view = loadFromNib()!
        view.item = item
        view.viewForContainer.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        view.styleUI()
        return view
    }
        
        
    func loadFromNib() -> OutletMenuItemDetailsView? {
        if let views = Bundle.main.loadNibNamed("OutletMenuItemDetailsView", owner: self, options: nil), let view = views[0] as? OutletMenuItemDetailsView{
            view.frame = UIApplication.shared.windows[0].bounds
            return view
        }
        return nil
    }
    
    func addToSuperView(view: UIViewController?) {
        if let tabBarController = view?.navigationController?.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
            tabBarController.tabBar.isHidden = true
        }
        if let view = view{
            view.view.addSubview(self)
        }else{
            getMainView().addSubview(self)
        }
        viewForContainer.isHidden = true
        self.backgroundAlpha.alpha = 0.0
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
            self.viewForContainer.isHidden = false
            self.backgroundAlpha.alpha = 1
        } completion: { (isCompleted) in
            self.viewForContainer.isHidden = false
        }
    }
    
    func removeToSuperView() {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
            self.viewForContainer.isHidden = true
            self.backgroundAlpha.alpha = 0.0
            self.layoutIfNeeded()
        } completion: { (isCompleted) in
            self.viewForContainer.isHidden = true
            self.backgroundAlpha.alpha = 0.0
            self.removeFromSuperview()
        }
    }
    
    func styleUI() {
        menuIcon.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0)
        fillUI()
    }
    
    
    func fillUI() {
        menuTitleLbl.text = item?.menuName
        menuDescLbl.text = item?.menuDescription
        foodTypeIcon.image = item?.menuType == .BBQVeg ? #imageLiteral(resourceName: "Group 846") : #imageLiteral(resourceName: "Group 845")
        if let image = item?.menuImageName, image != ""{
            UIView.animate(withDuration: 0.1, delay: 0, options: .curveLinear) {
                self.viewForImageView.isHidden = false
                self.layoutIfNeeded()
            } completion: { (isCompleted) in
                self.viewForImageView.isHidden = false
            }
            menuIcon.setImage(imageUrl: image)
        }
    }
    
    
    
    @IBAction func gestureRecognised(_ sender: Any) {
        self.removeToSuperView()
    }
    @IBAction func onSwipeUpGesture(_ sender: Any) {
//        if !viewForImageView.isHidden, menuTableView.frame.size.height < menuTableView.contentSize.height{
//            hideAndShow(isHidden: true)
//        }
    }
    
    
}
