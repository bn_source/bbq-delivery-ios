//
 //  Created by Nischitha on 14/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQOutletImageBottomSheet.swift
 //

import UIKit

class BBQOutletImageBottomSheet: UIViewController {

    var ambienceImages :[String]?
    var ambienceCollectionLayout = BottomSheetImageFlowLayout(cellsPerRow: 2)
    
    @IBOutlet weak var ambienceCollectionView: UICollectionView!
    @IBOutlet weak var outletImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
//        ambienceCollectionView.contentInset = UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
        ambienceCollectionView.collectionViewLayout = ambienceCollectionLayout;
        // Do any additional setup after loading the view.
    }
}

extension BBQOutletImageBottomSheet: UICollectionViewDelegate,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ambienceImages?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let ambienceCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ambienceCell", for: indexPath) as! BBQAmbienceCell
        let imageUrl = ambienceImages?[indexPath.row]
        LazyImageLoad.setImageOnImageViewFromURL(imageView: ambienceCell.ambienceImageView, url: imageUrl) { (image) in}
        ambienceCell.roundCorners(cornerRadius: 10, borderColor: .clear, borderWidth: 0)
        return ambienceCell
    }
  
}
