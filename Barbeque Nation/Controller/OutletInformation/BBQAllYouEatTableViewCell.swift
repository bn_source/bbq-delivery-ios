//
 //  Created by Arpana on 05/05/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified BBQAllYouEatTableViewCell.swift
 //

import UIKit

class BBQAllYouEatTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var itemAmountLbl: UILabel!
    @IBOutlet weak var itemNmLbl: UILabel!
    @IBOutlet weak var itemDetailsLbl: UILabel!
    @IBOutlet weak var menuImageView: UIImageView!
    @IBOutlet weak var itemTypeImageView : UIImageView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var spicinessImageView : UIImageView!
    var isBeverage: Bool = false
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        itemAmountLbl.setCornerRadius(5)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    // TODO: Move necessary methods to view model. Pass config class as constructor parameter instead of so many individual parsm.
    func configureCell(model : MenuModelDetails) {
        
        self.isBeverage = model.isBeverage
        
        if let imageStr = model.menuImageName, imageStr.count > 0 {
            menuImageView.setImage(url: imageStr, isForPromotions: false)
        } else {
            self.menuImageView.image = nil
        }
        
        
        if let tag = model.tagImage, tag.count > 0 {
            spicinessImageView.setImage(url: tag, isForPromotions: false)
        } else {
            self.spicinessImageView.image = nil
        }
        
        self.itemNmLbl.text = model.menuName
        self.itemDetailsLbl.text = model.menuDescription

        
        if isBeverage {
            itemTypeImageView.isHidden = true
            self.itemAmountLbl.isHidden = false
            
            if let priceValue = model.price, let currencyValue = model.currency, self.isBeverage ?? false {
                let currSymbol = currencyValue == "INR" ? "₹" : ""
                let formattedPrice = String(format: "%.0f", Double(priceValue) ?? 0.0)
                self.itemAmountLbl.text = "\(currSymbol)\(formattedPrice.toCurrencyFormat())"
                //  priceSeperator.isHidden = false
            }
        }else {
            itemTypeImageView.isHidden = false
            self.itemAmountLbl.isHidden = true
          //  priceSeperator.isHidden = true
        }
       
        if model.menuType == .BBQVeg {
            itemTypeImageView.image = UIImage(named: "vegIcon")
        } else if model.menuType == .BBQNonVeg {
            itemTypeImageView.image = UIImage(named: "nonvegIcon")
        } else {
            itemTypeImageView.image = UIImage()
        }
        
      //  self.detailsContainerView.detailsdescriptionTextView.text = desc.htmlToString
        
       // self.handleHiddings()
    }
    
   
    override func layoutSubviews() {
        super.layoutSubviews()
        
        mainView.setCornerRadius(10)
        mainView.setShadow(color: UIColor.hexStringToUIColor(hex: "55606D1A") , opacity: 0.2, offSet: .zero, radius: 7, scale: true)
        mainView.layer.masksToBounds = true
        
        //only right corners to imageview
        menuImageView.clipsToBounds = true
        menuImageView.layer.cornerRadius = 10
        menuImageView.layer.maskedCorners = [ .layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        
        
    }
}
