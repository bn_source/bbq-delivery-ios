//
 //  Created by Sakir on 07/06/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified BBQPricingDetailsCollectionViewCell.swift
 //

import UIKit

class BBQPricingDetailsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewForContainer: UIView!
    
    func setCellDate() {
        
        viewForContainer.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0)
        viewForContainer.setShadow(color: .lightGray, opacity: 0.5, offSet: .zero, radius: 10.0, scale: true)
    }
}

