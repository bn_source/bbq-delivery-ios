//
 //  Created by Abhijit on 10/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQOutletInfoViewController.swift
 //

import UIKit

class BBQOutletInfoViewController: BaseViewController {
     
    //MARK:-properties
    
    var bookingRedirectionBottomSheet=BookingRedirectionBottomSheet()
    var outletPhoneNumber:String?
    var promotionData :[PromotionModel]?
    var promosFlowLayout = BBQPromosFlowLayout()
    var menuBranchBuffetData :MenuBranchBuffetDataModel?
    var selectedBranchId: String?
    var bottomSheetController: BottomSheetController?

     //MARK:-IB outlets
    @IBOutlet weak var lblOutletName: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var viewForOutletInfo: UIView!
    @IBOutlet weak var viewForMenu: UIView!
    @IBOutlet weak var viewForAmbience: UIView!
    @IBOutlet weak var viewForOffers: UIView!
    @IBOutlet weak var viewForPricing: UIView!
    @IBOutlet weak var stackViewForOutletinfo: UIStackView!
    @IBOutlet weak var btnReserveTable: UIButton!
    @IBOutlet weak var lblOutetStatus: UILabel!
    @IBOutlet weak var lblOutletAddress: UILabel!
    @IBOutlet weak var referView: AppReferralView!
    lazy private var outletInfoViewModel : BBQOutletInfoViewModel = {
        let outletVM = BBQOutletInfoViewModel()
        return outletVM
    } ()
    
    //MARK:-Life cycle method
    override func viewDidLoad() {
        super.viewDidLoad()
        AnalyticsHelper.shared.triggerEvent(type: .OI01)
//        self.setupPromosCollectionFlowLayout()
        self.getOutletInformation()
//
//        BBQPulseAnimation.showPulseAnimation(inView: self.reserveTableButton,
//                                             presentingView: self.view)
//
//        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped))
//        mapsImage.isUserInteractionEnabled = true
//        mapsImage.addGestureRecognizer(tapGestureRecognizer)
//
//        outletChangeButton?.roundCorners(cornerRadius: (outletChangeButton?.frame.size.height)!/2, borderColor: .clear, borderWidth: 0.0)
//        outletChangeButton?.setAttributedTitle(setAttrubuttedTitleForButtonsWith(title: "  Change restaurant "),
//                                                     for: .normal)
//
        setUpOutletInfoUIFeilds()
        AnalyticsHelper.shared.triggerEvent(type: .reservation_menu)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.screenName(name: .Outlet_Info_Screen)
        if isAppReferAndEarnActivated == true {
            referView.isHidden = false
            referView.delegetToReferal = self
        } else {
            referView.isHidden = true

        }
        NotificationCenter.default.addObserver(self, selector: #selector(locationDataUpdated(_:)), name: Notification.Name(rawValue:Constants.NotificationName.locationDataUpdate), object:nil)
      //  addReferalView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(Constants.NotificationName.locationDataUpdate), object: nil)
    }
    
    // MARK: Data Fetch Methods
    
    private func getOutletInformation() {
        // Copied logics from view did load
        // TODO: Refactoring needed

//        BBQActivityIndicator.showIndicator( view: self.outletScrollView, addBackgroud: true)
        outletInfoViewModel.getOutletInfo(branchId: selectedBranchId ?? BBQUserDefaults.sharedInstance.branchIdValue ) {(result) in
//            BBQActivityIndicator.hideIndicator(from: self.outletScrollView)
            if result == true {
//                self.setUpOutletInfoUIFeilds()
               self.setUpDynamicUIFeilds()
//                self.promotionData = self.outletInfoViewModel.outletInfoData?.promotion
//                if let data = self.promotionData {
//                    self.promotionPageControl.numberOfPages = data.count
//                }
                if self.outletInfoViewModel.outletInfoData?.branchMenu?.count ?? 0 == 0{
                    self.menuBranchBuffetServiceCall()
                }
//                self.promotionCollectionView.reloadData()
//                self.promotionCollectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .right, animated: true)
            }
        }
    }
    
     // MARK:- IBAction Methods
    
    @IBAction func reserveTableButtonPressed(_ sender: UIButton) {
        openReservationController()
    }
    
    
    @IBAction func previousButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func amenitiesButton(_ sender: UIButton) {
        AnalyticsHelper.shared.triggerEvent(type: .OI05)
        if outletInfoViewModel.outletInfoData?.outletAmenities?.count ?? 0 > 0{
            let configuration = BottomSheetConfiguration(animationDuration: 0.4,
                                                         minimumTopSpacing: self.view.frame.size.height - 120)
            let bottomSheetController = BottomSheetController(configuration: configuration)
            let aminitiesController =  storyboard?.instantiateViewController(withIdentifier: "OutletAmenitiesViewController") as? OutletAmenitiesViewController
            guard let aminitiesController = aminitiesController else {
                ToastHandler.showToastWithMessage(message: kNoAminitiesAvailableText)
                return
            }
            aminitiesController.outletInfoViewModel = outletInfoViewModel
            //aminitiesController?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            aminitiesController.setupPresentingController(controller: bottomSheetController)
            bottomSheetController.present(aminitiesController, on: self)
        } else {
            ToastHandler.showToastWithMessage(message: kNoAminitiesAvailableText)
        }
    }
    
    @IBAction func changeOutletButtonPressed(_ sender: UIButton) {
        AnalyticsHelper.shared.triggerEvent(type: .OI03)
        let configuration = BottomSheetConfiguration(animationDuration: 0.4,
                                                     minimumTopSpacing: self.view.frame.size.height - 250.0)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        let viewController = BBQLocationViewController()
        viewController.location = BBQLocationManager.shared.currentLocationManager.location
        viewController.setupPresentingController(controller: bottomSheetController)
        bottomSheetController.present(viewController, on: self, viewHeight: UIScreen.main.bounds.height - 165)
    }
    
    @objc func locationDataUpdated(_ notification: Notification) {
        AnalyticsHelper.shared.triggerEvent(type: .OI03A)
        selectedBranchId = nil
        getOutletInformation()
        self.view.layoutIfNeeded()
    }

    
    //MARK:-location guesture action
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer){
        
    }
    
    @IBAction func callButton(_ sender: UIButton) {
        AnalyticsHelper.shared.triggerEvent(type: .OI04)
        callNumber(phone: outletPhoneNumber ?? "")
    }
    func callNumber(phone : String)
    {
        let phoneUrl = URL(string: "telprompt://\(phone.replacingOccurrences(of: " ", with: ""))")
        let phoneFallbackUrl = URL(string: "tel://\(phone.replacingOccurrences(of: " ", with: ""))")
        if(phoneUrl != nil && UIApplication.shared.canOpenURL(phoneUrl!)) {
            UIApplication.shared.open(phoneUrl!) { (success) in
                if(!success) {
                    // Show an error message: Failed opening the url
                }
            }
        } else if(phoneFallbackUrl != nil && UIApplication.shared.canOpenURL(phoneFallbackUrl!)) {
            UIApplication.shared.open(phoneFallbackUrl!) { (success) in
                if(!success) {
                }
            }
        } else {
        }
    }
    //MARK:-UI methods
    func setUpOutletInfoUIFeilds(){
        
        viewForMenu.setShadow(color: .lightGray, opacity: 0.2, offSet: .zero, radius: 10.0, scale: true)
        viewForAmbience.setShadow(color: .lightGray, opacity: 0.2, offSet: .zero, radius: 10.0, scale: true)
        viewForOffers.setShadow(color: .lightGray, opacity: 0.2, offSet: .zero, radius: 10.0, scale: true)
        stackViewForOutletinfo.setShadow(color: .lightGray, opacity: 0.2, offSet: .zero, radius: 10.0, scale: true)
        viewForPricing.setShadow(color: .lightGray, opacity: 0.2, offSet: .zero, radius: 10.0, scale: true)
        referView.setShadow(color: .lightGray, opacity: 0.2, offSet: .zero, radius: 10.0, scale: true)
        stackViewForOutletinfo.roundCorners(cornerRadius: 10, borderColor: .clear, borderWidth: 1)
        btnReserveTable.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0)
        viewForOutletInfo.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0)
    }
    func blurEffect(_ button: UIButton)
    {
        let blur = UIVisualEffectView(effect: UIBlurEffect(style:
            UIBlurEffect.Style.light))
        blur.frame = button.bounds
        blur.isUserInteractionEnabled = false
        button.addSubview(blur)
    }
    //MARK:-Ambience guesture action
    @IBAction func ambienceImageTapped(_ sender: UIButton) {
        AnalyticsHelper.shared.triggerEvent(type: .OI07)
        if outletInfoViewModel.outletInfoData?.branchGallery?.count ?? 0 > 0 {
            let bbqAmbienceVC = storyboard?.instantiateViewController(withIdentifier: "BBQAmbienceVC") as! BBQAmbienceVC
            bbqAmbienceVC.ambienceImages = outletInfoViewModel.outletInfoData?.branchGallery ?? [String]()
            self.present(bbqAmbienceVC, animated: true)
        }else{
            ToastHandler.showToastWithMessage(message: kNoAmbienceAvailableText)
        }
       
    }
    
    @IBAction func onClickBtnPricing(_ sender: Any) {
        //BBQPricingDetailsVC
        let bbqPrcingVC = UIStoryboard.loadOutletPricingViewController()
        self.navigationController?.pushViewController(bbqPrcingVC, animated: true)
    }
    
    @IBAction func onClickBtnOffers(_ sender: Any) {
        BBQActivityIndicator.showIndicator(view: self.view)
        let homeModel = HomeViewModel()
        homeModel.getPromosVouchers(latitide: 0.0, longitude: 0.0, brach_ID: BBQUserDefaults.sharedInstance.branchIdValue) { (isSuccess) in
            
            BBQActivityIndicator.hideIndicator(from: self.view)
            
            let promosArray = homeModel.getPromotionsDataData()
            let promoViewModel = PromosViewModel(promosDataArray: promosArray)
            
            let viewController = PromotionOfffersVC()
            viewController.promoDelegate = self as? BottomSheetImagesViewControllerProtocol
            viewController.promoScreenType = .Hamburger
            viewController.promosData = promoViewModel.promosArray
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    @IBAction func onClickBtnNavigation(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .OI06)
         let bottomSheetController = BottomSheetController()
        let latitude = outletInfoViewModel.outletInfoData?.latitude
        let longitude = outletInfoViewModel.outletInfoData?.longitude
        
        let directionBottomSheet = Bundle.main.loadNibNamed("BookingRedirectionBottomSheet", owner: nil)!.first as! BookingRedirectionBottomSheet
        if let latitude = latitude, let longitude = longitude {
            directionBottomSheet.latitude = latitude
            directionBottomSheet.longitude = longitude
        }
        directionBottomSheet.bottomSheetType = .Direction
        directionBottomSheet.view.maskByRoundingCorners(cornerRadius: 10, maskedCorners: CornerMask.topCornersMask)
        if(UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            directionBottomSheet.isGoogleMapsAvailable = true
        }
        bottomSheetController.present(directionBottomSheet, on: self)
    }
    //MARK:-Menu guesture action
    @IBAction func menuImageTapped(_ outlet: UIButton){
        if outletInfoViewModel.outletInfoData?.branchMenu?.count ?? 0 > 0, let branchMenuVC = UIStoryboard.loadOutletBranchMenuViewController() {
            branchMenuVC.menuImages = outletInfoViewModel.outletInfoData?.branchMenu ?? [String]()
            self.present(branchMenuVC, animated: true)
        }else if self.menuBranchBuffetData?.buffets?.buffetData?.count ?? 0 > 0{
             self.showMenuScreen()
        }else{
            ToastHandler.showToastWithMessage(message: kNoMenuAvailableText)
        }
    }
    //Mark:- MenuBranchBuffet ServiceCall
    func  menuBranchBuffetServiceCall(){
        let currentDate = getCurrentDate()
        outletInfoViewModel.getmenubranchBuffet(branchId: BBQUserDefaults.sharedInstance.branchIdValue, reservationDate: currentDate, slotId: 0) { (result) in
//            self.menuImage.image = UIImage(named: "MenuPlaceholder")

            if result == true{
                if let data = self.outletInfoViewModel.menuBranchBuffetData{
                    self.menuBranchBuffetData = data
//                    let menuImageData = self.menuBranchBuffetData?.buffets?.buffetData?[0].buffetImage
//                    LazyImageLoad.setImageOnImageViewFromURL(imageView: self.menuImage, url: menuImageData) { (image) in
//                        if image == nil {
//                            //if no image present show default placeholder
//                            self.menuImage.image = UIImage(named: "MenuPlaceholder")
//                        }
//                    }
                }
            }
        }
    }
    
    func showMenuScreen() {
        //Get instance of BottomSheet handler
        AnalyticsHelper.shared.triggerEvent(type: .OI02)
//        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: 150.0)
//        let bottomSheetController = BottomSheetController(configuration: configuration)
        //Create instance for the controller that will be shown
       // let viewController = BottomSheetMenuViewController()
        
        let orderDetailsVC = UIUtils.viewController(storyboardId: "OutletInformation", vcId: "BBQEatAllViewController") as! BBQEatAllViewController
        
        //viewController.dataSource = self
        if let branchBuffetData = self.menuBranchBuffetData, let buffet = branchBuffetData.buffets,
            let _ = buffet.buffetData {
            orderDetailsVC.buffetData = outletInfoViewModel.totalBuffetData()
            //viewController.buffetData = outletInfoViewModel.totalBuffetData()
        }
        
        //Present your controller over current controller
        self.present(orderDetailsVC, animated: true, completion: nil)
       // bottomSheetController.present(viewController, on: self)
    }
    
    func getCurrentDate() -> String{
      let todaysDate = NSDate()
      let dateFormatter = DateFormatter()
      dateFormatter.dateFormat = "yyyy-MM-dd"
      let DateInFormat = dateFormatter.string(from: todaysDate as Date)
       return DateInFormat
    }
    func setUpDynamicUIFeilds() {
//        address.text = outletInfoViewModel.outletInfoData?.address
        lblOutletName.text = outletInfoViewModel.outletInfoData?.name
        outletPhoneNumber = outletInfoViewModel.outletInfoData?.branch_phone
        lblOutletAddress.text = outletInfoViewModel.outletInfoData?.address
//        callButton.setTitle(outletPhoneNumber, for: .normal)
        lblPhoneNumber.text = outletPhoneNumber
//        let outletImage = outletInfoViewModel.outletInfoData?.branch_image
//        LazyImageLoad.setImageOnImageViewFromURL(imageView: outletBackgroundImage, url: outletImage) { (image) in
//        }
        if outletInfoViewModel.outletInfoData?.closed == "0"{
            lblOutetStatus.text = "Open"
            lblOutetStatus.textColor = .darkGreen
       }else{
            lblOutetStatus.text = "Closed"
            lblOutetStatus.textColor = .theme
        }
//        let lunchTime = outletInfoViewModel.outletInfoData?.lunchTime
//        let dinnerTime = outletInfoViewModel.outletInfoData?.dinnerTime
//        lunchTimingsLabel.text = lunchTime?.lowercased() ?? ""
//        dinnerTimingsLabel.text = dinnerTime?.lowercased() ?? ""
//
//        self.ambienceImage.image = UIImage(named: "AmbiencePlaceholder")
//        if let ambienceImageData = outletInfoViewModel.outletInfoData?.branchGallery{
//            if ambienceImageData.count > 0{
//                LazyImageLoad.setImageOnImageViewFromURL(imageView: ambienceImage, url: ambienceImageData[0]) { (image) in
//                    if image == nil {
//                        //if no image present show default placeholder
//                        self.ambienceImage.image = UIImage(named: "AmbiencePlaceholder")
//                    }
//                }
//            }
//        }
    }

    
    
}


extension BBQOutletInfoViewController:  ReferalDelegate{
    func shareReferalCode() {
        
        AnalyticsHelper.shared.triggerEvent(type: .RE09)

        if BBQUserDefaults.sharedInstance.isGuestUser == true {
            
            goToLoginPage()
            return

        }
        
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: 80)
        bottomSheetController = BottomSheetController(configuration: configuration)
        
       let vc = BBQShareReferalCodeViewController(nibName: "BBQShareReferalCodeViewController", bundle: nil)
        bottomSheetController?.present(vc, on: self, viewHeight: self.view.frame.size.height - (self.view.frame.size.height * (20/100)))

      
    }
}
private extension UIView {
    func addConstraintsWithFormat(_ format: String, views: UIView...) {
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDictionary))
    }
    
}
