//
 //  Created by Abhijit on 11/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified OutletAmenitiesViewController.swift
 //

import UIKit
import Alamofire
class OutletAmenitiesViewController: UIViewController {
    
    var outletInfoViewModel:BBQOutletInfoViewModel!
    @IBOutlet weak var amenitiesCollectionView: UICollectionView!
    @IBOutlet weak var moreInfoOfOutletTextLabel: UILabel!
    @IBOutlet weak var aminitiesViewHeightConstraint: NSLayoutConstraint!
    
    private var presentingController: BottomSheetController? = nil
    
    var amenitiesArray : [OutletAmenities]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        amenitiesCollectionView.delegate = self
        amenitiesCollectionView.dataSource = self
        moreInfoOfOutletTextLabel.text = kAminitiesTitle
       amenitiesArray = outletInfoViewModel?.outletInfoData?.outletAmenities
     //  height = height.rounded(.towardZero)
        
//        if aminitiesCount % 4 == 0
//        {
//            height = height - 1.0
//        }
        amenitiesCollectionView.reloadData {
            self.aminitiesViewHeightConstraint.constant = self.amenitiesCollectionView.contentSize.height
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            if let presentingController = self.presentingController {
                presentingController.chanceTheHeight(viewController: self, withHeight:  self.aminitiesViewHeightConstraint.constant+89)
            }
        }
        
    }

    @IBAction func onClickBtnClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
extension OutletAmenitiesViewController:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return amenitiesArray!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AmenitiesColletionViewCell", for: indexPath) as! OutletAmenitiesCollectionViewCell
        cell.amenityName.text = amenitiesArray![indexPath.row].name
        let url:URL = URL(string: (outletInfoViewModel.outletInfoData?.outletAmenities?[indexPath.row].image)!)!
        if let data = try? Data(contentsOf: url) {
            if let image = UIImage(data: data) {
                   cell.amenitiesImage.image = image
            }
        }
        return cell
        
    }
    
    func setupPresentingController(controller: BottomSheetController) {
        self.presentingController = controller
    }
  
}
