//
 //  Created by Nischitha on 19/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQAmbienceCell.swift
 //

import UIKit

class BBQAmbienceCell: UICollectionViewCell {
    
    @IBOutlet weak var ambienceImageView: UIImageView!
    
}
