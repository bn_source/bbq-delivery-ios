//
 //  Created by Sakir on 30/05/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified BBQBranchMenuCell.swift
 //

import UIKit

class BBQBranchMenuCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    var image: String = ""
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    var isZoomEventCalled = false
    
    func loadCellData(image: String, size: CGSize){
        self.image = image
        LazyImageLoad.setImageOnImageViewFromURL(imageView: imageView, url: image) { (image) in}
        imageView.contentMode = .scaleAspectFit
        widthConstraint.constant = size.width
        heightConstraint.constant = size.height
    }
}

extension BBQBranchMenuCell: UIScrollViewDelegate{
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        if !isZoomEventCalled{
            isZoomEventCalled = true
            AnalyticsHelper.shared.triggerEvent(type: .MI03, parameters: ["image_url": image])
        }
        return imageView
    }
}
