//
 //  Created by Mahmadsakir on 22/03/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified BBQAmbienceVC.swift
 //

import UIKit

class BBQAmbienceVC: UIViewController {
    
    var ambienceImages = [String]()
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblPageNumber: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AnalyticsHelper.shared.triggerEvent(type: .AI01)
        collectionView.reloadData()
        scrollViewDidScroll(collectionView)
    }
    
    @IBAction func onClickBtnBack(_ sender: Any) {
        self.dismiss(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension BBQAmbienceVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ambienceImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BBQAmbiencesCell", for: indexPath) as! BBQAmbiencesCell
        cell.loadCellData(image: ambienceImages[indexPath.row], size: collectionView.frame.size)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let number = Int(scrollView.contentOffset.x / scrollView.frame.size.width) + 1
        lblPageNumber.text = String(format: "Page %li/%li", number, ambienceImages.count)
        if number > 0, number <= ambienceImages.count{
            AnalyticsHelper.shared.triggerEvent(type: .AI02, parameters: ["image_url": ambienceImages[number-1]])
        }
    }
    
}
