//
 //  Created by Abhijit on 14/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified OutletAmenitiesCollectionViewCell.swift
 //

import UIKit

class OutletAmenitiesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var amenitiesImage: UIImageView!
    @IBOutlet weak var amenityName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
