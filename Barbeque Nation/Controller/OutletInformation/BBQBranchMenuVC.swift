//
 //  Created by Sakir on 30/05/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified BBQBranchMenuVC.swift
 //

import UIKit

class BBQBranchMenuVC: UIViewController {

    var menuImages = [String]()
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblPageNumber: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AnalyticsHelper.shared.triggerEvent(type: .MI01)
        collectionView.reloadData()
        scrollViewDidScroll(collectionView)
    }
    
    @IBAction func onClickBtnBack(_ sender: Any) {
        self.dismiss(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension BBQBranchMenuVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BBQBranchMenuCell", for: indexPath) as! BBQBranchMenuCell
        cell.loadCellData(image: menuImages[indexPath.row], size: collectionView.frame.size)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let number = Int(scrollView.contentOffset.x / scrollView.frame.size.width) + 1
        lblPageNumber.text = String(format: "Page %li/%li", number, menuImages.count)
        if number > 0, number <= menuImages.count{
            AnalyticsHelper.shared.triggerEvent(type: .MI02, parameters: ["image_url": menuImages[number-1]])
        }
        
    }
    
}
