//
//  BBQNoInternetViewController.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 05/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit

class BBQNoInternetViewController: UIViewController {

    @IBOutlet weak var connectionLabel: UILabel!
    @IBOutlet weak var connectionDescLabel: UILabel!
    @IBOutlet weak var locationButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        connectionLabel.text = kConnectionTitleMessage
        connectionDescLabel.text = kConnectionDescMessage
        
        locationButton.roundCorners(cornerRadius: locationButton.frame.size.height/2, borderColor: .clear, borderWidth: 0.0)
        if BBQUserDefaults.sharedInstance.CurrentSelectedLocation == "" {
            locationButton.setTitle(kSelectLocation, for: .normal)
        } else {
            locationButton.setTitle(BBQUserDefaults.sharedInstance.CurrentSelectedLocation, for: .normal)
        }
    }
}
