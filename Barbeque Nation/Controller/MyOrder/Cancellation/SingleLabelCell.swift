//
 //  Created by Rabindra L on 29/08/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified SingleLabelCell.swift
 //

import UIKit

class SingleLabelCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var detailedLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var checkBox: UIImageView!
    @IBOutlet weak var viewForContainer: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewForContainer.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension UILabel {
   
    func calculateMaxLines() -> Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let text = (self.text ?? "") as NSString
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font as Any], context: nil)
        let linesRoundedUp = Int(ceil(textSize.height/charSize))
        return linesRoundedUp
    }
    
}
