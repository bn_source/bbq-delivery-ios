//
 //  Created by Rabindra L on 29/08/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified CancellationButtonCell.swift
 //

import UIKit

class CancellationButtonCell: UITableViewCell {

    @IBOutlet weak var cancelButton: UIButton!
    
    let borderColor = UIColor(red:1.00, green:0.39, blue:0.00, alpha:1.0)
    let cornerRadius:CGFloat = 5
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.cancelButton.roundCorners(cornerRadius: cornerRadius,
                                       borderColor: borderColor,
                                       borderWidth: 1)
        self.cancelButton.setTitle(kProceedCancel, for: .normal)
    }
    
    func removeSeperatorLine() {
        self.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
        self.directionalLayoutMargins = .zero
    }
}
