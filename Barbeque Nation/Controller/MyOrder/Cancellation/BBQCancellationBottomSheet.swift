//
 //  Created by Rabindra L on 28/08/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQCancellationViewController.swift
 //

import UIKit

class BBQCancellationBottomSheet: UIViewController {
   
    
    let cancelBtnColor = UIColor(red:1.00, green:0.39, blue:0.00, alpha:1.0)
    let rescheduleBtnColor = UIColor(red:1.00, green:0.39, blue:0.00, alpha:1.0)
    
    @IBOutlet weak var rescheduleButton: UIButton!
    @IBOutlet weak var cancellationButton: UIButton!
    
    @IBOutlet weak var lblBookingId: UILabel!
    @IBOutlet weak var lblBookingAmount: UILabel!
    @IBOutlet weak var lblOutletLocation: UILabel!
    @IBOutlet weak var lblBookingDate: UILabel!
    @IBOutlet weak var lblBookingTime: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var cancellationTitleLabel: UILabel!
    @IBOutlet weak var rescheduleTextLabel: UILabel!
    @IBOutlet weak var viewForContainer: UIView!
    
    @IBOutlet weak var rescheduleTextLabelHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var rescheduleButtonHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var cancelButtonTopConstraint: NSLayoutConstraint!
    var superVC: UIViewController?
    
    var presentingBS : BottomSheetController?
    // TODO: Pass these properties through model
    var bookingInfo: String = ""
    var bookingID: String?
    var bookingAmount: String?
    var bookingDate: String?
    var bookingTime: String?
    var outletLocation: String?
    weak var cancellationDelegate: BBQCancellationControllerDelegate? = nil
    
    lazy private var afterDiningViewModel : BBQAfterDiningControllerViewModel = {
        let afterDiningVM = BBQAfterDiningControllerViewModel()
        return afterDiningVM
    } ()
    
    lazy var buffetsViewModel: BBQBookingBuffetsViewModel = {
        let buffetModel = BBQBookingBuffetsViewModel()
        return buffetModel
    }()
    
    // MARK: Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
    }
    
    func loadContentView(for advancePaid: Bool)  {
        if advancePaid {
            self.configureRescheduleSection()
        }
    }
    
    // MARK: Setup Methods
    
    func setupUI() {
        
        self.infoLabel.text = bookingInfo
        self.lblOutletLocation.text = "@" + (outletLocation ?? "")
        self.lblBookingId.text = bookingID
        self.lblBookingTime.text = bookingTime
        self.lblBookingDate.text = bookingDate
        self.lblBookingAmount.text = bookingAmount
        self.cancellationTitleLabel.text = kInfoCancelSorry
        //self.rescheduleTextLabel.text = kQuestionReschedule
        
        self.cancellationButton.setTitle(kProceedCancel, for: .normal)
        self.rescheduleButton.setTitle(kStringReschedule, for: .normal)

        self.cancellationButton.roundCorners(cornerRadius: 5.0,
                                             borderColor: cancelBtnColor,
                                             borderWidth: 0)
        self.rescheduleButton.roundCorners(cornerRadius: 5.0,
                                           borderColor: rescheduleBtnColor,
                                           borderWidth: 0)
        viewForContainer.roundCorners(cornerRadius: 10.0, borderColor: .lightGray, borderWidth: 1.0)
    }
    
    @IBAction func rescheduleButtonPressed(_ sender: UIButton) {
        self.showReschedulePopup()
    }
    
    @IBAction func cancellationButtonPressed(_ sender: UIButton) {
        
        let cancellationVC = UIStoryboard.loadBookingCancellation()
        cancellationVC.view.layoutIfNeeded()
        cancellationVC.delegate = self
        cancellationVC.bookingInfo = self.bookingInfo
        cancellationVC.presentingBS = self.presentingBS
        cancellationVC.bookingID = self.bookingID
        cancellationVC.view.maskByRoundingCorners(cornerRadius: 10.0,
                                                  maskedCorners: CornerMask.topCornersMask)
        
        self.setupCancellationBottomSheet(with: cancellationVC)
    }
    
    // MARK: Private Methods
    
    private func showReschedulePopup() {
        PopupHandler.showTwoButtonsPopup(title: kRescheduleResetAllTitle,
                                         message: kRescheduleResetAllDesc,
                                         on: self,
                                         firstButtonTitle: kNoButton,
                                         firstAction: { },
                                         secondButtonTitle: kYesButton) {
                                            self.resetAllForReschedule()
        }
    }
    
    private func configureRescheduleSection() {
        
        self.rescheduleButtonHeightConstraint.constant = 0
        self.rescheduleTextLabelHeightConstraint.constant = 0
        self.cancelButtonTopConstraint.constant = 16
        
        self.presentingBS?.expandOrCollapse(viewController: self, withHeight: 60, isExpand: false)
    }
    
    private func resetAllForReschedule() {
        BBQActivityIndicator.showIndicator(view: self.view)
        self.buffetsViewModel.rescheduleResetAll(bookingId: self.bookingID ?? "") { (isFetched) in
            BBQActivityIndicator.hideIndicator(from: self.view)
            if isFetched {
                NotificationCenter.default.post(name: Notification.Name(Constants.NotificationName.upadateHomeHeader), object: nil)
                self.initiateReschedule()
            }
        }
    }
    
    private func initiateReschedule() {
        BBQActivityIndicator.showIndicator(view: self.view)
        self.afterDiningViewModel.fetchAfterDiningPaymentData(for: self.bookingID ?? "") { (isFetched) in
            BBQActivityIndicator.hideIndicator(from: self.view)
            if isFetched {
                self.showRescheduleScreen()
            }
        }
    }
    
    private func showRescheduleScreen() {
        let bookingBSHeight = CGFloat(470.00) // As per UX
        let minimumTop = UIScreen.main.bounds.height - bookingBSHeight
        let configuration = BottomSheetConfiguration(animationDuration: 0.4,
                                                     minimumTopSpacing: minimumTop)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        let bookingVC = BBQRescheduleViewController()
        bookingVC.bookingBottomSheet = bottomSheetController
        bookingVC.afterDiningViewModel = self.afterDiningViewModel
        bookingVC.showCalendarInBooking = false
        bookingVC.superVC = superVC
        self.dismiss(animated: true) {
            self.superVC?.navigationController?.pushViewController(bookingVC, animated: true)
        }
    }
    
    private func setupCancellationBottomSheet(with viewController: BBQCancellationViewController) {
        
        BBQActivityIndicator.showIndicator(view: self.view)
        
        viewController.fetchCancellationReason { (tableHeight) in // Cancellation Reson
            viewController.fetchRefundOptions(completion: { (isFetched) in // Refund Options
                BBQActivityIndicator.hideIndicator(from: self.view)
                if isFetched {
                    self.addComponentToBottomSheet(viewController: viewController)
                    
                    let newHeight = (tableHeight + viewController.tableView.frame.minY) - self.view.frame.size.height + 40
                    if let bottomSheet = self.presentingBS {
                        bottomSheet.expandOrCollapse(viewController: self,
                                                     withHeight: CGFloat(newHeight),
                                                     isExpand: true)
                    }
                } else {
                    self.presentingBS?.dismissBottomSheet(on: self)
                }
            })
        }
    }
    
    private func addComponentToBottomSheet(viewController: BBQCancellationViewController) {
        for addedSubViews in self.view.subviews {
            addedSubViews.removeFromSuperview()
        }
        self.view.addSubview(viewController.subView)
        self.addChild(viewController)
        viewController.subView.pinEdgesToSuperView()
    }
}

extension BBQCancellationBottomSheet : BBQCancellationControllerDelegate {
   
    func cancellationDone(status: Bool) {
        self.cancellationDelegate?.cancellationDone(status: status)
    }
    
}
