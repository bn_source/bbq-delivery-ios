//
 //  Created by Rabindra L on 29/08/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified UserDefinedReasonCell.swift
 //

import UIKit

protocol UserDefinedReasonDelegate: AnyObject {
    func didBeginEditing()
    func didEndEditing()
    func textDidChange(text: String)
    func returnFromTextField()
}

class UserDefinedReasonCell: UITableViewCell, UITextViewDelegate {

    @IBOutlet weak var textField: UITextView!
    weak var delegate: UserDefinedReasonDelegate? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        textField.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
        textView.font = UIFont.appThemeRegularWith(size: 14.0)
        
        if let delegate = self.delegate {
            delegate.didBeginEditing()
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if let delegate = self.delegate {
            delegate.didEndEditing()
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if let delegate = self.delegate {
            delegate.textDidChange(text: textView.text)
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            if let delegate = self.delegate {
                delegate.returnFromTextField()
            }
            return false
        }
        
        return true
    }
}
