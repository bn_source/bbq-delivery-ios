//
 //  Created by Bhamidipati Kishore on 16/11/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQHeaderCell.swift
 //

import UIKit

class BBQHeaderCell: UITableViewHeaderFooterView {
    @IBOutlet weak var amountLabel:UILabel!
    @IBOutlet weak var headingLabel:UILabel!
    @IBOutlet weak var headerView:UIView!
}
