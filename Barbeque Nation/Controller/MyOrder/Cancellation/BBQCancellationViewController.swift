//
//  Created by Rabindra L on 29/08/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQCancellationViewController.swift
//

import UIKit

protocol BBQCancellationControllerDelegate: AnyObject {
    func cancellationDone(status: Bool)
}

enum tableSectionState: Int {
    case reasonList = 1, paymentMode, cancelButton
}

class BBQCancellationViewController: UIViewController {
    
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var subViewHeightConst: NSLayoutConstraint!
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var topBlackView: UIView!
    @IBOutlet weak var viewTopConst: NSLayoutConstraint!
    let orangeColor = #colorLiteral(red: 1, green: 0.3882352941, blue: 0.003921568627, alpha: 1)
    let blackColor = #colorLiteral(red: 0.2117647059, green: 0.2509803922, blue: 0.2666666667, alpha: 1)
    let cancellationVM = CancellationViewModel()
    var cancellationID = 0
    var refundType = ""
    var isKeyBoardOpened = false
    var keyboardHeight:CGFloat = CGFloat(Constants.values.keyboardHeight.hashValue)
    var reasonsList = [(Int, String)]()
    var bookingInfo:String = ""
    
    var presentingBS : BottomSheetController?
    var bookingID : String?
    private var isReasonSelected : Bool = false
    private var isRefundModeSelected : Bool = false
    
    weak var delegate: BBQCancellationControllerDelegate? = nil
    
    enum screenSize:CGFloat {
        case stepOne = 352
        case stepTwo = 550
        case stepThree = 628
        case stepThreeDirect = 450
    }
    
    enum extraCell:Int {
        case custom_and_empty_cell = 2
        case custom_or_button_cell = 1
    }
    
    enum position:CGFloat {
        case priceLabel_shift_left = 60.0
        case priceLabel_shift_right = 25.0
    }
    
    enum cellHeight:CGFloat {
        case reasonCell = 70
        case buttonCell = 100
        case refundCell = 69
    }
    
    var selectedReasonIndex:Int = -1
    var refundTypeIndex:Int = -1
    
    var currentState:tableSectionState = .reasonList {
        didSet {
            setScreenHeight()
        }
    }
    
    func setScreenHeight() {
        self.view.layoutIfNeeded()
        var tableHeaderHeight:CGFloat = 0.0
        if self.reasonsList.count > 0 && cancellationVM.refundTypeList.count == 0 {
            tableHeaderHeight = self.tableView.sectionHeaderHeight + 30 // Adjusting header height
        }else if cancellationVM.refundTypeList.count > 0 {
            tableHeaderHeight = self.tableView.sectionHeaderHeight + 60 // Adjusting header height
        }
        let expandedHeight  = (self.tableView.contentSize.height + tableHeaderHeight) -
            self.parent!.view.frame.height + 40
        self.expandCollapseBottomSheet(newHeight: expandedHeight, isExpand: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerCells()
        tableView.delegate = self
        tableView.dataSource = self
        
        topBlackView.roundCorners(cornerRadius: 3, borderColor: UIColor(red:54.00, green:64.0, blue:68.0, alpha:1.0), borderWidth: 0.1)
        self.subView.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        
        let tap = UITapGestureRecognizer(target: self, action: nil)
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if touch?.view == self.view {
            if isKeyBoardOpened {
                isKeyBoardOpened = false
                view.endEditing(true)
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    private func registerCells() {
        let singleLabelCell = UINib(nibName:Constants.CellIdentifiers.singleLabelCell, bundle:nil)
        self.tableView.register(singleLabelCell, forCellReuseIdentifier: Constants.CellIdentifiers.singleLabelCell)
        
        let userDefinedReasonCell = UINib(nibName:Constants.CellIdentifiers.userDefinedReasonCell, bundle:nil)
        self.tableView.register(userDefinedReasonCell, forCellReuseIdentifier: Constants.CellIdentifiers.userDefinedReasonCell)
        
        let cancelButtonCell = UINib(nibName:Constants.CellIdentifiers.cancelButtonCell, bundle:nil)
        self.tableView.register(cancelButtonCell, forCellReuseIdentifier: Constants.CellIdentifiers.cancelButtonCell)
        
        let refundTextCell = UINib(nibName:Constants.CellIdentifiers.BBQCancellationRefundTextCell, bundle:nil)
        self.tableView.register(refundTextCell, forCellReuseIdentifier: Constants.CellIdentifiers.BBQCancellationRefundTextCell)
    }
    
    @objc func cancelTapped() {
        BBQActivityIndicator.showIndicator(view: self.subView)
        AnalyticsHelper.shared.triggerEvent(type: .booking_refund_status)
        AnalyticsHelper.shared.triggerEvent(type: .RD04A)
        cancellationVM.cancelBooking(bookingID: self.bookingID ?? "",
                                     reasonID: cancellationID,
                                     refundType: self.refundType) { (isSuccess) in
                                        BBQActivityIndicator.hideIndicator(from: self.subView)
                                        if isSuccess {
                                            AnalyticsHelper.shared.triggerEvent(type: .RD04B)
                                            if let delegate = self.delegate {
                                                self.presentingBS?.dismissBottomSheet(on: self,
                                                                                      completion: { (isDismissed) in
                                                                                        delegate.cancellationDone(status: isSuccess)
                                                                                        
                                                })
                                            }
                                        }else{
                                            AnalyticsHelper.shared.triggerEvent(type: .RD04C)
                                        }
        }
    }
    
    // MARK: Data Fetch Methods
    // Calling this data fetch method from screen below reason list since we have to load bottom sheet
    // component with height of reason list adjusted table view.
    public func fetchCancellationReason(completion: @escaping (_ outcomeTableHeight: CGFloat)->())  {
        self.cancellationVM.getCancelReason() {_ in
            if let reasonList = self.cancellationVM.cancelReasonModel {
                for reason in reasonList {
                    if let reasonID = reason.id, let reasonString = reason.reason {
                        self.reasonsList.append((reasonID, reasonString))
                    }
                }
                self.tableView.reloadData {
                    self.infoLabel.text = self.bookingInfo
                    completion(self.tableView.contentSize.height)
                }
                self.view.layoutIfNeeded()
            }
        }
    }
    
    public func fetchRefundOptions(completion: @escaping (_ isFetched: Bool)->()) {
        self.cancellationVM.fetchRefundOptions(bookingID: self.bookingID ?? "") { (isFetched) in
            completion(isFetched)
        }
        
    }
}

extension BBQCancellationViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.cancellationVM.showRefundMode(isReasonSelected: self.isReasonSelected) ?  2 : 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            if self.isReasonSelected && !self.cancellationVM.showRefundMode(isReasonSelected: self.isReasonSelected) {
                return self.reasonsList.count + 1
            }
            return self.reasonsList.count
        case 1:
            var cellCount = cancellationVM.refundTypeList.count
            if self.isRefundModeSelected {
                cellCount += 2
            }
            return cellCount
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            if indexPath.row < self.reasonsList.count {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "BBQCancellationViewCell", for: indexPath) as? BBQCancellationViewCell else { return UITableViewCell() }
                cell.setSelectedCell(text: self.reasonsList[indexPath.row].1, indexPath: indexPath, selectedIndex: selectedReasonIndex)
                return cell
            } else {
                if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.cancelButtonCell) as? CancellationButtonCell {
                    cell.cancelButton.addTarget(self, action:#selector(cancelTapped), for: .touchUpInside)
                    cell.removeSeperatorLine()
                    return cell
                }
            }
        case 1:
            if indexPath.row < cancellationVM.refundTypeList.count {
                if let priceLabelCell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.singleLabelCell) as? SingleLabelCell {
                    priceLabelCell.label.text = cancellationVM.refundTypeList[indexPath.row].0
                    priceLabelCell.detailedLabel?.text = cancellationVM.refundTypeList[indexPath.row].1
                    priceLabelCell.priceLabel.text = cancellationVM.refundTypeList[indexPath.row].2
                    if indexPath.row == refundTypeIndex {
                        priceLabelCell.checkBox.isHidden = false
                        priceLabelCell.viewForContainer.backgroundColor = .lightThemeBackground
                    } else {
                        priceLabelCell.checkBox.isHidden = true
                        priceLabelCell.viewForContainer.backgroundColor = .lightBackground
                    }
                    if indexPath.row == 0 {
                        priceLabelCell.priceLabel.textColor = orangeColor
                    } else {
                        priceLabelCell.priceLabel.textColor = blackColor
                    }
                    priceLabelCell.selectionStyle = .none
                    return priceLabelCell
                }
            } else {
                if indexPath.row == cancellationVM.refundTypeList.count {
                    if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.BBQCancellationRefundTextCell) as? BBQCancellationRefundTextCell {
                        cell.refunText.text = kRefundText
                        cell.removeSeperatorLine()
                        return cell
                    }
                }
                if indexPath.row == cancellationVM.refundTypeList.count+1 {
                    if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.cancelButtonCell) as? CancellationButtonCell {
                        cell.cancelButton.addTarget(self, action:#selector(cancelTapped), for: .touchUpInside)
                        cell.removeSeperatorLine()
                        return cell
                    }
                }
            }
        default:
            break
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
//        if section == 0{
//            headerCell.headingLabel.text = kReasonForCancellation
//            headerCell.amountLabel.isHidden = true
//        }else
        if section == 1{
            let headerCell = Bundle.main.loadNibNamed(Constants.CellIdentifiers.BBQHeaderCell, owner: self, options: nil)?.first as! BBQHeaderCell
            headerCell.headingLabel.text = kModeOfRefund
            headerCell.amountLabel.isHidden = false
            headerCell.headerView.backgroundColor = UIColor.hexStringToUIColor(hex: "F7F7F7")
            headerCell.headingLabel.textColor = .darkText
            headerCell.headingLabel.font = UIFont.appThemeMediumWith(size: 14)
            headerCell.amountLabel.font = UIFont.appThemeMediumWith(size: 14)
            headerCell.amountLabel.textColor = .darkText
            headerCell.amountLabel.text = kAmount
            return headerCell
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 { return 57 }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            if indexPath.row == self.reasonsList.count {
                return cellHeight.buttonCell.rawValue
            } else {
                return cellHeight.reasonCell.rawValue
            }
        case 1:
            if indexPath.row == cancellationVM.refundTypeList.count {
                return cellHeight.refundCell.rawValue//refund text
            } else if indexPath.row == cancellationVM.refundTypeList.count+1 {
                return cellHeight.buttonCell.rawValue
            } else {
                return cellHeight.refundCell.rawValue
            }
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            self.didSelectedReason(index: indexPath.row)
        case 1:
            self.didSelectedRefundMode(index: indexPath.row)
        default:
            break
        }
        self.tableView.reloadData {
        }
    }
}

extension BBQCancellationViewController {
    
    // MARK: Private Methods
    
    private func didSelectedReason(index: Int) {
        if currentState == .reasonList {
            cancellationID = self.reasonsList[index].0
            currentState = .paymentMode
        }
        
        self.selectedReasonIndex = index
        self.isReasonSelected = true
        
        if self.cancellationVM.refundTypeList.count == 0 {
            // Enabling proceed with cancel even if refund options are not there.
            self.isRefundModeSelected = true
        }
        
        self.tableView.reloadData {
            self.currentState = .paymentMode
        }
    }
    
    private func didSelectedRefundMode(index: Int) {
        self.refundTypeIndex = index
        self.isRefundModeSelected = true
        self.refundType =  cancellationVM.refundMode(for: index).rawValue
        if currentState == .paymentMode {
            self.tableView.reloadData {
                self.currentState = .cancelButton
            }
        }
    }
    
    private func expandCollapseBottomSheet(newHeight: CGFloat, isExpand: Bool) {
        if let bottomSheet = self.presentingBS {
            bottomSheet.expandOrCollapse(viewController: self.parent!,
                                         withHeight: newHeight,
                                         isExpand: true)
        }
    }
}

extension UITableViewCell {
    enum dimention:CGFloat {
        case checkBox = 24
    }
    func checkMark(indexPath: IndexPath, selectedCell:Int) {
        switch indexPath.section {
        case 0:
            if indexPath.row == selectedCell {
                let sentImage = UIImage(named: Constants.AssetName.roundedTickOrange)
                let sentImageView = UIImageView(image: sentImage)
                sentImageView.frame = CGRect(x: 0, y: 0, width: dimention.checkBox.rawValue, height: dimention.checkBox.rawValue)
                self.accessoryView = sentImageView
            } else {
                self.accessoryView = nil
            }
            
        default:
            break
        }
    }
}
