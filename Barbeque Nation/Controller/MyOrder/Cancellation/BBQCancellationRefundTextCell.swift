//
 //  Created by Bhamidipati Kishore on 17/11/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQCancellationRefundTextCell.swift
 //

import UIKit

class BBQCancellationRefundTextCell: UITableViewCell {
    @IBOutlet weak var refunText:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func removeSeperatorLine() {
        self.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
        self.directionalLayoutMargins = .zero
    }
    
}
