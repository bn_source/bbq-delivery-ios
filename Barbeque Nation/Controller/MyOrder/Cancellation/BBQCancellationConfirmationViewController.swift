//
 //  Created by Rabindra L on 30/08/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQCancellationConfirmationViewController.swift
 //

import UIKit

class BBQCancellationConfirmationViewController: UIViewController {
    
    
    @IBOutlet weak var cancelSuccessMessageLabel: UILabel!
    @IBOutlet weak var cancelSuccessDescriptionLabel: UILabel!
    
    // MARK: Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
    }
    
    // MARK: Setup Methods
    
    func setupUI() {
        self.cancelSuccessMessageLabel.text = kTitleCancelConfirm
        self.cancelSuccessDescriptionLabel.text = kCancellationRefund//kInfoCancelConfirm
    }
}
