//
 //  Created by Sakir on 21/04/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified BBQCancellationViewCell.swift
 //

import UIKit

class BBQCancellationViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var viewForContainer: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        viewForContainer.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0.0)
    }
    
    func setSelectedCell(text: String, indexPath: IndexPath, selectedIndex: Int?) {
        if let selectedIndex = selectedIndex, indexPath.row == selectedIndex{
            imgView.isHidden = false
            viewForContainer.backgroundColor = .lightThemeBackground
            titleLabel.textColor = .theme
        }else{
            imgView.isHidden = true
            viewForContainer.backgroundColor = .lightBackground
            titleLabel.textColor = .black
        }
        titleLabel.text = text
    }
    

}
