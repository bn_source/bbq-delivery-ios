
/*
 *  Created by Nischitha on 06/09/19.
 *  Copyright © BARBEQUE NATION HOSPITALITY LIMITED 2019. All rights reserved.
 
 * CHANGE LOG
 * —------------------------------------------------------------------------------
 * ID          Bug ID      Author Name          Date                  Description
 * —------------------------------------------------------------------------------
 * 1            NA         Nischitha         06/09/19       Initial Version
 */

import UIKit

protocol HeaderViewDelegate: AnyObject {
    func toggleSection(header: UpcomingReservationCell, section: Int)
}

class UpcomingReservationCell: UITableViewHeaderFooterView {
    
    weak var delegate: HeaderViewDelegate?
    var section: Int = 0
    
    //MARK:- IBOutlet
    @IBOutlet weak var upcomingReservationBackgroundImage: UIImageView!
    @IBOutlet weak var upcomingReservationImage: UIImageView!
    @IBOutlet weak var bookingIdLabel: UILabel!
    @IBOutlet weak var upcomingRservationTextLabel: UILabel!
    @IBOutlet weak var dateAndTimeLabel: UILabel!
    @IBOutlet weak var eventTypeLabel: UILabel!
    @IBOutlet weak var branchLabel: UILabel!
    @IBOutlet weak var arrowImage: UIImageView!
    @IBOutlet weak var upcomingReservationView: UIView!
    
    //MARK:- Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupUpcomingReservationFeilds()
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapHeader)))
    }
    func setupUpcomingReservationFeilds(){
        upcomingRservationTextLabel.text = kUpcomingReservationTitle
    }
    @objc func didTapHeader() {
        if let delegate = self.delegate {
            delegate.toggleSection(header: self, section: section)
        }
    }
    
    
}
