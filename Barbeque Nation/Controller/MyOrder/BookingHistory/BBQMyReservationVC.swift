//
 //  Created by Sakir Sherasuya on 01/04/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQMyReservationViewController.swift
 //

import UIKit
import Foundation

import Razorpay
import ShimmerSwift

class BBQMyReservationVC:  BaseViewController {
    
    @objc func resheduleConfirmed(_ notification: Notification) {
        self.bookingHistory.removeAll()
        self.bookingHistoryTablePageNo = 1
        self.tablePageLimit = 0
        self.showShimmering()
        self.fetchBookingHistory(pageNo: String(self.bookingHistoryTablePageNo))
        self.myOrderTableView.reloadData()
    }
    
    //MARK:- IBOutlet
    @IBOutlet weak var shimmerSupportView: UIView!
    @IBOutlet weak var shimmerView: UIView!
    @IBOutlet weak var myOrderTableView: UITableView!
    
    @IBOutlet weak var responseStatusLabel: UILabel!
    
    @IBOutlet weak var myOrderTableTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var myOrderTableLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var stackViewForEmpty: UIStackView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    var selectedSegmentForStatus = 0
    var shimmerControl : ShimmeringView?
    var isShimmerRunning : Bool = false
    
    var isComingFromNotification = false
    //MARK:- Properties
    var tablePageLimit = 0
    var bookingHistoryTablePageNo = 1
    var bookingHistory : [BookingHistory] = []
    var isBookingDataFetched = false
    var isAdvanceAmountPaid = false
    var noOfBuffetLabels = 0
    var packs: [Int] = []
    var selectedIndex :IndexPath?
    lazy var bookingHistoryViewModel : BookingHistoryViewModel = {
        let bookingHistoryViewModel = BookingHistoryViewModel()
        return bookingHistoryViewModel
    }()
    var heightOfCell = 240.0
    var bottomSheetController: BottomSheetController?
    var bookingID :String?
    var isPaymentInitiated = false
    var bookingHistoryOpened = false
    var textToShare:String?
    var dateTimePlace:String?
    var cancellationBS : BBQCancellationBottomSheet?
    var isPullToRefreshClicked = false
    var isCancelled = false
    var isAnimationRequired = false
    
    
    lazy private var afterDiningViewModel : BBQAfterDiningControllerViewModel = {
        let afterDiningVM = BBQAfterDiningControllerViewModel()
        return afterDiningVM
    } ()
    
    lazy var buffetsViewModel: BBQBookingBuffetsViewModel = {
        let buffetModel = BBQBookingBuffetsViewModel()
        return buffetModel
    }()
    
    lazy var cartViewModel : CartViewModel = {
        let viewModel = CartViewModel()
        return viewModel
    }()
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(BBQMyReservationVC.handleRefresh(_:)), for: UIControl.Event.valueChanged)
        return refreshControl
    }()
    
    //MARK:- Button Actions
    @IBAction func reservationButtonTapped(_ sender: UIButton) {
        //        if isComingFromNotification == true {
        //            self.fetchBookingHistory(pageNo: String(bookingHistoryTablePageNo))
        //        }
        self.bookingHistory.removeAll()
        self.bookingHistoryTablePageNo = 1
        self.tablePageLimit = 0
        self.showShimmering()
        self.fetchBookingHistory(pageNo: String(self.bookingHistoryTablePageNo))
        self.myOrderTableLeadingConstraint.constant = 0
        self.myOrderTableTrailingConstraint.constant = 0
        isPullToRefreshClicked = false
        myOrderTableView.reloadData()
        self.responseStatusLabel.text = "You don’t have any upcoming reservations"
    }
    
    private func getUpdatedPoints() {
        if BBQUserDefaults.sharedInstance.accessToken != "" {
            self.cartViewModel.getLoyaltyPointsCount { (isSuccess) in
                if isSuccess {
                    if let remainingPointsModel = self.cartViewModel.getPointCountModel, let remainingPoints = remainingPointsModel.remainingPoints {
                        BBQUserDefaults.sharedInstance.UserLoyaltyPoints = remainingPoints
                    }
                }
            }
        }
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        isPullToRefreshClicked = true
        self.bookingHistory.removeAll()
        self.bookingHistoryTablePageNo = 1
        self.tablePageLimit = 0
        self.fetchBookingHistory(pageNo: String(self.bookingHistoryTablePageNo))
        
    }
    @IBAction func onClickBtnBookNow(_ sender: Any) {
        self.navigateToHomePage()
        if let tabBarController = navigationController?.viewControllers.first(where: {$0 is DeliveryTabBarController}) as? DeliveryTabBarController {
            tabBarController.selectedIndex = 0
        }
    }
    @IBAction func segmentControlValueChanged(_ sender: Any) {
        triggerTabBarEvent(isPagination: false)
        bookingHistoryTablePageNo = 1
        fetchBookingHistory(pageNo: String(bookingHistoryTablePageNo))
    }
    private func triggerTabBarEvent(isPagination: Bool){
        if isPagination{
            if segmentControl.selectedSegmentIndex == 0 {
                AnalyticsHelper.shared.triggerEvent(type: .RH07A)
            } else if segmentControl.selectedSegmentIndex == 0 {
                AnalyticsHelper.shared.triggerEvent(type: .RH08A)
            } else {
                AnalyticsHelper.shared.triggerEvent(type: .RH09A)
            }
        }else{
            if segmentControl.selectedSegmentIndex == 0 {
                AnalyticsHelper.shared.triggerEvent(type: .RH07)
            } else if segmentControl.selectedSegmentIndex == 0 {
                AnalyticsHelper.shared.triggerEvent(type: .RH08)
            } else {
                AnalyticsHelper.shared.triggerEvent(type: .RH09)
            }
        }
        
    }
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        AnalyticsHelper.shared.triggerEvent(type: .RH01)
        self.shimmerControl = ShimmeringView(frame: shimmerSupportView.bounds)
        self.showShimmering()
        
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: self.view.frame.size.height - 258.0)
        bottomSheetController = BottomSheetController(configuration: configuration)
        myOrderTableView.isHidden = true
        
        segmentControl.backgroundColor = UIColor.hexStringToUIColor(hex: "FEF1E4")
        
        segmentControl.tintColor =  UIColor.hexStringToUIColor(hex: "FEF1E4")
          // segmentedControl.backgroundColor = background
        
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.appThemeRegularWith(size: 12)], for: .normal)
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.appThemeSemiBoldWith(size: 12)], for: .selected)
        
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.hexStringToUIColor(hex: "F04B24")], for: .normal)
        
        segmentControl.selectedSegmentIndex = selectedSegmentForStatus
        fixBackgroundSegmentControl(segmentControl)
        self.myOrderTableView.addSubview(self.refreshControl)
        NotificationCenter.default.addObserver(self, selector: #selector(resheduleConfirmed(_:)), name: Notification.Name(rawValue:Constants.NotificationName.bookingConfirmed), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(bookingFlowCancelled(_:)), name: Notification.Name(rawValue:Constants.NotificationName.bookingFlowCancelled), object: nil)
    }
    
    
    
    func fixBackgroundSegmentControl( _ segmentControl: UISegmentedControl){
        if #available(iOS 13.0, *) {
            //just to be sure it is full loaded
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                for i in 0...(segmentControl.numberOfSegments-1)  {
                    let backgroundSegmentView = segmentControl.subviews[i]
                    //it is not enogh changing the background color. It has some kind of shadow layer
                    backgroundSegmentView.isHidden = true
                }
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.screenName(name: .My_Reservation_Screen)
        self.bookingHistoryTablePageNo = 1
        self.fetchBookingHistory(pageNo: String(self.bookingHistoryTablePageNo))
        self.getUpdatedPoints()
    }
    
    override func viewDidAppear(_ animated:Bool)
    {
        super.viewDidAppear(animated)
        //MARK: - Fix for cancelled reservations not reflected in the main screen
        if isComingFromNotification == true {
          //  self.voucherbuttonTapped(UIButton())
        }else {
            //self.fetchBookingHistory(pageNo: String(bookingHistoryTablePageNo))
        }
        //END
    }
    
    
    @objc func bookingFlowCancelled(_ notification: Notification){
        self.refreshForCancel()
    }
    
    // MARK: Data Fetch Methods
    private func fetchBookingHistory(pageNo: String) {
        // TODO: Refactoring needed
        if self.isPullToRefreshClicked == false{
            if !self.isShimmerRunning  {
                BBQActivityIndicator.showIndicator(view: self.view)
            }
            self.isPullToRefreshClicked = true
        }
        if tablePageLimit > 1{
            triggerTabBarEvent(isPagination: true)
        }
        self.bookingHistoryViewModel.getBookingHistory(status: segmentControl.selectedSegmentIndex, pageNo: pageNo) { (isSuccess) in
            if isSuccess {
                print("success")
                self.refreshControl.endRefreshing()
                BBQActivityIndicator.hideIndicator(from: self.view)
                self.stopShimmering()
                self.isBookingDataFetched = false
                self.stackViewForEmpty.isHidden = false
                if let data = self.bookingHistoryViewModel.getBookingHistoryDetailsData{
                    if data.count != 0 || self.bookingHistory.count != 0{
                        self.myOrderTableView.isHidden = false
                        self.responseStatusLabel.isHidden = true
                        self.stackViewForEmpty.isHidden = true
                        //removing pending status from Array
//                        for bookingData in data{
//                            self.bookingHistory.append(bookingData)
//                        }
//                        self.tablePageLimit = self.tablePageLimit +  (data.count - pendingBookingItemsCount)
                        
                        // TODO: Temporary fix to avoid duplicates in first page in voucher history.
                        if self.bookingHistoryTablePageNo > 1 {
                            self.bookingHistory.append(contentsOf: data)
                            self.tablePageLimit = self.tablePageLimit + data.count
                        } else {
                            self.bookingHistory = data
                            self.tablePageLimit = data.count
                        }
                    }else
                    {
                        BBQActivityIndicator.hideIndicator(from: self.view)
                        self.myOrderTableView.isHidden = true
                        self.responseStatusLabel.isHidden = false
                        self.responseStatusLabel.text = "You don’t have any upcoming reservations"
                    }
                }
                print(self.bookingHistory)
                
//                for (index,_)  in self.bookingHistory.enumerated()
//                {
//                    self.bookingHistory[index].cellExpand = false
//                    if  self.bookingHistory[index].bookingId == self.bookingID{
//                        self.bookingHistory[index].opened = true
//                        self.bookingHistory[index].cellExpand = true
//                        self.selectedIndex = IndexPath(row: index, section: 0)
//                        self.bookingHistory[index].isPaymentIntiated = self.isPaymentInitiated
//                    }
//                    else{
//                        self.bookingHistory[index].opened = false
//                        self.bookingHistory[index].cellExpand = false
//                        self.bookingHistory[index].isPaymentIntiated = false
//                    }
//                }
                self.myOrderTableView.reloadData()
                if self.bookingID != nil{
                    if let selectedIndex =  self.selectedIndex {
                        if !self.isCancelled{
                            self.myOrderTableView.scrollToRow(at: selectedIndex, at: .top, animated: true)
                        }
                        self.bookingID = nil
                        
                    }
                }
            }
            else {
                BBQActivityIndicator.hideIndicator(from: self.view)
                self.myOrderTableView.isHidden = true
                self.responseStatusLabel.isHidden = false
            }
            self.isPaymentInitiated = false
        }
        
        //MARK: - DEL-1624 Fix for duplicating and unsorted reservations.
        bookingHistory = Array(Set(bookingHistory))
        
        bookingHistory.sort()
        
        //MARK: - Fix for cancelled reservations not reflected in the main screen
 //       activeBookings.removeAll(where: {completedBookings.contains($0) || cancelledBookings.contains($0)})
        //END
        
        //END
    }

    func dateTimeConverter(section:Int)-> String{
        let dateAndTime =  bookingHistory[section].reservationDate ?? ""
        let dateAndTimeArray = dateAndTime.components(separatedBy: " ")
        let dateString = dateAndTimeArray[0]
        let timeString = dateAndTimeArray[1].components(separatedBy: ".")
        let originalTimeString = timeString[0]
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: dateString)
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "EEEE, dd MMM,"
        let convertedDate = dateformatter.string(from: date!)
        //converting time to 12 hr format
        let time = BBQBookingUtilies.shared.getDisplayTimeIn12HourFormat(originalTimeString)
        let convertedDateAndTime = convertedDate + " " + time
        return convertedDateAndTime
    }
    
    @IBAction func historyBackButtonClicked(_sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
   
}



    extension BBQMyReservationVC: UITableViewDelegate,UITableViewDataSource
    {
        func numberOfSections(in tableView: UITableView) -> Int {
                return 1
        }
        
        func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
            isPullToRefreshClicked = false
            
            if indexPath.row < (bookingHistoryViewModel.getTotalBookingItems ?? 0) - 1 {
                if indexPath.row == tablePageLimit - 1 && !isBookingDataFetched{
                    isBookingDataFetched = true
                    bookingHistoryTablePageNo = bookingHistoryTablePageNo + 1
                    self.fetchBookingHistory(pageNo: String(bookingHistoryTablePageNo))
                    self.perform(#selector(loadTable), with: nil, afterDelay: 1.0)
                }
            }
        }
        
        
        @objc func loadTable() {
            myOrderTableView.reloadData()
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return bookingHistory.count
        }
        
        // TODO: Refactor entire method, create viewmodel separately and draw logic from it.
        // Do data manipulation in cell itself using uypdate method.
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if bookingHistory.count > indexPath.row{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "BBQMyReservationCell", for: indexPath) as? BBQMyReservationCell else { return UITableViewCell() }
                if indexPath.row < bookingHistory.count {
                    cell.setBookingData(indexPath, bookingHistory[indexPath.row], delegate: self)
                }
                return cell
            }
            return UITableViewCell()
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            if bookingHistory.count > indexPath.row{
                let reservationDetailsVC = ReservationDetailsVC()
                guard let bookingId = bookingHistory[indexPath.row].bookingId else { return  }
                reservationDetailsVC.bookingID = bookingId
                self.navigationController?.pushViewController(reservationDetailsVC, animated: true)
            }
            
        }
        
        @objc private func setAnimationFalse(){
            DispatchQueue.main.async { [weak self] in
                self?.isAnimationRequired = false
            }
        }
        

        
       
        
        func refreshForCancel() {
            self.isCancelled = true
            self.bookingHistory.removeAll()
            self.bookingHistoryTablePageNo = 1
            self.tablePageLimit = 0
            self.showShimmering()
            self.fetchBookingHistory(pageNo: String(self.bookingHistoryTablePageNo))
            self.myOrderTableView.reloadData()
        }
    }
        
        

    
        
        

    extension BBQMyReservationVC{
        
        // MARK: Shimmer Methods
        
        private func showShimmering() {
            self.shimmerSupportView.isHidden = false
            
            if let shimmer = self.shimmerControl {
                self.shimmerSupportView.addSubview(shimmer)
                shimmer.contentView = shimmerView
                shimmer.shimmerAnimationOpacity = 0.2
                shimmer.shimmerSpeed = 500.00
                shimmer.isHidden = false
                shimmer.isShimmering = true
                self.isShimmerRunning = true
            }
        }
        
        private func stopShimmering() {
            self.shimmerSupportView.isHidden = true
            
            if let shimmer = self.shimmerControl {
                shimmer.isShimmering = false
                shimmer.isHidden = true
                self.isShimmerRunning = false
            }
        }
    }

    
    extension BBQMyReservationVC: BBQAfterDiningDelegate{
        func onPaymentSuceess() {
            self.bookingHistoryTablePageNo = 1
            self.fetchBookingHistory(pageNo: String(self.bookingHistoryTablePageNo))
        }
    }
extension BBQMyReservationVC: BBQMyReservationCellDelegate{
    func onClickBtnViewBill(booking: BookingHistory) {
        
    }
    
    func onClickBtnViewDetails(booking: BookingHistory) {
        
        
    }
    
    func onClickBtnPayNow(booking: BookingHistory) {
        
    }
    
    func onClickBtnReschedule(booking: BookingHistory) {
        
    }
    
    func onClickBtnCancel(booking: BookingHistory) {
        
    }
    
    func onClickBtnShare(booking: BookingHistory) {
        
    }
    
   
}

