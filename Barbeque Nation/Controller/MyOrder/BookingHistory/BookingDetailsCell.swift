/*
 *  Created by Nischitha on 06/09/19.
 *  Copyright © BARBEQUE NATION HOSPITALITY LIMITED 2019. All rights reserved.
 
 * CHANGE LOG
 * —------------------------------------------------------------------------------
 * ID          Bug ID      Author Name          Date                  Description
 * —------------------------------------------------------------------------------
 * 1            NA         Nischitha         06/09/19       Initial Version
 */

import UIKit

protocol BookingDetailsCellProtocol: AnyObject {
    func bookingHistoryButtonsPressed(type: ReservationButtonType)
}
class BookingDetailsCell: UITableViewCell {
    
    //MARK:- IBOutlet
    @IBOutlet weak var shadowLayer: UIView!
    @IBOutlet weak var bookingDetailsContainerView: UIView!
    @IBOutlet weak var bbqPointsRedeemedImage: UIImageView!
    
    @IBOutlet weak var estimatedTotalTextLabel: UILabel!
    @IBOutlet weak var bbqnPointsValueLabel: UILabel!
    @IBOutlet weak var bbqnpointsRedeemedTextLabel: UILabel!
    @IBOutlet weak var estimatedTotalValueLabel: UILabel!
    @IBOutlet weak var advanceAmountPaidTextLabel: UILabel!
    @IBOutlet weak var advanceAmountPaidValueLabel: UILabel!
    @IBOutlet weak var happinessCardLabel: UILabel!
    @IBOutlet weak var couponValueLabel: UILabel!
    @IBOutlet weak var couponLabel: UILabel!
    @IBOutlet weak var happinessValueLabel: UILabel!
    
    @IBOutlet weak var payButtonLabel: UIButton!
    @IBOutlet weak var resheduleButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var preInvoiceButton: UIButton!
    
    
    @IBOutlet weak var estimatedTotalStackHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var advancePayStackHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var smilesStackHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var payNowButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var directionStackViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var couponStackHeightConstrainty: NSLayoutConstraint!
    @IBOutlet weak var voucherStackHeightConstraint: NSLayoutConstraint!
    
    //MARK:- Properties
    weak var delegate: BookingDetailsCellProtocol?
    weak var paymentDelegate: BBQPaymentBottomSheetProtocol?
    weak var invoiceDelegate : BookingHistoryDetailsCellProtocol?
    var bookingID: String = ""
    
    //MARK:- Button Actions
    @IBAction func cabButton(_ sender: UIButton) {
        triggerButtonEvent(type: .Cab)
    }
    
    @IBAction func mapButton(_ sender: UIButton) {
        triggerButtonEvent(type: .Direction)
    }
    
    @IBAction func phoneButton(_ sender: UIButton) {
        triggerButtonEvent(type: .Phone)
    }
    
    @IBAction func shareButton(_ sender: UIButton) {
        triggerButtonEvent(type: .Share)
    }
    
    @IBAction func cancelButton(_ sender: UIButton) {
        triggerButtonEvent(type: .Cancel)
    }
    
    @IBAction func rescheduleButton(_ sender: UIButton) {
        triggerButtonEvent(type: .Reschedule)
    }
    
    @IBAction func preInvoiceButtonTapped(_ sender: Any) {
        self.invoiceDelegate?.invoiceButtonPressed()
    }
    @IBAction func payInAdvanceButtonTapped(_ sender: Any) {
        paymentDelegate?.proceedToPaymentPressed(bookingID: bookingID,
                                                 paymentAmount: Double(estimatedTotalValueLabel.text ?? "") ?? 0,
                                                 paymentRemarks: "Advance amount to be paid", controller: nil)
    }
    
    //MARK:- Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpBookingDetailsCell()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    //triggerButtonEvent upon button action will set the particular button pressed
    private func triggerButtonEvent(type: ReservationButtonType) {
        self.delegate?.bookingHistoryButtonsPressed(type: type)
    }
    func setUpBookingDetailsCell() {
        estimatedTotalTextLabel.text = kEstimatedTotal
        bbqnpointsRedeemedTextLabel.text = kBBQNPointsRedeemed
        advanceAmountPaidTextLabel.text = kAdvanceAmountPaid
        payButtonLabel.roundCorners(cornerRadius: 15.0, borderColor: .clear, borderWidth: 1.0)
        payNowButtonTopConstraint.constant = 30
        preInvoiceButton.isHidden = true

        //Shadow for left right and bottom
        bookingDetailsContainerView.layer.shadowColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.1).cgColor
        bookingDetailsContainerView.layer.shadowOffset = CGSize(width: 0, height: 7)
        bookingDetailsContainerView.layer.shadowOpacity = 0.4
        bookingDetailsContainerView.layer.shadowRadius = 5
        bookingDetailsContainerView.layer.masksToBounds = false
        bookingDetailsContainerView.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.bottomCornersMask)
        
        self.backgroundColor = .clear
        
    
    }
}
