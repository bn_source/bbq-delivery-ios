//
 //  Created by Mahmadsakir on 06/04/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQMyHappinesscardCell.swift
 //

import UIKit

class BBQMyHappinesscardCell: UITableViewCell {

    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var viewForData: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var viewForStatus: UIView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var btnGift: UIButton!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var  expireStackView : UIStackView!
    var voucher: Vouchers!
    var indexPath: IndexPath!
    var delegate: BBQMyHappinesscardCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setTheme()
    }
    
    private func setTheme(){
        viewForContainer.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
        viewForStatus.roundCorners(cornerRadius: 4, borderColor: .clear, borderWidth: 0.0)
        btnGift.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0.0)
        
        //only left corners to data view
        viewForData.clipsToBounds = true
        viewForData.layer.cornerRadius = 10
        viewForData.layer.maskedCorners = [ .layerMinXMinYCorner, .layerMinXMaxYCorner]
        
        
        //only right corners to imageview
        imgBackground.clipsToBounds = true
        imgBackground.layer.cornerRadius = 10
        imgBackground.layer.maskedCorners = [ .layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        
        self.setShadow()
        
        
    }
    
    func setCellData(voucher: Vouchers, indexPath: IndexPath) {
        viewForStatus.isHidden = false
        expireStackView.isHidden = true
        self.voucher = voucher
        self.indexPath = indexPath
        lblName.text = voucher.title
        lblAmount.text = getCurrency() + String(voucher.denomination ?? 0)
        
        let voucherStatus = getStatus()
        viewForStatus.backgroundColor = voucherStatus.1.withAlphaComponent(0.15)
        lblStatus.textColor = voucherStatus.1
        lblStatus.text = voucherStatus.0
        btnGift.isHidden = !voucherStatus.3
        
        if voucher.status == "Active" {
            
            //show expiery date time at different place
            //hide status view from its postition
            expireStackView.isHidden = false
            viewForStatus.isHidden = true
            lblDateTime.text = (voucher.validity ?? "")
        }
        
        LazyImageLoad.setImageOnImageViewFromURL(imageView: imgBackground, url: voucher.voucherImage) { (image) in
            if let image = image{
                
                self.imgBackground.image = UIImage.init(named: "trail")

                if !voucherStatus.2{
                    self.imgBackground.image = UIImage.init(named: "trail")!.grayscaleImage()
                }else if voucher.giftTo != nil, voucher.giftTo != ""{
                    self.imgBackground.image = UIImage.init(named: "trail")!.grayscaleImage()
                }
            }

        }
        
        if voucher.giftTo != nil, voucher.giftTo != ""{
            lblStatus.text = "Gifted to \(voucher.giftTo ?? "")"
            btnGift.isHidden = true
            viewForStatus.isHidden = false
        }
        if voucher.giftReceived != nil, voucher.giftReceived != ""{
            lblStatus.text = "Redeemed"
            lblStatus.textColor = .darkGreen
            viewForStatus.backgroundColor = UIColor.darkGreen.withAlphaComponent(0.15)
            btnGift.isHidden = true
            viewForStatus.isHidden = false
        }
    }

    @IBAction func onClickBtnGift(_ sender: Any) {
        delegate?.onClickGiftVoucher(indexPath: indexPath)
    }
    
    private func getStatus() -> (String?, UIColor, Bool, Bool){
        if voucher.status == "Active"{
            return ("Expires on \(voucher.validity ?? "")", .themeYello, true, true)
        }else if voucher.status == "Redeemed" || voucher.status == "Gifted"{
            return (voucher.status, .darkGreen, false, false)
        }else if voucher.status == "In-Active" || voucher.status == "Expired"{
            return (voucher.status, .darkGray, false, false)
        }
        return ("", .theme, false, false)
    }
    
}

protocol BBQMyHappinesscardCellDelegate{
    func onClickGiftVoucher(indexPath: IndexPath)
}
