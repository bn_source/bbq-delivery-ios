//
 //  Created by Nischitha on 13/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BookingHistoryDetailsCell.swift
 //

import UIKit

protocol BookingHistoryDetailsCellProtocol: AnyObject {
    func invoiceButtonPressed()
}
class BookingHistoryDetailsCell: UITableViewCell {

    weak var delegate: BookingHistoryDetailsCellProtocol?
    
    @IBOutlet weak var estimatedTotalValueLabel: UILabel!
    @IBOutlet weak var bbqnPointsRedeemedValueLabel: UILabel!
    @IBOutlet weak var estimatedTotalTextLabel: UILabel!
    @IBOutlet weak var bbqnPointsRedeemedTextLabel: UILabel!
    @IBOutlet weak var amountRefundStatusLabel: UILabel!
    @IBOutlet weak var bbqnPointsImage: UIImageView!
    @IBOutlet weak var advanceAmountValuelabel: UILabel!
    @IBOutlet weak var advanceAmountTextLabel: UILabel!
    @IBOutlet weak var happinessValueLabel: UILabel!
    @IBOutlet weak var couponValueLabel: UILabel!
    
    @IBOutlet weak var invoiceButton: UIButton!
    @IBOutlet weak var shadowLayer: UIView!
    
    @IBOutlet weak var invoiceButtonHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var refundStatusHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var estimatedStackHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var advancePayStackHeightCXonstraint: NSLayoutConstraint!
    @IBOutlet weak var voucherStackHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var couponStackheightConstyraint: NSLayoutConstraint!
    @IBOutlet weak var smilesStackHeightConstraint: NSLayoutConstraint!

    @IBAction func InvoiceButtonTapped(_ sender: UIButton) {
       self.delegate?.invoiceButtonPressed()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setUpUIFeilds()
       
    }
    func setUpUIFeilds()
    {
        bbqnPointsRedeemedTextLabel.text = kBBQNPointsRedeemed
        estimatedTotalTextLabel.text = kEstimatedTotal
        advanceAmountTextLabel.text = kAdvanceAmountPaid

        shadowLayer.layer.shadowColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.1).cgColor
        shadowLayer.layer.shadowOffset = CGSize(width: 0, height: 7)
        shadowLayer.layer.shadowOpacity = 0.4
        shadowLayer.layer.shadowRadius = 5
        shadowLayer.layer.masksToBounds = false
        shadowLayer.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.bottomCornersMask)
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

