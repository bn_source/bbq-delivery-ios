//
 //  Created by Mahmadsakir on 25/03/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQMyReservationCell.swift
 //

import UIKit

class BBQMyReservationCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var upcomingReservationLabel:UILabel!
    @IBOutlet weak var reservationDate:UILabel!
    @IBOutlet weak var reservationLocation:UILabel!
    @IBOutlet weak var btnPayNow: UIButton!
    @IBOutlet weak var btnViewBill: UIButton!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgGreen: UIImageView!
    
    var booking: BookingHistory!
    var delegate: BBQMyReservationCellDelegate!
    var indexPath: IndexPath!
    //@IBOutlet weak var reservationBackImageView:UIImageView!

    override func awakeFromNib() {
        containerView.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
        btnPayNow.roundCorners(cornerRadius: 5.0, borderColor: .clear, borderWidth: 0.0)
        btnViewBill.roundCorners(cornerRadius: 20.0, borderColor: .clear, borderWidth: 0.0)
        //btnPayNow.adjustImageAndTitleOffsets()
        btnViewBill.adjustImageAndTitleOffsets()
        self.setShadow()
       // self.reservationBackImageView.frame.size.width = self.frame.size.width
    }
    
    func setBookingData(_ indexPath: IndexPath, _ booking: BookingHistory, delegate: BBQMyReservationCellDelegate){
        self.booking = booking
        self.delegate = delegate
        upcomingReservationLabel.text = "#" + booking.bookingId!
        if let status = booking.bookingStatus, status != "" {
            self.reservationDate.text = dateTimeConverter(dateAndTime: booking.reservationDate!).date
            self.lblTime.text = dateTimeConverter(dateAndTime: booking.reservationDate!).time
        } else{
            self.reservationDate.text = booking.reservationDate!
        }
        self.reservationLocation.text = "@ " + booking.branchName!
        //btnPayNow.isHidden = !upcomingObj.showPayment
        self.lblAmount.text = String(format: "%@%.02f", getCurrency(), booking.getTotalAmount())
        let status = getStatus()
        btnPayNow.backgroundColor = status.1.withAlphaComponent(0.1)
        btnPayNow.setTitleColor(status.1, for: .normal)
        btnPayNow.setTitle(status.0, for: .normal)
        
        imgGreen.isHidden = true
        lblAmount.textColor = .text
        btnPayNow.isUserInteractionEnabled = false
        if booking.getStatus().0 == 1{
            btnPayNow.isHidden = false
            btnPayNow.isUserInteractionEnabled = true
        }else if booking.getStatus().0 == 2{
            imgGreen.isHidden = false
            lblAmount.textColor = .darkGreen
        }
        
        if booking.isBookingSettled(){
            btnPayNow.backgroundColor = UIColor(red: 5.0/255.0, green: 166.0/255.0, blue: 96.0/255.0, alpha: 0.1)
            btnPayNow.setTitleColor(UIColor(red: 5.0/255.0, green: 166.0/255.0, blue: 96.0/255.0, alpha: 1.0), for: .normal)
            btnPayNow.setTitle("Paid", for: .normal)
            imgGreen.isHidden = false
            lblAmount.textColor = .darkGreen
        }
    }
    
    func getTheAttributedString(_ inputString:String,upadteString:String)->NSMutableAttributedString{
        let longString = inputString
        let longestWord = upadteString
        let longestWordRange = (longString as NSString).range(of: longestWord)
        let attributedString1 = NSMutableAttributedString(string:longString)
        attributedString1.addAttribute(NSAttributedString.Key.font, value: UIFont.appThemeLightWith(size: 14.0) as Any, range: longestWordRange)
        return attributedString1
    }
    
    func dateTimeConverter(dateAndTime: String)-> (date: String, time: String){
        let dateAndTimeArray = dateAndTime.components(separatedBy: " ")
        let dateString = dateAndTimeArray[0]
        let timeString = dateAndTimeArray[1].components(separatedBy: ".")
        let originalTimeString = timeString[0]
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: dateString)
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "dd MMM, EEEE"
        let convertedDate = dateformatter.string(from: date!)
        //converting time to 12 hr format

        let time = BBQBookingUtilies.shared.getDisplayTimeIn12HourFormat(originalTimeString)
        return (convertedDate, time.uppercased())
    }
    
    private func getStatus() -> (String, UIColor) {
        if booking.bookingStatus == "CONFIRMED" || booking.bookingStatus == "EXPECTED" || booking.bookingStatus == "Rescheduled" || booking.bookingStatus == "UPDATE RESERVATION"{
            return ("Upcoming", UIColor(red: 255.0/255.0, green: 136.0/255.0, blue: 0.0/255.0, alpha: 1.0))
        }else if booking.bookingStatus == "ARRIVED" || booking.bookingStatus == "SEATED"{
            return ("On Going", UIColor(red: 255.0/255.0, green: 136.0/255.0, blue: 0.0/255.0, alpha: 1.0))
        }else if booking.bookingStatus == "PRE_RECEIPT"{
            return ("Unpaid", UIColor.red)
        }else if booking.bookingStatus == "SETTLED"{
            return ("Paid", UIColor(red: 5.0/255.0, green: 166.0/255.0, blue: 96.0/255.0, alpha: 1.0))
        }else if booking.bookingStatus == "CANCELLED" || booking.bookingStatus == "NO SHOW"{
            return ("Cancelled", UIColor(red: 60.0/255.0, green: 72.0/255.0, blue: 88.0/255.0, alpha: 1.0))
        }else{
            return (booking.bookingStatus ?? "", UIColor(red: 255.0/255.0, green: 136.0/255.0, blue: 0.0/255.0, alpha: 1.0))
        }
    }
    
    
    
    
   
    @IBAction func onClickBtnPayNow(_ sender: Any) {
        delegate.onClickBtnViewDetails(booking: booking)
    }
    @IBAction func onClickBtnViewBill(_ sender: Any) {
        delegate.onClickBtnViewBill(booking: booking)
    }
    
}

protocol BBQMyReservationCellDelegate {
    func onClickBtnViewBill(booking: BookingHistory)
    func onClickBtnViewDetails(booking: BookingHistory)
    func onClickBtnPayNow(booking: BookingHistory)
    func onClickBtnReschedule(booking: BookingHistory)
    func onClickBtnCancel(booking: BookingHistory)
    func onClickBtnShare(booking: BookingHistory)
}
