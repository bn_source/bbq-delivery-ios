//
 //  Created by Mahmadsakir on 25/03/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQMyReservationCell.swift
 //

import UIKit

class BBQMyReservationCell: UITableViewCell {
    @IBOutlet weak var viewForContainer: UIView!
    @IBOutlet weak var lblBookingId: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblBookingDate: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var imgIndicator: UIImageView!
    @IBOutlet weak var lblLiner: UILabel!
    @IBOutlet weak var stackviewForItems: UIStackView!
    @IBOutlet weak var StackViewForDetails: UIStackView!
    @IBOutlet weak var stackViewForSmile: UIStackView!
    @IBOutlet weak var lblSmileUsed: UILabel!
    @IBOutlet weak var stackViewForVoucher: UIStackView!
    @IBOutlet weak var lblVoucherUsed: UILabel!
    @IBOutlet weak var stackViewForCoupon: UIStackView!
    @IBOutlet weak var lblCouponUsed: UILabel!
    @IBOutlet weak var stackViewForAdvancePayment: UIStackView!
    @IBOutlet weak var lblAdvanceAmount: UILabel!
    @IBOutlet weak var stackViewForTotal: UIStackView!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblCouponCode: UILabel!
    @IBOutlet weak var lblVoucherCode: UILabel!
    @IBOutlet weak var lblTotalBillAmount: UILabel!
    @IBOutlet weak var stackViewForTotalBill: UIStackView!
    @IBOutlet weak var btnBill: UIButton!
    @IBOutlet weak var btnPayNow: UIButton!
    @IBOutlet weak var stackviewForReschedule: UIStackView!
    @IBOutlet weak var stackviewForCancel: UIStackView!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var stackViewForOption: UIStackView!
    @IBOutlet weak var lblRefundedAmount: UILabel!
    @IBOutlet weak var lblRefundedSmile: UILabel!
    @IBOutlet weak var stackviewRefundedSmile: UIStackView!
    @IBOutlet weak var stackviewRefundedAmount: UIStackView!
    @IBOutlet weak var btnReschedule: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    
    var booking: BookingHistory!
    var indexPath: IndexPath!
    var delegate: BBQMyReservationCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        btnPayNow.roundCorners(cornerRadius: 20, borderColor: .clear, borderWidth: 0)
        btnBill.roundCorners(cornerRadius: 20, borderColor: .theme, borderWidth: 1)
        btnPayNow.adjustImageAndTitleOffsets()
        btnBill.adjustImageAndTitleOffsets()
        btnShare.adjustImageAndTitleOffsets()
        btnCancel.adjustImageAndTitleOffsets()
        btnReschedule.adjustImageAndTitleOffsets()
    }

    func setCellData(booking: BookingHistory, indexPath: IndexPath, isSelected: Bool){
        self.booking = booking
        self.indexPath = indexPath
        viewForContainer.dropShadow()
        viewForContainer.roundCorners(cornerRadius: 10.0, borderColor: .clear, borderWidth: 0.0)
        lblBookingId.text = String(format: "#%@", booking.bookingId ?? "")
        lblBookingDate.text = booking.reservationDate ?? ""
        lblLocation.text = booking.branchName
        let status = getStatus()
        lblStatus.backgroundColor = status.1.withAlphaComponent(0.1)
        lblStatus.textColor = status.1
        lblStatus.text = status.0
        
        if booking.bookingStatus == "CANCELLED"{
            viewForContainer.alpha = 0.7
        }else{
            viewForContainer.alpha = 1.0
        }
        
        setHideUnhideView(isSelected: isSelected)
    }
    
    func setAnimateCellView(isSelected: Bool, delay: TimeInterval) {
        setHideUnhideView(isSelected: isSelected, isAnimated: true, delay: delay)
    }
    
    func setHideUnhideView(isSelected: Bool, isAnimated: Bool = false, delay: TimeInterval = 0.0){
        if isSelected{
            imgIndicator.image = UIImage(named: "black_up_arrow")
            for subview in stackviewForItems.arrangedSubviews{
                subview.removeFromSuperview()
            }
            for objBooking in booking.bookingDetails ?? [BookingsDetails](){
                weak var itemView = BBQMyReservationItem().initWith()
                itemView?.setData(title: (objBooking.name ?? "") + " x " + String(objBooking.packs ?? 0), value: nil, imageUrl: nil, image: objBooking.type == "NONVEG" ? .nonVeg : objBooking.type == "VEG" ? .veg : nil , imageHeight: 12)
                if let itemView = itemView{
                    stackviewForItems.addArrangedSubview(itemView)
                }
            }
            setCellData()
        }else{
            imgIndicator.image = UIImage(named: "black_down_arrow")
        }
        if isAnimated{
            if isSelected{
                UIView.animate(withDuration: 0.2, delay: delay) {
                    self.StackViewForDetails.isHidden = !isSelected
                    self.layoutIfNeeded()
                }completion: { (isCompleted) in
                    self.StackViewForDetails.isHidden = !isSelected
                    self.layoutIfNeeded()
                }
            }else{
                UIView.animate(withDuration: 0.2) {
                    self.StackViewForDetails.isHidden = !isSelected
                    self.layoutIfNeeded()
                }completion: { (isCompleted) in
                    self.StackViewForDetails.isHidden = !isSelected
                    self.layoutIfNeeded()
                }
            }
        }else{
            StackViewForDetails.isHidden = !isSelected
            self.layoutIfNeeded()
        }
    }
    
    private func setCellData(){
        if booking.bookingStatus == "CANCELLED" ||  booking.bookingStatus == "NO SHOW"{
            setCancelledData()
        }else if booking.bookingStatus == "SETTLED"{
            setSettledBookingData()
        }else if booking.bookingStatus == "PRE_RECEIPT" || booking.bookingStatus == "SEATED" || booking.bookingStatus == "ARRIVED"{
            setArrivedData()
        }else{
            setUpcomingBookingData()
        }
    }
    
    private func setCancelledData(){
        stackViewForOption.isHidden = true
        stackViewForCoupon.isHidden = true
        stackViewForSmile.isHidden = true
        stackViewForVoucher.isHidden = true
        stackViewForTotal.isHidden = true
        btnPayNow.isHidden = true
        btnBill.isHidden = true
        stackViewForAdvancePayment.isHidden = true
        
        
        let totalAmount = booking.getTotalAmount()
        if totalAmount > 0 {
            stackViewForTotalBill.isHidden = false
            lblTotalBillAmount.text =  String(format: "%@%.0lf", getCurrency(), totalAmount)
        }else{
            stackViewForTotalBill.isHidden = true
        }
        
        if let smileRefunded = booking.loyaltyPointsRefund, let points = Int(smileRefunded), points > 0{
            stackviewRefundedSmile.isHidden = false
            lblRefundedSmile.text = "+" + smileRefunded
        }else{
            stackviewRefundedSmile.isHidden = true
        }
        
        if let amountRefund = booking.payment?.amountRefund, amountRefund > 0{
            stackviewRefundedAmount.isHidden = false
            lblRefundedAmount.text = String(format: "+%@%.02f", getCurrency(), amountRefund)
        }else{
            stackviewRefundedAmount.isHidden = true
        }
        
    }
    
    private func setSettledBookingData(){
        setArrivedData()
        btnPayNow.isHidden = true
        btnBill.isHidden = true
        if booking.billLink != nil{
            btnBill.isHidden = false
        }
    }
    
    private func setArrivedData(){
        setUpcomingBookingData()
        stackViewForOption.isHidden = true
    }

    private func setUpcomingBookingData(){
        stackviewRefundedAmount.isHidden = true
        stackviewRefundedSmile.isHidden = true
        stackViewForOption.isHidden = false
        
        if let estimatedAmount:Double = booking.billTotal{
            lblTotalAmount.text = String(format: "%@%.0lf", getCurrency(), round(estimatedAmount))
        }
        if let advancePaid = booking.advanceAmount{
            stackViewForAdvancePayment.isHidden = advancePaid > 0 ? false : true
            lblAdvanceAmount.text = String(format: "-%@%.0lf", getCurrency(), round(advancePaid))
        }
        if let smileUsed = booking.bbqnPointsRedeemed{
            stackViewForSmile.isHidden = smileUsed > 0 ? false : true
            lblSmileUsed.text = String(format: "-%@%li", getCurrency(), smileUsed)
        }
        
        let totalAmount = booking.getTotalAmount()
        if totalAmount > 0 {
            stackViewForTotalBill.isHidden = false
            lblTotalBillAmount.text =  String(format: "%@%.0lf", getCurrency(), totalAmount)
        }else{
            stackViewForTotalBill.isHidden = true
        }
        if let vouchers = booking.couponsAndVouchers{
            var voucherAmount = ""
            var couponAmount = ""
            var code: String = "Coupons Applied"
            var voucherCode: String = "Vouchers Applied"
            for voucher in vouchers {
                if voucher.voucherType == "GV"{
                    voucherCode += "\n" + (voucher.voucherCode ?? "")
                    voucherAmount +=  String(format: "\n-%@%.0lf", getCurrency(), voucher.amount ?? 0)
                }else{
                    code += "\n" + (voucher.voucherCode ?? "")
                    couponAmount += String(format: "\n-%@%.0lf", getCurrency(), voucher.amount ?? 0)
                }
            }
            stackViewForVoucher.isHidden = voucherAmount.count > 0 ? false : true
            stackViewForCoupon.isHidden = couponAmount.count > 0 ? false : true
            lblVoucherUsed.text = voucherAmount
            lblCouponUsed.text =  couponAmount
            lblCouponCode.text = code
            lblVoucherCode.text = voucherCode
        }
        btnPayNow.isHidden = !(booking.showPayment ?? true)
        btnBill.isHidden = (booking.billLink ?? "").count > 1 ? false : true
    }
    
    func getStatus() -> (String, UIColor) {
        if booking.bookingStatus == "CONFIRMED" || booking.bookingStatus == "EXPECTED" || booking.bookingStatus == "Rescheduled" || booking.bookingStatus == "UPDATE RESERVATION"{
            return ("UPCOMING", UIColor(red: 255.0/255.0, green: 136.0/255.0, blue: 0.0/255.0, alpha: 1.0))
        }else if booking.bookingStatus == "ARRIVED" || booking.bookingStatus == "SEATED"{
            return ("ON GOING", UIColor(red: 255.0/255.0, green: 136.0/255.0, blue: 0.0/255.0, alpha: 1.0))
        }else if booking.bookingStatus == "PRE_RECEIPT"{
            return ("PENDING", UIColor.red)
        }else if booking.bookingStatus == "SETTLED"{
            return ("COMPLETED", UIColor(red: 5.0/255.0, green: 166.0/255.0, blue: 96.0/255.0, alpha: 1.0))
        }else if booking.bookingStatus == "CANCELLED" || booking.bookingStatus == "NO SHOW"{
            return ("CANCELLED", UIColor(red: 60.0/255.0, green: 72.0/255.0, blue: 88.0/255.0, alpha: 1.0))
        }else{
            return (booking.bookingStatus ?? "", UIColor(red: 255.0/255.0, green: 136.0/255.0, blue: 0.0/255.0, alpha: 1.0))
        }
    }
    
    @IBAction func onClickBtnViewBill(_ sender: Any) {
        delegate?.onClickBtnViewBill(indexPath: indexPath, booking: booking)
    }
    
    @IBAction func onClickBtnPayNow(_ sender: Any) {
        delegate?.onClickBtnPayNow(indexPath: indexPath, booking: booking)
    }
    
    @IBAction func onClickBtnReschedule(_ sender: Any) {
        delegate?.onClickBtnReschedule(indexPath: indexPath, booking: booking)
    }
    
    @IBAction func onClickBtnCancel(_ sender: Any) {
        delegate?.onClickBtnCancel(indexPath: indexPath, booking: booking)
    }
    
    @IBAction func onClickBtnShare(_ sender: Any) {
        delegate?.onClickBtnShare(indexPath: indexPath, booking: booking)
    }
}

protocol BBQMyReservationCellDelegate {
    func onClickBtnViewBill(indexPath: IndexPath, booking: BookingHistory)
    func onClickBtnPayNow(indexPath: IndexPath, booking: BookingHistory)
    func onClickBtnReschedule(indexPath: IndexPath, booking: BookingHistory)
    func onClickBtnCancel(indexPath: IndexPath, booking: BookingHistory)
    func onClickBtnShare(indexPath: IndexPath, booking: BookingHistory)
}
