//
 //  Created by Mahmadsakir on 07/04/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified BBQHappinesscardDetailsVC.swift
 //

import UIKit

class BBQHappinesscardDetailsVC: BaseViewController {
   // @IBOutlet weak var imgGiftCard: UIImageView!
    @IBOutlet weak var stackViewForMessage: UIStackView!
    @IBOutlet weak var imgMessage: UIImageView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var stackViewForVoucherCode: UIStackView!
    @IBOutlet weak var lblVoucherCode: UILabel!
    @IBOutlet weak var lblVoucherExpiryDate: UILabel!
    @IBOutlet weak var stackViewForTerms: UIStackView!
    @IBOutlet weak var imgGiftImage: UIImageView!
    @IBOutlet weak var lblGiftName: UILabel!
    @IBOutlet weak var lblGiftDate: UILabel!
    @IBOutlet weak var viewForGift: UIView!
    @IBOutlet weak var lblGiftedBy: UILabel!
    @IBOutlet weak var viewForVoucherCode: CustomDashedView!
    @IBOutlet weak var btnGiftNow: UIButton!
    @IBOutlet weak var stackViewForExpiryDate: UIStackView!
    @IBOutlet weak var imgTerms: UIImageView!
    @IBOutlet weak var btnTerms: UIButton!
    @IBOutlet weak var lblTerms: UILabel!
    @IBOutlet weak var referView: AppReferralView!
    var voucher: Vouchers!
    var delegate: BBQHappinesscardDetailsVCDelegate?
    var bottomSheetController: BottomSheetController?

    override func viewDidLoad() {
        super.viewDidLoad()
        AnalyticsHelper.shared.triggerEvent(type: .HD01)
        setTheme()
        setData()
    }
    
    private func setTheme() {
        btnGiftNow.roundCorners(cornerRadius: 8.0, borderColor: .theme, borderWidth: 1.0)
        if isAppReferAndEarnActivated == true {
            referView.isHidden = false
            referView.delegetToReferal = self
            referView.setShadow(color: .lightGray, opacity: 0.2, offSet: .zero, radius: 10.0, scale: true)
        }else{
            referView.isHidden = true
        }
       // viewForGift.setShadow(color: .lightGray, opacity: 0.2, offSet: .zero, radius: 10.0, scale: true)


    }
    
    private func setData() {
        let responseData = getStatus()
//        LazyImageLoad.setImageOnImageViewFromURL(imageView: imgGiftCard, url: voucher.voucherImage) { (image) in
//            if !responseData.isActive{
//                self.imgGiftCard.image = image?.grayscaleImage()
//            }
//        }
        
        lblTitle.text = String(format: "%@ - %@%li", voucher.title ?? "" , getCurrency(), voucher.denomination ?? 0)
        
        stackViewForVoucherCode.isHidden = true
        stackViewForExpiryDate.isHidden = true
        stackViewForTerms.isHidden = true
        if responseData.isActive && !responseData.isRedeemed{
            stackViewForVoucherCode.isHidden = false
            lblVoucherCode.text = voucher.barCode
            
            stackViewForExpiryDate.isHidden = false
            lblVoucherExpiryDate.attributedText = getExpiredDate()
            stackViewForTerms.isHidden = false
        }
        
        stackViewForMessage.isHidden = true
        if responseData.isRedeemed{
            stackViewForMessage.isHidden = false
            imgMessage.image = UIImage(named: "icon_green_success")
            lblMessage.text = "Redeemed"
            
        }else if voucher.giftTo != nil, voucher.giftTo != ""{
            stackViewForMessage.isHidden = false
            imgMessage.image = UIImage(named: "icon_green_success")
            lblMessage.text = "Gifted"
            stackViewForVoucherCode.isHidden = true
        }else if responseData.isExpired{
            stackViewForMessage.isHidden = false
            imgMessage.image = UIImage(named: "icon_red_warning")
            lblMessage.text = String(format: "Expired on %@", voucher.giftToOn ?? "")
            if voucher.status == "In-Active"{
                lblMessage.text = "In-Active"
            }
        }
        
        viewForGift.isHidden = true
        lblTerms.text = voucher.description
        
        if voucher.giftTo != nil, voucher.giftTo != ""{
            lblGiftName.text = voucher.giftTo
            lblGiftDate.text = voucher.giftToOn
            lblGiftedBy.text = "GIFTED TO"
            viewForGift.isHidden = false
            stackViewForVoucherCode.isHidden = true
        }else if voucher.giftReceived != nil, voucher.giftReceived != ""{
            lblGiftName.text = voucher.giftReceived
            lblGiftDate.text = voucher.giftReceivedOn
            lblGiftedBy.text = "GIFTED BY"
            viewForGift.isHidden = false
            stackViewForVoucherCode.isHidden = true
            if !responseData.isExpired{
                stackViewForVoucherCode.isHidden = false
                btnGiftNow.isHidden = true
            }
        }
    }
    
    private func getStatus() -> (isActive: Bool, isRedeemed: Bool, isExpired: Bool){
        if voucher.status == "Active"{
            return (true, false, false)
        }else if voucher.status == "Redeemed"{
            return (true, true, false)
        }else if voucher.status == "In-Active" || voucher.status == "Expired"{
            return (false, false, true)
        }
        return (false, false, false)
    }
    
    private func getExpiredDate() -> NSAttributedString {
        let nsAttributedString = NSMutableAttributedString(string: "Expires on ", attributes: [NSAttributedString.Key.font: UIFont(name: Constants.App.BBQAppFont.ThemeRegular, size: 14.0)!, NSAttributedString.Key.foregroundColor: UIColor(named: "TextColor") as Any])
        let subTitleString = NSAttributedString(string: voucher.validity ?? "", attributes: [NSAttributedString.Key.font: UIFont(name: Constants.App.BBQAppFont.ThemeSemiBold, size: 14.0)!, NSAttributedString.Key.foregroundColor: UIColor(named: "TextColor") as Any])
        nsAttributedString.append(subTitleString)
        return nsAttributedString
    }
    
    @IBAction func onClickBtnCopyVoucherCode(_ sender: Any) {
        UIPasteboard.general.string = voucher.barCode
        UIUtils.showToast(message: "Voucher code: " + (voucher.barCode ?? "") + " is copied")
    }
    
    @IBAction func onClickBtnGiftNow(_ sender: Any) {
        AnalyticsHelper.shared.triggerEvent(type: .HD02)
        if let barCode = voucher.barCode {
            let giftingVC = UIStoryboard.loadGiftVoucherScreen()
            giftingVC.view.layoutIfNeeded()
            giftingVC.giftingDelegate = self
            giftingVC.voucherBarCode = barCode
            giftingVC.isFromDetailsPage = true
            giftingVC.modalPresentationStyle = .overFullScreen
            self.present(giftingVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func onClickBtnTerms(_ sender: Any) {
        btnTerms.isSelected = !btnTerms.isSelected
        UIView.animate(withDuration: 0.2) {
            self.lblTerms.isHidden = !self.btnTerms.isSelected
            if self.btnTerms.isSelected{
                self.imgTerms.image = UIImage(named: "black_up_arrow")
            }else{
                self.imgTerms.image = UIImage(named: "black_down_arrow")
            }
        } completion: { (isCompleted) in
            if self.btnTerms.isSelected{
                AnalyticsHelper.shared.triggerEvent(type: .HD04)
            }else{
                AnalyticsHelper.shared.triggerEvent(type: .HD04A)
            }
            self.lblTerms.isHidden = !self.btnTerms.isSelected
            if self.btnTerms.isSelected{
                self.imgTerms.image = UIImage(named: "black_up_arrow")
            }else{
                self.imgTerms.image = UIImage(named: "black_down_arrow")
            }
        }

    }
    
    @IBAction func onClickBtnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension BBQHappinesscardDetailsVC: BBQVoucherGiftingViewControllerProtocol {
    
    // MARK: BBQVoucherGiftingViewControllerProtocol Methods
    
    func didFinishedVoucherGifting() {
        delegate?.fetchCouponAndVoucherCount()
        self.navigationController?.popViewController(animated: true)
    }
}

protocol BBQHappinesscardDetailsVCDelegate{
    func fetchCouponAndVoucherCount()
}

extension BBQHappinesscardDetailsVC:  ReferalDelegate{
    func shareReferalCode() {
        
        AnalyticsHelper.shared.triggerEvent(type: .RE10)

        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: 80)
        bottomSheetController = BottomSheetController(configuration: configuration)
        
       let vc = BBQShareReferalCodeViewController(nibName: "BBQShareReferalCodeViewController", bundle: nil)
        bottomSheetController?.present(vc, on: self, viewHeight: self.view.frame.size.height - (self.view.frame.size.height * (20/100)))

      
    }
}
