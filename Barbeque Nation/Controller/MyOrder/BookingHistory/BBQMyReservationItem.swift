//
 //  Created by Mahmadsakir on 29/12/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified ItemDetailViewController.swift
 //

import UIKit


class BBQMyReservationItem: UIView {
    
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var heightConstant: NSLayoutConstraint!
    
    
    
    
    //MARK:- View Configuration
    func initWith() -> BBQMyReservationItem {
        let view = loadFromNib()!
        view.styleUI()
        return view
    }
        
        
    func loadFromNib() -> BBQMyReservationItem? {
        if let views = Bundle.main.loadNibNamed("BBQMyReservationItem", owner: self, options: nil), let view = views[0] as? BBQMyReservationItem{
            view.frame = UIApplication.shared.windows[0].bounds
            return view
        }
        return nil
    }
    
    
    func styleUI() {
        lblTitle.setTheme(size: 14.0, weight: .semibold, color: .text)
        lblValue.setTheme(size: 14.0, weight: .bold, color: .text)
    }
    
    func setData(title: String?, value: String?, imageUrl: String?, image: UIImage?, imageHeight: CGFloat) {
        lblTitle.text = title
        lblValue.text = value
        imgIcon.image = image
        if let imageUrl = imageUrl{
            imgIcon.setImage(url: imageUrl, placeholderImage: image, isForPromotions: true)
        }
        heightConstant.constant = imageHeight
    }
    
}
