//
//  Created by Sakir Sherasiya on 06/04/21
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
//  All rights reserved.
//  Last modified BBQMyReservationVC.swift
//

import UIKit
import Foundation

import Razorpay
import ShimmerSwift

class BBQMyHappinesscardVC:  BBQBaseViewController {
    
    //MARK:- IBOutlet
    @IBOutlet weak var shimmerSupportView: UIView!
    @IBOutlet weak var shimmerView: UIView!
    @IBOutlet weak var stackViewForEmpty: UIStackView!
    
    @IBOutlet weak var myOrderTableView: UITableView!
    
    @IBOutlet weak var responseStatusLabel: UILabel!
    //@IBOutlet weak var happinessCardTitleLabelHeader: UILabel!
//    @IBOutlet weak var myOrderTableTrailingConstraint: NSLayoutConstraint!
//    @IBOutlet weak var myOrderTableLeadingConstraint: NSLayoutConstraint!
    
    var shimmerControl : ShimmeringView?
    var isShimmerRunning : Bool = false
    
    var isComingFromNotification = false
    //MARK:- Properties
    var tablePageLimit = 0
    var voucherHistoryPageLimit = 0
    var vocherHistoryTablePageNo = 0
    var vouchorHistoryViewModel = BBQVoucherHistoryViewModel()
    var vouchersData : [Vouchers] = []
    var voucherHistoryData : [VoucherHistoryModel] = []
    var isVocherDataFetched = false
    private let detailScreenNib = Constants.CellIdentifiers.voucherDetailsControllerNib
    var heightOfCell = 240.0
    var bottomSheetController: BottomSheetController?
    var voucherId : Int?
    var textToShare:String?
    var dateTimePlace:String?
    var isPullToRefreshClicked = false
    var isCancelled = false
    var superVC: UIViewController?
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(BBQMyHappinesscardVC.handleRefresh(_:)), for: UIControl.Event.valueChanged)
        return refreshControl
    }()
    
    //MARK:- Button Actions
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        isPullToRefreshClicked = true
        
        self.vocherHistoryTablePageNo = 0
        self.voucherHistoryPageLimit = 0
        self.vouchersData.removeAll()
        fetchVoucherHistoryData(pageNo: String(vocherHistoryTablePageNo))
        
    }
    
    @IBAction func voucherbuttonTapped(_ sender: UIButton) {
//        self.myOrderTableLeadingConstraint.constant = 15
//        self.myOrderTableTrailingConstraint.constant = 15
        isPullToRefreshClicked = false
        voucherHistoryPageLimit = 0
        self.vouchersData = []
        self.responseStatusLabel.text = "You don’t have any Happiness Card"
        
        self.showShimmering()
        // TODO: Call this API when view loads for history view controllef.
        fetchVoucherHistoryData(pageNo: String(vocherHistoryTablePageNo))
        myOrderTableView.reloadData()
    }
    func fetchVoucherHistoryData(pageNo: String){
        if isPullToRefreshClicked == false{
            if !self.isShimmerRunning {
                BBQActivityIndicator.showIndicator(view: self.view)
            }
            self.isPullToRefreshClicked = true
        }
        vouchorHistoryViewModel.getVoucherHistoryDetails(pageNo: pageNo) { (result) in
            if result == true{
                self.refreshControl.endRefreshing()
                self.stopShimmering()
                BBQActivityIndicator.hideIndicator(from: self.view)
                self.isVocherDataFetched = false
                if let data = self.vouchorHistoryViewModel.getVoucherHistoryDetailsData{
                    if data.count != 0{
                        self.myOrderTableView.isHidden = false
                        self.responseStatusLabel.isHidden = true
                      //  self.happinessCardTitleLabelHeader.isHidden = false
                        self.stackViewForEmpty.isHidden = true
                        
                        
                        // TODO: Temporary fix to avoid duplicates in first page in voucher history.
                        if self.vocherHistoryTablePageNo > 0 {
                            self.vouchersData.append(contentsOf: data)
                            self.voucherHistoryPageLimit = self.voucherHistoryPageLimit + data.count
                        } else {
                            self.vouchersData = data
                            self.voucherHistoryPageLimit = data.count
                        }
                        
                    }else{
                        self.myOrderTableView.isHidden = true
                        self.responseStatusLabel.isHidden = true
                        self.stackViewForEmpty.isHidden = false
                     //   self.happinessCardTitleLabelHeader.isHidden = true
                    }
                }
                self.myOrderTableView.reloadData()
            }
            else{
                BBQActivityIndicator.hideIndicator(from: self.view)
                self.isVocherDataFetched = false
                self.myOrderTableView.isHidden = true
                self.responseStatusLabel.isHidden = false
                
            }
        }
    }
    //MARK:- Life Cycle
    override func viewDidLoad() {
        self.isNeedLoadLoyalityPoints = false
        super.viewDidLoad()
        AnalyticsHelper.shared.triggerEvent(type: .HH01)
        self.shimmerControl = ShimmeringView(frame: shimmerSupportView.bounds)
        self.showShimmering()
        
        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: self.view.frame.size.height - 258.0)
        bottomSheetController = BottomSheetController(configuration: configuration)
        myOrderTableView.isHidden = true
        self.myOrderTableView.addSubview(self.refreshControl)
        loadVoucherData()
    }
    
    func loadVoucherData() {
//        self.myOrderTableLeadingConstraint.constant = 15
//        self.myOrderTableTrailingConstraint.constant = 15
        isPullToRefreshClicked = false
        voucherHistoryPageLimit = 0
        self.vouchersData = []
        self.responseStatusLabel.text = "You don’t have any Happiness Card"
        self.showShimmering()
        fetchVoucherHistoryData(pageNo: String(vocherHistoryTablePageNo))
        myOrderTableView.reloadData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.screenName(name: .My_Happiness_Card_Screen)
    }
    
    @IBAction func historyBackButtonClicked(_sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    // MARK:- In case of no happiness cart go to 1st tab
    @IBAction func onClickBtnBuyCardNow(_ sender: Any) {
        self.navigateToHomePage()
    }
    
        
    
}



extension BBQMyHappinesscardVC: UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    @objc func loadTable() {
        myOrderTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.vouchersData.count
    }
    
    // TODO: Refactor entire method, create viewmodel separately and draw logic from it.
    // Do data manipulation in cell itself using uypdate method.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row < vouchersData.count
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BBQMyHappinesscardCell", for: indexPath) as! BBQMyHappinesscardCell
            cell.setCellData(voucher: vouchersData[indexPath.row], indexPath: indexPath)
            cell.delegate = self
            return cell
        }
        else{
            let cell = UITableViewCell()
            cell.isUserInteractionEnabled = false
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row < vouchersData.count
        {
            AnalyticsHelper.shared.triggerEvent(type: .HH04)
            let detailsViewController = UIStoryboard.loadHappinesscardDetailViewController()
            detailsViewController.voucher = vouchersData[indexPath.row]
            self.navigationController?.pushViewController(detailsViewController, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        isPullToRefreshClicked = false
        if indexPath.row < (vouchorHistoryViewModel.getTotalVoucherItems ?? 0) - 1{
            if indexPath.row == voucherHistoryPageLimit - 1 && !isVocherDataFetched {
                isVocherDataFetched = true
                vocherHistoryTablePageNo = vocherHistoryTablePageNo + 1
                self.fetchVoucherHistoryData(pageNo: String(vocherHistoryTablePageNo))
                self.perform(#selector(loadTable), with: nil, afterDelay: 1.0)
            }
        }
    }
    
}


extension BBQMyHappinesscardVC{
    // MARK: Shimmer Methods
    
    private func showShimmering() {
        self.shimmerSupportView.isHidden = false
        
        if let shimmer = self.shimmerControl {
            self.shimmerSupportView.addSubview(shimmer)
            shimmer.contentView = shimmerView
            shimmer.shimmerAnimationOpacity = 0.2
            shimmer.shimmerSpeed = 500.00
            shimmer.isHidden = false
            shimmer.isShimmering = true
            self.isShimmerRunning = true
        }
    }
    
    private func stopShimmering() {
        self.shimmerSupportView.isHidden = true
        
        if let shimmer = self.shimmerControl {
            shimmer.isShimmering = false
            shimmer.isHidden = true
            self.isShimmerRunning = false
        }
    }
}

extension BBQMyHappinesscardVC: BBQVoucherGiftingViewControllerProtocol {
    
    // MARK: BBQVoucherGiftingViewControllerProtocol Methods
    
    func didFinishedVoucherGifting() {
        self.vocherHistoryTablePageNo = 0
        self.voucherHistoryPageLimit = 0
        fetchVoucherHistoryData(pageNo: String(vocherHistoryTablePageNo))
    }
}


extension BBQMyHappinesscardVC: BBQMyHappinesscardCellDelegate{
    func onClickGiftVoucher(indexPath: IndexPath) {
        AnalyticsHelper.shared.triggerEvent(type: .HH02)
        if let barCode = self.vouchersData[indexPath.row].barCode {
            let giftingVC = UIStoryboard.loadGiftVoucherScreen()
            giftingVC.view.layoutIfNeeded()
            giftingVC.giftingDelegate = self
            giftingVC.voucherBarCode = barCode
            giftingVC.modalPresentationStyle = .overFullScreen
            self.present(giftingVC, animated: true, completion: nil)
        }
    }
}

extension BBQMyHappinesscardVC {
    
    @IBAction func backBtnPressed(_ sender : UIButton){
        if let superVC = superVC{
            if let isContain = self.navigationController?.viewControllers.contains(superVC), isContain{
                self.navigationController?.popToViewController(superVC, animated: true)
            }else{
                self.navigationController?.popToRootViewController(animated: true)
            }
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
}
