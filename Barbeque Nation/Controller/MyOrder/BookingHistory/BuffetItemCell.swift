/*
 *  Created by Nischitha on 11/09/19.
 *  Copyright © BARBEQUE NATION HOSPITALITY LIMITED 2019. All rights reserved.
 
 * CHANGE LOG
 * —------------------------------------------------------------------------------
 * ID          Bug ID      Author Name          Date                  Description
 * —------------------------------------------------------------------------------
 * 1            NA         Nischitha         11/09/19      Initial Version
 */

import UIKit

class BuffetItemCell: UITableViewCell {
    
    //MARK:- IB Outlets
    @IBOutlet weak var buffetImage: UIImageView!
    @IBOutlet weak var buffetNameLabel: UILabel!
    @IBOutlet weak var shadowLayer: UIView!
    @IBOutlet weak var buffetItemShadowLayer: UIView!
    
    //MARK:- UI Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
