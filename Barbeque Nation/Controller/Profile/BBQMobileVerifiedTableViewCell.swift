//
 //  Created by Ajith CP on 01/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQMobileVerifiedTableViewCell.swift
 //

import UIKit

class BBQMobileVerifiedTableViewCell: UITableViewCell {

    @IBOutlet weak var verifiedNumberField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func loadVerifiedContent(viewModel: BBQMobileVerifierViewModel) {
        self.verifiedNumberField.text = viewModel.verifiedMobileNumberString()
    }

}
