//
//  Created by Ajith CP on 05/08/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQProfileHeaderTableViewCell.swift
//

import UIKit

import SDWebImage

protocol BBQProfileHeaderTableViewCellDelegate {
    /* Delegates for profile header view */
    
    func didPressedSignupOption(_ sender: UIButton)
    
    func didPressedImagePickerOption(_ sender: UIButton)
    
    func didPressedEditProfile(with name: String)
    
    func didPressedLoyaltyPoints()
    
    func didPressBackButton()
}

class BBQProfileHeaderTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets

    @IBOutlet weak var locationStackView: UIStackView!
    @IBOutlet weak var imageLoader: UIActivityIndicatorView!
    
    @IBOutlet weak var dashView: UIView!
    @IBOutlet weak var editNameView: UIView!
   // @IBOutlet weak var addPhotoView: UIView!
    
  //  @IBOutlet weak var addProfileImageView: UIImageView!
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var exclmLabel: UILabel!
    @IBOutlet weak var greetingsLabel: UILabel!
    @IBOutlet weak var pointsTitleLabel: UILabel!
    
    @IBOutlet weak var pointsValueButton: UIButton!
    @IBOutlet weak var imagePickerButton: UIButton!
    @IBOutlet weak var updateProfileButton: UIButton!
    @IBOutlet weak var addPhotoButton: UIButton!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var btnSmile: UIButton!
    @IBOutlet weak var smileStackView : UIStackView!
    @IBOutlet weak var editNameTextField: UITextField!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var locationStackHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var pointsCrownImageHeightConstraint: NSLayoutConstraint!
    
    var headerViewDelegate : BBQProfileHeaderTableViewCellDelegate?
    var isEditMode       : Bool = false
    var profileVM : BBQProfileViewModel?
    
    let allowedCharacters = CharacterSet(charactersIn:"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvxyz ").inverted

    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupTableViewCell()
    }
    
    // MARK: - Setup Methods
    
    func setupTableViewCell()  {
        self.stopImageLoader()
      //  self.pointsTitleLabel.text = kPointsTitleLabelString
        self.dashView.topLayerShadow(opacity: 0.4, shadowSize: 1.0)
        self.dashView.maskByRoundingCorners(cornerRadius: 15.0, maskedCorners: CornerMask.topCornersMask)
        profileImageView.setCircleView()
        
        
//        self.dashView.clipsToBounds = true
//        self.dashView.layer.cornerRadius = 50
//        self.dashView.layer.maskedCorners = [ .layerMinXMinYCorner, .layerMaxXMaxYCorner]
        
      //  profileImageView.addConstraint(NSLayoutConstraint(item: addPhotoButton!, attribute: .trailing, relatedBy: .equal, toItem: profileImageView, attribute: .trailing, multiplier: 1, constant: 0))
     //   addPhotoButton.addConstraint(NSLayoutConstraint(item: profileImageView!, attribute: .bottom, relatedBy: .equal, toItem: addPhotoButton, attribute: .bottom, multiplier: 1, constant: 0))
        
    }
    
    // MARK:- IBActions
    
    @IBAction func editProfileButtonPressed(_ sender: UIButton) {

//        var name = self.editNameTextField.text!
//        if name.count < 1 {
//            name = self.nameLabel.text ?? ""
//        }
        self.headerViewDelegate?.didPressedEditProfile(with: "name")
        
    }
    
    
    @IBAction func profileBackButtonPressed(_ sender: UIButton) {

        self.headerViewDelegate?.didPressBackButton()
        
    }
    
    @IBAction func imagePickerButtonPressed(_ sender: UIButton) {
        self.headerViewDelegate?.didPressedImagePickerOption(sender)
    }
    
    @IBAction func addPhotoButtonPressed(_ sender: UIButton) {
        self.headerViewDelegate?.didPressedImagePickerOption(sender)
    }
    

    @IBAction func signupButtonPressed(_ sender: UIButton) {
        self.headerViewDelegate?.didPressedSignupOption(sender)
    }
    
    @IBAction func loyaltyPointsButtonPressed(_ sender: Any) {
        self.headerViewDelegate?.didPressedLoyaltyPoints()
    }
    
    
    // MARK: - Public Methods
    
    public func loadProfileHeaderCell(with viewModel: BBQProfileViewModel,
                                      isEditMode: Bool) {
        self.profileVM = viewModel
        self.isEditMode = isEditMode
       // self.titleLabel.text = viewModel.profileTitleLabelString()

       // self.dashView.backgroundColor =  UIColor.profileDarkGreyColor

        self.nameLabel.text = viewModel.profileNameLabelString()
       // self.nameLabel.textColor =  UIColor.hexStringToUIColor(hex: "2B3849")
        self.nameLabel.font = UIFont(name: Constants.App.BBQAppFont.ThemeRegular, size: 24.0)
        //self.editNameTextField.text = viewModel.profileNameLabelString()
        self.exclmLabel.text = viewModel.profileEntriesDescription(for: 2).description

        // Image picker enabled in edit mode only if user have already a profile pic.
        self.imagePickerButton.isEnabled = (isEditMode || !(viewModel.profileData?.profileImageURL != nil))


        self.configureAddPhotoView(with: viewModel)
        self.configureEditFeature(with: viewModel, isEditMode: isEditMode)
        self.configureLoyaltyPoints(with: viewModel)

      //  self.editNameView.isHidden = !isEditMode
        self.nameLabel.isHidden    = isEditMode
        self.exclmLabel.isHidden = isEditMode
        self.greetingsLabel.isHidden = isEditMode
      //  self.titleLabel.isHidden = isEditMode
        
        updateUserPoints()
    }
    
    
    
    func updateUserPoints() {
        //btnSmile.setTitle("\(BBQUserDefaults.sharedInstance.UserLoyaltyPoints)", for: .normal)
        let userLoyaltyPoints = BBQUserDefaults.sharedInstance.UserLoyaltyPoints
        let points_string = " \(kMsgBBQPoints)  \(userLoyaltyPoints)"
        let string_to_color = String(userLoyaltyPoints)
        let range = (points_string as NSString).range(of: string_to_color)
        let mainStringRange = (points_string as NSString).range(of: kMsgBBQPoints)
        let attributedString = NSMutableAttributedString(string: points_string)
        let attribute = [NSAttributedString.Key.font: UIFont.appThemeRegularWith(size: 12.0),
                         NSAttributedString.Key.foregroundColor: UIColor.deliveryThemeColor] as [NSAttributedString.Key : Any]
        attributedString.addAttributes(attribute, range: mainStringRange)
        
        let pointAttribute = [NSAttributedString.Key.font: UIFont.appThemeSemiBoldWith(size: 12.0),
                              NSAttributedString.Key.foregroundColor: UIColor.deliveryThemeColor] as [NSAttributedString.Key : Any]
        attributedString.addAttributes(pointAttribute, range: range)
        
        let imageAttachment = NSTextAttachment()
        imageAttachment.image = UIImage(named: "smiles_coins_logo")
        let iconsSize = CGRect(x: 0, y: -8, width: 28, height: 28)
        imageAttachment.bounds = iconsSize
        
        // wrap the attachment in its own attributed string so we can append it
        let imageString = NSMutableAttributedString(attachment: imageAttachment)
        imageString.append(attributedString)
        self.btnSmile.setAttributedTitle(imageString, for: .normal)
        btnSmile.layer.cornerRadius = 5
        smileStackView.setShadow()
    }
    
    private func configureLoyaltyPoints(with viewModel:BBQProfileViewModel) {
        
        _ = "\(viewModel.profileData?.loyaltyPoints ?? 0)"
       // self.pointsValueButton.setTitle(pointsValue, for: .normal)
        self.btnSmile.isHidden = viewModel.isGuestUser
        self.signupButton.isHidden = !viewModel.isGuestUser
        
        let yourAttributes: [NSAttributedString.Key: Any] = [
            .foregroundColor: UIColor.white,
            .underlineStyle: NSUnderlineStyle.single.rawValue]
        let attributeString = NSMutableAttributedString(string: kSignupForPointsMessage,
                                                        attributes: yourAttributes)
        self.signupButton.setAttributedTitle(attributeString, for: .normal)
        
    }
    
   
    
    private func configureEditFeature(with viewModel: BBQProfileViewModel, isEditMode: Bool) {
        //self.updateProfileButton.setImage(viewModel.editButton(isEditMode: isEditMode).image, for: .normal)
       // self.updateProfileButton.setTitle(viewModel.editButton(isEditMode: isEditMode).editTitle, for: .normal)
        //self.updateProfileButton.setTitleColor(viewModel.editButton(isEditMode: isEditMode).color,
       //                                        for: .normal)
        self.updateProfileButton.isEnabled = viewModel.editButton(isEditMode: isEditMode).isEnabled
    }
    
    private func configureAddPhotoView(with viewModel: BBQProfileViewModel) {
        self.startImageLoader()
        if let userImageURL = viewModel.profileData?.profileImageURL, userImageURL != "" {
            self.profileImageView.sd_setImage(with:  URL(string: userImageURL))
            { (image, error, type, url) in
                self.stopImageLoader()
            }
          //  self.addPhotoView.isHidden = true
        } else {
            self.stopImageLoader()
            self.profileImageView.image = viewModel.addProfileImage()
            profileImageView.contentMode = .scaleAspectFill
           self.addPhotoButton.setTitleColor(viewModel.addProfileImageButton().textColor,
                                              for: .normal)
            self.addPhotoButton.isEnabled = viewModel.addProfileImageButton().isEnabled
          //  self.addPhotoView.isHidden = false
        }
      
    }
    
    func startImageLoader() {
        self.imageLoader.isHidden = false
        self.imageLoader.startAnimating()
        
    }
    
    func stopImageLoader() {
        self.imageLoader.isHidden = true
        self.imageLoader.stopAnimating()
    }

}

extension BBQProfileHeaderTableViewCell : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.endEditing(true)
        return true
    }
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 200 //50
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
            if newString.contains("  ") {
                return false
            } else if currentString.length == 0 && newString == " "{
                return false
            }
            
            let components = string.components(separatedBy: allowedCharacters)
            let filtered = components.joined(separator: "")
            
            if string != filtered {
                
                return false
            }
        return newString.length <= maxLength
    }
}
