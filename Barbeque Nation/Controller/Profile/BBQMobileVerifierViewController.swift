//
 //  Created by Ajith CP on 13/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQMobileVerifierViewController.swift
 //

import UIKit
import FirebaseAnalytics

protocol BBQMobileVerifierViewControllerDelegate {
    
    func didVerifiedMobileOTP(countryCode: String,
                              updatedNumber: String,
                              otpID: String,
                              verifierOTP: String)
    
}

class BBQMobileVerifierViewController: UIViewController {
    
    private static let keyboardAdjustDistance = -220
    private static let keyboardAdjustDuration = 0.3
    

    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var dashView: UIView!
    
    @IBOutlet weak var verifierTableView: UITableView!
    
    var verifierDelegate : BBQMobileVerifierViewControllerDelegate?
    
    var updatedMobileNumber : Int64 = 0
    var isMobileVerified = false
    var validatedOTP = 0
    
    var countryCode : String = Constants.App.defaultValues.phoneNumberCode {
        didSet {
            BBQUserDefaults.sharedInstance.phoneCode = countryCode
        }
    }
    
    
    // MARK:- Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AnalyticsHelper.shared.screenName(name: "MobileVerificationScreen", className: "BBQMobileVerifierViewController")
        Analytics.logEvent("Click on MobileVerification", parameters: [
            "name": "MobileVerification Check" as NSObject,
            "full_text": "MobileVerification page" as NSObject
            ])
        
        
        self.setupTableView()
        self.setupDashView()
        
        self.countryCode = Constants.App.defaultValues.phoneNumberCode

    }
    
    // MARK:- Lazy Inits
    
    lazy private var verifierViewModel : BBQMobileVerifierViewModel = {
        let verifierVM = BBQMobileVerifierViewModel()
        return verifierVM
    } ()
    
    
    // MARK:- Setup Methods
    
    private func setupDashView() {
        self.dashView.topLayerShadow(opacity: 0.4, shadowSize: 1.0)
        self.dashView.maskByRoundingCorners(cornerRadius: 10.0, maskedCorners: CornerMask.topCornersMask)
        
        // Dismissing view while clicking outside validator component.
        self.overlayView.addGestureRecognizer(UITapGestureRecognizer(target: self,
                                                                     action: #selector(endMobileValidation)))
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self,
                                                              action: #selector(endEditingFields)))
    }
    
    private func setupTableView()  {
        
        self.verifierTableView.estimatedRowHeight = Constants.App.ProfilePage.headerEstimatedRowHeight
        self.verifierTableView.rowHeight = UITableView.automaticDimension
        
        self.verifierTableView.register(UINib(nibName: Constants.NibName.verifierHeaderCell,
                                             bundle: nil),
                                       forCellReuseIdentifier: Constants.CellIdentifiers.verifierHeaderCellID)
        self.verifierTableView.register(UINib(nibName:  Constants.NibName.verifierPhoneNumberCell,
                                             bundle: nil),
                                       forCellReuseIdentifier: Constants.CellIdentifiers.verifierPhoneNumberCellID)
        self.verifierTableView.register(UINib(nibName:  Constants.NibName.verifierActionsCell,
                                             bundle: nil),
                                       forCellReuseIdentifier: Constants.CellIdentifiers.verifierActionsCellID)
        self.verifierTableView.register(UINib(nibName:  Constants.NibName.verifiedMobileCell,
                                              bundle: nil),
                                        forCellReuseIdentifier: Constants.CellIdentifiers.verifiedMobileCellID)
        
    }
    
    
    // MARK:- API Methods
    
    
    // MARK:- Private Methods
    
    @objc func endMobileValidation() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func endEditingFields() {
       self.view.endEditing(true)
    }


}

extension BBQMobileVerifierViewController : UITableViewDelegate, UITableViewDataSource {
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        // Adding fixed heights since reusing PhonenumberCell:- constraints reworks needed.
        switch indexPath.row {
        case 0:
            return 100
        case 1:
            return 72
        default:
            return 80
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0 :
            return self.loadVerifierHeaderCell(for: indexPath)
        case 1:
            if isMobileVerified {
                return self.loadMobileNumberVerifiedCell(for: indexPath)
            }
            return self.loadVerifierPhoneNumberCell(for: indexPath)
            
        default :
            return self.loadVerifierActionsCell(for: indexPath)
        }
    }
    
    // MARK:- Private Methods
    
    private func loadVerifierHeaderCell(for indexPath: IndexPath) -> BBQMobileVerifierHeaderTableViewCell {
        
        let headerCell =
            self.verifierTableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.verifierHeaderCellID)
                as! BBQMobileVerifierHeaderTableViewCell
        headerCell.topDrawerView.isHidden = self.isMobileVerified
        return headerCell
    }
    
    private func loadVerifierPhoneNumberCell(for indexPath: IndexPath) -> PhoneNumberCell {
        let numberCell =
            self.verifierTableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.verifierPhoneNumberCellID)
                as! PhoneNumberCell
        self.configurePhoneNumberCell(numberCell)
        numberCell.updatePhoneNumberCell(with: self.countryCode)
        numberCell.errorLabel.text = kAddProfileMobileNumber
        numberCell.delegate = self
        return numberCell
    }
    
    private func loadMobileNumberVerifiedCell(for indexPath: IndexPath) -> BBQMobileVerifiedTableViewCell {
        let verifiedCell =
            self.verifierTableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.verifiedMobileCellID)
                as! BBQMobileVerifiedTableViewCell
        verifiedCell.loadVerifiedContent(viewModel: self.verifierViewModel)
        return verifiedCell
    }
    
    private func loadVerifierActionsCell(for indexPath: IndexPath) -> BBQMobileVerifierActionTableViewCell {
        let actionsCell =
            self.verifierTableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.verifierActionsCellID)
                as! BBQMobileVerifierActionTableViewCell
        actionsCell.actionDelegate = self
        actionsCell.loadActionContent(isVerified: self.isMobileVerified)
        return actionsCell
    }
    
    private func configurePhoneNumberCell(_ cell:PhoneNumberCell) {
        
        cell.countryCodeButton.addTarget(self,
                                         action:#selector(countryCodeSelectionButtonPresed),
                                         for: .touchUpInside)
        
        // Configuring theme for mobile verifier page.
        cell.errorMessage.textColor = .darkGray
        cell.isdCodeLabel.textColor = .darkGray
        cell.errorLabel.textColor   = .darkGray
        cell.textField.textColor    = .darkGray
        cell.view1.backgroundColor  = .darkGray
        cell.view2.backgroundColor  = .darkGray
        cell.countryCodeButton.setTitleColor(.darkGray, for: .normal)
    }
    
    func animateEmailTextField(textField: UITextField, up: Bool){
        let movementDistance:CGFloat = CGFloat(BBQMobileVerifierViewController.keyboardAdjustDistance)
        let movementDuration: Double = BBQMobileVerifierViewController.keyboardAdjustDuration
        var movement:CGFloat = 0
        if up{
            movement = movementDistance
        }else{
            movement = -movementDistance
        }
        UIView.beginAnimations(Constants.NotificationName.animateTextField, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
    
    @objc func countryCodeSelectionButtonPresed(with textField: UITextField) {
        let pickerViewController = UIStoryboard.loadPickerViewController()
        pickerViewController.delegate = self
        pickerViewController.type = .countryCode
        pickerViewController.isComingFromProfile = true
        self.present(pickerViewController, animated: false, completion: nil)
    }
    
}

extension BBQMobileVerifierViewController : PickerViewDelegate, BBQMobileVerifierActionTableViewCellDelegate,
BBQOTPScreenDelegate, PhoneNumberCellDelegate, UITextFieldDelegate {
    
    // MARK:- PickerViewDelegate Methods
    
    func countryCodeSelected(phoneCode: String) {
        if self.countryCode != phoneCode {
            if let phoneCell = self.verifierTableView.cellForRow(at: IndexPath(row: 1, section: 0)) as? PhoneNumberCell {
                 phoneCell.textField.text = ""
                 phoneCell.changeLabelPosition(position: 4.0)
            }
        }
        self.countryCode = phoneCode
        self.verifierTableView.reloadData()

    }
    
    func titleSelected(title: String) {
        // Implementation not needed here.
    }
    
    func pickerClosed() {
        // Implementation not needed here.
    }
    
    // MARK:- BBQMobileVerifierActionTableViewCellDelegate Methods
    
    func didPressedNextButton() {
        AnalyticsHelper.shared.triggerEvent(type: .verify_user_status)
        let isMobileValid = BBQPhoneNumberValidation.isValidNumber(number: String(self.updatedMobileNumber),
                                                                   for: self.countryCode)
        if isMobileValid {
            self.updatePhoneNumberCell(showError: false)
            BBQActivityIndicator.showIndicator(view: self.view)
            
            // 1. Verifying user mobile first
            // 2. If existing user mobile, blocks to update mobile, showing error
            // 3. New user generates OTP and gpto OTP page
            self.verifierViewModel.verifyUser(countryCode: self.countryCode,
                                              mobileNumber: self.updatedMobileNumber,
                                              otpGenerate: false) { (isExistingUser) in
                                                BBQActivityIndicator.hideIndicator(from: self.view)
                                                if isExistingUser {
                                                    self.updatePhoneNumberCell(showError: true, text: kAlreadyRegistredMobile)
                                                } else {
                                                    self.verifierViewModel.generateMobileOTP(countryCode: self.countryCode,
                                                                                             mobileNumber: self.updatedMobileNumber,
                                                                                             otpId: self.verifierViewModel.otpIdentifier) { (isGenerated) in
                                                                                                if isGenerated {
                                                                                                    self.goToOTPVerifierScreen()
                                                                                                }
                                                                                                BBQActivityIndicator.hideIndicator(from: self.view)
                                                    }
                                                }
            }
            
            self.view.endEditing(true) // Dismissing keyboard for listening server messages.
            
        } else {
            self.updatePhoneNumberCell(showError: true, text: kInValidMobile)
        }
    }
    
    func didPressedBackButton() {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK:- OTPScreen Delegate Methods
    
    func otpValidation(status: Bool, otp: String) {
        if status == true {
            self.validatedOTP = Int(otp) ?? 0
            self.isMobileVerified = true
            self.verifierTableView.reloadData {
            }
        } else {
            ToastHandler.showToastWithMessage(message: kMaxAtmpOtpReachDesc)
        }
    }
    
    func backButtonPressed() {
       
    }
    
    func didPressedDoneButton() {
        self.dismiss(animated: true, completion: {
            self.verifierDelegate?.didVerifiedMobileOTP(countryCode: self.countryCode,
                                                        updatedNumber: String(self.updatedMobileNumber),
                                                        otpID: self.verifierViewModel.otpIdentifier,
                                                        verifierOTP: String(self.validatedOTP))
        })
    }
    
    // MARK:- PhoneNumberCellDelegate Methods
    
    func phoneNumberEntered(length: Int, text: String) {
        self.updatedMobileNumber = Int64(text) ?? 0
    }
    
    func textFieldStartsEditing(_ textField: UITextField) {
        textField.becomeFirstResponder()
        self.animateEmailTextField(textField: textField, up:true)
    }
    
    func textFieldStopsEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        self.animateEmailTextField(textField: textField, up:false)
    }
    
    
    // MARK:- Private Methods
    
    private func goToOTPVerifierScreen() {
        let validatorVC = UIStoryboard.loadOTPViewController()
        validatorVC.delegate = self
        validatorVC.otpType = .others
        validatorVC.currentTheme = .dark
        validatorVC.screenHeight = 400
        validatorVC.validationPath = String(self.updatedMobileNumber)
        validatorVC.otpID = verifierViewModel.otpIdentifier
        validatorVC.validationType = .mobileNumber
        validatorVC.header = kUpdateMobileString
        validatorVC.otpMessage =  kOTPSentNotificvation
        validatorVC.logoImage =  Constants.AssetName.kAddMobileLogo
        validatorVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        let configuration = BottomSheetConfiguration(animationDuration: 0.4,
                                                     minimumTopSpacing: 0.0)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        bottomSheetController.present(validatorVC, on: self)
    }
    
    func updatePhoneNumberCell(showError:Bool, text: String? = "") {
        let cell = self.verifierTableView.cellForRow(at: IndexPath(row: 1, section: 0)) as? PhoneNumberCell
        cell?.errorMessage.textColor = .red
        cell?.errorMessage.text = text
        cell?.errorMessage.stopBlink()
        cell?.errorMessage.startBlink()
        cell?.errorMessage.isHidden = !showError
    }
}
