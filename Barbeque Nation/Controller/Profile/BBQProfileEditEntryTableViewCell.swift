//
 //  Created by Ajith CP on 03/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQProfileEditEntryTableViewCell.swift
 //

import UIKit

enum EditFieldType : Int {
    case mobileField = 2
    case emailField
    case birthdayField
    case anniversaryField
    case none
}

protocol BBQProfileEditEntryTableViewCellDelegate {
    
    func editFieldValueChanged(with type:EditFieldType, newValue: String)
    
}

class BBQProfileEditEntryTableViewCell: UITableViewCell, UITextFieldDelegate {
    
    var editDelegate : BBQProfileEditEntryTableViewCellDelegate?
    
    private var datePicker = UIDatePicker()
    
    private var selectedTextField : UITextField?
    
    private var editFieldType : EditFieldType = .birthdayField
    
    private var viewModel : BBQProfileViewModel?
    
    @IBOutlet weak var editTextField: UITextField!
    
    @IBOutlet weak var entryIconImageView: UIImageView!
    
    @IBOutlet weak var bottomSeparatorView: UIView!
    
    @IBOutlet weak var calendarButton: UIButton!
    
    @IBOutlet weak var fieldErrorTextLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let tapOutside = UITapGestureRecognizer(target: self, action: #selector(UIView.endEditing(_:)))
        self.addGestureRecognizer(tapOutside)
        
        self.setupDatePicker()
        self.fieldErrorTextLabel.isHidden = true
        
        
    }
    
    private func setupDatePicker()  {
        self.datePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            self.datePicker.preferredDatePickerStyle = .wheels
        }
    }
    
    func setupEditableFields(with index: IndexPath, viewModel: BBQProfileViewModel)  {
    
        if (index.row == 1 || index.row == 2) && !viewModel.isGuestUser {
            self.bottomSeparatorView.isHidden = false
        } else {
            self.bottomSeparatorView.isHidden = true
        }
        self.fieldErrorTextLabel.isHidden = true
    }


    // MARK: IBAction Methods

    @objc func dateValueChanged(datePicker: UIDatePicker) {
        let dateString = BBQProfileViewModel.formattedDateString(date: datePicker.date)
        selectedTextField?.text = dateString
        self.editDelegate?.editFieldValueChanged(with: self.editFieldType, newValue: dateString)
    }
    
    @IBAction func dateSelectionButtonPressed(_ sender: UIButton) {
        self.editTextField.becomeFirstResponder()
    }
    
    // MARK: Setup UI Methods
    
    func loadContent(with viewModel: BBQProfileViewModel?, for index: IndexPath) {
        if let profileVM = viewModel {
            self.viewModel = profileVM
            self.setupEditableFields(with: index, viewModel: profileVM)
            self.editTextField.placeholder = profileVM.profileEditEntryData(for: index.row).description
            self.entryIconImageView.image = UIImage(named: profileVM.profileEditEntryData(for: index.row).imageName)
            self.calendarButton.isHidden = !profileVM.isCalendarIconVisible(for: index.row)
           
            if let mobileNumber = profileVM.updatedNumber, index.row == 1, mobileNumber.count > 2 {
                self.editTextField.text = mobileNumber
            }
            if let emailID = profileVM.updatedEmailID, index.row == 2, emailID.count > 0 {
                self.editTextField.text = emailID
            }
            if let birthDay = profileVM.profileData?.dob, index.row == 3 {
                let formattedBDay = profileVM.displayFormattedDateString(dateString: birthDay,
                                                                              entryType: .birthDay)
                self.editTextField.text = formattedBDay
            }
            if let annDate = profileVM.profileData?.anniversaryDate, index.row == 4 {
                let formattedADay = profileVM.displayFormattedDateString(dateString: annDate,
                                                                         entryType: .anniversary)
                self.editTextField.text = formattedADay
            }
      
            self.addToolBar(textField: self.editTextField, with: self.datePicker)
            self.editTextField.inputView = nil
        }
    }
    
    // MARK: Private Methods
    
    private func addToolBar(textField: UITextField, with datePicker: UIDatePicker){
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.menuOrangeColor
        let doneButton = UIBarButtonItem(title: kDoneButton, style: UIBarButtonItem.Style.done, target: self, action: #selector(toolbarDonePressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        textField.delegate = self
        textField.inputAccessoryView = toolBar
    }
    
    @objc func toolbarDonePressed() {
        self.dateValueChanged(datePicker: self.datePicker)
        self.endEditing(true)
    }
    
    // MARK:- UITextFieldDelegate Methods
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.selectedTextField = textField
        self.fieldErrorTextLabel.isHidden = true
        
        let cell: UITableViewCell = textField.superview?.superview as! UITableViewCell
        let table: UITableView = cell.superview as! UITableView
        let textFieldIndexPath = table.indexPath(for: cell)
        
        self.editFieldType = (self.viewModel?.editTextFieldType(for: textFieldIndexPath!.row))!
        textField.keyboardType = (self.viewModel?.keyBoardType(for: textFieldIndexPath!.row))!
        
        if self.editFieldType == .birthdayField  {
            self.datePicker.minimumDate = self.viewModel?.minimumDatePickerValue()
            self.datePicker.maximumDate = Date()
            self.selectedTextField?.inputView = datePicker
        } else if self.editFieldType == .anniversaryField {
            self.datePicker.minimumDate = self.viewModel?.minimumDatePickerValue()
            self.datePicker.maximumDate = Date()
            self.selectedTextField?.inputView = datePicker
        } else if self.editFieldType == .mobileField || self.editFieldType == .emailField {
            self.editDelegate?.editFieldValueChanged(with: self.editFieldType, newValue: "")
            return false 
        }

        return true
    }
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        
        return false 
    }

    
}
