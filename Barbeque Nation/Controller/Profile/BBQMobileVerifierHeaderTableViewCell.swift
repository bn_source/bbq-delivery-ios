//
 //  Created by Ajith CP on 13/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQMobileVerifierHeaderTableViewCell.swift
 //

import UIKit

class BBQMobileVerifierHeaderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var topDrawerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
