//
 //  Created by Arpana on 11/03/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified BBQProfileEditViewController.swift
 //

import UIKit

class BBQProfileEditViewController: BaseViewController {
    
    @IBOutlet weak var femaleImageButton: UIButton!
    @IBOutlet weak var maleImageButton: UIButton!
//    @IBOutlet weak var maleImageView: UIImageView!
//    @IBOutlet weak var femaleImageView: UIImageView!
    var profileView : BBQProfileViewModel?
    @IBOutlet weak var txtFieldName: UITextField!
    private var mobileBottomSheet = BottomSheetController()
    private var imagePicker: BBQImagePicker!
    private var pickedImage: UIImage?
    @IBOutlet weak var pickerViewDate: UIDatePicker!

    @IBOutlet weak var anniversaryDatePicker: UIDatePicker!
    @IBOutlet weak var anniversaryTxtField: UITextField! {
        
        didSet {
            anniversaryTxtField.tintColor = UIColor.lightGray
            anniversaryTxtField.setRightButton(UIImage(named: "CalenderEdit")!)
            
            
            if let button = anniversaryTxtField.rightView?.subviews.last, button.isKind(of: UIButton.self) {
                (button as! UIButton).addTarget(self, action: #selector(self.rightButtonAction), for: .touchUpInside)
                button.tag = 4
            }
        }
    }
    @IBOutlet weak var female: UILabel!
    @IBOutlet weak var maleLabel: UILabel!
    private var selectedBirthDay : String?
    private var selectedAnniversary : String?
    @IBOutlet weak var txtFieldBirthday: UITextField!{
        
        didSet {
            txtFieldBirthday.tintColor = UIColor.lightGray
            txtFieldBirthday.setRightButton(UIImage(named: "CalenderEdit")!)
            
            
            if let button = txtFieldBirthday.rightView?.subviews.last, button.isKind(of: UIButton.self) {
                (button as! UIButton).addTarget(self, action: #selector(self.rightButtonAction), for: .touchUpInside)
                button.tag = 3
            }
        }
    }
    
    @IBAction func GenderMaleSelected(_ sender: Any) {
        self.profileView?.profileDataModel?.title = "Mr"
        maleImageButton.setImage(UIImage(named: "Male"), for: .normal)
        femaleImageButton.setImage(UIImage(named: "Female"), for: .normal)
        maleLabel.textColor = UIColor.hexStringToUIColor(hex: "FA3232")
        female.textColor = UIColor.hexStringToUIColor(hex: "1C2C40")
    }
    
    @IBAction func GenderFemaleSelected(_ sender: Any) {
            self.profileView?.profileDataModel?.title = "Mrs"
            maleImageButton.setImage(UIImage(named: "MaleUnselected"), for: .normal)
            femaleImageButton.setImage(UIImage(named: "FemaleSelected"), for: .normal)
            maleLabel.textColor = UIColor.hexStringToUIColor(hex: "1C2C40")
            female.textColor = UIColor.hexStringToUIColor(hex: "FA3232")
    }
    
    @IBOutlet weak var txtFieldContactNumber: UITextField!{
        
        didSet {
            txtFieldContactNumber.tintColor = UIColor.lightGray
            txtFieldContactNumber.setRightButton( UIImage(named: "TextEdit")!)
            
            
            if let button = txtFieldContactNumber.rightView?.subviews.last, button.isKind(of: UIButton.self) {
                (button as! UIButton).addTarget(self, action: #selector(self.rightButtonAction), for: .touchUpInside)
                button.tag = 1
            }
        }
    }
    @IBOutlet weak var txtFieldEmail: UITextField!{
        
        didSet {
            txtFieldEmail.tintColor = UIColor.lightGray
            txtFieldEmail.setRightButton(UIImage(named: "TextEdit")!)
            
            
            if let button = txtFieldEmail.rightView?.subviews.last, button.isKind(of: UIButton.self) {
                (button as! UIButton).addTarget(self, action: #selector(self.rightButtonAction), for: .touchUpInside)
                button.tag = 2
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("----------------")
        let configuration = BottomSheetConfiguration(animationDuration: 0.4,
                                                     minimumTopSpacing: 0.0)
        self.mobileBottomSheet = BottomSheetController(configuration: configuration)
        pickerViewDate.contentHorizontalAlignment = .leading

        anniversaryDatePicker.contentHorizontalAlignment = .leading
//        if !(self.profileView?.isGuestUser ?? true) {
//            self.loadProfileInformation()
//        }
    }
    private func setupImagePicker() {
        self.imagePicker = BBQImagePicker(presentationController: self, delegate: self)
    }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        setUI()
    }

    func setUI(){
        var isMale = true
        if profileView?.profileDataModel?.title == "Mrs"{
            isMale = false
        }
        if isMale == true {
            
//            maleImageView.image = UIImage.init(named: "Male")
//            femaleImageView.image =  UIImage.init(named: "Female")
            maleImageButton.setTitle("", for: .normal)
            femaleImageButton.setTitle("", for: .normal)
            maleImageButton.setImage(UIImage(named: "Male"), for: .normal)
            femaleImageButton.setImage(UIImage(named: "Female"), for: .normal)
            maleLabel.textColor = UIColor.hexStringToUIColor(hex: "FA3232")
            female.textColor = UIColor.hexStringToUIColor(hex: "1C2C40")

            
        }else{
//            maleImageView.image = UIImage.init(named: "MaleUnselected")
//            femaleImageView.image =  UIImage.init(named: "FemaleSelected")
            maleImageButton.setTitle("", for: .normal)
            femaleImageButton.setTitle("", for: .normal)
            maleImageButton.setImage(UIImage(named: "MaleUnselected"), for: .normal)
            femaleImageButton.setImage(UIImage(named: "FemaleSelected"), for: .normal)
            maleLabel.textColor = UIColor.hexStringToUIColor(hex: "1C2C40")
            female.textColor = UIColor.hexStringToUIColor(hex: "FA3232")
        }
        
        self.txtFieldName.text = profileView?.profileDataModel?.name
        self.txtFieldContactNumber.text = profileView?.profileDataModel?.mobileNumber
        self.txtFieldEmail.text = profileView?.profileDataModel?.email
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
    
        if profileView?.profileDataModel?.dob != "" && profileView?.profileDataModel?.dob != nil {
            
            //MARK: - Fix for DEL-1546
            //            self.pickerViewDate.date = dateFormatter.date(from: profileView?.profileDataModel?.dob ?? dateFormatter.string(from: Date()) )!
            //            pickerViewDate.isHidden = false
            self.pickerViewDate.date = Date.date(from: profileView?.profileDataModel?.dob ?? Date().bbqnDOBFormattedDateString) ?? Date()
            self.txtFieldBirthday.text = self.pickerViewDate.date.bbqnPresentedFormattedDateString
            pickerViewDate.isHidden = true
            //END
        }
        
        if profileView?.profileDataModel?.anniversaryDate != "" && profileView?.profileDataModel?.anniversaryDate != nil {
            //MARK: - Fix for DEL-1546
//            self.anniversaryDatePicker.date = dateFormatter.date(from: profileView?.profileDataModel?.anniversaryDate ?? dateFormatter.string(from: Date()) )!
//            anniversaryDatePicker.isHidden = false
            self.anniversaryDatePicker.date = Date.date(from: profileView?.profileDataModel?.anniversaryDate ?? Date().bbqnDOBFormattedDateString) ?? Date()
            self.anniversaryTxtField.text = self.anniversaryDatePicker.date.bbqnPresentedFormattedDateString
            anniversaryDatePicker.isHidden = true
            //END
        }
        
       // self.pickerViewDate.date =  Date( profileView?.profileDataModel.dob
    }
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension BBQProfileEditViewController {
    
    @IBAction func rightButtonAction(_ sender : UIButton){
        
        //   txtFieldPassword.isSecureTextEntry =  txtFieldPassword.isSecureTextEntry == true ? false :true
        
        if(sender.tag == 1){
            self.showMobileNumberValidatorController()
        }
        else if(sender.tag == 2){
            self.showEmailValidationController()
        }
        else if(sender.tag == 3){
            //MARK: - Fix for DEL-1546
//            pickerViewDate.isHidden = false
            if (!pickerViewDate.isHidden)
            {
                
                txtFieldBirthday.text = pickerViewDate.date.bbqnPresentedFormattedDateString
                profileView?.profileDataModel?.dob = pickerViewDate.date.bbqnDOBFormattedDateString
            }
            else{
                txtFieldBirthday.text = ""
            }
            pickerViewDate.isHidden = !pickerViewDate.isHidden
            //END
            
             
             if #available(iOS 13.4, *) {
                 pickerViewDate.preferredDatePickerStyle = .compact
             }
        }
        else if(sender.tag == 4){
        //   txtFieldDate.text = pickerViewDate.date
            //MARK: - Fix for DEL-1546
//            pickerViewDate.isHidden = false
            if(!anniversaryDatePicker.isHidden)
            {
                anniversaryTxtField.text = anniversaryDatePicker.date.bbqnPresentedFormattedDateString
                profileView?.profileDataModel?.anniversaryDate = anniversaryDatePicker.date.bbqnDOBFormattedDateString
            }
            else{
                anniversaryTxtField.text = ""
            }
            anniversaryDatePicker.isHidden = !anniversaryDatePicker.isHidden
            //END
            
             if #available(iOS 13.4, *) {
                 pickerViewDate.preferredDatePickerStyle = .compact
             }
        }
    }
    
    
    @IBAction func saveButtonPressed(_ sender : UIButton){
        
        self.updateUserInformation(with: txtFieldName.text!)
    }
}
extension BBQProfileEditViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField)-> Bool {
        
//        if textField == txtFieldEmail  ||   textField == txtFieldContactNumber {
//
//            return false
//        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtFieldEmail {
            
            self.showEmailValidationController()
        }
        else if textField == txtFieldContactNumber {
            
            self.showMobileNumberValidatorController()
        }
        else if textField == txtFieldBirthday {
            //MARK: - Fix for DEL-1546
            //            pickerViewDate.isHidden = false
            //END

        //   txtFieldDate.text = pickerViewDate.date
             
             if #available(iOS 13.4, *) {
                 pickerViewDate.preferredDatePickerStyle = .compact
             }
         
        }else if textField == anniversaryTxtField {
            //MARK: - Fix for DEL-1546
            //            anniversaryDatePicker.isHidden = false
            //END

            if #available(iOS 13.4, *) {
                anniversaryDatePicker.preferredDatePickerStyle = .compact
            }
        }
        
    }
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        
        if textField == txtFieldEmail  ||   textField == txtFieldContactNumber  || textField == txtFieldBirthday  || textField == anniversaryTxtField {

        return false
        }
        return true

    }
    
}
extension BBQProfileEditViewController {
    private func showEmailValidationController() {
        let viewController = BBQBottomSheetCorporateViewController()
        viewController.validatorDelegate = self
        viewController.componentTitle = kUpdateEmailTitle
        viewController.currentBottomSheet = self.mobileBottomSheet
        viewController.componentTitleLogo = UIImage(named: Constants.AssetName.bbqAddEmailTitle)
        viewController.emailType = BBQEmailType.personal
        
        let configuration = BottomSheetConfiguration(animationDuration: 0.4,
                                                     minimumTopSpacing: 0.0)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        self.present(viewController, animated: true, completion: nil)
        //bottomSheetController.present(viewController, on: self)

    }
    
    private func showMobileNumberValidatorController() {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "BBQMobileVerifierViewControllerID") as! BBQMobileVerifierViewController
        viewController.verifierDelegate = self
        mobileBottomSheet.present(viewController, on: self)
    }
    
//    private func loadProfileInformation()  {
//
//        BBQActivityIndicator.showIndicator(view: self.view)
//        self.profileView?.getUserProfile { (isFetchedSuccessfully) in
//            if isFetchedSuccessfully {
//
//                /* Commenting for now, device settings only updating when user update profgile manually.
//
//                // If there is conflict btwn device notification settings and back end settings
//                // updating device settings with backend
//                let backEndChoice = self.profileView?.profileData?.isNotificationEnabled?.boolValue
//                if backEndChoice != BBQNotificationManager.shared.isPushEnabledFromSettings {
//                    self.userName = self.profileView?.profileData?.name ?? ""
//                    self.updateUserInformation(with: self.userName)
//                } */
//
//                self.profileTableView.reloadData {
//                    BBQActivityIndicator.hideIndicator(from: self.view)
//                }
//            }
//
//            if BBQUserDefaults.sharedInstance.isGuestUser{
//                 BBQActivityIndicator.hideIndicator(from: self.view)
//            }
//        }
//    }
    
    private func updateUserEmailAddress(with emailID: String, otpID: Int, otp: Int) {
        BBQActivityIndicator.showIndicator(view: self.view)
        AnalyticsHelper.shared.triggerEvent(type: .update_email_status)
        self.profileView?.updateUserEmail(email: emailID,
                                                        otpID: otpID,
                                                        otp: otp) { (isEmailUpdated) in
                                                            BBQActivityIndicator.hideIndicator(from: self.view)
                                                            
                                                            if isEmailUpdated {
                                                                AnalyticsHelper.shared.triggerEvent(type: .update_email__success_status)
                                                                // Update only email field.
                                                              //  self.profileTableView.reloadRows(at: [IndexPath(row: 2, section: 0)],
//                                                                                                 with: UITableView.RowAnimation.fade)
                                                                self.goToPreviousScreen()
                                                             self.showAPICallbackAlert(title: kAPISuccess,
                                                                                          message: kAPIUpdateEmailSuccess)

                                                            } else {
                                                                AnalyticsHelper.shared.triggerEvent(type: .update_email_failure_value)
                                                                self.showAPICallbackAlert(title: kFailed, message: kAPIUpdateEmailFailed)
                                                            }
                            
        }
    }
    
    func goToPreviousScreen(){
        
        if let tabBarController = self.navigationController?.viewControllers{
            for vc in tabBarController {
                if vc.isKind(of: BBQProfileViewController.self){
                    
                    let transition = CATransition()
                    transition.duration = 0.9
                    transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
                    transition.type = CATransitionType.fade
                    //(vc as? OrderHistoryViewController)?.foodratedOrderID = OrderIDForRatedFood
                    self.navigationController?.view.layer.add(transition, forKey: kCATransition)
                    self.navigationController?.popToViewController(vc, animated: false)
                    
                }
            }
        }
    }
    private func showAPICallbackAlert(title: String, message: String) {
        PopupHandler.showSingleButtonsPopup(title: title,
                                            message: message,
                                            on: self, firstButtonTitle: kOkButton) { }
    }
//    private func updateUserProfilePicture(with image: UIImage?, imgExtension: String?) {
//
//
//       // cell?.startImageLoader()
//
//        self.profileView?.updateProfileImage(selectedImage:image ?? UIImage(),
//                                                           imgExtension: imgExtension) { (isUpadated) in
//            if isUpadated {
//               self.loadProfileInformation()
//            }
//        }
//    }
    
    private func updateUserMobileNumber(with countryCode: String,
                                        mobileNumber: Int64,
                                        otpID: Int,
                                        otp: Int) {
        BBQActivityIndicator.showIndicator(view: self.view)
        AnalyticsHelper.shared.triggerEvent(type: .update_mobile_status)
        self.profileView?.updateUserMobile(countryCode: countryCode,
                                                         mobileNumber: mobileNumber,
                                                         otpId: otpID,
                                                         otp: otp) { (isMobileNumberUpdated) in
                                                            BBQActivityIndicator.hideIndicator(from: self.view)
                                                            
                                                            if isMobileNumberUpdated {
                                                                AnalyticsHelper.shared.triggerEvent(type: .update_mobile__success_status)
                                                                // Update only mobile number field.
//                                                                self.profileTableView.reloadRows(at: [IndexPath(row: 1,
//                                                                                                                section: 0)],
//                                                                                                 with: UITableView.RowAnimation.fade)
                                                                self.showAPICallbackAlert(title: kAPISuccess,
                                                                                          message:kAPIUpdateMobileSuccess)
                                                                self.goToPreviousScreen()

                                                            } else {
                                                                AnalyticsHelper.shared.triggerEvent(type: .update_mobile_failure_value)
                                                                self.showAPICallbackAlert(title: kFailed, message: self.profileView?.serverErrorMessage ?? "Failed")
                                                            }
        }
    }
   
}
extension BBQProfileEditViewController :  BBQImagePickerDelegate,
     BBQMobileVerifierViewControllerDelegate, BBQBottomSheetCorporateViewControllerDelegate,
                                         LoginControllerDelegate {
   
    
    
    // MARK:- BBQImagePickerDelegate Methods
    
    func didSelect(image: UIImage?, imgExtension: String?) {
        self.profileView?.selectedProfileImage = image ?? nil
       // self.updateUserProfilePicture(with: image, imgExtension: imgExtension)
    }
    
    // MARK:- BBQProfileHeaderTableViewCellDelegate Methods
    
    func didPressedImagePickerOption(_ sender: UIButton) {
        self.imagePicker.present(from: sender)
    }
    
    func didPressedSignupOption(_ sender: UIButton) {
        // Not presenting in bottom sheet since we have incompatability issue with Logincontroller
//        let controller = UIStoryboard.loadLoginViewController()
//        controller.delegate = self
//        controller.subView?.backgroundColor = .white
//        controller.shouldCloseTouchingOutside = true
//        controller.currentTheme = .dark
//        controller.navigationFromScreen = .Profile
//        controller.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
  //      self.present(controller, animated: true, completion: nil)
        let tabViewController = UIStoryboard.loadLoginThroughPhoneController()
        tabViewController.navigationFromScreen = .Profile
        tabViewController.delegate = self
        
       self.navigationController?.pushViewController(tabViewController, animated: false)
    }
   
    
    func didVerifiedMobileOTP(countryCode: String, updatedNumber: String, otpID: String, verifierOTP: String) {
        
        let fullNumber = "\(countryCode) \(updatedNumber)"
       // self.editedMobileNumberFull = fullNumber
        self.profileView?.updatedNumber = fullNumber

        self.updateUserMobileNumber(with: countryCode,
                                    mobileNumber: Int64(updatedNumber) ?? 0,
                                    otpID: Int(otpID) ?? 0,
                                    otp: Int(verifierOTP) ?? 0)
    }
    
    
    func didFinishSelectingCorporateOffer(corporateOffer: CorporateOffer) {
        
    }
    func didFinishedEmailValidation(emailId: String, otpID: Int, verifierOTP: Int) {
        self.profileView?.updatedEmailID = emailId
        self.updateUserEmailAddress(with: emailId, otpID: otpID, otp: verifierOTP)
        
        mobileBottomSheet.dismissBottomSheet(on: self)
    }
    
    func showRegistrationConfirmationPage(navigationFrom: LoginNavigationScreen) {
        let signupVC = UIStoryboard.loadRegistrationConfirmation()
        signupVC.modalPresentationStyle = .overFullScreen
        signupVC.titleText = kSignUpConfirmationTitleText
        signupVC.navigationText = kSignUpConfirmationNavigationText
        self.present(signupVC, animated: false) {
            sleep(UInt32(LoginBackground.constantValues.confirmationScreenDisplayWaitTime.rawValue))
            self.dismiss(animated: false) {
                /*let homeViewController = UIStoryboard.loadHomeViewController()
                homeViewController.navigationFromScreen = navigationFrom
                self.navigationController?.pushViewController(homeViewController, animated: true)*/
                let tabBarController = DeliveryTabBarController()
                tabBarController.navigationFromScreen = navigationFrom
                self.navigationController?.pushViewController(tabBarController, animated: true)
            }
        }
    }
    
}
extension BBQProfileEditViewController {
    
    private func updateUserInformation(with name: String) {
        
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ssZ"
    
        if let date = pickerViewDate.date as? Date {
            selectedBirthDay = dateFormatter.string(from: date)
        }
        
        if let dateAni = anniversaryDatePicker.date  as? Date{
            selectedAnniversary = dateFormatter.string(from: dateAni)
        }
        
        let dateOfBirthFromString = dateFormatter.date(from: selectedBirthDay ?? "")
        let dateOfAnniversaryFromString = dateFormatter.date(from: selectedAnniversary ?? "")

        
        dateFormatter.dateFormat = " yyyy-MM-dd"

        
        if dateOfBirthFromString != nil  {
            
            if let date = dateOfBirthFromString {
                selectedBirthDay = dateFormatter.string(from: date)
            }
        }
      
        if dateOfAnniversaryFromString != nil  {
            
            if let date = dateOfAnniversaryFromString {
                selectedAnniversary = dateFormatter.string(from: date)
            }
        }
        
        // Selecting default values when edit data not available for fields.
        AnalyticsHelper.shared.triggerEvent(type: .H04)
        let title = self.profileView?.profileData?.title ?? "Mr"
        let newsLetterOption = self.profileView?.profileData?.newsletter ?? "1"
        let maritalStatus = self.profileView?.profileData?.maritalStatus ?? "1"
        let selectedBday = self.selectedBirthDay ?? self.profileView?.profileData?.dob ?? ""
        let selectedAnniversary = self.selectedAnniversary ?? self.profileView?.profileData?.anniversaryDate ?? ""
        let formattedName = name.trimmingCharacters(in: .whitespaces)
        
        //    dob: self.profileView?.requestFormmatedDateString(with: selectedBday) ?? "",
   // anniversary:self.profileView?.requestFormmatedDateString(with: selectedAnniversary) ?? "",
        let notificationChoice = BBQNotificationManager.shared.isPushEnabledFromSettings
        AnalyticsHelper.shared.triggerEvent(type: .update_profile_status)
        BBQActivityIndicator.showIndicator(view: self.view)
        self.profileView?.updateUser(title: title,
                                                   newsLetter: newsLetterOption,
                                                   maritalStatus: maritalStatus,
                                                   name: formattedName,
                                     dob: selectedBday,
                                     anniversary: selectedAnniversary ,
                                                   isNotificationEnabled: notificationChoice
        ) { (isUpdated) in
            AnalyticsHelper.shared.triggerEvent(type: .update_profile__success_status)
            BBQActivityIndicator.hideIndicator(from: self.view)
            
            if isUpdated {
                AnalyticsHelper.shared.triggerEvent(type: .P04A)
                self.showAPICallbackAlert(title: kAPISuccess,
                                          message:kAPIUpdateUserSuccess)
                BBQUserDefaults.sharedInstance.UserName = name
                self.goToPreviousScreen()
            } else {
                AnalyticsHelper.shared.triggerEvent(type: .P04B)
                self.showAPICallbackAlert(title: kFailed, message: kAPIUpdateUserFailed)
            }
        }
    }
}
