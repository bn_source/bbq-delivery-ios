//
 //  Created by Ajith CP on 13/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQMobileVerifierActionTableViewCell.swift
 //

import UIKit

protocol BBQMobileVerifierActionTableViewCellDelegate {
    
    func didPressedNextButton()
    
    func didPressedBackButton()
    
    func didPressedDoneButton()
}

class BBQMobileVerifierActionTableViewCell: UITableViewCell {

    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    
    var actionDelegate : BBQMobileVerifierActionTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let buttonAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.black,
                                                             .underlineStyle: NSUnderlineStyle.single.rawValue]
        let attributeString = NSMutableAttributedString(string: kSendOtp,
                                                        attributes: buttonAttributes)
        self.nextButton.setAttributedTitle(attributeString, for: .normal)
    }
    
    func loadActionContent(isVerified: Bool)  {
        self.nextButton.isHidden = isVerified
        self.doneButton.isHidden = !isVerified
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        self.actionDelegate?.didPressedNextButton()
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.actionDelegate?.didPressedBackButton()
    }
    
    @IBAction func doneButtonPressed(_ sender: Any) {
        self.actionDelegate?.didPressedDoneButton()
    }
}
