//
//  Created by Ajith CP on 05/08/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQProfileEntriesTableViewCell.swift
//

import UIKit

protocol BBQProfileEntriesTableViewCellDelegate {
    func didSelectNotificationOption(switchOn: Bool)
}

class BBQProfileEntriesTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var entriesIconImageView: UIImageView!
    
    @IBOutlet weak var entriesDescriptionLabel: UILabel!
    @IBOutlet weak var viewForBackground: UIView!
    @IBOutlet weak var switchButton: UIButton!
    
    @IBOutlet weak var titleLabel: UILabel!
    var isNotificationsEnabled : Bool = true
    var isEditMode: Bool = false
    
    var entriesCellDelegate: BBQProfileEntriesTableViewCellDelegate?
    
    // MARK: View Model
    public var viewModel: BBQProfileDetailViewModel? {
        didSet {
            //set the data here to respective cells accordindly
        }
    }
    
    // MARK:- Life Cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
    
    // MARK: - Public Methods
    
    public func loadProfileEntries(with viewModel: BBQProfileViewModel,
                                   for index: IndexPath, isEditing: Bool)  {
        
        switch index.row {
        case EntryType.notificationsChoice.rawValue:
            self.switchButton.isHidden = false
        default:
            self.switchButton.isHidden = true
        }
        
        self.updateUI(with: viewModel, for: index.row,  isEditing: isEditing)
    }

    // MARK: - IBActions
    
    @IBAction func notificationSwitchPressed(_ sender: UIButton) {
        
        /*if !self.isEditMode && !BBQUserDefaults.sharedInstance.isGuestUser {
            return // Returning without any action if non edit mode
        } */
        
        // Enabling edit notification settinghs always.
        self.entriesCellDelegate?.didSelectNotificationOption(switchOn: isNotificationsEnabled)
    }
    
    
    // MARK:- Private Methods
    
    private func updateUI(with viewModel: BBQProfileViewModel, for index: Int, isEditing: Bool)  {
       
        self.isEditMode = isEditing
        self.entriesDescriptionLabel.textColor = UIColor.hexStringToUIColor(hex: "55606D")
        self.isNotificationsEnabled = BBQNotificationManager.shared.isPushEnabledFromSettings
        self.switchButton.isSelected = self.isNotificationsEnabled
        self.switchButton.setBackgroundImage(UIImage(named: Constants.AssetName.notifSwitchOff), for: .normal)
        self.switchButton.setBackgroundImage(UIImage(named: Constants.AssetName.notifSwitchOn), for: .selected)
        //self.switchButton.alpha = self.isEditMode ? 1 : 0.5

        self.entriesDescriptionLabel.text =
            viewModel.profileEntriesDescription(for: index).description
        self.titleLabel.text =  viewModel.profileEntriesDescription(for: index).titleText
        if index == 6 {
            self.entriesDescriptionLabel.textColor =  UIColor.hexStringToUIColor(hex: "E53535")
            if #available(iOS 13.0, *) {
                self.entriesIconImageView.image =
                UIImage(systemName: "trash")
                self.entriesIconImageView.tintColor =  .deliveryThemeColor
                
//                self.contentView.clipsToBounds = true
//                self.contentView.layer.cornerRadius = 50
//                self.contentView.layer.maskedCorners = [ .layerMinXMinYCorner, .layerMinXMaxYCorner]
                
            } else {
                // Fallback on earlier versions
                self.entriesIconImageView.image = UIImage(named: viewModel.profileEntriesDescription(for: index).imageName)

            }
        }else{
            self.entriesIconImageView.image =
            UIImage(named: viewModel.profileEntriesDescription(for: index).imageName)
        }
    }
}

extension Int {
   
    var boolValue: Bool { return self != 0 }
    
}
