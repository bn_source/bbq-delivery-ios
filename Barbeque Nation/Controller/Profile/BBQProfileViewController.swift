//
//  Created by Ajith CP on 29/07/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQProfileViewController.swift
//

import UIKit
import FirebaseAnalytics

class BBQProfileViewController: BBQBaseViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var profileTableView: UITableView!
    @IBOutlet var keyboardHeightLayoutConstraint: NSLayoutConstraint?
    @IBOutlet weak var floatingBackButton: UIButton!
    
    private var mobileBottomSheet = BottomSheetController()
    private var imagePicker: BBQImagePicker!
    private var pickedImage: UIImage?
    
    private var selectedBirthDay : String?
    private var selectedAnniversary : String?
    
    private var userName : String = ""
    private var editedMobileNumber  : String = ""
    private var editedMobileNumberFull  : String = ""
    private var editedEmail : String = ""
    private var isNotificationEnabled = true
    private var tapOutside: UITapGestureRecognizer? = nil
    var bottomSheetController: BottomSheetController?

    var doStartAnimation : Bool?
    var isEditMode       : Bool = false
    
    // MARK: - Lazy Initializer
    
    lazy private var profileControllerViewModel : BBQProfileViewModel = {
        let profileVM = BBQProfileViewModel()
        return profileVM
    } ()
    
    lazy private var verifierViewModel : BBQMobileVerifierViewModel = {
        let verifierVM = BBQMobileVerifierViewModel()
        return verifierVM
    } ()
    
    // MARK: - Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        AnalyticsHelper.shared.screenName(name: "ProfileScreen", className: "BBQProfileViewController")
//        Analytics.logEvent("Click on Profile", parameters: [
//            "name": "Profile Check" as NSObject,
//            "full_text": "Profile page" as NSObject
//            ])
//
        AnalyticsHelper.shared.triggerEvent(type: .P01)
       // self.setupHeaderView()
        //self.setupMenuHeader()
        self.setupProfileTableView()
        self.setupImagePicker()
        self.setupNotificationCenter()
        
        let configuration = BottomSheetConfiguration(animationDuration: 0.4,
                                                     minimumTopSpacing: 0.0)
        self.mobileBottomSheet = BottomSheetController(configuration: configuration)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.screenName(name: .Profile_Screen)
        if !self.profileControllerViewModel.isGuestUser {
            self.loadProfileInformation()
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    // MARK: - Setup Methods
    
    private func setupImagePicker() {
        self.imagePicker = BBQImagePicker(presentationController: self, delegate: self)
    }
    
    private func setupMenuHeader() {
        headerView.pointsButton.isHidden = true
        headerView.goldMemberButton.isHidden = true
    }
    
    private func setupHeaderView()  {
        self.doStartAnimation = false
        self.isEditMode = false
        
        self.headerView.nameLabel.isHidden = true
    }
    
    func setupProfileTableView() {
        
        self.profileTableView.estimatedRowHeight = Constants.App.ProfilePage.headerEstimatedRowHeight
        self.profileTableView.rowHeight = UITableView.automaticDimension
      
        self.profileTableView.register(UINib(nibName: Constants.NibName.profileHaderCellNibName,
                                             bundle: nil),
                                       forCellReuseIdentifier: Constants.CellIdentifiers.profileHaderCellID)
        self.profileTableView.register(UINib(nibName:  Constants.NibName.profileEntriesCellNibName,
                                             bundle: nil),
                                       forCellReuseIdentifier: Constants.CellIdentifiers.profileEntriesCellID)
        self.profileTableView.register(UINib(nibName:  Constants.NibName.profileEntriesEditCellNibName,
                                             bundle: nil),
                                       forCellReuseIdentifier: Constants.CellIdentifiers.profileEntriesEditCellID)
        
    }
    
    
    func setupNotificationCenter() {
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(BBQProfileViewController.appMovedToForeground),
                                               name:UIApplication.willEnterForegroundNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }
    
    // MARK:- API Methods
    
    private func loadProfileInformation()  {
        
        BBQActivityIndicator.showIndicator(view: self.view)
        self.profileControllerViewModel.getUserProfile { (isFetchedSuccessfully) in
            if isFetchedSuccessfully {
                
                /* Commenting for now, device settings only updating when user update profgile manually.
                
                // If there is conflict btwn device notification settings and back end settings
                // updating device settings with backend
                let backEndChoice = self.profileControllerViewModel.profileData?.isNotificationEnabled?.boolValue
                if backEndChoice != BBQNotificationManager.shared.isPushEnabledFromSettings {
                    self.userName = self.profileControllerViewModel.profileData?.name ?? ""
                    self.updateUserInformation(with: self.userName)
                } */
                
                self.profileTableView.reloadData {
                    BBQActivityIndicator.hideIndicator(from: self.view)
                }
            }
            
            if BBQUserDefaults.sharedInstance.isGuestUser{
                 BBQActivityIndicator.hideIndicator(from: self.view)
            }
        }
    }

    private func updateUserInformation(with name: String) {
        
        // Selecting default values when edit data not available for fields.
        AnalyticsHelper.shared.triggerEvent(type: .H04)
        let title = self.profileControllerViewModel.profileData?.title ?? "Mr"
        let newsLetterOption = self.profileControllerViewModel.profileData?.newsletter ?? "1"
        let maritalStatus = self.profileControllerViewModel.profileData?.maritalStatus ?? "1"
        let selectedBday = self.selectedBirthDay ?? self.profileControllerViewModel.profileData?.dob ?? ""
        let selectedAnniversary = self.selectedAnniversary ?? self.profileControllerViewModel.profileData?.anniversaryDate ?? ""
        let formattedName = name.trimmingCharacters(in: .whitespaces)
        
        let notificationChoice = BBQNotificationManager.shared.isPushEnabledFromSettings
        AnalyticsHelper.shared.triggerEvent(type: .update_profile_status)
        BBQActivityIndicator.showIndicator(view: self.view)
        self.profileControllerViewModel.updateUser(title: title,
                                                   newsLetter: newsLetterOption,
                                                   maritalStatus: maritalStatus,
                                                   name: formattedName,
                                                   dob: self.profileControllerViewModel.requestFormmatedDateString(with: selectedBday),
                                                   anniversary:self.profileControllerViewModel.requestFormmatedDateString(with: selectedAnniversary),
                                                   isNotificationEnabled: notificationChoice
        ) { (isUpdated) in
            AnalyticsHelper.shared.triggerEvent(type: .update_profile__success_status)
            BBQActivityIndicator.hideIndicator(from: self.view)
            
            if isUpdated {
                AnalyticsHelper.shared.triggerEvent(type: .P04A)
                self.showAPICallbackAlert(title: kAPISuccess,
                                          message:kAPIUpdateUserSuccess)
                BBQUserDefaults.sharedInstance.UserName = name
                self.loadProfileInformation()
            } else {
                AnalyticsHelper.shared.triggerEvent(type: .P04B)
                self.showAPICallbackAlert(title: kFailed, message: kAPIUpdateUserFailed)
            }
        }
    }
    
    private func updateUserMobileNumber(with countryCode: String,
                                        mobileNumber: Int64,
                                        otpID: Int,
                                        otp: Int) {
        BBQActivityIndicator.showIndicator(view: self.view)
        AnalyticsHelper.shared.triggerEvent(type: .update_mobile_status)
        self.profileControllerViewModel.updateUserMobile(countryCode: countryCode,
                                                         mobileNumber: mobileNumber,
                                                         otpId: otpID,
                                                         otp: otp) { (isMobileNumberUpdated) in
                                                            BBQActivityIndicator.hideIndicator(from: self.view)
                                                            
                                                            if isMobileNumberUpdated {
                                                                AnalyticsHelper.shared.triggerEvent(type: .update_mobile__success_status)
                                                                // Update only mobile number field.
                                                                self.profileTableView.reloadRows(at: [IndexPath(row: 1,
                                                                                                                section: 0)],
                                                                                                 with: UITableView.RowAnimation.fade)
                                                                self.showAPICallbackAlert(title: kAPISuccess,
                                                                                          message:kAPIUpdateMobileSuccess)
                                                            } else {
                                                                AnalyticsHelper.shared.triggerEvent(type: .update_mobile_failure_value)
                                                                self.showAPICallbackAlert(title: kFailed, message: self.profileControllerViewModel.serverErrorMessage)
                                                            }
        }
    }
    
    private func updateUserEmailAddress(with emailID: String, otpID: Int, otp: Int) {
        BBQActivityIndicator.showIndicator(view: self.view)
        AnalyticsHelper.shared.triggerEvent(type: .update_email_status)
        self.profileControllerViewModel.updateUserEmail(email: emailID,
                                                        otpID: otpID,
                                                        otp: otp) { (isEmailUpdated) in
                                                            BBQActivityIndicator.hideIndicator(from: self.view)
                                                            
                                                            if isEmailUpdated {
                                                                AnalyticsHelper.shared.triggerEvent(type: .update_email__success_status)
                                                                // Update only email field.
                                                                self.profileTableView.reloadRows(at: [IndexPath(row: 2, section: 0)],
                                                                                                 with: UITableView.RowAnimation.fade)
                                                                self.showAPICallbackAlert(title: kAPISuccess,
                                                                                          message: kAPIUpdateEmailSuccess)
                                                            } else {
                                                                AnalyticsHelper.shared.triggerEvent(type: .update_email_failure_value)
                                                                self.showAPICallbackAlert(title: kFailed, message: kAPIUpdateEmailFailed)
                                                            }
                            
        }
    }
    
    private func updateUserProfilePicture(with image: UIImage?, imgExtension: String?) {
        
        // Adding spinner to profile image while updating
        let cell = self.profileTableView.cellForRow(at: IndexPath(row: 0, section: 0))
            as? BBQProfileHeaderTableViewCell
        cell?.startImageLoader()

        self.profileControllerViewModel.updateProfileImage(selectedImage:image ?? UIImage(),
                                                           imgExtension: imgExtension) { (isUpadated) in
            if isUpadated {
               self.loadProfileInformation()
            }
        }
    }
    
    private func showAPICallbackAlert(title: String, message: String) {
        PopupHandler.showSingleButtonsPopup(title: title,
                                            message: message,
                                            on: self, firstButtonTitle: kOkButton) { }
    }
    
    // MARK:- IBActions
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func deleteProfile() {
        AnalyticsHelper.shared.triggerEvent(type: .verify_user_status)
        
        let isMobileValid = BBQPhoneNumberValidation.isValidNumber(number:self.profileControllerViewModel.profileData?.mobileNumber ?? "000000000",
                                                                   for: BBQUserDefaults.sharedInstance.phoneCode)
        
        
        if isMobileValid {
            self.verifierViewModel.generateMobileOTP(countryCode: BBQUserDefaults.sharedInstance.phoneCode,
                                                     mobileNumber:Int64(self.profileControllerViewModel.profileData?.mobileNumber ?? "000000000") ?? Int64(0.0) ,
                                                     otpId: self.verifierViewModel.otpIdentifier) { (isGenerated) in
                if isGenerated {
                    self.goToOTPVerifierScreen()
                }
                BBQActivityIndicator.hideIndicator(from: self.view)
            }
        } else {
            UIUtils.showToast(message: "User's mobile number is invalid")
        }
    }
   
    private func goToOTPVerifierScreen() {
        let validatorVC = UIStoryboard.loadOTPForDeleteViewController()
        validatorVC.delegate = self
        validatorVC.otpType = .others
        validatorVC.currentTheme = .dark
        validatorVC.screenHeight = 400
        validatorVC.validationPath = self.profileControllerViewModel.profileData?.mobileNumber ?? ""
        validatorVC.otpID = verifierViewModel.otpIdentifier
        validatorVC.validationType = .mobileNumber
        validatorVC.header = kDeleteAccountString
        validatorVC.otpMessage =  kOTPSentNotificvation
        validatorVC.logoImage = "deleteImageIcon" // Constants.AssetName.kAddMobileLogo
        validatorVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        let configuration = BottomSheetConfiguration(animationDuration: 0.4,
                                                     minimumTopSpacing: 0.0)
        let bottomSheetController = BottomSheetController(configuration: configuration)
        bottomSheetController.present(validatorVC, on: self)
    }

}

extension BBQProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.profileControllerViewModel.profileTableEntriesCount()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        view.backgroundColor = .clear
        view.tintColor = .clear
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case  6 : //delete account
            
            self.showDeleteAccountWarning { [self] (doReset) in
                if doReset {
                    //To check for user has logged in or not
                    if BBQUserDefaults.sharedInstance.isGuestUser {
                        // self.navigateToLogin()
                    } else {
                        self.deleteProfile()
                    }
                }
            }
            
        default : print("not delete account")
        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if isAppReferAndEarnActivated == true {
            
            return 80 // 80 Keeping the floating button below while scrolling.
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0 :
            return self.loadProfileHeaderTableCell(indexPath: indexPath)
            
        case 1, 2, 3, 4 : // Mobile, email, birthday, dob are editable.
            
            if profileControllerViewModel.isGuestUser {
                return self.loadProfileEntryEditTableCell(indexPath: indexPath)
            } else if self.isEditMode && indexPath.row != 0 {
                return self.loadProfileEntryEditTableCell(indexPath: indexPath)
            }
            return self.loadProfileEntriesTableCell(indexPath: indexPath)
            
        default :
            return self.loadProfileEntriesTableCell(indexPath: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        if isAppReferAndEarnActivated == true {
            let footerView  = AppReferralView.init()
            footerView.delegetToReferal = self
            return footerView
        }else{
            return nil
        }
    }
    // MARK: - Private Methods
    
    
    private func showDeleteAccountWarning(completion: @escaping (_ doReset: Bool)->()) {
        if !(profileControllerViewModel.isGuestUser) {
            
                PopupHandler.showTwoButtonsPopup(title: "Are you sure you want to delete your account?",
                                                 message: "You can retrieve your Smiles Coins balance, Happiness Meal Cards and transaction history after signing up again.",
                                                 on: self, firstButtonTitle: "Cancel",
                                                 firstAction: {
                    completion(false)
                    
                }, secondButtonTitle: "OK", secondAction: {
                    completion(true)
                })
            } else {
                completion(true)
            }
        }
    
    /* Load different cells for tableview */
    
    private func loadProfileHeaderTableCell(indexPath: IndexPath) -> BBQProfileHeaderTableViewCell {
        let headerCell =
            self.profileTableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.profileHaderCellID)
                as! BBQProfileHeaderTableViewCell
        headerCell.loadProfileHeaderCell(with: self.profileControllerViewModel,
                                         isEditMode: self.isEditMode)
        headerCell.headerViewDelegate = self
        return headerCell
    }
    
    private func loadProfileEntriesTableCell(indexPath: IndexPath) -> BBQProfileEntriesTableViewCell {
        let entriesCell =
            self.profileTableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.profileEntriesCellID)
                as! BBQProfileEntriesTableViewCell
        entriesCell.loadProfileEntries(with: self.profileControllerViewModel,
                                       for: indexPath,
                                       isEditing: self.isEditMode)
        entriesCell.viewModel = profileControllerViewModel.profileDetailViewModel()
        entriesCell.entriesCellDelegate = self
        return entriesCell
    }
    
    private func loadProfileEntryEditTableCell(indexPath: IndexPath) -> BBQProfileEditEntryTableViewCell {
        
        let editCell =
            self.profileTableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.profileEntriesEditCellID)
                as! BBQProfileEditEntryTableViewCell
        editCell.editDelegate = self
        editCell.editTextField.isEnabled = !profileControllerViewModel.isGuestUser
        editCell.loadContent(with: self.profileControllerViewModel, for: indexPath)
        
        return editCell
    }
    
}


extension BBQProfileViewController : BBQProfileHeaderTableViewCellDelegate, BBQImagePickerDelegate,
    BBQProfileEntriesTableViewCellDelegate, BBQProfileEditEntryTableViewCellDelegate,
    BBQMobileVerifierViewControllerDelegate, BBQBottomSheetCorporateViewControllerDelegate,
LoginControllerDelegate {
    func didFinishSelectingCorporateOffer(corporateOffer: CorporateOffer) {
        
    }
    
    
    
    // MARK:- BBQImagePickerDelegate Methods
    
    func didSelect(image: UIImage?, imgExtension: String?) {
        self.profileControllerViewModel.selectedProfileImage = image ?? nil
        self.updateUserProfilePicture(with: image, imgExtension: imgExtension)
    }
    
    // MARK:- BBQProfileHeaderTableViewCellDelegate Methods
    
    func didPressedImagePickerOption(_ sender: UIButton) {
        self.imagePicker.present(from: sender)
    }
    
    func didPressedSignupOption(_ sender: UIButton) {
        // Not presenting in bottom sheet since we have incompatability issue with Logincontroller
//        let controller = UIStoryboard.loadLoginViewController()
//        controller.delegate = self
//        controller.subView?.backgroundColor = .white
//        controller.shouldCloseTouchingOutside = true
//        controller.currentTheme = .dark
//        controller.navigationFromScreen = .Profile
//        controller.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//        self.present(controller, animated: true, completion: nil)
        let loginViewcontroller = UIStoryboard.loadLoginThroughPhoneController()
        loginViewcontroller.navigationFromScreen = .Profile
        loginViewcontroller.delegate = self
        self.navigationController?.pushViewController(loginViewcontroller, animated: false)
    }
    
    func didPressBackButton(){
        
        self.navigationController?.popViewController(animated: true)

    }
    func didPressedEditProfile(with name: String) {
        if !isEditMode{
            AnalyticsHelper.shared.triggerEvent(type: .P03)
        }
        
        
        let offerListVC = UIStoryboard.loadProfileEditViewController(model : profileControllerViewModel)
//        offerListVC.cms_offer_id = cms_offer_id
//        offerListVC.image_url = image_url
//        offerListVC.isServicable = viewModel.isServicable
//        UIUtils.showLoader()
        
       // UIUtils.hideLoader()
      //  offerListVC.offerListItems = arrItems
        self.navigationController?.pushViewController(offerListVC, animated: true)
        //return
        
//        self.isEditMode = !self.isEditMode
//        if !self.isEditMode {
//            // After edit mode we have to update API with new values.
//            // Running all APIs for update in dispatch group
//            self.view.endEditing(true)
//            self.userName = name
//            self.updateUserInformation(with: name)
//        }
//        self.profileTableView.reloadData()
    }
    
    func didPressedLoyaltyPoints() {
        if self.isEditMode {
            ToastHandler.showToastWithMessage(message: kSaveChanges)
            return
        }
        
        AnalyticsHelper.shared.triggerEvent(type: .P02)
        let  loyalityPointsViewController = UIStoryboard.loadLoyalityPointsViewController()
        self.navigationController?.pushViewController(loyalityPointsViewController,
                                                      animated: true)
       
    }
    
    // MARK:- BBQProfileEntriesTableViewCellDelegate Methods
    
    func didSelectNotificationOption(switchOn: Bool) {
        BBQNotificationManager.shared.openDeviceSettings(isDisabled: BBQNotificationManager.shared.isPushEnabledFromSettings)
        self.isNotificationEnabled = BBQNotificationManager.shared.isPushEnabledFromSettings 
    }
    
    // MARK:- BBQMobileVerifierViewControllerDelegate Methods
    
    func didVerifiedMobileOTP(countryCode: String,
                              updatedNumber: String,
                              otpID: String,
                              verifierOTP: String) {
        
        let fullNumber = "\(countryCode) \(updatedNumber)"
        self.editedMobileNumberFull = fullNumber
        self.profileControllerViewModel.updatedNumber = fullNumber

        self.updateUserMobileNumber(with: countryCode,
                                    mobileNumber: Int64(updatedNumber) ?? 0,
                                    otpID: Int(otpID) ?? 0,
                                    otp: Int(verifierOTP) ?? 0)
        
    }
    
    // MARK:- BBQBottomSheetCorporateViewControllerDelegate Methods
    
    func didFinishedEmailValidation(emailId: String, otpID: Int, verifierOTP: Int) {
        self.profileControllerViewModel.updatedEmailID = emailId
        self.updateUserEmailAddress(with: emailId, otpID: otpID, otp: verifierOTP)
        
        mobileBottomSheet.dismissBottomSheet(on: self)
    }
    
    // MARK:- BBQProfileEditEntryTableViewCellDelegate Methods
    
    func editFieldValueChanged(with type:EditFieldType, newValue: String) {
        
        switch type {
        case .mobileField:
            self.editedMobileNumberFull = newValue
            self.showMobileNumberValidatorController()
        case .emailField:
            self.editedEmail = newValue
            self.showEmailValidationController()
        case .anniversaryField:
            self.selectedAnniversary = newValue
        default:
            self.selectedBirthDay = newValue
        }
    }
    
    // MARK:- LoginControllerDelegate Methods
    
    func gotoHomeScreen(isExistingUser: Bool, navigationFrom: LoginNavigationScreen) {
        self.loadProfileInformation()
        if !isExistingUser {
            self.showRegistrationConfirmationPage(navigationFrom: navigationFrom)
        }
        self.doPostAccountProcessing()
    }
    
    // MARK:- Private Methods
    private func showMobileNumberValidatorController() {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "BBQMobileVerifierViewControllerID") as! BBQMobileVerifierViewController
        viewController.verifierDelegate = self
        mobileBottomSheet.present(viewController, on: self)
    }
    
    
    private func showEmailValidationController() {
        let viewController = BBQBottomSheetCorporateViewController()
        viewController.validatorDelegate = self
        viewController.componentTitle = kUpdateEmailTitle
        viewController.currentBottomSheet = self.mobileBottomSheet
        viewController.componentTitleLogo = UIImage(named: Constants.AssetName.bbqAddEmailTitle)
        viewController.emailType = BBQEmailType.personal
        
//        let configuration = BottomSheetConfiguration(animationDuration: 0.4,
//                                                     minimumTopSpacing: 0.0)
//        let bottomSheetController = BottomSheetController(configuration: configuration)
        self.present(viewController, animated: true, completion: nil)
        //bottomSheetController.present(viewController, on: self)

    }
    
    func showRegistrationConfirmationPage(navigationFrom: LoginNavigationScreen) {
        let signupVC = UIStoryboard.loadRegistrationConfirmation()
        signupVC.modalPresentationStyle = .overFullScreen
        signupVC.titleText = kSignUpConfirmationTitleText
        signupVC.navigationText = kSignUpConfirmationNavigationText
        self.present(signupVC, animated: false) {
            sleep(UInt32(LoginBackground.constantValues.confirmationScreenDisplayWaitTime.rawValue))
            self.dismiss(animated: false) {
                /*let homeViewController = UIStoryboard.loadHomeViewController()
                homeViewController.navigationFromScreen = navigationFrom
                self.navigationController?.pushViewController(homeViewController, animated: true)*/
                self.goToHomePage()
            }
        }
    }
    
}

extension BBQProfileViewController {
    
    // MARK:- Notification Recievers
    
    // This is for handling date picker presentation and dismissal.
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.profileTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height + 50, right: 0)
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        if let _ = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.profileTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    
    
    @objc func appMovedToForeground() {
        // Reloading tableview after app went background and coming back to foreground to reflect
        // device settings changes.
        /* Commenting this now since device settings is not relevant for back end to handkle notification.
        self.userName = self.profileControllerViewModel.profileData?.name ?? ""
        self.updateUserInformation(with: self.userName)
        */
        
        // Reloading for notification settings change
        self.profileTableView.reloadData {
            
        }
    }
}
extension BBQProfileViewController : BBQOTPScreenDelegate {
    
    func didPressedNextButton() {
//        AnalyticsHelper.shared.triggerEvent(type: .verify_user_status)
        let isMobileValid = BBQPhoneNumberValidation.isValidNumber(number: "String(self.updatedMobileNumber)",
                                                                  for:"sdf")
//        if isMobileValid {
//            self.updatePhoneNumberCell(showError: false)
//            BBQActivityIndicator.showIndicator(view: self.view)
//
//            // 1. Verifying user mobile first
//            // 2. If existing user mobile, blocks to update mobile, showing error
//            // 3. New user generates OTP and gpto OTP page
//            self.verifierViewModel.verifyUser(countryCode: self.countryCode,
//                                              mobileNumber: self.updatedMobileNumber,
//                                              otpGenerate: false) { (isExistingUser) in
//                                                BBQActivityIndicator.hideIndicator(from: self.view)
//                                                if isExistingUser {
//                                                    self.updatePhoneNumberCell(showError: true, text: kAlreadyRegistredMobile)
//                                                } else {
//                                                    self.verifierViewModel.generateMobileOTP(countryCode: self.countryCode,
//                                                                                             mobileNumber: self.updatedMobileNumber,
//                                                                                             otpId: self.verifierViewModel.otpIdentifier) { (isGenerated) in
//                                                                                                if isGenerated {
//                                                                                                    self.goToOTPVerifierScreen()
//                                                                                                }
//                                                                                                BBQActivityIndicator.hideIndicator(from: self.view)
//                                                    }
//                                                }
//            }
//
//            self.view.endEditing(true) // Dismissing keyboard for listening server messages.
//
//        } else {
//            self.updatePhoneNumberCell(showError: true, text: kInValidMobile)
//        }
    }
    
    func didPressedBackButton() {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK:- OTPScreen Delegate Methods
    
    func otpValidation(status: Bool, otp: String) {

        if status == true {
            
            BBQActivityIndicator.showIndicator(view: self.view)
            self.profileControllerViewModel.deleteUser(otp: Int(otp) ?? 0, otpId: Int(self.verifierViewModel.otpIdentifier) ?? 0 ) { (isUpdated) in
                
                DispatchQueue.main.async {
                    
                    if isUpdated {
                        AnalyticsHelper.shared.triggerEvent(type: .profile__delete_status)
                        BBQActivityIndicator.hideIndicator(from: self.view)
                        
                        if isUpdated {
                            ToastHandler.showToastWithMessage(message: self.profileControllerViewModel.responseMessage)
                            BBQUserDefaults.sharedInstance.clearAppDefaults()
                            self.goToLoginPage()
                        } else {
                            AnalyticsHelper.shared.triggerEvent(type: .P04B)
                            ToastHandler.showToastWithMessage(message: self.profileControllerViewModel.responseMessage )
                            
                        }
                    }
                    else {
                        ToastHandler.showToastWithMessage(message: "Account could not be deleted")
                        
                        
                    }
                }
            }
        }
    }
    
    func backButtonPressed() {
       
    }
    
}
extension BBQProfileViewController: ReferalDelegate{
    func shareReferalCode() {
        
        AnalyticsHelper.shared.triggerEvent(type: .RE08)

        let configuration = BottomSheetConfiguration(animationDuration: 0.4, minimumTopSpacing: 80)
        bottomSheetController = BottomSheetController(configuration: configuration)
        
       let vc = BBQShareReferalCodeViewController(nibName: "BBQShareReferalCodeViewController", bundle: nil)
        bottomSheetController?.present(vc, on: self, viewHeight: self.view.frame.size.height - (self.view.frame.size.height * (20/100)))

      
    }
}
