//
 //  Created by Arpana on 16/02/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified BBQPhoneNumberViewController.swift
 //

import UIKit
import IQKeyboardManagerSwift
class BBQPhoneNumberViewController: UIViewController {
    
    @IBOutlet weak var txtFieldPhoneNumber: UITextField! {
        
        didSet {
            txtFieldPhoneNumber.tintColor = UIColor.lightGray
            txtFieldPhoneNumber.setRightButton( UIImage(named: "TextFieldCross")!)
            
            
            if let button = txtFieldPhoneNumber.rightView?.subviews.last, button.isKind(of: UIButton.self) {
                (button as! UIButton).addTarget(self, action: #selector(self.rightButtonAction), for: .touchUpInside)
            }
            
            
        }
    }
    @IBOutlet weak var txtFieldName: UITextField! {
        
        didSet {
            txtFieldName.tintColor = UIColor.lightGray
            txtFieldName.setRightButton( UIImage(named: "TextFieldCross")!)
            
            
            if let button = txtFieldName.rightView?.subviews.last, button.isKind(of: UIButton.self) {
                (button as! UIButton).addTarget(self, action: #selector(self.rightButtonAction), for: .touchUpInside)
            }
            
            
        }
    }
    lazy private var verifierViewModel : BBQMobileVerifierViewModel = {
        let verifierVM = BBQMobileVerifierViewModel()
        return verifierVM
    } ()
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var txtFieldReferalCode: UITextField!
    @IBOutlet weak var txtFieldCountryCode:UITextField!
    @IBOutlet weak var txtFieldEmail: UITextField!
    @IBOutlet weak var btnNext:UIButton!
    @IBOutlet weak var stackViewNumber: UIStackView!
    @IBOutlet weak var stackViewName: UIStackView!
    @IBOutlet weak var stackViewEmail : UIStackView!
    weak var delegate: LoginControllerDelegate? = nil
    private var userSelectedLocation:String = ""
    let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
    var navigationFromScreen: LoginNavigationScreen = .Splash
    
    var phoneCode: String = "" {
        didSet {
            BBQUserDefaults.sharedInstance.phoneCode = phoneCode
        }
    }
    var currentTheme = theme.white
    private var isReferralClicked:Bool = false
    var nameLength = 0, phoneNumberLength = 0,referralCodeLength = 0
    var isPickerView = false
    var currentScreen = screen.onlyMobileNumber_screen
    lazy var loginVerifyViewModel : LoginVerifyViewModel = {
        let viewModel = LoginVerifyViewModel()
        return viewModel
    }()
    lazy private var loginControllerViewModel : LoginControllerViewModel = {
        let viewModel = LoginControllerViewModel()
        return viewModel
    }()
    lazy private var profileViewModel : BBQProfileViewModel = {
        let profileVM = BBQProfileViewModel()
        return profileVM
    } ()
    
    lazy var notificationViewModel : BBQNotificationTokenViewModel = {
        let viewModel = BBQNotificationTokenViewModel()
        return viewModel
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 100
        
        AnalyticsHelper.shared.screenName(name: "LoginScreen", className: "LoginController")
        
        AnalyticsHelper.shared.triggerEvent(type: .A01)
        //Authorize
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (_, error) in
            guard error == nil else{
                print(error!.localizedDescription)
                return
            }
        }
        
        
        
        phoneCode = Constants.App.defaultValues.phoneNumberCode
        BBQUserDefaults.sharedInstance.phoneCode = phoneCode
        
        stackViewName.isHidden = true
        currentScreen = screen.onlyMobileNumber_screen
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //  setViewHeight()
        self.screenName(name: .Login_Screen)
        setUI()
    }
    
    func setUI(){
        txtFieldPhoneNumber.layer.cornerRadius = 4.0
        txtFieldPhoneNumber.layer.masksToBounds = true
        txtFieldCountryCode.layer.cornerRadius = 4.0
        txtFieldCountryCode.layer.masksToBounds = true
        
        
        if UserDefaults.standard.object(forKey:referalCodeReceived) != nil {
            txtFieldReferalCode.text = UserDefaults.standard.object(forKey:referalCodeReceived) as? String ?? ""
        }
        
        //Add a space at begining of textfield
        txtFieldPhoneNumber.addLeftView(UIView.init(frame: CGRect(x: 0, y: 0, width: 10, height: txtFieldPhoneNumber.frame.size.height)))
        
        //Add a space at begining of textfield
        txtFieldEmail.addLeftView(UIView.init(frame: CGRect(x: 0, y: 0, width: 10, height: txtFieldEmail.frame.size.height)))
        //Add a space at begining of textfield
        
        txtFieldReferalCode.addLeftView(UIView.init(frame: CGRect(x: 0, y: 0, width: 10, height: txtFieldReferalCode.frame.size.height)))
        
        //don't show cross button until any text is typed
        txtFieldPhoneNumber.rightView?.isHidden = true
        
        if currentScreen == .onlyMobileNumber_screen {
            
            stackViewName.isHidden = true
            stackViewNumber.isHidden = false
            btnNext.setTitle("Next", for: .normal)
            btnBack.isHidden = true
        }
        else {
            btnBack.isHidden = false
        }
    }
    
    private func isValidNumber(number: String) -> Bool {
        return BBQPhoneNumberValidation.isValidNumber(number: number, for: phoneCode)
    }
    
    @IBAction func nextButtonPressed(_ sender : UIButton){
        
        
        if  currentScreen == .onlyMobileNumber_screen {
            
            let validNumber = isValidNumber(number: self.txtFieldPhoneNumber.text!)
            
            if validNumber {
                
                checkIfNewRegistrationOrExistingUser()
                
            }
            // If not a valid number, do not allow to go to next screen
            //show a toast message
            btnBack.isHidden = false

        }
        else if currentScreen == .mobile_Email_screen {
            //its an email screen, i.e new user
            
            let validEmail = txtFieldEmail.text?.isValidEmailID() ?? false
            
            if validEmail {
                proccessToOTPVerification()
            }else{
                UIUtils.showToast(message: "Please enter a valid email id")
                return
            }
        }else  {
            
            //It is name input field
            //validate name and proceed for otp
            
            let validName = (txtFieldName.text?.trimmingCharacters(in: .whitespaces).count ?? 0 >= Constants.App.Validations.nameMinLength)
            
            if validName {
                
                //Go to OTP Screen to get name if user is registering for 1st time
                // if already registered user , go to name screen to get name
                goToEmaileScreen()
            }
            
        }
        
    }
    @IBAction func backBtnPressed(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .layoutSubviews) {
            if  self.currentScreen == .mobile_Email_screen {
                self.stackViewName.isHidden = false
                self.stackViewEmail.isHidden = true
             //   self.currentScreen = .mobile_NameField_screen
                //its email screen, on back show name screen
            }else if self.currentScreen == .mobile_NameField_screen {
                //its nam screen, on back show mobile screen
                self.stackViewName.isHidden = true
                self.stackViewEmail.isHidden = true
                self.stackViewNumber.isHidden = false
              //  self.currentScreen = .onlyMobileNumber_screen
                
            }else{
                self.btnBack.isHidden = true
            }
        } completion: { (isCompleted) in
            if  self.currentScreen == .mobile_Email_screen {
                self.stackViewName.isHidden = false
                self.stackViewEmail.isHidden = true
                self.currentScreen = .mobile_NameField_screen
                //its email screen, on back show name screen
            }else if self.currentScreen == .mobile_NameField_screen {
                //its nam screen, on back show mobile screen
                self.stackViewName.isHidden = true
                self.stackViewEmail.isHidden = true
                self.stackViewNumber.isHidden = false
                self.currentScreen = .onlyMobileNumber_screen
                
            }else{
                self.btnBack.isHidden = true
            }
        }
       
    }
    
    func checkIfNewRegistrationOrExistingUser(){
        
        BBQActivityIndicator.showIndicator(view: self.view)
        AnalyticsHelper.shared.triggerEvent(type: .A02)
        loginVerifyViewModel.verifyUserPhoneNUmber(countryCode: self.phoneCode,
                                                   mobileNumber: Int64(txtFieldPhoneNumber.text!) ?? 0000,
                                                   otpGenerate: true) { (isSuccess) in
            if isSuccess {
                BBQActivityIndicator.hideIndicator(from: self.view)
                AnalyticsHelper.shared.triggerEvent(type: .A02A)
                if let userData = self.loginVerifyViewModel.userVerificationData, !userData.isExistingUser {
                    AnalyticsHelper.shared.triggerEvent(type: .A05)
                    //The given number is not registered , So show Name viewcontroller
                    self.goToNameScreen()
                    
                } else {
                    AnalyticsHelper.shared.triggerEvent(type: .A06)
                    self.goToOTPScreen()
                }
            } else {
                // Showing response error field from server, written in base manager class
                self.view.endEditing(true) // Forcing keyboiard to dismiss to show error toast.
                AnalyticsHelper.shared.triggerEvent(type: .A02B)
                BBQActivityIndicator.hideIndicator(from: self.view)
            }
        }
    }
    
    func goToNameScreen(){
        
        //Go to name Screen to get name if user is registering for 1st time
        // if already registered user , go to name screen to get name
        //Retain mobile number which has been enterd
        
        self.stackViewNumber.isHidden = true
        self.stackViewEmail.isHidden = true
        UIView.animate(withDuration: 0.3, delay: 0, options: .transitionFlipFromBottom) {
            self.stackViewName.isHidden = false
            self.txtFieldName.becomeFirstResponder()
        } completion: { (isCompleted) in
            self.stackViewName.isHidden = false
            self.stackViewNumber.isHidden = true
            self.stackViewEmail.isHidden = true
            self.currentScreen = .mobile_NameField_screen
            self.btnNext.setTitle("Save", for: .normal)
        }
        
        
        
    }
    
    
    func goToEmaileScreen(){
        
        //Go to name Screen to get name if user is registering for 1st time
        // if already registered user , go to name screen to get name
        //Retain mobile number which has been enterd
        
        if UserDefaults.standard.object(forKey:referalCodeReceived) != nil {
            txtFieldReferalCode.text = UserDefaults.standard.object(forKey:referalCodeReceived) as? String ?? ""
        }

        self.stackViewName.isHidden = true
        self.stackViewNumber.isHidden = true
        UIView.animate(withDuration: 0.3, delay: 0, options: .showHideTransitionViews) {
            
            self.stackViewEmail.isHidden = false
        } completion: { (isCompleted) in
            self.stackViewName.isHidden = true
            self.stackViewNumber.isHidden = true
            self.stackViewEmail.isHidden = false
            self.txtFieldEmail.becomeFirstResponder()
            self.btnNext.setTitle("Save", for: .normal)
            self.currentScreen = .mobile_Email_screen
            
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if touch?.view == self.view {
            view.endEditing(true)
            
            if navigationFromScreen != .Splash {
                self.dismiss(animated: false, completion: nil) }
            
        }
    }
    
    
    @IBAction func skipLogin(_ sender : UIButton){
        
        //when user pressed Guest button, Allow to go home page without login
        AnalyticsHelper.shared.triggerEvent(type: .A07)
        BBQUserDefaults.sharedInstance.isGuestUser = true
        gotoHomeScreen()
    }
    
    
    
    func proccessToOTPVerification(){
        
            AnalyticsHelper.shared.triggerEvent(type: .verify_user_status)
//            let isMobileValid = BBQPhoneNumberValidation.isValidNumber(number: String(self.updatedMobileNumber),
//                                                                       for: self.countryCode)
        
        let validNumber = isValidNumber(number: self.txtFieldPhoneNumber.text!)
        
        if validNumber {
            
            
            if validNumber {
                BBQActivityIndicator.showIndicator(view: self.view)
                
                // 1. Verifying user mobile first
                // 2. If existing user mobile, blocks to update mobile, showing error
                // 3. New user generates OTP and gpto OTP page
                self.verifierViewModel.verifyUser(countryCode: self.txtFieldCountryCode.text!,
                                                  mobileNumber: Int64(self.txtFieldPhoneNumber.text ?? "00" ) ?? 0,
                                                  otpGenerate: false) { (isExistingUser) in
                                                    BBQActivityIndicator.hideIndicator(from: self.view)
                                                    if isExistingUser {
                                                        UIUtils.showToast(message: kAlreadyRegistredMobile)
                                                    } else {
                                                        self.verifierViewModel.generateMobileOTP(countryCode: self.txtFieldCountryCode.text!,
                                                                                                 mobileNumber: Int64(self.txtFieldPhoneNumber.text ?? "00" ) ?? 0 ,
                                                                                                 otpId: self.verifierViewModel.otpIdentifier) { (isGenerated) in
                                                                                                    if isGenerated {
                                                                                                        self.loginVerifyViewModel.otpId = self.verifierViewModel.otpIdentifier
                                                                                                        self.goToOTPScreen()
                                                                                                    }
                                                                                                    BBQActivityIndicator.hideIndicator(from: self.view)
                                                        }
                                                    }
                }
                
                self.view.endEditing(true) // Dismissing keyboard for listening server messages.
                
            } else {
                UIUtils.showToast(message: kInValidMobile)
            }
        }
    }
    
    func registerUser(otp:String) {
        BBQActivityIndicator.showIndicator(view: self.view)
        AnalyticsHelper.shared.triggerEvent(type: .registration_status)
        let formattedTitle = loginVerifyViewModel.formattedUserTitle(with: "self.userTitle")
        AnalyticsHelper.shared.triggerEvent(type: .A05A)
        self.loginControllerViewModel.registerUser(countryCode: txtFieldCountryCode.text!,
                                                   mobileNumber: Int64(txtFieldPhoneNumber.text!)!,
                                                   email: txtFieldEmail.text!,
                                                   title: "Mrs",
                                                   name: txtFieldName.text!,
                                                   otpId: self.loginVerifyViewModel.otpId,
                                                   otp: Int(otp)!, referralCode: "" , completion: { (isRegisterSuccess) in
            
            BBQActivityIndicator.hideIndicator(from: self.view)
            if isRegisterSuccess {
                AnalyticsHelper.shared.triggerEvent(type: .A05AA)
                AnalyticsHelper.shared.triggerEvent(type: .registration_success_value)
                self.profileViewModel.getUserProfile { (isFetched) in
                    if isFetched {
                        BBQActivityIndicator.hideIndicator(from: self.view)
                        if let userModel =  self.profileViewModel.profileData{
                            
//                            if (self.txtFieldReferalCode.text != "" && self.txtFieldReferalCode.text != nil){
//
//                                if userModel.mobileNumber != nil {
//
//                                    self.updateInviteReferalCode(userId: userModel.mobileNumber! )
//                                }
//                            }
                            if userModel.lastVisitedBranchName!.count>0{
                                BBQUserDefaults.sharedInstance.CurrentSelectedLocation = (userModel.lastVisitedBranchName)!
                                self.userSelectedLocation = BBQUserDefaults.sharedInstance.CurrentSelectedLocation
                                
                            }else{
                                self.userSelectedLocation = ""
                                
                            }
                            if let appDelegate = self.appDelegate {
                                appDelegate.saveNotificationData(dataArray: [])
                            }
                            BBQUserDefaults.sharedInstance.branchIdValue = (userModel.lastVisitedBranch)!
                            BBQUserDefaults.sharedInstance.signUpReferralCode = (userModel.referralCode)
                            DispatchQueue.main.async {
                                let userDict = ["userSelectedLocation": userModel.lastVisitedBranchName,
                                                "selectedBrachID": userModel.lastVisitedBranch]
                                NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.NotificationName.receiedUserProfileData), object: nil, userInfo: userDict as [AnyHashable : Any])
                                self.gotoHomeScreen()
                            }
                            
                            
                        }
                    }else{
                        BBQActivityIndicator.hideIndicator(from: self.view)
                    }
                }
            } else {
                AnalyticsHelper.shared.triggerEvent(type: .A05AB)
                AnalyticsHelper.shared.triggerEvent(type: .registration_failure_value)
                PopupHandler.showSingleButtonsPopup(title: kFailed,
                                                    message: kRegistrationFailed,
                                                    image: UIImage(named: "icon_cross_black"),
                                                    on: self,
                                                    firstButtonTitle: kOkButton,
                                                    firstAction: { })
            }
        })
    }
    
    
    func loginUser(otp:String) {
        self.view.endEditing(true)
        
        BBQActivityIndicator.showIndicator(view: self.view)
        AnalyticsHelper.shared.triggerEvent(type: .login_status)
        self.loginControllerViewModel.login(countryCode: self.phoneCode, mobileNumber: Int64(txtFieldPhoneNumber.text!) ?? 000, otpId: self.loginVerifyViewModel.otpId, otp: Int(otp)!, completion: { (isLoginSuccess) in
            BBQActivityIndicator.hideIndicator(from: self.view)
            if isLoginSuccess {
                //if Login success update header with user previously selected location
                AnalyticsHelper.shared.triggerEvent(type: .login_success_value)
                if !BBQUserDefaults.sharedInstance.isGuestUser{
                    //                    if BBQUserDefaults.sharedInstance.branchIdValue == ""{
                    self.profileViewModel.getUserProfile { (isFetched) in
                        if isFetched {
                            
                            //Delete token for guest user And send the token for  logged in user
                            if BBQUserDefaults.sharedInstance.accessToken != "" && BBQUserDefaults.sharedInstance.isGuestUserAPI == true {
                                
                                self.notificationViewModel.deleteNotificationTokenForGuestUser(token: BBQUserDefaults.sharedInstance.firebaseToken, deviceType: "IOS"){ (isSuccess) in
                                    print("Login: delete token for guest user",isSuccess)
                                    BBQUserDefaults.sharedInstance.isGuestUserAPI = false
                                    
                                    //In case postNotificationToken has return unsucessful , then BBQUserDefaults.sharedInstance.firebaseToken will be ""
                                    //restrict to hit api when its blank
                                    
                                    if  BBQUserDefaults.sharedInstance.firebaseLocalToken != "" {
                                        self.notificationViewModel.postNotificationToken(token: BBQUserDefaults.sharedInstance.firebaseLocalToken, deviceType: "IOS"){ (isSuccess) in
                                            BBQUserDefaults.sharedInstance.firebaseToken = BBQUserDefaults.sharedInstance.firebaseLocalToken
                                            print("Login: create token for logged in user",isSuccess)
                                        }
                                    }
                                }
                            }else if BBQUserDefaults.sharedInstance.accessToken != "" {
                                self.notificationViewModel.postNotificationToken(token: BBQUserDefaults.sharedInstance.firebaseLocalToken, deviceType: "IOS"){ (isSuccess) in
                                    BBQUserDefaults.sharedInstance.firebaseToken = BBQUserDefaults.sharedInstance.firebaseLocalToken
                                    print("Login: create token for logged in user",isSuccess)
                                }
                            }
                            //
                            if let userModel =  self.profileViewModel.profileData{
                                
                                if !BBQBookingUtilies.shared.isLocationAlreadySet() {
                                    // If location set already once, no need to fetch and show last visited branch.
                                    BBQUserDefaults.sharedInstance.CurrentSelectedLocation = (userModel.lastVisitedBranchName)!
                                    BBQUserDefaults.sharedInstance.branchIdValue = (userModel.lastVisitedBranch)!
                                }
                                if userModel.lastVisitedBranchName!.count>0{
                                    BBQUserDefaults.sharedInstance.CurrentSelectedLocation = (userModel.lastVisitedBranchName)!
                                    self.userSelectedLocation = BBQUserDefaults.sharedInstance.CurrentSelectedLocation
                                    
                                }else{
                                    self.userSelectedLocation = ""
                                }
                                BBQUserDefaults.sharedInstance.signUpReferralCode = (userModel.referralCode)
                                if let appDelegate = self.appDelegate {
                                    appDelegate.saveNotificationData(dataArray: [])
                                }
                                DispatchQueue.main.async {
                                    let userDict = ["userSelectedLocation": userModel.lastVisitedBranchName,
                                                    "selectedBrachID": userModel.lastVisitedBranch]
                                    NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.NotificationName.receiedUserProfileData), object: nil, userInfo: userDict as [AnyHashable : Any])
                                    self.gotoHomeScreen()
                                }
                                
                                
                            }
                        }else{
                        }
                    }
                    //                    }
                }
            } else {
                AnalyticsHelper.shared.triggerEvent(type: .login_failure_value)
                PopupHandler.showSingleButtonsPopup(title: kFailed,
                                                    message: kLoginFailed,
                                                    image: UIImage(named: "icon_cross_black"),
                                                    on: self,
                                                    firstButtonTitle: kOkButton,
                                                    firstAction: { })
            }
        })
    }
}

extension BBQPhoneNumberViewController :UITextFieldDelegate {
   
    
    func textFieldDidBeginEditing(_ textField: UITextField) {

        if textField == txtFieldCountryCode {

            txtFieldPhoneNumber.resignFirstResponder()
            textField.resignFirstResponder()
            let pickerViewController = UIStoryboard.loadPickerViewController()
            pickerViewController.delegate = self
            pickerViewController.type = .countryCode
            pickerViewController.isComingFromProfile = false
            pickerViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            self.present(pickerViewController, animated: false, completion: nil)
            isPickerView = true
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtFieldCountryCode {
            txtFieldPhoneNumber.resignFirstResponder()
            view.endEditing(true)
            let pickerViewController = UIStoryboard.loadPickerViewController()
            pickerViewController.delegate = self
            pickerViewController.type = .countryCode
            pickerViewController.isComingFromProfile = false
            pickerViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            self.present(pickerViewController, animated: false, completion: nil)
            isPickerView = true
            return false
        }
        return true
    }
    

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
           textField.resignFirstResponder()
           return true
       }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textVal = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        
        if textField == txtFieldPhoneNumber {
            
            //Show cross icon when any text is typed
            textField.rightView?.isHidden = (textVal.count == 0)
            
            if textVal.count >  3 {
                
                //unhide nextButton
                btnNext.isEnabled = true
                
            }else{
                btnNext.isEnabled = false
                
            }

            if  textVal.count > BBQPhoneNumberValidation.getValidNumberOfDigitsForCountryCode(code: txtFieldCountryCode.text!) {
                return false
            }
        }
        
        if textField == txtFieldCountryCode {
         
            return false

        }
        return true
    }

    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    
   
    
    
private func gotoHomeScreen() {
    
  
        self.view.endEditing(true)
// If user login/signup from the reserve screen and no location is set by user before login,The outlet selected as guest user will be taken as the outlet for booking and same has to be updated as the last visited branch of the user
        if self.userSelectedLocation.count == 0{
            let bbqLastVisitedViewModel = BBQLastVisitedViewModel()
            bbqLastVisitedViewModel.getLastVisitedBranch(id: BBQLocationManager.shared.outlet_branch_id) {_ in
                //                print(self.bbqLastVisitedViewModel)
            }

        }
    if let delegate = self.delegate {
        //  self.dismiss(animated: false) {
        if navigationFromScreen != .Splash {
            self.navigationController?.popViewController(animated: false)
        }else{
            
            self.dismiss(animated: false)
            
        }
        if let userData = self.loginVerifyViewModel.userVerificationData, userData.isExistingUser {
            delegate.gotoHomeScreen(isExistingUser: true, navigationFrom: self.navigationFromScreen)
        } else {
            delegate.gotoHomeScreen(isExistingUser: false, navigationFrom: self.navigationFromScreen)
        }
        
    }else{
        goToHomePage()
    }
}
}


extension BBQPhoneNumberViewController {
    
    @objc func rightButtonAction(_ sender : UIButton){
        
    //get the textfield from button superview
    // button superview is textfield's right view
        // rightviews super virew is uitextfield
       //clear textfield data
        let superView = (sender as? UIButton)?.superview?.superview as? UITextField
        superView?.text = ""
    }
}

//MARK: Picker View
extension BBQPhoneNumberViewController: PickerViewDelegate {
    
    //According to new designs , this metod is not required
    func titleSelected(title: String) {
        print("title")
    }
    
    
    
    func countryCodeSelected(phoneCode: String) {
        if self.phoneCode != phoneCode {
            self.txtFieldPhoneNumber.text = ""
            phoneNumberLength = 0
            txtFieldCountryCode.text = ""
            btnNext.isEnabled = true
            self.phoneCode = phoneCode
            txtFieldCountryCode.text = self.phoneCode
            isPickerView = false
            //  showSendOTPButton()
        }
    }
    func pickerClosed() {
        isPickerView = false
    }
}
extension BBQPhoneNumberViewController :BBQOTPScreenDelegate {
    func otpValidation(status: Bool, otp:String) {
        if status == true {
            if let userData = self.loginVerifyViewModel.userVerificationData, userData.isExistingUser {
                
                self.loginUser(otp: otp)
                
                
            } else {
                self.view.endEditing(true)
                
                self.registerUser(otp: otp)
                
                
            }
        } else {
            ToastHandler.showToastWithMessage(message: kMaxAtmpOtpReachDesc)
            //  self.signUpFailed = true
            let lastScreen = screen.onlyMobileNumber_screen
            // self.currentScreen = lastScreen
            
        }
    }
    
    
    func updateInviteReferalCode(userId: String) {
    
        AnalyticsHelper.shared.triggerEvent(type: .RE05)
        //Once registered removethe code from defaults
        UserDefaults.standard.removeObject(forKey: referalCodeReceived)
        BBQServiceManager.getInstance().ShareYourReferralCodeToShare(params: [ "customer_id" :userId, "referral_code" : txtFieldReferalCode.text ?? ""  ]) { error, model in
        
        DispatchQueue.main.async {
            print(model)
        }
    }
}
    func backButtonPressed() {
      //  let screen = self.currentScreen
        self.view.endEditing(true)

     //   self.currentScreen = screen

        //When back button is pressed from OTP screen, Fill the previously entered phone number
        //self.phoneNumberEntered(length: 10, text: phoneNumberText)

          //  phoneNumberCell?.textField.text = self.phoneNumberText
        }



    @objc func dismiss() {
        self.dismiss(animated: false, completion: nil)
    }

    func goToOTPScreen(){

        //Go to name Screen to get name if user is registering for 1st time
        // if already registered user , go to name screen to get name
        //Retain mobile number which has been enterd
        let otpViewController = UIStoryboard.loadOTPReceiverViewController()
        otpViewController.delegate = self
        otpViewController.delegateLogin = self.delegate
        otpViewController.validationPath = self.txtFieldPhoneNumber.text!
        otpViewController.validationType = .mobileNumber
        otpViewController.userName = txtFieldName.text!
        otpViewController.loginVerifyViewModelReceived = loginVerifyViewModel
        otpViewController.navigationFromScreen = navigationFromScreen
        let countryCode = BBQUserDefaults.sharedInstance.phoneCode
        if countryCode == "" {
            otpViewController.phoneCode  = "+91"
        }else{
            otpViewController.phoneCode = countryCode
        }
        otpViewController.expiryTime = verifierViewModel.otpDataModel?.expiryTime ?? 0
        //otpViewController.otpID = verifierViewModel.otpIdentifier
        otpViewController.otpID = loginVerifyViewModel.otpId
        otpViewController.header = ""
        otpViewController.otpMessage = ""
        otpViewController.signUp = kSignUp
        otpViewController.otpType = .normalLogin

        if let userData = self.loginVerifyViewModel.userVerificationData, userData.isExistingUser {
            otpViewController.isExistingUser = true
        } else {
            otpViewController.isExistingUser = false
        }
        otpViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(otpViewController, animated: true, completion: nil)
       // self.navigationController?.pushViewController(otpViewController, animated: true)
    }
}
