//
 //  Created by Arpana on 16/02/22
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2022.
 //  All rights reserved.
 //  Last modified BBQOTPViewController.swift
 //

import UIKit

class SingleDigitField: UITextField {
    var pressedDelete = false
    override func willMove(toSuperview newSuperview: UIView?) {
        keyboardType = .numberPad
        textAlignment = .center
        backgroundColor = UIColor.hexStringToUIColor(hex: "F5F5F5")
        isSecureTextEntry = false
       // isUserInteractionEnabled = false
    }
    override func caretRect(for position: UITextPosition) -> CGRect { .zero }
    override func selectionRects(for range: UITextRange) -> [UITextSelectionRect] { [] }
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool { false }
    override func deleteBackward() {
        pressedDelete = true
        sendActions(for: .editingChanged)
    }
}

class BBQOTPViewController: UIViewController {

    @IBOutlet weak var lblResendTimer: UILabel!
    @IBOutlet weak var resendOtp:UIButton!
    @IBOutlet weak var txtFieldFirst: SingleDigitField!
    
    @IBOutlet weak var txtFieldSecond :SingleDigitField!
    
    @IBOutlet weak var txtFieldThird :SingleDigitField!
    
    @IBOutlet weak var txtFieldFourth: SingleDigitField!
    @IBOutlet weak var txtFieldFifth: SingleDigitField!
    @IBOutlet weak var txtFieldSixth: SingleDigitField!
    @IBOutlet weak var expiryTimeLbl: UILabel!
    
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet var textFieldsCollection: [SingleDigitField]!

    private var userSelectedLocation:String = ""

    let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
    weak var delegate: BBQOTPScreenDelegate? = nil
    var resendOTPCounter =  0
    private var verifyOTPCounter = 0
    var signUp = ""
    var otpMessage = kOtpSentNotification
    var otpType = OtpType.normalLogin
    var validationType : BBQOTPValidationType = BBQOTPValidationType.mobileNumber
    var header = kCorporateOffers
    var logoImage = Constants.AssetName.corporateLogoBig
    var otpID = ""
    var validationPath = "" // Set email id or mobile number here.
    var phoneCode = ""
    var isExistingUser = false
    var  loginVerifyViewModelReceived = LoginVerifyViewModel()
    var expiryTime : Int64 = 0
    weak var delegateLogin: LoginControllerDelegate? = nil

    
    //Variables to retain previous screen data/////////
    var MobileNumber = ""
    var countryCode = ""
    var userName = ""
    lazy var bookingViewModel : CorporateBookingViewModel = {
        let viewModel = CorporateBookingViewModel()
        return viewModel
    }()
    
    lazy private var loginControllerViewModel : LoginControllerViewModel = {
        let viewModel = LoginControllerViewModel()
        return viewModel
    }()
    
    lazy private var profileViewModel : BBQProfileViewModel = {
        let profileVM = BBQProfileViewModel()
        return profileVM
    } ()
    
    lazy var notificationViewModel : BBQNotificationTokenViewModel = {
        let viewModel = BBQNotificationTokenViewModel()
        return viewModel
    }()
    
    var navigationFromScreen: LoginNavigationScreen = .Splash

    var secondsRemaining = 0
    var blinkStatus = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AnalyticsHelper.shared.screenName(name: "OTPScreen", className: "BBQOTPScreenViewController")
        AnalyticsHelper.shared.triggerEvent(type: .B01)

    
        
        [txtFieldFirst, txtFieldSecond, txtFieldThird, txtFieldFourth, txtFieldFifth, txtFieldSixth].forEach {
            $0?.addTarget(self, action: #selector(editingChanged), for: .editingChanged)
        }
        txtFieldFirst.isUserInteractionEnabled = true
        txtFieldFirst.becomeFirstResponder()
        
        let strAtr = NSMutableAttributedString()
        strAtr.append(NSMutableAttributedString(string: "Your verification code was sent to", attributes: [NSAttributedString.Key.font: UIFont.init(name: "Poppins-Regular", size: 17.0)!, NSAttributedString.Key.foregroundColor: UIColor.hexStringToUIColor(hex: "666666")]))
        strAtr.append(NSMutableAttributedString(string: "\n\(self.phoneCode + validationPath)", attributes: [NSAttributedString.Key.font: UIFont.init(name: "Poppins-Regular", size:  17.0)!, NSAttributedString.Key.foregroundColor: UIColor.hexStringToUIColor(hex: "F04B24") as Any]))
            
        lblMessage.lineBreakMode = .byWordWrapping;
        lblMessage.attributedText = strAtr
        startTimerToValidateOTPTime()
        //
//        let formatter = DateComponentsFormatter()
//        formatter.allowedUnits = [.hour,.minute]
//        formatter.unitsStyle = .full
//
//        let formattedString = formatter.string(from: TimeInterval(self.expiryTime))!
//        print(formattedString)
        


            
      //  expiryTimeLbl.text = String(format: "Resend in %@" , formattedString)
    }
    

    func setSecondsToValidateOTP(){
        
        switch(resendOTPCounter) {
            
        case  0 ,1, 2: secondsRemaining = 30
            break
        case  3 : secondsRemaining = 60
            break
       
        default : secondsRemaining = 120
            
        }
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        
        self.dismiss(animated: true)
    }
    @objc func editingChanged(_ textField: SingleDigitField) {
    if textField.pressedDelete {
        textField.pressedDelete = false
        if textField.hasText {
            textField.text = ""
        } else {
            switch textField {
            case txtFieldSecond, txtFieldThird, txtFieldFourth, txtFieldFifth, txtFieldSixth:
               // textField.resignFirstResponder()
 //               textField.isUserInteractionEnabled = false
                switch textField {
                case txtFieldSecond:
                   // txtFieldFirst.isUserInteractionEnabled = true
                    txtFieldFirst.becomeFirstResponder()
                    txtFieldFirst.text = ""
                case txtFieldThird:
                   // txtFieldSecond.isUserInteractionEnabled = true
                    txtFieldSecond.becomeFirstResponder()
                    txtFieldSecond.text = ""
                case txtFieldFourth:
                   // txtFieldThird.isUserInteractionEnabled = true
                    txtFieldThird.becomeFirstResponder()
                    txtFieldThird.text = ""
                case txtFieldFifth:
                   // txtFieldFourth.isUserInteractionEnabled = true
                    txtFieldFourth.becomeFirstResponder()
                    txtFieldFourth.text = ""
                case txtFieldSixth:
                   // txtFieldFifth.isUserInteractionEnabled = true
                    txtFieldFifth.becomeFirstResponder()
                    txtFieldFifth.text = ""
                default:
                    break
                }
            default: break
            }
        }
    }

    guard textField.text?.count == 1, textField.text?.last?.isWholeNumber == true else {
        textField.text = ""
        return
    }
    switch textField {
    case txtFieldFirst, txtFieldSecond, txtFieldThird , txtFieldFourth, txtFieldFifth:
       // textField.resignFirstResponder()
      //  textField.isUserInteractionEnabled = false
        switch textField {
        case txtFieldFirst:
         //   txtFieldSecond.isUserInteractionEnabled = true
            txtFieldSecond.becomeFirstResponder()
            
        case txtFieldSecond:
           txtFieldThird.becomeFirstResponder()
          //  txtFieldThird.isUserInteractionEnabled = true

        case txtFieldThird:
            txtFieldFourth.becomeFirstResponder()
          //  txtFieldFourth.isUserInteractionEnabled = true

        case txtFieldFourth:
           // txtFieldFifth.isUserInteractionEnabled = true
            txtFieldFifth.becomeFirstResponder()
        case txtFieldFifth:
           // txtFieldSixth.isUserInteractionEnabled = true
            txtFieldSixth.becomeFirstResponder()
        default: break
        }
    case txtFieldSixth:
        txtFieldSixth.resignFirstResponder()
        checkForValidity()
    default: break
    }
}
    
    func setUI(){
        
        txtFieldFirst.layer.cornerRadius = (txtFieldFirst.frame.size.width / 2)
        txtFieldFirst.layer.masksToBounds = true
        
        txtFieldSecond.layer.cornerRadius = (txtFieldSecond.frame.size.height / 2)
        txtFieldSecond.layer.masksToBounds = true
        
        txtFieldThird.layer.cornerRadius = (txtFieldThird.frame.size.width / 2)
        txtFieldThird.layer.masksToBounds = true
        
        txtFieldFourth.layer.masksToBounds = true
        txtFieldFourth.layer.cornerRadius = (txtFieldFourth.frame.size.width / 2)
        
        txtFieldFifth.layer.masksToBounds = true
        txtFieldFifth.layer.cornerRadius = (txtFieldFourth.frame.size.width / 2)
        
        txtFieldSixth.layer.masksToBounds = true
        txtFieldSixth.layer.cornerRadius = (txtFieldFourth.frame.size.width / 2)
    }
    
    func blink() {
        
        // To make resend timer blink
        if blinkStatus == false{
            lblResendTimer.alpha = 1.0
            blinkStatus = true
        }else {
            lblResendTimer.alpha = 0.0
            blinkStatus = false
        }
    }

     func startTimerToValidateOTPTime() {
            
         setSecondsToValidateOTP()
         blinkStatus  = false
        Timer.scheduledTimer(withTimeInterval: 0.7, repeats: true) { (Timer) in
            if self.secondsRemaining > 0 {
                //print ("\(self.secondsRemaining) seconds")
                self.secondsRemaining -= 1

                UIView.animate(withDuration: 0.2) {
                    self.resendOtp.isHidden = true
                    self.expiryTimeLbl.isHidden = true
                    self.lblResendTimer.isHidden = false
                    //self.blink()
                    self.lblResendTimer.text = ("Resend OTP in \(self.hmsFromSecondsFormatted(seconds:self.secondsRemaining))")

                } completion: { (isCompleted) in
                    self.resendOtp.isHidden = true
                    self.expiryTimeLbl.isHidden = true
                    self.lblResendTimer.isHidden = false
                    self.lblResendTimer.text = ("Resend OTP in \(self.hmsFromSecondsFormatted(seconds:self.secondsRemaining))")

                }
            } else {
                UIView.animate(withDuration: 0.2) {
                    self.resendOtp.isHidden = false
                    self.expiryTimeLbl.isHidden = false
                    self.lblResendTimer.isHidden = true
                } completion: { (isCompleted) in
                    self.resendOtp.isHidden = false
                    self.expiryTimeLbl.isHidden = false
                    self.lblResendTimer.isHidden = true
                }

                Timer.invalidate()
                
            }
        }
                
    }
    
    func hmsFromSecondsFormatted(seconds: Int) -> String {

        let m = (seconds % 3600) / 60
        let s = seconds % 60

        var minutesAndSeconds = ""

        minutesAndSeconds += "0\(m):"
        minutesAndSeconds += String(format:"%02d", s)
       

        return minutesAndSeconds
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.screenName(name: .Otp_Screen)
        setUI()
    }

    @objc func backButtonTapped(sender: UIButton){
            self.verifyOTPCounter = 0
            self.resendOTPCounter = 0
           
            self.dismiss(animated: true, completion: nil)
            AnalyticsHelper.shared.triggerEvent(type: .B04)
        }
    
    
    @IBAction func sendOTPTapped(sender: UIButton) {
        
        //clear textfield
        txtFieldFirst.text = ""
        txtFieldSecond.text = ""
        txtFieldThird.text = ""
        txtFieldFourth.text = ""
        txtFieldFifth.text = ""
        txtFieldSixth.text = ""
        
        resendOTPCounter += 1
        if(resendOTPCounter <= Constants.App.Validations.maxResendOTP) {
            if resendOTPCounter == Constants.App.Validations.maxResendOTP {
                PopupHandler.showSingleButtonsPopup(title: kMaxAtmpReachTitle,
                                                    message: kMaxAtmpReachDesc,
                                                    image: UIImage(named: "icon_cross_black"),
                                                    on: self,
                                                    firstButtonTitle: kMaxOtpTryOkayButton,
                                                    firstAction: {
                                                        //make first responder
                                                    })
                view.endEditing(true)
            }
            generateOTPWithOtpId(otpId: self.otpID)
        } else {
            self.showMaximumOTPTryAlert()
        }
    }
    
    private func generateOTPWithOtpId(otpId: String) {
        BBQActivityIndicator.showIndicator(view: self.view)
        // Switching resend OTP API calls in between mobile and email.
        switch self.validationType {
        case .corporateEmailID, .profileEmailID:
            self.resendEmailOTP(with: otpId)
        default:
            self.resendMobileOTP(with: otpId)
            
        }
    }
    
    // MARK:- Private Methods
    private func resendMobileOTP(with otpID: String) {
        AnalyticsHelper.shared.triggerEvent(type: .B03)
        loginVerifyViewModelReceived.generateOTP(countryCode: self.phoneCode,
                                         mobileNumber: Int64(self.validationPath)!,
                                         otpId:otpID) { (isSuccess) in
            DispatchQueue.main.async {
                if isSuccess{
                    AnalyticsHelper.shared.triggerEvent(type: .B03A)
                }else{
                    AnalyticsHelper.shared.triggerEvent(type: .B03B)
                }
                BBQActivityIndicator.hideIndicator(from: self.view)
                
                self.txtFieldFirst.becomeFirstResponder()
                self.txtFieldFirst.isUserInteractionEnabled = true
                self.txtFieldSecond.isUserInteractionEnabled = true
                self.txtFieldThird.isUserInteractionEnabled = true
                self.txtFieldFourth.isUserInteractionEnabled = true
                self.txtFieldFifth.isUserInteractionEnabled = true
                self.txtFieldSixth.isUserInteractionEnabled = true
                
            }
        }
        startTimerToValidateOTPTime()
    }
    
    private func resendEmailOTP(with otpID: String) {
        AnalyticsHelper.shared.triggerEvent(type: .B03)
        self.bookingViewModel.verifyCoporateEmail(corporateEmailId: self.validationPath,
                                                  otpID: otpID) { (isOTPResent) in
            BBQActivityIndicator.hideIndicator(from: self.view)
            if isOTPResent{
                AnalyticsHelper.shared.triggerEvent(type: .B03A)
            }else{
                AnalyticsHelper.shared.triggerEvent(type: .B03B)
            }
        }
    }
    // MARK: Private Method
    
    private func showMaximumOTPTryAlert() {
        PopupHandler.showSingleButtonsPopup(title: kMaxReqCrossedTitle,
                                            message: kMaxReqCrossedDesc,
                                            image: UIImage(named: "icon_cross_black"),
                                            on: self,
                                            firstButtonTitle: kMaxOtpTryOkayButton,
                                            firstAction: {
                                               //first responder
        })
        view.endEditing(true)
    }
    
   /*
    func registerUser(otp:String) {
        BBQActivityIndicator.showIndicator(view: self.view)
        AnalyticsHelper.shared.triggerEvent(type: .registration_status)
        let formattedTitle = loginVerifyViewModelReceived.formattedUserTitle(with: "self.userTitle")
        AnalyticsHelper.shared.triggerEvent(type: .A05A)
        self.loginControllerViewModel.registerUser(countryCode: countryCode,
                                                   mobileNumber: Int64(MobileNumber)!,
                                                   title: "Mrs",
                                                   name: userName,
                                                   otpId: self.loginVerifyViewModelReceived.otpId,
                                                   otp: Int(otp)!, referralCode: "self.referralCode" , completion: { (isRegisterSuccess) in
                                                    
                                                    BBQActivityIndicator.hideIndicator(from: self.view)
                                                    if isRegisterSuccess {
                                                        AnalyticsHelper.shared.triggerEvent(type: .A05AA)
                                                        AnalyticsHelper.shared.triggerEvent(type: .registration_success_value)
                                                        self.profileViewModel.getUserProfile { (isFetched) in
                                                            if isFetched {
                                                                BBQActivityIndicator.hideIndicator(from: self.view)
                                                                if let userModel =  self.profileViewModel.profileData{
                                                                    
                                                                    if userModel.lastVisitedBranchName!.count>0{
                                                                        BBQUserDefaults.sharedInstance.CurrentSelectedLocation = (userModel.lastVisitedBranchName)!
                                                                        self.userSelectedLocation = BBQUserDefaults.sharedInstance.CurrentSelectedLocation

                                                                    }else{
                                                                        self.userSelectedLocation = ""

                                                                    }
                                                                    if let appDelegate = self.appDelegate {
                                                                        appDelegate.saveNotificationData(dataArray: [])
                                                                    }
                                                                    BBQUserDefaults.sharedInstance.branchIdValue = (userModel.lastVisitedBranch)!
                                                                    BBQUserDefaults.sharedInstance.signUpReferralCode = (userModel.referralCode)
                                                                    DispatchQueue.main.async {
                                                                        let userDict = ["userSelectedLocation": userModel.lastVisitedBranchName,
                                                                                        "selectedBrachID": userModel.lastVisitedBranch]
                                                                        NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.NotificationName.receiedUserProfileData), object: nil, userInfo: userDict as [AnyHashable : Any])
                                                                     //   self.gotoHomeScreen()
                                                                    }
                                                                    
                                                                    
                                                                }
                                                            }else{
                                                                BBQActivityIndicator.hideIndicator(from: self.view)
                                                            }
                                                        }
                                                    } else {
                                                        AnalyticsHelper.shared.triggerEvent(type: .A05AB)
                                                        AnalyticsHelper.shared.triggerEvent(type: .registration_failure_value)
                                                        PopupHandler.showSingleButtonsPopup(title: kFailed,
                                                                                            message: kRegistrationFailed,
                                                                                            image: UIImage(named: "icon_cross_black"),
                                                                                            on: self,
                                                                                            firstButtonTitle: kOkButton,
                                                                                            firstAction: { })
                                                    }
        })
    }
    
    
    func loginUser(otp:String) {
        self.view.endEditing(true)
        
        BBQActivityIndicator.showIndicator(view: self.view)
        AnalyticsHelper.shared.triggerEvent(type: .login_status)
        self.loginControllerViewModel.login(countryCode: self.phoneCode, mobileNumber: Int64(MobileNumber) ?? 000, otpId: self.loginVerifyViewModelReceived.otpId, otp: Int(otp)!, completion: { (isLoginSuccess) in
            BBQActivityIndicator.hideIndicator(from: self.view)
            if isLoginSuccess {
                //if Login success update header with user previously selected location
                AnalyticsHelper.shared.triggerEvent(type: .login_success_value)
                if !BBQUserDefaults.sharedInstance.isGuestUser{
                    //                    if BBQUserDefaults.sharedInstance.branchIdValue == ""{
                    self.profileViewModel.getUserProfile { (isFetched) in
                        if isFetched {

                            //Delete token for guest user And send the token for  logged in user
                            if BBQUserDefaults.sharedInstance.accessToken != "" && BBQUserDefaults.sharedInstance.isGuestUserAPI == true {
                                
                                self.notificationViewModel.deleteNotificationTokenForGuestUser(token: BBQUserDefaults.sharedInstance.firebaseToken, deviceType: "IOS"){ (isSuccess) in
                                    print("Login: delete token for guest user",isSuccess)
                                    BBQUserDefaults.sharedInstance.isGuestUserAPI = false
                                    
                                    //In case postNotificationToken has return unsucessful , then BBQUserDefaults.sharedInstance.firebaseToken will be ""
                                    //restrict to hit api when its blank
                                    
                                    if  BBQUserDefaults.sharedInstance.firebaseLocalToken != "" {
                                        self.notificationViewModel.postNotificationToken(token: BBQUserDefaults.sharedInstance.firebaseLocalToken, deviceType: "IOS"){ (isSuccess) in
                                            BBQUserDefaults.sharedInstance.firebaseToken = BBQUserDefaults.sharedInstance.firebaseLocalToken
                                            print("Login: create token for logged in user",isSuccess)
                                        }
                                    }
                                }
                            }else if BBQUserDefaults.sharedInstance.accessToken != "" {
                                self.notificationViewModel.postNotificationToken(token: BBQUserDefaults.sharedInstance.firebaseLocalToken, deviceType: "IOS"){ (isSuccess) in
                                    BBQUserDefaults.sharedInstance.firebaseToken = BBQUserDefaults.sharedInstance.firebaseLocalToken
                                    print("Login: create token for logged in user",isSuccess)
                                }
                            }
                            //
                            if let userModel =  self.profileViewModel.profileData{
                                
                                if !BBQBookingUtilies.shared.isLocationAlreadySet() {
                                    // If location set already once, no need to fetch and show last visited branch.
                                    BBQUserDefaults.sharedInstance.CurrentSelectedLocation = (userModel.lastVisitedBranchName)!
                                    BBQUserDefaults.sharedInstance.branchIdValue = (userModel.lastVisitedBranch)!
                                }
                                if userModel.lastVisitedBranchName!.count>0{
                                    BBQUserDefaults.sharedInstance.CurrentSelectedLocation = (userModel.lastVisitedBranchName)!
                                    self.userSelectedLocation = BBQUserDefaults.sharedInstance.CurrentSelectedLocation

                                }else{
                                    self.userSelectedLocation = ""
                                }
                                BBQUserDefaults.sharedInstance.signUpReferralCode = (userModel.referralCode)
                                if let appDelegate = self.appDelegate {
                                    appDelegate.saveNotificationData(dataArray: [])
                                }
                                DispatchQueue.main.async {
                                    let userDict = ["userSelectedLocation": userModel.lastVisitedBranchName,
                                                    "selectedBrachID": userModel.lastVisitedBranch]
                                    NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.NotificationName.receiedUserProfileData), object: nil, userInfo: userDict as [AnyHashable : Any])
                                    delegate.gotoHomeScreen()
                                }
                                
                                
                            }
                        }else{
                        }
                    }
                    //                    }
                }
            } else {
                AnalyticsHelper.shared.triggerEvent(type: .login_failure_value)
                PopupHandler.showSingleButtonsPopup(title: kFailed,
                                                    message: kLoginFailed,
                                                    image: UIImage(named: "icon_cross_black"),
                                                    on: self,
                                                    firstButtonTitle: kOkButton,
                                                    firstAction: { })
            }
        })
    }
    private func gotoHomeScreen() {
        self.view.endEditing(true)
// If user login/signup from the reserve screen and no location is set by user before login,The outlet selected as guest user will be taken as the outlet for booking and same has to be updated as the last visited branch of the user
        if self.userSelectedLocation.count == 0{
            let bbqLastVisitedViewModel = BBQLastVisitedViewModel()
            bbqLastVisitedViewModel.getLastVisitedBranch(id: BBQLocationManager.shared.outlet_branch_id) {_ in
                //                print(self.bbqLastVisitedViewModel)
            }

        }
        if let delegate = self.delegate {
            self.dismiss(animated: false) {
                if let userData = self.loginVerifyViewModelReceived.userVerificationData, userData.isExistingUser {
                    delegate.gotoHomeScreen(isExistingUser: true, navigationFrom: .Splash)
                } else {
                    delegate.gotoHomeScreen(isExistingUser: false, navigationFrom: .Splash)
                }
            }
        }
    }*/
    
    @IBAction func skipLogin(_ sender : UIButton){
        
        //when user pressed Guest button, Allow to go home page without login
        AnalyticsHelper.shared.triggerEvent(type: .A07)
        BBQUserDefaults.sharedInstance.isGuestUser = true
        goToHomePageDelegate()
        
    }
    
    func goToHomePageDelegate() {
        
        //        let tabBarController = DeliveryTabBarController()
        //        tabBarController.navigationFromScreen = .Splash
        //        self.navigationController?.pushViewController(tabBarController, animated: true)
        
        self.view.endEditing(true)
        // If user login/signup from the reserve screen and no location is set by user before login,The outlet selected as guest user will be taken as the outlet for booking and same has to be updated as the last visited branch of the user
        if self.userSelectedLocation.count == 0{
            let bbqLastVisitedViewModel = BBQLastVisitedViewModel()
            bbqLastVisitedViewModel.getLastVisitedBranch(id: BBQLocationManager.shared.outlet_branch_id) {_ in
                //                print(self.bbqLastVisitedViewModel)
            }
            
        }
        delegateLogin?.gotoHomeScreen(isExistingUser: isExistingUser, navigationFrom: self.navigationFromScreen)

        
    }
    
}
extension BBQOTPViewController {
    
    
    func didChangeValidity(isValid: Bool) {
        
        
        if isValid == true {
            
            //all fields are filled
            //now verify otp api will  be called
       
            //        }
            
            print("OTPTExt  \(getOTP())")
            let finalOTP = (getOTP())
            didFinishEnteringOTP(otp: finalOTP)
        }

    }
    

    func didFinishEnteringOTP(otp: String) {
        verifyOTPCounter += 1
        AnalyticsHelper.shared.triggerEvent(type: .B02)
        if verifyOTPCounter <= Constants.App.Validations.maxOTPTrial {
            BBQActivityIndicator.showIndicator(view: self.view)
            loginVerifyViewModelReceived.verifyOTP(otp: Int(otp) ?? 0, otpId: self.otpID) { (isSuccess) in
                BBQActivityIndicator.hideIndicator(from: self.view)
                
                self.view.endEditing(true)
                if isSuccess {
                    AnalyticsHelper.shared.triggerEvent(type: .B02A)
                    if let delegate = self.delegate {
                        self.verifyOTPCounter = 0
                        self.resendOTPCounter = 0
                        self.view.endEditing(true)
                        self.dismiss(animated: true, completion: nil)
                        delegate.otpValidation(status: true, otp: otp)
                    }
                } else {
                    UIUtils.showToast(message: "Wrong otp, please retry")
                    self.resetOTP()
                    AnalyticsHelper.shared.triggerEvent(type: .B02B)
                    if self.verifyOTPCounter >= Constants.App.Validations.maxOTPTrial {
                        if let delegate = self.delegate {
                            self.verifyOTPCounter = 0
                            self.resendOTPCounter = 0
//shown error msg
                            delegate.otpValidation(status: false, otp: otp)
                        }
                    } else {
//                        cell!.errorLabel.isHidden = false
//                        cell?.reset()
//                        cell?.enterOTPLabel.text = kReEnterOTPMessage
//
//                        cell!.otpSentNotificationLabel.isHidden = true
//                        cell?.errorLabel.text = kOTPErrorMessage
//                        cell?.errorLabel.textColor = .red
//                        cell?.errorLabel.text = (cell?.errorLabel.text ?? "")
                    }
                }
            } //verifyOTP
        }
    }
    //checks if all the OTPfields are filled
    private final func checkForValidity(){
        for fields in textFieldsCollection{
            if (fields.text == ""){
                didChangeValidity(isValid: false)
                return
            }
        }
       didChangeValidity(isValid: true)
    }
    
    //gives the OTP text
    final func getOTP() -> String {
        var OTP = ""
        for textField in textFieldsCollection{
            OTP += textField.text ?? ""
        }
        return OTP
    }
    
    final func resetOTP() {
        for textField in textFieldsCollection{
            textField.text = ""
           // textFieldsCollection.first?.isUserInteractionEnabled = false
        }
        textFieldsCollection.first?.isUserInteractionEnabled = true
        textFieldsCollection.first?.becomeFirstResponder()
        
    }

    
//    func otpValidation(status: Bool, otp:String) {
//        if status == true {
//            if let userData = self.loginVerifyViewModelReceived.userVerificationData, userData.isExistingUser {
//
//                self.loginUser(otp: otp)
//
//            } else {
//                self.view.endEditing(true)
//
//                self.registerUser(otp: otp)
//            }
//
//        } else {
//            ToastHandler.showToastWithMessage(message: kMaxAtmpOtpReachDesc)
//            // go to previous screen with phone number input
//
//        }
//    }
}
