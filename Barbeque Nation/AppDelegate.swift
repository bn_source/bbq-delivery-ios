//
//  AppDelegate.swift
//  Barbeque Nation
//
//  Created by Rabindra L on 17/07/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit
import Firebase
import FirebaseMessaging
import UserNotifications
import FirebaseCore
import GooglePlaces
import GoogleMaps
import IQKeyboardManagerSwift
import FirebaseDynamicLinks

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, FirebaseMessaging.MessagingDelegate {
    
    var window: UIWindow?
    private let guestUserStatus = "guestUserStatus"
    let gmsSessionToken = GMSAutocompleteSessionToken.init()
    let reachabilityManager = ReachabilityManager.shared
    var notificationDataArray = [NotificationModel]()
    lazy var notificationViewModel : BBQNotificationTokenViewModel = {
        let viewModel = BBQNotificationTokenViewModel()
        return viewModel
    }()
    var navigationController:UINavigationController?
    var isClickedOnPush = false
    
    static func shared() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        //Firebase init
        GMSPlacesClient.provideAPIKey(GOOGLE_PLACES_API_KEY)
        GMSServices.provideAPIKey(GOOGLE_MAPS_API_KEY)
        IQKeyboardManager.shared.enable = true
        FirebaseApp.configure()
        
        UNUserNotificationCenter.current().delegate = self
        Messaging.messaging().delegate = self
        Messaging.messaging().isAutoInitEnabled = true
        
        let activityKey = NSString(string: "UIApplicationLaunchOptionsUserActivityKey")
        if let userActivityDict = launchOptions?[.userActivityDictionary] as? [NSObject : AnyObject], let userActivity = userActivityDict[activityKey] as? NSUserActivity, let webPageUrl = userActivity.webpageURL {
            
            DynamicLinks.dynamicLinks().handleUniversalLink(webPageUrl) { (dynamiclink, error) in
                
                _ = DynamicLinks.dynamicLinks()
                    .handleUniversalLink((userActivity.webpageURL)!) { dynamiclink, error in
                        
                        let parameterRetrieved = self.queryParameters(from: (dynamiclink?.url)!)
                        
                        UserDefaults.standard.set(parameterRetrieved["invitedBy"], forKey: referalCodeReceived)
                        
                    }
            }
        }
        //Clear all data
        if BBQUserDefaults.sharedInstance.customerId.isEmpty || BBQUserDefaults.sharedInstance.customerId == "" {
            //BBQUserDefaults.sharedInstance.clearAppDefaults()
            BBQUserDefaults.sharedInstance.isGuestUser = true
            BBQLocationManager.shared.clearAllData()
        }
        // If accessToken returns zero length it is considred to be guest user.
        // Setting values for initial load.
        if (BBQUserDefaults.sharedInstance.accessToken.count == 0) {
            BBQUserDefaults.sharedInstance.defaults.set(true, forKey: guestUserStatus)
        }
        if isClickedOnPush && bookingIdForPrint == ""{
            navigationController = nil
            application.keyWindow?.rootViewController = getBBQNotificationVC()
            application.keyWindow?.makeKeyAndVisible()
        }else{
            loadHomeViewController()
        }
        isClickedOnPush = false
        
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.tokenRefreshNotification),
            name: NSNotification.Name.MessagingRegistrationTokenRefreshed,
            object: nil)
        
        NotificationCenter.default.addObserver(self, selector:
            #selector(self.fcmConnectionStateChange), name:
            NSNotification.Name.MessagingRegistrationTokenRefreshed, object: nil)
        
        application.registerForRemoteNotifications()
        
        //Reachabilty Notifier
        reachabilityManager.startNetworkReachabilityObserver()
        reachabilityManager.delegate = self
        
        BBQNotificationManager.shared.listenUserNotificationSettings()
        return true//ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
//    private func configureMoEngage(launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
//        // Separate initialization methods for Dev and Prod initializations
//        //MoEngage.enableSDKLogs(true)
//
//        let sdkConfig = MOSDKConfig(withAppID: MOENGAGE_KEY)
//        sdkConfig.moeDataCenter = .data_center_03
//        sdkConfig.appGroupID = "group.com.app.BarbequeNation.MoEngage"
//
//        #if DEBUG
//          MoEngage.sharedInstance().initializeDefaultTestInstance(with: sdkConfig, andLaunchOptions: launchOptions)
//        #else
//          MoEngage.sharedInstance().initializeDefaultLiveInstance(with: sdkConfig, andLaunchOptions: launchOptions)
//        #endif
//
//
//        MoEngage.sharedInstance().enableSDK()
//        MoEngage.sharedInstance().registerForRemoteNotification(withCategories: nil, withUserNotificationCenterDelegate: self)
//        AnalyticsHelper.shared.setMoEngageUserProfile()
//    }
    
     func getBBQNotificationVC() -> BBQNotificationVC{
        let vc = BBQNotificationVC(nibName: "BBQNotificationVC", bundle: nil)
        return vc
    }
    
    func loadHomeViewController(){
        self.window?.rootViewController = UIStoryboard(name: "SplashScreen", bundle: nil).instantiateInitialViewController()
        navigationController = self.window?.rootViewController as? UINavigationController
        self.window?.makeKeyAndVisible()
    }
    
    lazy private var loginControllerViewModel : LoginControllerViewModel = {
        let loginVM = LoginControllerViewModel()
        return loginVM
    } ()
    
    @objc func tokenRefreshNotification(notification: NSNotification) {
        
        //get application instance ID
//        InstanceID.instanceID().instanceID { (result, error) in
//            if let error = error {
//                print("Error fetching remote instance ID: \(error)")
//            } else if let result = result {
//                print("Remote instance ID token: \(result.token)")
//            }
//        }
    }
    
    @objc func fcmConnectionStateChange() {
//        if Messaging.messaging().isAutoInitEnabled {
//            //Connected to FCM
//        } else {
//            //Disconnected from FCM
//        }
    }
    
    func loadNotificationData() -> NSMutableArray{
        // getting path to Notification.plist
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
        let documentsDirectory = paths.object(at: 0) as! NSString
        let path = documentsDirectory.appendingPathComponent("\(Constants.App.NotificationPage.notification).plist")
        let fileManager = FileManager.default
        //check if file exists
        if !fileManager.fileExists(atPath: path) {
            guard let bundlePath = Bundle.main.path(forResource: Constants.App.NotificationPage.notification, ofType: "plist") else { return []}
            do {
                try fileManager.copyItem(atPath: bundlePath, toPath: path)
            } catch let error as NSError {
                print("Unable to copy file. ERROR: \(error.localizedDescription)")
            }
        }
        var dataArray = NSMutableArray(contentsOfFile: path) ?? []
        for i in 0..<dataArray.count{
            if let indexData = dataArray[i] as? NSMutableDictionary{
                indexData["index"] = i
                dataArray[i] = indexData
            }
        }
        if dataArray.count > 1{
            dataArray = NSMutableArray(array:dataArray.sorted {
                if let data1 = $0 as? [String: Any], let data2 = $1 as? NSMutableDictionary{
                    return Int(String(format: "%@", data1["google.c.a.ts"] as? CVarArg ?? "0")) ?? 0 > Int(String(format: "%@", data2["google.c.a.ts"] as? CVarArg ?? "0")) ?? 0
                }
                return false
            })
        }
        return dataArray
    }
    
    func saveNotificationData(dataArray:NSMutableArray) {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
        if paths.count > 0, let documentsDirectory = paths.object(at: 0) as? NSString{
            let path = documentsDirectory.appendingPathComponent("\(Constants.App.NotificationPage.notification).plist")
            //writing to Notification.plist
            dataArray.write(toFile: path, atomically: false)
            let resultArray = NSMutableArray(contentsOfFile: path)
            print("Saved plist file is --> \(resultArray?.description ?? "")")
        }
    }    
    
    func storeNotificationContent(userInfo: [AnyHashable: Any]) {
        let dict: NSMutableDictionary = [:]
        
        if let messageID = userInfo[Constants.App.NotificationPage.gcmMessageIDKey] {
            print(messageID)//messaeID
        }
        UserDefaults.standard.set(true, forKey: Constants.App.NotificationPage.notificationActive)
        // title and body
        if let aps = userInfo[Constants.App.NotificationPage.aps] as? NSDictionary {
            if let alert = aps[Constants.App.NotificationPage.alert] as? NSDictionary {
                if let title = alert[Constants.App.NotificationPage.title] as? String {
                    dict.setValue(title, forKey: Constants.App.NotificationPage.title)
                }
                else{
                    dict.setValue("", forKey: Constants.App.NotificationPage.title)
                }
                if let body = alert[Constants.App.NotificationPage.body] as? String {
                    dict.setValue(body, forKey: Constants.App.NotificationPage.body)
                }else{
                    dict.setValue("", forKey: Constants.App.NotificationPage.body)
                }
            }
        }
        //image
        if let imageDictionary = userInfo[Constants.App.NotificationPage.fcm_options] as? NSDictionary {
            if let imageUrl = imageDictionary[Constants.App.NotificationPage.image] as? String {
                dict.setValue(imageUrl, forKey: Constants.App.NotificationPage.image)
            }
        }else{
            dict.setValue("", forKey: Constants.App.NotificationPage.image)
        }
        //tagline
        if let tagline = userInfo[Constants.App.NotificationPage.tagline] as? String {
            dict.setValue(tagline, forKey: Constants.App.NotificationPage.tagline)
        }else{
            dict.setValue("", forKey: Constants.App.NotificationPage.tagline)
        }
        //channel
        if let channelString = userInfo[Constants.App.NotificationPage.channel] as? String {
            dict.setValue(channelString, forKey: Constants.App.NotificationPage.channel)
        }else{
            dict.setValue("", forKey: Constants.App.NotificationPage.channel)
        }
        //booking_status
        if let booking_status = userInfo[Constants.App.NotificationPage.booking_status] as? String {
            dict.setValue(booking_status, forKey: Constants.App.NotificationPage.booking_status)
        }
        //invoice_url
        if let invoice_url = userInfo[Constants.App.NotificationPage.invoice_url] as? String {
            dict.setValue(invoice_url, forKey: Constants.App.NotificationPage.invoice_url)
        }
        //entity_id
        if let entity_id = userInfo[Constants.App.NotificationPage.entity_id] as? String {
            dict.setValue(entity_id, forKey: Constants.App.NotificationPage.entity_id)
        }
        //expirytime
        if let timeValue = userInfo[Constants.App.NotificationPage.expirytime] as? String {
            dict.setValue(timeValue, forKey: Constants.App.NotificationPage.expirytime)
        }
        if let timeValue = userInfo["google.c.a.ts"] as? String, timeValue.count > 0{
            dict.setValue(timeValue, forKey: "google.c.a.ts")
        }else{
            dict.setValue(String(format: "%ld",Date().timeIntervalSinceNow), forKey: "google.c.a.ts")
        }
        //unread
        dict.setValue(false, forKey: Constants.App.NotificationPage.readunread)
        let mutableArray = loadNotificationData()
        mutableArray.insert(dict, at: 0)
        //mutableArray.add(dict)
        saveNotificationData(dataArray: mutableArray)
        NotificationCenter.default.post(name: Notification.Name(Constants.App.NotificationPage.receiveNotification), object: nil)
        NotificationCenter.default.post(name: Notification.Name(Constants.App.NotificationPage.refreshNotification), object: nil)
        
        //Navigating to Home screen
        let state = UIApplication.shared.applicationState
        if state == .background || state == .inactive {
            // background
            if (userInfo[Constants.App.NotificationPage.channel] as? String ?? "") == Constants.App.NotificationPage.booking, let bookingId = userInfo[Constants.App.NotificationPage.entity_id] as? String, bookingId != ""{
                gotoHomePageFromPush(bookingId: bookingId)
            }
        } else if state == .active {
            if (userInfo[Constants.App.NotificationPage.channel] as? String ?? "") == Constants.App.NotificationPage.booking, let bookingId = userInfo[Constants.App.NotificationPage.entity_id] as? String, bookingId != ""{
                gotoHomePageFromPush(bookingId: bookingId)
            }
        }
    }
    
    private func gotoHomePageFromPush(bookingId: String?){
        if !BBQUserDefaults.sharedInstance.isGuestUser {
            bookingIdForPrint = bookingId
        }
        
        let currentNavigationStack = self.navigationController?.viewControllers
        guard let modifyingStack = currentNavigationStack else {
            return
        }
        if let lastController = modifyingStack.last {
            if let deliveryVC = lastController as? DeliveryTabBarController {
                //FIXME: Handle tabbar for this case
                /*let  homeController = UIStoryboard.loadHomeViewController()
                homeController.replaceControllerInCurrentStack(new: homeController)*/
                if !BBQUserDefaults.sharedInstance.isGuestUser {
                    if deliveryVC.selectedIndex == 0, let homeVC = deliveryVC.viewControllers?.first as? BBQHomeViewController{
                        homeVC.checkForReservationDetails()
                    }else{
                        deliveryVC.selectedIndex = 0
                    }
                }
               
            } else {
                /*let  homeController = UIStoryboard.loadHomeViewController()
                self.navigationController?.pushViewController(homeController, animated: false)*/
                lastController.goToHomePage()
            }
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        print("userInfo======",userInfo)
        storeNotificationContent(userInfo: userInfo)
        isClickedOnPush = true
        if UIApplication.shared.applicationState == .inactive{
            navigationController = nil
            application.keyWindow?.rootViewController = getBBQNotificationVC()
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        storeNotificationContent(userInfo: userInfo)
        isClickedOnPush = true
        if UIApplication.shared.applicationState == .inactive{
            navigationController = nil
            application.keyWindow?.rootViewController = getBBQNotificationVC()
        }
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let content = notification.request.content // to get the content
        print("info--->",content.userInfo)
        storeNotificationContent(userInfo: content.userInfo)
        //completionHandler([.alert, .sound]) // Display notification Banner (not required to show the banner)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        guard let fcmToken = fcmToken else { return }
        print("Firebase registration token: \(fcmToken)")
        
        let dataDict:[String: Any] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        BBQUserDefaults.sharedInstance.firebaseLocalToken = fcmToken
        
        if !isTokenExpiredChecked {
            return
        }
        
        self.notificationViewModel.checkAnUpdateFCMToken()
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    
//    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
//        print("Received data message: \(remoteMessage.appData)")
//    }
    func application(
      _ application: UIApplication,
      didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data
    ) {
      Messaging.messaging().apnsToken = deviceToken
    }
    
    private func application(application: UIApplication,
                             didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        Messaging.messaging().apnsToken = deviceToken as Data
    }
    
//    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
//        if userActivity.activityType == NSUserActivityTypeBrowsingWeb, let url = userActivity.webpageURL {
//            //  return handleDeepLinkURL(url: url)
//
//            print(userActivity.userInfo)
//            print(userActivity.referrerURL)
//            // print the url parameters dictionary
//            print(queryParameters(from: url))
//
//        }
//        return true
//    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
       
       // if DynamicLinks.dynamicLinks().shouldHandleDynamicLink(fromCustomSchemeURL: (userActivity.webpageURL!)){
            
            let handled = DynamicLinks.dynamicLinks().handleUniversalLink(userActivity.webpageURL!) { dynamiclink, error in
                
                let  parameterRetrieved = self.queryParameters(from: (dynamiclink?.url)!)
                UserDefaults.standard.set(parameterRetrieved["invitedBy"], forKey: referalCodeReceived)
            }
       // }
            return handled
        
    }
    func queryParameters(from url: URL) -> [String: String] {
        let urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false)
//        if urlComponents != nil {
//            test(components: urlComponents!)
//        }
        var queryParams = [String: String]()
        
        if (urlComponents?.queryItems != nil) {
            for queryItem: URLQueryItem in (urlComponents?.queryItems)! {
                if queryItem.value == nil {
                    continue
                }
                queryParams[queryItem.name] = queryItem.value
            }
        }
        return queryParams
    }

    
//    func test(components: URLComponents){
//        if let component = components as? URLComponents {
//
//            if let queryItems = component.queryItems {
//                for queryItem in queryItems {
//                    print("\(queryItem.name): \(queryItem.value)")
//                }
//            }
//        }
//    }
        
        
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool
    {
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
           // Handle the deep link. For example, show the deep-linked content or
           // apply a promotional offer to the user's account.
           // ...
            
            print(dynamicLink)
           return true
         }
         return false
    }
    
    /********* Facebook login delegate start *********/
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String, annotation: AnyObject?) -> Bool {
        return true
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return true
    }
    
    
    /********* Facebook login delegate End *********/
    
    func applicationWillResignActive(_ application: UIApplication) {
        if BBQUserDefaults.sharedInstance.accessToken.count==0{
            BBQUserDefaults.sharedInstance.CurrentSelectedLocation = ""
        }
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        print("applicationDidEnterBackground")
     //   BBQWelcomeCollectionViewCell.player?.pause()
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        print("applicationWillEnterForeground")
      //  BBQWelcomeCollectionViewCell.player?.play()
        BBQNotificationManager.shared.listenUserNotificationSettings()
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        print("applicationDidBecomeActive")
        // Fixed crash issue occuring on video play
//        if let player = BBQWelcomeCollectionViewCell.player{
//            player.play()
//        }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
      //   BBQWelcomeCollectionViewCell.player = nil
        //print(BBQWelcomeCollectionViewCell.player as Any)
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

extension AppDelegate: ReachabilityManagerProtocol {
    func isNetworkReachable(isConnected: Bool) {
        BBQNoConnectionHandler.noInternetScreen(show: !isConnected)
    }
}

extension UIApplication {
    static var appVersion: String? {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
    }
}
