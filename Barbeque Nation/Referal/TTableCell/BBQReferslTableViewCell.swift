//
 //  Created by Arpana on 30/01/23
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2023.
 //  All rights reserved.
 //  Last modified BBQReferslTableViewCell.swift
 //

import UIKit

class BBQReferslTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitleMessage :UILabel!
    @IBOutlet weak var dashedLine: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
