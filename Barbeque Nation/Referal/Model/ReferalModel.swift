//
 //  Created by Arpana on 02/01/23
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2023.
 //  All rights reserved.
 //  Last modified ReferalModel.swift
 //

import Foundation
import ObjectMapper

struct ReferalModel : Mappable {
    var message : String?
    var message_type : String?
    var data : ReferalData?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        message <- map["message"]
        message_type <- map["message_type"]
        data <- map["data"]
    }

}
struct ReferalData : Mappable {
    var referral_code : String?
    var short_url : String?
    var text_messages : [Text_messages]?
    var share_text : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        referral_code <- map["referral_code"]
        short_url <- map["short_url"]
        text_messages <- map["text_messages"]
        share_text <- map["share_text"]
    }

}

struct Text_messages : Mappable {
    var messgae : String?
    var bold_text : [String]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        messgae <- map["messgae"]
        bold_text <- map["bold_text"]
    }

}

//struct ReferalModel: Codable {
//   var message, message_type: String?
//   var referalData: ReferalData?
//
//   enum CodingKeys: String, CodingKey {
//       case message
//       case message_type
//       case referalData =  "data"
//   }
//}
//
//// MARK: - DataClass
//struct ReferalData: Codable {
//   var referralCode: String?
//   var shortURL: String?
//   var textMessages: String?
//
//   enum CodingKeys: String, CodingKey {
//       case referralCode = "referral_code"
//       case shortURL = "short_url"
//       case textMessages = "text_messages"
//   }
//}
//extension ReferalModel {
//    init(from decoder:Decoder) throws{
//        let codingKeys = try decoder.container(keyedBy: CodingKeys.self)
//        message_type = try codingKeys.decode(String.self, forKey: .message_type)
//        referalData = try codingKeys.decode(ReferalData.self, forKey: .referalData)
//        message =  try codingKeys.decode(String.self, forKey: .message)
//    }
//}
//extension ReferalData {
//    init(from decoder:Decoder) throws{
//        let codingKeys = try decoder.container(keyedBy: CodingKeys.self)
//        referralCode = try codingKeys.decode(String.self, forKey: .referralCode)
//        shortURL = try codingKeys.decode(String.self, forKey: .shortURL)
//        textMessages =  try codingKeys.decode(String.self, forKey: .textMessages)
//    }
//}
