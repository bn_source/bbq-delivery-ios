//
 //  Created by Abhijit on 12/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQOutletInfoManager.swift
 //

import Foundation


class BBQOutletInfoManager:BaseManager{
    
    class func outletInfo(branchId: String, completion: @escaping (_ model: OutletInfoModel?, _ result: Bool?)->()) {
        
        // The GET api call /api/v1/branches/null is sending request with null id
        //if branchid is null or "" or 0 , dont call the api

        if branchId == ""  ||  branchId == "0" {
            
            completion(nil, false)
        }
        
        let endPoint = BBQOutletInfoRouter.outletInfo(branchId: branchId)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let model = BBQOutletInfoManager.parseOutletInfoResponseData(data: responseData, error: responseError)
            DispatchQueue.main.async {
                model != nil ? completion(model, true) : completion(nil, false)
            }
        }
    }
}

extension BBQOutletInfoManager{
    
    private class func parseOutletInfoResponseData(data: Data?, error: Error?) -> OutletInfoModel? {
        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = data else {
                    return nil
                }
                let outletInfoDataModel = try decoder.decode(OutletInfoModel.self, from: responseData)
                return outletInfoDataModel

            } catch {
                return nil
            }
        } else {
            return nil
        }
    }
    
}
