//
 //  Created by Abhijit on 23/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQLastVisitedBranchManager.swift
 //

import Foundation


class BBQLastVisitedBranchManager: BaseManager {
    class func getLastVistedBranch(branchId: String, completion: @escaping (_ resonse: Data?, _ error: Error?)->()) {
        
        // Blocking API call for guest user
        if BBQUserDefaults.sharedInstance.isGuestUser {
            completion(nil, nil)
        }
        
        let endPoint = BBQLastVisitedBranchRouters.lastVistedBranch(branchId: branchId)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            completion(responseData, responseError)
        }
    }
}

