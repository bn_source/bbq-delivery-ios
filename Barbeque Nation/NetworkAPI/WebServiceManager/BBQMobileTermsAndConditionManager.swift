//
//  Created by Bhamidipati Kishore on 14/11/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQMobileTermsAndConditionManager.swift
//

import UIKit

class BBQMobileTermsAndConditionManager: BaseManager {
    
    class func getMobileTermsAndConditions(completion: @escaping (_ model: [BBQMobileTermsAndConditionsDataModel]?, _ result: Bool)->()) {
        let endPoint = BBQMobileTermsAndConditionRouter.getMobileTermsAndConditions
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQMobileTermsAndConditionManager.parseMobileTermsAndConditionsData(data: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
}

extension BBQMobileTermsAndConditionManager {
    
    private class func parseMobileTermsAndConditionsData(data: Data?, error: Error?) -> [BBQMobileTermsAndConditionsDataModel]? {
        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = data else {
                    return nil
                }
                let mobileTermsAndConditionsModel = try decoder.decode([BBQMobileTermsAndConditionsDataModel].self, from: responseData)
                return mobileTermsAndConditionsModel
                
            } catch {
                print(error)
                return nil
            }
        } else {
            if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse!.statusCode >= 300 {
                BBQUserManager.configureErrorModel(data: data)
            }
            return nil
        }
    }
}
