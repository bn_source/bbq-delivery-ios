//
//  FileBBQHomeManager.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 04/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import Foundation
class BBQHomeManager: BaseManager {
    
    class func getNearbyOutletDetails(latitude lat: Double, longitude long: Double, completion: @escaping (_ resonse: Data?, _ error: Error?)->()) {
        let endPoint = BBQHomeRouter.getNearbyOutlet(latitude: lat, longitude: long)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            completion(responseData, responseError)
        }
    }
    
    class func getNearbyOutletDetailsV1(latitude lat: Double, longitude long: Double, completion: @escaping (_ resonse: Data?, _ error: Error?)->()) {
        let endPoint = BBQHomeRouter.getNearbyOutletV1(latitude: lat, longitude: long)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            completion(responseData, responseError)
        }
    }
    
    class func getVouchersAndPromotions(latitude lat: Double, longitude long: Double, brachID branc_id: String, completion: @escaping (_ resonse: Data?, _ error: Error?)->()) {
        let endPoint = BBQHomeRouter.getPromotionsAndVouchers(latitude: lat, longitude: long, branch_id: branc_id)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            completion(responseData, responseError)
        }
    }
    
    class func getVoucherDetails(voucherId: String, completion: @escaping (_ model: VoucherDetails?, _ result: Bool)->()) {
        let endPoint = BBQHomeRouter.voucherDetails(voucherId: voucherId)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQHomeManager.parseVoucherDetailsData(data: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
    
    class func getPromotionDetails(promotionId: String, completion: @escaping (_ model: PromotionDetailsModel?, _ result: Bool)->()) {
        let endPoint = BBQHomeRouter.promotionDetails(promotionId: promotionId)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQHomeManager.parsePromotionDetailsData(data: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
    
    class func getupcomingReservations(completion: @escaping (_ resonse: Data?, _ error: Error?)->()) {
        
        // Blocking API call for guest user
        if BBQUserDefaults.sharedInstance.isGuestUser {
            completion(nil, nil)
        }
        
        let endPoint = BBQHomeRouter.getUpcomingReservation
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            completion(responseData, responseError)
        }
    }
    
    class func latLong(latitude:Float,longitude:Float,completion:@escaping(_ _model:LocationResponse?,_ _result :Bool)->()) {
        let endPoint = BBQHomeRouter.latLong(lat: latitude, long: longitude, only_reservation: true)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let locationDataModel = self.parseLocationResponseData(data: responseData, error: responseError)
            DispatchQueue.main.async {
                locationDataModel != nil ? completion(locationDataModel, true): completion(nil, true)
            }
        }
    }
}

extension BBQHomeManager {
    
    private class func parseVoucherDetailsData(data: Data?, error: Error?) -> VoucherDetails? {
        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = data else {
                    return nil
                }
                let voucherDetailsDataModel = try decoder.decode(VoucherDetails.self, from: responseData)
                return voucherDetailsDataModel
                
            } catch {
                print(error)
                return nil
            }
        } else {
            if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse!.statusCode >= 300 {
                BBQUserManager.configureErrorModel(data: data)
            }
            return nil
        }
    }
    
    private class func parsePromotionDetailsData(data: Data?, error: Error?) -> PromotionDetailsModel? {
        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = data else {
                    return nil
                }
                let voucherDetailsDataModel = try decoder.decode(PromotionDetailsModel.self, from: responseData)
                return voucherDetailsDataModel
                
            } catch {
                print(error)
                return nil
            }
        } else {
            if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse!.statusCode >= 300 {
                BBQUserManager.configureErrorModel(data: data)
            }
            return nil
        }
    }
    
    private class func parseLocationResponseData(data: Data?, error: Error?) -> LocationResponse? {
        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = data else {
                    
                    return nil
                }
                var locationDataModel = try decoder.decode(LocationResponse.self, from: responseData)
                if var currentUserCity = currentUserCity, currentUserCity != ""{
                    currentUserCity = currentUserCity.lowercased()
                    for i in 0 ..< (locationDataModel.bcitydata?.count ?? 0){
                        if locationDataModel.bcitydata?.count ?? 0 > i, currentUserCity == locationDataModel.bcitydata?[i].city_name?.lowercased() ?? "" {
                            if let cities = locationDataModel.bcitydata?[i]{
                                locationDataModel.bcitydata?.remove(at: i)
                                locationDataModel.bcitydata?.insert(cities, at: 0)
                            }
                            break
                        }
                    }
                }
                return locationDataModel
            } catch {
                return nil
            }
        } else {
            return nil
        }
    }
}
