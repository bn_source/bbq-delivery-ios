//
//  BBQReachabilityManager.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 06/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import Foundation
import Alamofire

protocol ReachabilityManagerProtocol: AnyObject {
    func isNetworkReachable(isConnected: Bool)
}

class ReachabilityManager {
    
    //shared instance
    static let shared = ReachabilityManager()
    private init() { }
    
    private let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.google.com")
    
    weak var delegate: ReachabilityManagerProtocol? = nil
    
    func startNetworkReachabilityObserver() {
        
        var isConnected = false
        reachabilityManager?.listener = { status in
            switch status {
                
            case .notReachable:
                print("The network is not reachable")
                isConnected = false
                
            case .unknown:
                print("It is unknown whether the network is reachable")
                isConnected = true
                
            case .reachable(.ethernetOrWiFi), .reachable(.wwan):
                isConnected = true
                print("The network is reachable over the WiFi/WWAN connection")
            }
            
            if let delegate = self.delegate {
                delegate.isNetworkReachable(isConnected: isConnected)
            }
        }
        
        // start listening
        reachabilityManager?.startListening()
    }
}

extension ReachabilityManager {
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
}
