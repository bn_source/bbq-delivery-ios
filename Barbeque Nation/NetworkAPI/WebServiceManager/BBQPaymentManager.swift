//
//  BBQPaymentManager.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 04/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import Foundation

class BBQPaymentManager : NSObject {
    
    
    class func createCustomer(completion: @escaping (_ resonse: Data?, _ error: Error?)->()) {
        let reqURL = BBQPaymentRouter.createCustomer(name: SharedProfileInfo.shared.profileData?.name ?? "", email: SharedProfileInfo.shared.profileData?.email ?? "", contact: SharedProfileInfo.shared.profileData?.mobileNumber ?? "")
        BBQWebServiceHandler.request(url: reqURL) { (optionalResponse) in
            
            if let response = optionalResponse?.data {
                completion(response, nil)
            }
            else {
                completion(nil, optionalResponse?.error)
            }
        }
        
    }
    class func fetchSavedTokens(completion: @escaping (_ resonse: Data?, _ error: Error?)->()) {
        let reqURL = BBQPaymentRouter.fetchSavedTokens(SharedProfileInfo.shared.profileData?.razorpayId ?? "")
        BBQWebServiceHandler.request(url: reqURL) { (optionalResponse) in
            if let response = optionalResponse?.data {
                completion(response, nil)
            }
            else {
                completion(nil, optionalResponse?.error)
            }
        }
    }
    
    class func createPaymentOrder(amount: String,
                                 currency: String,
                                 receipt: String,
                                 payment_capture: String,
                                 completion: @escaping (_ resonse: Data?, _ error: Error?)->()) {
        let reqURL = BBQPaymentRouter.createOrder(amount: amount, currency: currency,
                                                  receipt: receipt,
                                                  payment_capture: payment_capture)
        BBQWebServiceHandler.request(url: reqURL) { (optionalResponse) in
            
            if let response = optionalResponse?.data {
                completion(response, nil)
            }
            else {
                completion(nil, optionalResponse?.error)
            }
        }
    }
    
    class func createRazorpayOrder(amount: String, currency: String, bookingID: String, loyalty_points: Int?, completion: @escaping (_ resonse: Data?, _ error: Error?)->()) {
        let reqURL = BBQPaymentRouter.rpCreateOrder(amount: amount, currency: currency, bookingID: bookingID, loyalty_points: loyalty_points)
        
        BaseManager.requestforServiceWith(endPoint: reqURL) { (response, error) in
            //let responseString = BBQPaymentManager.parseRazorpayCreateOrderResponse(data: response, error: error)
            
            if let responseString = response {
                completion(responseString, nil)
            }
            else {
                completion(nil, error)
            }
        }
    }
    
    class func getCartPaymentDetails(discount: String = "0", loyalty_points: String = "0", bar_code: String = "", completion: @escaping (_ model: BBQCartPayementModel?, _ result: Bool)->()) {
        
        let endPoint = BBQPaymentRouter.rpCreateVoucherOrder(discount: discount, loyalty_points: loyalty_points, bar_code: bar_code)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQPaymentManager.parseCartPaymentData(data: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
    
    class func paymentCheckout(points: Int, barCode: String, razorpayPaymentId: String, razorpayOrderId: String, razorpaySignature: String, completion: @escaping (_ resonseString: String?, _ result: Bool)->()) {
        
        let endPoint = BBQBookingRouter.cartCheckout(points: points, barCode: barCode, razorpayPaymentId: razorpayPaymentId, razorpayOrderId: razorpayOrderId, razorpaySignature: razorpaySignature)
        
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let responseString = BBQPaymentManager.parsePaymentCheckoutResponse(data: responseData, error: responseError)
            if let value = responseString {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
}

extension BBQPaymentManager {
    class func parsePaymentCheckoutResponse(data: Data?, error: Error?) -> String? {
        if error == nil {
            do {
                guard let responseData = data else {
                    return nil
                }
                let responseJson = try JSONSerialization.jsonObject(with: responseData, options: .allowFragments) as? Json
                let status = responseJson?["message"] as? String ?? ""
                return status
                
            } catch {
                print(error)
                return nil
            }
        } else {
            if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse!.statusCode >= 300 {
                BBQUserManager.configureErrorModel(data: data)
            }
            return nil
        }
    }
    
    class func parseRazorpayCreateOrderResponse(data: Data?, error: Error?) -> String? {
        if error == nil {
            do {
                guard let responseData = data else {
                    return nil
                }
                let responseJson = try JSONSerialization.jsonObject(with: responseData, options: .allowFragments) as? Json
                let status = responseJson?["message"] as? String ?? ""
                return status
                
            } catch {
                print(error)
                return nil
            }
        } else {
            if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse!.statusCode >= 300 {
                BBQUserManager.configureErrorModel(data: data, isDisplayRequired: true)
            }
            return nil
        }
    }
    
    private class func parseCartPaymentData(data: Data?, error: Error?) -> BBQCartPayementModel? {
        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = data else {
                    return nil
                }
                let cartPaymentModel = try decoder.decode(BBQCartPayementModel.self, from: responseData)
                return cartPaymentModel
                
            } catch {
                print(error)
                return nil
            }
        } else {
            if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse!.statusCode >= 300 {
                BBQUserManager.configureErrorModel(data: data)
            }
            return nil
        }
    }
}
