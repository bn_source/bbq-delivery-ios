//
 //  Created by Rabindra L on 03/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQFeedbackManager.swift
 //

import Foundation
import Alamofire

class BBQFeedbackManager: BaseManager {
    
    class func getBookingFeedbackQuestions(completion: @escaping (_ resonse: Data?, _ error: Error?)->()) {
        let endPoint = BBQFeedbackRouter.bookingFeedbackQuestions
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            completion(responseData, responseError)
        }
    }
    
    class func submitBookingFeedback(rating: Int, bookingID:String, qid:Int, completion: @escaping (_ resonse: Data?, _ error: Error?)->()) {
        let endPoint = BBQFeedbackRouter.submitBookingFeedback(rating: rating, bookingID: bookingID, qid: qid)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            completion(responseData, responseError)
        }
    }
}
