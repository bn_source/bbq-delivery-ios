//
//  BBQBookingManager.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 03/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import Foundation
import Alamofire

// TODO: Move all error creating codes to base class.
class BBQBookingManager: BaseManager {
    
    class func getBookingHistory(status: Int, pageNo: String, completion: @escaping (_ resonse: Data?, _ error: Error?)->()) {
        let endPoint = BBQBookingRouter.getBookingHistory(status: status, pageNo: pageNo)
        BaseManager.requestforServiceWith( endPoint: endPoint) { (responseData, responseError) in
            completion(responseData, responseError)
        }
    }
    
    class func getBookingSlots(reservationDate date: String, branchId id: String, latitude lat: Double, longitude long: Double, dinnerType type: String, completion: @escaping (_ resonse: Data?, _ error: Error?)->()) {
        
        let endPoint = BBQBookingRouter.getSlots(reservationDate: date, branchId: id, lat: lat, long: long, dinnerType: type)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            completion(responseData, responseError)
        }
    }
    
    class func getBookingSlotsWithBranch(reservationDate date: String, branchId id: String, dinnerType type: String, completion: @escaping (_ model: BookingSlotsWithBranchDataModel?, _ result: Bool)->()) {
        
        let endPoint = BBQBookingRouter.getSlotWithBranch(reservationDate: date, branchId: id, dinnerType: type)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQBookingManager.parseBookingSlotsWithBranchResponse(response: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
    
    class func updateBooking(bookingId id: String, paymentOrderId orderId:String, paymentId payId: String, paymentGatewayName gatewayName: String, paymentSignature signature:String, completion: @escaping (_ model:CreatebookingDataModel?, _ result: Bool)->()) {
        
        let endPoint = BBQBookingRouter.updateBooking(bookingId: id, paymentOrderId: orderId, paymentId: payId, paymentGatewayName: gatewayName, paymentSignature: signature)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQBookingManager.parseUpdateBookingResponse(response: responseData, error:responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
    
    class func checkForReservationRules(createBookingData: Json, no_of_pax: Int, payable_amt: Double, completion: @escaping (_ model:PendingBookingViewModel.PendingBookingData?, _ result: Bool)->()) {
        var params = createBookingData
        params["mobile_no"] = BBQUserDefaults.sharedInstance.customerId
        params["customer_id"] = BBQUserDefaults.sharedInstance.customerId
        params["no_of_pax"] = no_of_pax
        params["payable_amt"] = Double(String(format: "%.2f", payable_amt)) ?? 0.0
        print("parameters for booking \(params)")
        let endPoint = BBQBookingRouter.checkReservationRules(createBookingJsonData: params)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQBookingManager.parseCheckReservationRules(response: responseData, error:responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
    
    class func updateReservationAdvPayment(params: [String: Any], completion: @escaping (_ model:PendingBookingViewModel.ReservationAdvPayment?, _ result: Bool)->()) {
        var payloadParams = params
        payloadParams["customer_id"] = BBQUserDefaults.sharedInstance.customerId
        let endPoint = BBQBookingRouter.updateReservationPayment(params: payloadParams)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQBookingManager.parseReservationAdvPayment(response: responseData, error:responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
    
    class func getSpecialInstruction( completion: @escaping (_ resonse: Data?, _ error: Error?)->()) {
        let endPoint = BBQBookingRouter.getSpecialInstructions
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            completion(responseData, responseError)
        }
    }
    
    class func updateSpecialInstruction(bookingId id: String, occassionIds occasions: [String], joinedBy by: [String], specialNote note: String, completion: @escaping (_ resonse: Data?, _ error: Error?)->()) {
        
        let endPoint = BBQBookingRouter.updateSpecialInstructions(bookingId: id, occassionIds: occasions, joinedBy: by, specialNote: note)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            completion(responseData, responseError)
        }
    }
    
    class func getmenubranchBuffet(branchId id: String, reservationDate date: String, time reservationTime: String,
                                   slotId slot: Int, isEarlyBird: Bool, isLateBird: Bool,
                                   completion: @escaping (_ model: BranchBuffetDataModel?, _ result: Bool)->()) {
        
        let endPoint = BBQBookingRouter.getmenubranchforreservation(branchId: id, reservationDate: date, reservationTime: reservationTime, slotId: slot, isEarlyBird: isEarlyBird, isLateBird: isLateBird)
        BaseManager.requestforServiceWith(endPoint: endPoint){ (responseData, responseError) in
            let modelValue = BBQBookingManager.parseMenuBranchBuffetResponse(response: responseData, error: responseError)
            if let value = modelValue {
                print("modelvalue>>\(value)")
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
    
    class func getCancelReaosns(completion: @escaping (_ resonse: Data?, _ error: Error?)->()) {
        let endPoint = BBQBookingRouter.cancelReasons
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            completion(responseData, responseError)
        }
    }
    
    class func cancelBooking(bookingId id: String, reasonID:Int, refundType:String, completion: @escaping (_ resonse: Data?, _ error: Error?)->()) {
        
        let endPoint = BBQBookingRouter.cancelBooking(bookingId: id, reasonID: reasonID, refundType: refundType)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            completion(responseData, responseError)
        }
    }
    
    class func refundOptions(bookingID: String, completion: @escaping (_ resonseMoel: BBQRefundDataModel?,
        _ error: Error?)->()) {
        let endPoint = BBQBookingRouter.refundOptions(bookingID: bookingID)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (resultData, error) in
            
            let modelValue = self.parseRefundOptionsData(response: resultData, error: error)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, error)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, error)
                }
            }
        }
    }
    
    class func createBooking(createBookingJsonData: Json, completion: @escaping (_ model: CreatebookingDataModel?, _ result: Bool,_ error: String?)->()){
        
        let endPoint = BBQBookingRouter.createBooking(createBookingJsonData: createBookingJsonData)
        print(endPoint)
        BaseManager.requestforServiceWith(endPoint: endPoint){ (responseData, responseError) in
            print(responseData)
            let modelValue = BBQBookingManager.parseCreateBookingResponse(response: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true, nil)
                    if let errors = value.fieldErrors, errors.count > 0{
                        do {
                            if let data = responseData{
                                FirestoreHelper.shared.uploadData(input: createBookingJsonData, output: try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String : Any] ?? [String: Any]())
                            }
                        } catch {
                            
                        }
                       
                    }
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false, responseError.debugDescription)
                    FirestoreHelper.shared.uploadData(input: createBookingJsonData, output: ["serverError": responseError.debugDescription, "desc": responseError?.localizedDescription as Any])
                }
            }
        }
    }
    
    class func updateFinalPayment(updatePayJsonData : Json,
                                  completion: @escaping (_ model:CreatebookingDataModel?, _ result: Bool)->()) {
        
        let endPoint = BBQBookingRouter.updateFinalPayment(updateFinalPayData: updatePayJsonData)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQBookingManager.parseUpdateBookingResponse(response: responseData, error:responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
    

    
    class func getVoucherHistory(pageNo: String, completion: @escaping (_ model: VoucherHistoryModel?, _ result: Bool)->()) {
        
        let endPoint = BBQBookingRouter.getVoucherHistory(pageNo: pageNo)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQBookingManager.parseVoucherHistoryDataModel(resonse: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, false)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, true)
                }
            }
        }
    }
    
    class func getVoucherAndCoupons(completion: @escaping (_ model: VoucherCouponsModel?, _ result: Bool)->()) {
        
        let endPoint = BBQBookingRouter.getVoucherAndCoupon
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQBookingManager.parseVoucherCouponsData(data: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
    
    class func verifyBookingVoucher(barCode: String,covers: Int,voucherAmount: Double, completion: @escaping (_ model: VoucherCouponsModel?, _ result: Bool)->()) {
        
        let endPoint = BBQBookingRouter.verifyVoucher(voucherBarCode: barCode, voucherCovers: covers, amount: voucherAmount)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQBookingManager.parseVoucherCouponsData(data: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
    
    class func rescheduleBooking(rescheduleBookingJsonData: Json, completion: @escaping (_ model: CreatebookingDataModel?, _ result: Bool)->()) {
        
        let endPoint = BBQBookingRouter.rescheduleBooking(rescheduleBookingJsonData: rescheduleBookingJsonData)
        BaseManager.requestforServiceWith(endPoint: endPoint){ (responseData, responseError) in
            let modelValue = BBQBookingManager.parseCreateBookingResponse(response: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
    
    class func rescheduleResetAll(bookingId: String, completion: @escaping (_ result: Bool)->()) {
        
        let endPoint = BBQBookingRouter.rescheduleResetAll(bookingId: bookingId)
        BaseManager.requestforServiceWith(endPoint: endPoint){ (responseData, responseError) in
            if responseError == nil {
                DispatchQueue.main.async {
                    completion(true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(false)
                }
            }
        }
    }
    
    class func afterDiningPaymentData(for bookingID: String, completion: @escaping (_ model: BBQDiningDataModel?,
        _ error: Error?)->()) {
        
        let endPoint = BBQBookingRouter.bookingPaymentData(bookingID: bookingID)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQBookingManager.parseAfterDiningPayment(response: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, responseError)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, responseError)
                }
            }
        }
    }
    
    class func afterDiningPaymentDataDetails(for bookingID: String, completion: @escaping (_ model: BBQDiningDataModel?,
        _ error: Error?)->()) {
        
        let endPoint = BBQBookingRouter.afterDiningPaymentData(bookingID: bookingID)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQBookingManager.parseAfterDiningPayment(response: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, responseError)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, responseError)
                }
            }
        }
    }
    
    class func giftVoucher(barCode: String, countryCode: String, phoneNumber: String, email: String, name: String, completion: @escaping (_ result: Bool)->()) {
        
        let endPoint = BBQBookingRouter.giftVoucher(barCode: barCode, countryCode: countryCode, mobileNumber: phoneNumber, email: email, name: name)
        BaseManager.requestforServiceWith(endPoint: endPoint){ (responseData, responseError) in
            let _ = BBQBookingManager.parseGiftVoucherResponse(response: responseData, error: responseError)
            
            if BBQBookingManager.networkResponse != nil,
                BBQBookingManager.networkResponse!.statusCode >= 200, BBQBookingManager.networkResponse!.statusCode < 300 {
                DispatchQueue.main.async {
                    completion(true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(false)
                }
            }
        }
    }
}

extension BBQBookingManager {
    private class func parseVoucherCouponsData(data: Data?, error: Error?) -> VoucherCouponsModel? {
        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = data else {
                    return nil
                }
                let voucherCouponsModel = try decoder.decode(VoucherCouponsModel.self, from: responseData)
                return voucherCouponsModel
                
            } catch {
                print(error)
                return nil
            }
        } else {
            if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse!.statusCode >= 300 {
                BBQUserManager.configureErrorModel(data: data)
            }
            return nil
        }
    }
    
    private class func parseBookingSlotsWithBranchResponse(response: Data?, error: Error?)-> BookingSlotsWithBranchDataModel? {
        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = response else {
                    return nil
                }
                let bookingSlotsWithBranchDataModel = try decoder.decode(BookingSlotsWithBranchDataModel.self, from: responseData)
                return bookingSlotsWithBranchDataModel
            } catch {
                return nil
            }
        } else {
            if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse!.statusCode >= 300 {
                BBQUserManager.configureErrorModel(data: response, isDisplayRequired: true)
            }
            return nil
        }
    }
    
    private class func parseCreateBookingResponse(response: Data?, error: Error?)-> CreatebookingDataModel? {
        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = response else {
                    return nil
                }
                let createBookingDataModel = try decoder.decode(CreatebookingDataModel.self, from: responseData)
                return createBookingDataModel
            } catch {
                return nil
            }
        } else {
            do {
                let decoder = JSONDecoder()
                guard let responseData = response else {
                    return nil
                }
                let createBookingDataModel = try decoder.decode(CreatebookingDataModel.self, from: responseData)
                if let errors = createBookingDataModel.fieldErrors, errors.count > 0{
                    return createBookingDataModel
                }
            } catch {
                return nil
            }
            if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse!.statusCode >= 300 {
                BBQUserManager.configureErrorModel(data: response, isDisplayRequired: true)
            }
            return nil
        }
    }
    
    private class func parseMenuBranchBuffetResponse(response: Data?, error: Error?)-> BranchBuffetDataModel? {
        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = response else {
                    return nil
                }
                let menuBranchBuffetDataModel = try decoder.decode(BranchBuffetDataModel.self, from: responseData)
                print("\(menuBranchBuffetDataModel)")
                return menuBranchBuffetDataModel
            } catch {
                return nil
            }
        } else {
            if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse!.statusCode >= 300 {
                BBQUserManager.configureErrorModel(data: response, isDisplayRequired: true)
            }
            return nil
        }
    }
    
    private class func parseUpdateBookingResponse(response: Data?, error: Error?)->CreatebookingDataModel? {
        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = response else {
                    return nil
                }
                let updateBookingDataModel = try decoder.decode(CreatebookingDataModel.self, from: responseData)
                return updateBookingDataModel
            } catch {
                return nil
            }
        } else {
            if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse!.statusCode >= 300 {
                BBQUserManager.configureErrorModel(data: response, isDisplayRequired: true)
            }
            return nil
        }
    }
    
    private class func parseCheckReservationRules(response: Data?, error: Error?)->PendingBookingViewModel.PendingBookingData? {
        if error == nil {
            do {
                guard let responseData = response else {
                    return nil
                }
                guard let json = try JSONSerialization.jsonObject(with: responseData, options: .allowFragments) as? [String: Any] else{
                    return nil
                }
                let updateBookingDataModel = PendingBookingViewModel.PendingBookingData(JSON: json, context: nil)
                return updateBookingDataModel
            } catch {
                return nil
            }
        } else {
            if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse!.statusCode >= 300 {
                BBQUserManager.configureErrorModel(data: response, isDisplayRequired: true)
            }
            return nil
        }
    }
    
    private class func parseReservationAdvPayment(response: Data?, error: Error?)->PendingBookingViewModel.ReservationAdvPayment? {
        if error == nil {
            do {
                guard let responseData = response else {
                    return nil
                }
                guard let json = try JSONSerialization.jsonObject(with: responseData, options: .allowFragments) as? [String: Any] else{
                    return nil
                }
                let updateBookingDataModel = PendingBookingViewModel.ReservationAdvPayment(JSON: json, context: nil)
                return updateBookingDataModel
            } catch {
                return nil
            }
        } else {
            if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse!.statusCode >= 300 {
                BBQUserManager.configureErrorModel(data: response, isDisplayRequired: true)
            }
            return nil
        }
    }
    
    private class func parseVoucherHistoryDataModel(resonse: Data?, error: Error?) -> VoucherHistoryModel? {
        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = resonse else {
                    return nil
                }
                let voucherHistoryDataModel = try decoder.decode(VoucherHistoryModel.self, from: responseData)
                return voucherHistoryDataModel
            } catch {
                return nil
            }
        } else {
            if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse!.statusCode >= 300 {
                BBQUserManager.configureErrorModel(data: resonse)
            }
            
            return nil
        }
    }
    
    private class func parseRefundOptionsData(response: Data?, error: Error?)-> BBQRefundDataModel? {
        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = response else {
                    return nil
                }
                let refundModel = try decoder.decode(BBQRefundDataModel.self, from: responseData)
                return refundModel
            } catch {
                return nil
            }
        } else {
            if BBQUserManager.networkResponse != nil,
                BBQUserManager.networkResponse!.statusCode >= 300 {
                BBQUserManager.configureErrorModel(data: response, isDisplayRequired: true)
            }
            return nil
        }
    }
    
    class func parseAfterDiningPayment(response: Data?, error: Error?)-> BBQDiningDataModel? {
        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = response else {
                    return nil
                }
                let diningModel = try decoder.decode(BBQDiningDataModel.self, from: responseData)
                return diningModel
            } catch {
                return nil
            }
        } else {
            if BBQUserManager.networkResponse != nil,
                BBQUserManager.networkResponse!.statusCode >= 300 {
                BBQUserManager.configureErrorModel(data: response, isDisplayRequired: true)
            }
            return nil
        }
    }
    
    private class func parseGiftVoucherResponse(response: Data?, error: Error?) -> Bool {
        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = response else {
                    return false
                }
                let gitVoucherModel = try decoder.decode(BBQGiftVoucherModel.self, from: responseData)
                if let message = gitVoucherModel.message {
                    if message.lowercased() == "success" {
                        return true
                    } else {
                        return false
                    }
                }
            } catch {
                return false
            }
        } else {
            if BBQUserManager.networkResponse != nil,
                BBQUserManager.networkResponse!.statusCode >= 300 {
                BBQUserManager.configureErrorModel(data: response, isDisplayRequired: true)
            }
            return false
        }
        return false
    }
}

