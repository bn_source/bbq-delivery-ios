//
//  BBQUserManager.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 04/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import Foundation
import Alamofire

class BBQUserManager: BaseManager {
    
    class func getAppUpdateInformation(appVersion: String,
                                       deviceType: String,
                                       completion: @escaping (_ model: UpdateInfoModel?, _ result: Bool)->()) {
        let endPoint = BBQUserRouter.getUpdateInfo(currentVersion: appVersion,
                                                   deviceType: deviceType)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQUserManager.parseUpdateInformation(response: responseData,
                                                                   error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }

    class func getUser(completion: @escaping (_ model: ProfileDataModel?, _ result: Bool)->()) {
        let endPoint = BBQUserRouter.getUser
        
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQUserManager.parseUserResponseData(data: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
    
    class func updateUser(title: String, newsLetter: String, maritalStatus: String, name: String,
                          dob: String, anniversary: String, isNotificationEnabled: Bool,
                          completion: @escaping (_ resonseMessage: String?, _ result: Bool)->()) {
        
        
       
        //MARK: Remove it after testing the build
        let shortenName = name.prefix(50) // playground

        
        let endPoint = BBQUserRouter.updateUser(title: title, newsLetter: newsLetter, maritalStatus: maritalStatus,
                                                name: String(shortenName), dob: dob, anniversary: anniversary, isNotificationEnabled: isNotificationEnabled)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let resString = BBQUserManager.parseUpdateUserResponseData(data: responseData, error: responseError)
            if let value = resString {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
    
    //Delete is not used as of now. But lets keep it for any future use.
    class func deleteUser(otp: Int, otpId: Int, completion: @escaping (_ resonseMessage: String?, _ result: Bool)->()) {
        
        let endPoint = BBQUserRouter.deleteUser(otp: otp, otpId: otpId)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let resString = BBQUserManager.parseUpdateUserResponseData(data: responseData, error: responseError)
            if let value = resString {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
    
    class func updateUserMobile(countryCode: String, mobileNumber: Int64, otpId: Int, otp: Int, completion: @escaping (Bool) -> ()) {
        
        let endPoint = BBQUserRouter.updateUserMobile(countryCode: countryCode, mobileNumber: mobileNumber, otpId: otpId, otp: otp)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let resString = BBQUserManager.parseUpdateEntries(response: responseData, error: responseError)
            if let _ = resString {
                DispatchQueue.main.async {
                    completion(true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(false)
                }
            }
        }
    }
    
    class func  generateEmailOTP(emailID: String,
                                 completion: @escaping (_ model: GenerateOTPDataModel?, _ result: Bool) -> ()) {
        let endPoint = BBQUserRouter.generateEmailOTP(email: emailID)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQLoginManager.parseGenerateOTPResponse(resonse: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
    class func updateRazorPayID(razorpayID:String,completion: @escaping (Bool) -> ()) {
        let endPoint = BBQUserRouter.updateRazorpayID(razorpayID: razorpayID)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let resString = BBQUserManager.parseUpdateEntries(response: responseData, error: responseError)
            if let _ = resString {
                DispatchQueue.main.async {
                    completion(true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(false)
                }
            }
        }
    }
    class func updateUserEmail(email: String, otpID: Int,
                               otp: Int,
                               completion: @escaping (Bool) -> ()) {
        
        let endPoint = BBQUserRouter.updateUserEmail(email: email, otpID: otpID, otp: otp)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let resString = BBQUserManager.parseUpdateEntries(response: responseData, error: responseError)
            if let _ = resString {
                DispatchQueue.main.async {
                    completion(true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(false)
                }
            }
        }
    }
    
    class func getUserLocation(from lattitude: String, longtitude: String,
                               completion: @escaping (String) -> ()) {
    
    }
    
    /* Uploads profile image as Alamofire multipart data. No compressioning is doing, image is
       uploading directly.
    */
    class func updateUserProfileImage(selectedImage: UIImage, fileName : String, completion: @escaping (Bool) ->()) {
        
        let headers = BBQUserRouter.updateUserProfileImage.headers
        let presignedUrl = BBQUserRouter.baseUrl + BBQUserRouter.updateUserProfileImage.path
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
        
                if let imageData = selectedImage.compressedImage(.low) {
                    multipartFormData.append(imageData,
                                             withName: "profile_picture",
                                             fileName: fileName,
                                             mimeType: "image/*")
                }
        },
            to: presignedUrl,
            method:.put,
            headers:headers,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseString { response in
                        debugPrint(response)
                        completion(true)
                        }
                        .uploadProgress { progress in
                            debugPrint("Upload Progress: \(progress.fractionCompleted)")
                    }
                    return
                case .failure(let encodingError):
                    completion(false)
                    debugPrint(encodingError)
                }
        })
    }
    
    // Fetching instagram user information using access token
    class func getInstagramUser(with accessToken: String,
                                completion: @escaping (_ resonse: Data?, _ error: Error?)->()) {
        let endURL = BBQUserRouter.instagramUser(accessToken: accessToken)
        BBQWebServiceHandler.request(url: endURL) { (optionalResponse) in
            
            if let response = optionalResponse?.data {
                completion(response, nil)
            }
            else {
                completion(nil, optionalResponse?.error)
            }
        }
    }
}

//MARK:- Parsing Logic
extension BBQUserManager {
    private class func parseUserResponseData(data: Data?, error: Error?) -> ProfileDataModel? {
        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = data else {
                    return nil
                }
                let profileDataModel = try decoder.decode(ProfileDataModel.self, from: responseData)
                BBQUserDefaults.sharedInstance.UserName = profileDataModel.name ?? ""
                BBQUserDefaults.sharedInstance.UserLoyaltyPoints = Int(profileDataModel.loyaltyPoints ?? 0)
                
                if !BBQBookingUtilies.shared.isLocationAlreadySet() {BBQUserDefaults.sharedInstance.CurrentSelectedLocation =
                    profileDataModel.lastVisitedBranchName ?? ""
                    BBQUserDefaults.sharedInstance.branchIdValue = profileDataModel.lastVisitedBranch ?? ""
                }
                
                SharedProfileInfo.shared.loadSharedProfileInfo(profileInfo: profileDataModel)
                return profileDataModel
                
            } catch {
                print(error)
                return nil
            }
        } else {
            if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse?.statusCode == 400 {
                BBQUserManager.configureErrorModel(data: data)
            }
            return nil
        }
    }
    
    private class func parseUpdateUserResponseData(data: Data?, error: Error?) -> String? {
        if error == nil {
            guard let responseData = data else {
                return nil
            }
            do {
                let responseJson = try JSONSerialization.jsonObject(with: responseData, options: .allowFragments) as? Json
                let responseMessage = responseJson?["message"] as? String ?? ""
                return responseMessage
            } catch {
                return nil
            }
        } else {
            if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse?.statusCode == 400 {
                BBQUserManager.configureErrorModel(data: data)
            }
            return nil
        }
    }
 
    // Common for both email, mobile field updates.
    
    private class func parseUpdateEntries(response: Data?, error: Error?)-> TokenDataModel? {
        if error == nil {
            guard let responseData = response else {
                return nil
            }
            let _ = BaseManager.parseResponseData(data: responseData, error: error)
            return BaseManager.tokenDataModel
        } else {
            if BBQUserManager.networkResponse != nil,
                BBQUserManager.networkResponse?.statusCode == 400 {
                BBQUserManager.configureErrorModel(data: response)
            }
            return nil
        }
    }
    
    private class func parseUpdateInformation(response: Data?, error: Error?)-> UpdateInfoModel? {

        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = response else {
                    
                    return nil
                }
                let locationDataModel = try decoder.decode(UpdateInfoModel.self, from: responseData)
                return locationDataModel
            } catch {
                return nil
            }
        }  else {
            if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse!.statusCode >= 300 {
                BBQUserManager.configureErrorModel(data: response, isDisplayRequired: true)
            }
            return nil
        }
    }
}
