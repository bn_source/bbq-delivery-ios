//
//  Created by Maya R on 16/10/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQCorporateBookingManager.swift
//

import Foundation
import Alamofire

class BBQCorporateBookingManager: BaseManager {
    class func verifyCorporateBooking(corporateEmail corpEmailId: String,
                                      otpID: String,
                                      completion: @escaping (_ resonse: Data?, _ error: Error?)->()){
        
        let endPoint = BBQBookingRouter.verifyCorporates(corporateEmailId: corpEmailId, otpID: otpID )
        BaseManager.requestforServiceWith(endPoint: endPoint){ (responseData, responseError) in completion(responseData, responseError)
        }
    }
    
    class func getCoporateOffers(corpEmail:String,
                                 noOfPacks:Int,
                                 reservationDate:String,
                                 branchId:String,
                                 calculatedAmount:Double,
                                 slotId:Int,
                                 completion: @escaping (_ resonse: Data?, _ error: Error?)->()){
        let endPoint = BBQBookingRouter.getCorporateOffers(corporateEmail: corpEmail, noOfPacks: noOfPacks, reservationDate: reservationDate, branchId: branchId, totalAmount: calculatedAmount, slotId: slotId)
        BaseManager.requestforServiceWith(endPoint: endPoint){ (responseData, responseError) in completion(responseData, responseError)
        }
    }
    class func verifyBookingCouponsandVouchers(barcodeValue: String,
                                               packs:Int,
                                               amount:Double,
                                               branch_ID:String,
                                               reservationDate:String,
                                               slotID: Int,
                                               completion: @escaping (_ resonse: Data?, _ error: Error?)->()){
        let endPoint = BBQBookingRouter.verifyCouponsAndVouchersForBooking(voucherBarCode: barcodeValue,packs:packs,amount:amount,branchID:branch_ID,reservationDate:reservationDate,slotID:slotID)
        BaseManager.requestforServiceWith(endPoint: endPoint){ (responseData, responseError) in completion(responseData, responseError)
        }
    }
    
    class func verifyManualCoupons(barcodeValue: String,
                                               packs:Int,
                                               amount:Double? = 0.0,
                                               branch_ID:String,
                                               reservationDate:String,
                                               completion: @escaping (_ resonse: VoucherCouponsData?, _ error: Error?)->()){
        let countryCode = SharedProfileInfo.shared.profileData?.countryCode
        let mobileNumber = SharedProfileInfo.shared.profileData?.mobileNumber ?? "0"
        let endPoint = BBQBookingRouter.verifyManualCouponForBooking(voucherBarCode: barcodeValue,
                                                                     packs:packs,
                                                                     branchID:branch_ID,
                                                                     reservationDate:reservationDate,
                                                                     phoneNumber: mobileNumber,
                                                                     countryCode: countryCode ?? "+91")
        BaseManager.requestforServiceWith(endPoint: endPoint){ (responseData, responseError) in
            
            let modelValue = BBQCorporateBookingManager.parseCouponsDetailsData(data: responseData, error: responseError)
            
            if let value = modelValue {
                DispatchQueue.main.async {
                     completion(value, responseError)
                }
            } else {
                DispatchQueue.main.async {
                     completion(nil, responseError)
                }
            }
           
        }
    }
}

extension BBQCorporateBookingManager {
    
    private class func parseCouponsDetailsData(data: Data?, error: Error?) -> VoucherCouponsData? {
        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = data else {
                    return nil
                }
                let voucherCouponsModel = try decoder.decode(VoucherCouponsData.self, from: responseData)
                return voucherCouponsModel
                
            } catch {
                print(error)
                return nil
            }
        } else {
            if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse!.statusCode >= 300 {
                BBQUserManager.configureErrorModel(data: data, isDisplayRequired: true)
            }
            return nil
        }
    }
    
    
}
