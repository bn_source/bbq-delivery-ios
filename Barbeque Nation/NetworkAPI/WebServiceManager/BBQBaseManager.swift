//
//  BBQBaseManager.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 03/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import UIKit
import Alamofire

typealias NetworkResponse = (statusCode: Int, message: String)

enum NetworkResponseMessage: String {
    case success
    case authenticationError
    case badRequest
    case outdated
    case failed
    case noData
    case unableToDecode
    
    var description: String {
        get {
            switch(self) {
            case .success:
                return kSuccessNetworkMessage
            case .authenticationError:
                return kAuthErrorNetworkMessage
            case .badRequest:
                return kBadReqNetworkMessage
            case .outdated:
                return kOutdatedErrorNetworkMessage
            case .failed:
                return kFailedNetworkMessage
            case .noData:
                return kNoDataErrorNetworkMessage
            case .unableToDecode:
                return kDecodeErrorNetworkMessage
            }
        }
    }
}

enum ConnectionType: String {
    case none
    case wifi
    case wwan
}

class BaseManager: NSObject {
    
    // MARK: - Properties
    static var stringResponse: String?
    static var bodyData: String?
    static var networkResponse: NetworkResponse?
    
    static var tokenDataModel: TokenDataModel?
    static var errorDataModel: ErrorDataModel?
    
    static var appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
    // MARK: - Connection type
    class func connectionType() -> ConnectionType {
        var type: ConnectionType = .none
        if let reachability = NetworkReachabilityManager() {
            if reachability.isReachableOnEthernetOrWiFi {
                type = .wifi
            }
            else if reachability.isReachableOnWWAN {
                type = .wwan
            }
        }        
        return type
    }
    
    class func requestforServiceWith(endPoint: URLRequestConvertible, completion: @escaping (_ resonse: Data?, _ error: Error?)->()) {
        
        BBQWebServiceHandler.request(url: endPoint) { (optionalResponse) in
            
            guard let response = optionalResponse else {
                completion(nil, nil)
                return
            }
            
            switch response.result {
            case .success:
                self.networkResponse = BaseManager.handleNetworkResponse(response.response)
                completion(response.data, nil)

            case .failure(let error):
                self.networkResponse = BaseManager.handleNetworkResponse(response.response)
                print("Request failed with error: \(error.localizedDescription)")
                if let statusCode = response.response?.statusCode {
                    switch statusCode {
                    case 401,403:
                        
//                        print("Session expired for:\(String(describing: endPoint.urlRequest?.url)) ")
//                        ToastHandler.showToastWithMessage(message: Constants.App.Splash.sessionExpired)
                        //Clear all data
                        print(endPoint)
                        BBQUserDefaults.sharedInstance.clearAppDefaults()
                        BBQLocationManager.shared.clearAllData()
                        
                        if let appDelegate = self.appDelegate {
                            appDelegate.saveNotificationData(dataArray: [])
                        }
                        
                        let currentNavigationStack = UIApplication.shared.keyWindow?.rootViewController
                        currentNavigationStack?.navigateToLoginForceFull()
                        
                        ToastHandler.showToastWithMessage(message: Constants.App.Splash.sessionExpired)
                    default:
                        print("")
                    }
                }
                completion(response.data, response.error)
            }
        }
    }
    
    class func replaceControllerInCurrentStack(new controller: UIViewController) {
        let currentNavigationStack = appDelegate?.navigationController?.viewControllers
        
        guard var modifyingStack = currentNavigationStack else {
            return
        }
        
        var controllerFoundIndex = -1
        //Iterate through current navigation stack
        for (index, item) in modifyingStack.enumerated() {
            if ((item.isKind(of: controller.classForCoder))) {
                controllerFoundIndex = index
                break
            } else {
                controllerFoundIndex = -1
            }
        }
        
        if controllerFoundIndex != -1 {
            if controllerFoundIndex != modifyingStack.count - 1 {
                //Controller is found in the stack but it is not the last in stack. Remove later items from stack.
                modifyingStack.removeSubrange((controllerFoundIndex + 1)...(modifyingStack.count - 1))
                //isOperationCompleted = true
            } else {
                //Controller is found in the stack and it is the last. Do not do anything.
                //isOperationCompleted = true
            }
        } else {
            //Controller not found in the stack. Remove last item and insert
            if let lastController = modifyingStack.last {
                if ((lastController.isKind(of: LoginController.self))) {
                    modifyingStack.append(controller)
                } else {
                    modifyingStack.removeLast()
                    modifyingStack.append(controller)
                }
            }
        }
        
        appDelegate?.navigationController?.viewControllers = modifyingStack
    }
    
    fileprivate class func handleNetworkResponse(_ response: HTTPURLResponse?) -> NetworkResponse? {
        
        guard let response = response else {
            return nil
        }
        
        var message: String = ""
        switch response.statusCode {
        case 200...299: message = NetworkResponseMessage.success.description
        case 401...499: message = NetworkResponseMessage.authenticationError.description
        case 500...599: message = NetworkResponseMessage.badRequest.description
        case 600: message = NetworkResponseMessage.outdated.description
        default: message = NetworkResponseMessage.failed.description
        }
        
        return (statusCode: response.statusCode, message: message)
    }
}

//Common Model Parsing
extension BaseManager {
    class func parseResponseData(data: Data?, error: Error?) -> Bool {
        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = data else {
                    return false
                }
                self.tokenDataModel = try decoder.decode(TokenDataModel.self, from: responseData)
                BBQUserDefaults.sharedInstance.accessToken = self.tokenDataModel?.accessToken ?? ""
                BBQUserDefaults.sharedInstance.refreshToken = self.tokenDataModel?.refreshToken ?? ""
                BBQUserDefaults.sharedInstance.expiryDuration = self.tokenDataModel?.expiryTime ?? 0
                BBQUserDefaults.sharedInstance.UserName = self.tokenDataModel?.name ?? ""
                BBQUserDefaults.sharedInstance.UserLoyaltyPoints = self.tokenDataModel?.loyaltyPoints ?? 0
                return true
                
            } catch {
                return false
            }
        } else {
            return false
        }
    }
    
    class func configureErrorModel(data: Data?, isDisplayRequired: Bool = false) {
        do {
            let decoder = JSONDecoder()
            guard let responseData = data else {
                return
            }
            self.errorDataModel = try decoder.decode(ErrorDataModel.self, from: responseData)
            //Chandan: Uncomment below line to get server errors message on Toast gloabally in application.
            //No need to handle in each class
            if let model = self.errorDataModel, let fieldError = model.fieldErrors?[0], let errorMessage = fieldError.message, isDisplayRequired {
                ToastHandler.showToastWithMessage(message: errorMessage)
            }
        } catch let error {
            print(error)
        }
    }
}
