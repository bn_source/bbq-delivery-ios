//
//  Created by Bhamidipati Kishore on 23/09/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQLoyalityPointsManager.swift
//

import UIKit

class BBQLoyalityPointsManager: BaseManager {
    
    class func getLoyalityPoints(pageNumber:Int, pageSize:Int, sort:String,completion: @escaping (_ model: BBQLoyalityPointsDataModel?, _ result: Bool?)->()) {
        let endPoint = BBQLoyalityPointsRouter.getLoyalityPoints(pageNumber: pageNumber, pageSize: pageSize, sort: sort)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            print(responseData as Any)
            let modelValue = BBQLoyalityPointsManager.parseGetLoyalityPointsData(data: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
    
    class func postConvertLoyalityToCoupons(currency_code: String, loyalty_event_type: String = "COUPON_PURCHASED", mobile_number: String, points: String, transaction_id: String = "2",completion: @escaping (_ model: LoyalityCoupon?, _ result: Bool?)->()) {
        let endPoint = BBQLoyalityPointsRouter.postConvertLoyalityToCoupons(loyalty_event_type: loyalty_event_type, mobile_number: mobile_number, points: points, transaction_id: transaction_id)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            print(responseData as Any)
            let modelValue = BBQLoyalityPointsManager.parseLoyalityCouponData(data: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
}

//MARK: Parsing
extension BBQLoyalityPointsManager {
    
    private class func parseGetLoyalityPointsData(data: Data?, error: Error?) -> BBQLoyalityPointsDataModel? {
        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = data else {
                    return nil
                }
                let loyalityModel = try decoder.decode(BBQLoyalityPointsDataModel.self, from: responseData)
                return loyalityModel
                
            } catch {
                print(error)
                return nil
            }
        } else {
            if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse!.statusCode >= 300 {
                BBQUserManager.configureErrorModel(data: data)
            }
            return nil
        }
    }
    
    private class func parseLoyalityCouponData(data: Data?, error: Error?) -> LoyalityCoupon? {
        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = data else {
                    return nil
                }
                let loyalityModel = try decoder.decode(LoyalityCoupon.self, from: responseData)
                return loyalityModel
                
            } catch {
                print(error)
                return nil
            }
        } else {
            if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse!.statusCode >= 300 {
                BBQUserManager.configureErrorModel(data: data)
            }
            return nil
        }
    }
}
