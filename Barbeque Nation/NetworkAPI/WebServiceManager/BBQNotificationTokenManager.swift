//
//  Created by Bhamidipati Kishore on 24/09/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQNotificationTokenManager.swift
//

import UIKit

class BBQNotificationTokenManager: BaseViewModel {
    
    class func deleteNotificationToken(token: String, deviceType: String, completion: @escaping (_ model: BBQNotificationTokenModel?, _ result: Bool)->()) {
        
        let endPoint = BBQNotificationDeleteTokenRouter.deleteNotificationToken(token: token, deviceType: deviceType)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQNotificationTokenManager.parseNotificationResponse(resonse: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
    
    class func deleteNotificationTokenForGuestUser(token: String, deviceType: String, completion: @escaping (_ model: BBQNotificationTokenModel?, _ result: Bool)->()) {
        
        let endPoint = BBQNotificationDeleteTokenRouter.deleteNotificationTokenForGuestUser(token: token, deviceType: deviceType)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQNotificationTokenManager.parseNotificationResponse(resonse: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
    
    class func postNotificationToken(token: String, deviceType: String, completion: @escaping (_ model: BBQNotificationTokenModel?, _ result: Bool)->()) {
        
        let endPoint = BBQNotificationTokenRouter.postNotificationToken(token: token, deviceType: deviceType)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQNotificationTokenManager.parseNotificationResponse(resonse: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
    
    class func postNotificationTokenForGuestUser(token: String, deviceType: String, completion: @escaping (_ model: BBQNotificationTokenModel?, _ result: Bool)->()) {
        
        let endPoint = BBQNotificationTokenRouter.postNotificationTokenForGuestUser(token: token, deviceType: deviceType)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQNotificationTokenManager.parseNotificationResponse(resonse: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
}

extension BBQNotificationTokenManager {
    
    class func parseNotificationResponse(resonse: Data?, error: Error?) -> BBQNotificationTokenModel? {
        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = resonse else {
                    return nil
                }
                let tokenDataModel = try decoder.decode(BBQNotificationTokenModel.self, from: responseData)
                return tokenDataModel
            } catch {
                return nil
            }
        } else {
            if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse?.statusCode == 400 {
                BBQUserManager.configureErrorModel(data: resonse)
            }
            return nil
        }
    }
}


