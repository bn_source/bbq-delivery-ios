//
 //  Created by Rabindra L on 19/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQHelpSupportManager.swift
 //

import Foundation
import Alamofire

class BBQHelpSupportManager: BaseManager {
    
    class func getFAQList(completion: @escaping (_ resonse: Data?, _ error: Error?)->()) {
        let endPoint = BBQHelpSupportRouters.helpSupport
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            completion(responseData, responseError)
        }
    }
    
    class func getAboutList(completion: @escaping (_ resonse: Data?, _ error: Error?)->()) {
        let endPoint = BBQHelpSupportRouters.about
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            completion(responseData, responseError)
        }
    }
    
    class func getFAQListById(id:[Int], completion: @escaping (_ resonse: Data?, _ error: Error?)->()) {
        let endPoint = BBQHelpSupportRouters.helpSupportById(id: id)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            completion(responseData, responseError)
        }
    }
    
    class func getWelcomeInfo( completion: @escaping (_ model: WelcomeModel?, _ result: Bool)->()) {
        
        let endPoint = BBQHelpSupportRouters.getWelcomeInfo
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQHelpSupportManager.parseWelcomeDataModel(resonse: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, false)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, true)
                }
            }
        }
    }
}

extension BBQHelpSupportManager
{
    //parsing logic for getWelcomeInfo
    private class func parseWelcomeDataModel(resonse: Data?, error: Error?) -> WelcomeModel? {
        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = resonse else {
                    return nil
                }
                let welcomeDataModel = try decoder.decode(WelcomeModel.self, from: responseData)
                return welcomeDataModel
            } catch {
                return nil
            }
        } else {
            if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse?.statusCode == 400 {
                BBQUserManager.configureErrorModel(data: resonse)
            }
            return nil
        }
    }
}
