//
//  BBQLoginManager.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 03/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import Foundation
import Alamofire

class BBQLoginManager: BaseManager {
    
    class func verifyUser(countryCode: String, mobileNumber: Int64, otpGenerate: Bool,
                          completion: @escaping (_ model: VerifyUserDataModel?, _ result: Bool)->()) {
        
        let endPoint = BBQLoginRouter.verifyUser(countryCode: countryCode, mobileNumber: mobileNumber, generateOTP: otpGenerate)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQLoginManager.parseVerifyUserResponse(resonse: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }

    class func generateOTP(countryCode: String, mobileNumber: Int64, otpId: String, completion: @escaping (_ model: GenerateOTPDataModel?, _ result: Bool)->()) {
        
        let endPoint = BBQLoginRouter.generateOTP(countryCode: countryCode, mobileNumber: mobileNumber, otpId: otpId)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQLoginManager.parseGenerateOTPResponse(resonse: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
    
    class func verifyOTP(otp: Int, otpId: String, completion: @escaping (_ model: VerifyOTPDataModel?, _ result: Bool)->()) {
        
        let endPoint = BBQLoginRouter.verifyOTP(otp: otp, otpId: otpId)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQLoginManager.parseVerifyOTPResponse(resonse: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
    
    class func verifyReferralCode(referralcode: String, completion: @escaping (_ model: veriyReferralCodeModel?, _ result: Bool)->()) {
        
        let endPoint = BBQLoginRouter.verifyReferralCode(referralCode: referralcode)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQLoginManager.parseVerifyReferralCodeResponse(resonse: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
    
    class func login(countryCode: String, mobileNumber: Int64, otpId: String, otp: Int, completion: @escaping (_ model: TokenDataModel?, _ result: Bool)->()) {
        
        let endPoint = BBQLoginRouter.login(countryCode: countryCode, mobileNumber: mobileNumber, otpId: otpId, otp: otp)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            if responseError == nil { BBQUserDefaults.sharedInstance.customerId = String(mobileNumber) }
            let result = self.parseResponseData(data: responseData, error: responseError)
            DispatchQueue.main.async {
                result == true ? completion(BaseManager.tokenDataModel, true): completion(nil, true)
            }
        }
    }
    
    class func socialLogin(token: String,
                           provider: String,
                           countryCode: String,
                           mobileNumber: Int64, otpId: String,
                           otp: Int,
                           title: String,
                           name: String,
                           completion: @escaping (_ model: TokenDataModel?,
        _ isNewUser: Bool,
        _ isLoginFailed: Bool)->()) {
     
        
        let endPoint = BBQLoginRouter.social(token: token, provider: provider, countryCode: countryCode, mobileNumber: mobileNumber, otpId: otpId, otp: otp, title: title, name: name)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let result = BBQLoginManager.parseSocialLogin(resonse: responseData, error: responseError)
            DispatchQueue.main.async {
                if result?.isNewUser == true {
                    completion(nil, true, true)
                } else {
                    if responseError == nil { BBQUserDefaults.sharedInstance.customerId = String(mobileNumber) }
                    let tokenModelResult = BaseManager.parseResponseData(data: responseData, error: responseError)
                    tokenModelResult == true ? completion(BaseManager.tokenDataModel, false, false) :
                                               completion(nil, false, true)
                }
            }
        }
    }
    
    class func refreshToken(completion: @escaping (_ model: TokenDataModel?, _ result: Bool)->()) {
        
        let endPoint = BBQLoginRouter.refreshToken
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let result = self.parseResponseData(data: responseData, error: responseError)
            DispatchQueue.main.async {
                result == true ? completion(BaseManager.tokenDataModel, true): completion(nil, false)
            }
        }
    }
    
    class func registerUser(countryCode: String, mobileNumber: Int64, email: String, title: String, name: String, dob: String, maritalStatus: Int, newsletter: String, anniversary: String, otpId: String, otp: Int,referralCode: String, completion: @escaping (_ model: TokenDataModel?, _ result: Bool)->()) {
        
        let endPoint = BBQLoginRouter.register(countryCode: countryCode, mobileNumber: mobileNumber, email: email, title: title, name: name, dob: dob, maritalStatus: maritalStatus, newsletter: newsletter, anniversary: anniversary, otpId: otpId, otp: otp, referralCode: referralCode)
        
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            if responseError == nil { BBQUserDefaults.sharedInstance.customerId = String(mobileNumber) }
            let result = self.parseResponseData(data: responseData, error: responseError)
            DispatchQueue.main.async {
                result == true ? completion(BaseManager.tokenDataModel, true): completion(nil, true)
            }
        }
    }
    
    class func logoutUser( completion: @escaping (_ model: LogoutModel?, _ result: Bool?)->()) {
        let endPoint = BBQLoginRouter.logout
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = self.parseLogoutResponseData(data: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
}

//Parsing Logic
extension BBQLoginManager {
    private class func parseVerifyUserResponse(resonse: Data?, error: Error?) -> VerifyUserDataModel? {
        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = resonse else {
                    return nil
                }
                let verifyUserDataModel = try decoder.decode(VerifyUserDataModel.self, from: responseData)
                return verifyUserDataModel
            } catch {
                return nil
            }
        } else {
            if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse?.statusCode == 400 {
                BBQUserManager.configureErrorModel(data: resonse, isDisplayRequired: true)
            }
            return nil
        }
    }
    
    class func parseGenerateOTPResponse(resonse: Data?, error: Error?) -> GenerateOTPDataModel? {
        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = resonse else {
                    return nil
                }
                let generateOtpDataModel = try decoder.decode(GenerateOTPDataModel.self, from: responseData)
                return generateOtpDataModel
            } catch {
                return nil
            }
        } else {
            if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse?.statusCode == 400 {
                BBQUserManager.configureErrorModel(data: resonse, isDisplayRequired: true)
            }
            return nil
        }
    }
    
    private class func parseVerifyOTPResponse(resonse: Data?, error: Error?) -> VerifyOTPDataModel? {
        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = resonse else {
                    return nil
                }
                let verifyOtpDataModel = try decoder.decode(VerifyOTPDataModel.self, from: responseData)
                return verifyOtpDataModel
            } catch {
                return nil
            }
        } else {
            if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse?.statusCode == 400 {
                BBQUserManager.configureErrorModel(data: resonse)
            }
            return nil
        }
    }
    
    private class func parseVerifyReferralCodeResponse(resonse: Data?, error: Error?) -> veriyReferralCodeModel? {
        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = resonse else {
                    return nil
                }
                let verifyReferralModel = try decoder.decode(veriyReferralCodeModel.self, from: responseData)
                return verifyReferralModel
            } catch {
                return nil
            }
        } else {
            if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse?.statusCode == 400 {
                BBQUserManager.configureErrorModel(data: resonse)
            }
            return nil
        }
    }
    private class func parseSocialLogin(resonse: Data?, error: Error?) -> SocialLoginUserModel? {
        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = resonse else {
                    return nil
                }
                let newUserDataModel = try decoder.decode(SocialLoginUserModel.self, from: responseData)
                return newUserDataModel
            } catch {
                return nil
            }
        } else {
            if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse?.statusCode == 400 {
                BBQUserManager.configureErrorModel(data: resonse)
            }
            return nil
        }
    }
    
    private class func parseLogoutResponseData(data: Data?, error: Error?) -> LogoutModel? {
        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = data else {
                    return nil
                }
                let logoutDataModel = try decoder.decode(LogoutModel.self, from: responseData)
                return logoutDataModel
                
            } catch {
                return nil
            }
        } else {
            return nil
        }
    }
}

