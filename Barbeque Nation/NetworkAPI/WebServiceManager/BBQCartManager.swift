//
 //  Created by Chandan Singh on 08/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQCartManager.swift
 //

import Foundation
import Alamofire

class BBQCartManager: BaseManager {
    
    class func getCart(completion: @escaping (_ model: ModifyCartModel?, _ result: Bool)->()) {
        
        // Blocking API call for guest user
        if BBQUserDefaults.sharedInstance.isGuestUser {
            completion(nil, false)
        }
        
        let endPoint = BBQCartRouter.getCart
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQCartManager.parseGetAndPacthCartData(data: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
    
    class func addToCart(productId: Int, quantity: Int, completion: @escaping (_ model: ModifyCartModel?, _ result: Bool)->()) {
        
        let endPoint = BBQCartRouter.addCart(productId: productId, quantity: quantity)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQCartManager.parseAddCartData(data: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
    
    class func updateOrderItem(orderId: String, orderItem: String, quantity: Int, completion: @escaping (_ model: ModifyCartModel?, _ result: Bool)->()) {
        
        let endPoint = BBQCartRouter.updateOrderItem(orderId: orderId, orderItemId: orderItem, quantity: quantity)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQCartManager.parseGetAndPacthCartData(data: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
    
    class func deleteOrderItem(orderId: String, orderItemId: String, completion: @escaping (_ model: ModifyCartModel?, _ result: Bool)->()) {
        
        let endPoint = BBQCartRouter.deleteOrderItem(orderId: orderId, orderItemId: orderItemId)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQCartManager.parseGetAndPacthCartData(data: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
    
    class func emptyCartFor(cartID: String, completion: @escaping (Bool)->()) {
        let endPoint = BBQCartRouter.emptyCart(cartID: cartID)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            if responseError == nil {
                do {
                    DispatchQueue.main.async {
                        completion(true)
                    }
                }
            } else {
                if BBQUserManager.networkResponse != nil,
                    BBQUserManager.networkResponse?.statusCode == 400 {
                    BBQUserManager.configureErrorModel(data: responseData, isDisplayRequired: true)
                }
                DispatchQueue.main.async {
                    completion(false)
                }
            }
        }
    }
    
    class func getLoyaltyPointsValue(amount: String, currency: String, completion: @escaping (_ model: BBQPointsValueModel?, _ result: Bool)->()) {
        
        //Amount=0.0 &currency = INR" is sending request with amount = 0
        //if the amount is being sent 0 in the parameter then it throws an exception "Method argument not valid"
        //Handle that
        
        let amountIntValue = Int(amount) ?? 0
        
        if amountIntValue == 0 {
            //do not hit api, just return
            completion(nil, false)

        }

        let endPoint = BBQCartRouter.loyaltyPointsValue(amount: amount, currency: currency)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQCartManager.parsePointsValueData(data: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
    
    class func getLoyaltyPointsCount(completion: @escaping (_ model: BBQPointsCountModel?, _ result: Bool)->()) {
        
        // Blocking API call for guest user
        if BBQUserDefaults.sharedInstance.isGuestUser {
            completion(nil, false)
        }
        
        let endPoint = BBQCartRouter.loyaltyPointsCount
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQCartManager.parsePointsCountData(data: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
}

//MARK: Parsing
extension BBQCartManager {
    
    private class func parseAddCartData(data: Data?, error: Error?) -> ModifyCartModel? {
        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = data else {
                    return nil
                }
                let addCartModel = try decoder.decode(ModifyCartModel.self, from: responseData)
                return addCartModel
                
            } catch {
                print(error)
                return nil
            }
        } else {
            if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse!.statusCode >= 300 {
                BBQUserManager.configureErrorModel(data: data)
            }
            return nil
        }
    }
    
    private class func parseGetAndPacthCartData(data: Data?, error: Error?) -> ModifyCartModel? {
        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = data else {
                    return nil
                }
                let getAndPatchCartModel = try decoder.decode(ModifyCartModel.self, from: responseData)
                return getAndPatchCartModel
                
            } catch {
                print(error)
                return nil
            }
        } else {
            if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse!.statusCode >= 300 {
                BBQUserManager.configureErrorModel(data: data)
            }
            return nil
        }
    }
    
    private class func parsePointsValueData(data: Data?, error: Error?) -> BBQPointsValueModel? {
        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = data else {
                    return nil
                }
                let pointsValueModel = try decoder.decode(BBQPointsValueModel.self, from: responseData)
                return pointsValueModel
                
            } catch {
                print(error)
                return nil
            }
        } else {
            if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse!.statusCode >= 300 {
                BBQUserManager.configureErrorModel(data: data)
            }
            return nil
        }
    }
    
    class func parsePointsCountData(data: Data?, error: Error?) -> BBQPointsCountModel? {
        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = data else {
                    return nil
                }
                let pointsCountModel = try decoder.decode(BBQPointsCountModel.self, from: responseData)
                return pointsCountModel
                
            } catch {
                print(error)
                return nil
            }
        } else {
            if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse!.statusCode >= 300 {
                BBQUserManager.configureErrorModel(data: data)
            }
            return nil
        }
    }
}
