//
 //  Created by Maya R on 18/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQVouchersCouponsManager.swift
 //

import Foundation
import Alamofire

class BBQVouchersCouponsManager: BaseManager {
    
    class func getTheVouchersForBooking(completion: @escaping (_ model: VoucherCouponsModel?, _ result: Bool)->()) {
        
        let endPoint = BBQBookingRouter.getAllTheBookingVouchers(transactionType: "BOOKING", voucherType: "GV")
        BaseManager.requestforServiceWith(endPoint: endPoint as URLRequestConvertible) { (responseData, responseError) in
            
            let modelValue = BBQVouchersCouponsManager.parseCouponsDetailsData(data: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
            
        }
    }
    
    class func getTheCouponsForBooking(noOfPack:Int,amount:Double,branchID:String,reservationDate:String,slotID:Int,currency:String,completion: @escaping (_ model: VoucherCouponsModel?, _ result: Bool)->()) {
        
        let endPoint = BBQBookingRouter.getAllTheBookingCoupons(transactionType: "BOOKING", voucherType: "GC", noOfPacks: noOfPack, totalAmount: amount, selectedBranchID: branchID, reservationDate: reservationDate, slotID: slotID, currency: currency)
        BaseManager.requestforServiceWith(endPoint: endPoint as URLRequestConvertible) { (responseData, responseError) in
            
            let modelValue = BBQVouchersCouponsManager.parseCouponsDetailsData(data: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
            
        }
    }
    
    class func getCouponAndVoucherListWithCount( completion:  @escaping (_ model: VoucherAndCouponListWithCountModel?, _ result: Bool)->()) {
        
        
        let endPoint = BBQHomeRouter.getVoucherAndCouponListWithCountDetails(branch_id: Int(BBQUserDefaults.sharedInstance.DeliveryBranchIdValue) ?? 0, customer_id: BBQUserDefaults.sharedInstance.customerId)
        BaseManager.requestforServiceWith(endPoint: endPoint) { (responseData, responseError) in
            let modelValue = BBQVouchersCouponsManager.parseVoucherAndCouponListWithCount(data: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
        }
    }
    
    class func getPurchasedCouponsInCart(completion: @escaping (_ model: VoucherCouponsModel?, _ result: Bool)->()) {
        
        let endPoint = BBQBookingRouter.getAllThePurchaseCoupons(transactionType: "PURCHASE", voucherType: "GC")
        BaseManager.requestforServiceWith(endPoint: endPoint as URLRequestConvertible) { (responseData, responseError) in
            
            let modelValue = BBQVouchersCouponsManager.parseCouponsDetailsData(data: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }
            
        }
    }
    class func verifyPurchasedCouponsandVouchers(barcodeValue: String,
                                               mobileNumber:String,
                                               totalAmount:Double,
                                               countryCode:String,
                                               completion: @escaping (_ model: VoucherCouponsData?, _ result: Bool?)->()){
        let endPoint = BBQBookingRouter.verifyPurchasedVouchersandCoupons(voucherBarCode: barcodeValue, mobileNumber: mobileNumber, calculatedAmount: totalAmount, countryCode: countryCode)
        BaseManager.requestforServiceWith(endPoint: endPoint){ (responseData, responseError) in
            let modelValue = BBQVouchersCouponsManager.parsePurchasedCouponResponse(resonse: responseData, error: responseError)
            if let value = modelValue {
                DispatchQueue.main.async {
                    completion(value, true)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, false)
                }
            }            
        }
    }


}

extension BBQVouchersCouponsManager {
    private class func parseCouponsDetailsData(data: Data?, error: Error?) -> VoucherCouponsModel? {
        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = data else {
                    return nil
                }
                let voucherCouponsModel = try decoder.decode(VoucherCouponsModel.self, from: responseData)
                return voucherCouponsModel
                
            } catch {
                print(error)
                return nil
            }
        } else {
            if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse!.statusCode >= 300 {
                BBQUserManager.configureErrorModel(data: data)
            }
            return nil
        }
    }
    
    private class func parsePurchasedCouponResponse(resonse: Data?, error: Error?) -> VoucherCouponsData? {
        if error == nil {
            do {
                let decoder = JSONDecoder()
                guard let responseData = resonse else {
                    return nil
                }
                let verifyCouponModel = try decoder.decode(VoucherCouponsData.self, from: responseData)
                return verifyCouponModel
            } catch {
                return nil
            }
        } else {
            if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse?.statusCode == 400 {
                BBQUserManager.configureErrorModel(data: resonse, isDisplayRequired: true)
            }
            return nil
        }
    }
    
    class func parseVoucherAndCouponListWithCount(data: Data?, error: Error?) -> VoucherAndCouponListWithCountModel? {
       if error == nil {
           do {
               let decoder = JSONDecoder()
               guard let responseData = data else {
                   return nil
               }
               let voucherDetailsDataModel = try decoder.decode(VoucherAndCouponListWithCountModel.self, from: responseData)
              print(voucherDetailsDataModel)
               return voucherDetailsDataModel
               
           } catch {
               print(error)
               return nil
           }
       } else {
           if BBQUserManager.networkResponse != nil, BBQUserManager.networkResponse!.statusCode >= 300 {
               BBQUserManager.configureErrorModel(data: data)
           }
           return nil
       }
   }
}
