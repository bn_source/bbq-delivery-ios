//
//  BBQUserRouter.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 04/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import Foundation
import Alamofire

enum BBQUserRouter: Router {

    case getUpdateInfo(currentVersion: String, deviceType: String)
    case getUser
    case updateUser(title: String, newsLetter: String, maritalStatus: String, name: String,
        dob: String, anniversary: String, isNotificationEnabled: Bool)
    case deleteUser(otp: Int, otpId: Int)
    case updateUserMobile(countryCode: String, mobileNumber: Int64, otpId: Int, otp: Int)
    case generateEmailOTP(email: String)
    case updateUserEmail(email: String, otpID: Int, otp: Int)
    case updateUserProfileImage
    case instagramUser(accessToken: String)
    case updateRazorpayID(razorpayID:String)
    
    var method: HTTPMethod {
        switch self {
        case .getUser, .instagramUser:
            return .get
            
        case .updateUser, .updateUserProfileImage,.updateRazorpayID:
            return .put
            
        case .deleteUser:
            return .delete
            
        case .updateUserMobile, .updateUserEmail, .generateEmailOTP, .getUpdateInfo:
            return .post
        }
    }
    
    var path: String {
        switch self {
            
        case .getUpdateInfo:
            return ServicePath.appUpdate.rawValue
            
        case .getUser, .updateUser, .deleteUser:
            return ServicePath.user.rawValue
            
        case .instagramUser:
            return ServicePath.instagramUserInfo.rawValue
            
        case .generateEmailOTP:
            return ServicePath.generateOTP.rawValue
            
        case .updateUserMobile:
            return ServicePath.changeMobile.rawValue
            
        case .updateUserEmail:
            return ServicePath.changeEmail.rawValue
            
        case .updateUserProfileImage:
            return ServicePath.changeProfilePicture.rawValue
        case .updateRazorpayID:
            return ServicePath.updateRazorPayID.rawValue
        }
    }
    
    var queryItems: [URLQueryItem]? {
        switch self {
        case .instagramUser(let accessToken):
            let queryItem = URLQueryItem(name: "access_token", value: accessToken)
            return [queryItem]
    
        default: return nil
            
        }
    }
    
    var bodyParams: Any? {
        switch self {
            
        case .getUpdateInfo(let currentVersion, let deviceType):
            return ["app_version": currentVersion,
                    "type": deviceType]
            
        case .deleteUser(let opt, let otpId):
            return ["otp_id": otpId,
                    "otp": opt]
            
        case .updateUserMobile(let countryCode, let mobileNumber, let otpId, let otp):
            return ["country_code": countryCode,
                    "mobile_number": mobileNumber,
                    "otp_id": otpId,
                    "otp": otp]
            
        case .generateEmailOTP(let email):
            return ["email": email,
                    "country_code": "",
                    "mobile_number": ""]
            
        case .updateUserEmail(let email, let otpID, let otp):
            return ["email": email,
                    "otp_id": otpID,
                    "otp": otp]
            
        case .updateUser(let title, let newsLetter, let maritalStatus, let name, let dob, let anniversary, let notifChoice):
            return ["title": title,
                    "name": name,
                    "date_of_birth": dob,
                    "marital_status": maritalStatus,
                    "anniversary_date": anniversary,
                    "newsletter": newsLetter,
                    "notification_enabled": notifChoice]
            
        case .updateRazorpayID(razorpayID: let razorPayID):
            return ["customer_id":BBQUserDefaults.sharedInstance.customerId,
                    "razorpay_id":razorPayID]
            
        default:
            return nil
        }
    }
    
    var headers: HTTPHeaders? {
        switch self {
        case .updateUser, .deleteUser, .updateRazorpayID:
            return ["Authorization": EnviornmentConfiguration().environment.authorization,
                    "Content-Type": "application/json"]
            
        case .getUser:
            return ["Authorization": EnviornmentConfiguration().environment.authorization,
                    "Accept": "application/json",
                    "Content-Type": "application/json"]
            
        case .generateEmailOTP, .getUpdateInfo:
            return ["Content-Type": "application/json",
                    "BBQ-Client-Id": EnviornmentConfiguration().environment.clientID,
                    "BBQ-Client-Secret": EnviornmentConfiguration().environment.clientSecret]
            
        case .updateUserMobile, .updateUserEmail:
            return ["Authorization":
                EnviornmentConfiguration().environment.authorization,
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                    "BBQ-Client-Id": EnviornmentConfiguration().environment.clientID,
                    "BBQ-Client-Secret": EnviornmentConfiguration().environment.clientSecret]
            
        case .updateUserProfileImage:
            return ["Authorization": EnviornmentConfiguration().environment.authorization,
                    "Accept": "application/json",
                    "Content-Type": "application/x-www-form-urlencoded",
                    "BBQ-Client-Id": EnviornmentConfiguration().environment.clientID,
                    "BBQ-Client-Secret": EnviornmentConfiguration().environment.clientSecret]

        case .instagramUser:
            return nil
        }
    }
}
