//
 //  Created by Bhamidipati Kishore on 14/11/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQMobileTermsAndConditionRouter.swift
 //

import Foundation
import Alamofire

enum BBQMobileTermsAndConditionRouter: Router {
    
    case getMobileTermsAndConditions
    
    var method: HTTPMethod {
        switch self {
        case .getMobileTermsAndConditions:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .getMobileTermsAndConditions:
            return ServicePath.mobileTermsAndCondition.rawValue
        }
    }
    
    var headers: HTTPHeaders? {
        switch self {
        case .getMobileTermsAndConditions:
            return ["Authorization": EnviornmentConfiguration().environment.authorization]
        }
    }
    
    var bodyParams: Any? {
        return nil
    }
}
