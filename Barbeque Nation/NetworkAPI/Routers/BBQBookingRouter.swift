//
//  BBQBookingRouter.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 03/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import Foundation
import Alamofire

enum BBQBookingRouter: Router {
    //Booking Flow
    case getSlotWithBranch(reservationDate: String, branchId: String, dinnerType: String)
    case getSlots(reservationDate: String, branchId: String, lat: Double, long: Double, dinnerType: String)
    case getmenubranchBuffet(branchId: String, reservationDate: String, reservationTime: String, slotId: Int, isEarlyBird : Bool, isLateBird : Bool)
    case getmenubranchforreservation(branchId: String, reservationDate: String, reservationTime: String, slotId: Int, isEarlyBird : Bool, isLateBird : Bool)
    case createBooking(createBookingJsonData: Json)
    case updateBooking(bookingId: String, paymentOrderId:String, paymentId: String, paymentGatewayName: String, paymentSignature: String)
    case rescheduleBooking(rescheduleBookingJsonData: Json)
    case rescheduleResetAll(bookingId: String)
    
    case getBookingHistory(status: Int, pageNo: String)
    case getSpecialInstructions
    case updateSpecialInstructions(bookingId: String, occassionIds: [String], joinedBy: [String], specialNote: String)
    case checkReservationRules(createBookingJsonData: Json)
    case updateReservationPayment(params: [String: Any])
    
    //Corporate booking
    case verifyCorporates(corporateEmailId:String, otpID: String)
    case getCorporateOffers(corporateEmail:String, noOfPacks:Int, reservationDate:String, branchId:String, totalAmount:Double, slotId:Int)
    
    //Verify corporate coupons and user coupons
    case verifyCouponsAndVouchersForBooking(voucherBarCode: String, packs:Int, amount:Double, branchID:String, reservationDate:String, slotID:Int)
    
    case verifyManualCouponForBooking(voucherBarCode: String,
        packs:Int,
        branchID:String,
        reservationDate:String,
        phoneNumber: String,
        countryCode: String)
    
    //Verify verifyPurchasedouchers
    case verifyPurchasedVouchersandCoupons(voucherBarCode: String,mobileNumber:String,calculatedAmount:Double,countryCode:String)

    //Get the coupons for booking
    case getAllTheBookingCoupons(transactionType:String,voucherType:String,noOfPacks:Int,totalAmount:Double,selectedBranchID:String,reservationDate:String,slotID:Int,currency:String)

    //Get the copoons for booking
    case getAllTheBookingVouchers(transactionType:String,voucherType:String)
    
    //Get the copoons for purchase
    case getAllThePurchaseCoupons(transactionType:String,voucherType:String)


    case getVoucherHistory(pageNo: String)
    
    // Cancel Booking
    case cancelReasons
    case refundOptions(bookingID: String)
    case cancelBooking(bookingId: String, reasonID:Int, refundType:String)
    
    // After Dining
    case afterDiningPaymentData(bookingID: String)
    case bookingPaymentData(bookingID: String)
    case updateFinalPayment(updateFinalPayData: Json)
    
    //Voucher/Coupons
    case getVoucherAndCoupon
    case verifyVoucher(voucherBarCode: String,voucherCovers:Int,amount:Double)
    case cartCheckout(points: Int, barCode: String, razorpayPaymentId: String, razorpayOrderId: String, razorpaySignature: String)
    case giftVoucher(barCode: String, countryCode: String, mobileNumber: String, email: String, name: String)
    
    var method: HTTPMethod {
        switch self {
        case .getBookingHistory, .getSpecialInstructions, .getVoucherHistory, .cancelReasons,
             .refundOptions, .afterDiningPaymentData, .bookingPaymentData, .getAllTheBookingCoupons, .getAllTheBookingVouchers, .rescheduleResetAll,.getAllThePurchaseCoupons:
            return .get
            
        case .getSlots, .getSlotWithBranch, .createBooking, .getmenubranchBuffet,.getmenubranchforreservation,.verifyCorporates,
                .verifyVoucher, .cartCheckout, .getVoucherAndCoupon, .getCorporateOffers, .verifyCouponsAndVouchersForBooking, .verifyPurchasedVouchersandCoupons, .giftVoucher, .verifyManualCouponForBooking, .checkReservationRules, .updateReservationPayment:
            return .post
            
        case .updateSpecialInstructions, .cancelBooking, .rescheduleBooking, .updateBooking:
            return .put
        case .updateFinalPayment:
            return .put
        }
    }
    
    var path: String {
        switch self {
        case .checkReservationRules:
            return ServicePath.reservation_rules.rawValue
        case .updateReservationPayment:
            return ServicePath.update_adv_res_payment.rawValue
        case .updateBooking, .createBooking:
            return ServicePath.bookings.rawValue
        
        case .updateFinalPayment:
            return ServicePath.bookings.rawValue
        
        case .getBookingHistory:
            return ServicePath.bookings.rawValue
            
        case .getSlots, .getSlotWithBranch:
            return ServicePath.getSlots.rawValue
            
        case .updateSpecialInstructions:
            return ServicePath.updateSpecialInstructions.rawValue
            
        case .getmenubranchBuffet:
            return ServicePath.menubranchbuffet.rawValue
            
        case .getmenubranchforreservation:
            return ServicePath.menubranchbuffetNew.rawValue
            
        case .cancelBooking:
            return ServicePath.cancelBooking.rawValue
            
        case .verifyCorporates:
            return ServicePath.verifyCorporates.rawValue
            
        case .getSpecialInstructions:
            return ServicePath.getSpecialInstructions.rawValue
            
        case .getVoucherHistory:
            return ServicePath.getVouchorHistory.rawValue
            
        case .getVoucherAndCoupon:
            return ServicePath.voucherAndCoupons.rawValue
            
        case .verifyVoucher:
            return ServicePath.verifyVoucher.rawValue
            
        case .cartCheckout:
            return ServicePath.checkoutPayment.rawValue
            
        case .rescheduleBooking:
            return ServicePath.rescheduleBooking.rawValue
            
        case .cancelReasons:
            return ServicePath.cancelReasons.rawValue
            
        case .refundOptions(let bookingID):
            return ServicePath.refundOptions.rawValue + "/\(bookingID)"
            
        case .afterDiningPaymentData(let bookingID):
            return ServicePath.bookings.rawValue + "/\(bookingID)"
            
        case .bookingPaymentData(let bookingID):
            return ServicePath.bookings.rawValue + "/\(bookingID)"
            
        case .getCorporateOffers:
            return ServicePath.corporateOffers.rawValue
            
        case .verifyCouponsAndVouchersForBooking:
            return ServicePath.verifyBookingCouponsandVouchers.rawValue
            
        case .verifyPurchasedVouchersandCoupons:
            return ServicePath.verifyPurchasedCouponsandVouchers.rawValue
            
        case .getAllTheBookingCoupons:
            return ServicePath.getTheCoupons.rawValue
            
        case .getAllTheBookingVouchers:
            return ServicePath.getTheCoupons.rawValue
            
        case .rescheduleResetAll(let bookingID):
            return ServicePath.rescheduleResetAll.rawValue + "/\(bookingID)"
            
        case .getAllThePurchaseCoupons:
            return ServicePath.getTheCoupons.rawValue

        case .giftVoucher:
            return ServicePath.giftVoucher.rawValue
            
        case .verifyManualCouponForBooking:
            return ServicePath.verifyManualCouponCode.rawValue
        }
    }
    
    var queryItems: [URLQueryItem]? {
        switch self {
            
        case .getBookingHistory(let status, let pageNo):
            let status = URLQueryItem(name: "status", value: String(status))
            let queryItem = URLQueryItem(name: "page", value: pageNo)
            return [status, queryItem]
            
        case .getVoucherHistory(let pageNo):
            let queryItem = URLQueryItem(name: "page", value: pageNo)
            return [queryItem]
            
        case .refundOptions(let bookingID):
            let queryItem = URLQueryItem(name: "booking_id", value: bookingID)
            return [queryItem]
            
        default:
            return nil
            
        }
    }
    
    var params: Json? {
        switch self {
        case .getAllTheBookingCoupons (let transactionType, let voucherType, let noOfPacks, let totalAmount, let selectedBranchID, let reservationDate,let slotID ,let currency):
            return ["transaction_type": transactionType,
                    "voucher_type": voucherType,
                    "packs": noOfPacks,
                    "amount": totalAmount,
                    "branch_id": selectedBranchID,
                    "reservation_date": reservationDate,
                    "slot_id": slotID,
                    "currency": currency]

        case .getAllTheBookingVouchers(let transactionType,let voucherType):
            return ["transaction_type": transactionType,
                    "voucher_type": voucherType]
            
        case .getAllThePurchaseCoupons(let transactionType,let voucherType):
            return ["transaction_type": transactionType,
                    "voucher_type": voucherType]

    
        default:
            return nil

        }
    }
    
    var bodyParams: Any? {
        switch self {
            
        case .getSlots(let reservationDate, let branchId, let lat, let long, let dinnerType):
            return ["reservation_date": reservationDate,
                    "branch_id": branchId,
                    "lat": lat,
                    "long": long,
                    "dinner_type": dinnerType]
            
        case .getSlotWithBranch(let reservationDate, let branchId, let dinnerType):
            return ["reservation_date": reservationDate,
                    "branch_id": branchId,
                    "dinner_type": dinnerType]
            
        case .updateBooking(let bookingId, let paymentOrderId, let paymentId, let paymentGatewayName, let paymentSignature):
            return ["booking_id": bookingId,
                    "payment_order_id": paymentOrderId,
                    "payment_id": paymentId,
                    "payment_gateway": paymentGatewayName,
                    "payment_signature": paymentSignature]
            
        case .updateSpecialInstructions(let bookingId, let occassionIds, let joinedBy, let specialNote):
            return ["booking_id": bookingId,
                    "occasion_id": occassionIds,
                    "joined_by": joinedBy,
                    "special_note": specialNote]
            
        case .createBooking(let createBookingJsonData), .checkReservationRules(let createBookingJsonData):
            return createBookingJsonData
        
        case .updateReservationPayment(let params):
            return params
            
        case .updateFinalPayment(let updatePayJson):
            return updatePayJson
            
        case .getmenubranchBuffet(let branchId, let reservationDate, let reservationTime, let slotId, let isEarlyBird, let isLatyeBird):
            
            let finalSlotId = slotId == 0 ? "" : String(format: "%d", slotId)
            return ["branch_id": branchId,
                    "reservation_date": reservationDate,
                    "reservation_time": reservationTime,
                    "slot_id": finalSlotId,
                    "early_bird": isEarlyBird,
                    "late_bird": isLatyeBird]
            
        case .getmenubranchforreservation(let branchId, let reservationDate, let reservationTime, let slotId, let isEarlyBird, let isLatyeBird):
            
            let finalSlotId = slotId == 0 ? "" : String(format: "%d", slotId)
            return ["branch_id": branchId,
                    "reservation_date": reservationDate,
                    "reservation_time": reservationTime,
                    "slot_id": finalSlotId,
                    "early_bird": isEarlyBird,
                    "late_bird": isLatyeBird]
            
        case .cancelBooking(let bookingId, let reason, let refundType):
            return ["booking_id": bookingId,
                    "reason_id": reason,
                    "refund_type": refundType]
            
        case .verifyCorporates(let corporateEmailId, let otpID):
            return ["corporate_email": corporateEmailId,
                    "otp_id": otpID]
            
        case .rescheduleBooking(let rescheduleBookingJsonData):
            return rescheduleBookingJsonData
            
        case .cartCheckout(let points, let barCode, let razorpayPaymentId, let razorpayOrderId, let razorpaySignature):
            return ["loyalty_points": points,
                    "bar_code": barCode,
                    "razorpay_payment_id": razorpayPaymentId,
                    "razorpay_order_id": razorpayOrderId,
                    "razorpay_signature": razorpaySignature]
            
        case .getVoucherAndCoupon:
            return ["mobile_number": "",
                    "country_code": ""]
            
        case .verifyVoucher(let voucherBarCode, let voucherCovers,let amount):
            return ["bar_code": voucherBarCode,
                    "covers": voucherCovers,
                    "amount": amount]

            
        case .getCorporateOffers(let corporateEmail, let noOfPacks,let reservationDate,let branchId,let totalAmount, let slotId):
            return["email":corporateEmail,
                   "packs":noOfPacks,
                   "reservation_date":reservationDate,
                   "branch_id":branchId,
                   "amount":totalAmount,
                   "slot":slotId
                   ]
            

        case .verifyCouponsAndVouchersForBooking(let voucherBarCode,let packs,let amount,let branch_id,let reservationDate, let slotID):
                return["bar_code":voucherBarCode,
                    "packs":packs,
                    "amount":amount,
                    "branch_id":branch_id,
                    "reservation_date":reservationDate,
                    "slot":slotID]
            
        case .verifyManualCouponForBooking(let voucherBarCode,let packs,let branch_id,
                                           let reservationDate, let phoneNumber, let countryCode):
            return["bar_code":voucherBarCode,
                   "mobile_number":phoneNumber,
                   "country_code":countryCode,
                   "packs":packs,
                   "amount": 0,
                   "currency": "",
                   "branch_id":branch_id,
                   "reservation_date":reservationDate,
                   "slot_id":""]
            
        case .verifyPurchasedVouchersandCoupons(let voucherBarCode,let mobileNumber, let calculatedAmount,let countryCode):
            return["bar_code":voucherBarCode,
                   "mobile_number":mobileNumber,
                   "amount":calculatedAmount,
                   "country_code":countryCode]
            
        case .giftVoucher(let barCode, let countryCode, let mobileNumber, let email, let name):
            return["bar_code": barCode,
                   "country_code": countryCode,
                   "mobile_number": mobileNumber,
                   "email": email,
                   "name": name]
            
        default:
            return nil
        }
    }
    
    var headers: HTTPHeaders? {
        switch self {
        case .getSlots, .getSlotWithBranch, .getmenubranchBuffet, .getmenubranchforreservation, .verifyCorporates:
            if BBQUserDefaults.sharedInstance.isGuestUser {
                return ["Content-Type": "application/json",
                        "BBQ-Client-Id": EnviornmentConfiguration().environment.clientID,
                        "BBQ-Client-Secret": EnviornmentConfiguration().environment.clientSecret]
            } else {
                return ["Authorization": EnviornmentConfiguration().environment.authorization,
                        "Content-Type": "application/json"]
            }
            
        case .getBookingHistory, .updateBooking, .updateSpecialInstructions, .createBooking,
             .cancelBooking, .getSpecialInstructions, .rescheduleBooking, .cancelReasons,
             .refundOptions, .afterDiningPaymentData, .bookingPaymentData, .updateFinalPayment, .getCorporateOffers, .verifyCouponsAndVouchersForBooking, .verifyPurchasedVouchersandCoupons,
             .verifyManualCouponForBooking, .checkReservationRules, .updateReservationPayment:
            return ["Authorization": EnviornmentConfiguration().environment.authorization,
                    "Content-Type": "application/json"]
            
        case .getVoucherHistory, .getAllTheBookingCoupons, .getAllTheBookingVouchers, .rescheduleResetAll, .getAllThePurchaseCoupons:
            return ["Authorization": EnviornmentConfiguration().environment.authorization]
            
        case .getVoucherAndCoupon, .giftVoucher:
            return ["Authorization": EnviornmentConfiguration().environment.authorization,
                    "Content-Type": "application/json",
                    "Accept": "application/json"]
            
        case .verifyVoucher:
            if BBQUserDefaults.sharedInstance.isGuestUser {
                return ["Content-Type": "application/json",
                        "Accept": "application/json",
                        "BBQ-Client-Id": EnviornmentConfiguration().environment.clientID,
                        "BBQ-Client-Secret": EnviornmentConfiguration().environment.clientSecret]
            } else {
                return ["Authorization": EnviornmentConfiguration().environment.authorization,
                        "Content-Type": "application/json",
                        "Accept": "application/json"]
            }
            
        case .cartCheckout:
            return ["Content-Type": "application/json",
                    "Accept": "application/json",
                    "Authorization": EnviornmentConfiguration().environment.authorization]
        }
    }
}
