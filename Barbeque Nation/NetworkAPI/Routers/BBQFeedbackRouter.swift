//
 //  Created by Rabindra L on 03/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQFeedbackRouter.swift
 //

import Foundation
import Alamofire

enum BBQFeedbackRouter: Router {

    case bookingFeedbackQuestions
    case submitBookingFeedback(rating: Int, bookingID:String, qid:Int)

    var method: HTTPMethod {
        switch self {
        case .bookingFeedbackQuestions:
            return .get
            
        case .submitBookingFeedback:
            return .post
        }
    }
    
    var path: String {
        switch self {
            
        case .bookingFeedbackQuestions:
            return ServicePath.bookingFeedbackQuestions.rawValue
            
        case .submitBookingFeedback:
            return ServicePath.submitBookingFeedback.rawValue
            
        }
    }
    
    
    var bodyParams: Any? {

        switch self {
        case .submitBookingFeedback(let rating, let bookingID, let qid):
            return ["rating":rating,
            "booking_id":bookingID,
            "qid":qid]

        default:
            return nil
        }
    }
    
    var headers: HTTPHeaders? {
        switch self {
            
        case .bookingFeedbackQuestions, .submitBookingFeedback:
            return ["Authorization": EnviornmentConfiguration().environment.authorization,
                    "Content-Type": "application/json"]
        }
    }
}
