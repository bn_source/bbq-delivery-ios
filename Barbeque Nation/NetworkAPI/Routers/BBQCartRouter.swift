//
//  Created by Chandan Singh on 07/09/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQCartRouter.swift
//

import Foundation
import Alamofire

enum BBQCartRouter: Router {
    
    //Cart APIs
    case getCart
    case addCart(productId: Int, quantity: Int)
    case updateOrderItem(orderId: String, orderItemId: String, quantity: Int)
    case deleteOrderItem(orderId: String, orderItemId: String)
    case emptyCart(cartID: String)
    
    //Points APIs
    case loyaltyPointsValue(amount: String, currency: String)
    case loyaltyPointsCount
    
    var method: HTTPMethod {
        switch self {
        case .addCart:
            return .post
            
        case .getCart, .loyaltyPointsValue, .loyaltyPointsCount, .emptyCart:
            return .get
            
        case .updateOrderItem:
            return .put
            
        case .deleteOrderItem:
            return .delete
        }
    }
    
    var path: String {
        switch self {
        case .addCart, .getCart, .updateOrderItem:
            return ServicePath.cart.rawValue
            
        case .deleteOrderItem(let orderId, let orderItemId):
            var formattedPath = ServicePath.deleteOrderItem.rawValue.replacingOccurrences(of: "{order_id}", with: orderId)
            formattedPath = formattedPath.replacingOccurrences(of: "{order_item_Id}", with: orderItemId)
            return formattedPath
            
       case .emptyCart(let cartID):
            let formattedPath = ServicePath.emptyCart.rawValue.replacingOccurrences(of: "{orderId}", with: cartID)
            return formattedPath
            
        case .loyaltyPointsValue:
            return ServicePath.loyaltyPointsValue.rawValue
            
        case .loyaltyPointsCount:
            return ServicePath.loyaltyPointsCount.rawValue
        }
    }
    
    var params: Json? {
        switch self {
        case .loyaltyPointsValue(let amount, let currency):
            return ["amount": amount,
                    "currency": currency]
            
        default:
            return nil
        }
    }
    
    var bodyParams: Any? {
        switch self {
        case .addCart(let productId, let quantity):
            return ["product_id": productId,
                    "quantity": quantity]
            
        case .updateOrderItem(let orderId, let orderItemId, let quantity):
            return ["order_id": orderId,
                    "order_item_id": orderItemId,
                    "quantity": quantity]
            
        default:
            return nil
        }
    }
    
    var headers: HTTPHeaders? {
        switch self {
        case .loyaltyPointsValue, .loyaltyPointsCount:
            return ["Authorization": EnviornmentConfiguration().environment.authorization]
            
        default:
            return ["Authorization": EnviornmentConfiguration().environment.authorization,
                    "Accept": "application/json",
                    "Content-Type": "application/json"]
        }
    }
}
