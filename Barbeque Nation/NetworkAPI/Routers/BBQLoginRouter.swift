//
//  BBQLoginRouter.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 02/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import Foundation
import Alamofire

enum BBQLoginRouter: Router {
    
    case generateOTP(countryCode: String, mobileNumber: Int64, otpId: String)
    case verifyOTP(otp: Int, otpId: String)
    case login(countryCode: String, mobileNumber: Int64, otpId: String, otp: Int)
    case social(token: String, provider: String, countryCode: String, mobileNumber: Int64,
    otpId: String, otp: Int, title: String, name: String)
    case refreshToken
    case verifyReferralCode(referralCode:String)
    case verifyUser(countryCode: String, mobileNumber: Int64, generateOTP: Bool)
    case register(countryCode: String, mobileNumber: Int64, email: String, title: String, name: String, dob: String, maritalStatus: Int, newsletter: String, anniversary: String, otpId: String, otp: Int,referralCode:String)
    case logout
    
    var method: HTTPMethod {
        
        switch self {
            
        case .generateOTP, .verifyOTP, .login, .social, .refreshToken, .verifyUser, .register:
            return .post
        case .logout,.verifyReferralCode:
            return .get
        }
    }
    
    var path: String {
        
        switch self {
            
        case .generateOTP:
            return ServicePath.generateOTP.rawValue
            
        case .verifyOTP:
            return ServicePath.verifyOTP.rawValue
            
        case .login:
            return ServicePath.login.rawValue
            
        case .social:
            return ServicePath.socialLogin.rawValue
            
        case .refreshToken:
            return ServicePath.refreshToken.rawValue
            
        case .verifyUser:
            return ServicePath.verifyUser.rawValue
            
        case .verifyReferralCode(let referralCode):
            let formattedPath = ServicePath.verifyReferralCode.rawValue.replacingOccurrences(of: "{referral_code}", with: referralCode)
            return formattedPath
            
        case .register:
            return ServicePath.register.rawValue
            
        case .logout:
            return ServicePath.logout.rawValue
        }
    }
    
    var bodyParams: Any? {
        
        switch self {
        case .generateOTP(let countryCode, let mobileNumber, let otpId):
            return ["country_code": countryCode,
                    "mobile_number": mobileNumber,
                    "otp_id": otpId]
            
        case .verifyOTP(let opt, let otpId):
            return ["otp_id": otpId,
                    "otp": opt]
            
        case .login(let countryCode, let mobileNumber, let otpId, let otp):
            return ["country_code": countryCode,
                    "mobile_number": mobileNumber,
                    "otp_id": Int(otpId)!,
                    "otp": otp]
            
        case .social(let token, let provider, let countryCode,
                     let mobileNumber, let otpId, let otp, let title, let name):
            return ["token": token,
                    "provider": provider,
                    "country_code": countryCode,
                    "mobile_number": mobileNumber,
                    "otp_id": Int(otpId) ?? 0,
                    "otp": otp,
                    "title": title,
                    "name":name]
            
        case .refreshToken:
            return ["grant_type": "refresh_token",
                    "refresh_token": BBQUserDefaults.sharedInstance.refreshToken]
            
        case .verifyUser(let countryCode, let mobileNumber, let generateOTP):
            return ["country_code": countryCode,
                    "mobile_number": mobileNumber,
                    "verify":!generateOTP]
            
        case .register(let countryCode, let mobileNumber, let email, let title, let name, let dob, let maritalStatus, let newsletter, let anniversary, let otpId, let otp,let referralCode):
            return ["country_code": countryCode,
                    "mobile_number": mobileNumber,
                    "email": email,
                    "title": title,
                    "name": name,
                    "date_of_birth": dob,
                    "marital_status": maritalStatus,
                    "newsletter": newsletter,
                    "anniversary_date": anniversary,
                    "otp_id": Int(otpId) ?? 0,
                    "otp": otp,
                    "referral_code":referralCode]
            
        default:
            return nil
        }
    }
    
    var headers: HTTPHeaders? {
        switch self {
            
        case .logout:
            return ["Content-Type": "application/json",
                    "Authorization": EnviornmentConfiguration().environment.authorization]
            
        case .refreshToken:
            return ["Content-Type": "application/json",
                    "Accept": "application/json",
                    "BBQ-Client-Id": EnviornmentConfiguration().environment.clientID,
                    "BBQ-Client-Secret": EnviornmentConfiguration().environment.clientSecret]
            
        case .verifyUser, .register,.verifyReferralCode:
            return ["Content-Type": "application/json",
                    "Accept": "application/json",
                    "BBQ-Client-Id": EnviornmentConfiguration().environment.clientID,
                    "BBQ-Client-Secret": EnviornmentConfiguration().environment.clientSecret]
            
        default:
            return ["Content-Type": "application/json",
                    "BBQ-Client-Id": EnviornmentConfiguration().environment.clientID,
                    "BBQ-Client-Secret": EnviornmentConfiguration().environment.clientSecret]
        }    }
}
