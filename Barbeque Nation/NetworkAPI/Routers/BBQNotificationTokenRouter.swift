//
//  Created by Bhamidipati Kishore on 24/09/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQNotificationTokenRouter.swift
//

import Foundation
import Alamofire

enum BBQNotificationTokenRouter: Router {
    
    case postNotificationTokenForGuestUser(token: String , deviceType: String)
    case postNotificationToken(token: String , deviceType: String)
    
    var method: HTTPMethod {
        switch self {
        case .postNotificationToken:
            return .post
        case .postNotificationTokenForGuestUser:
            return .post
        }
    }
    
    var path: String {
        switch self {
        case .postNotificationToken:
            return ServicePath.notificationToken.rawValue
        case .postNotificationTokenForGuestUser:
            return ServicePath.notificationToken.rawValue
        }
    }
    
    var headers: HTTPHeaders? {
        switch self {
        case .postNotificationToken:
            return ["Authorization": EnviornmentConfiguration().environment.authorization,
                    "Content-Type": "application/json"]
        case .postNotificationTokenForGuestUser:
            return ["Content-Type": "application/json",
                    "BBQ-Client-Id": EnviornmentConfiguration().environment.clientID,
                    "BBQ-Client-Secret": EnviornmentConfiguration().environment.clientSecret]
        }
    }
    
    var bodyParams: Any? {
        switch self {
        case .postNotificationToken(let token, let deviceType):
            return ["token": token,
                    "device_type": deviceType]
        case .postNotificationTokenForGuestUser(let token, let deviceType):
            return ["token": token,
                    "device_type": deviceType]
        }
    }
}
