//
//  BBQPaymentRouter.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 04/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import Foundation
import Alamofire

enum BBQPaymentRouter: Router {
    
    var razorPayBaseUrl: String? {
        switch self {
        case .createCustomer:
            return EnviornmentConfiguration().environment.razorPayBaseURL
        default:
            return nil;
        }
    }
    
    case createCustomer(name:String,email:String,contact:String)
    case createOrder(amount: String, currency: String, receipt: String, payment_capture: String)
    case rpCreateOrder(amount: String, currency: String, bookingID: String, loyalty_points: Int?)
    case rpCreateVoucherOrder(discount: String, loyalty_points: String, bar_code: String)
    case fetchSavedTokens(_ razorPayId:String)
    
    var method: HTTPMethod {
        switch self {
        case .createOrder, .rpCreateOrder,.createCustomer,.fetchSavedTokens:
            return .post
            
        case .rpCreateVoucherOrder:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .createOrder:
            return ServicePath.createPaymentOrder.rawValue
            
        case .rpCreateOrder:
            return ServicePath.rpCreateOrder.rawValue
            
        case .rpCreateVoucherOrder:
            return ServicePath.rpCreateVoucherOrder.rawValue
        case .createCustomer:
            return ServicePath.rpCreateCustomer.rawValue
        case .fetchSavedTokens(_):
            return ServicePath.rpfetchTokens.rawValue
        }
    }
    
    var queryItems: [URLQueryItem]? {
        switch self {
    
        case .rpCreateVoucherOrder(let discount,let loyalty_points, let bar_code):
            var queryItems = [URLQueryItem]()
            if discount != "0" {
                queryItems.append(URLQueryItem(name: "discount", value: discount))
            }
            
            if loyalty_points != "0"{
                queryItems.append(URLQueryItem(name: "loyalty_points", value: loyalty_points))
            }
            
            if bar_code != ""{
                queryItems.append(URLQueryItem(name: "bar_code", value: bar_code))
            }
            if queryItems.count == 0{
                return nil
            }
            return queryItems
            
        default:
            return nil
        }
    }
    
    var headers: HTTPHeaders? {
        switch self {
        case .createOrder:
            return ["Content-Type": "application/json",
                    "Authorization": EnviornmentConfiguration().environment.razorpayAuthorization]
            
        case .rpCreateOrder, .rpCreateVoucherOrder,.createCustomer,.fetchSavedTokens:
            return ["Content-Type": "application/json",
                    "Authorization": EnviornmentConfiguration().environment.authorization]
        }
    }
    
    var bodyParams: Any? {
        switch self {
        case .createOrder(let amnt, let curr, let receipt, let pymntCapture):
            return ["amount": amnt,
                    "currency": curr,
                    "receipt": receipt,
                    "payment_capture": pymntCapture]
            
        case .rpCreateOrder(let amount, let currency, let bookingID, let loyalty_points):
            if let loyalty_points = loyalty_points, loyalty_points != 0, loyalty_points > 0{
                return ["amount": amount,
                        "currency": currency,
                        "booking_id": bookingID,
                        "loyalty_points": loyalty_points]
            }
            return ["amount": amount,
                    "currency": currency,
                    "booking_id": bookingID]
        case .fetchSavedTokens(let razorPayId):
            return ["customer_id":razorPayId]

        case .createCustomer(let name, let email, let contact):
            return ["name":name,
                    "email":email,
                    "contact":contact,
                    "fail_existing":"0"
//                    ,
//                    "notes":[
//                        "landmark": "Razorpay Office Building",
//                        "location": "Bangalore"
//                      ]
            ]
            
        default:
            return nil
        }
    }
}
