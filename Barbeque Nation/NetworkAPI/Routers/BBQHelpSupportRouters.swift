//
 //  Created by Rabindra L on 19/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQHelpSupportRouters.swift
 //

import Foundation
import Alamofire

enum BBQHelpSupportRouters: Router {
    
    case helpSupport
    case about
    case helpSupportById(id:[Int])
    case getWelcomeInfo
    
    var method: HTTPMethod {
        switch self {
        case .helpSupport, .about, .getWelcomeInfo:
            return .get
        case .helpSupportById:
            return .post
        }
    }
    
    var path: String {
    switch self {
        
        case .helpSupportById:
            return ServicePath.helpSupportById.rawValue
        
        case .helpSupport:
            return ServicePath.helpSupport.rawValue
            
        case .about:
            return ServicePath.about.rawValue
        
        case .getWelcomeInfo:
            return ServicePath.getWelcomeInfo.rawValue
            
        }
    }
    
    
    var bodyParams: Any? {

        switch self {
        case .helpSupportById(let id):
            return ["tag_ids":id]
            
        default:
            return nil
        }
    }
    
    var headers: HTTPHeaders? {
        switch self {

        case .helpSupport, .about, .helpSupportById:
            return ["Authorization": EnviornmentConfiguration().environment.authorization,
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                    "BBQ-Client-Id": EnviornmentConfiguration().environment.clientID,
                    "BBQ-Client-Secret": EnviornmentConfiguration().environment.clientSecret]
            
        case .getWelcomeInfo:
            return ["BBQ-Client-Id": EnviornmentConfiguration().environment.clientID,
                    "BBQ-Client-Secret": EnviornmentConfiguration().environment.clientSecret]
        }
    }
}
