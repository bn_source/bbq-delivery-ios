//
//  Created by Bhamidipati Kishore on 23/09/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQLoyalityPointsRouter.swift
//

import Foundation
import Alamofire

enum BBQLoyalityPointsRouter: Router {
    
    case getLoyalityPoints(pageNumber:Int, pageSize:Int, sort:String)
    case postConvertLoyalityToCoupons(country_code: String = "+91", loyalty_event_type: String = "COUPON_PURCHASED", mobile_number: String, points: String, transaction_id: String = "2")
    
    
    var method: HTTPMethod {
        switch self {
        case .getLoyalityPoints:
            return .get
        case .postConvertLoyalityToCoupons:
            return .post
        }
    }
    
    var path: String {
        switch self {
        case .getLoyalityPoints:
            return ServicePath.getLoyalityPoints.rawValue
        case .postConvertLoyalityToCoupons:
            return ServicePath.convert_to_coupon.rawValue
        }
    }
    
    var headers: HTTPHeaders? {
        switch self {
        case .getLoyalityPoints:
            return ["Authorization": EnviornmentConfiguration().environment.authorization]
        case .postConvertLoyalityToCoupons:
            return ["Authorization": EnviornmentConfiguration().environment.authorization]
        }
    }
    
    var params: Json? {
        switch self {
        case .getLoyalityPoints(let pageNumber, let pageSize, let sort):
            return ["page": pageNumber,
                    "size": pageSize,
                    "sort": sort]
        case .postConvertLoyalityToCoupons:
            return nil
        }
    }
    
    var bodyParams: Any? {
        switch self {
        case .getLoyalityPoints:
            return nil
        case .postConvertLoyalityToCoupons(country_code: let country_code, loyalty_event_type: let loyalty_event_type, mobile_number: let mobile_number, points: let points, transaction_id: let transaction_id):
            return [
              "country_code": country_code,
              "loyalty_event_type": loyalty_event_type,
              "mobile_number": mobile_number,
              "points": points,
              "transaction_id": transaction_id]
        }
    }
}
