//
 //  Created by Abhijit on 23/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQLastVisitedBranchRouters.swift
 //

import Foundation
import Alamofire

enum BBQLastVisitedBranchRouters:Router {
    
    case lastVistedBranch(branchId: String)
    
    var method: HTTPMethod{
        
        switch self {
        case .lastVistedBranch:
            return .post
        }
    }
    
    var path: String{
        
        switch self {
        case .lastVistedBranch:
            
            return ServicePath.lastVistedBranch.rawValue
        }
    }
    
    var bodyParams: Any? {
        
        switch self {
        case .lastVistedBranch(let id):
            return ["branch_id":id]
        }
    }
    
    var headers: HTTPHeaders?{
        
        switch self {
        case .lastVistedBranch:
            return ["Content-Type": "application/json",
                    "Authorization": EnviornmentConfiguration().environment.authorization]
        }
    }
}
