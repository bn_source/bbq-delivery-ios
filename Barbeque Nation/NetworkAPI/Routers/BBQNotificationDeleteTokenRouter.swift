//
 //  Created by Bhamidipati Kishore on 24/10/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQNotificationDeleteTokenRouter.swift
 //

import Foundation
import Alamofire

enum BBQNotificationDeleteTokenRouter: Router {
    
    case deleteNotificationTokenForGuestUser(token: String , deviceType: String)
    case deleteNotificationToken(token: String , deviceType: String)
    
    var method: HTTPMethod {
        switch self {
        case .deleteNotificationToken:
            return .delete
        case .deleteNotificationTokenForGuestUser:
            return .delete
        }
    }
    
    var path: String {
        switch self {
        case .deleteNotificationToken:
            return ServicePath.notificationToken.rawValue
        case .deleteNotificationTokenForGuestUser:
            return ServicePath.notificationToken.rawValue
        }
    }
    
    var headers: HTTPHeaders? {
        switch self {
        case .deleteNotificationToken:
            return ["Authorization": EnviornmentConfiguration().environment.authorization,
                    "Content-Type": "application/json"]
        case .deleteNotificationTokenForGuestUser:
            return ["Content-Type": "application/json",
                    "BBQ-Client-Id": EnviornmentConfiguration().environment.clientID,
                    "BBQ-Client-Secret": EnviornmentConfiguration().environment.clientSecret]
        }
    }
    
    var bodyParams: Any? {
        switch self {
        case .deleteNotificationToken(let token, let deviceType):
            return ["token": token,
                    "device_type": deviceType]
        case .deleteNotificationTokenForGuestUser(let token, let deviceType):
            return ["token": token,
                    "device_type": deviceType]
        }
    }
}
