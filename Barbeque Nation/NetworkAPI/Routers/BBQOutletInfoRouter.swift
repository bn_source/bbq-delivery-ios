//
//  Created by Abhijit on 12/09/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQOutletInfoRouter.swift
//

import Foundation
import Alamofire

enum BBQOutletInfoRouter:Router {
    
    case outletInfo(branchId: String)
    
    var method: HTTPMethod{
        
        switch self {
        case .outletInfo:
            return .get
        }
    }
    
    var path: String{
        
        switch self {
        case .outletInfo(let branchId):
            
            let formattedPath = ServicePath.outletInfo.rawValue.replacingOccurrences(of: "{branchID}", with: branchId)
            return formattedPath
        }
    }
    
    var headers: HTTPHeaders?{
        
        switch self{
        case .outletInfo:
            if BBQUserDefaults.sharedInstance.isGuestUser{
                return ["Content-Type": "application/json",
                        "BBQ-Client-Id": EnviornmentConfiguration().environment.clientID,
                        "BBQ-Client-Secret": EnviornmentConfiguration().environment.clientSecret]
            }else{
                return ["Content-Type": "application/json",
                        "Authorization": EnviornmentConfiguration().environment.authorization]
            }
            
        }
    }
}
