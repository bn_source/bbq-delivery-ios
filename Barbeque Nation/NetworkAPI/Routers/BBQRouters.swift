//
//  BBQRouters.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 02/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import Foundation
import Alamofire

typealias Json = [String : Any]

// MARK: - Protocol
protocol Router: URLRequestConvertible {
    static var baseUrl: String { get }
    var method: HTTPMethod { get }
    var path: String { get }
    var params: Parameters? { get }
    var bodyParams: Any? { get }
    var headers: HTTPHeaders? { get }
    var timeOut: TimeInterval { get }
    var queryItems: [URLQueryItem]? { get }
    var razorPayBaseUrl:String? { get }
}

// MARK: - Url request convertible delegate
extension Router {
    
    static var baseUrl: String {
        return EnviornmentConfiguration().environment.baseURL
    }
    
     var razorPayBaseUrl: String? {
        return nil
    }
    
    static var instagramBaseUrl: String {
        return EnviornmentConfiguration().environment.instagramBaseURL
    }
    
    var timeOut: TimeInterval {
        return 30.0
    }
    
    var params: Parameters? {
        return nil
    }
    
    var bodyParams: Any? {
        return nil
    }
    
    var queryItems: [URLQueryItem]? {
           return nil
       }
    
    func asURLRequest() throws -> URLRequest {
        
        var url = try type(of: self).baseUrl.asURL()
        
        switch self.path { // Use instagram URL for instagram APIs
        case ServicePath.instagramUserInfo.rawValue:
            url = try type(of: self).instagramBaseUrl.asURL()
        default:
            break
        }
        
        url.appendPathComponent(self.path)
        
        // Adds custom query strings to request URL if they have any.
        if let queryStrings = self.queryItems {
             var components = URLComponents(url: url, resolvingAgainstBaseURL: true)
             components?.queryItems = queryStrings
            if let finalURL = components?.url {
                url = finalURL
            }
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        urlRequest.timeoutInterval = timeOut
        urlRequest.httpShouldHandleCookies = false
        
        if let httpHeaders = headers {
            for header in httpHeaders {
                urlRequest.setValue(header.value, forHTTPHeaderField: header.key)
            }
        }
        
        if BBQUserDefaults.sharedInstance.isGuestUser {
                  
              urlRequest.setValue(String(format: "Ios-%@", UIDevice.current.identifierForVendor?.uuidString ?? ""), forHTTPHeaderField: "User-Agent")

              }else{
                  if SharedProfileInfo.shared.profileData?.mobileNumber == "" ||  SharedProfileInfo.shared.profileData?.mobileNumber == nil {
                      urlRequest.setValue(String(format: "User-%@", UIDevice.current.identifierForVendor?.uuidString ?? ""), forHTTPHeaderField: "User-Agent")

                  }else {
                      urlRequest.setValue(String(format: "User-%@",SharedProfileInfo.shared.profileData?.mobileNumber ?? ( UIDevice.current.identifierForVendor?.uuidString ?? "") ), forHTTPHeaderField: "User-Agent")
                  }
              }
        
        if let parameters = bodyParams {
            if let data = parameters as? Data {
                urlRequest.httpBody = data
            }
            else {
                urlRequest = try JSONEncoding.default.encode(urlRequest, withJSONObject: parameters)
            }
        }
        else if let contentType = headers?["Content-Type"], contentType.caseInsensitiveCompare("application/json") == .orderedSame {
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: params)
        }
        else {
            urlRequest = try URLEncoding.default.encode(urlRequest, with: params)
        }
        return urlRequest
    }
}
