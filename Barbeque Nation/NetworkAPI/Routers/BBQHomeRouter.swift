//
//  BBQHomeRouter.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 04/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import Foundation
import Alamofire

enum BBQHomeRouter: Router {
    case getPromotionsAndVouchers(latitude: Double, longitude: Double, branch_id: String)
    case getUpcomingReservation
    case promotionDetails(promotionId: String)
    case voucherDetails(voucherId: String)
    case getNearbyOutlet(latitude: Double,longitude: Double)
    case getNearbyOutletV1(latitude: Double,longitude: Double)
    case latLong(lat: Float, long: Float, only_reservation: Bool)
    case getVoucherAndCouponListWithCountDetails(branch_id: Int, customer_id : String)
    
    var method: HTTPMethod {
        switch self {
        case .getPromotionsAndVouchers,.getNearbyOutlet, .getNearbyOutletV1, .getVoucherAndCouponListWithCountDetails:
            return .post

        case .latLong:
            return .post
            
        case .getUpcomingReservation, .voucherDetails, .promotionDetails:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .getPromotionsAndVouchers:
            return ServicePath.promotions.rawValue
            
        case .getUpcomingReservation:
            return ServicePath.upcomingReservation.rawValue
            
        case .voucherDetails(let voucherId):
            return ServicePath.voucherDetails.rawValue + "/\(voucherId)"
            
        case .promotionDetails(let promotionId):
            return ServicePath.promotionDetails.rawValue + "/\(promotionId)"
            
        case .getNearbyOutlet:
            return ServicePath.latLong.rawValue
            
        case .latLong:
            return ServicePath.latLong.rawValue
            
        case .getNearbyOutletV1:
            return ServicePath.nearbyBranchV1.rawValue
        case .getVoucherAndCouponListWithCountDetails:
            return ServicePath.getVoucherAndCouponListWithCountDetails.rawValue
        }
    }
    
    var bodyParams: Any? {
        switch self {
        case .getPromotionsAndVouchers(let latitude, let longitude, let branch_id):
            if latitude == 0.0, longitude == 0.00, branch_id == "" {
                return["lat":"",
                       "long":"",
                       "branch_id":""]
            }
            return["lat":latitude,
                   "long":longitude,
                   "branch_id":branch_id]
            
        case .getNearbyOutlet(let latitude, let longitude):
            return["lat":latitude,
                   "long":longitude,
                   "type": "reservation"]
        
        case .getNearbyOutletV1(let latitude, let longitude):
            return["lat":latitude,
                   "long":longitude]

            
        case .latLong(let latitude,let longitude, let only_reservation):
            if only_reservation{
                return ["lat":latitude,
                        "long":longitude,
                        "type": "reservation"]
            }
            return ["lat":latitude,
                    "long":longitude]
            
        case .getVoucherAndCouponListWithCountDetails( let branchId , let customer_id ):
            
            return ["customer_id": customer_id,
                "branch_id": branchId ]
            
        default:
            return nil
        }
    }
    
    var headers: HTTPHeaders? {
        switch self {
        case .getPromotionsAndVouchers, .latLong:
            return ["Content-Type": "application/json",
                    "Accept": "application/json",
                    "BBQ-Client-Id": EnviornmentConfiguration().environment.clientID,
                    "BBQ-Client-Secret": EnviornmentConfiguration().environment.clientSecret]
            
        case.getUpcomingReservation:
            return ["Authorization": EnviornmentConfiguration().environment.authorization,
                    "Content-Type": "application/json"]
            
        case .voucherDetails, .promotionDetails:
            return ["Authorization": EnviornmentConfiguration().environment.authorization,
                    "Content-Type": "application/json",
                    "Accept": "application/json",
                    "BBQ-Client-Id": EnviornmentConfiguration().environment.clientID,
                    "BBQ-Client-Secret": EnviornmentConfiguration().environment.clientSecret]
            
        case .getNearbyOutlet, .getNearbyOutletV1:
            return ["Content-Type": "application/json",
                    "Accept": "application/json",
                    "BBQ-Client-Id": EnviornmentConfiguration().environment.clientID,
                    "BBQ-Client-Secret": EnviornmentConfiguration().environment.clientSecret]
            
        case .getVoucherAndCouponListWithCountDetails:
            return ["Content-Type": "application/json",
                    "Accept": "application/json",
                    "Authorization": EnviornmentConfiguration().environment.authorization,
                    "BBQ-Client-Id": EnviornmentConfiguration().environment.clientID,
                    "BBQ-Client-Secret": EnviornmentConfiguration().environment.clientSecret]
        }
    }
}
