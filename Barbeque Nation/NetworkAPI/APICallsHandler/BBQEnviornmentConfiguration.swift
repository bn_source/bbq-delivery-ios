//
//  BBQEnviornmentConfiguration.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 03/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import Foundation

enum ServicePath: String {
    //MARK: Login Service Path
    case generateOTP = "generate-otp"
    case verifyOTP = "verifyotp"
    case login = "login"
    case socialLogin = "social"
    case refreshToken = "oauth/token"
    case verifyUser = "verify-user"
    case verifyReferralCode = "verify-referral-code/{referral_code}"
    case register = "register"
    case logout = "logout"
    
    // MARK: Promotion and Vouchers
    case promotions = "promotions"
    case promotionDetails = "promotion-details"
    case voucherDetails = "voucher-details"
    case voucherAndCoupons = "user-coupon-vouchers"
    case verifyVoucher = "verify-voucher"
    case giftVoucher = "gift-voucher"
    //to verify user coupons and corporate coupons
    case verifyBookingCouponsandVouchers = "verify-reservation-vouchers"
    case verifyPurchasedCouponsandVouchers = "verify-purchase-vouchers"
    case verifyManualCouponCode = "verify-manual-bar-code"
    case getTheCoupons = "vouchers"
    case getVoucherAndCouponListWithCountDetails = "voucher/voucher-coupon-list"
    //MARK: Location
    case lastVistedBranch = "last-visited-branch"
    case latLong = "get-all-nearby-branches"
    case nearbyBranchV1 = "get-all-nearby-branches-v1"
    
    //MARK: Cart
    case cart = "cart"
    case deleteOrderItem = "cart/{order_id}/{order_item_Id}"
    case loyaltyPointsValue = "loyalty-points-value"
    case loyaltyPointsCount = "loyalty-points-count"
    case emptyCart = "empty-cart/{orderId}"
    
    
    //MARK: User Service Path
    case user = "user"
    case changeMobile = "user/mobile"
    case changeEmail = "user/email"
    case changeProfilePicture = "/profile-picture"
    case updateRazorPayID = "profile-razorpay-id"
    
    //MARK: Booking Service Path
    case bookings = "bookings"
    case booking_details = "bookings-details"
    case getSlots = "slots"
    case reservation_rules = "reservation/reservation-rules"
    case update_adv_res_payment = "reservation/update-adv-res-payment"
    //Upcoming reservation
    case upcomingReservation = "up-coming-bookings"
    
    //MARK:Special Instruction
    case updateSpecialInstructions = "bookings/special-instructions"
    case getSpecialInstructions =  "get-occasion-joined-by-master"
    case menubranchbuffet = "menu-branch-buffet"
    case menubranchbuffetNew = "menu-buffet-price"
    case cancelBooking = "bookings/cancel"
    case rescheduleBooking = "bookings/reschedule"
    case rescheduleResetAll = "bookings/remove-points-coupons"
    
    // Cancel Booking
    case cancelReasons = "bookings/cancellation-reasons"
    case refundOptions = "bookings/refundAmount"
    
    //MARK: Feedback
    case bookingFeedbackQuestions = "feedback-questions"
    case submitBookingFeedback = "feedback-submit"
    
    // MARK: Instagram User
    case instagramUserInfo = "users/self"
    
    // MARK: Payment
    case createPaymentOrder = "orders"
    case checkoutPayment = "cart/checkout"
    
    case rpCreateOrder = "bookings/generate-rp-order"
    case rpCreateVoucherOrder = "cart/generate-rp-order"
    case rpCreateCustomer = "customer/create"
    case rpfetchTokens = "customer/tokens"

    // MARK: OutletInfo
    case outletInfo = "branches/{branchID}"
    
    // MARK: Corporate Booking
    case verifyCorporates = "verify-corporates"
    case corporateOffers = "corporate-vouchers"
    
    // MARK: MyOrder Voucher History
    case getVouchorHistory = "user-vouchers"
    
    // MARK: Welcomescreen
    case getWelcomeInfo = "user-welcome"
    
    case helpSupport = "help-support-all-faq"
    case helpSupportById = "help-support-faq-by-ids"
    case about = "about"
    
    // MARK: Loyality Points
    case getLoyalityPoints = "loyalty-points"
    case convert_to_coupon = "customer-loyalty-points/convert-to-coupon"
    
    // MARK: Notification Token
    case notificationToken = "push-notification-tokens"
    
    // MARK:
    case mobileTermsAndCondition = "mobile-terms-and-condition"
    
    // MARK: Update
    case appUpdate = "force-update"
    
    //MARK: FeedBack get Question
    case getFeedbackQQuestion = "q"
    case submitFeedBackData =  "r"
}

enum BackendEnvironment: String {
    case Development = "dev"
    case Staging = "staging"
    case Production = "production"
    
    var baseURL: String {
//        switch self {
//        case .Development:
//            return "https://api.barbequenation.com/api/v1"//SIT
//
//        case .Staging:
//            return "https://api.barbequenation.com/api/v1"
//
//        case .Production:
//            return "https://api.barbequenation.com/api/v1"
////            return "https://prod-copy.barbequenation.com/api/v1"
//        }
         return "https://api.barbequenation.com/api/v1"//"https://bbq-stage-mobile.bnhl.in/api/v1"
    }
    
    var baseURLForNewServer: String {
//        switch self {
//        case .Development:
//            return "https://api.barbequenation.com/api/v1"//SIT
//
//        case .Staging:
//            return "https://api.barbequenation.com/api/v1"
//
//        case .Production:
//            return "https://api.barbequenation.com/api/v1"
////            return "https://prod-copy.barbequenation.com/api/v1"
//        }
         return "https://api.barbequenation.com/api/v2"// "https://bbq-stage-mobile.bnhl.in/api/v2" //
    }
    
    var razorPayBaseURL: String {
        return "https://api.razorpay.com/v1/"
    }
    
    var instagramBaseURL: String {
        return "https://api.instagram.com/v1/"
    }
    
    var clientID: String {
        switch self {
        case .Development:
            return "36634b64-d4fe-4659-a06b-8c0b23b16335"
            
        case .Staging:
            return "36634b64-d4fe-4659-a06b-8c0b23b16335"
            
        case .Production:
            return "36634b64-d4fe-4659-a06b-8c0b23b16335"
        }
    }
    
    var newDBSetUP: Bool{
         
        return  false
    }
    var clientSecret: String {
        switch self {
        case .Development:
            return "k!w6hr5+ZJ"
            
        case .Staging:
            return "k!w6hr5+ZJ"
            
        case .Production:
            return "k!w6hr5+ZJ"
        }
    }
    
    var authorization: String {
        
        return "Bearer \(BBQUserDefaults.sharedInstance.accessToken)"
    }
    
    var razorpayAuthorization : String {
        /*
         Razorpay APIs use basic access authentication to authenticate all server-side requests.
         The API Key can be used for basic authentication by sending the Key ID as the username
         and the Key Secret as the password.
         */
        
        //let userName = "rzp_test_Q3XZ8wLagEEgaG" // Development mode
        let userName = "rzp_live_woEpCznOt0mINU"
        let password = self.clientSecret
        let credentialData = "\(userName):\(password)".data(using: .utf8)
        guard let cred = credentialData else { return "" }
        let base64Credentials = cred.base64EncodedData(options: [])
        guard let base64Date = Data(base64Encoded: base64Credentials) else { return "" }
        return  "Basic \(base64Date.base64EncodedString())"
        
    }
    
    var gmailClientID: String {
        switch self {
        case .Development, .Staging:
            return "155650652337-6036mgdf4f2ejau8ulvvvo09e6dd6djj.apps.googleusercontent.com"
            
        case .Production:
            return "155650652337-6036mgdf4f2ejau8ulvvvo09e6dd6djj.apps.googleusercontent.com"
        } 
    }
}

struct EnviornmentConfiguration {
    var environment: BackendEnvironment {
        if let configuration = Bundle.main.object(forInfoDictionaryKey: "Configuration") as? String {
            if (configuration.range(of: "Dev") != nil) {
                return BackendEnvironment.Development
            } else if (configuration.range(of: "Staging") != nil) {
                return BackendEnvironment.Staging
            } else if (configuration.range(of: "Production") != nil) {
                return BackendEnvironment.Production
            }
        }
        return BackendEnvironment.Development
    }
}
