//
//  BBQAPICallsHandler.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 02/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import Foundation
import Alamofire

typealias AlamofireResponse = DataResponse<Any>

class BBQWebServiceHandler: NSObject {
    
    // MARK: - Properties
    static var currentTask: DataRequest?
    
    // MARK: - Request
    class func request(url: URLRequestConvertible, loading: Bool = false, completion: @escaping (AlamofireResponse?) -> ()) {
        
        //currentTask = Alamofire.request(url).validate().responseJSON { (response) in
        currentTask = BBQServiceManager.getInstance().alamofireManager.request(url).validate().responseJSON { (response) in
            print(response)
            completion(response)
        } 
    }
}
