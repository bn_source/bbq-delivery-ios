//
//  BBQLazyImageLoad.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 05/09/19.
//  Copyright © 2019 Organization. All rights reserved.
//

import UIKit
import SDWebImage

class LazyImageLoad {
    
    static func setImageOnImageViewFromURL(imageView : UIImageView, url:String?, response:@escaping (_ image : UIImage?) -> Void) {
        
        imageView.sd_cancelCurrentImageLoad()
        guard let urlString = url, let pathURL = getURLFromString(url: urlString) else {
            response(nil)
            return
        }
        
        imageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imageView.sd_setImage(with: pathURL, completed: { (image, error, type, url) in
            response(image)
            if let error = error{
                print("ImageError-\(error.localizedDescription)")
                print("ImageError-\(type)")
                print("ImageError-\(url?.absoluteString ?? "")")
            }
        })
    }
    
    class func getURLFromString(url : String) -> URL? {
        let removePercentEncodeURL  = url.removingPercentEncoding
        let urlWithEscapeString = removePercentEncodeURL?.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
        
        if urlWithEscapeString == nil {
            return nil
        }
        
        let  strURL : URL? = URL(string: (urlWithEscapeString)!)
        return strURL
    }
}
