//
 //  Created by Chandan Singh on 30/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQToastHandler.swift
 //

import Foundation
import TTGSnackbar
import UIKit

typealias ToastActionHandler = () -> Void

class ToastHandler {
    
    static var currentToast : TTGSnackbar?
    
    class func showToastWithMessage(message: String) {
        let snackbar = TTGSnackbar(message: message, duration: .middle)
        
        snackbar.backgroundColor = .white
        snackbar.messageTextColor = .black
        snackbar.messageTextFont = UIFont.appThemeSemiBoldWith(size: 14.0)
        snackbar.leftMargin = 20
        snackbar.rightMargin = 20
        snackbar.bottomMargin = 30
        snackbar.shouldDismissOnSwipe = true
        ToastHandler.currentToast?.dismiss()
        snackbar.show()
        ToastHandler.currentToast = snackbar
    }
    
    class func showToastWithMessageAndAction(message: String, actionText: String,
                                             action: @escaping ToastActionHandler) {
        
        let snackbar = TTGSnackbar(message: message,
                                   duration: .middle,
                                   actionText: actionText,
                                   actionBlock: { (snackbar) in
                                    action()
        })
        
        snackbar.backgroundColor = .white
        snackbar.messageTextColor = .black
        snackbar.messageTextFont = UIFont.appThemeSemiBoldWith(size: 14.0)
        snackbar.actionTextColor = .black
        snackbar.actionTextFont = UIFont.appThemeRegularWith(size: 14.0)
        snackbar.leftMargin = 20
        snackbar.rightMargin = 20
        snackbar.bottomMargin = 30
        snackbar.show()
    }
}
