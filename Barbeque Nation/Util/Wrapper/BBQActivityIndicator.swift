////
 //  Created by Chandan Singh on 19/08/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQActivityIndicator.swift
 //

import UIKit

class BBQActivityIndicator {
    
    class func showIndicator(withTitle title: String = "", and Description:String = kLoadingProgressString, view: UIView, addBackgroud: Bool = false) {
        
//        let indicator = MBProgressHUD.showAdded(to: view, animated: true)
//        indicator.label.text = title
//
//        if addBackgroud {
//            indicator.backgroundColor = .white
//        }
//
//        /*Setting userInteractionEnabled = true means that the HUD receives touches. This in turn means that it prevents touches from passing through it to other views. Setting userInteractionEnabled = false makes the hud opaque to touches, meaning that they pass through it to views that are positioned below it.*/
//        indicator.isUserInteractionEnabled = true
//        //Note: Do not turn this off. Very sensitive. It will create another issues through out the app.
//
//        indicator.detailsLabel.text = Description
//        indicator.show(animated: true)
        
        UIUtils.showLoader()
    }
    
    class func hideIndicator(from view: UIView) {
//        MBProgressHUD.hide(for: view, animated: true)
        UIUtils.hideLoader()
    }
    
    class func addBackgroundView() {
        
    }
}
