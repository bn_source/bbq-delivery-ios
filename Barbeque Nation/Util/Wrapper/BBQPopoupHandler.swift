//
 //  Created by Chandan Singh on 29/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQPopoupHandler.swift
 //

import Foundation
import UIKit

typealias ActionHandler = () -> Void

class PopupHandler {
    
    class func showSingleButtonsPopup(title: String, message: String, isCentered: Bool = false,
                                      image: UIImage? = nil,
                                      titleImageFrame: CGRect = .zero,
                                      on controller:UIViewController, blurEffect: Bool = false,
                                      firstButtonTitle: String, firstAction: @escaping ActionHandler) {
        
        // Create the dialog
        var popupInitializer: PopupDialog?
        var formattedMessage: String? = message
        if message == "" {
            formattedMessage = nil
        }
        if image == nil {
            popupInitializer = PopupDialog(title: title, message: formattedMessage, buttonAlignment: .horizontal, transitionStyle: .zoomIn, tapGestureDismissal: false, panGestureDismissal: false)
        } else {
            let formattedTitle = PopupHandler.stringStartingWithImage(image: image, text: title, titleImageFrame: titleImageFrame)
            popupInitializer = PopupDialog(title: formattedTitle, message: formattedMessage, buttonAlignment: .horizontal, transitionStyle: .zoomIn, tapGestureDismissal: false, panGestureDismissal: false)
        }
        
        guard let popup = popupInitializer else {
            return
        }
        
        // Create buttons
        let buttonOne = CancelButton(title: firstButtonTitle) {
            firstAction()
        }
        
        // Add buttons to dialog
        popup.addButtons([buttonOne])
        
        PopupHandler.customizeAppearance(popup: popup, blurEffect: blurEffect, isCentered: isCentered)
        
        // Present dialog
        controller.present(popup, animated: true, completion: nil)
    }
    
    class func showTwoButtonsPopup(title: String, message: String, isCentered: Bool = false,
                                   image: UIImage? = nil,
                                   titleImageFrame: CGRect = .zero,
                                   on controller:UIViewController, blurEffect: Bool = false,
                                   firstButtonTitle: String, firstAction: @escaping ActionHandler,
                                   secondButtonTitle: String, secondAction: @escaping ActionHandler) {
        
        // Create the dialog
        var popupInitializer: PopupDialog?
        var formattedMessage: String? = message
        if message == "" {
            formattedMessage = nil
        }
        if image == nil {
            popupInitializer = PopupDialog(title: title, message: formattedMessage, buttonAlignment: .horizontal, transitionStyle: .zoomIn, tapGestureDismissal: false, panGestureDismissal: false)
        } else {
            let formattedTitle = PopupHandler.stringStartingWithImage(image: image, text: title, titleImageFrame: titleImageFrame)
            popupInitializer = PopupDialog(title: formattedTitle, message: formattedMessage, buttonAlignment: .horizontal, transitionStyle: .zoomIn, tapGestureDismissal: false, panGestureDismissal: false)
        }
        
        guard let popup = popupInitializer else {
            return
        }
        
        // Create buttons
        let buttonOne = CancelButton(title: firstButtonTitle) {
            firstAction()
        }
        
        // This button will not the dismiss the dialog
        let buttonTwo = DefaultButton(title: secondButtonTitle, dismissOnTap: true) {
            secondAction()
        }
        
        // Add buttons to dialog
        popup.addButtons([buttonOne, buttonTwo])
        
        PopupHandler.customizeAppearance(popup: popup, blurEffect: blurEffect, isCentered: isCentered)
        
        // Present dialog
        controller.present(popup, animated: true, completion: nil)
    }
    
    
    class func showTwoButtonsPopupWithReturnController(title: String, message: String, isCentered: Bool = false,
                                   image: UIImage? = nil,
                                   titleImageFrame: CGRect = .zero,
                                   on controller:UIViewController, blurEffect: Bool = false,
                                   firstButtonTitle: String, firstAction: @escaping ActionHandler,
                                   secondButtonTitle: String, secondAction: @escaping ActionHandler) -> PopupDialog? {
        
        // Create the dialog
        var popupInitializer: PopupDialog?
        var formattedMessage: String? = message
        if message == "" {
            formattedMessage = nil
        }
        if image == nil {
            popupInitializer = PopupDialog(title: title, message: formattedMessage, buttonAlignment: .horizontal, transitionStyle: .zoomIn, tapGestureDismissal: false, panGestureDismissal: false)
        } else {
            let formattedTitle = PopupHandler.stringStartingWithImage(image: image, text: title, titleImageFrame: titleImageFrame)
            popupInitializer = PopupDialog(title: formattedTitle, message: formattedMessage, buttonAlignment: .horizontal, transitionStyle: .zoomIn, tapGestureDismissal: false, panGestureDismissal: false)
        }
        
        guard let popup = popupInitializer else {
            return nil
        }
        
        // Create buttons
        let buttonOne = CancelButton(title: firstButtonTitle) {
            firstAction()
        }
        
        // This button will not the dismiss the dialog
        let buttonTwo = DefaultButton(title: secondButtonTitle, dismissOnTap: true) {
            secondAction()
        }
        
        // Add buttons to dialog
        popup.addButtons([buttonOne, buttonTwo])
        
        PopupHandler.customizeAppearance(popup: popup, blurEffect: blurEffect, isCentered: isCentered)
        
        return popup
        // Present dialog
    }
    
    class func addController(controller: UIViewController, presentingController: UIViewController) {
        let popup = PopupDialog(viewController: controller, transitionStyle: .bounceUp)
        presentingController.present(popup, animated: true, completion: nil)
    }
}

extension PopupHandler {
    private class func customizeAppearance(popup: PopupDialog, blurEffect: Bool, isCentered: Bool = false) {
        // Get the default view controller and cast it
        // Unfortunately, casting is necessary to support Objective-C
        let popupController = popup.viewController as! PopupDialogDefaultViewController
        if isCentered {
            popupController.titleTextAlignment = .center
        } else {
            popupController.titleTextAlignment = .left
        }
        popupController.titleColor = .alertTitleColor
        
        if isCentered {
            popupController.messageTextAlignment = .center
        } else {
            popupController.messageTextAlignment = .left
        }
        popupController.messageColor = .alertMessageColor
        popupController.titleFont = UIFont.appThemeMediumWith(size: 16.0)
         
        popupController.messageFont = UIFont.appThemeLightWith(size: 14.0)
        
        let buttonAppearance = DefaultButton.appearance()
        buttonAppearance.titleFont = UIFont.appThemeMediumWith(size: 16.0)
        buttonAppearance.titleColor = .theme
        
        let cancelButtonAppearance = CancelButton.appearance()
        cancelButtonAppearance.titleFont = UIFont.appThemeMediumWith(size: 16.0)
        cancelButtonAppearance.titleColor = .theme
        
        let overlayAppearance = PopupDialogOverlayView.appearance()
        if blurEffect {
            overlayAppearance.color = .white
        } else {
            overlayAppearance.color = .black
        }
        overlayAppearance.blurRadius = 20
        overlayAppearance.blurEnabled = blurEffect
        overlayAppearance.liveBlurEnabled = false
        overlayAppearance.opacity = 0.5
        
        let containerAppearance = PopupDialogContainerView.appearance()
        containerAppearance.cornerRadius = 10
    }
    
    private class func stringStartingWithImage(image: UIImage?, text: String, titleImageFrame: CGRect = .zero) -> NSAttributedString {
        // create an NSMutableAttributedString that we'll append everything to
        let attribute = [NSAttributedString.Key.font: UIFont.appThemeMediumWith(size: 16.0),
                         NSAttributedString.Key.foregroundColor: UIColor.alertTitleColor]
        let fullString = NSMutableAttributedString(string: "  " + text, attributes: attribute)
        
        // create our NSTextAttachment
        let imageAttachment = NSTextAttachment()
        imageAttachment.image = image
        var iconsSize = CGRect(x: 0, y: -2, width: 15, height: 15)
        if titleImageFrame != .zero {
            iconsSize = titleImageFrame
        }
        imageAttachment.bounds = iconsSize
        
        // wrap the attachment in its own attributed string so we can append it
        let imageString = NSMutableAttributedString(attachment: imageAttachment)
        
        // add the NSTextAttachment wrapper to our full string, then add some more text.
        imageString.append(fullString)
        
        return imageString
    }
}
