//
//  BBQUserDefaults.swift
//  Barbeque Nation
//
//  Created by Chandan Singh on 05/08/19.
//  Copyright © 2019 Mindtree. All rights reserved.
//

import Foundation
import UIKit

class BBQUserDefaults {
    
    private let accessTokenKey = "accesstoken"
    private let refreshTokenKey = "refreshtoken"
    private let expTimeKey = "expires_in"
    private let signUpScreenCount = "signupcount"
    private let phoneNumberCode = "phonecode"
    private let userNameKey = "UserName"
    private let userLoyaltyPointsKey = "UserLoyaltyPoints"
    private let userDenied = "userDenied"
    private let guestUserStatus = "guestUserStatus"
    private let guestUserStatusAPI = "guestUserStatusAPI"
    private let userRatingStatus = "userRatingStatus"
    private let welcomescreen = "welcomescreen"
    private let currentSelectedLocationKey = "currentSelectedLocation"
    private let delivertCurrentSelectedLocationKey = "delivertCurrentSelectedLocation"
    private let welcomeTimestamp = "welcomeTimestamp"
    private let keyAppCheckedVersion = "keyAppCheckedVersion"
    private let firebaseTokenKey = "firebaseToken"
    private let firebaseLocalTokenKey = "firebaseLocalToken"
    private let accessTokenDateKey = "accessTokenDate"
    private let latitudeKey = "latitudeKey"
    private let longitudeKey = "longitudeKey"
    private let currentLatitudeKey = "currentLatitudeKey"
    private let currentLongitudeKey = "currentLongitudeKey"
    private let branchIdKey = "branchIdKey"
    private let deliveryBranchIdKey = "deliveryBranchIdKey"
    private let customerIdKey = "customerIdKey"
    private let customPaymentEmailKey = "customPaymentEmailKey"
    private let CorporateEmailKey = "CorporateEmailKey"
    private let referralCodeKey = "referralCodeKey"
    private let estimatedTotalAmountKey = "estimatedTotalAmountKey"
    private let multiBrandIntroCountKey = "multiBrandIntroCountKey"
    private let referalCodeToShare = "referalCodeToShare"
    private let referalCodeURLToShare = "referalCodeURLToShare"




    var defaults: UserDefaults = UserDefaults.standard
    
    private var accessTokenStartDate : Date?
    
    static let sharedInstance: BBQUserDefaults = {
        let instance = BBQUserDefaults()
        // setup code
        return instance
    }()
    
    private init() { }
    
    private func token(key tokenKey: String) -> String {
        if let token = defaults.string(forKey: tokenKey) {
            return token
        } else {
            return ""
        }
    }
    
    var firebaseToken: String {
        set {
            defaults.set(newValue, forKey: firebaseTokenKey)
            defaults.synchronize()
        }
        get {
            return token(key: firebaseTokenKey)
        }
    }
    
    var firebaseLocalToken: String {
        set {
            defaults.set(newValue, forKey: firebaseLocalTokenKey)
            defaults.synchronize()
        }
        get {
            return token(key: firebaseLocalTokenKey)
        }
    }
    
    var accessToken: String {
        set {
            defaults.set(newValue, forKey: accessTokenKey)
            defaults.set(false, forKey: guestUserStatus)
            defaults.set(Date(), forKey: accessTokenDateKey)
            defaults.synchronize()
        }
        get {
            return token(key: accessTokenKey)
        }
    }
    
    var expiryDuration: Int64 {
        set {
            defaults.set(newValue, forKey: expTimeKey)
        } get {
            return Int64(defaults.integer(forKey: expTimeKey))
        }
    }
    
    var accessTokenDate: Date {
        set {
            defaults.set(newValue, forKey: accessTokenDateKey)
        } get {
            return defaults.value(forKey: accessTokenDateKey) as? Date ?? Date()
        }
    }
    
    var welcomeScreen: String {
        set {
            defaults.set(newValue, forKey: welcomescreen)
            defaults.synchronize()
        }
        get {
            return token(key: welcomescreen)
        }
    }
    
    var phoneCode: String {
        set {
            defaults.set(newValue, forKey: phoneNumberCode)
            defaults.synchronize()
        }
        get {
            return token(key: phoneNumberCode)
        }
    }
    
    var signupCount: Int {
        set {
            defaults.set(newValue, forKey: signUpScreenCount)
            defaults.synchronize()
        }
        get {
            return defaults.integer(forKey: signUpScreenCount)
        }
    }
    
    var refreshToken: String {
        set {
            defaults.set(newValue, forKey: refreshTokenKey)
            defaults.synchronize()
        }
        get {
            return token(key: refreshTokenKey)
        }
    }
    
    var UserName: String {
        set {
            defaults.set(newValue, forKey: userNameKey)
            defaults.synchronize()
        }
        get {
            return token(key: userNameKey)
        }
    }
    
    
    var YourReferalCode: String {
        set {
            defaults.set(newValue, forKey: referalCodeToShare)
            defaults.synchronize()
        }
        get {
            return token(key: referalCodeToShare)
        }
    }
    
    var YourReferalCodeURL: String {
        set {
            defaults.set(newValue, forKey: referalCodeURLToShare)
            defaults.synchronize()
        }
        get {
            return token(key: referalCodeURLToShare)
        }
    }
    
    var CurrentSelectedLocation: String {
        set {
            defaults.set(newValue, forKey: currentSelectedLocationKey)
            defaults.synchronize()
        }
        get {
            return token(key: currentSelectedLocationKey)
        }
    }
    
    var DeliveryCurrentSelectedLocation: String {
        set {
            defaults.set(newValue, forKey: delivertCurrentSelectedLocationKey)
            defaults.synchronize()
        }
        get {
            return defaults.string(forKey: delivertCurrentSelectedLocationKey) ?? CurrentSelectedLocation
        }
    }
    
    var UserLoyaltyPoints: Int {
        set {
            defaults.set(newValue, forKey: userLoyaltyPointsKey)
            defaults.synchronize()
        }
        get {
            return defaults.integer(forKey: userLoyaltyPointsKey)
        }
    }
    
    var userDeniedLocation:Bool {
        set {
            defaults.set(newValue, forKey: userDenied)
            defaults.synchronize()
        }
        get {
            return defaults.bool(forKey: userDenied)
        }
    }
    
    var isGuestUser: Bool {
        set {
            defaults.set(newValue, forKey: guestUserStatus)
            defaults.synchronize()
        }
        get {
            return defaults.bool(forKey: guestUserStatus)
        }
    }
    
    var isGuestUserAPI: Bool {
        set {
            defaults.set(newValue, forKey: guestUserStatusAPI)
            defaults.synchronize()
        }
        get {
            return defaults.bool(forKey: guestUserStatusAPI)
        }
    }
    
    var isUserRatedBBQApp : Bool {
        set {
            defaults.set(newValue, forKey: userRatingStatus)
            defaults.synchronize()
        }
        get {
            return defaults.bool(forKey: userRatingStatus)
        }
    }
    
    var welcomeTimeStamp: String {
        set {
            defaults.set(newValue, forKey: welcomeTimestamp)
            defaults.synchronize()
        }
        get {
            return token(key: welcomeTimestamp)
        }
    }
    
    var appCheckedVersion: String {
        set {
            defaults.set(newValue, forKey: keyAppCheckedVersion)
            defaults.synchronize()
        }
        get {
            return token(key: keyAppCheckedVersion)
        }
    }
    
    var latitudeValue: Double {
        set {
            defaults.set(newValue, forKey: latitudeKey)
            defaults.synchronize()
        }
        get {
            return Double(defaults.double(forKey: latitudeKey))
        }
    }
    
    var currentLatitudeValue: Double {
        set {
            defaults.set(newValue, forKey: currentLatitudeKey)
            defaults.synchronize()
        }
        get {
            return Double(defaults.double(forKey: currentLatitudeKey))
        }
    }
    
    var longitudeValue: Double {
        set {
            defaults.set(newValue, forKey: longitudeKey)
            defaults.synchronize()
        }
        get {
            return Double(defaults.double(forKey: longitudeKey))
        }
    }
    
    var currentLongitudeValue: Double {
        set {
            defaults.set(newValue, forKey: currentLongitudeKey)
            defaults.synchronize()
        }
        get {
            return Double(defaults.double(forKey: currentLongitudeKey))
        }
    }
    
    var customerId: String {
        set {
            defaults.set(newValue, forKey: customerIdKey)
            defaults.synchronize()
        }
        get {
            return token(key: customerIdKey)
        }
    }
    
    var customPaymentEmail: String {
        set {
            defaults.set(newValue, forKey: customPaymentEmailKey)
            defaults.synchronize()
        }
        get {
            return token(key: customPaymentEmailKey)
        }
    }
    
    var branchIdValue: String {
        set {
            defaults.set(newValue, forKey: branchIdKey)
            defaults.synchronize()
        }
        get {
            return defaults.string(forKey: branchIdKey) ?? "0"
        }
    }
    
    var DeliveryBranchIdValue: String {
        set {
            defaults.set(newValue, forKey: deliveryBranchIdKey)
            defaults.synchronize()
        }
        get {
            return defaults.string(forKey: deliveryBranchIdKey) ?? branchIdValue
        }
    }
    
    var MultiBrandIntroCount: Int {
        set {
            defaults.set(newValue, forKey: multiBrandIntroCountKey)
            defaults.synchronize()
        }
        get {
            return defaults.integer(forKey: multiBrandIntroCountKey)
        }
    }
    
    var CanMultiBrandIntroOpen: Bool{
        get{
            if MultiBrandIntroCount < 5{
                MultiBrandIntroCount += 1
                return true
            }
            return false
        }
    }
    
    var signUpReferralCode: String {
        set {
            defaults.set(newValue, forKey: referralCodeKey)
            defaults.synchronize()
        }
        get {
            return token(key: referralCodeKey)
        }
    }
    //estimatedTotalAmount
    var bookingEstimatedTotal:Double{
        set{
            defaults.set(newValue, forKey: estimatedTotalAmountKey)
            defaults.synchronize()
        }get{
            return Double(defaults.double(forKey: estimatedTotalAmountKey))
        }
    }
    var savedCorporateEmailValue:String{
        set{
            defaults.set(newValue, forKey: CorporateEmailKey)
            defaults.synchronize()
        }
        get{
            return token(key: CorporateEmailKey)
        }
    }
    func clearAppDefaults() {
        if let bundleIdentifier = Bundle.main.bundleIdentifier, defaults.persistentDomain(forName: bundleIdentifier) != nil{
            defaults.removePersistentDomain(forName: bundleIdentifier)
            defaults.synchronize()
        }
        //AnalyticsHelper.shared.resetMoEngageuser()
        self.isGuestUser = true
    }
    
    func getBottomSafeAreaHeight() -> CGFloat {
        var bottomPadding: CGFloat = 0.0
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            bottomPadding = window?.safeAreaInsets.bottom ?? 0.0
        }
        return bottomPadding
    }
}
func getCurrency() -> String{
    return "₹"
}
