//
 //  Created by Ajith CP on 22/08/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQImagePicker.swift
 //

import Foundation
import UIKit

public protocol BBQImagePickerDelegate: AnyObject {
    /// Returns the image selected from camera/ photo library.
    /// - Parameters:
    ///     - image: The UIImage selected/captured
    func didSelect(image: UIImage?, imgExtension: String?)
}

open class BBQImagePicker: NSObject {
   
    private let pickerController: UIImagePickerController
    private weak var presentationController: UIViewController?
    private weak var delegate: BBQImagePickerDelegate?
    
    private var imageExtension : String?

    public init(presentationController: UIViewController, delegate: BBQImagePickerDelegate) {
        self.pickerController = UIImagePickerController()

        super.init()

        self.presentationController = presentationController
        self.delegate = delegate
    
        self.pickerController.delegate = self
        self.pickerController.allowsEditing = true
        self.pickerController.mediaTypes = ["public.image"]
    }
    
    private func action(for type: UIImagePickerController.SourceType, title: String) -> UIAlertAction? {
        guard UIImagePickerController.isSourceTypeAvailable(type) else {
            return nil
        }
        
        return UIAlertAction(title: title, style: .default) { [unowned self] _ in
            self.pickerController.sourceType = type
            self.presentationController?.present(self.pickerController, animated: true)
        }
    }
    
    public func present(from sourceView: UIView) {

        /**
            The presenting picker view from view controller
        */
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
         
        if let action = self.action(for: .camera, title: kTakePhoto) {
            alertController.addAction(action)
        }
//        if let action = self.action(for: .savedPhotosAlbum, title: kCameraRoll) {
//            alertController.addAction(action)
//        }
        if let action = self.action(for: .photoLibrary, title: kPhotoLibrary) {
            alertController.addAction(action)
        }
        
        alertController.addAction(UIAlertAction(title: kCancelString, style: .cancel, handler: nil))

        if UIDevice.current.userInterfaceIdiom == .pad {
            alertController.popoverPresentationController?.sourceView = sourceView
            alertController.popoverPresentationController?.sourceRect = sourceView.bounds
            alertController.popoverPresentationController?.permittedArrowDirections = [.down, .up]
        }

        self.presentationController?.present(alertController, animated: true)
    }
    
    private func pickerController(_ controller: UIImagePickerController, didSelect image: UIImage?) {
        controller.dismiss(animated: true, completion: nil)
        self.delegate?.didSelect(image: image, imgExtension: self.imageExtension)
    }
    
    private func imageExtensionFrom(info : [UIImagePickerController.InfoKey: Any] ) -> String {
//        let assetPath = info[UIImagePickerController.InfoKey.imageURL] as! NSURL
//        if (assetPath.absoluteString?.hasSuffix("JPG"))! {
//            return ".JPG"
//        }
//        else if (assetPath.absoluteString?.hasSuffix("PNG"))! {
//            return ".PNG"
//        }
//        else if (assetPath.absoluteString?.hasSuffix("GIF"))! {
//            return ".GIF"
//        }
//        else {
//            return ".Unknown"
//        }
        // Always append jpg with file name.
        return ".JPG"
        
    }
}

extension BBQImagePicker: UIImagePickerControllerDelegate {
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.pickerController(picker, didSelect: nil)
    }

    public func imagePickerController(_ picker: UIImagePickerController,
                                      didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        guard let image = info[.editedImage] as? UIImage else {
            return self.pickerController(picker, didSelect: nil)
        }
        self.imageExtension = self.imageExtensionFrom(info: info)
        self.pickerController(picker, didSelect: image)
    }
}

extension BBQImagePicker: UINavigationControllerDelegate {

}
