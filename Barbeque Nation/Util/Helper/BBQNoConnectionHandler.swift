////
 //  Created by Chandan Singh on 19/08/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019 .
 //  All rights reserved.
 //  Last modified BBQNoConnectionHandler.swift
 //

import UIKit

class BBQNoConnectionHandler {
    
    private static let noInternetController: BBQNoInternetViewController = BBQNoInternetViewController(nibName: "BBQNoInternetViewController", bundle: nil)
    
    private static var isNetworkViewDisplayed = false
    
    class func noInternetScreen(show: Bool) {
        let keyWindow = UIApplication.shared.windows.last
        
        guard let window = keyWindow else {
            return
        }
        
        if show {
            noInternetController.view.frame = window.frame
            window.addSubview(noInternetController.view)
            isNetworkViewDisplayed = true
        } else {
            if let lastView = window.subviews.last, lastView.isKind(of: UIView.self), isNetworkViewDisplayed {
                isNetworkViewDisplayed = false
                lastView.removeFromSuperview()
            }
        }
    }
}
