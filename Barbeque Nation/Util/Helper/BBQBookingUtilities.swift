//
 //  Created by Sridhar on 12/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQBookingUtilities.swift
 //

import Foundation
import UIKit

class BBQBookingUtilies {
    
    static let shared = BBQBookingUtilies()
    
    private init() { }
    
    //MARK: - Date Formatter to display Starts
    func getTodayDateForAPI() -> String {
        let today = Date()
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = Constants.BBQBookingStatus.todaytomorrowDateTimeFormat//"yyyy-MM-dd"
        let currentselectedDate = dateformatter.string(from: today.self)
        return currentselectedDate
    }
    
    func getTomorrowDateForAPI() -> String {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = Constants.BBQBookingStatus.todaytomorrowDateTimeFormat//"yyyy-MM-dd"
        let tomorrowString = dateformatter.string(from: Date.tomorrow)
        return tomorrowString
    }
    
    func getTodayDate() -> String {
        let today = Date()
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = Constants.BBQBookingStatus.todaytomorrowDisplayTimeFormat//"dd MMM, EEEE"
        let currentselectedDate = dateformatter.string(from: today.self)
        return currentselectedDate
    }
    
    func getTomorrowDate() -> String {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = Constants.BBQBookingStatus.todaytomorrowDisplayTimeFormat//"dd MMM, EEEE"
        let tomorrowString = dateformatter.string(from: Date.tomorrow)
        return tomorrowString
    }
    
    func getDisplayTimeIn12HourFormat (_ fullhourTime: String) -> String {
        let dateAsString = fullhourTime // need to guard
        guard dateAsString != "" else {
            return ""
        }
        let separatedString = dateAsString.components(separatedBy: ":")
        let dateStringValue = separatedString[0]+":"+separatedString[1]
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.BBQBookingStatus.timeTwentyFourHourFormat//"HH:mm"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let date = dateFormatter.date(from: dateStringValue)
        dateFormatter.dateFormat = Constants.BBQBookingStatus.timeTwelveHourFormat//"hh:mma"
        let changedDate = dateFormatter.string(from: date!).lowercased()
        
        return changedDate
    }
    
    //MARK: - Date Formatter to display Ends
    func getDisplayImageForFoodType (_ foodType:String) -> String {
        if foodType == Constants.BBQBookingStatus.nonVegFoodType//"NONVEG"
        {
            return "nonvegIcon.png"
        }
        else if foodType == Constants.BBQBookingStatus.vegFoodType//"VEG"
        {
            return "vegIcon.png"
        }
        else
        {
            return "icon_kid.png"
        }
    }
    
    func getDisplayColorForFoodType (_ foodType:String) -> UIColor? {
        if foodType == Constants.BBQBookingStatus.nonVegFoodType//"NONVEG"
        {
            return Constants.App.themeColor.orange//UIColor(red:1.00, green:0.39, blue:0.00, alpha:1.0)
        }
        else if foodType == Constants.BBQBookingStatus.vegFoodType//"VEG"
        {
            return Constants.App.themeColor.green//UIColor(red:0.41, green:0.78, blue:0.00, alpha:1.0)
        }
        else
        {
            return Constants.App.themeColor.grey//UIColor(red:0.21, green:0.25, blue:0.27, alpha:1.0)
        }
    }
    
    func formattedBookingDate(from dateSelected: String? = nil, dateType: BBQBookingDateType) -> String {
        var bookingDate = Date()
        
        switch dateType {
        case BBQBookingDateType.Tomorrow:
            bookingDate = Date().dayAfter
        case BBQBookingDateType.Custom:
            bookingDate = self.stringToDate(dateString: dateSelected ?? "")
        default:
            bookingDate = Date()
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM, EEEE"
        let formattedString = dateFormatter.string(from: bookingDate)
        
        return formattedString
    }
    
    func stringToDate(dateString: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        let date = dateFormatter.date(from: dateString)!
        return date
    }
    
    func formattedReservationDate(from dateSelected: String? = nil, dateType: BBQBookingDateType) -> String {
        switch dateType {
        case BBQBookingDateType.Today:
            return getTodayDateForAPI()
            
        case BBQBookingDateType.Tomorrow:
            return getTomorrowDateForAPI()
            
        case BBQBookingDateType.Custom:
            return dateSelected ?? ""
        }
    }
    
    func getDateType(date: String) -> BBQBookingDateType {
        let dateData = BBQBookingUtilies.shared.stringToDate(dateString: date)
        if Calendar.current.isDateInToday(dateData) {
            return .Today
        } else if Calendar.current.isDateInTomorrow(dateData) {
            return .Tomorrow
        }
        return .Custom
    }
    
    func isLocationAlreadySet() -> Bool {
        if BBQUserDefaults.sharedInstance.CurrentSelectedLocation != "" &&
            BBQUserDefaults.sharedInstance.branchIdValue != "" {
            return true
        }
        return false
    }
}
