//
 //  Created by Mahmadsakir on 31/08/21
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2021.
 //  All rights reserved.
 //  Last modified PayUPaymentHelper.swift
 //

import Foundation
import UIKit
import CommonCrypto
import PayUCheckoutProBaseKit
import PayUCheckoutProKit
import PayUBizCoreKit
import PayUParamsKit


class PayUPaymentHelper: NSObject{
    private override init() {
        super.init()
    }

    static let shared = PayUPaymentHelper()
    var merchantKey = "HFtCII"
    var merchantSalt = "TkloVSOd67QY2uF13J4enXKrWy9Bb1Xw"
    var merchantAccessKey = "E5ABOXOWAAZNXB6JEF5Z"
    var merchantPrivateKey = "e425e539233044146a2d185a346978794afd7c66"
    var transactionId = ""

    func getPayUParams(transactionId: String?, amount: String) -> PayUPaymentParam {
        self.transactionId = txnId()
        let paymentParam = PayUPaymentParam(key: merchantKey,
                                transactionId: self.transactionId,
                                amount: amount,
                                productInfo: "Barbequenation",
                                firstName: BBQUserDefaults.sharedInstance.UserName,
                                email: SharedProfileInfo.shared.profileData?.email ?? "",
                                phone: SharedProfileInfo.shared.profileData?.mobileNumber ?? "",
                                surl: "https://erprep.bnhl.in/DeliveryAPI/api/PayUPayment/payments_success",
                                furl: "https://erprep.bnhl.in/DeliveryAPI/api/PayUPayment/payments_failed",
                                environment: Environment.test)

        paymentParam.additionalParam[PaymentParamConstant.udf1] = "udf11"
        paymentParam.additionalParam[PaymentParamConstant.udf2] = "udf22"
        paymentParam.additionalParam[PaymentParamConstant.udf3] = "udf33"
        paymentParam.additionalParam[PaymentParamConstant.udf4] = "udf44"
        paymentParam.additionalParam[PaymentParamConstant.udf5] = "udf55"
        paymentParam.additionalParam[PaymentParamConstant.merchantAccessKey] = merchantAccessKey
        paymentParam.userCredential = merchantKey+":\(BBQUserDefaults.sharedInstance.customerId)"
        return paymentParam
    }


    func openPayment(on rootVC: UIViewController, params: PayUPaymentParam) {
        let config = PayUCheckoutProConfig()
        config.autoSelectOtp = true
        config.merchantLogo = UIImage(named: "BBQ_logo_welcome")
        config.customiseUI(primaryColor: .themeYello, secondaryColor: .white)
        config.merchantName = "Barbequenation"
        config.merchantResponseTimeout = 300
        config.surePayCount = 1

        var preferredPaymentModes: [PaymentMode] = [PaymentMode]()
        preferredPaymentModes.append(PaymentMode(paymentType: .savedCard))
        preferredPaymentModes.append(PaymentMode(paymentType: .ccdc))
        preferredPaymentModes.append(PaymentMode(paymentType: .netBanking))
        preferredPaymentModes.append(PaymentMode(paymentType: .upi))
        preferredPaymentModes.append(PaymentMode(paymentType: .wallet))

        config.paymentModesOrder = preferredPaymentModes

        PayUCheckoutPro.open(on: rootVC, paymentParam: params, config: config, delegate: self)
    }

}

extension PayUPaymentHelper: PayUCheckoutProDelegate{
    func onPaymentSuccess(response: Any?) {

    }

    func onPaymentFailure(response: Any?) {

    }

    func onPaymentCancel(isTxnInitiated: Bool) {

    }

    func onError(_ error: Error?) {

    }

    func generateHash(for param: DictOfString, onCompletion: @escaping PayUHashGenerationCompletion) {
        let commandName = (param[HashConstant.hashName] ?? "")
        let hashStringWithoutSalt = (param[HashConstant.hashString] ?? "")
        // get hash for "commandName" from server
        // get hash for "hashStringWithoutSalt" from server



        // After fetching hash set its value in below variable "hashValue"
        var hashValue = ""
        if commandName == HashConstant.mcpLookup {
            hashValue = hmacsha1(of: hashStringWithoutSalt, secret: merchantPrivateKey)
        } else {
            hashValue = sha512Hex(string: hashStringWithoutSalt  + merchantSalt)
        }
        onCompletion([commandName : hashValue])
    }


}

extension PayUPaymentHelper{
    func sha512Hex(string: String) -> String {
        PayUDontUseThisClass().getHash(string)
    }

    func hmacsha1(of string: String, secret: String) -> String {
        PayUDontUseThisClass.hmacsha1(string, secret: secret)
    }

    func txnId() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyMMddHHmmss"
        let txnID = "iOS" + formatter.string(from: Date())
        return txnID
    }
}
