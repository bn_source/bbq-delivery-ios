//
 //  Created by Rabindra L on 11/09/19
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
 //  All rights reserved.
 //  Last modified BBQPhoneNumberValidation.swift
 //

import Foundation
import PhoneNumberKit

class BBQPhoneNumberValidation {
    class func isValidNumber(number: String, for countryCode:String) -> Bool {
        switch countryCode {
        case Constants.CountryCode.india:
            return (number.count == Constants.CountryCode.ValidLength.india)
        case Constants.CountryCode.USA:
            return (number.count == Constants.CountryCode.ValidLength.USA)
        case Constants.CountryCode.UAE:
            return (number.count == Constants.CountryCode.ValidLength.UAE)
        case Constants.CountryCode.oman:
            return (number.count == Constants.CountryCode.ValidLength.oman)
        case Constants.CountryCode.malaysia:
            return (number.count == Constants.CountryCode.ValidLength.malaysia)
        case Constants.CountryCode.nepal:
            return (number.count == Constants.CountryCode.ValidLength.nepal)
        case Constants.CountryCode.UK:
            return (number.count == Constants.CountryCode.ValidLength.UK)
        case Constants.CountryCode.singapore:
            return (number.count == Constants.CountryCode.ValidLength.singapore)
        case Constants.CountryCode.saudiArabia:
            return (number.count == Constants.CountryCode.ValidLength.saudiArabia)
        default:
            return (number.count <= Constants.CountryCode.ValidLength.others)
        }
    }
    
    
   class func getValidNumberOfDigitsForCountryCode(code: String) -> Int {
        switch  (code) {
        case Constants.CountryCode.india:
            return Constants.CountryCode.ValidLength.india
        case Constants.CountryCode.USA:
            return  Constants.CountryCode.ValidLength.USA
        case Constants.CountryCode.UAE:
            return  Constants.CountryCode.ValidLength.UAE
        case Constants.CountryCode.oman:
            return  Constants.CountryCode.ValidLength.oman
        case Constants.CountryCode.malaysia:
            return  Constants.CountryCode.ValidLength.malaysia
        case Constants.CountryCode.nepal:
            return  Constants.CountryCode.ValidLength.nepal
        case Constants.CountryCode.UK:
            return  Constants.CountryCode.ValidLength.UK
        case Constants.CountryCode.singapore:
            return  Constants.CountryCode.ValidLength.singapore
        case Constants.CountryCode.saudiArabia:
            return  Constants.CountryCode.ValidLength.saudiArabia
        default:
            return Constants.CountryCode.ValidLength.others
        }
    }
}

class PhoneNumberCode {
    static let phoneNumberKit: PhoneNumberKit = {
        let instance = PhoneNumberKit()
        return instance
    }()
    
    class func forCountryCode(code:String) -> UInt64 {
        return (phoneNumberKit.countryCode(for: code) ?? 0)
    }
    
//    class func validateTheNumber(number:String, code:String)-> Bool {
//        do {
//            let phoneNumber = try phoneNumberKit.parse("\(code)\(number)")
//
//            return true
//        }
//        catch {
//            return false
//        }
//    }
}

