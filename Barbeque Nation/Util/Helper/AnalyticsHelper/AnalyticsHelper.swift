//
 //  Created by Mahmadsakir on 31/12/20
 //  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2020.
 //  All rights reserved.
 //  Last modified AnalyticsHelper.swift
 //

import UIKit
import FirebaseAnalytics

class AnalyticsHelper: NSObject {
    
    //MARK: - Old Google Events
    enum GoogleScreenTrack: String {
        //MARK: - Google Screen Track Events
        case Login_Screen = "A_Login_Screen"
        case Otp_Screen = "B_Otp_Screen"
        case Reservation_Home_Screen = "H_Reservation_Home_Screen"
        case Reservation_Screen = "R_Reservation_Screen"
        case Delivery_Home_Screen = "D_Delivery_Home_Screen"
        case Delivery_Cart_Screen = "C_Delivery_Cart_Screen"
        case Delivery_Address_Screen = "CA_Delivery_Address_Screen"
        case Delivery_Coupon_Screen = "CC_Delivery_Coupon_Screen"
        case Manage_Address_Screen = "MA_Manage_Address_Screen"
        case Add_Edit_Address_Screen = "AE_Add_Edit_Address_Screen"
        case Happiness_Cart_Screen = "HC_Happiness_Cart_Screen"
        case My_Reservation_Screen = "RH_My_Reservation_Screen"
        case My_Happiness_Card_Screen = "HH_My_Happiness_Card_Screen"
        case My_Happiness_Card_Detail_Screen = "HD_My_Happiness_Card_Detail_Screen"
        case Delivery_History_Screen = "DH_Delivery_History_Screen"
        case Special_Offer_Screen = "SO_Special_Offer_Screen"
        case Profile_Screen = "P_Profile_Screen"
        case Help_Support_Screen = "HS_Help_Support_Screen"
        case About_Us_Screen = "AU_About_Us_Screen"
        case Outlet_Info_Screen = "OI_Outlet_Info_Screen"
        case After_Dining_Payment_Screen = "AP_After_Dining_Payment_Screen"
        case Delivery_Item_Search_Screen = "DS_Delivery_Item_Search_Screen"
        case Order_Status_Screen = "OS_Order_Status_Screen"
        case Order_Feedback_Screen = "FB_Order_Feedback_Screen"
        case Delete_Otp_Screen = "Delete_Otp_Screen"
    }
    
    enum GoogleEvent: String {
        //MARK: - Google Analytics Old Events
        case reservation_menu = "reservation_menu" //Reservation/Menu
        case visited_delivery_home = "visited_delivery_home" //VisitedDeliveryHome
        case delivery_home_event = "delivery_home_event" //DeliveryHome
        case added_address = "added_address" //DeliveryAddressAdd
        case added_address_delivery = "added_address_delivery" //Added Address
        case add_address_event = "add_address_event" //AddAddressEvent
        case pay_clicked = "pay_clicked" //DeliveryClickedPay
        case pay_clicked_event = "pay_clicked_event" //DeliveryClickedPayEvent
        case visited_delivery_cart = "visited_delivery_cart" //Visited Delivery Cart
        case visited_delivery_home_screen = "visited_delivery_home_screen" //Visited Delivery Home Screen
        case delivery_clear_cart = "delivery_clear_cart" //Delivery Cleared Cart
        case delivery_add_to_cart = "delivery_add_to_cart" //Delivery Add To Cart
        case delivery_remove_coupon_happiness_card = "delivery_remove_coupon_happiness_card" //Delivery Remove Coupon Happiness card----
        case delivery_remove_smiles = "delivery_remove_smiles" //Delivery Remove Smiles
        case delivery_use_all_smiles = "delivery_use_all_smiles" //Delivery Use All Smiles
        case takeaway_date_selected = "takeaway_date_selected" //TakeAwayDateSelected
        case takeaway_date_selection_event = "takeaway_date_selection_event" //TakeAwayDateSelectionEvent
        case explored_menu = "explored_menu" //ExploredMenu
        case explored_menu_event = "explored_menu_event" //ExploredMenuEvent
        case added_to_cart_delivery = "added_to_cart_delivery" //AddedToCart
        case added_to_cart_event = "added_to_cart_event" //AddedToCartEvent
        case add_smiles_delivery = "add_smiles_delivery" //AddSmiles
        case add_smiles_delivery_event = "add_smiles_delivery_event" //AddSmilesEvent
        case delivery_order_success = "delivery_order_success" //DeliveryOrderSuccess
        case delivery_payment_success = "delivery_payment_success" //DeliveryPaymentSuccess
        case delivery_payment_failed = "delivery_payment_failed" //DeliveryPaymentFailed
        case delivery_order_failed = "delivery_order_failed" //DeliveryOrderFailed
        case delivery_address_search_button_clicked = "delivery_address_search_button_clicked" //Delivery Address Search Button Clicked
        case address_places_api_called = "address_places_api_called" //Delivery Address Places API Called
        case delivery_location_search_button_clicked = "delivery_location_search_button_clicked" //Delivery Location Search Button Clicked---
        case location_places_api_called = "location_places_api_called" //Delivery Location Places API Called
        case delivery_voucher_verify_success = "delivery_voucher_verify_success" //Delivery Happiness Card Verify Success---
        case delivery_coupon_verify_success = "delivery_coupon_verify_success" //Delivery Coupon Verify Success
        case delivery_voucher_update_payment_success = "delivery_voucher_update_payment_success" //Delivery Happiness Card Update Payment Success
        case delivery_coupon_update_payment_success = "delivery_coupon_update_payment_success" //Delivery Coupon Update Payment Success
        case delivery_voucher_verify_fail = "delivery_voucher_verify_fail" //Delivery Happiness Card Verify Failure
        case delivery_coupon_verify_fail = "delivery_coupon_verify_fail" //Delivery Coupon Verify Failure
        case delivery_voucher_update_payment_fail = "delivery_voucher_update_payment_fail" //Delivery Happiness Card Update Payment Failure
        case delivery_coupon_update_payment_fail = "delivery_coupon_update_payment_fail" //Delivery Coupon Update Payment Failure
        case delivery_manual_coupon_code_apply = "delivery_manual_coupon_code_apply" //Delivery Manual Coupon Applied
        
        
        case verify_user_status = "verify_user_status" //verify_user_status
        case verify_user_success_value = "verify_user_success_value" //VerifyUserSuccess
        case verify_user_failure_value = "verify_user_failure_value" //VerifyUserFailure

        case login_status = "login_status" //user_login_status
        case login_success_value = "login_success_value" //LoginSuccess
        case login_failure_value = "login_failure_value" //LoginFailure

        case login_otp_status = "login_otp_status" //otp_status
        case login_otp_success_value = "login_otp_success_value" //LoginOTP_Success
        case login_otp_failure_value = "login_otp_failure_value" //LoginOTP_Failure
        case login_event = "login_event" //LOGIN

        case social_login_status = "social_login_status" //user_social_login_status
        case social_login_success_value = "social_login_success_value" //SocialLoginSuccess
        case social_login_failure_value = "social_login_failure_value" //SocialLoginFailure
        case social_login_event = "social_login_event" //SOCIAL_LOGIN

        //<!-- Firebase analytics events for Registration-->
        case registration_status = "registration_status" //user_registration_status
        case registration_success_value = "registration_success_value" //RegistrationSuccess
        case registration_failure_value = "registration_failure_value" //RegistrationFailure
        case registration_event = "registration_event" //REGISTRATION

        //<!-- Firebase analytics events for Booking-->
        case booking_status = "booking_status" //booking_status
        case booking_success_value = "booking_success_value" //BookingConfirm
        case booking_failure_value = "booking_failure_value" //BookingCancelled
        case booking_branch = "booking_branch" //BookingBranch

        case booking_refund_status = "booking_refund_status" //booking_refund_status
        case booking_refund_success_value = "booking_refund_success_value" //RefundSuccess
        case booking_refund_failure_value = "booking_refund_failure_value" //RefundFailed

        case booking_payment_status = "booking_payment_status" //booking_payment_status
        case booking_payment_success_value = "booking_payment_success_value" //PaymentSuccess
        case booking_payment_failure_value = "booking_payment_failure_value" //PaymentFailed

        case corporate_offers_status = "corporate_offers_status" //corporate_offer_status
        case corporate_offers_success_value = "corporate_offers_success_value" //CorporateVoucherApplied
        case corporate_offers_failure_value = "corporate_offers_failure_value" //CorporateVoucherRemoved

        case coupon_status = "coupon_status" //coupon_status
        case coupon_success_value = "coupon_success_value" //CouponApplied
        case coupon_failure_value = "coupon_failure_value" //CouponRemoved

        case vouchers_status = "vouchers_status" //vouchers_status
        case vouchers_failure_value = "vouchers_failure_value" //VoucherRemoved
        case booking_event = "booking_event" //BOOKING

        case location_status = "location_status" //location_status
        case location_success_status = "location_success_status" //LocationChanged
        case location_event = "location_event" //LOCATION

        case outlet_opening_hours = "outlet_opening_hours" //Outlet Timings
        case outlet_lunch_hours = "outlet_lunch_hours" //Lunch:
        case outlet_dinner_hours = "outlet_dinner_hours" //Dinner:
        case play_store_link = "play_store_link" //https://play.google.com/store/apps/details?id=
        case no_data_available = "no_data_available" //No data available!

        //<!-- Firebase analytics events for Profile-->
        case update_profile_status = "update_profile_status" //update_profile_status
        case update_profile__success_status = "update_profile__success_status" //UpdateProfileSuccess
        case profile__delete_status = "profile__delete_status" //UpdateProfileSuccess


        case update_mobile_status = "update_mobile_status" //update_mobile_status
        case update_mobile__success_status = "update_mobile__success_status" //UpdateMobileSuccess
        case update_mobile_failure_value = "update_mobile_failure_value" //UpdateMobileFailed

        case update_email_status = "update_email_status" //update_email_status
        case update_email__success_status = "update_email__success_status" //UpdateEmailSuccess
        case update_email_failure_value = "update_email_failure_value" //UpdateEmailFailed
        case profile_event = "profile_event" //PROFILE
        
        case clicked_dc_address_button = "clicked_dc_address_button"
    }
    
    //MARK: - General Events
    enum AnalyticsEvent: String {
        
        //MARK: - Login Page Event
        case A01 = "A01_log_in_page_loaded"
        case A02 = "A02_user_mobile_number_login"
        case A02A = "A02A_user_mobile_number_login_success"
        case A02B = "A02B_user_mobile_number_login_failure"
        case A03 = "A03_user_facebook_login"
        case A03A = "A03A_user_facebook_login_success"
        case A03B = "A03B_user_facebook_login_failure"
        case A04 = "A04_user_google_login"
        case A04A = "A04A_user_google_login_success"
        case A04B = "A04B_user_google_login_failure"
        case A05 = "A05_user_new_sign_up_start"
        case A05A = "A05A_user_new_sign_up"
        case A05AA = "A05AA_user_new_sign_up_success"
        case A05AB = "A05AB_user_new_sign_up_failure"
        case A06 = "A06_user_existing__login"
        case A07 = "A07_user_clicked_skip_button"
        
        //MARK: - OTP Page Event
        case B01 = "B01_otp_page_loaded"
        case B02 = "B02_user_verify_otp"
        case B02A = "B02A_user_verify_otp_success"
        case B02B = "B02B_user_verify_otp_failure"
        case B03 = "B03_user_resent_otp"
        case B03A = "B03A_user_resent_otp_success"
        case B03B = "B03B_user_resent_otp_failure"
        case B04 = "B04_user_otp_page_tapped_back"
    
        //MARK: - Home Page Event
        case H01 = "H01_home_page_loaded"
        case H01A = "H01A_home_page_viewed"
        case H02 = "H02_user_home_taped_promotion"
        case H03 = "H03_user_home_clicked_ubq"
        case H04 = "H04_user_home_clicked_smile"
        case H05 = "H05_user_home_clicked_header_smile"
        case H05A = "H05A_user_home_clicked_coupons"
        case H05B = "H05B_user_home_clicked_vouchers"
        case H06 = "H06_user_home_clicked_dining_table"
        case H07 = "H07_user_home_clicked_hp_card"
        case H08 = "H08_user_home_clicked_hp_buy"
        case H09 = "H09_user_home_clicked_cart"
        case H10 = "H10_user_home_clicked_notification"
        case H11 = "H11_user_home_clicked_branch_address"
        case H11A = "H11A_user_home_change_branch_address"
        case H12  = "H12_user_home_clicked_outlet_information"
        case H13 = "H13_user_home_clicked_reservation"
        case H14 = "H14_user_home_upcoming_res_display"
        case H14A = "H14A_user_home_upcoming_res_clicked"
        case H14B = "H14B_user_home_upcoming_res_navigation"
        case H14C = "H15C_user_home_upcoming_res_cab"
        case H14D = "H15D_user_home_upcoming_res_call"
        case H14E = "H15E_user_home_upcoming_res_paynow"
        case H15 = "H15_user_home_order_status_track_clicked"
        case H16 = "H16_user_home_order_status_rate_clicked"

    
        //MARK: - Reservation Table Page Event
        case R01 = "R01_reservation_date_page_loaded"
        case R02 = "R02_user_res_clicked_change_address"
        case R03 = "R03_user_res_confirm_date"
        case R03A = "R03A_user_res_confirm_today"
        case R03B = "R03B_user_res_confirm_tomorrow"
        case R03C = "R03C_user_res_confirm_calendar"
        case R04 = "R04_reservation_menu_page_loaded"
        case R04A = "R0A4_reservation_buffet_menu_clicked"
        case R05 = "R05_user_res_smile_clicked"
        case R05A = "R05A_user_res_smile_applied_success"
        case R05B = "R05B_user_res_smile_applied_failure"
        case R06 = "R06_user_res_smile_remove_clicked"
        case R06A = "R06A_user_res_smile_remove_success"
        case R06B = "R06B_user_res_smile_remove_failure"
        case R07 = "R07_user_res_coupon_clicked"
        case R07A = "R07A_user_res_coupon_applied_success"
        case R07B = "R07B_user_res_coupon_applied_failure"
        case R08 = "R08_user_res_coupon_remove_clicked"
        case R08A = "R08A_user_res_coupon_remove_success"
        case R08B = "R08B_user_res_coupon_remove_failure"
        case R09 = "R09_user_res_hp_card_clicked"
        case R09A = "R09A_user_res_hp_card_applied_success"
        case R09B = "R09B_user_res_hp_card_applied_failure"
        case R10 = "R10_user_res_hp_card_remove_clicked"
        case R10A = "R10A_user_res_hp_card_remove_success"
        case R10B = "R10B_user_res_hp_card_remove_failure"
        case R11 = "R11_user_res_corporate_clicked"
        case R11A = "R11A_user_res_corporate_applied_success"
        case R11B = "R11B_user_res_corporate_applied_failure"
        case R12 = "R12_user_res_corporate_remove_clicked"
        case R12A = "R12A_user_res_corporate_remove_success"
        case R12B = "R12B_user_res_corporate_remove_failure"
        case R13 = "R13_user_res_clicked_change_address"
        case R14 = "R14_user_res_reserve_table_clicked"
        case R14A = "R14A_user_res_reserve_table_success"
        case R14B = "R14B_user_res_reserve_table_failure"
        case R15 = "R15_res_adv_payment_required"
        case R16 = "R16_res_adv_no_payment"
        
        //MARK: - Reservation Confirmation Page Event
        case RC01 = "RC01_rc_page_load"
        case RC02 = "RC02_rc_facilities_clicked"
        case RC03 = "RC03_rc_phone_clicked"
        case RC04 = "RC04_rc_menu_clicked"
        case RC05 = "RC05_rc_ambience_clicked"
        case RC06 = "RC06_rc_offers_clicked"
        case RC07 = "RC07_rc_special_request_clicked"
        case RC08 = "RC08_rc_details_clicked"
        case RC09 = "RC09_rc_closed_clicked"
        
        //MARK: - Delivery Home Page Event
        case D01 = "D01_delivery_home_page_loaded"
        case D02 = "D02_delivery_home_page_viewed"
        case D03 = "D03_user_del_clicked_branch_address"
        case D03A = "D03A_user_del_change_branch_address"
        case D04 = "D04_user_del_delivery_address_clicked"
        case D04A = "D04A_user_del_delivery_address_changed"
        case D05 = "D05_user_del_selected_veg_only"
        case D05A = "D05A_user_del_diselected_veg_only"
        case D06 = "D06_user_del_combox_added"
        case D06A = "D06A_user_del_combox_increase"
        case D06B = "D06B_user_del_combox_decrease"
        case D07 = "D07_user_del_comitem_added"
        case D07A = "D07A_user_del_comitem_increase"
        case D07B = "D07B_user_del_comitem_decrease"
        case D08 = "D08_user_del_item_added"
        case D08A = "D08A_user_del_item_increase"
        case D08B = "D08B_user_del_item_decrease"
        case D09 = "D09_user_del_select_home_delivery"
        case D10 = "D10_user_del_select_takeaway"
        case D11 = "D11_user_del_unit_added"
        case D11A = "D11A_user_del_unit_increase"
        case D11B = "D11B_user_del_unit_decrease"
        case D12 = "D12_user_del_clicked_schedule_into"
        case D13 = "D13_user_del_brand_intro_display"
        case D14 = "D14_user_del_brand_picker_clicked"
        case D14A = "D14A_user_del_brand_changed"
        case D14B = "D14B_user_del_brand_change_dismiss"
        case D15 = "D15_user_del_brand_not_available"
        case D15A = "D15A_user_del_select_branch_clicked"
        case D15B = "D15B_user_del_select_brand_clicked"
        
        //MARK: - Delivery Cart Page Event
        case C01 = "C01_delivery_cart_page_loaded"
        case C02 = "C02_delivery_cart_page_viewed"
        case C02A = "C02A_delivery_cart_data_avaliable"
        case C03 = "C03_user_cart_delivery_address_clicked"
        case C03A = "C03A_user_cart_delivery_address_changed"
        case C04 = "C04_user_cart_back_clicked"
        case C05 = "C05_user_cart_clear_clicked"
        case C06 = "C06_user_cart_item_increase"
        case C06A = "C06A_user_cart_item_decrease"
        case C07 = "C07_user_cart_select_delivery"
        case C08 = "C08_user_cart_select_takeaway"
        case C08A = "C08A_user_cart_takeaway_confirm_timeslot"
        case C09 = "C09_user_cart_select_schedule"
        case C09A = "C09A_user_cart_schedule_confirm_timeslot"
        case C10 = "C10_user_cart_smile_clicked"
        case C10A = "C10A_user_cart_all_smile_clicked"
        case C10B = "C10B_user_cart_partial_smile_clicked"
        case C10C = "C10C_user_cart_smile_applied_success"
        case C10D = "C10D_user_cart_smile_applied_failure"
        case C11 = "C11_user_cart_smile_remove_clicked"
        case C11A = "C11A_user_cart_smile_remove_success"
        case C11B = "C11B_user_cart_smile_remove_failure"
        case C12S = "C12S_user_cart_coupon_hp_clicked"
        case C12 = "C12_user_cart_coupon_clicked"
        case C12A = "C12A_user_cart_coupon_applied_success"
        case C12B = "C12B_user_cart_coupon_applied_failure"
        case C13 = "C13_user_cart_coupon_remove_clicked"
        case C13A = "C13A_user_cart_coupon_remove_success"
        case C13B = "C13B_user_cart_coupon_remove_failure"
        case C14 = "C14_user_cart_hp_card_clicked"
        case C14A = "C14A_user_cart_hp_card_applied_success"
        case C14B = "C14B_user_cart_hp_card_applied_failure"
        case C15 = "C15_user_cart_hp_card_remove_clicked"
        case C15A = "C15A_user_cart_hp_card_remove_success"
        case C15B = "C15B_user_cart_hp_card_remove_failure"
        case C16 = "C16_user_cart_enter_bar_code"
        case C16A = "C16A_user_cart_bar_code_applied_success"
        case C16B = "C16B_user_cart_bar_code_applied_failure"
        case C17 = "C17_user_cart_pay_clicked"
        case C17A = "C17A_user_cart_pay_success"
        case C17B = "C17B_user_cart_pay_failure"
        case C18 = "C18_user_cart_place_order_clicked"
        case C18A = "C18A_user_cart_place_order_success"
        case C18AA = "C18AA_user_cart_delivery_order"
        case C18AB = "C18AB_user_cart_schedule_order"
        case C18AC = "C18AC_user_cart_takeaway_order"
        case C18B = "C18B_user_cart_place_order_failure"
        case C19 = "C19_user_del_clicked_taxes_charges"
        case C20 = "C20_user_cart_offer_clicked"
        case C20A = "C20A_user_cart_offer_applied_success"
        case C20B = "C20B_user_cart_offer_applied_failure"
        case C21 = "C21_user_cart_offer_remove_clicked"
        case C21A = "C21A_user_cart_offer_remove_success"
        case C21B = "C21B_user_cart_offer_remove_failure"
        
        //MARK: - Cart Select Address Page Event
        case CA01 = "CA01_cart_select_address_page_loaded"
        case CA02 = "CA02_user_address_selected_enable"
        case CA02A = "CA02A_user_address_selected_applied"
        case CA03 = "CA03_user_address_selected_disable"
        case CA04 = "CA04_user_address_tap_add_new"
        case CA04A = "CA04_user_address_tap_add_new_from_cart_page"
        case CA05 = "CA05_user_address_back_clicked"
        
        //MARK: - Cart Coupon Code Page Event
        case CC01 = "CC01_cart_coupon_page_loaded"
        case CC02 = "CC02_user_coupon_available"
        case CC02A = "CC02A_user_coupon_not_available"
        case CC03 = "CC03_user_hp_card_available"
        case CC03A = "CC03A_user_hp_card_not_available"
        case CC04 = "CC04_user_coupon_apply_clicked"
        case CC04A = "CC04A_user_coupon_apply_success"
        case CC04B = "CC04B_user_coupon_apply_failed"
        case CC05 = "CC05_user_hp_card_apply_clicked"
        case CC05A = "CC05A_user_hp_card_apply_success"
        case CC05B = "CC05B_user_hp_card_apply_failure"
        case CC06 = "CC06_user_manually_enter_code"
        case CC06A = "CC06A_user_manually_enter_code_success"
        case CC06B = "CC06B_user_manually_enter_code_failure"
        case CC07 = "CC07_user_clicked_coupon_view_details"
        case CC08 = "CC08_user_clicked_hp_card_view_details"
        case CC09 = "CC09_user_offer_apply_clicked"
        case CC09A = "CC09A_user_offer_apply_success"
        case CC09B = "CC09B_user_offer_apply_failed"
        
        //MARK: - Manage Address Page Event
        case MA01 = "MA01_manage_address_page_loaded"
        case MA02 = "MA02_user_address_add_new"
        case MA03 = "MA03_user_address_edit_address"
        case MA04 = "MA04_user_address_delete_address"
        case MA04A = "MA04A_user_address_delete_address_success"
        case MA04B = "MA04B_user_address_delete_address_failure"
        
        //MARK: - Add/Edit Address Page Event
        case AE01 = "AE01_add_edit_page_loaded"
        case AE01A = "AE01A_user_add_new_address"
        case AE01B = "AE01B_user_edit_address"
        case AE02 = "AE02_user_change_address_clicked"
        case AE02A = "AE02A_user_change_address_from_mapview"
        case AE02B = "AE02B_user_change_address_from_search"
        case AE03 = "AE03_user_save_address"
        case AE03A = "AE03A_user_save_address_success"
        case AE03B = "AE03B_user_save_address_failure"
        case AE04 = "AE04_user_save_home_address"
        case AE04A = "AE04A_user_save_home_address_success"
        case AE04B = "AE04B_user_save_home_address_failure"
        case AE05 = "AE05_user_save_work_address"
        case AE05A = "AE05A_user_save_work_address_success"
        case AE05B = "AE05B_user_save_work_address_failure"
        case AE06 = "AE06_user_save_other_address"
        case AE06A = "AE06A_user_save_other_address_success"
        case AE06B = "AE06B_user_save_other_address_failure"
        
        //MARK: - Happiness Cart Page Event
        case HC01 = "HC01_happiness_cart_page_loaded"
        case HC02 = "HC02_hp_cart_clear_clicked"
        case HC02A = "HC02A_hp_cart_clear_success"
        case HC02B = "HC02B_hp_cart_clear_failure"
        case HC03 = "HC03_hp_cart_incerase_qty"
        case HC03A = "HC03A_hp_cart_incerase_qty_success"
        case HC03B = "HC03B_hp_cart_incerase_qty_failure"
        case HC04 = "HC04_hp_cart_smile_clicked"
        case HC04A = "HC04A_hp_cart_smile_applied_success"
        case HC04B = "HC04B_hp_cart_smile_applied_failure"
        case HC05 = "HC05_hp_cart_smile_remove_clicked"
        case HC05A = "HC05A_hp_cart_smile_remove_success"
        case HC05B = "HC05B_hp_cart_smile_remove_failure"
        case HC06 = "HC06_hp_cart_coupon_clicked"
        case HC06A = "HC06A_hp_cart_coupon_applied_success"
        case HC06B = "HC06B_hp_cart_coupon_applied_failure"
        case HC07 = "HC07_hp_cart_coupon_remove_clicked"
        case HC07A = "HC07A_hp_cart_coupon_remove_success"
        case HC07B = "HC07B_hp_cart_coupon_remove_failure"
        case HC08 = "HC08_hp_cart_decerase_qty"
        case HC08A = "HC08A_hp_cart_decerase_qty_success"
        case HC08B = "HC08B_hp_cart_decerase_qty_failure"
        case HC09 = "HC09_hp_cart_remove_item"
        case HC09A = "HC09A_hp_cart_remove_item_success"
        case HC09B = "HC09B_hp_cart_remove_item_failure"
        case HC10 = "HC10_hp_cart_buy_now_clicked"
        case HC10A = "HC10A_hp_cart_buy_now_success"
        case HC10B = "HC10B_hp_cart_buy_now_failure"
        case HC11 = "HC11_hp_cart_create_voucher"
        case HC11A = "HC11A_hp_cart_create_voucher_success"
        case HC11B = "HC11B_hp_cart_create_voucher_failure"
        
        //MARK: - Reservation/Booking History Page Event
        case RH01 = "RH01_reservation_history_page_loaded"
        case RH02 = "RH02_res_history_details_click"
        case RH02A = "RH02A_res_history_details_navigation"
        case RH02B = "RH02B_res_history_details_cab"
        case RH02C = "RH02C_res_history_details_call"
        case RH02D = "RH02D_res_history_details_paynow"
        case RH02E = "RH02E_res_history_paynow_settled"
        case RH02F = "RH02F_res_history_details_share"
        case RH03 = "RH03_res_history_booking_reschedule"
        case RH03A = "RH03A_res_booking_reschedule_called"
        case RH03B = "RH03B_res_booking_reschedule_success"
        case RH03C = "RH03C_res_booking_reschedule_failure"
        case RH04 = "RH04_res_history_booking_cancel"
        case RH04A = "RH04A_res_booking_cancel_called"
        case RH04B = "RH04B_res_booking_cancel_success"
        case RH04C = "RH04C_res_booking_cancel_failure"
        case RH05 = "RH05_res_booking_proccess_payment"
        case RH05A = "RH05A_res_booking_payment_success"
        case RH05B = "RH05B_res_booking_payment_failure"
        case RH06 = "RH06_res_booking_payment_update"
        case RH06A = "RH06A_res_booking_update_success"
        case RH06B = "RH06B_res_booking_update_failure"
        case RH07 = "RH07_res_history_active_clicked"
        case RH07A = "RH07A_res_history_active_pagination"
        case RH08 = "RH08_res_history_completed_clicked"
        case RH08A = "RH08_res_history_completed_pagination"
        case RH09 = "RH09_res_history_cancelled_clicked"
        case RH09A = "RH09_res_history_cancelled_pagination"
        
        //MARK: - Reservation/Booking Details Page Event
        case RD01 = "RD01_reservation_details_page_loaded"
        case RD02 = "RD02_res_history_details_info_click"
        case RD02A = "RD02A_res_details_navigation"
        case RD02B = "RD02B_res_details_cab"
        case RD02C = "RD02C_res_details_call"
        case RD02D = "RD02D_res_details_paynow"
        case RD02E = "RD02E_res_details_paynow_settled"
        case RD02F = "RD02F_res_details_share"
        case RD03 = "RD03_res_details_booking_reschedule"
        case RD03A = "RD03A_res_booking_reschedule_called"
        case RD03B = "RD03B_res_booking_reschedule_success"
        case RD03C = "RD03C_res_booking_reschedule_failure"
        case RD04 = "RD04_res_details_booking_cancel"
        case RD04A = "RD04A_res_booking_cancel_called"
        case RD04B = "RD04B_res_booking_cancel_success"
        case RD04C = "RD04C_res_booking_cancel_failure"
        case RD05 = "RD05_res_booking_proccess_payment"
        case RD05A = "RD05A_res_booking_payment_success"
        case RD05B = "RD05B_res_booking_payment_failure"
        case RD06 = "RD06_res_booking_payment_update"
        case RD06A = "RD06A_res_booking_update_success"
        case RD06B = "RD06B_res_booking_update_failure"
        
        //MARK: - Menu Image Page Event
        case MI01 = "MI01_menu_image_page_loaded"
        case MI02 = "MI02_menu_image_scroll"
        case MI03 = "MI03_menu_image_zoom"

        //MARK: - Ambience Image Page Event
        case AI01 = "AI01_ambience_image_page_loaded"
        case AI02 = " AI02_ambience_image_scroll"
        case AI03 = "AI03_ambience_image_zoom"
        
        //MARK: - Happiness Card History Page Event
        case HH01 = "HH01_happiness_history_page_loaded"
        case HH02 = "HH02_hp_history_gift_card"
        case HH03 = "HH03_hp_history_gift_card_process"
        case HH03A = "HH03A_hp_history_gift_card_success"
        case HH03B = "HH03B_hp_history_gift_card_failure"
        case HH04 = "HH04_hp_history_cell_clicked"
        
        //MARK: - Delivery Hisroty Page Event
        case DH01 = "DH01_delivery_history_page_loaded"
        case DH02 = "DH02_del_history_available"
        case DH02A = "DH02A_del_history_not_available"
        case DH03 = "DH03_del_history_order_clicked"
        case DH03A = "DH03A_del_history_order_confirm"
        case DH03B = "DH03B_del_history_order_proccessing"
        case DH03C = "DH03C_del_history_order_delivered"
        case DH03D = "DH03D_del_history_order_cancelled"
        case DH04 = "DH04_del_history_track_order"
        case DH05 = "DH05_del_history_rate_order"

        
        //MARK: - Special Offer Page Event
        case SO01 = "SO01_special_offer_page_loaded"
        case SO02 = "SO02_sp_offer_cliked"
        case SO03 = "SO03_sp_offer_reserve_clicked"
        
        //MARK: - Profile Page Event
        case P01 = "P01_profile_page_loaded"
        case P02 = "P02_profile_smile_clicked"
        case P03 = "P03_profile_edit_clicked"
        case P04 = "P04_profile_update_data"
        case P04A = "P04A_profile_update_success"
        case P04B = "P04B_profile_update_failure"
        case P05 = "P05_profile_notification_enabled_clicked"
        case P05A = "P05A_profile_notification_open_setting"
        case P06 = "P06_profile_notification_disabled_clicked"
        case P06A = "P06A_profile_notification_open_setting"
        
        //MARK: - Help/Support Page Event
        case HS01 = "HS01_help_support_page_loaded"
        case HS02 = "HS02_help_section_tapped"
        case HS03 = "HS03_help_cell_tapped"
        case HS04 = "HS04_help_contact_us_tapped"
        case HS04A = "HS04A_help_contact_us_call_tapped"
        case HS04B = "HS04B_help_contact_us_email_tapped"
        
        //MARK: - About Us Page Event
        case AU01 = "AU01_about_us_page_loaded"
        case AU02 = "AU02_about_us_bbq"
        
        //MARK: - Outlet Info Page Event
        case OI01 = "OI01_outlet_page_load"
        case OI02 = "OI02_outlet_menu_clicked"
        case OI02A = "OI02A_outlet_kid_menu_viewed"
        case OI02B = "OI02B_outlet_veg_menu_viewed"
        case OI02C = "OI02C_outlet_non_veg_menu_viewed"
        case OI02D = "OI02D_outlet_beverage_menu_viewed"
        case OI03 = "OI03_outlet_branch_change_tapped"
        case OI03A = "OI03A_outlet_branch_changed"
        case OI04 = "OI04_outlet_call_tapped"
        case OI05 = "OI05_outlet_facilities_tapped"
        case OI06 = "OI06_outlet_direction_tapped"
        case OI07 = "OI07_outlet_ambience_tapped"
        case OI08 = "OI08_outlet_promotion_tapped"
        
        //MARK: - Utitlity Event
        case UI01 = "UI01_swipe_right_gesture"
        
        //MARK: - After Dining Payment Event
        case AP01 = "AP01_dining_payment_page_loaded"
        case AP02 = "AP02_af_dining_smile_clicked"
        case AP03 = "AP03_af_dining_smile_remove_clicked"
        case AP04 = "AP04_af_dining_coupon_clicked"
        case AP05 = "AP05_af_dining_coupon_remove_clicked"
        case AP06 = "AP06_af_dining_hp_card_clicked"
        case AP07 = "AP07_af_dining_hp_card_remove_clicked"
        case AP08 = "AP08_af_dining_pay_now_clicked"
        case AP08A = "AP08A_af_dining_direct_settl_start"
        case AP09 = "AP09_af_dining_proccess_payment"
        case AP09A = "AP09A_af_dining_payment_success"
        case AP09B = "AP09B_af_dining_payment_failure"
        case AP10 = "AP10_af_dining_payment_update"
        case AP10A = "AP10A_af_dining_update_success"
        case AP10B = "AP10B_af_dining_update_failure"
        
        //MARK: - Deilvery Search Item Event
        case DS01 = "DS01_delivery_item_search_page_loaded"
        case DS02 = "DS02_ds_start_search_item"
        case DS02A = "DS02A_ds_search_item_found"
        case DS02B = "DS02B_ds_search_item_not_found"
        case DS02C = "DS02C_ds_search_character_less"
        case DS03 = "DS03_ds_combox_added"
        case DS03A = "DS03A_ds_combox_increase"
        case DS03B = "DS03B_ds_combox_decrease"
        case DS04 = "DS04_ds_comitem_added"
        case DS04A = "DS04A_ds_comitem_increase"
        case DS04B = "DS04B_ds_comitem_decrease"
        case DS05 = "DS05_ds_item_added"
        case DS05A = "DS05A_ds_item_increase"
        case DS05B = "DS05B_ds_item_decrease"
        case DS06 = "DS06_ds_item_clicked_on_item"
        case DS07 = "DS07_delivery_item_search_page_closed"
        
        
        // MARK: - Order Status page

        case OS01 =   "order_status_page_load"
        case OS02 = "order_status_pull_to_refresh"
        case OS03 = "order_status_bill_details_clicked"
        case OS04 = "order_status_get_api"
        case OS04A = "order_status__get_api_sucess"
        case OS04B = "order_status_get_api_failure"
        case OS05  = "order_status_current_status"
        case OS05A = "order_status_accepted"
        case OS05B = "order_status_confirmed"
        case OS05C = "order_status_prepared"
        case OS05D = "order_status_dispatched"
        case OS05E = "order_status_delivered"
        case OS06 = "order_status_call_driver"
        case OS07 = "order_status_call_branch"
   
       // MARK: - Order Feedback page
        case FB01 = "FB01_On_page_loaded"
        case FB02 = "FB02_Api_call_to_retreive_feedback_questions"
        case FB02A = "FB02A_Sucessfully_retreive_feedback_questions"
        case FB02B = "FB02B_Failed_to_retreive_feedback_questions"
        case FB03 = "FB03_On_click_of_submit_button"
        case FB04 = "FB04_on_click_of_taste_button"
        case FB05 = "FB05_on_click_of_pacake_button"
        case FB06 = "FB06_on_click_of_delivery_button"
        case FB07 = "FB07_submit_feedback_api_call"
        case FB07A = "FB07A_Successfully_submit_feedback"
        case FB07B = "FB07B_failed_to_submit_status"
        case FB09 = "FB09_on_did_select_question_of_star_type"
        case FB10 = "FB10_on_did_select_question_of_single_select_type"
        case FB11 = "FB11_on_did _select_question_of_toogle_type"
        
        
        
        //MARK: - MultiLocation Query
        case ML04 = "ML04_Clicked_on_multiLocation_query"
        case ML01 = "ML01_submit_multiLocation_Query_api_call"
        case ML02B = "ML02_failed_to_submit_multiLocation_Query"
        case ML02A = "ML02A_Successfully_submit_multiLocation_Query"
        case ML03A = "ML03A_Sucessfully_retreive_feedback_questions_for_multilocation"
        
        //MARK: - Happiness card details page
        case HD01 = "HD01_happiness_card_detail_page_loaded"
        case HD02 = "HD02_hp_detail_gift_card"
        case HD03 = "HD03_hp_detail_gift_card_process"
        case HD03A = "HD03A_hp_detail_gift_card_success"
        case HD03B = "HD03B_hp_detail_gift_card_failure"
        case HD04 = "HD04_hp_detail_terms_show_clicked"
        case HD04A = "HD04A_hp_detail_terms_hide_clicked"
        
        //MARK: - Advance reservation payment page
        case AD01 = "AD01_page_loaded"
        case AD02 = "AD02_back_btn_clicked"
        case AD02A = "AD02A_back_btn_confirm"
        case AD02B = "AD02B_back_btn_cancel"
        case AD03 = "AD03_adv_pay_btn_clicked"
        case AD03A = "AD03A_adv_pay_payment_success"
        case AD03B = "AD03B_adv_pay_payment_failure"
        case AD04 = "AD04_adv_pay_booking_started"
        case AD04A = "AD04A_adv_pay_booking_success"
        case AD04B = "AD04B_adv_pay_booking_failure"
        case AD05 = "AD05_adv_pay_payment_link"
        case AD05A = "AD05A_adv_pay_payment_link_success"
        case AD05B = "AD05B_adv_pay_payment_link_failure"
        
        
        //MARK: - Refer and Earn Page
        case RE01A = "RE01A_page_loaded_Home_Top_Button"
        case RE01B = "RE01B_page_loaded_Home_Bootom_Sticky_View"
        case RE02 = "RE02_page_loaded_Sidemenu"
        case RE03 = "RE03_refer_code_copy"
        case RE04 = "RE04_refer_code_shared"
        case RE05 = "RE05_app_installed_through_referal_code"
        case RE06 = "RE06_page_loaded_Order_Status"
        case RE07 = "RE07_page_loaded_Smile_History"
        case RE08 = "RE08_page_loaded_Profile_Screen"
        case RE09 = "RE09_page_loaded_Outlet_Info_Screen"
        case RE10 = "RE10_page_loaded_Happiness_Card_Detail_Screen"
        case RE11 = "RE10_page_loaded_Booking_Confirmation_Screen"
        case RE012 = "RE012_page_loaded_from_res_promotion_url"



     
        //MARK: - My Benefits Page
        case MY01 = "MY01_page_loaded"
        case MY02 = "MY02_smile_details_viewed"
        case MY03 = "MY03_coupon_details_viewed"
        case MY03A = "MY03A_my_benefits_coupon_copy"
        case MY03B = "MY03B_my_benefits_coupon_tc_clicked"
        case MY04 = "MY04_voucher_details_viewed"
        case MY04A = "MY04A_my_benefits_gift_card"
        case MY04B = "MY04B_my_benefits_gift_tc_clicked"
        case MY04C = "MY04C_my_benefits_gift_details_clicked"

        //MARK: - Custom Payment Page
        case CP01 = "CP01_page_loaded"
        case CP02 = "CP02_upi_payment_started"
        case CP02A = "CP02A_upi_saved_payment_started"
        case CP02B = "CP02B_upi_add_new"
        case CP02S = "CP02S_upi_save_checked"
        case CP02BA = "CP02BA_upi_new_payment_started"
        case CP03 = "CP03_card_payment_started"
        case CP03A = "CP03A_card_saved_payment_started"
        case CP03B = "CP03B_card_add_new"
        case CP03S = "CP03S_card_save_checked"
        case CP03BA = "CP03BA_card_new_payment_started"
        case CP04 = "CP04_net_banking_payment_started"
        case CP05 = "CP05_wallet_payment_started"
        case CP06 = "CP06_payment_started"
        case CP06A = "CP06A_payment_success"
        case CP06AA = "CP06AA_upi_payment_success"
        case CP06AB = "CP06AB_card_payment_success"
        case CP06AC = "CP06AC_net_banking_payment_success"
        case CP06AD = "CP06AD_wallet_payment_success"
        case CP06B = "CP06B_payment_failed"
        case CP06BA = "CP06BA_upi_payment_failed"
        case CP06BB = "CP06BB_card_payment_failed"
        case CP06BC = "CP06BC_net_banking_payment_failed"
        case CP06BD = "CP06BD_wallet_payment_failed"
        case CP06C = "CP06C_payment_cancelled"
        case CP06CA = "CP06CA_upi_payment_cancelled"
        case CP06CB = "CP06CB_card_payment_cancelled"
        case CP06CC = "CP06CC_net_banking_payment_cancelled"
        case CP06CD = "CP06CD_wallet_payment_cancelled"
        case CP07 = "CP07_payment_retry_dialod"
        case CP07A = "CP07A_payment_retry_upi"
        case CP07B = "CP07B_payment_retry_card"
        case CP07C = "CP07C_payment_retry_another_method"
        case CP07D = "CP07D_payment_retry_cancelled"
        
        
        //MARK: - App version update
        case VU01 = "VU01_update_version_from_Homepage_alert"
        case VU02 = "VU01_update_version_from_Homepage_bottom_strip"


        
    }
    
    //MARK: - MoEngage Events
    enum MoEngageEvent: String{
        case Login_Home = "Login_Home"
        case App_Install = "App_Install"
        case Footer_App_Intsall = "Footer_App_Intsall"
        case Reservation = "Reservation"
        case Reservation_Checkout = "Reservation_Checkout"
        case Reservation_Status = "Reservation_Status"
        case Home_Delivery_Order_Now = "Home_Delivery_Order_Now"
        case Home_Delivery_Takeaway = "Home_Delivery_Takeaway"
        case Delivery_Takeaway_Catalog = "Delivery_Takeaway_Catalog"
        case Delivery_Takeaway_Product_Item_info = "Delivery_Takeaway_Product_Item_info"
        case Delivery_Takeaway_Added = "Delivery_Takeaway_Added"
        case Delivery_Takeaway_Checkout = "Delivery_Takeaway_Checkout"
        case Delivery_Takeaway_Pay = "Delivery_Takeaway_Pay"
        case Delivery_Takeaway_Order_Status = "Delivery_Takeaway_Order_Status"
        case Home_Happinesscard = "Home_Happinesscard"
        case Happinesscard_Added = "Happinesscard_Added"
        case Happinesscard_Checkout = "Happinesscard_Checkout"
        case Happinesscard_Pay = "Happinesscard_Pay"
        case Happinesscard_Order_Status = "Happinesscard_Order_Status"
    }
    
    enum MoEngageAttrKey: String{
        case Login_Type = "Login_Type"
        case Mobile_Number = "Mobile_Number"
        case User_Creation = "User_Creation"
        case Location = "Location"
        case Page_Name = "Page_Name"
        case Calendar = "Calendar"
        case Time = "Time"
        case Reserve_Button = "Reserve_Button"
        case Booking_id = "Booking_id"
        case Reservation_booked_Status = "Reservation_booked_Status"
        case Error = "Error"
        case Delivery_Takeaway = "Delivery_Takeaway"
        case Food_Type = "Food_Type"
        case Catalog_Items = "Catalog_Items"
        case Catalog_Product_Item = "Catalog_Product_Item"
        case Catalog_Product_Item_info = "Catalog_Product_Item_info"
        case Catalog_Product_Item_Cart_Selection = "Catalog_Product_Item_Cart_Selection"
        case Catalog_Cart_Checkout = "Catalog_Cart_Checkout"
        case Delivery_Takeaway_Pay = "Delivery_Takeaway_Pay"
        case Delivery_Takeaway_Status = "Delivery_Takeaway_Status"
        case Oderid = "Oderid"
        case Buy_now = "Buy now"
        case Happiness_Card_Title = "Happiness_Card_Title"
        case Happiness_Cart_Checkout_Amount = "Happiness_Cart_Checkout_Amount"
        case Happiness_Card_Pay = "Happiness_Card_Pay"
        case Happiness_Card_Success = "Happiness_Card_Success"
        case Happiness_Card_Cart_Count = "Happiness_Card_Cart_Count"
    }
    
    enum MoEngageAttrValue: String{
        case Success = "Success"
        case Failed = "Failed"
        case Google = "Google"
        case Facebook = "Facebook"
        case Mobile = "Mobile"
        case User_Selected_Delivery = "User_Selected_Delivery"
        case User_Selected_Takeaway = "User_Selected_Takeaway"
    }
    
    
    //MARK: - Class Files
    private override init() {
        super.init()
    }
    
    static let shared = AnalyticsHelper()
    
    //MARK: - Analytics Functions
    func triggerEvent(type: GoogleEvent, parameters: [String : Any]? = nil) {
        #if DEBUG
            
        #else
            Analytics.logEvent(type.rawValue, parameters: parameters)
        #endif
        
    }
    
    func triggerEvent(type: AnalyticsEvent, parameters: [String : Any]? = nil) {
        #if DEBUG
            
        #else
            Analytics.logEvent(type.rawValue, parameters: parameters)
        #endif
    }
    

    
    
    func screenName(name: String, className: String? = nil) {
        #if DEBUG
            
        #else
        //Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: name, AnalyticsParameterScreenClass: className])
        #endif
    }
    
    func triggerEvent(type: MoEngageEvent, properties: [MoEngageAttrKey: Any] = [:]) {
        //Removed MoEngage Library code
//        if type == .Login_Home{
//            setMoEngageUserProfile()
//        }
//        MOAnalytics.sharedInstance.trackEvent(type.rawValue, withProperties: moEngageProperties(properties: properties))
    }
    
    
}

extension NSObject{
    func screenName(name: AnalyticsHelper.GoogleScreenTrack) {
        #if DEBUG
        
        #else
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: name.rawValue, AnalyticsParameterScreenClass: String(describing: type(of: self))])
        #endif
    }
}

//Removed MoEngage Library code

//    func setMoEngageUserProfile(){
//
//        if BBQUserDefaults.sharedInstance.customerId != ""{
//            MOAnalytics.sharedInstance.setUniqueID(BBQUserDefaults.sharedInstance.customerId)
//            MOAnalytics.sharedInstance.setAlias(BBQUserDefaults.sharedInstance.customerId)
//
//            MOAnalytics.sharedInstance.setEmailID(SharedProfileInfo.shared.profileData?.email ?? "")
//            MOAnalytics.sharedInstance.setMobileNumber(BBQUserDefaults.sharedInstance.customerId)
//            MOAnalytics.sharedInstance.setName(BBQUserDefaults.sharedInstance.UserName)
//
//            let nameArray = BBQUserDefaults.sharedInstance.UserName.components(separatedBy: " ")
//
//            if nameArray.count > 1{
//                MOAnalytics.sharedInstance.setFirstName(nameArray.first ?? "")
//                MOAnalytics.sharedInstance.setLastName(nameArray.last ?? "")
//            }else{
//                MOAnalytics.sharedInstance.setFirstName(nameArray.first ?? "")
//            }
//
//            if let title = SharedProfileInfo.shared.profileData?.title, title == "Mr"{
//                MOAnalytics.sharedInstance.setGender(.male)
//            }else{
//                MOAnalytics.sharedInstance.setGender(.female)
//            }
//
//        }
//
//
//
//
//    }

//    func resetMoEngageuser() {
//        MOAnalytics.sharedInstance.resetUser()
//    }
//    private func moEngageProperties(properties: [MoEngageAttrKey: Any]) -> MOProperties?{
//        let attributes = MOProperties()
//        for object in properties {
//            attributes.addAttribute(object.value, withName: object.key.rawValue)
//        }
//        attributes.addAttribute("Mobile", withName: MoEngageAttrKey.Login_Type.rawValue)
//        if BBQUserDefaults.sharedInstance.customerId != ""{
//            attributes.addAttribute(BBQUserDefaults.sharedInstance.customerId, withName: MoEngageAttrKey.Mobile_Number.rawValue)
//        }
//        return attributes
//    }
