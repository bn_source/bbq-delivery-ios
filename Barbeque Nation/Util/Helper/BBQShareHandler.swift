//
//  Created by Abhijit on 18/09/19
//  Copyright (c) BARBEQUE NATION HOSPITALITY LIMITED 2019.
//  All rights reserved.
//  Last modified BBQSharingImplementation.swift
//

import Foundation
import UIKit

enum ShareDataType {
    case Text, Url, Image, attributedText
}

struct ShareModel {
    var data: String
    var type: ShareDataType
    var attributedText : NSMutableAttributedString?
}

class BBQShareHandler {
    
    class func shareContent(contentToShare: ShareModel..., on controller: UIViewController, isBottomSheetPresenter: Bool = false) {
        
        var contentsArray = [Any]()
        var contentToAdd: Any = ""
        for content in contentToShare {
            switch content.type {
            case .Text:
                 contentToAdd = content.data as Any
                
            case .Url:
                contentToAdd = URL(string: content.data)!
                
            case .Image:
                contentToAdd = UIImage(named: content.data)!
            
            case .attributedText:
                contentToAdd = content.attributedText as Any
            }
            contentsArray.append(contentToAdd)
        }
        
        let activityViewController = UIActivityViewController(activityItems: contentsArray, applicationActivities: nil)
        activityViewController.modalPresentationStyle = .overCurrentContext
        activityViewController.completionWithItemsHandler = {(activityType: UIActivity.ActivityType?, completed: Bool, returnedItems: [Any]?, error: Error?) in
            if completed {
                UIUtils.showToast(message: "message shared.")
                return
            }
        }
        controller.present(activityViewController, animated: true, completion: nil)
    }
}
